﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.AboutAdi
{
    [TestFixture]
    public class AboutAdi_AboutAdiLandingPage : BaseSetUp
    {
        public AboutAdi_AboutAdiLandingPage() : base() { }

        //--- URLs ---//
        string aboutAdi_page_url = "/about-adi.html";
        string corporateInformation_page_url = "/corporate-information.html";
        string investorRelations_page_url = "investor.analog.com";
        string newsRoom_page_url = "/news-room.html";
        //string adiNewsletters_page_url = "/newsletters.html";
        string events_page_url = "/events.html";
        string qualityAndReliability_page_url = "/quality-reliability.html";
        //string careers_page_url = "careers.analog.com";
        string contactUs_page_url = "/contact-us.html";

        [Test, Category("About ADI"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Left Rail Navigation is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Left Rail Navigation is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Left Rail Navigation is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Left Rail Navigation is Present and Working as Expected in RU Locale")]
        public void AboutAdi_AboutAdiLandingPage_VerifyLeftRailNavigation(string Locale)
        {
            //--- Action: Navigate to the About ADI Landing Page ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + aboutAdi_page_url);

            //----- About ADI TestPlan > Desktop Tab > R1 > T6: Verify all the links from left rails navigation -----//

            if (util.CheckElement(driver, Elements.AboutAdi_LeftRailNav_CorporateInformation_Link, 1))
            {
                //--- Action: Click on Corporate Information ---//
                action.IOpenLinkInNewTab(driver, Elements.AboutAdi_LeftRailNav_CorporateInformation_Link);
                Thread.Sleep(2000);

                //--- Expected Result: Corporate Information page should be displayed ---//
                test.validateStringInstance(driver, driver.Url, corporateInformation_page_url);

                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles.First());
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.AboutAdi_LeftRailNav_CorporateInformation_Link);
            }

            if (util.CheckElement(driver, Elements.AboutAdi_LeftRailNav_InvestorRelations_Link, 1))
            {
                //--- Action: Click on Investor Relations ---//
                action.IOpenLinkInNewTab(driver, Elements.AboutAdi_LeftRailNav_InvestorRelations_Link);
                Thread.Sleep(3000);

                //--- Expected Result: User should be redirected to Investor Relations landing page ---//
                test.validateStringInstance(driver, driver.Url, investorRelations_page_url);

                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles.First());
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.AboutAdi_LeftRailNav_InvestorRelations_Link);
            }

            if (util.CheckElement(driver, Elements.AboutAdi_LeftRailNav_NewsRoom_Link, 1))
            {
                //--- Action: Click on News and Media Relations ---//
                action.IOpenLinkInNewTab(driver, Elements.AboutAdi_LeftRailNav_NewsRoom_Link);
                Thread.Sleep(2000);

                //--- Expected Result: News and Media Relations page should be displayed ---//
                test.validateStringInstance(driver, driver.Url, newsRoom_page_url);

                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles.First());
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.AboutAdi_LeftRailNav_NewsRoom_Link);
            }

            ////NOTE: PENDING. Manual Testing Team to provide the URL for the Expected Result.
            //if (util.CheckElement(driver, Elements.AboutAdi_LeftRailNav_Newsletters_Link, 1))
            //{
            //    //--- Action: Click on Newsletters ---//
            //    action.IOpenLinkInNewTab(driver, Elements.AboutAdi_LeftRailNav_Newsletters_Link);

            //    //--- Expected Result: ADI newsletter page is displayed ---//
            //    test.validateStringInstance(driver, driver.Url, adiNewsletters_page_url);

            //    driver.Close();
            //    driver.SwitchTo().Window(driver.WindowHandles.First());
            //}
            //else
            //{
            //    test.validateElementIsPresent(driver, Elements.AboutAdi_LeftRailNav_Newsletters_Link);
            //}

            if (util.CheckElement(driver, Elements.AboutAdi_LeftRailNav_Events_Link, 1))
            {
                //--- Action: Click on Events ---//
                action.IOpenLinkInNewTab(driver, Elements.AboutAdi_LeftRailNav_Events_Link);
                Thread.Sleep(2000);

                //--- Expected Result: Events page is displayed ---//
                test.validateStringInstance(driver, driver.Url, events_page_url);

                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles.First());
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.AboutAdi_LeftRailNav_Events_Link);
            }

            if (util.CheckElement(driver, Elements.AboutAdi_LeftRailNav_QualityAndReliability_Link, 1))
            {
                //--- Action: Click on Quality and Reliability ---//
                action.IOpenLinkInNewTab(driver, Elements.AboutAdi_LeftRailNav_QualityAndReliability_Link);
                Thread.Sleep(3000);

                //--- Expected Result: Quality and Reliability should be displayed ---//
                test.validateStringInstance(driver, driver.Url, qualityAndReliability_page_url);

                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles.First());
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.AboutAdi_LeftRailNav_QualityAndReliability_Link);
            }

            ////NOTE: PENDING. Manual Testing Team to provide the URL for the Expected Result.
            //if (util.CheckElement(driver, Elements.AboutAdi_LeftRailNav_Careers_Link, 1))
            //{
            //    //--- Action: Click on Careers ---//
            //    action.IOpenLinkInNewTab(driver, Elements.AboutAdi_LeftRailNav_Careers_Link);

            //    //--- Expected Result: Careers page should be displayed ---//
            //    test.validateStringInstance(driver, driver.Url, careers_page_url);

            //    driver.Close();
            //    driver.SwitchTo().Window(driver.WindowHandles.First());
            //}
            //else
            //{
            //    test.validateElementIsPresent(driver, Elements.AboutAdi_LeftRailNav_Careers_Link);
            //}

            Thread.Sleep(1000);
            if (util.CheckElement(driver, Elements.AboutAdi_LeftRailNav_ContactUs_Link, 1))
            {
                //--- Action: Click on Contact Us ---//
                action.IOpenLinkInNewTab(driver, Elements.AboutAdi_LeftRailNav_ContactUs_Link);
                Thread.Sleep(1000);

                //--- Expected Result: Contact Us page should be displayed ---//
                test.validateStringInstance(driver, driver.Url, contactUs_page_url);
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.AboutAdi_LeftRailNav_ContactUs_Link);
            }
        }

        [Test, Category("About ADI"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the About ADI Landing Page is Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the About ADI Landing Page is Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the About ADI Landing Page is Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the About ADI Landing Page is Working as Expected in RU Locale")]
        public void AboutAdi_VerifyAboutAdiLandingPage(string Locale)
        {
            //--- Action: Navigate to the About ADI Landing Page ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + aboutAdi_page_url);

            //----- About ADI TestPlan > Desktop Tab > R1 > T2: Verify About ADI Landing Page -----//

            //--- Expected Result: An ADI company video should be displayed ---//
            test.validateElementIsPresent(driver, Elements.AboutAdi_LandingPage_Video);

            //--- Expected Result: Description should be displayed ---//
            test.validateElementIsPresent(driver, Elements.AboutAdi_LandingPage_Video_Desc_Txt);
        }
    }
}