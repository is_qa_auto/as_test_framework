﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Threading;

namespace AS_Test_Framework.AboutAdi
{
    [TestFixture]
    public class AboutAdi_PrivacyAndSecurityStatement : BaseSetUp
    {
        public AboutAdi_PrivacyAndSecurityStatement() : base() { }

        //--- URLs ---//
        string privacyAndSecurityStatement_page_url = "/about-adi/landing-pages/001/privacy_security_statement.html";

        //--- Labels ---//
        string acceptedCookies_message = "You have accepted cookies.";
        string declinedCookies_message = "You have declined cookies.";

        [Test, Category("About ADI"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Privacy and Security Statement Page is Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Privacy and Security Statement Page is Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Privacy and Security Statement Page is Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Privacy and Security Statement Page is Working as Expected in RU Locale")]
        public void AboutAdi_VerifyPrivacyAndSecurityStatementPage(string Locale)
        {
            //--- Action: Navigate to Privacy & Security Statement Page ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + privacyAndSecurityStatement_page_url);

            //----- About ADI TestPlan > Desktop Tab > R1 > T8: Verify if the site visitors to visit the privacy page by clicking on the link in the consent banner (IQ-7935/AL-15167) -----//

            if (util.CheckElement(driver, Elements.AboutAdi_PrivacyAndSecurityStatement_AcceptAndProceed_Button, 1))
            {
                //--- Action: Click Accept and proceed button. ---//
                action.IClick(driver, Elements.AboutAdi_PrivacyAndSecurityStatement_AcceptAndProceed_Button);
                Thread.Sleep(1000);

                //--- Expected Result: message "You have accepted cookies" will appear below the button ---//
                if (Locale.Equals("en"))
                {
                    test.validateStringIsCorrect(driver, Elements.AboutAdi_PrivacyAndSecurityStatement_AcceptedCookies_Message, acceptedCookies_message);
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.AboutAdi_PrivacyAndSecurityStatement_AcceptedCookies_Message);
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.AboutAdi_PrivacyAndSecurityStatement_AcceptAndProceed_Button);
            }

            if (util.CheckElement(driver, Elements.AboutAdi_PrivacyAndSecurityStatement_DeclineCookies_Button, 1))
            {
                //--- Action: Click Decline cookies link ---//
                action.IClick(driver, Elements.AboutAdi_PrivacyAndSecurityStatement_DeclineCookies_Button);

                //--- Expected Result: message "You have declined cookies" will appear below the button ---//
                if (Locale.Equals("en"))
                {
                    test.validateStringIsCorrect(driver, Elements.AboutAdi_PrivacyAndSecurityStatement_DeclinedCookies_Message, declinedCookies_message);
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.AboutAdi_PrivacyAndSecurityStatement_DeclinedCookies_Message);
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.AboutAdi_PrivacyAndSecurityStatement_DeclineCookies_Button);
            }
        }
    }
}