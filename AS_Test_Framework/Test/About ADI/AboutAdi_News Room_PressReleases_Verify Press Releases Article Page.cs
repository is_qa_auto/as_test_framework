﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;

namespace AS_Test_Framework.AboutAdi
{
    [TestFixture]
    public class AboutAdi_NewsRoom_PressReleases_PressReleasesArticlePage : BaseSetUp
    {
        public AboutAdi_NewsRoom_PressReleases_PressReleasesArticlePage() : base() { }

        [Test, Category("About ADI"), Category("Core"), Retry(2)]
        [TestCase("en", "/about-adi/news-room/press-releases/2018/adi-72v-hybrid-step-down-dc-dc-controller-reduces-solution-size-by-50-compared-to-traditional.html", TestName = "Verify that the Press Releases Article Page is Working as Expected in EN Locale")]
        [TestCase("cn", "/about-adi/news-room/press-releases/2018/adi-72v-hybrid-step-down-dc-dc-controller-reduces-solution-size-by-50-compared-to-traditional.html", TestName = "Verify that the Press Releases Article Page is Working as Expected in CN Locale")]
        [TestCase("jp", "/about-adi/news-room/press-releases/2016/11-22-2016-adi-fourth-quarter-fiscal-year2016-results.html", TestName = "Verify that the Press Releases Article Page is Working as Expected in JP Locale")]
        [TestCase("ru", "/about-adi/news-room/press-releases/2016/07-12-2016-teson-ru.html", TestName = "Verify that the Press Releases Article Page is Working as Expected in RU Locale")]
        public void AboutAdi_NewsRoom_PressReleases_VerifyPressReleasesArticlePage(string Locale, string PressReleases_Article_Page_Url)
        {
            //--- Action: Navigate to the Press Releases Page ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + PressReleases_Article_Page_Url);

            //----- About ADI TestPlan > Desktop Tab > R3 > T7_1: Validate that the Press Releases page -----//

            //--- Expected Result: Press release description is available ---//
            test.validateElementIsPresent(driver, Elements.AboutAdi_PressReleasesArticlePage_Desc_Text);

            if (util.CheckElement(driver, Elements.AboutAdi_PressReleasesArticlePage_Image, 1))
            {
                //----- About ADI TestPlan > Desktop Tab > R3 > T9: Verify the Images in a Press Releases Page (AL-10873) -----//

                //--- Action: Click on the Image ---//
                action.IClick(driver, Elements.AboutAdi_PressReleasesArticlePage_Image);

                //--- Expected Result: The Image Light Box will be Displayed. ---//
                test.validateElementIsPresent(driver, Elements.AboutAdi_PressReleasesArticlePage_Image_LightBox);

                if (util.CheckElement(driver, Elements.AboutAdi_PressReleasesArticlePage_Image_LightBox, 1))
                {
                    //--- Action: Click the X Button in the Light Box ---//
                    action.IClick(driver, Elements.AboutAdi_PressReleasesArticlePage_Image_LightBox_X_Button);

                    //--- Expected Result: The Image Light Box will be Closed. ---//
                    test.validateElementIsNotPresent(driver, Elements.AboutAdi_PressReleasesArticlePage_Image_LightBox);
                }

                if (util.CheckElement(driver, Elements.AboutAdi_PressReleasesArticlePage_Image_LightBox, 1))
                {
                    driver.Navigate().Refresh();
                }

                if (util.CheckElement(driver, Elements.AboutAdi_PressReleasesArticlePage_Image_ZoomIn_Button, 1))
                {
                    //--- Action: Click on the Zoom In Button ---//
                    action.IClick(driver, Elements.AboutAdi_PressReleasesArticlePage_Image_ZoomIn_Button);

                    //--- Expected Result: The Image Light Box will be Displayed. ---//
                    test.validateElementIsPresent(driver, Elements.AboutAdi_PressReleasesArticlePage_Image_LightBox);
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.AboutAdi_PressReleasesArticlePage_Image_ZoomIn_Button);
                }
            }
        }
    }
}