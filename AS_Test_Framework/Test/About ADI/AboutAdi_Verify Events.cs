﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.AboutAdi
{
    [TestFixture]
    public class AboutAdi_Events : BaseSetUp
    {
        public AboutAdi_Events() : base() { }

        //--- URLs ---//
        string events_page_url = "/about-adi/events.html";

        [Test, Category("About ADI"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Left Rail is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Left Rail is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Left Rail is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Left Rail is Present and Working as Expected in RU Locale")]
        public void AboutAdi_Events_VerifyLeftRail(string Locale)
        {
            //--- Action: Navigate to About ADI - Events ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + events_page_url);

            //----- About ADI TestPlan > Desktop Tab > R5 > T1: Verify Events landing page -----//

            //--- Expected Result: Events  from the left rail should be highlighted. ---//
            test.validateElementIsPresent(driver, Elements.AboutAdi_LeftRailNav_Selected_Events_Link);
        }

        [Test, Category("About ADI"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Events Page is Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Events Page is Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Events Page is Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Events Page is Working as Expected in RU Locale")]
        public void AboutAdi_VerifyEventsPage(string Locale)
        {
            //--- Action: Navigate to About ADI - Events ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + events_page_url);

            //----- About ADI TestPlan > Desktop Tab > R1 > T6: Verify all the links from left rails navigation -----//

            //--- Expected Result: Accordion must be collapsed. (AL-9494) ---//
            test.validateElementIsNotPresent(driver, Elements.AboutAdi_Events_Expanded_Accordion_Sections);
            test.validateElementIsNotPresent(driver, Elements.AboutAdi_Events_Expanded_Accordions);
        }
    }
}