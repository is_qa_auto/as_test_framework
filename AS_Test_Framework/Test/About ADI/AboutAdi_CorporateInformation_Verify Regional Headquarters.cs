﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;

namespace AS_Test_Framework.AboutAdi
{
    [TestFixture]
    public class AboutAdi_CorporateInformation_RegionalHeadquarters : BaseSetUp
    {
        public AboutAdi_CorporateInformation_RegionalHeadquarters() : base() { }

        //--- URLs ---//
        string regionalHeadquarters_page_url = "/about-adi/corporate-information/regional-headquarters.html";

        [Test, Category("About ADI"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Regional Headquarters Page is Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Regional Headquarters Page is Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Regional Headquarters Page is Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Regional Headquarters Page is Working as Expected in RU Locale")]
        public void AboutAdi_CorporateInformation_VerifyRegionalHeadquarters(string Locale)
        {
            //--- Action: Navigate to the Regional Headquarters Page ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + regionalHeadquarters_page_url);

            //----- About ADI TestPlan > Desktop Tab > R2 > T4: Verify Regional Headquarters -----//

            //--- Expected Result: The Window Title is not empty ---//
            test.validateWindowTitleIsCorrect(driver, util.GetText(driver, Elements.AboutAdi_2nd_Column_Page_Title_Txt));
        }
    }
}