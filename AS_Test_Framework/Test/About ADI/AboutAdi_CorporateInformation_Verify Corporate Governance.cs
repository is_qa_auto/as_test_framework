﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;

namespace AS_Test_Framework.AboutAdi
{
    [TestFixture]
    public class AboutAdi_CorporateInformation_CorporateGovernance : BaseSetUp
    {
        public AboutAdi_CorporateInformation_CorporateGovernance() : base() { }

        //--- URLs ---//
        string corporateGovernance_page_url = "/about-adi/corporate-information/corporate-governance.html";

        [Test, Category("About ADI"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Corporate Governance Page is Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Corporate Governance Page is Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Corporate Governance Page is Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Corporate Governance Page is Working as Expected in RU Locale")]
        public void AboutAdi_CorporateInformation_VerifyCorporateGovernance(string Locale)
        {
            //--- Action: Navigate to the Corporate Governance Page ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + corporateGovernance_page_url);

            //----- About ADI TestPlan > Desktop Tab > R2 > T2: Verify Corporate Governance page -----//

            //--- Expected Result: The Board of Directors link is available ---//
            test.validateElementIsPresent(driver, Elements.AboutAdi_CorporateGovernance_BoardOfDirectors_Link);

            //--- Expected Result: The Board of Committee Composition link is available ---//
            test.validateElementIsPresent(driver, Elements.AboutAdi_CorporateGovernance_BoardCommitteeComposition_Link);

            //--- Expected Result: The Corporate Officers & Executives link is available ---//
            if (Locale.Equals("cn") || Locale.Equals("jp"))
            {
                test.validateElementIsPresent(driver, Elements.AboutAdi_CorporateGovernance_CorporateOfficersAndExecutives_Link2);
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.AboutAdi_CorporateGovernance_CorporateOfficersAndExecutives_Link);
            }


            //--- Expected Result: The Governance Documents link is available ---//
            test.validateElementIsPresent(driver, Elements.AboutAdi_CorporateGovernance_GovernanceDocuments_Link);
        }

        [Test, Category("About ADI"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Left Rail is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Left Rail is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Left Rail is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Left Rail is Present and Working as Expected in RU Locale")]
        public void AboutAdi_CorporateInformation_CorporateGovernance_VerifyLeftRailNavigation(string Locale)
        {
            //--- Action: Navigate to the Corporate Governance Page ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + corporateGovernance_page_url);

            //----- About ADI TestPlan > Desktop Tab > R2 > T2: Verify Corporate Governance page -----//

            //--- Expected Result: Corporate Governance is Selected ---//
            test.validateElementIsPresent(driver, Elements.AboutAdi_LeftRailNav_Selected_CorporateGovernance_Link);         
        }
    }
}