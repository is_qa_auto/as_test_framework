﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;

namespace AS_Test_Framework.AboutAdi
{
    [TestFixture]
    public class AboutAdi_CorporateInformation_JpOfficeMap : BaseSetUp
    {
        public AboutAdi_CorporateInformation_JpOfficeMap() : base() { }

        //--- URLs ---//
        string jpOfficeMap_page_url = "/about-adi/corporate-information/jp-office-map.html";

        [Test, Category("About ADI"), Category("Core"), Retry(2)]
        [TestCase("jp", TestName = "Verify that the JP Office Map Page is Working as Expected in JP Locale")]
        public void AboutAdi_CorporateInformation_VerifyJpOfficeMap(string Locale)
        {
            //--- Action: Navigate to the JP Office Map Page ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + jpOfficeMap_page_url);

            //----- About ADI TestPlan > Desktop Tab > R2 > T3: Verify オフィス地図 in JP Locale -----//

            //--- Expected Result: The Window Title is not empty ---//
            test.validateWindowTitleIsCorrect(driver, util.GetText(driver, Elements.AboutAdi_2nd_Column_Page_Title_Txt));
        }
    }
}