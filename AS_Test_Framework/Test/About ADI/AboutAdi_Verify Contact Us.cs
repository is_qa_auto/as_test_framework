﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.AboutAdi
{
    [TestFixture]
    public class AboutAdi_ContactUs : BaseSetUp
    {
        public AboutAdi_ContactUs() : base() { }

        ////--- Login Credentials ---//
        //string username = "marvin.bebe@analog.com";
        //string password = "Test_1234";

        //--- URLs ---//
        string contactUs_page_url = "/about-adi/contact-us.html";
        string searchSupport_page_url = "/searchsupport.html";
        string documentFeedback_form_url = "/Form_Pages/feedback/documentfeedback.aspx";
        //string newsRoom_page_url = "/news-room.html";
        string login_page_url = "b2clogin";
        //string myanalog_dashboard_page_url = "/app";
        //string myanalog_subscriptions_page_url = "/app/subscriptions";

        [Test, Category("About ADI"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Left Rail is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Left Rail is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Left Rail is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Left Rail is Present and Working as Expected in RU Locale")]
        public void AboutAdi_ContactUs_VerifyLeftRail(string Locale)
        {
            //--- Action: Navigate to About ADI - Contact Us ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + contactUs_page_url);

            //----- About ADI TestPlan > Desktop Tab > R8 > T1.1: Verify Contact us page -----//

            //--- Expected Result: Contact Us  from the left rail should be highlighted. ---//
            test.validateElementIsPresent(driver, Elements.AboutAdi_LeftRailNav_Selected_ContactUs_Link);
        }

        [Test, Category("About ADI"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Corporate Contact Information Section is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Corporate Contact Information Section is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Corporate Contact Information Section is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Corporate Contact Information Section is Present and Working as Expected in RU Locale")]
        public void AboutAdi_ContactUs_VerifyCorporateContactInformationSection(string Locale)
        {
            //--- Action: Navigate to About ADI - Contact Us ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + contactUs_page_url);

            //----- About ADI TestPlan > Desktop Tab > R8 > T1.1: Verify Contact us page -----//

            //--- Expected Result: Corporate headquarters contact information should be displayed ---//
            test.validateElementIsPresent(driver, Elements.AboutAdi_ContactUs_CorporateContactInformation_Section);
        }

        [Test, Category("About ADI"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Customer Service and Technical Support Section is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Customer Service and Technical Support Section is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Customer Service and Technical Support Section is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Customer Service and Technical Support Section is Present and Working as Expected in RU Locale")]
        public void AboutAdi_ContactUs_VerifyCustomerServiceAndTechnicalSupportSection(string Locale)
        {
            //--- Action: Navigate to About ADI - Contact Us ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + contactUs_page_url);
            Thread.Sleep(2000);

            //----- About ADI TestPlan > Desktop Tab > R8 > T1.1: Verify Contact us page -----//

            //--- Action: Click on the Search within Support Button ---//
            action.IClick(driver, Elements.AboutAdi_ContactUs_SearchWithinSupport_Button);
            Thread.Sleep(1000);

            //--- Expected Result: Send users to SUPPORT SEARCH in the SUPPORT section of the website ---//
            test.validateStringInstance(driver, driver.Url, searchSupport_page_url);
        }

        [Test, Category("About ADI"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Document Feedback Section is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Document Feedback Section is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Document Feedback Section is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Document Feedback Section is Present and Working as Expected in RU Locale")]
        public void AboutAdi_ContactUs_VerifyDocumentFeedbackSection(string Locale)
        {
            //--- Action: Navigate to About ADI - Contact Us ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + contactUs_page_url);

            //----- About ADI TestPlan > Desktop Tab > R8 > T1.1: Verify Contact us page -----//

            //--- Expected Result: Document feedback module should be present in the contact us page. ---//
            test.validateElementIsPresent(driver, Elements.AboutAdi_ContactUs_SubmitDocumentFeedback_Link);

            if (util.CheckElement(driver, Elements.AboutAdi_ContactUs_SubmitDocumentFeedback_Link, 1) && (!Locale.Equals("jp") && !Configuration.Browser.Equals("Chrome_HL")))
            {
                //--- Action: Click on Submit Document Feedback ---//
                action.IOpenLinkInNewTab(driver, Elements.AboutAdi_ContactUs_SubmitDocumentFeedback_Link);

                //--- Expected Result: Page should be redirected to Document Feedback form ---//
                test.validateStringInstance(driver, driver.Url, "analog.com" + documentFeedback_form_url);
            }
        }

        [Test, Category("About ADI"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Human Resources Section is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Human Resources Section is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Human Resources Section is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Human Resources Section is Present and Working as Expected in RU Locale")]
        public void AboutAdi_ContactUs_VerifyHumanResourcesSection(string Locale)
        {
            //--- Action: Navigate to About ADI - Contact Us ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + contactUs_page_url);
            Thread.Sleep(1000);

            //----- About ADI TestPlan > Desktop Tab > R8 > T1.1: Verify Contact us page -----//

            //--- Expected Result: Human Resources module should be present in the contact us page. ---//
            if (Locale.Equals("jp"))
            {
                test.validateElementIsPresent(driver, Elements.AboutAdi_ContactUs_RecruitJapan_AnalogCom_Link);
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.AboutAdi_ContactUs_ConnectionsHrServices_AnalogCom_Link);
            }
        }

        //[Test, Category("About ADI"), Category("Core"), Retry(2)]
        //[TestCase("en", TestName = "Verify that the Media Contacts Section is Present and Working as Expected in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Media Contacts Section is Present and Working as Expected in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Media Contacts Section is Present and Working as Expected in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Media Contacts Section is Present and Working as Expected in RU Locale")]
        //public void AboutAdi_ContactUs_VerifyMediaContactsSection(string Locale)
        //{
        //    //--- Action: Navigate to About ADI - Contact Us ---//
        //    action.Navigate(driver, Configuration.Env_Url + Locale + contactUs_page_url);

        //    //----- About ADI TestPlan > Desktop Tab > R8 > T1.1: Verify Contact us page -----//

        //    //--- Expected Result: News & Media Relations link should be present ---//
        //    test.validateElementIsPresent(driver, Elements.AboutAdi_ContactUs_NewsAndMediaRelations_Link);

        //    if (util.CheckElement(driver, Elements.AboutAdi_ContactUs_NewsAndMediaRelations_Link, 1))
        //    {
        //        //--- Action: Click on News & Media Relations link ---//
        //        action.IOpenLinkInNewTab(driver, Elements.AboutAdi_ContactUs_NewsAndMediaRelations_Link);
        //        Thread.Sleep(1000);

        //        //--- Expected Result: User should be linked out to Media and Relations page ---//
        //        test.validateStringInstance(driver, driver.Url, newsRoom_page_url);
        //    }
        //}

        [Test, Category("About ADI"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Literature and Mailing List Section is Present and Working as Expected when the User is Logged Out in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Literature and Mailing List Section is Present and Working as Expected when the User is Logged Out in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Literature and Mailing List Section is Present and Working as Expected when the User is Logged Out in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Literature and Mailing List Section is Present and Working as Expected when the User is Logged Out in RU Locale")]
        public void AboutAdi_ContactUs_VerifyLiteratureAndMailingListSectionWhenUserIsLoggedOut(string Locale)
        {
            //--- Action: Navigate to About ADI - Contact Us ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + contactUs_page_url);
            Thread.Sleep(6000);

            //----- About ADI TestPlan > Desktop Tab > R8 > T1.1: Verify Contact us page -----//

            //--- Expected Result: Subscribe Link should be Present. ---//
            if (Locale.Equals("cn"))
            {
                test.validateElementIsPresent(driver, Elements.AboutAdi_ContactUs_ReadOnlineNews_Link);
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.AboutAdi_ContactUs_SubscribeToENewsletters_Link);
            }

            //--- Expected Result: Contact us Link should be Present. ---//
            test.validateElementIsPresent(driver, Elements.AboutAdi_ContactUs_ContactUs_Link);

            //--- Expected Result: distribution.literature@analog.com (distribution management email) Link should be Present. ---//
            test.validateElementIsPresent(driver, Elements.AboutAdi_ContactUs_DistributionLiterature_AnalogCom_Link);

            if (util.CheckElement(driver, Elements.AboutAdi_ContactUs_MyAnalog_Link, 1))
            {
                //----- About ADI TestPlan > Desktop Tab > R8 > T1.2: Verify myAnalog link in Literature and Mailing List section -----//

                //--- Action: Click myAnalog link when  the user is logged-out ---//
                action.IOpenLinkInNewTab(driver, Elements.AboutAdi_ContactUs_MyAnalog_Link);
                Thread.Sleep(17000);

                //--- Expected Result: The page should redirect to Log-in Page ---//
                test.validateStringInstance(driver, driver.Url, login_page_url);

                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles.First());
            }

            if (Locale.Equals("cn"))
            {
                if (util.CheckElement(driver, Elements.AboutAdi_ContactUs_ReadOnlineNews_Link, 1))
                {
                    //----- About ADI TestPlan > Desktop Tab > R8 > T1.3: Verify Subscribe to eNewsletters link in Literature and Mailing List section -----//

                    //--- Action: Click Subscribe to eNewsletters link when  the user is logged-out ---//
                    action.IOpenLinkInNewTab(driver, Elements.AboutAdi_ContactUs_ReadOnlineNews_Link);
                    Thread.Sleep(1000);

                    //--- Expected Result: The page should redirect to Log-in Page ---//
                    test.validateStringInstance(driver, driver.Url, login_page_url);
                }
            }
            else
            {
                if (util.CheckElement(driver, Elements.AboutAdi_ContactUs_SubscribeToENewsletters_Link, 1))
                {
                    //----- About ADI TestPlan > Desktop Tab > R8 > T1.3: Verify Subscribe to eNewsletters link in Literature and Mailing List section -----//

                    //--- Action: Click Subscribe to eNewsletters link when  the user is logged-out ---//
                    action.IOpenLinkInNewTab(driver, Elements.AboutAdi_ContactUs_SubscribeToENewsletters_Link);
                    Thread.Sleep(2000);

                    //--- Expected Result: The page should redirect to Log-in Page ---//
                    test.validateStringInstance(driver, driver.Url, login_page_url);
                }
            }
        }

        //NOTE: PENDING. Manual Testing to confirm if this is still valid. The Links redirects to the Login Page.
        //[Test, Category("About ADI"), Category("Core"), Retry(2)]
        //[TestCase("en", TestName = "Verify that the Literature and Mailing List Section is Present and Working as Expected when the User is Logged In in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Literature and Mailing List Section is Present and Working as Expected when the User is Logged In in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Literature and Mailing List Section is Present and Working as Expected when the User is Logged In in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Literature and Mailing List Section is Present and Working as Expected when the User is Logged In in RU Locale")]
        //public void AboutAdi_ContactUs_VerifyLiteratureAndMailingListSectionWhenUserIsLoggedIn(string Locale)
        //{
        //    //--- Action: Navigate to About ADI - Contact Us ---//
        //    action.Navigate(driver, Configuration.Env_Url + Locale + contactUs_page_url);

        //    //--- Action: Login via myAnalog Widget ---//
        //    action.ILogInViaMyAnalogWidget(driver, username, password);

        //    //--- Action: Login via MYANALOG MEGA MENU ---//
        //    action.ILoginViaMyAnalogMegaMenu(driver, username, password);

        //    if (util.CheckElement(driver, Elements.AboutAdi_ContactUs_MyAnalog_Link, 1))
        //    {
        //        //----- About ADI TestPlan > Desktop Tab > R8 > T1.2: Verify myAnalog link in Literature and Mailing List section -----//

        //        //--- Action: Click myAnalog link when  the user is logged-in ---//
        //        action.IOpenLinkInNewTab(driver, Elements.AboutAdi_ContactUs_MyAnalog_Link);
        //        //action.IClick(driver, Elements.AboutAdi_ContactUs_MyAnalog_Link);

        //        //--- Expected Result: The page should redirect to myAnalog page ---//
        //        test.validateStringInstance(driver, driver.Url, myanalog_dashboard_page_url);

        //        driver.Close();
        //        driver.SwitchTo().Window(driver.WindowHandles.First());
        //    }

        //    if (Locale.Equals("cn"))
        //    {
        //        if (util.CheckElement(driver, Elements.AboutAdi_ContactUs_ReadOnlineNews_Link, 1))
        //        {
        //            //----- About ADI TestPlan > Desktop Tab > R8 > T1.3: Verify Subscribe to eNewsletters link in Literature and Mailing List section -----//

        //            //--- Action: Click Subscribe to eNewsletters link when  the user is logged-in ---//
        //            action.IOpenLinkInNewTab(driver, Elements.AboutAdi_ContactUs_ReadOnlineNews_Link);

        //            //--- Expected Result: The page should redirect to  Manage Newsletter page ---//
        //            test.validateStringInstance(driver, driver.Url, myanalog_subscriptions_page_url);
        //        }
        //    }
        //    else
        //    {
        //        if (util.CheckElement(driver, Elements.AboutAdi_ContactUs_SubscribeToENewsletters_Link, 1))
        //        {
        //            //----- About ADI TestPlan > Desktop Tab > R8 > T1.3: Verify Subscribe to eNewsletters link in Literature and Mailing List section -----//

        //            //--- Action: Click Subscribe to eNewsletters link when  the user is logged-in ---//
        //            action.IOpenLinkInNewTab(driver, Elements.AboutAdi_ContactUs_SubscribeToENewsletters_Link);

        //            //--- Expected Result: The page should redirect to  Manage Newsletter page ---//
        //            test.validateStringInstance(driver, driver.Url, myanalog_subscriptions_page_url);
        //        }
        //    }
        //}
    }
}