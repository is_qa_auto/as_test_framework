﻿using AS_Test_Framework.Util;
using NUnit.Framework;
using System;

namespace AS_Test_Framework.AboutAdi
{
    [TestFixture]
    public class AboutAdi_NewsRoom_PressReleases_OldListingPages : BaseSetUp
    {
        public AboutAdi_NewsRoom_PressReleases_OldListingPages() : base() { }

        //--- URLs ---//
        string pressReleases_page_url = "/about-adi/news-room/press-releases.html";

        [Test, Category("About ADI"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Old Listing Pages is Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Old Listing Pages is Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Old Listing Pages is Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Old Listing Pages is Working as Expected in RU Locale")]
        public void AboutAdi_NewsRoom_PressReleases_VerifyOldListingPages(string Locale)
        {
            //----- About ADI TestPlan > Desktop Tab > R3 > T8: Verify the old listing pages for press releases (AL-10580) -----//

            //--- Action: Open old press release listing page with 3 years before current year ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + "/about-adi/news-room/press-releases/" + (Int32.Parse(DateTime.Now.ToString("yyyy")) - 3) + "-listing.html");

            //--- Expected Result: Old press release listing pages with 3 years before current year must be redirected to press release landing page ---//
            test.validateStringInstance(driver, driver.Url, Configuration.Env_Url + Locale + pressReleases_page_url);
        }

        [Test, Category("About ADI"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Advanced Listing Pages is Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Advanced Listing Pages is Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Advanced Listing Pages is Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Advanced Listing Pages is Working as Expected in RU Locale")]
        public void AboutAdi_NewsRoom_PressReleases_VerifyAdvancedListingPages(string Locale)
        {
            //----- About ADI TestPlan > Desktop Tab > R3 > T8: Verify the old listing pages for press releases (AL-10580) -----//

            //--- Action: Open press release listing page with year greater than current year ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + "/about-adi/news-room/press-releases/" + (Int32.Parse(DateTime.Now.ToString("yyyy")) + 1) + "-listing.html");

            //--- Expected Result: Press release listing pages with year greater than current year must be redirected to press release landing page ---//
            test.validateStringInstance(driver, driver.Url, Configuration.Env_Url + Locale + pressReleases_page_url);
        }
    }
}