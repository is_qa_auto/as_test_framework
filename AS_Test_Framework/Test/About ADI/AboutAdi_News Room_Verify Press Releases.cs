﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Threading;

namespace AS_Test_Framework.AboutAdi
{
    [TestFixture]
    public class AboutAdi_NewsRoom_PressReleases : BaseSetUp
    {
        public AboutAdi_NewsRoom_PressReleases() : base() { }

        //--- URLs ---//
        string pressReleases_page_url = "/about-adi/news-room/press-releases.html";
        string searchResults_page_url = "/search.html";
        string pressReleases_article_url_format = "/press-releases/";

        //--- Labels ---//
        string pressReleases_txt = "Press Releases";

        [Test, Category("About ADI"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Left-rail is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Left-rail is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Left-rail is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Left-rail is Present and Working as Expected in RU Locale")]
        public void AboutAdi_NewsRoom_PressReleases_VerifyLeftRail(string Locale)
        {
            //--- Action: Navigate to the Press Releases Page ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + pressReleases_page_url);

            //----- About ADI TestPlan > Desktop Tab > R3 > T7_1: Validate that the Press Releases page -----//

            //--- Expected Result: The News Room will be highlighted. ---//
            test.validateElementIsPresent(driver, Elements.AboutAdi_LeftRailNav_Selected_NewsRoom_Link);

            //--- Expected Result: The Press Releases will be in bold. ---//
            test.validateElementIsPresent(driver, Elements.AboutAdi_LeftRailNav_Selected_PressReleases_Link);
        }

        [Test, Category("About ADI"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the 2nd Column is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the 2nd Column is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the 2nd Column is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the 2nd Column is Present and Working as Expected in RU Locale")]
        public void AboutAdi_NewsRoom_PressReleases_Verify2ndColumn(string Locale)
        {
            //--- Action: Navigate to the Press Releases Page ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + pressReleases_page_url);

            //----- About ADI TestPlan > Desktop Tab > R3 > T7_1: Validate that the Press Releases page -----//

            //--- Expected Result: The page is displayed properly with the Page Title ---//
            test.validateElementIsPresent(driver, Elements.AboutAdi_2nd_Column_Page_Title_Txt);

            //--- Expected Result: The page is displayed properly with the Year Filters ---//
            test.validateElementIsPresent(driver, Elements.AboutAdi_PressReleases_YearFilter_Links);

            if (util.CheckElement(driver, Elements.AboutAdi_PressReleases_YearFilter_Links, 1) && !Configuration.Browser.Equals("Chrome_HL"))
            {
                //--- Action: Click any of the Year Filter options ---//
                action.IClick(driver, Elements.AboutAdi_PressReleases_YearFilter_Links);
                Thread.Sleep(3000);

                //--- Expected Result: The selected filter will be in bold and bigger font-size. ---//
                test.validateElementIsPresent(driver, Elements.AboutAdi_PressReleases_Selected_YearFilter_Link);
            }

            if (util.CheckElement(driver, Elements.AboutAdi_PressReleases_PressReleases_Article_Title_Links, 1) && !Configuration.Browser.Equals("Chrome_HL"))
            {
                //--- Action: Click on the press release urls randomly. ---//
                action.IOpenLinkInNewTab(driver, Elements.AboutAdi_PressReleases_PressReleases_Article_Title_Links);

                //--- Expected Result: Press release article is displayed ---//
                test.validateStringInstance(driver, driver.Url, pressReleases_article_url_format);
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.AboutAdi_PressReleases_PressReleases_Article_Title_Links);
            }
        }

        [Test, Category("About ADI"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Content Column is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Content Column is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Content Column is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Content Column is Present and Working as Expected in RU Locale")]
        public void AboutAdi_NewsRoom_PressReleases_VerifyContentColumn(string Locale)
        {
            //--- Action: Navigate to the Press Releases Page ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + pressReleases_page_url);

            //----- About ADI TestPlan > Desktop Tab > R3 > T7_1: Validate that the Press Releases page -----//

            //--- Expected Result: The page is displayed properly with the Selected Year Filter Title ---//
            test.validateElementIsPresent(driver, Elements.AboutAdi_PressReleases_Selected_Year_Title_Label);

            if (util.CheckElement(driver, Elements.AboutAdi_PressReleases_YearFilter_Links, 1) && !Configuration.Browser.Equals("Chrome_HL"))
            {
                //--- Action: Click any of the Year Filter options ---//
                action.IClick(driver, Elements.AboutAdi_PressReleases_YearFilter_Links);
                Thread.Sleep(3000);
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.AboutAdi_PressReleases_YearFilter_Links);
            }

            //--- Expected Result: Displays Date of the Article ---//
            test.validateElementIsPresent(driver, Elements.AboutAdi_PressReleases_PressReleases_Article_Date_Labels);

            if (util.CheckElement(driver, Elements.AboutAdi_PressReleases_PressReleases_Article_Date_Labels, 1))
            {
                //--- Expected Result: Press Release list will be filtered corresponding to the selected filter. ---//
                test.validateStringInstance(driver, util.GetText(driver, Elements.AboutAdi_PressReleases_PressReleases_Article_Date_Labels), util.ExtractNumber(driver, Elements.AboutAdi_PressReleases_Selected_YearFilter_Link));
            }

            //--- Expected Result: Displays Title of the Article ---//
            test.validateElementIsPresent(driver, Elements.AboutAdi_PressReleases_PressReleases_Article_Title_Links);
        }

        [Test, Category("About ADI"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Search within Press Releases is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Search within Press Releases is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Search within Press Releases is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Search within Press Releases is Present and Working as Expected in RU Locale")]
        public void AboutAdi_NewsRoom_PressReleases_VerifySearchWithinPressReleases(string Locale)
        {
            //--- Action: Navigate to the Press Releases Page ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + pressReleases_page_url);
            Thread.Sleep(9000);

            //----- About ADI TestPlan > Desktop Tab > R3 > T7_1: Validate that the Press Releases page -----//

            //--- Expected Result: Search section should be displayed ---//
            test.validateElementIsPresent(driver, Elements.AboutAdi_PressReleases_SearchWithinPressReleases_InputBox);

            if (util.CheckElement(driver, Elements.AboutAdi_PressReleases_SearchWithinPressReleases_InputBox, 1))
            {
                //--- Expected Result: The ghost text "Search within Press Releases" should be displayed. ---//
                string ghost_text = "Search within Press Releases";
                if (Locale.Equals("en"))
                {
                    test.validateString(driver, ghost_text, util.ReturnAttribute(driver, Elements.AboutAdi_PressReleases_SearchWithinPressReleases_InputBox, "placeholder"));
                }

                //--- Action: Enter a value in the Search Text box --//
                string search_input = "*";
                action.IDeleteValueOnFields(driver, Elements.AboutAdi_PressReleases_SearchWithinPressReleases_InputBox);
                action.IType(driver, Elements.AboutAdi_PressReleases_SearchWithinPressReleases_InputBox, search_input);
                Thread.Sleep(5000);

                //--- Action: Click the Search icon or Enter key ---//
                action.IClick(driver, Elements.AboutAdi_PressReleases_SearchWithinPressReleases_Button);
                Thread.Sleep(1000);

                //--- Expected Result: The Search results page will be opened. ---//
                test.validateStringInstance(driver, driver.Url, searchResults_page_url);

                if (driver.Url.Contains(searchResults_page_url))
                {
                    if (Locale.Equals("en"))
                    {
                        //--- Expected Result: Press Release filter under Resources will be selected. ---//
                        test.validateStringIsCorrect(driver, Elements.GlobalSearchResults_Resources_Selected_Filter_Lbl, pressReleases_txt);
                    }
                    else
                    {
                        test.validateElementIsPresent(driver, Elements.GlobalSearchResults_Resources_Selected_Filter_Lbl);
                    }
                }
            }
        }
    }
}