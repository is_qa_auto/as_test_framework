﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.AboutAdi
{
    [TestFixture]
    public class AboutAdi_CorporateInformation : BaseSetUp
    {
        public AboutAdi_CorporateInformation() : base() { }

        //--- URLs ---//
        string corporateInformation_page_url = "/about-adi/corporate-information.html";
        string regionalHeadquarters_page_url = "/regional-headquarters.html";

        [Test, Category("About ADI"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Left Rail is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Left Rail is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Left Rail is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Left Rail is Present and Working as Expected in RU Locale")]
        public void AboutAdi_CorporateInformation_VerifyLeftRail(string Locale)
        {
            //--- Action: Navigate to About ADI - Corporate Information Page ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + corporateInformation_page_url);

            //----- About ADI TestPlan > Desktop Tab > R2 > T1: Verify Corporate Information page -----//

            //--- Expected Result: Corporate information  category from the left rail should be highlighted ---//
            test.validateElementIsPresent(driver, Elements.AboutAdi_LeftRailNav_Selected_CorporateInformation_Link);

            //--- Expected Result: Corporate information subcategory(if there is) from the left rail should be highlighted ---//
            test.validateElementIsPresent(driver, Elements.AboutAdi_LeftRailNav_Selected_Category_SubCategory_Links);

            if (util.CheckElement(driver, Elements.AboutAdi_LeftRailNav_Selected_Category_SubCategory_Links, 2))
            {
                //--- Expected Result: The Corporate Governance link should be displayed ---//
                test.validateElementIsPresent(driver, Elements.AboutAdi_LeftRailNav_CorporateGovernance_Link);

                //--- Expected Result: The Regional Headquarters link should be displayed ---//
                test.validateElementIsPresent(driver, Elements.AboutAdi_LeftRailNav_RegionalHeadquarters_Link);

                if (util.CheckElement(driver, Elements.AboutAdi_LeftRailNav_RegionalHeadquarters_Link, 1))
                {
                    //----- About ADI TestPlan > Desktop Tab > R2 > T4: Verify Regional Headquarters -----//

                    //--- Action: Click on Regional Headquarters link ---//
                    action.IOpenLinkInNewTab(driver, Elements.AboutAdi_LeftRailNav_RegionalHeadquarters_Link);
                    Thread.Sleep(5000);

                    //--- Expected Result: Regional Headquarters page should be displayed ---//
                    test.validateStringInstance(driver, driver.Url, regionalHeadquarters_page_url);

                    driver.Close();
                    driver.SwitchTo().Window(driver.WindowHandles.First());
                }

                //--- Expected Result: The Sales & Distribution link should be displayed ---//
                test.validateElementIsPresent(driver, Elements.AboutAdi_LeftRailNav_SalesAndDistribution_Link);
            }
        }

        [Test, Category("About ADI"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Corporate Information Page is Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Corporate Information Page is Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Corporate Information Page is Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Corporate Information Page is Working as Expected in RU Locale")]
        public void AboutAdi_VerifyCorporateInformationPage(string Locale)
        {
            //--- Action: Navigate to About ADI - Corporate Information Page ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + corporateInformation_page_url);

            //----- About ADI TestPlan > Desktop Tab > R1 > T6: Verify all the links from left rails navigation -----//

            //--- Expected Result: Page title should be displayed ---//
            test.validateElementIsPresent(driver, Elements.AboutAdi_2nd_Column_Page_Title_Txt);
        }
    }
}