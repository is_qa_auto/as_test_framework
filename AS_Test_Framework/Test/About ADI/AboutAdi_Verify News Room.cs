﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.AboutAdi
{
    [TestFixture]
    public class AboutAdi_NewsRoom : BaseSetUp
    {
        public AboutAdi_NewsRoom() : base() { }

        //--- URLs ---//
        string newsRoom_page_url = "/about-adi/news-room.html";
        string awards_page_url = "/external-recognition.html";
        string multimediaGallery_page_url = "/multimedia-gallery.html";
        string pressReleases_page_url = "/press-releases.html";

        [Test, Category("About ADI"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Left Navigation Panel is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Left Navigation Panel is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Left Navigation Panel is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Left Navigation Panel is Present and Working as Expected in RU Locale")]
        public void AboutAdi_NewsRoom_VerifyLeftNavigationPanel(string Locale)
        {
            //--- Action: Navigate to About ADI - News Room Page ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + newsRoom_page_url);

            //----- About ADI TestPlan > Desktop Tab > R3 > T4: Validate that left navigation panel is accessible -----//

            if (util.CheckElement(driver, Elements.AboutAdi_LeftRailNav_PressReleases_Link, 1))
            {
                //--- Action: Click Press releases link ---//
                action.IOpenLinkInNewTab(driver, Elements.AboutAdi_LeftRailNav_PressReleases_Link);
                Thread.Sleep(3000);

                //--- Expected Result: Press Release page is displayed ---//
                test.validateStringInstance(driver, driver.Url, pressReleases_page_url);

                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles.First());
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.AboutAdi_LeftRailNav_PressReleases_Link);
            }

            if (util.CheckElement(driver, Elements.AboutAdi_LeftRailNav_MultimediaGallery_Link, 1))
            {
                //--- Action: Click Multimedia Gallery link ---//
                action.IOpenLinkInNewTab(driver, Elements.AboutAdi_LeftRailNav_MultimediaGallery_Link);
                Thread.Sleep(4000);

                //--- Expected Result: Multimedia Gallery page is displayed ---//
                test.validateStringInstance(driver, driver.Url, multimediaGallery_page_url);

                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles.First());
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.AboutAdi_LeftRailNav_MultimediaGallery_Link);
            }

            if (util.CheckElement(driver, Elements.AboutAdi_LeftRailNav_ExternalRecognition_Link, 1))
            {
                //--- Action: Click External Recognition link ---//
                action.IOpenLinkInNewTab(driver, Elements.AboutAdi_LeftRailNav_ExternalRecognition_Link);
                Thread.Sleep(3000);

                //--- Expected Result: Awards page is displayed ---//
                test.validateStringInstance(driver, driver.Url, awards_page_url);
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.AboutAdi_LeftRailNav_ExternalRecognition_Link);
            }
        }

        [Test, Category("About ADI"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the News Room Page is Working as Expected in EN Locale", Category = "Smoke_ADIWeb")]
        [TestCase("cn", TestName = "Verify that the News Room Page is Working as Expected in CN Locale", Category = "Smoke_ADIWeb")]
        [TestCase("jp", TestName = "Verify that the News Room Page is Working as Expected in JP Locale", Category = "Smoke_ADIWeb")]
        [TestCase("ru", TestName = "Verify that the News Room Page is Working as Expected in RU Locale")]
        public void AboutAdi_VerifyNewsRoomPage(string Locale)
        {

            string initial_steps = "1. Go to " + Configuration.Env_Url + Locale + newsRoom_page_url;
            string scenario;

            //--- Action: Navigate to About ADI - News Room Page ---//
            action.INavigate(driver, Configuration.Env_Url + Locale + newsRoom_page_url);
            Thread.Sleep(3000);

            //----- About ADI TestPlan > Desktop Tab > R3 > T1: Validate new News Room page -----//

            //----- PRESS RELEASE -----//

            //--- Expected Result: Displays title ---//
            scenario = "Verify that Press Releases section title is present in News Room page";
            test.validateElementIsPresentv2(driver, Elements.AboutAdi_NewsRoom_PressReleases_Section_Title_Label, scenario, initial_steps);

            //--- Expected Result: Displays Date of the article ---//
            scenario = "Verify that date is present in every press release article";
            test.validateElementIsPresentv2(driver, Elements.AboutAdi_NewsRoom_PressReleases_Section_Article_Date_Labels, scenario, initial_steps);

            //--- Expected Result: Displays Title of the Article ---//
            scenario = "Verify that title is present in every press release article";
            test.validateElementIsPresentv2(driver, Elements.AboutAdi_NewsRoom_PressReleases_Section_Article_Title_Labels, scenario, initial_steps);

            ////NOTE: PENDING. Manual Testing Team to confirm if this is applicable for all Locales.
            ////----- ADI IN THE NEWS -----//

            ////--- Expected Result: Displays title ---//
            //test.validateElementIsPresent(driver, Elements.AboutAdi_NewsRoom_AdiInTheNews_Section_Title_Label);

            ////--- Expected Result: Displays Date of the article ---//
            ////--- Expected Result: Displays Name of where the news came from ---//
            //test.validateElementIsPresent(driver, Elements.AboutAdi_NewsRoom_AdiInTheNews_Section_Article_Date_Labels);

            ////--- Expected Result: Displays Title of the News ---//
            //test.validateElementIsPresent(driver, Elements.AboutAdi_NewsRoom_AdiInTheNews_Section_Article_Title_Labels);

            ////--- Expected Result: Displays a link to read the full article. "Read More" ---//
            //test.validateElementIsPresent(driver, Elements.AboutAdi_NewsRoom_AdiInTheNews_Section_ReadMore_Links);

            //----- MEDIA CONTACTS -----//

            //--- Expected Result: Displays title ---//
            scenario = "Verify that media contacts section title is present in News Room page";
            test.validateElementIsPresentv2(driver, Elements.AboutAdi_NewsRoom_MediaContacts_Section_Title_Label, scenario, initial_steps);

            //--- Expected Result: Displays Contacts for members of media only ---//
            scenario = "Verify that media contacts content is present";
            test.validateElementIsPresentv2(driver, Elements.AboutAdi_NewsRoom_MediaContacts_Section_Content, scenario, initial_steps);

            //----- LEADERSHIP -----//
            //--- Expected Result: Displays title ---//
            scenario = "Verify that Leadership section title is present in News Room page";
            test.validateElementIsPresentv2(driver, Elements.AboutAdi_NewsRoom_Leadership_Section_Title_Label, scenario, initial_steps);
            
            //----- STAY INFORMED -----//
            //--- Expected Result: Displays title ---//
            scenario = "Verify that Stay Informed section title is present in News Room page";
            test.validateElementIsPresentv2(driver, Elements.AboutAdi_NewsRoom_StayInformed_Section_Title_Label, scenario, initial_steps);

            //--- Expected Result: Displays context information ---//
            scenario = "Verify that Stay Informed content is present";
            test.validateElementIsPresentv2(driver, Elements.AboutAdi_NewsRoom_StayInformed_Section_Content, scenario, initial_steps);

            ////NOTE: PENDING. Manual Testing Team to confirm if this is applicable for all Locales.
            ////--- Expected Result: Displays MyAnalog.com link ---//
            //test.validateElementIsPresent(driver, Elements.AboutAdi_NewsRoom_StayInformed_Section_myAnalogCom_Link);

            ////NOTE: PENDING. Manual Testing Team to confirm if this is applicable for all Locales.
            ////--- Expected Result: Displays Subscribe link to eNewsletters ---//
            //test.validateElementIsPresent(driver, Elements.AboutAdi_NewsRoom_StayInformed_Section_Subscribe_Link);
        }
    }
}