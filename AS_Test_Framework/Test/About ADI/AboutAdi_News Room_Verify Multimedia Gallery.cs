﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Threading;

namespace AS_Test_Framework.AboutAdi
{
    [TestFixture]
    public class AboutAdi_NewsRoom_MultimediaGallery : BaseSetUp
    {
        public AboutAdi_NewsRoom_MultimediaGallery() : base() { }

        //--- URLs ---//
        string multimedieGallery_page_url = "/about-adi/news-room/multimedia-gallery.html";

        [Test, Category("About ADI"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Multimedia Gallery Page is Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Multimedia Gallery Page is Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Multimedia Gallery Page is Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Multimedia Gallery Page is Working as Expected in RU Locale")]
        public void AboutAdi_NewsRoom_VerifyMultimediaGallery(string Locale)
        {
            //--- Action: Navigate to the Multimedia Gallery ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + multimedieGallery_page_url);

            //----- About ADI TestPlan > Desktop Tab > R3 > T6_2: Validate thatMultimeda Gallery Page is accessible -----//

            //--- Expected Result: Title is displayed ---//
            test.validateElementIsPresent(driver, Elements.AboutAdi_Page_Title_Txt);
        }

        [Test, Category("About ADI"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Graphics Section is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Graphics Section is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Graphics Section is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Graphics Section is Present and Working as Expected in RU Locale")]
        public void AboutAdi_NewsRoom_MultimediaGallery_VerifyGraphicsSection(string Locale)
        {
            //--- Action: Navigate to the Multimedia Gallery ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + multimedieGallery_page_url);
            Thread.Sleep(1000);

            //----- About ADI TestPlan > Desktop Tab > R3 > T6_2: Validate thatMultimeda Gallery Page is accessible -----//

            //--- Expected Result: Graphics section is displayed ---//
            test.validateElementIsPresent(driver, Elements.AboutAdi_MultimediaGallery_Graphics_Section);

            if (util.CheckElement(driver, Elements.AboutAdi_MultimediaGallery_Graphics_Section, 1))
            {
                //----- About ADI TestPlan > Desktop Tab > R3 > T6_1: Validate Multi-Media Gallery page -----//

                //--- Expected Result: Displays title ---//
                test.validateElementIsPresent(driver, Elements.AboutAdi_MultimediaGallery_Graphics_Section_Title_Label);

                //--- Expected Result: Thumbnail Images are shown ---//
                test.validateElementIsPresent(driver, Elements.AboutAdi_MultimediaGallery_Graphics_Section_Item_Thumbnail_Images);

                //--- Expected Result: There's a Download button ---//
                test.validateElementIsPresent(driver, Elements.AboutAdi_MultimediaGallery_Graphics_Section_Item_Download_Buttons);

                if (util.CheckElement(driver, Elements.AboutAdi_MultimediaGallery_Graphics_Section_Item_ShowMore_Button, 1))
                {
                    //--- Action: Click Show More ---//
                    action.IClick(driver, Elements.AboutAdi_MultimediaGallery_Graphics_Section_Item_ShowMore_Button);

                    //--- Expected Result: Section will display remaining items ---//
                    test.validateElementIsPresent(driver, Elements.AboutAdi_MultimediaGallery_Expanded_Graphics_Section_Items);

                    //--- Expected Result: Show More will be changed to Show Less ---//
                    test.validateElementIsPresent(driver, Elements.AboutAdi_MultimediaGallery_Graphics_Section_Item_ShowLess_Button);

                    if (util.CheckElement(driver, Elements.AboutAdi_MultimediaGallery_Graphics_Section_Item_ShowLess_Button, 1))
                    {
                        //--- Action: Click Show Less ---//
                        action.IClick(driver, Elements.AboutAdi_MultimediaGallery_Graphics_Section_Item_ShowLess_Button);

                        //--- Expected Result: Section will retract the remaining items and just show 3 ---//
                        test.validateElementIsNotPresent(driver, Elements.AboutAdi_MultimediaGallery_Expanded_Graphics_Section_Items);

                        //--- Expected Result: The Show More option shown again ---//
                        test.validateElementIsPresent(driver, Elements.AboutAdi_MultimediaGallery_Graphics_Section_Item_ShowMore_Button);
                    }
                }

                if (util.CheckElement(driver, Elements.AboutAdi_MultimediaGallery_Graphics_Section_Item_Thumbnail_Images, 1))
                {
                    //--- Action: Click on an item ---//
                    action.IClick(driver, Elements.AboutAdi_MultimediaGallery_Graphics_Section_Item_Thumbnail_Images);

                    //--- Expected Result: A pop out  box will open ---//
                    test.validateElementIsPresent(driver, Elements.AboutAdi_MultimediaGallery_PopOut_Box);

                    if (util.CheckElement(driver, Elements.AboutAdi_MultimediaGallery_Graphics_Section_Item_Thumbnail_Images, 1))
                    {
                        //--- Expected Result: There will be a Download button. ---//
                        test.validateElementIsPresent(driver, Elements.AboutAdi_MultimediaGallery_PopOut_Box_Download_Button);

                        //--- Expected Result: There will be a Print button. ---//
                        test.validateElementIsPresent(driver, Elements.AboutAdi_MultimediaGallery_PopOut_Box_Print_Button);

                        //--- Expected Result: There will be an X button. ---//
                        test.validateElementIsPresent(driver, Elements.AboutAdi_MultimediaGallery_PopOut_Box_X_Button);

                        if (util.CheckElement(driver, Elements.AboutAdi_MultimediaGallery_PopOut_Box_X_Button, 1))
                        {
                            //--- Action: Click the X button ---//
                            action.IClick(driver, Elements.AboutAdi_MultimediaGallery_PopOut_Box_X_Button);

                            //--- Expected Result: The Light box will be closed. ---//
                            test.validateElementIsNotPresent(driver, Elements.AboutAdi_MultimediaGallery_PopOut_Box);
                        }
                    }
                }
            }
        }

        [Test, Category("About ADI"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Logos Section is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Logos Section is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Logos Section is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Logos Section is Present and Working as Expected in RU Locale")]
        public void AboutAdi_NewsRoom_MultimediaGallery_VerifyLogosSection(string Locale)
        {
            //--- Action: Navigate to the Multimedia Gallery ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + multimedieGallery_page_url);

            //-----About ADI TestPlan > Desktop Tab > R3 > T6_2: Validate thatMultimeda Gallery Page is accessible -----//

            //--- Expected Result: Logos section is displayed ---//
            test.validateElementIsPresent(driver, Elements.AboutAdi_MultimediaGallery_Logos_Section);

            if (util.CheckElement(driver, Elements.AboutAdi_MultimediaGallery_Logos_Section, 1))
            {
                //----- About ADI TestPlan > Desktop Tab > R3 > T6_1: Validate Multi-Media Gallery page -----//

                //--- Expected Result: Displays title ---//
                test.validateElementIsPresent(driver, Elements.AboutAdi_MultimediaGallery_Logos_Section_Title_Label);

                //--- Expected Result: Thumbnail Images are shown ---//
                test.validateElementIsPresent(driver, Elements.AboutAdi_MultimediaGallery_Logos_Section_Item_Thumbnail_Images);

                //--- Expected Result: There's a Download button ---//
                test.validateElementIsPresent(driver, Elements.AboutAdi_MultimediaGallery_Logos_Section_Item_Download_Buttons);

                if (util.CheckElement(driver, Elements.AboutAdi_MultimediaGallery_Logos_Section_Item_ShowMore_Button, 1))
                {
                    //--- Action: Click Show More ---//
                    action.IClick(driver, Elements.AboutAdi_MultimediaGallery_Logos_Section_Item_ShowMore_Button);

                    //--- Expected Result: Section will display remaining items ---//
                    test.validateElementIsPresent(driver, Elements.AboutAdi_MultimediaGallery_Expanded_Logos_Section_Items);

                    //--- Expected Result: Show More will be changed to Show Less ---//
                    test.validateElementIsPresent(driver, Elements.AboutAdi_MultimediaGallery_Logos_Section_Item_ShowLess_Button);

                    if (util.CheckElement(driver, Elements.AboutAdi_MultimediaGallery_Logos_Section_Item_ShowLess_Button, 1))
                    {
                        //--- Action: Click Show Less ---//
                        action.IClick(driver, Elements.AboutAdi_MultimediaGallery_Logos_Section_Item_ShowLess_Button);

                        //--- Expected Result: Section will retract the remaining items and just show 3 ---//
                        test.validateElementIsPresent(driver, Elements.AboutAdi_MultimediaGallery_Expanded_Logos_Section_Items);

                        //--- Expected Result: The Show More option shown again ---//
                        test.validateElementIsPresent(driver, Elements.AboutAdi_MultimediaGallery_Logos_Section_Item_ShowMore_Button);
                    }
                }

                if (util.CheckElement(driver, Elements.AboutAdi_MultimediaGallery_Logos_Section_Item_Thumbnail_Images, 1))
                {
                    //--- Action: Click on an item ---//
                    action.IClick(driver, Elements.AboutAdi_MultimediaGallery_Logos_Section_Item_Thumbnail_Images);

                    //--- Expected Result: A pop out  box will open ---//
                    test.validateElementIsPresent(driver, Elements.AboutAdi_MultimediaGallery_PopOut_Box);

                    if (util.CheckElement(driver, Elements.AboutAdi_MultimediaGallery_Logos_Section_Item_Thumbnail_Images, 1))
                    {
                        //--- Expected Result: There will be a Download button. ---//
                        test.validateElementIsPresent(driver, Elements.AboutAdi_MultimediaGallery_PopOut_Box_Download_Button);

                        //--- Expected Result: There will be a Print button. ---//
                        test.validateElementIsPresent(driver, Elements.AboutAdi_MultimediaGallery_PopOut_Box_Print_Button);

                        //--- Expected Result: There will be an X button. ---//
                        test.validateElementIsPresent(driver, Elements.AboutAdi_MultimediaGallery_PopOut_Box_X_Button);

                        if (util.CheckElement(driver, Elements.AboutAdi_MultimediaGallery_PopOut_Box_X_Button, 1))
                        {
                            //--- Action: Click the X button ---//
                            action.IClick(driver, Elements.AboutAdi_MultimediaGallery_PopOut_Box_X_Button);

                            //--- Expected Result: The Light box will be closed. ---//
                            test.validateElementIsNotPresent(driver, Elements.AboutAdi_MultimediaGallery_PopOut_Box);
                        }
                    }
                }
            }
        }
    }
}