﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.AboutAdi
{
    [TestFixture]
    public class AboutAdi_CorporateInformation_SalesAndDistribution : BaseSetUp
    {
        public AboutAdi_CorporateInformation_SalesAndDistribution() : base() { }

        //--- URLs ---//
        string salesAndDistribution_page_url = "/about-adi/corporate-information/sales-distribution.html";
        string viewExportClassifications_page_url = "/view-export-classification.html";

        [Test, Category("About ADI"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Sales and Distribution Page is Working as Expected in EN Locale")]
        [TestCase("ru", TestName = "Verify that the Sales and Distribution Page is Working as Expected in RU Locale")]
        public void AboutAdi_CorporateInformation_VerifySalesAndDistribution(string Locale)
        {
            //--- Action: Navigate to the Sales and Distribution Page ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + salesAndDistribution_page_url);

            //----- About ADI TestPlan > Desktop Tab > R2 > T6: Verify the changes to disti page map for China and Asia Pacific. -----//

            //--- Expected Result: New map should displayed ---//
            test.validateElementIsPresent(driver, Elements.AboutAdi_SalesAndDistribution_Map_Img);

            //--- Expected Result: "China" should be separated from "Asia" and is displayed on the map along with other regions. ---//
            test.validateElementIsPresent(driver, Elements.AboutAdi_SalesAndDistribution_Map_China_Region);

            //--- Expected Result: "Asia" region should renamed as "Asia Pacific" ---//
            test.validateElementIsPresent(driver, Elements.AboutAdi_SalesAndDistribution_Map_AsiaPacific_Region);

            //----- About ADI TestPlan > Desktop Tab > R2 > T5_2: Verify Sales and Distribution (AL-7502) -----//

            //--- Action: Click on the regions ---//
            action.IClick(driver, Elements.AboutAdi_SalesAndDistribution_Map_Regions);

            if (util.CheckElement(driver, Elements.AboutAdi_SalesAndDistribution_Map_Regions, 1))
            {
                //--- Expected Result: Region should expand ---//
                test.validateElementIsPresent(driver, Elements.AboutAdi_SalesAndDistribution_Map_Expanded_Regions);

                if (util.CheckElement(driver, Elements.AboutAdi_SalesAndDistribution_Map_Expanded_Regions, 1))
                {
                    //----- About ADI TestPlan > Desktop Tab > R2 > T6: Verify the changes to disti page map for China and Asia Pacific. -----//

                    //--- Action: Select any country/ies from any regions available on the map. ---//
                    action.IClick(driver, Elements.AboutAdi_SalesAndDistribution_Map_Expanded_Region_Country_Links);

                    //--- Expected Result: Sales Distribution details should get displayed on the page for the selected country. ---//
                    test.validateElementIsPresent(driver, Elements.AboutAdi_SalesAndDistribution_Region_Info);
                }
            }
        }

        [Test, Category("About ADI"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Links are Present and Working as Expected in EN Locale")]
        [TestCase("ru", TestName = "Verify that the Links are Present and Working as Expected in RU Locale")]
        public void AboutAdi_CorporateInformation_SalesAndDistribution_VerifyLinks(string Locale)
        {
            //--- Action: Navigate to the Sales and Distribution Page ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + salesAndDistribution_page_url);

            //----- About ADI TestPlan > Desktop Tab > R2 > T5_1: Verify Sales and Distribution -----//

            if (util.CheckElement(driver, Elements.AboutAdi_SalesAndDistribution_AdiExportAndImportClassificationInformation_Link, 1))
            {
                //--- Action: Click on ADI Export and Import Classification Information ---//
                action.IOpenLinkInNewTab(driver, Elements.AboutAdi_SalesAndDistribution_AdiExportAndImportClassificationInformation_Link);
                Thread.Sleep(2000);

                //--- Expected Result: User should be redirected to export classification page ---//
                test.validateStringInstance(driver, driver.Url, viewExportClassifications_page_url);

                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles.First());
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.AboutAdi_SalesAndDistribution_AdiExportAndImportClassificationInformation_Link);
            }

            if (util.CheckElement(driver, Elements.AboutAdi_SalesAndDistribution_PolicyOnPurchasesFromNonAuthorizedDistributorsOrBrokers_Link, 1))
            {
                //--- Action: Click on Policy on Purchases from Non-Authorized Distributors or Brokers ---//
                action.IClick(driver, Elements.AboutAdi_SalesAndDistribution_PolicyOnPurchasesFromNonAuthorizedDistributorsOrBrokers_Link);

                //--- Expected Result: Policy on Purchases from Non-Authorized Distributors or Brokers should be displayed ---//
                test.validateElementIsPresent(driver, Elements.AboutAdi_SalesAndDistribution_PolicyOnPurchasesFromNonAuthorizedDistributorsOrBrokers_LightBox);
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.AboutAdi_SalesAndDistribution_PolicyOnPurchasesFromNonAuthorizedDistributorsOrBrokers_Link);
            }
        }
    }
}