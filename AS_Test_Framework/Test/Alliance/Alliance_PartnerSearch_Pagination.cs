﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;

namespace AS_Test_Framework.Alliance
{

    [TestFixture]
    public class Alliance_PartnerSearch_Pagination : BaseSetUp
    {
        public Alliance_PartnerSearch_Pagination() : base() { }
        [Test, Category("Alliance"), Category("Core")]
        [TestCase("en", TestName = "Verify if search result is more than 10, then the pagination should display.")]
        public void Alliance_SearchResultPage_SearchField(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/about-adi/alliances/search.html?q=*");
            if (util.GetCount(driver, By.CssSelector("div[class='search-results-item']")) >= 10)
            {
                test.validateElementIsPresent(driver, By.CssSelector("div[class='pagination-control pagination-top pull-right visible-desktop']"));
            }
            else {
                test.validateElementIsNotPresent(driver, By.CssSelector("div[class='pagination-control pagination-top pull-right visible-desktop']"));
            }

        }

        [Test, Category("Alliance"), Category("Core")]
        [TestCase("en", TestName = "Verify Pagination's Previous and Next are working as expected")]
        public void Alliance_SearchResultPage_Prev(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/about-adi/alliances/search.html?q=*");
            if (util.GetCount(driver, By.CssSelector("div[class='search-results-item']")) >= 10)
            {
                test.validateElementIsPresent(driver, Elements.GlobalSearchResults_Next_Btn);
                test.validateElementIsNotPresent(driver, Elements.GlobalSearchResults_Prev_Btn);
                action.IClick(driver, Elements.GlobalSearchResults_Next_Btn);
                test.validateElementIsPresent(driver, Elements.GlobalSearchResults_Prev_Btn);
                test.validateStringIsCorrect(driver, By.CssSelector("div[class='pagination-control pagination-top pull-right visible-desktop']>span[class='active']"), "2");
                action.IClick(driver, Elements.GlobalSearchResults_Prev_Btn);
                test.validateStringIsCorrect(driver, By.CssSelector("div[class='pagination-control pagination-top pull-right visible-desktop']>span[class='active']"), "1");
            }
        }
    }

}
