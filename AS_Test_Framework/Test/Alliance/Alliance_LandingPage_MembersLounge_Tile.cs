﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Threading;
using OpenQA.Selenium;
using System.Linq;

namespace AS_Test_Framework.Alliance
{

    [TestFixture]
    public class Alliance_LandingPage_MembersLounge_Tile : BaseSetUp
    {
        public Alliance_LandingPage_MembersLounge_Tile() : base() { }

        [Test, Category("Alliance"), Category("Core")]
        [TestCase("en", TestName = "Verify that Image for Members Lounge Tile is present for EN Locale")]
        [TestCase("cn", TestName = "Verify that Image for Members Lounge Tile is present for CN Locale")]
        [TestCase("jp", TestName = "Verify that Image for Members Lounge Tile is present for JP Locale")]
        [TestCase("ru", TestName = "Verify that Image for Members Lounge Tile is present for RU Locale")]
        public void Alliance_LandingPage_AboutAllianceImage(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/about-adi/alliances.html");
            if (Locale.Equals("jp"))
            {
                test.validateElementIsPresent(driver, By.CssSelector("div[class='third-tilecom'] * img[alt='メンバーズ・ラウンジ']"));
            } else { 
                test.validateElementIsPresent(driver, Elements.Alliance_LandingPage_MembersLounge_Img);
            }
        }

        [Test, Category("Alliance"), Category("Core")]
        [TestCase("en", TestName = "Verify Go To Lounge button when the user is logged-out")]
        public void Alliance_LandingPage_AboutAlliance_GotoLounge1(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/about-adi/alliances.html");
            test.validateElementIsPresent(driver, Elements.Alliance_LandingPage_MembersLounge_GoTo_Btn);
            action.IClick(driver, Elements.Alliance_LandingPage_MembersLounge_GoTo_Btn);
            Thread.Sleep(1000);
            test.validateStringInstance(driver, driver.Url, "b2clogin.com");
        }

        [Test, Category("Alliance"), Category("Core")]
        [TestCase("en", TestName = "Verify Go To Lounge button when the user is logged-in")]
        public void Alliance_LandingPage_AboutAlliance_GotoLounge2(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/about-adi/alliances.html");
            action.ILoginViaMyAnalogMegaMenu(driver, "aries.sorosoro@analog.com", "Test_1234");
            action.IClick(driver, Elements.Alliance_LandingPage_MembersLounge_GoTo_Btn);
            Thread.Sleep(1000);
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/gated/alliances/members-lounge.html?locale=en");
        }

        //Remove due to content - please see AL-18053
        //[Test, Category("Alliance"), Category("Core")]
        //[TestCase("en", TestName = "Verify Access instructions link if working")]
        //public void Alliance_LandingPage_AboutAlliance_Instruction(string Locale)
        //{
        //    action.Navigate(driver, Configuration.Env_Url + Locale + "/about-adi/alliances.html");
        //    test.validateStringIsCorrect(driver, By.CssSelector("div[class='third-tilecom']>p>a"), "Access instructions");
        //}
    }

}
