﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Threading;
using System.Linq;

namespace AS_Test_Framework.Alliance
{

    [TestFixture]
    public class Alliance_ProfileRegistrationForms : BaseSetUp
    {   
        public Alliance_ProfileRegistrationForms() : base() { }

        string CompanyName = null;

        string FiveHundredOneChar = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaatest";

        public void GoToProfileRegForm()
        {
            action.Navigate(driver, Configuration.Env_Url.Replace("www", "alliances").Replace("cldnet", "corpnt") + "/member/profile/create");
            action.ILogin(driver, "alliance_qa_auto@mailinator.com", "Test_1234");
        }

    
        [Test, Category("Alliance"), Category("Non-Core")]
        [TestCase("en", TestName = "Verify alliance profile registration form Personal Information fields")]
        public void Alliance_ProfileRegForms_PersonalInfoFieldValidations(string Locale)
        {
            GoToProfileRegForm();
            test.validateStringIsCorrect(driver, By.CssSelector("div[class='fca-row']>h1"), "My Company Profile");
            test.validateElementIsPresent(driver, Elements.Alliance_ProfileRegForm_Desc);
            test.validateStringIsCorrect(driver, By.CssSelector("div[class='section-heading']>h3"), "Company & Contact Information");

            /****First Name****/
            test.validateElementIsPresent(driver, Elements.Alliance_ProfileRegForm_FirstName);
            action.IDeleteValueOnFields(driver, Elements.Alliance_ProfileRegForm_FirstName);
            test.validateStringIsCorrect(driver, By.CssSelector("div[class='form-group col-sm-6 has-feedback has-error']>div[class='help-block with-errors']>ul>li"), "First Name is required");
            test.validateElementIsPresent(driver, By.CssSelector("button[class='btn btn-primary btn-lg disabled']"));
            action.IDeleteValueOnFields(driver, Elements.Alliance_ProfileRegForm_FirstName);
            action.IType(driver, Elements.Alliance_ProfileRegForm_FirstName, "Test FName");

            /****Last Name****/
            test.validateElementIsPresent(driver, Elements.Alliance_ProfileRegForm_LastName);
            action.IDeleteValueOnFields(driver, Elements.Alliance_ProfileRegForm_LastName);
            test.validateStringIsCorrect(driver, By.CssSelector("div[class='form-group col-sm-6 has-feedback has-error']>div[class='help-block with-errors']>ul>li"), "Last Name is required");
            test.validateElementIsPresent(driver, By.CssSelector("button[class='btn btn-primary btn-lg disabled']"));
            action.IDeleteValueOnFields(driver, Elements.Alliance_ProfileRegForm_LastName);
            action.IType(driver, Elements.Alliance_ProfileRegForm_LastName, "Test LName");

            /****Address****/
            test.validateElementIsPresent(driver, Elements.Alliance_ProfileRegForm_Address);
            action.IDeleteValueOnFields(driver, Elements.Alliance_ProfileRegForm_Address);
            test.validateStringIsCorrect(driver, By.CssSelector("div[class='form-group has-feedback has-error']>div[class='help-block with-errors']>ul>li"), "Address is required");
            action.IDeleteValueOnFields(driver, Elements.Alliance_ProfileRegForm_Address);
            action.IType(driver, Elements.Alliance_ProfileRegForm_Address, "Test Address");

            /****City****/
            test.validateElementIsPresent(driver, Elements.Alliance_ProfileRegForm_City);
            action.IDeleteValueOnFields(driver, Elements.Alliance_ProfileRegForm_City);
            test.validateStringIsCorrect(driver, By.CssSelector("div[class='form-group col-sm-6 has-feedback has-error']>div[class='help-block with-errors']>ul>li"), "City is required");
            test.validateElementIsPresent(driver, By.CssSelector("button[class='btn btn-primary btn-lg disabled']"));
            action.IDeleteValueOnFields(driver, Elements.Alliance_ProfileRegForm_City);
            action.IType(driver, Elements.Alliance_ProfileRegForm_City, "Test City");

            /****State***/
            test.validateElementIsNotEnabled(driver, Elements.Alliance_ProfileRegForm_State);
            //action.IDeleteValueOnFields(driver, Elements.Alliance_ProfileRegForm_State);
            //action.IType(driver, Elements.Alliance_ProfileRegForm_State, "Test State");*/

            /****Country/Region****/
            test.validateElementIsPresent(driver, Elements.Alliance_ProfileRegForm_CountryDD);
            action.ISelectFromDropdownByStringText(driver, Elements.Alliance_ProfileRegForm_CountryDD, "-Select Country/Region-");
            test.validateStringIsCorrect(driver, By.CssSelector("div[class='form-group col-sm-6 has-feedback has-error']>div[class='help-block with-errors']>ul>li"), "Country/Region is required");
            test.validateElementIsPresent(driver, By.CssSelector("button[class='btn btn-primary btn-lg disabled']"));
            action.ISelectFromDropdown(driver, Elements.Alliance_ProfileRegForm_CountryDD, "US");
            test.validateElementIsEnabled(driver, Elements.Alliance_ProfileRegForm_State);

            /****Contact Email****/
            test.validateElementIsPresent(driver, Elements.Alliance_ProfileRegForm_ContactEmail);
            action.IDeleteValueOnFields(driver, Elements.Alliance_ProfileRegForm_ContactEmail);
            test.validateStringIsCorrect(driver, By.CssSelector("div[class='form-group col-sm-6 has-feedback has-error']>div[class='help-block with-errors']>ul>li"), "Email is required");
            test.validateElementIsPresent(driver, By.CssSelector("button[class='btn btn-primary btn-lg disabled']"));
            action.IType(driver, Elements.Alliance_ProfileRegForm_ContactEmail, "alliance_qa");
            test.validateStringIsCorrect(driver, By.CssSelector("div[class='form-group col-sm-6 has-feedback has-error']>div[class='help-block with-errors']>ul>li"), "Please enter a valid email address");
            action.IType(driver, Elements.Alliance_ProfileRegForm_ContactEmail, "_auto@mailinator.com");
        
            /****Website****/
            test.validateElementIsPresent(driver, Elements.Alliance_ProfileRegForm_WebSite);
            action.IDeleteValueOnFields(driver, Elements.Alliance_ProfileRegForm_WebSite);
            test.validateStringIsCorrect(driver, By.CssSelector("div[class='form-group col-sm-6 has-feedback has-error']>div[class='help-block with-errors']>ul>li"), "Website is required");
            test.validateElementIsPresent(driver, By.CssSelector("button[class='btn btn-primary btn-lg disabled']"));
            action.IType(driver, Elements.Alliance_ProfileRegForm_WebSite, "alliance_qa");
            test.validateStringIsCorrect(driver, By.CssSelector("div[class='form-group col-sm-6 has-feedback has-error']>div[class='help-block with-errors']>ul>li"), "Invalid website, website must start with either http or https");
            action.IDeleteValueOnFields(driver, Elements.Alliance_ProfileRegForm_WebSite);
            action.IType(driver, Elements.Alliance_ProfileRegForm_WebSite, "https://analog.com");

            /****Phone****/
            test.validateElementIsPresent(driver, Elements.Alliance_ProfileRegForm_Phone);
            action.IDeleteValueOnFields(driver, Elements.Alliance_ProfileRegForm_Phone);
            test.validateStringIsCorrect(driver, By.CssSelector("div[class='form-group col-sm-6 has-feedback has-error']>div[class='help-block with-errors']>ul>li"), "Phone is required");
            test.validateElementIsPresent(driver, By.CssSelector("button[class='btn btn-primary btn-lg disabled']"));
            action.IType(driver, Elements.Alliance_ProfileRegForm_Phone, "1234");
            test.validateStringIsCorrect(driver, By.CssSelector("div[class='form-group col-sm-6 has-feedback has-error']>div[class='help-block with-errors']>ul>li"), "Minimum of 5 characters");
            action.IDeleteValueOnFields(driver, Elements.Alliance_ProfileRegForm_Phone);
            action.IType(driver, Elements.Alliance_ProfileRegForm_Phone, "123456789");

            /****Zip Code****/
            test.validateElementIsPresent(driver, Elements.Alliance_ProfileRegForm_ZipCode);
            action.IDeleteValueOnFields(driver, Elements.Alliance_ProfileRegForm_ZipCode);
            test.validateElementIsNotPresent(driver, By.CssSelector("div[class='form-group col-sm-6 has-feedback has-error']>div[class='help-block with-errors']>ul>li"));
            action.IType(driver, Elements.Alliance_ProfileRegForm_ZipCode, "123");
            test.validateStringIsCorrect(driver, By.CssSelector("div[class='form-group col-sm-6 has-feedback has-error']>div[class='help-block with-errors']>ul>li"), "Minimum of 5 characters");
            action.IDeleteValueOnFields(driver, Elements.Alliance_ProfileRegForm_ZipCode);
            action.IType(driver, Elements.Alliance_ProfileRegForm_ZipCode, "12345");

            /****Social Media****/
            test.validateElementIsPresent(driver, Elements.Alliance_ProfileRegForm_FB);
            action.IType(driver, Elements.Alliance_ProfileRegForm_FB, "aaaaaaA");
            test.validateStringIsCorrect(driver, By.CssSelector("div[class='form-group has-feedback has-error']>div[class='help-block with-errors']>ul>li"), "Please enter a valid URL");
            action.IDeleteValueOnFields(driver, Elements.Alliance_ProfileRegForm_FB);

            test.validateElementIsPresent(driver, Elements.Alliance_ProfileRegForm_LinkedIn);
            action.IType(driver, Elements.Alliance_ProfileRegForm_LinkedIn, "aaaaaaA");
            test.validateStringIsCorrect(driver, By.CssSelector("div[class='form-group has-feedback has-error']>div[class='help-block with-errors']>ul>li"), "Please enter a valid URL");
            action.IDeleteValueOnFields(driver, Elements.Alliance_ProfileRegForm_LinkedIn);

            //test.validateElementIsPresent(driver, Elements.Alliance_ProfileRegForm_Gplus);
            //action.IType(driver, Elements.Alliance_ProfileRegForm_Gplus, "aaaaaaA");
            //test.validateStringIsCorrect(driver, By.CssSelector("div[class='form-group has-feedback has-error']>div[class='help-block with-errors']>ul>li"), "Please enter a valid URL");
            //action.IDeleteValueOnFields(driver, Elements.Alliance_ProfileRegForm_Gplus);

            test.validateElementIsPresent(driver, Elements.Alliance_ProfileRegForm_Twitter);
            action.IType(driver, Elements.Alliance_ProfileRegForm_Twitter, "aaaaaaA");
            test.validateStringIsCorrect(driver, By.CssSelector("div[class='form-group has-feedback has-error']>div[class='help-block with-errors']>ul>li"), "Please enter a valid URL");
            action.IDeleteValueOnFields(driver, Elements.Alliance_ProfileRegForm_Twitter);

        }

        [Test, Category("Alliance"), Category("Non-Core")]
        [TestCase("en", TestName = "Verify alliance profile registration form Company Information fields")]
        public void Alliance_ProfileRegForms_FieldValdiations_CompanyName(string Locale)
        {
            GoToProfileRegForm();
            CompanyName = util.Generate_EmailAddress().Replace("@mailinator.com", "");
            /*****Company Name****/
            test.validateElementIsPresent(driver, Elements.Alliance_ProfileRegForm_CompName);
            test.validateFieldIsNotEmpty(driver, Elements.Alliance_ProfileRegForm_CompName);
            action.IDeleteValueOnFields(driver, Elements.Alliance_ProfileRegForm_CompName);
            action.IType(driver, Elements.Alliance_ProfileRegForm_CompName, "HUBER SIGNAL PROCESSING");
            action.IType(driver, Elements.Alliance_ProfileRegForm_CompName, Keys.Tab);
            test.validateStringIsCorrect(driver, Elements.Alliance_ProfileRegForm_CompExists, "Company Name already exists");
            action.IDeleteValueOnFields(driver, Elements.Alliance_ProfileRegForm_CompName);
            action.IType(driver, Elements.Alliance_ProfileRegForm_CompName, Keys.Tab);
            test.validateStringIsCorrect(driver, By.CssSelector("div[id='companyNameDiv'] div[class='help-block with-errors']>ul>li"), "Company Name is required");
            test.validateElementIsPresent(driver, By.CssSelector("button[class='btn btn-primary btn-lg disabled']"));
            action.IDeleteValueOnFields(driver, Elements.Alliance_ProfileRegForm_CompName);
            action.IType(driver, Elements.Alliance_ProfileRegForm_CompName, CompanyName);

            /****Company Overview******/
            action.IDeleteValueOnFields(driver, Elements.Alliance_ProfileRegForm_CompOverview);
            action.IType(driver, Elements.Alliance_ProfileRegForm_CompOverview, Keys.Tab);
            test.validateStringIsCorrect(driver, By.CssSelector("div[class='form-group has-feedback textarea-border-grey has-error']>div[class='help-block with-errors']>ul>li"), "Company Overview is required");
            test.validateElementIsPresent(driver, By.CssSelector("button[class='btn btn-primary btn-lg disabled']"));
            action.IType(driver, Elements.Alliance_ProfileRegForm_CompOverview, FiveHundredOneChar);
            test.validateCountIsEqual(driver, 500, util.ReturnAttribute(driver, Elements.Alliance_ProfileRegForm_CompOverview, "value").Length);
            test.validateStringIsCorrect(driver, By.CssSelector("div[class='form-group has-feedback has-success textarea-border-grey']>div[class='help-block']"), "0 characters remaining.");
            action.IDeleteValueOnFields(driver, Elements.Alliance_ProfileRegForm_CompOverview);
            action.IType(driver, Elements.Alliance_ProfileRegForm_CompOverview, "This is just a test");

            /****Capabilities Overview******/
            action.IDeleteValueOnFields(driver, Elements.Alliance_ProfileRegForm_CapabilitiesOverview);
            action.IType(driver, Elements.Alliance_ProfileRegForm_CapabilitiesOverview, Keys.Tab);
            test.validateStringIsCorrect(driver, By.CssSelector("div[class='form-group has-feedback textarea-border-grey has-error']>div[class='help-block with-errors']>ul>li"), "Capability Overview is required");
            test.validateElementIsPresent(driver, By.CssSelector("button[class='btn btn-primary btn-lg disabled']"));
            action.IType(driver, Elements.Alliance_ProfileRegForm_CapabilitiesOverview, FiveHundredOneChar);
            test.validateCountIsEqual(driver, 500, util.ReturnAttribute(driver, Elements.Alliance_ProfileRegForm_CapabilitiesOverview, "value").Length);
            test.validateStringIsCorrect(driver, By.CssSelector("div[class='form-container registrationform clearfix']>div:nth-child(4)>div[class='help-block']"), "0 characters remaining.");
            action.IDeleteValueOnFields(driver, Elements.Alliance_ProfileRegForm_CapabilitiesOverview);
            action.IType(driver, Elements.Alliance_ProfileRegForm_CapabilitiesOverview, "This is just a test");

        }

        [Test, Category("Alliance"), Category("Non-Core")]
        [TestCase("en", TestName = "Verify alliance profile registration form Other Details Checkboxes fields")]
        public void Alliance_ProfileRegForms_FieldValdiations_OtherDetailsCheckboxes(string Locale)
        {
            GoToProfileRegForm();
            string CompanyName = util.Generate_EmailAddress().Replace("@mailinator.com", "");

            string[] OtherDetailsfor = { "* Markets", "* Capabilities", "* Product Categories", "* Regions" };


            for (int x = 3, y=0; y != OtherDetailsfor.Length; x++,y++)
            {
                test.validateStringIsCorrect(driver, By.CssSelector("div[class='form-container checkbox-div clearfix']:nth-of-type(" + x + ") * h4"), OtherDetailsfor[y]);
                action.IClick(driver, By.CssSelector("div[class='form-container checkbox-div clearfix']:nth-of-type(" + x + ") * div>div * input"));
                test.validateElementIsPresent(driver, By.CssSelector("div[class='form-container checkbox-div clearfix']:nth-of-type(" + x + ")>div[class='form-group has-error']"));
                test.validateElementIsPresent(driver, By.CssSelector("button[class='btn btn-primary btn-lg disabled']"));
                action.IClick(driver, By.CssSelector("div[class='form-container checkbox-div clearfix']:nth-of-type(" + x + ") * div>div * input"));
            }
        }

        [Test, Category("Alliance"), Category("Non-Core")]
        [TestCase(TestName = "Verify message prompt when alliance application is not yet approved in webcontent")]
        public void PendingAccess()
        {
            action.Navigate(driver, Configuration.Env_Url + "en/about-adi/alliances/about-members-lounge.html");
            action.ILogInViaMyAnalogWidget(driver, "qa_alliance_pending@mailinator.com", "Test_1234");
            Thread.Sleep(2000);
            //action.IClick(driver, By.CssSelector("div[class='row fca-row one-columnspot top-spacing-32 bottom-spacing-32 bg-grey'] * div[class='clearfix rte']>p:nth-child(3)>a"));
            //action.ILogin(driver, "qa_alliance_pending@mailinator.com", "Test_1234");
            driver.Navigate().GoToUrl(Configuration.Env_Url + "en/gated/alliances/members-lounge.html");
            action.IClick(driver, Elements.Alliance_MemersLounge_CompanyProfile);
            test.validateStringIsCorrect(driver, Elements.Alliance_ProfileRegForm_Modal, "Your Alliances Application needs to be approved before using this page. Please contact Alliances@analog.com for more information.");
            action.IClick(driver, By.CssSelector("button[id='saveProfileModal']"));
            test.validateScreenByUrl(driver, Configuration.Env_Url + "en/about-adi/alliances.html");
        }

        [Test, Category("Alliance"), Category("Non-Core")]
        [TestCase(TestName = "Verify message prompt when the user does not have appliance application")]
        public void NoAccess()
        {
            action.Navigate(driver, Configuration.Env_Url + "en/about-adi/alliances/about-members-lounge.html");
            Thread.Sleep(2000);
            action.ILogInViaMyAnalogWidget(driver, "aries.sorosoro@analog.com", "Test_1234");
            Thread.Sleep(2000);
            //action.IClick(driver, By.CssSelector("div[class='row fca-row one-columnspot top-spacing-32 bottom-spacing-32 bg-grey'] * div[class='clearfix rte']>p:nth-child(3)>a"));
            //action.ILogin(driver, "qa_alliance_pending@mailinator.com", "Test_1234");
            driver.Navigate().GoToUrl(Configuration.Env_Url + "en/gated/alliances/members-lounge.html");
            action.IClick(driver, Elements.Alliance_MemersLounge_CompanyProfile);
            test.validateStringIsCorrect(driver, Elements.Alliance_ProfileRegForm_Modal, "You can not access this form because you do not have an approved alliance application");
        }

        [Test, Category("Alliance"), Category("Non-Core")]
        [TestCase(TestName = "Verify successful submission of forms")]
        public void SuccessfulSubmit()
        {
            string expectedmessage = "Thank you for completing your profile. We will review your entry shortly. Upon approval, your profile will be published to the web site.\r\nRegards,\r\nAnalog Devices Alliances Team";
            CompanyName =  util.Generate_EmailAddress().Replace("@mailinator.com", "");
            GoToProfileRegForm();
            action.IDeleteValueOnFields(driver, Elements.Alliance_ProfileRegForm_CompName);
            action.IType(driver, Elements.Alliance_ProfileRegForm_CompName, CompanyName);
            action.IClick(driver, Elements.Alliance_ProfileRegForm_SubmitBtn);
            test.validateStringIsCorrect(driver, Elements.Alliance_ProfileRegForm_Modal, expectedmessage);

            if (Configuration.Environment.Equals("production"))
            {
                action.Navigate(driver, Configuration.Env_Url.Replace("www", "webcontent.corpnt") + "AlliancesAdmin/AlliancesAdmin/ManageAlliancesProfile.aspx");
            }
            else
            {
                action.Navigate(driver, Configuration.Env_Url.Replace("www", "webcontent").Replace("cldnet", "corpnt") + "AlliancesAdmin/ManageAlliancesProfile.aspx");
            }
            action.IType(driver, By.CssSelector("input[id='_ctl0__content_txtCompanyName']"), CompanyName);
            action.IClick(driver, By.CssSelector("input[id='_ctl0__content_btnSearch']"));
            test.validateStringIsCorrect(driver, By.CssSelector("table[id='_ctl0__content_grdManageAllianceProfile']>tbody>tr:nth-child(2)>td:nth-child(5)"), "Pending");

            /***reject submitted form***/
            action.IClick(driver, By.CssSelector("a[id='_ctl0__content_grdManageAllianceProfile__ctl2_lnkViewEdit']"));
            action.IType(driver, By.CssSelector("textarea[id='_ctl0__content_txtAdminComments']"), "This is just a test");
            action.IClick(driver, By.CssSelector("input[id='_ctl0__content_btnRejected']"));
        }
    }
}
