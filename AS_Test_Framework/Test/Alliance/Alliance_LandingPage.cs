﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;

namespace AS_Test_Framework.Alliance
{

    [TestFixture]
    public class Alliance_LandingPage : BaseSetUp
    {
        public Alliance_LandingPage() : base() { }

        [Test, Category("Alliance"), Category("Core")]
        [TestCase("en", TestName = "Verify the Hero Image for EN Locale")]
        [TestCase("cn", TestName = "Verify the Hero Image for CN Locale")]
        [TestCase("jp", TestName = "Verify the Hero Image for JP Locale")]
        [TestCase("ru", TestName = "Verify the Hero Image for RU Locale")]
        public void Alliance_LandingPage_HeroImage(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/about-adi/alliances.html");
            test.validateElementIsPresent(driver, Elements.Alliance_LandingPage_HeroImage);
        }

        [Test, Category("Alliance"), Category("Core")]
        [TestCase("en", "Alliances Partner Program", TestName = "Verify Alliances title is displayed for EN Locale")]
        [TestCase("cn", "战略联盟", TestName = "Verify Alliances title is displayed for CN Locale")]
        [TestCase("jp", "アライアンス", TestName = "Verify Alliances title is displayed for JP Locale")]
        [TestCase("ru", "Программа Alliances", TestName = "Verify Alliances title is displayed for RU Locale")]
        public void Alliance_LandingPage_Title(string Locale, string Title)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/about-adi/alliances.html");
            test.validateString(driver, Title, util.GetText(driver, By.CssSelector("div[class='clearfix rte']>p>span>strong")));
        }
    }

}
