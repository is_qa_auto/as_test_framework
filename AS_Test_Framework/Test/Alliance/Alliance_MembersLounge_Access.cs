﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Threading;
using System.Linq;
using System.Collections.Generic;

namespace AS_Test_Framework.Alliance
{

    [TestFixture]
    public class Alliance_MembersLounge_Access : BaseSetUp
    {
        //[Test, Category("Alliance"), Category("Core"), Retry(2)]
        //[TestCase("en", TestName = "Verify \"click here\" link when user is not yet an Alliances member but already registered as Analog member.")]
        //public void Alliance_MemberLounge_Apply2(string Locale)
        //{
        //    action.Navigate(driver, Configuration.Env_Url + Locale + "/about-adi/alliances/about-members-lounge.html");
        //    action.ILoginViaMyAnalogMegaMenu(driver, "alliance_not_member@mailinator.com", "Test_1234");
        //    action.IClick(driver, By.CssSelector("div[class='clearfix rte']>p:nth-child(3)>a"));
        //    driver.SwitchTo().Window(driver.WindowHandles.Last());
        //    Thread.Sleep(1000);
        //    test.validateStringInstance(driver, driver.Url, Configuration.Env_Url.Replace("www", "alliances").Replace("cldnet", "corpnt") + "member/application/signup");

        //}

        [Test, Category("Alliance"), Category("Core")]
        //[TestCase("en", "Welcome to the Alliances Members Lounge", TestName = "Verify \"click here\" link when user is already an Analog and Alliances member for EN Locale")]
        [TestCase("cn", "Welcome to the Alliances Members Lounge", TestName = "Verify \"click here\" link when user is already an Analog and Alliances member for CN Locale")]
        [TestCase("jp", "アライアンス・メンバーズ・ラウンジへようこそ", TestName = "Verify \"click here\" link when user is already an Analog and Alliances member for JP Locale")]
        [TestCase("ru", "Welcome to the Alliances Members Lounge", TestName = "Verify \"click here\" link when user is already an Analog and Alliances member for RU Locale")]
        public void Alliance_MemberLounge_Access1111(string Locale, string MembersLoungeTitle)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/about-adi/alliances/about-members-lounge.html");
            action.ILoginViaMyAnalogMegaMenu(driver, "alliance_qa_auto@mailinator.com", "Test_1234");
            action.IClick(driver, By.CssSelector("div[class='clearfix rte']>p:nth-child(3)>a"));
            if (Locale.Equals("jp")) {
                driver.SwitchTo().Window(driver.WindowHandles.Last());
            }
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/gated/alliances/members-lounge.html");
            test.validateString(driver, MembersLoungeTitle, util.GetText(driver, By.CssSelector("div[class='container page-title '] h1")));
        }

        [Test, Category("Alliance"), Category("Core")]
        [TestCase("en", TestName = "Verify the Floating Left Navigation is present")]
        public void Alliance_MemberLounge_LeftNav(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/about-adi/alliances/about-members-lounge.html");
            action.ILoginViaMyAnalogMegaMenu(driver, "alliance_qa_auto@mailinator.com", "Test_1234");
            driver.Navigate().GoToUrl(Configuration.Env_Url + Locale + "/gated/alliances/members-lounge.html");
            test.validateElementIsPresent(driver, Elements.Alliance_MembersLounge_LeftNav);
        }

        [Test, Category("Alliance"), Category("Core")]
        [TestCase("en", TestName = "Verify Left Navigation Item redirection")]
        public void Alliance_MemberLounge_LeftNav_Redirection(string Locale)
        {
            Dictionary<string, string> LeftNavLink = new Dictionary<string, string>();

            LeftNavLink.Add("Alliances", "about-adi/alliances.html");
            LeftNavLink.Add("About Alliances", "about-adi/alliances/about-alliances.html");
            LeftNavLink.Add("Find a Partner", "about-adi/alliances/search.html?q=*");
            LeftNavLink.Add("My Company Profile", "member/profile/create");
            LeftNavLink.Add("Contact Alliances", "form_pages/alliances/contact.aspx");

            action.Navigate(driver, Configuration.Env_Url + Locale + "/about-adi/alliances/about-members-lounge.html");
            action.ILoginViaMyAnalogMegaMenu(driver, "alliance_qa_auto@mailinator.com", "Test_1234");
            driver.Navigate().GoToUrl(Configuration.Env_Url + Locale + "/gated/alliances/members-lounge.html");
            //action.IClick(driver, By.CssSelector("div[class='clearfix rte']>p:nth-child(3)>a"));
            foreach (KeyValuePair<string, string> value in LeftNavLink) {
                action.IClickALink(driver, value.Key);
                test.validateStringInstance(driver, driver.Url, value.Value);
                if (value.Key.Equals("Find a Partner")) {
                    test.validateElementIsPresent(driver, Elements.GlobalSearchResults_SearchResultList);
                }
                driver.Navigate().Back();
            }
            
        }
        
    }

}
