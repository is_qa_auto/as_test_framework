﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;

namespace AS_Test_Framework.Alliance
{

    [TestFixture]
    public class Alliance_PartnerSearch_DidYouMean : BaseSetUp
    {
        public Alliance_PartnerSearch_DidYouMean() : base() { }

        [Test, Category("Alliance"), Category("Core"), Retry(2)]
        [TestCase("en", "LPA Conceps", TestName = "Verify that \"Show results instead for\" is being displayed for EN Lcoale")]
        //[TestCase("cn", "LPA Conceps", TestName = "Verify that \"Show results instead for\" is being displayed for CN Lcoale")]
        //[TestCase("jp", "LPA Conceps", TestName = "Verify that \"Show results instead for\" is being displayed for JP Lcoale")]
        //[TestCase("ru", "LPA Conceps", TestName = "Verify that \"Show results instead for\" is being displayed for RU Lcoale")]
        public void Alliance_SearchResultPage_DidYouMean(string Locale, string SearchKw)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/about-adi/alliances/search.html?q=*");
            action.IDeleteValueOnFields(driver, Elements.GlobalSearchResults_Search_Txtbox);
            action.IType(driver, Elements.GlobalSearchResults_Search_Txtbox, SearchKw);
            action.IClick(driver, Elements.GlobalSearchResults_Search_Btn);
            test.validateElementIsPresent(driver, Elements.GlobalSearchResults_ShowingResultsFor_Txt);
            test.validateElementIsPresent(driver, Elements.GlobalSearchResults_ShowResultsInsteadFor_Txt);
        }
    }

}
