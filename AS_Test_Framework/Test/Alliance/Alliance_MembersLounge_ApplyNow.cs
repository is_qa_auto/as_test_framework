﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Threading;
using System.Linq;

namespace AS_Test_Framework.Alliance
{

    [TestFixture]
    public class Alliance_MembersLounge_ApplyNow : BaseSetUp
    {
        public Alliance_MembersLounge_ApplyNow() : base() { }

        [Test, Category("Alliance"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify If the user is logged-out, Clicking Apply Now button should redirect to Log-in page")]
        public void Alliance_MemberLounge_Apply1(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/about-adi/alliances/about-members-lounge.html");
            action.IClick(driver, By.CssSelector("a[class='btn btn-primary btn-lg']"));
            driver.SwitchTo().Window(driver.WindowHandles.Last());
            Thread.Sleep(1000);
            test.validateStringInstance(driver, driver.Url, "b2clogin.com");
        }

        [Test, Category("Alliance"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify If the user is logged-in but not yet registered as a memeber, Clicking Apply Now button should redirect to Membership Application without error for EN Locale")]
        [TestCase("cn", TestName = "Verify If the user is logged-in but not yet registered as a memeber, Clicking Apply Now button should redirect to Membership Application without error for CN Locale")]
        public void Alliance_MemberLounge_Apply2(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/about-adi/alliances/about-members-lounge.html");
            action.ILoginViaMyAnalogMegaMenu(driver, "aries.sorosoro@analog.com", "Test_1234");
            action.IClick(driver, By.CssSelector("a[class='btn btn-primary btn-lg']"));
            driver.SwitchTo().Window(driver.WindowHandles.Last());
            Thread.Sleep(1000);
            if (Locale.Equals("cn"))
            {
                test.validateScreenByUrl(driver, Configuration.Env_Url.Replace("www", "alliances").Replace("cldnet", "corpnt") + "member/application/signup?locale=zh");
            }
            else { 
                test.validateScreenByUrl(driver, Configuration.Env_Url.Replace("www", "alliances").Replace("cldnet", "corpnt") + "member/application/signup");
            }
        }

        [Test, Category("Alliance"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify If the user is logged-in but is registered as a memeber, Clicking Apply Now button should redirect to Membership Application with pop-up error")]
        public void Alliance_MemberLounge_Apply3(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/about-adi/alliances/about-members-lounge.html");
            action.ILoginViaMyAnalogMegaMenu(driver, "alliance_qa_auto@mailinator.com", "Test_1234");
            action.IClick(driver, By.CssSelector("a[class='btn btn-primary btn-lg']"));
            driver.SwitchTo().Window(driver.WindowHandles.Last());
            Thread.Sleep(1000);
            test.validateString(driver, "You have already submitted your Application. Please contact alliances@analog.com if you have any questions.", util.GetText(driver, Elements.Alliance_Application_Modal));
        }
    }

}
