﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;

namespace AS_Test_Framework.Alliance
{

    [TestFixture]
    public class Alliance_PartnerSearch_SocialMedia : BaseSetUp
    {
        public Alliance_PartnerSearch_SocialMedia() : base() { }

        [Test, Category("Alliance"), Category("Core")]
        [TestCase("en", TestName = "Verify The icons for Facebook, Twitter, and LinkedIn for EN Lcoale")]
        [TestCase("cn", TestName = "Verify The icons for Facebook, Twitter, and LinkedIn for CN Lcoale")]
        [TestCase("jp", TestName = "Verify The icons for Facebook, Twitter, and LinkedIn for JP Lcoale")]
        [TestCase("ru", TestName = "Verify The icons for Facebook, Twitter, and LinkedIn for RU Lcoale")]
        public void Alliance_SocialMediaLinks(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/about-adi/alliances/partner/trenz-electronic-gmbh.html");
            test.validateElementIsPresent(driver, Elements.Alliance_PartnerPage_FBIcon);
            test.validateElementIsPresent(driver, Elements.Alliance_PartnerPage_LIIcon);
            test.validateElementIsPresent(driver, Elements.Alliance_PartnerPage_TwitterIcon);
        }
    }

}
