﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;

namespace AS_Test_Framework.Alliance
{

    [TestFixture]
    public class Alliance_PartnerSearch_Filter : BaseSetUp
    {
        public Alliance_PartnerSearch_Filter() : base() { }

        [Test, Category("Alliance"), Category("Core")]
        [TestCase("en", TestName = "Verify that the alliance filter - market is present and working in EN Locale")]
        [TestCase("cn", TestName = "Verify that the alliance filter - market is present and working in CN Locale")]
        [TestCase("jp", TestName = "Verify that the alliance filter - market is present and working in JP Locale")]
        [TestCase("ru", TestName = "Verify that the alliance filter - market is present and working in RU Locale")]
        public void Alliance_SearchResultPage_MarketFilter(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/about-adi/alliances/search.html?q=*");
            test.validateElementIsPresent(driver, By.CssSelector("li[class=''][data-facetcategory^='market_l1']"));
            action.IClick(driver, By.CssSelector("li[class=''][data-facetcategory^='market_l1']>a"));
            test.validateElementIsPresent(driver, By.CssSelector("li[class='active'][data-facetcategory^='market_l1']>a"));
            action.IClick(driver, By.CssSelector("li[class='active'][data-facetcategory^='market_l1']>ul>li>a"));
            test.validateElementIsPresent(driver, By.CssSelector("li[class='active'][data-facetcategory^='market_l1']>ul>li[class=' checkbox on']>a"));
        }

        [Test, Category("Alliance"), Category("Core")]
        [TestCase("en", TestName = "Verify that the alliance filter - Capabilities is present and working in EN Locale")]
        [TestCase("cn", TestName = "Verify that the alliance filter - Capabilities is present and working in CN Locale")]
        [TestCase("jp", TestName = "Verify that the alliance filter - Capabilities is present and working in JP Locale")]
        [TestCase("ru", TestName = "Verify that the alliance filter - Capabilities is present and working in RU Locale")]
        public void Alliance_SearchResultPage_CapabilitiesFilter(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/about-adi/alliances/search.html?q=*");
            test.validateElementIsPresent(driver, By.CssSelector("li[class=''][data-facetcategory^='capabilities_sm']"));
            action.IClick(driver, By.CssSelector("li[class=''][data-facetcategory^='capabilities_sm']>a"));
            test.validateElementIsPresent(driver, By.CssSelector("li[class='active'][data-facetcategory^='capabilities_sm']>a"));
            action.IClick(driver, By.CssSelector("li[class='active'][data-facetcategory^='capabilities_sm']>ul>li>a"));
            test.validateElementIsPresent(driver, By.CssSelector("li[class='active'][data-facetcategory^='capabilities_sm']>ul>li[class=' checkbox on']>a"));
        }

        [Test, Category("Alliance"), Category("Core")]
        [TestCase("en", TestName = "Verify that the alliance filter - Product Categories is present and working in EN Locale")]
        [TestCase("cn", TestName = "Verify that the alliance filter - Product Categories is present and working in CN Locale")]
        [TestCase("jp", TestName = "Verify that the alliance filter - Product Categories is present and working in JP Locale")]
        [TestCase("ru", TestName = "Verify that the alliance filter - Product Categories is present and working in RU Locale")]
        public void Alliance_SearchResultPage_ProdCatFilter(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/about-adi/alliances/search.html?q=*");
            test.validateElementIsPresent(driver, By.CssSelector("li[class=''][data-facetcategory^='prod_cat_l1']"));
            action.IClick(driver, By.CssSelector("li[class=''][data-facetcategory^='prod_cat_l1']>a"));
            test.validateElementIsPresent(driver, By.CssSelector("li[class='active'][data-facetcategory^='prod_cat_l1']>a"));
            action.IClick(driver, By.CssSelector("li[class='active'][data-facetcategory^='prod_cat_l1']>ul>li>a"));
            test.validateElementIsPresent(driver, By.CssSelector("li[class='active'][data-facetcategory^='prod_cat_l1']>ul>li[class=' radio on']>a"));
        }

        [Test, Category("Alliance"), Category("Core")]
        [TestCase("en", TestName = "Verify that the alliance filter - Regions is present and working in EN Locale")]
        [TestCase("cn", TestName = "Verify that the alliance filter - Regions Categories is present and working in CN Locale")]
        [TestCase("jp", TestName = "Verify that the alliance filter - Regions Categories is present and working in JP Locale")]
        [TestCase("ru", TestName = "Verify that the alliance filter - Regions Categories is present and working in RU Locale")]
        public void Alliance_SearchResultPage_RegionsFilter(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/about-adi/alliances/search.html?q=*");
            test.validateElementIsPresent(driver, By.CssSelector("li[class=''][data-facetcategory^='regions_sm']"));
            action.IClick(driver, By.CssSelector("li[class=''][data-facetcategory^='regions_sm']>a"));
            test.validateElementIsPresent(driver, By.CssSelector("li[class='active'][data-facetcategory^='regions_sm']>a"));
            action.IClick(driver, By.CssSelector("li[class='active'][data-facetcategory^='regions_sm']>ul>li>a"));
            test.validateElementIsPresent(driver, By.CssSelector("li[class='active'][data-facetcategory^='regions_sm']>ul>li[class=' checkbox on']>a"));
        }

        [Test, Category("Alliance"), Category("Core")]
        [TestCase("en", TestName = "Verify that reset filter is present and working in EN Locale")]
        [TestCase("cn", TestName = "Verify that reset filter is present and working in CN Locale")]
        [TestCase("jp", TestName = "Verify that reset filter is present and working in JP Locale")]
        [TestCase("ru", TestName = "Verify that reset filter is present and working in RU Locale")]
        public void Alliance_SearchResultPage_ResetFilter(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/about-adi/alliances/search.html?q=*");
            test.validateElementIsPresent(driver, By.CssSelector("div[class='facet-filters facet-custom-filters'] button[class='btn btn-primary button-main']"));
            action.IClick(driver, By.CssSelector("li[class=''][data-facetcategory^='market_l1']>a"));
            action.IClick(driver, By.CssSelector("li[class='active'][data-facetcategory^='market_l1']>ul>li>a"));
            action.IClick(driver, By.CssSelector("div[class='facet-filters facet-custom-filters'] button[class='btn btn-primary button-main']"));

            //test.validateElementIsPresent(driver, By.CssSelector("li[class='active'][data-facetcategory^='market_l1']"));
            test.validateElementIsNotPresent(driver, By.CssSelector("li[class='active'][data-facetcategory^='market_l1']>ul>li[class=' checkbox on']>a"));
        }
    }

}
