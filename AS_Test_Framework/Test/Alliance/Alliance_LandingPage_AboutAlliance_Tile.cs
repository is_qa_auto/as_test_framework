﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;

namespace AS_Test_Framework.Alliance
{

    [TestFixture]
    public class Alliance_LandingPage_AboutAlliance_Tile : BaseSetUp
    {
        public Alliance_LandingPage_AboutAlliance_Tile() : base() { }

        [Test, Category("Alliance"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that Image for About Alliance Tile is present for EN Locale")]
        [TestCase("cn", TestName = "Verify that Image for About Alliance Tile is present for CN Locale")]
        [TestCase("jp", TestName = "Verify that Image for About Alliance Tile is present for JP Locale")]
        [TestCase("ru", TestName = "Verify that Image for About Alliance Tile is present for RU Locale")]
        public void Alliance_LandingPage_AboutAllianceImage(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/about-adi/alliances.html");
            test.validateElementIsPresent(driver, Elements.Alliance_LandingPage_AboutAlliances_Img);
        }

        [Test, Category("Alliance"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that Learn More button for About Alliance Tile is present and working for EN Locale")]
        [TestCase("cn", TestName = "Verify that Learn More button for About Alliance Tile is present and working for for CN Locale")]
        [TestCase("jp", TestName = "Verify that Learn More button for About Alliance Tile is present and working for JP Locale")]
        [TestCase("ru", TestName = "Verify that Learn More button for About Alliance Tile is present and working for RU Locale")]
        public void Alliance_LandingPage_AboutAlliance_LearnMoreBtn(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/about-adi/alliances.html");
            test.validateElementIsPresent(driver, Elements.Alliance_LandingPage_AboutAlliances_LearnMore_Btn);
            action.IClick(driver, Elements.Alliance_LandingPage_AboutAlliances_LearnMore_Btn);
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/about-adi/alliances/about-alliances.html");
        }
    }

}
