﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;

namespace AS_Test_Framework.Alliance
{

    [TestFixture]
    public class Alliance_PartnerSearch_SearchFields : BaseSetUp
    {
        public Alliance_PartnerSearch_SearchFields() : base() { }
        [Test, Category("Alliance"), Category("Core")]
        [TestCase("en", TestName = "Verify Seach field in EN Locale")]
        [TestCase("cn", TestName = "Verify Seach field in CN Locale")]
        [TestCase("jp", TestName = "Verify Seach field in JP Locale")]
        [TestCase("ru", TestName = "Verify Seach field in RU Locale")]
        public void Alliance_SearchResultPage_SearchField(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/about-adi/alliances/search.html?q=*");
            test.validateElementIsPresent(driver, Elements.GlobalSearchResults_Search_Txtbox);
            test.validateElementIsPresent(driver, Elements.GlobalSearchResults_Search_Btn);
            test.validateElementIsPresent(driver, Elements.GlobalSearchResults_SearchResultList);
            test.validateElementIsPresent(driver, By.CssSelector("div[class='facet-filters facet-custom-filters']>div"));
            test.validateCountIsEqual(driver, 10, util.GetCount(driver, By.CssSelector("div[class='search-results-item']")));
            test.validateElementIsPresent(driver, By.CssSelector("div[class='search-results-items'] * img"));
            test.validateElementIsPresent(driver, By.CssSelector("div[class='search-results-items'] * h4>a"));
            string CompTitle = util.GetText(driver, By.CssSelector("div[class='page-title title-vertical-top40p title-vertical-bottom30p']>h1"));
            action.IClick(driver, By.CssSelector("div[class='search-results-items'] * h4>a"));
            test.validateStringInstance(driver, driver.Title, CompTitle);
        }

        [Test, Category("Alliance"), Category("Core")]
        [TestCase("en", TestName = "Verify No Matched result in EN Locale")]
        [TestCase("cn", TestName = "Verify No Matched result in CN Locale")]
        [TestCase("jp", TestName = "Verify No Matched result in JP Locale")]
        [TestCase("ru", TestName = "Verify No Matched result in RU Locale")]
        public void Alliance_SearchResultPage_NoMatched(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/about-adi/alliances/search.html?q=*");
            action.IDeleteValueOnFields(driver, Elements.GlobalSearchResults_Search_Txtbox);
            action.IType(driver, Elements.GlobalSearchResults_Search_Txtbox, "aaaaaa");
            action.IClick(driver, Elements.GlobalSearchResults_Search_Btn);
            test.validateElementIsPresent(driver, Elements.GlobalSearchResults_InvalidSearchResults_Txt_Line1);
            test.validateElementIsNotPresent(driver, By.CssSelector("div[class='facet-filters facet-custom-filters']>div"));
        }
    }

}
