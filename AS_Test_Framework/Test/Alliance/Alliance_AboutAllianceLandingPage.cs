﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Linq;

namespace AS_Test_Framework.Alliance
{

    [TestFixture]
    public class Alliance_AboutAllianceLandingPage : BaseSetUp
    {
        public Alliance_AboutAllianceLandingPage() : base() { }

        [Test, Category("Alliance"), Category("Core")]
        [TestCase("en", TestName = "Verify the About Alliance Hero Image for EN Locale")]
        //[TestCase("cn", TestName = "Verify the About Alliance Hero Image for CN Locale")]
        //[TestCase("jp", TestName = "Verify the About Alliance Hero Image for JP Locale")]
        //[TestCase("ru", TestName = "Verify the About Alliance Hero Image for RU Locale")]
        public void Alliance_LandingPage_HeroImage(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/about-adi/alliances/about-alliances.html");
            test.validateElementIsPresent(driver, By.CssSelector("img[title='About Alliances']"));
        }

        [Test, Category("Alliance"), Category("Core")]
        [TestCase("en", "About Alliances", TestName = "Verify About Alliances title is displayed for EN Locale")]
        //[TestCase("cn", "关于战略联盟", TestName = "Verify About Alliances title is displayed for CN Locale")]
        //[TestCase("jp", "アライアンスについて", TestName = "Verify About Alliances title is displayed for JP Locale")]
        //[TestCase("ru", "О программе Alliances", TestName = "Verify About Alliances title is displayed for RU Locale")]
        public void Alliance_LandingPage_Title(string Locale, string Title)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/about-adi/alliances/about-alliances.html");
            test.validateString(driver, Title, util.GetText(driver, By.CssSelector("div[class='row fca-row one-columnspot top-spacing-16 ignore-bottom-spacing '] * span>strong")));
        }

        //[Test, Category("Alliance"), Category("Core")]
        //[TestCase("en", "Members Lounge", TestName = "Verify About Alliance Member Lounge Section is present for EN Locale")]
        ////[TestCase("cn", "会员社区", TestName = "Verify About Alliance Member Lounge Section is present for CN Locale")]
        ////[TestCase("jp", "メンバーズ・ラウンジ", TestName = "Verify About Alliance Member Lounge Section is present for JP Locale")]
        ////[TestCase("ru", "Зал для участников", TestName = "Verify About Alliance Member Lounge Section is present for RU Locale")]
        //public void Alliance_LandingPage_MembersLoungeSection(string Locale, string Title)
        //{
        //    action.Navigate(driver, Configuration.Env_Url + Locale + "/about-adi/alliances/about-alliances.html");
        //    test.validateString(driver, Title, util.GetText(driver, By.CssSelector("div[class='row fca-row one-columnspot   ']:nth-of-type(3) * span>strong")));
        //    test.validateElementIsPresent(driver, By.CssSelector("div[class='row fca-row one-columnspot   ']:nth-of-type(3) * p[class='analog-rounded-button']>a"));
        //    action.IClick(driver, By.CssSelector("div[class='row fca-row one-columnspot   ']:nth-of-type(3) * p[class='analog-rounded-button']>a"));
        //    test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/about-adi/alliances/about-members-lounge.html");
        //}

        [Test, Category("Alliance"), Category("Core")]
        [TestCase("en", TestName = "Verify Accordion is present and working as expected")]
        public void Alliance_LandingPage_Accordion(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/about-adi/alliances/about-alliances.html");
            test.validateElementIsPresent(driver, By.CssSelector("section[id='faqs-sectionband'] * div[class='accordion-expand-all']>a[class='expand']"));
            test.validateElementIsPresent(driver, By.CssSelector("div[class='container product-content campaign '] * div[class='adi-accordion ']"));
            action.IClick(driver, By.CssSelector("section[id='faqs-sectionband'] * div[class='accordion-expand-all']>a[class='expand']"));
            test.validateElementIsPresent(driver, By.CssSelector("div[class='container product-content campaign '] * div[class='adi-accordion  expand-all']"));
            test.validateElementIsPresent(driver, By.CssSelector("section[id='faqs-sectionband'] * div[class='accordion-expand-all']>a[class='collapse']"));
        }

        [Test, Category("Alliance"), Category("Core")]
        [TestCase("en", TestName = "Verify Download Complete FAQ is present and working as expected")]
        public void Alliance_LandingPage_FAQ_Btn(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/about-adi/alliances/about-alliances.html");
            test.validateStringIsCorrect(driver, By.CssSelector("div[class='clearfix rte']>p:nth-child(1)[class='analog-rounded-button']>a"), "Download Complete FAQ");
        //    action.IClick(driver, By.CssSelector(" div[class='clearfix rte']>p:nth-child(1)[class='analog-rounded-button']>a"));
        //    driver.SwitchTo().Window(driver.WindowHandles.Last());
        //    test.validateStringInstance(driver, driver.Url, "Other/About-ADI/Alliances/Alliances-FAQ.pdf");
        }       
    }
}
