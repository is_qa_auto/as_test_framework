﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.Alliance
{

    [TestFixture]
    public class Alliance_LandingPage_AboutAlliance_OnlineApp : BaseSetUp
    {
        public Alliance_LandingPage_AboutAlliance_OnlineApp() : base() { }

        [Test, Category("Alliance"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify online application link when the user is logged-out")]
        public void Alliance_LandingPage_AboutAlliance_OnlineApp1(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/about-adi/alliances.html");
            action.IClick(driver, By.CssSelector("div[class='second-tilecom']>p:nth-of-type(6)>a"));
            driver.SwitchTo().Window(driver.WindowHandles.Last());
            Thread.Sleep(1000);
            test.validateStringInstance(driver, driver.Url, "b2clogin.com");
        }

        [Test, Category("Alliance"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify online application link when the user is logged-in")]
        public void Alliance_LandingPage_AboutAlliance_OnlineApp2(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/about-adi/alliances.html");
            action.ILoginViaMyAnalogMegaMenu(driver, "aries.sorosoro@analog.com", "Test_1234");
            action.IClick(driver, By.CssSelector("div[class='second-tilecom']>p:nth-of-type(6)>a"));
            driver.SwitchTo().Window(driver.WindowHandles.Last());
            Thread.Sleep(1000);
            test.validateStringInstance(driver, driver.Url, Configuration.Env_Url.Replace("www", "alliances").Replace("cldnet", "corpnt") + "member/application/signup");
        }

        [Test, Category("Alliance"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify online application link when the user is logged-in and have already submitted an application")]
        public void Alliance_LandingPage_AboutAlliance_OnlineApp3(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/about-adi/alliances.html");
            action.ILoginViaMyAnalogMegaMenu(driver, "alliance_qa_auto@mailinator.com", "Test_1234");
            action.IClick(driver, By.CssSelector("div[class='second-tilecom']>p:nth-of-type(6)>a"));
            driver.SwitchTo().Window(driver.WindowHandles.Last());
            Thread.Sleep(1000);
            test.validateString(driver, "You have already submitted your Application. Please contact alliances@analog.com if you have any questions.", util.GetText(driver, Elements.Alliance_Application_Modal));
        }
    }
}
