﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Threading;
using System.Linq;
using System;

namespace AS_Test_Framework.Alliance
{

    [TestFixture]
    public class Alliance_ContactAlliancesForms : BaseSetUp
    {   
        public Alliance_ContactAlliancesForms() : base() { }

        string CompanyName = null;

        string FiveHundredOneChar = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaatest";

        [Test, Category("Alliance"), Category("Non-Core")]
        [TestCase(TestName = "Verify Save Time By Logging In Section")]
        public void Alliance_ContactAlliance_SaveTimeByLoggingIn()
        {
            action.Navigate(driver, Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + "form_pages/alliances/contact.aspx");
            test.validateElementIsPresent(driver, By.CssSelector("div[class='panel panel-primary']>div[class='panel-collapse in']"));
            action.IClick(driver, Elements.Forms_SaveTimeByLoggingIn_Header);
            test.validateElementIsNotPresent(driver, By.CssSelector("div[class='panel panel-primary']>div[id='collapseOne']"));
            action.IClick(driver, Elements.Forms_SaveTimeByLoggingIn_Header);
            test.validateElementIsPresent(driver, By.CssSelector("div[class='panel panel-primary']>div[id='collapseOne']"));
        }

        [Test, Category("Alliance"), Category("Non-Core")]
        [TestCase(TestName = "Verify the form when the user is logged-in")]
        public void Alliance_ContactAlliance_PrePopulatedFields()
        {
            action.Navigate(driver, Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + "form_pages/alliances/contact.aspx");
            action.IClick(driver, Elements.Forms_SaveTimeByLoggingIn_LoginBtn);
            action.ILogin(driver, "aries.sorosoro@analog.com", "Test_1234");
            test.validateFieldIsNotEmpty(driver, Elements.Forms_Email_TxtField);
            test.validateFieldIsNotEmpty(driver, Elements.Forms_OrgName_TxtField);
            test.validateFieldIsNotEmpty(driver, Elements.Forms_FirstName_TxtField);
            test.validateFieldIsNotEmpty(driver, Elements.Forms_LastName_TxtField);
            test.validateFieldIsNotEmpty(driver, Elements.Forms_TelPhone_TxtField);
            test.validateSelectedValueIsCorrect(driver, Elements.Forms_ContactAlliance_CountryDD, "UNITED STATES");
        }

        [Test, Category("Alliance"), Category("Non-Core")]
        [TestCase(TestName = "Verify Contact Alliances Fields")]
        public void Alliance_ContactAlliance_FieldValidations()
        {
            action.Navigate(driver, Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + "form_pages/alliances/contact.aspx");
            /*****Type Of Support****/
            test.validateStringIsCorrect(driver, Elements.Forms_ContactAlliance_TypeOfSupp_Label, "*What type of support do you need?");
            action.IClick(driver, Elements.Forms_ContactAlliance_TypeOfSupp_RadBtn);
            test.validateStringInstance(driver, util.ReturnAttribute(driver, By.CssSelector("span[id='_content_rfvSupport']"), "style"), "display: inline;");

            /*****Email***/
            test.validateStringIsCorrect(driver, Elements.Forms_Email_Label, "*Email Id");
            action.IType(driver, Elements.Forms_Email_TxtField, "Test_Aries");
            action.IType(driver, Elements.Forms_Email_TxtField, Keys.Tab);
            test.validateStringIsCorrect(driver, By.CssSelector("span[id='_content_revEmail']"), "Please enter a valid email address");
            action.IDeleteValueOnFields(driver, Elements.Forms_Email_TxtField);
            action.IType(driver, Elements.Forms_Email_TxtField, Keys.Tab);
            test.validateStringIsCorrect(driver, By.CssSelector("span[id='_content_rfvEmail']"), "Email address is required");

            /*****Organization***/
            test.validateStringIsCorrect(driver, Elements.Forms_OrgName_Label, "*Organization Name");
            action.IType(driver, Elements.Forms_OrgName_TxtField, "AAA");
            action.IDeleteValueOnFields(driver, Elements.Forms_OrgName_TxtField);
            action.IType(driver, Elements.Forms_OrgName_TxtField, Keys.Tab);
            test.validateStringIsCorrect(driver, By.CssSelector("span[id='_content_rfvOrganizationName']"), "Organization Name is required");

            /*****First Name***/
            test.validateStringIsCorrect(driver, Elements.Forms_FirstName_Label, "*First Name");
            action.IType(driver, Elements.Forms_FirstName_TxtField, "AAA");
            action.IDeleteValueOnFields(driver, Elements.Forms_FirstName_TxtField);
            action.IType(driver, Elements.Forms_FirstName_TxtField, Keys.Tab);
            test.validateStringIsCorrect(driver, By.CssSelector("span[id='_content_rfvFirstName']"), "First Name is required");

            /*****Last Name***/
            test.validateStringIsCorrect(driver, Elements.Forms_LastName_Label, "Last Name");

            /*****Telephone***/
            test.validateStringIsCorrect(driver, Elements.Forms_TelPhone_Label, "Telephone");

            /*****Country****/
            test.validateStringIsCorrect(driver, Elements.Forms_Country_Label, "*Country/Region");
            action.ISelectFromDropdown(driver, Elements.Forms_ContactAlliance_CountryDD, "US");
            action.ISelectFromDropdownByStringText(driver, Elements.Forms_ContactAlliance_CountryDD, "Select Country/Region");
            test.validateStringIsCorrect(driver, By.CssSelector("span[id='_content_RequiredFieldValidator1']"), "Country/Region is required");

            /****Comments****/
            test.validateStringIsCorrect(driver, Elements.Forms_Comments_Label, "*Please write your questions or comments here");
            test.validateStringIsCorrect(driver, By.CssSelector("span[id='_content_Label1']"), "(Maximum characters allowed: 500)");
            action.IType(driver, Elements.Forms_Comments_TxtField, "AAA");
            action.IDeleteValueOnFields(driver, Elements.Forms_Comments_TxtField);
            action.IType(driver, Elements.Forms_Comments_TxtField, Keys.Tab);
            test.validateStringIsCorrect(driver, By.CssSelector("span[id='_content_rfvComments']"), "Question / Comment is required");
            action.IType(driver, Elements.Forms_Comments_TxtField, " ");
            action.IType(driver, Elements.Forms_Comments_TxtField, Keys.Tab);
            test.validateStringIsCorrect(driver, By.CssSelector("span[id='_content_rfvComments']"), "Question / Comment is required");
            action.IType(driver, Elements.Forms_Comments_TxtField, FiveHundredOneChar);
            action.IType(driver, Elements.Forms_Comments_TxtField, Keys.Tab);
            test.validateStringIsCorrect(driver, By.CssSelector("span[id='_content_revComments']"), "Maximum 500 characters allowed.");

        }
    }
}
