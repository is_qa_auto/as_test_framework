﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;

namespace AS_Test_Framework.Alliance
{

    [TestFixture]
    public class Alliance_LandingPage_PartnerTile : BaseSetUp
    {
        public Alliance_LandingPage_PartnerTile() : base() { }

        [Test, Category("Alliance"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that Image for Find Partner Tile is present for EN Locale")]
        [TestCase("cn", TestName = "Verify that Image for Find Partner Tile is present for for CN Locale")]
        [TestCase("jp", TestName = "Verify that Image for Find Partner Tile is present for JP Locale")]
        [TestCase("ru", TestName = "Verify that Image for Find Partner Tile is present for RU Locale")]
        public void Alliance_LandingPage_FindPartnerImage(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/about-adi/alliances.html");
            test.validateElementIsPresent(driver, Elements.Alliance_LandingPage_FindPartner_Img);
        }

        [Test, Category("Alliance"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that Partner Search button in Find Partner Tile is present and working for EN Locale")]
        [TestCase("cn", TestName = "Verify that Partner Search button in Find Partner Tile is present and working for CN Locale")]
        [TestCase("jp", TestName = "Verify that Partner Search button in Find Partner Tile is present and working for JP Locale")]
        [TestCase("ru", TestName = "Verify that Partner Search button in Find Partner Tile is present and working for RU Locale")]
        public void Alliance_LandingPage_PartnerSearch_Btn(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/about-adi/alliances.html");
            test.validateElementIsPresent(driver, Elements.Alliance_LandingPage_FindPartner_Btn);
            action.IClick(driver, Elements.Alliance_LandingPage_FindPartner_Btn);
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/about-adi/alliances/search.html?q=*");

        }

    }

}
