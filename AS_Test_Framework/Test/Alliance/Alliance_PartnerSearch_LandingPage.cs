﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;

namespace AS_Test_Framework.Alliance
{

    [TestFixture]
    public class Alliance_PartnerSearch_LandingPage : BaseSetUp
    {
        public Alliance_PartnerSearch_LandingPage() : base() { }

        [Test, Category("Alliance"), Category("Core")]
        [TestCase("en", TestName = "Verify the Company Details is present in Partner Page for EN Lcoale")]
        [TestCase("cn", TestName = "Verify the Company Details is present in Partner Page for CN Lcoale")]
        [TestCase("jp", TestName = "Verify the Company Details is present in Partner Page for JP Lcoale")]
        [TestCase("ru", TestName = "Verify the Company Details is present in Partner Page for RU Lcoale")]
        public void Alliance_PartnerSearch_CompanyDetail(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/about-adi/alliances/partner/trenz-electronic-gmbh.html");
            test.validateElementIsPresent(driver, By.CssSelector("div[class='page-title title-vertical-top40p title-vertical-bottom30p']>h1"));
            test.validateElementIsPresent(driver, Elements.Alliance_PartnerPage_CompanyLogo);
            test.validateElementIsPresent(driver, Elements.Alliance_PartnerPage_CompanySite);
            test.validateElementIsPresent(driver, Elements.Alliance_PartnerPage_CompanySite);
        }

        [Test, Category("Alliance"), Category("Core")]
        [TestCase("en", "Overview",  TestName = "Verify the Tabs are present and working in Partner Page for EN Lcoale")]
        [TestCase("cn", "概述", TestName = "Verify the Tabs are present and working in Partner Page for CN Lcoale")]
        [TestCase("jp", "会社概要",  TestName = "Verify the Tabs are present and working in Partner Page for JP Lcoale")]
        [TestCase("ru", "Обзор", TestName = "Verify the Tabs are present and working in Partner Page for RU Lcoale")]
        public void Alliance_PartnerSearch_Tabs(string Locale, string DefaultTab)
        {
            string[] TabsSelect_En = { "Contact", "Capabilities" };
            string[] TabsSelect_Cn = { "联系方式", "优势", };
            string[] TabsSelect_Jp = { "連絡先", "保有技術", };
            string[] TabsSelect_Ru = { "Контакты", "Ресурсы"};

            action.Navigate(driver, Configuration.Env_Url + Locale + "/about-adi/alliances/partner/trenz-electronic-gmbh.html");
            test.validateStringIsCorrect(driver, By.CssSelector("ul[class='nav nav-tabs']>li[class='active']>a"), DefaultTab);
            for (int x = 2, y = 0; util.CheckElement(driver, By.CssSelector("ul[class='nav nav-tabs']>li:nth-child(" + x + ")"), 1); x++,y++)
            {
                action.IClick(driver, By.CssSelector("ul[class='nav nav-tabs']>li:nth-child("+x+")>a"));
                if (Locale.Equals("en"))
                { 
                    test.validateStringIsCorrect(driver, By.CssSelector("ul[class='nav nav-tabs']>li[class='active']>a"), TabsSelect_En[y]);
                }
                else if (Locale.Equals("cn"))
                {
                    test.validateStringIsCorrect(driver, By.CssSelector("ul[class='nav nav-tabs']>li[class='active']>a"), TabsSelect_Cn[y]);
                }
                else if (Locale.Equals("jp"))
                {
                    test.validateStringIsCorrect(driver, By.CssSelector("ul[class='nav nav-tabs']>li[class='active']>a"), TabsSelect_Jp[y]);
                }
                else
                {
                    test.validateStringIsCorrect(driver, By.CssSelector("ul[class='nav nav-tabs']>li[class='active']>a"), TabsSelect_Ru[y]);
                }
            }
        }

    }

}
