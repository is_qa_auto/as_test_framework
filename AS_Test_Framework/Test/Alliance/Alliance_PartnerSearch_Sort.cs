﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;

namespace AS_Test_Framework.Alliance
{

    [TestFixture]
    public class Alliance_PartnerSearch_Sort : BaseSetUp
    {
        public Alliance_PartnerSearch_Sort() : base() { }

        [Test, Category("Alliance"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Sort menu is present on the page and working as expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Sort menu is present on the page and working as expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Sort menu is present on the page and working as expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Sort menu is present on the page and working as expected in RU Locale")]
        public void Alliance_SearchResultPage_Sort(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/about-adi/alliances/search.html?q=*");
            test.validateElementIsPresent(driver, Elements.GlobalSearchResults_Sort_Menu);
            test.validateSelectedValueIsCorrect(driver, Elements.GlobalSearchResults_Sort_Menu, "relevancy");

        }
    }

}
