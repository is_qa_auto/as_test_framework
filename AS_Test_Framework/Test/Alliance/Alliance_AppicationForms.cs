﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Threading;
using System.Linq;

namespace AS_Test_Framework.Alliance
{

    [TestFixture]
    public class Alliance_ApplicationForms : BaseSetUp
    {   
        public Alliance_ApplicationForms() : base() { }

        string FiveHundredOneChar = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaatest";

        string Email_Approved = null;

        [Test, Category("Alliance"), Category("Non-Core")]
        [TestCase("en", TestName = "Verify alliance application form fields")]
        public void Alliance_ApplicationForms_FieldValidations(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/about-adi/alliances/about-members-lounge.html");
            action.ILoginViaMyAnalogMegaMenu(driver, "aries.sorosoro@analog.com", "Test_1234");
            action.IClick(driver, By.CssSelector("a[class='btn btn-primary btn-lg']"));
            driver.SwitchTo().Window(driver.WindowHandles.Last());
            //test.validateString(driver, "readonly", util.ReturnAttribute(driver, Elements.Alliance_ApplicationForm_SubmitBtn, "readonly"));
            test.validateFieldIsNotEmpty(driver, Elements.Alliance_ApplicationForm_CompName);
            test.validateFieldIsNotEmpty(driver, Elements.Alliance_ApplicationForm_Fname);
            test.validateFieldIsNotEmpty(driver, Elements.Alliance_ApplicationForm_Lname);
            test.validateFieldIsNotEmpty(driver, Elements.Alliance_ApplicationForm_Email);
            action.IDeleteValueOnFields(driver, Elements.Alliance_ApplicationForm_CompName);
            action.IDeleteValueOnFields(driver, Elements.Alliance_ApplicationForm_Fname);
            action.IDeleteValueOnFields(driver, Elements.Alliance_ApplicationForm_Phone);
            action.IType(driver, Elements.Alliance_ApplicationForm_Phone, Keys.Tab);
            test.validateString(driver, "Company Name is required", util.GetText(driver, By.CssSelector("div[class='form-container clearfix']>div:nth-child(2) * ul>li")));
            test.validateString(driver, "First Name is required", util.GetText(driver, By.CssSelector("div[class='form-container clearfix']>div:nth-child(3) * ul>li")));
            test.validateString(driver, "Phone is required", util.GetText(driver, By.CssSelector("div[class='form-container clearfix']>div:nth-child(4) * ul>li")));
            test.validateString(driver, "Please complete all required fields (marked *)", util.GetText(driver, By.CssSelector("div[class='form-group text-center']>span")));
            test.validateElementIsPresent(driver, By.CssSelector("button[class='btn btn-primary btn-lg disabled']"));

            action.IType(driver, Elements.Alliance_ApplicationForm_CompValue, FiveHundredOneChar);
            test.validateString(driver, "0 characters remaining.", util.GetText(driver, By.CssSelector("div[class='form-group has-feedback extra-spaing-right margin-bottom-application-form']>div>div[class='help-block']")));
            test.validateCountIsEqual(driver, 500, util.ReturnAttribute(driver, Elements.Alliance_ApplicationForm_CompValue, "value").Length);

            action.IType(driver, Elements.Alliance_ApplicationForm_PartnershipValue, FiveHundredOneChar);
            test.validateString(driver, "0 characters remaining.", util.GetText(driver, By.CssSelector("div[class='form-group has-feedback extra-spaing-right margin-bottom-application-form']>div>div[class='help-block']")));
            test.validateCountIsEqual(driver, 500, util.ReturnAttribute(driver, Elements.Alliance_ApplicationForm_PartnershipValue, "value").Length);

            action.IType(driver, Elements.Alliance_ApplicationForm_ActiveEngagement, FiveHundredOneChar);
            test.validateString(driver, "0 characters remaining.", util.GetText(driver, By.CssSelector("div[class='form-group has-feedback extra-spaing-right margin-bottom-application-form']>div>div[class='help-block']")));
            test.validateCountIsEqual(driver, 500, util.ReturnAttribute(driver, Elements.Alliance_ApplicationForm_ActiveEngagement, "value").Length);

            test.validateElementIsPresent(driver, Elements.Alliance_ApplicationForm_SalesRep_Fname);
            test.validateElementIsPresent(driver, Elements.Alliance_ApplicationForm_SalesRep_Lname);
            test.validateElementIsPresent(driver, Elements.Alliance_ApplicationForm_Sponsor_Fname);
            test.validateElementIsPresent(driver, Elements.Alliance_ApplicationForm_Sponsor_Lname);
        }

        [Test, Category("Alliance"), Category("Non-Core")]
        [TestCase("en", TestName = "Verify successful submission of forms")]
        public void Alliance_ApplicationForms_SuccessfulSubmit(string Locale) {
            Email_Approved = util.Generate_EmailAddress();
            action.Navigate(driver, Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/app/registration");
            action.ICreateNewUser(driver, Email_Approved, "Test", "Approve", "Test_1234", "Test_City", "12345", "Analog");
            action.Navigate(driver, Configuration.Env_Url + Locale + "/about-adi/alliances/about-members-lounge.html");
            action.ILoginViaMyAnalogMegaMenu(driver, Email_Approved, "Test_1234");
            action.IClick(driver, By.CssSelector("a[class='btn btn-primary btn-lg']"));
            driver.SwitchTo().Window(driver.WindowHandles.Last());
            action.IType(driver, Elements.Alliance_ApplicationForm_Phone,"123456789");
            action.IClick(driver, Elements.Alliance_ApplicationForm_SubmitBtn);
            Thread.Sleep(3000);
            test.validateString(driver, "You have successfully submitted your application", util.GetText(driver, By.CssSelector("div[id='saveApplication'] * div[class='modal-body']>p[id='modalHtmlBody']")));
            action.IClick(driver, By.CssSelector("div[id='saveApplication'] * div[class='modal-footer']>button"));
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/about-adi/alliances.html");
        }

        [Test, Category("Alliance"), Category("Non-Core")]
        [TestCase(TestName = "Verify successfully submitted application is available in Webcontent and user can reject the application")]
        public void Alliance_ApplicationForms_WebContentValidation1()
        {
            if (Configuration.Environment.Equals("production"))
            {
                action.Navigate(driver, Configuration.Env_Url.Replace("www", "webcontent.corpnt") + "AlliancesAdmin/ManageAlliancesApplication.aspx");
            }
            else
            { 
                action.Navigate(driver, Configuration.Env_Url.Replace("www", "webcontent").Replace("cldnet", "corpnt") + "AlliancesAdmin/ManageAlliancesApplication.aspx");
            }

            action.IType(driver, Elements.WebContent_Email_TextField, Email_Approved);
            action.IClick(driver, Elements.WebContent_Search_Btn);
            test.validateString(driver, Email_Approved, util.GetText(driver, By.CssSelector("table[id='_ctl0__content_grdAllianceapplication'] * tr:nth-child(2)>td:nth-child(9)")));
            action.IClick(driver, By.CssSelector("table[id='_ctl0__content_grdAllianceapplication'] * tr:nth-child(2)>td>input[id='_ctl0__content_grdAllianceapplication__ctl2_cbStatus']"));
            action.IClick(driver, Elements.WebContent_Reject_Btn);
            test.validateString(driver, "Application is rejected successfully, however domain was not removed from gated content  because there is another approved application for same domain", util.GetAlertMessageThenAccept(driver));
            action.IClick(driver, Elements.WebContent_Search_Btn);
            test.validateString(driver, "Rejected", util.GetText(driver, By.CssSelector("table[id='_ctl0__content_grdAllianceapplication'] * tr:nth-child(2)>td:nth-child(5)")));
        }
    }
}
