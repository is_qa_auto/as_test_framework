﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;

namespace AS_Test_Framework.Alliance
{

    [TestFixture]
    public class Alliance_MembersLoungeLandingPage : BaseSetUp
    {
        public Alliance_MembersLoungeLandingPage() : base() { }

        [Test, Category("Alliance"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify the Hero Image for EN Locale")]
        [TestCase("cn", TestName = "Verify the Hero Image for CN Locale")]
        [TestCase("jp", TestName = "Verify the Hero Image for JP Locale")]
        [TestCase("ru", TestName = "Verify the Hero Image for RU Locale")]
        public void Alliance_MemberLounge_LandingPage_HeroImage(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/about-adi/alliances/about-members-lounge.html");
            test.validateElementIsPresent(driver, Elements.Alliance_MembersLounge_LandingPage_HeroImage);
        }

        [Test, Category("Alliance"), Category("Core"), Retry(2)]
        [TestCase("en", "About the Members Lounge", TestName = "Verify About members lounge title is displayed for EN Locale")]
        [TestCase("cn", "关于会员专属空间", TestName = "Verify About members lounge title is displayed for CN Locale")]
        [TestCase("jp", "メンバーズ・ラウンジについて", TestName = "Verify About members lounge title is displayed for JP Locale")]
        [TestCase("ru", "About the Members Lounge", TestName = "Verify About members lounge title is displayed for RU Locale")]
        public void Alliance_MemberLounge_LandingPage_Title(string Locale, string Title)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/about-adi/alliances/about-members-lounge.html");
            test.validateString(driver, Title, util.GetText(driver, By.CssSelector("div[class='clearfix rte']>p>span>strong")));
        }


        [Test, Category("Alliance"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify \"See All Alliances Benefits\" button is present and working as expected")]
        public void Alliance_MemberLounge_SeeAllBenefits(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/about-adi/alliances/about-members-lounge.html");
            test.validateElementIsPresent(driver, By.CssSelector("div[class='col-md-6  RTEColBorder']>span>a"));
            action.IClick(driver, By.CssSelector("div[class='col-md-6  RTEColBorder']>span>a"));
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/about-adi/alliances/about-alliances.html");
        }



    }

}
