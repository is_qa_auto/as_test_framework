﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.DesignCenter
{
    [TestFixture]
    public class DesignCenter_CircuitDesignToolsAndCalculators : BaseSetUp
    {
        public DesignCenter_CircuitDesignToolsAndCalculators() : base() { }

        //--- URLs ---//
        string circuitDesignToolsAndCalculators_page_url = "/design-center/design-tools-and-calculators.html";
        string amplifierAndLinearTools_page_url = "/amplifier-and-linear-tools.html";
        string clockAndTimingTools_page_url = "/clock-and-timing-tools.html";
        string dataConverterTools_page_url = "/converter-tools.html";
        string ltSpice_page_url = "/ltspice-simulator.html";
        string powerManagementTools_page_url = "/power-management-tools.html";
        string rfAndSynthesisTools_page_url = "/rf-and-synthesis-tools.html";

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that Circuit Design Tools & Calculators is Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that Circuit Design Tools & Calculators is Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that Circuit Design Tools & Calculators is Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that Circuit Design Tools & Calculators is Working as Expected in RU Locale")]
        public void DesignCenter_VerifyCircuitDesignToolsAndCalculators(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + circuitDesignToolsAndCalculators_page_url);

            //----- Design Center - Symbols and Footprints TestPlan > R2 > T1: Select all items in the "Left Rail Navigation" section of the page. -----//

            //--- Expected Result: page Header should be displayed. ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_CircuitDesignToolsAndCalculators_Page_Title_Txt);

            //----- Design Center - Design Tools and Calculators TestPlan > R1 > T1: Validate Design Center dropdown if Design Tools and Calculators is listed. -----//

            //--- Expected Result: Design Tools And Calculator is listed. ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_CircuitDesignToolsAndCalculators_Accordions);

            //----- Design Center - Design Tools and Calculators TestPlan > R1 > T8: Validate Spice Section (AL-11289/IQ-4622) -----//

            if (util.CheckElement(driver, Elements.DesignCenter_CircuitDesignToolsAndCalculators_Spice_Accordion_Title_Expand_Btn, 2))
            {
                //--- Action: Click the arrow to expand the accordions ---//
                action.IClick(driver, Elements.DesignCenter_CircuitDesignToolsAndCalculators_Spice_Accordion_Title_Expand_Btn);

                //--- Expected Result: The accordion will expand ---//
                test.validateElementIsPresent(driver, Elements.DesignCenter_CircuitDesignToolsAndCalculators_Expanded_Spice_Accordion);

                if (util.CheckElement(driver, Elements.DesignCenter_CircuitDesignToolsAndCalculators_Expanded_Spice_Accordion, 2))
                {
                    //--- Expected Result: Ltspice Image should be displayed ---//
                    test.validateElementIsPresent(driver, Elements.DesignCenter_CircuitDesignToolsAndCalculators_LtSpice_Img);

                    if (util.CheckElement(driver, Elements.DesignCenter_CircuitDesignToolsAndCalculators_LtSpice_Img, 2))
                    {
                        //--- Action: Click the Ltspice Image ---//
                        action.IOpenLinkInNewTab(driver, Elements.DesignCenter_CircuitDesignToolsAndCalculators_LtSpice_Img);
                        Thread.Sleep(1000);

                        //--- Expected Result: Upon clicking the Ltspice Image, it should redirect to the LTspice page ---//
                        test.validateStringInstance(driver, driver.Url, ltSpice_page_url);

                        driver.Close();
                        driver.SwitchTo().Window(driver.WindowHandles.First());
                    }

                    //--- Expected Result: A descirpton should be displayed. ---//
                    test.validateElementIsPresent(driver, Elements.DesignCenter_CircuitDesignToolsAndCalculators_LtSpice_Desc_Txt);

                    //--- Expected Result: Learn more Link should be displayed ---//
                    test.validateElementIsPresent(driver, Elements.DesignCenter_CircuitDesignToolsAndCalculators_LtSpice_LearnMore_Link);

                    if (util.CheckElement(driver, Elements.DesignCenter_CircuitDesignToolsAndCalculators_LtSpice_LearnMore_Link, 2))
                    {
                        //--- Action: Click the Learn more Link ---//
                        action.IOpenLinkInNewTab(driver, Elements.DesignCenter_CircuitDesignToolsAndCalculators_LtSpice_LearnMore_Link);
                        Thread.Sleep(1000);

                        //--- Expected Result: Learn more Link should redirect to LTspice page ---//
                        test.validateStringInstance(driver, driver.Url, ltSpice_page_url);
                    }
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.DesignCenter_CircuitDesignToolsAndCalculators_Spice_Accordion_Title_Expand_Btn);
            }
        }

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Left Navigation Panel is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Left Navigation Panel is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Left Navigation Panel is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Left Navigation Panel is Present and Working as Expected in RU Locale")]
        public void DesignCenter_CircuitDesignToolsAndCalculators_VerifyLeftNavigationPanel(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + circuitDesignToolsAndCalculators_page_url);

            //----- Design Center - Design Tools and Calculators TestPlan > R1 > T7: Validate Left navigation panel (AL-6229) -----//

            //--- Expected Result: The items are listed below Circuit Design Tools And Calculator ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_CircuitDesignToolsAndCalculators_LeftNav_Subsec_Links);

            if (util.CheckElement(driver, Elements.DesignCenter_CircuitDesignToolsAndCalculators_LeftNav_Subsec_Links, 2))
            {
                if (util.CheckElement(driver, Elements.DesignCenter_CircuitDesignToolsAndCalculators_LeftNav_AmplifierAndLinear_Link, 2))
                {
                    //--- Action: Click Amplifier and Linear Tools link ---//
                    action.IOpenLinkInNewTab(driver, Elements.DesignCenter_CircuitDesignToolsAndCalculators_LeftNav_AmplifierAndLinear_Link);
                    Thread.Sleep(1000);

                    //--- Expected Result: Amplifier and Linear Tools  landing page will be displayed. ---//
                    test.validateStringInstance(driver, driver.Url, amplifierAndLinearTools_page_url);

                    driver.Close();
                    driver.SwitchTo().Window(driver.WindowHandles.First());
                }

                if (util.CheckElement(driver, Elements.DesignCenter_CircuitDesignToolsAndCalculators_LeftNav_ClockAndTiming_Link, 2))
                {
                    //--- Action: Click Clock and Timing Tools link ---//
                    action.IOpenLinkInNewTab(driver, Elements.DesignCenter_CircuitDesignToolsAndCalculators_LeftNav_ClockAndTiming_Link);
                    Thread.Sleep(1000);

                    //--- Expected Result: Clock and Timing Tools  landing page will be displayed. ---//
                    test.validateStringInstance(driver, driver.Url, clockAndTimingTools_page_url);

                    driver.Close();
                    driver.SwitchTo().Window(driver.WindowHandles.First());
                }

                if (util.CheckElement(driver, Elements.DesignCenter_CircuitDesignToolsAndCalculators_LeftNav_DataConverter_Link, 2))
                {
                    //--- Action: Click Converter Tools link ---//
                    action.IOpenLinkInNewTab(driver, Elements.DesignCenter_CircuitDesignToolsAndCalculators_LeftNav_DataConverter_Link);
                    Thread.Sleep(2000);

                    //--- Expected Result: Converter Tools  landing page will be displayed. ---//
                    test.validateStringInstance(driver, driver.Url, dataConverterTools_page_url);

                    driver.Close();
                    driver.SwitchTo().Window(driver.WindowHandles.First());
                }

                if (util.CheckElement(driver, Elements.DesignCenter_CircuitDesignToolsAndCalculators_LeftNav_PowerManagement_Link, 2))
                {
                    //--- Action: Click Power Management Tools link ---//
                    action.IOpenLinkInNewTab(driver, Elements.DesignCenter_CircuitDesignToolsAndCalculators_LeftNav_PowerManagement_Link);
                    Thread.Sleep(1000);

                    //--- Expected Result: Power Management Tools  landing page will be displayed. ---//
                    test.validateStringInstance(driver, driver.Url, powerManagementTools_page_url);

                    driver.Close();
                    driver.SwitchTo().Window(driver.WindowHandles.First());
                }

                if (util.CheckElement(driver, Elements.DesignCenter_CircuitDesignToolsAndCalculators_LeftNav_RfAndSynthesis_Link, 2))
                {
                    //--- Action: Click RF and Synthesis Tools link ---//
                    action.IOpenLinkInNewTab(driver, Elements.DesignCenter_CircuitDesignToolsAndCalculators_LeftNav_RfAndSynthesis_Link);
                    Thread.Sleep(1000);

                    //--- Expected Result: RF and Synthesis Tools  landing page will be displayed. ---//
                    test.validateStringInstance(driver, driver.Url, rfAndSynthesisTools_page_url);
                }
            }
        }
    }
}