﻿using AS_Test_Framework.Util;
using NUnit.Framework;

namespace AS_Test_Framework.DesignCenter
{
    [TestFixture]
    public class DesignCenter_AdvancedSelectionAndDesignTools : BaseSetUp
    {
        public DesignCenter_AdvancedSelectionAndDesignTools() : base() { }

        //--- URLs ---//
        string advancedSelectionAndDesignTools_page_url = "/design-center/advanced-selection-and-design-tools.html";
        string circuitDesignToolsAndCalculators_page_url = "/design-tools-and-calculators.html";

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Advanced Selection and Design Tools URL is Redirecting to the Correct Page in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Advanced Selection and Design Tools URL is Redirecting to the Correct Page in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Advanced Selection and Design Tools URL is Redirecting to the Correct Page in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Advanced Selection and Design Tools URL is Redirecting to the Correct Page in RU Locale")]
        public void DesignCenter_VerifyRedirectionOfAdvancedSelectionAndDesignToolsUrl(string Locale)
        {
            //----- Design Center - Advanced Selection and Design Tools TestPlan > R1 > T2: Verify that Advanced Selection and Design Tools page using direct url -----//

            //--- Action: Access direct url ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + advancedSelectionAndDesignTools_page_url);

            //--- Expected Result: The page should be redirected to the Circuit Design Tools & Calculators Page  ---//
            test.validateStringInstance(driver, driver.Url, circuitDesignToolsAndCalculators_page_url);            
        }
    }
}