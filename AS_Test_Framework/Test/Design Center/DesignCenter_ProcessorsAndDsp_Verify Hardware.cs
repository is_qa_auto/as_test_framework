﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.DesignCenter
{
    [TestFixture]
    public class DesignCenter_ProcessorsAndDsp_Hardware : BaseSetUp
    {
        public DesignCenter_ProcessorsAndDsp_Hardware() : base() { }

        //--- URLs ---//
        string hardware_page_url = "/design-center/processors-and-dsp/evaluation-development-hardware.html";
        string home_page_url = "/index.html";
        string emulators_page_url = "/emulators.html";
        string designCenter_Search_page_url = "/design-center/search.html";
        string emulatorToolsUpgradeArchives_page_url = "/emulator-tools-upgrade-archives.html";
        string productEvaluationBoardsAndKits_page_url = "/evaluation-boards-kits.html";
        string evaluationEzExtenderBoards_page_url = "/evaluation-ez-extender-boards.html";

        //--- Labels ---//
        string emulators_txt = "Emulators";
        string evaluationBoardsAndKits_txt = "Evaluation Boards and Kits";
        string evaluationEzExtenderBoards_txt = "Evaluation EZ-Extender Boards";

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Breadcrumb is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Breadcrumb is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Breadcrumb is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Breadcrumb is Present and Working as Expected in RU Locale")]
        public void DesignCenter_ProcessorsAndDsp_Hardware_VerifyBreadcrumb(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + hardware_page_url);

            //----- Design Center - Processors and DSP TestPlan > R3 > T1: Verify the Hardware section -----//

            //--- Expected Result: The Hardware subcategory page should contain the Print widget ---//
            test.validateElementIsPresent(driver, Elements.Print_Widget_Btn);

            //--- Expected Result: The Hardware subcategory page should contain the My Analog widget ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Widget_Btn);

            //--- Expected Result: The Hardware subcategory page should contain the Breadcrumbs ---//
            test.validateElementIsPresent(driver, Elements.Breadcrumb_Details);

            if (util.CheckElement(driver, Elements.Breadcrumb_Details, 2))
            {
                //----- Design Center - Processors and DSP TestPlan > R3 > T2: Verify the breadcrumb -----//

                //--- Expected Result: The breadcrumb of the current  page should is just a label name of the page ---//
                test.validateStringIsCorrect(driver, Elements.Breadcrumb_CurrentPage_Txt, util.GetText(driver, Elements.DesignCenter_Hardware_Page_Title_Txt));

                if (util.CheckElement(driver, Elements.BreadCrumb_Home_Icon, 2))
                {
                    //--- Action: Click on the ADI Home Icon ---//
                    action.IOpenLinkInNewTab(driver, Elements.BreadCrumb_Home_Icon);
                    Thread.Sleep(2000);

                    //--- Expected Result: Home Page should be displayed ---//
                    test.validateStringInstance(driver, driver.Url, home_page_url);
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.BreadCrumb_Home_Icon);
                }
            }
        }

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that Hardware is Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that Hardware is Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that Hardware is Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that Hardware is Working as Expected in RU Locale")]
        public void DesignCenter_ProcessorsAndDsp_VerifyHardware(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + hardware_page_url);

            //----- Design Center - Processors and DSP TestPlan > R3 > T1: Verify the Hardware section -----//

            //--- Expected Result: The Hardware subcategory page should contain the subcategory name ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_Hardware_SubCat_Name_Txts);

            //--- Expected Result: The Hardware subcategory page should contain the description ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_Hardware_SubCat_Desc_Txts);

            //--- Expected Result: The Hardware subcategory page should contain the linked lists ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_Hardware_SubCat_ViewMore_Links);

            if (util.CheckElement(driver, Elements.DesignCenter_Hardware_SubCat_Secs, 2) && Locale.Equals("en"))
            {
                int hardware_subSec_count = util.GetCount(driver, Elements.DesignCenter_Hardware_SubCat_Secs);

                bool emulators_sec = false;
                bool evaluationBoardsAndKits_sec = false;
                bool evaluationEzExtenderBoards_sec = false;

                for (int hardware_ctr = 1; hardware_ctr <= hardware_subSec_count; hardware_ctr++)
                {
                    if (util.GetText(driver, By.CssSelector("div[name$='content_section']>div:nth-last-of-type(" + hardware_ctr + ") * h3[class*='header']")).Equals(emulators_txt))
                    {
                        //----- EMULATORS -----//

                        emulators_sec = true;

                        //--- Locators ----//
                        string subCategory_sec_locator = "div[name$='content_section']>div:nth-last-of-type(" + hardware_ctr + ")";
                        string viewMore_link_locator = "div[name$='content_section']>div:nth-last-of-type(" + hardware_ctr + ") * a[class$='link']";

                        //----- Design Center - Processors and DSP TestPlan > R3 > T5: Verify the Hardware subcategory linked lists -----//

                        //--- Expected Result: The Emulators linked lists should be available on Hardware subcategory page ---//
                        test.validateElementIsPresent(driver, By.CssSelector(subCategory_sec_locator));

                        if (util.CheckElement(driver, By.CssSelector(viewMore_link_locator), 2) && !Configuration.Browser.Equals("Chrome_HL"))
                        {
                            //----- Design Center - Processors and DSP TestPlan > R3 > T6: Verify the Emulators page -----//

                            //--- Action: Click on the View More link under the Emulators linkedlist ---//
                            action.IOpenLinkInNewTab(driver, By.CssSelector(viewMore_link_locator));

                            //--- Expected Result: The Emulators page should be displayed ---//
                            test.validateStringInstance(driver, driver.Url, emulators_page_url);

                            if (util.CheckElement(driver, Elements.DesignCenter_Emulators_EmulatorToolsUpgradeArchives_Link, 2))
                            {
                                //--- Action: Click on Emulator Tools Upgrade Archives ---//
                                action.IOpenLinkInNewTab(driver, Elements.DesignCenter_Emulators_EmulatorToolsUpgradeArchives_Link);

                                //--- Expected Result: Emulator Tools Upgrade Archives should be display. ---//
                                test.validateStringInstance(driver, driver.Url, emulatorToolsUpgradeArchives_page_url);

                                driver.Close();
                                driver.SwitchTo().Window(driver.WindowHandles.Last());
                            }
                            else
                            {
                                test.validateElementIsPresent(driver, Elements.DesignCenter_Emulators_EmulatorToolsUpgradeArchives_Link);
                            }

                            if (util.CheckElement(driver, Elements.DesignCenter_Emulators_EmulatorSearch_Txtbox, 2) && util.CheckElement(driver, Elements.DesignCenter_Emulators_EmulatorSearch_Btn, 2))
                            {
                                //--- Action: Search a keyword on the Search section ---//
                                string emulatorSearch_input = "Emulator";

                                action.IDeleteValueOnFields(driver, Elements.DesignCenter_Emulators_EmulatorSearch_Txtbox);
                                action.IType(driver, Elements.DesignCenter_Emulators_EmulatorSearch_Txtbox, emulatorSearch_input);

                                action.IClick(driver, Elements.DesignCenter_Emulators_EmulatorSearch_Btn);

                                //--- Expected Result: The Emulator Search results should be displayed ---//
                                test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + designCenter_Search_page_url);
                            }
                            else
                            {
                                test.validateElementIsPresent(driver, Elements.DesignCenter_Emulators_EmulatorSearch_Txtbox);
                                test.validateElementIsPresent(driver, Elements.DesignCenter_Emulators_EmulatorSearch_Btn);
                            }

                            driver.Close();
                            driver.SwitchTo().Window(driver.WindowHandles.First());
                        }
                        else
                        {
                            test.validateElementIsPresent(driver, By.CssSelector(viewMore_link_locator));
                        }
                    }

                    if (util.GetText(driver, By.CssSelector("div[name$='content_section']>div:nth-last-of-type(" + hardware_ctr + ") * h3[class*='header']")).Equals(evaluationEzExtenderBoards_txt))
                    {
                        //----- EVALUATION EZ-EXTENDER BOARDS -----//

                        evaluationEzExtenderBoards_sec = true;

                        //--- Locators ----//
                        string subCategory_sec_locator = "div[name$='content_section']>div:nth-last-of-type(" + hardware_ctr + ")";
                        string viewMore_link_locator = "div[name$='content_section']>div:nth-last-of-type(" + hardware_ctr + ") * a[class$='link']";

                        //----- Design Center - Processors and DSP TestPlan > R3 > T5: Verify the Hardware subcategory linked lists -----//

                        //--- Expected Result: The Evaluation EZ-Extender Boards linked lists should be available on Hardware subcategory page ---//
                        test.validateElementIsPresent(driver, By.CssSelector(subCategory_sec_locator));

                        if (util.CheckElement(driver, By.CssSelector(viewMore_link_locator), 2) && !Configuration.Browser.Equals("Chrome_HL"))
                        {
                            //----- Design Center - Processors and DSP TestPlan > R3 > T7: Verify the Evaluation EZ-Extender Boards page -----//

                            //--- Action: Click on the View More link under the Evaluation EZ-Extender Boards linkedlist ---//
                            action.IOpenLinkInNewTab(driver, By.CssSelector(viewMore_link_locator));

                            //--- Expected Result: The Evaluation EZ-Extender Boards page should be displayed ---//
                            test.validateStringInstance(driver, driver.Url, evaluationEzExtenderBoards_page_url);

                            driver.Close();
                            driver.SwitchTo().Window(driver.WindowHandles.First());
                        }
                        else
                        {
                            test.validateElementIsPresent(driver, By.CssSelector(viewMore_link_locator));
                        }
                    }

                    if (util.GetText(driver, By.CssSelector("div[name$='content_section']>div:nth-last-of-type(" + hardware_ctr + ") * h3[class*='header']")).Equals(evaluationBoardsAndKits_txt))
                    {
                        //----- EVALUATION BOARDS AND KITS -----//

                        evaluationBoardsAndKits_sec = true;

                        //--- Locators ----//
                        string subCategory_sec_locator = "div[name$='content_section']>div:nth-last-of-type(" + hardware_ctr + ")";
                        string viewMore_link_locator = "div[name$='content_section']>div:nth-last-of-type(" + hardware_ctr + ") * a[class$='link']";

                        //----- Design Center - Processors and DSP TestPlan > R3 > T5: Verify the Hardware subcategory linked lists -----//

                        //--- Expected Result: The Evaluation Kits and Boards linked lists should be available on Hardware subcategory page ---//
                        test.validateElementIsPresent(driver, By.CssSelector(subCategory_sec_locator));

                        if (util.CheckElement(driver, By.CssSelector(viewMore_link_locator), 2) && !Configuration.Browser.Equals("Chrome_HL"))
                        {
                            //----- Design Center - Processors and DSP TestPlan > R3 > T8: Verify the Evaluation Kits and Boards page -----//

                            //--- Action: Click on the View More link under the Evaluation Kits and Boards linkedlist ---//
                            action.IOpenLinkInNewTab(driver, By.CssSelector(viewMore_link_locator));

                            //--- Expected Result: The Evaluation Kits and Boards page should be displayed ---//
                            test.validateStringInstance(driver, driver.Url, productEvaluationBoardsAndKits_page_url);

                            driver.Close();
                            driver.SwitchTo().Window(driver.WindowHandles.First());
                        }
                        else
                        {
                            test.validateElementIsPresent(driver, By.CssSelector(viewMore_link_locator));
                        }
                    }

                    if (emulators_sec == true && evaluationBoardsAndKits_sec == true && evaluationEzExtenderBoards_sec == true)
                    {
                        break;
                    }
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.DesignCenter_Hardware_SubCat_Secs);
            }
        }
    }
}