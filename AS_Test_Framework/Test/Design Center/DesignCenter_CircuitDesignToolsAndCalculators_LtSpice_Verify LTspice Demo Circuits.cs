﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Threading;

namespace AS_Test_Framework.DesignCenter
{
    [TestFixture]
    public class DesignCenter_CircuitDesignToolsAndCalculators_LtSpice_LtSpiceDemoCircuits : BaseSetUp
    {
        public DesignCenter_CircuitDesignToolsAndCalculators_LtSpice_LtSpiceDemoCircuits() : base() { }

        //--- URLs ---//
        string ltSpiceDemoCircuits_page_url = "/design-center/design-tools-and-calculators/ltspice-simulator/lt-spice-demo-circuits.html";
        string pdp_url_format = "/products/";

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that LTspice Demo Circuits is Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that LTspice Demo Circuits is Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that LTspice Demo Circuits is Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that LTspice Demo Circuits is Working as Expected in RU Locale")]
        public void DesignCenter_CircuitDesignToolsAndCalculators_LtSpice_VerifyltSpiceDemoCircuits(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + ltSpiceDemoCircuits_page_url);

            ////----- R7 > T1: Verify Page Title (AL-10161) -----//

            ////--- Expected Result: The page title should be properly displayed and same label that was clicked on the category linked list ---//
            //test.validateStringIsCorrect(driver, Elements.DesignCenter_LtSpiceDemoCircuits_Page_Title_Txt, util.GetText(driver, Elements.DesignCenter_CircuitDesignToolsAndCalculators_LeftNav_SelectedVal));

            //----- R7 > T3: Verify LTspice Demo Circuit Model Files -----//

            //--- Expected Result: The Search box should be displayed on the table ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_LtSpiceDemoCircuits_Search_Txtbox);

            //--- Expected Result: The Product should be displayed on the table ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_LtSpiceDemoCircuits_Product_Search_Txtbox);

            //--- Expected Result: The Posted Date should be displayed on the table ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_LtSpiceDemoCircuits_PostedDate_Search_Txtbox);

            //--- Expected Result: The Model File should be displayed on the table ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_LtSpiceDemoCircuits_DemonstrationCircuit_Search_Txtbox);

            if (util.CheckElement(driver, Elements.DesignCenter_LtSpiceDemoCircuits_Tbl, 2))
            {
                //----- R7 > T4: Verify Ltspice Demo circuit Files Search Section -----//

                if (util.CheckElement(driver, Elements.DesignCenter_LtSpiceDemoCircuits_Search_Txtbox, 2))
                {
                    //--- Input ---//
                    string search_keyword = "LT";

                    //--- Action: Enter LT model number in search text field ---//
                    action.IDeleteValueOnFields(driver, Elements.DesignCenter_LtSpiceDemoCircuits_Search_Txtbox);
                    action.IType(driver, Elements.DesignCenter_LtSpiceDemoCircuits_Search_Txtbox, search_keyword);

                    //--- Expected Result: Search result should be displayed on the table ---//
                    test.validateElementIsPresent(driver, Elements.DesignCenter_LtSpiceDemoCircuits_Tbl_Search_Results);

                    action.IDeleteValueOnFields(driver, Elements.DesignCenter_LtSpiceDemoCircuits_Search_Txtbox);
                }

                if (util.CheckElement(driver, Elements.DesignCenter_LtSpiceDemoCircuits_Product_Search_Txtbox, 2))
                {
                    //--- Input ---//
                    string product_search_keyword = "LT";

                    //--- Action: Enter LT model number in product field ---//
                    action.IDeleteValueOnFields(driver, Elements.DesignCenter_LtSpiceDemoCircuits_Product_Search_Txtbox);
                    action.IType(driver, Elements.DesignCenter_LtSpiceDemoCircuits_Product_Search_Txtbox, product_search_keyword);

                    //--- Expected Result: Search result should be displayed on the table ---//
                    test.validateElementIsPresent(driver, Elements.DesignCenter_LtSpiceDemoCircuits_Tbl_Search_Results);

                    action.IDeleteValueOnFields(driver, Elements.DesignCenter_LtSpiceDemoCircuits_Product_Search_Txtbox);
                }

                if (util.CheckElement(driver, Elements.DesignCenter_LtSpiceDemoCircuits_PostedDate_Search_Txtbox, 2))
                {
                    //--- Input ---//
                    string desc_search_keyword = "2018";

                    //--- Action: Enter a valid date format in the posted date field ---//
                    action.IDeleteValueOnFields(driver, Elements.DesignCenter_LtSpiceDemoCircuits_PostedDate_Search_Txtbox);
                    action.IType(driver, Elements.DesignCenter_LtSpiceDemoCircuits_PostedDate_Search_Txtbox, desc_search_keyword);

                    //--- Expected Result: All LT number with searched date should be filtered ---//
                    test.validateElementIsPresent(driver, Elements.DesignCenter_LtSpiceDemoCircuits_Tbl_Search_Results);

                    action.IDeleteValueOnFields(driver, Elements.DesignCenter_LtSpiceDemoCircuits_PostedDate_Search_Txtbox);
                }

                if (util.CheckElement(driver, Elements.DesignCenter_LtSpiceDemoCircuits_DemonstrationCircuit_Search_Txtbox, 2))
                {
                    //--- Input ---//
                    string modelFile_search_keyword = "LT";

                    //--- Action: Enter LT model number in model file field ---//
                    action.IDeleteValueOnFields(driver, Elements.DesignCenter_LtSpiceDemoCircuits_DemonstrationCircuit_Search_Txtbox);
                    action.IType(driver, Elements.DesignCenter_LtSpiceDemoCircuits_DemonstrationCircuit_Search_Txtbox, modelFile_search_keyword);

                    //--- Expected Result: All LT model number with search Model file field should be filtered ---//
                    test.validateElementIsPresent(driver, Elements.DesignCenter_LtSpiceDemoCircuits_Tbl_Search_Results);

                    action.IDeleteValueOnFields(driver, Elements.DesignCenter_LtSpiceDemoCircuits_DemonstrationCircuit_Search_Txtbox);
                }

                if (util.CheckElement(driver, Elements.DesignCenter_LtSpiceDemoCircuits_Product_Search_Txtbox, 2))
                {
                    //--- Input ---//
                    string product_search_keyword = "AD771";

                    //--- Action: Enter invalid search in any field ---//
                    action.IDeleteValueOnFields(driver, Elements.DesignCenter_LtSpiceDemoCircuits_Product_Search_Txtbox);
                    action.IType(driver, Elements.DesignCenter_LtSpiceDemoCircuits_Product_Search_Txtbox, product_search_keyword);

                    //--- Expected Result: No matching records found will be displayed in the table. ---//
                    test.validateElementIsPresent(driver, Elements.DesignCenter_LtSpiceDemoCircuits_Tbl_Empty_Search_Results);

                    action.IDeleteValueOnFields(driver, Elements.DesignCenter_LtSpiceDemoCircuits_Product_Search_Txtbox);
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.DesignCenter_LtSpiceDemoCircuits_Product_Search_Txtbox);
                }

                if (util.CheckElement(driver, Elements.DesignCenter_LtSpiceDemoCircuits_Tbl_Product_Links, 2))
                {
                    //----- R7 > T5: Verify links on the table -----//

                    //--- Action: Click on LTC model number  ---//
                    action.IOpenLinkInNewTab(driver, Elements.DesignCenter_LtSpiceDemoCircuits_Tbl_Product_Links);
                    Thread.Sleep(4000);

                    //--- Expected Result: Page should be redirected to Product Detail Page ---//
                    test.validateStringInstance(driver, driver.Url, /*"analog.com/" + Locale +*/ pdp_url_format);
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.DesignCenter_LtSpiceDemoCircuits_Tbl_Product_Links);
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.DesignCenter_LtSpiceDemoCircuits_Tbl);
            }
        }
    }
}