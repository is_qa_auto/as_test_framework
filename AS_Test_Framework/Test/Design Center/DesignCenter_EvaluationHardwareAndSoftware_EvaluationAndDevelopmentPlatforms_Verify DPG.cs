﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Threading;

namespace AS_Test_Framework.DesignCenter
{
    [TestFixture]
    public class DesignCenter_EvaluationHardwareAndSoftware_EvaluationAndDevelopmentPlatforms_Dpg : BaseSetUp
    {
        public DesignCenter_EvaluationHardwareAndSoftware_EvaluationAndDevelopmentPlatforms_Dpg() : base() { }

        //--- URLs ---//
        string dpg_page_url = "/design-center/evaluation-hardware-and-software/evaluation-development-platforms/dpg.html";
        string pdp_url_format = "/products/";

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that Data Pattern Generator High-Speed DAC Evaluation Platform is Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that Data Pattern Generator High-Speed DAC Evaluation Platform is Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that Data Pattern Generator High-Speed DAC Evaluation Platform is Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that Data Pattern Generator High-Speed DAC Evaluation Platform is Working as Expected in RU Locale")]
        public void DesignCenter_EvaluationHardwareAndSoftware_EvaluationAndDevelopmentPlatforms_VerifyDpg(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + dpg_page_url);

            //----- R1 > T5_3: Verify Data Pattern Generator (DPG) High-Speed DAC of Evaluation Platform page -----//

            //--- Expected Result: Page Title of the page should be shown ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_Dpg_Page_Title_Txt);

            //--- Expected Result: Description of the page should be shown ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_Dpg_Desc_Txt);

            //----- COMPATIBLE PRODUCT EVALUATION BOARDS -----//

            //--- Expected Result:  COMPATIBLE PRODUCT EVALUATION BOARDS  should have Section description ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_Dpg_CompatibleProductEvaluationBoards_Sec_Desc_Txt);

            //--- Expected Result:  COMPATIBLE PRODUCT EVALUATION BOARDS  should have Recommended Controller ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_Dpg_CompatibleProductEvaluationBoards_Sec_RecommendedController_Txt);

            //--- Expected Result:  COMPATIBLE PRODUCT EVALUATION BOARDS  should have Arrow down ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_Dpg_CompatibleProductEvaluationBoards_Sec_Expand_Accordion_Btn);

            //--- Expected Result:  COMPATIBLE PRODUCT EVALUATION BOARDS  should have Category Name ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_Dpg_CompatibleProductEvaluationBoards_Sec_Category_Name_Txts);

            if (util.CheckElement(driver, Elements.DesignCenter_Dpg_CompatibleProductEvaluationBoards_Sec_Expand_Accordion_Btn, 2))
            {
                action.IClick(driver, Elements.DesignCenter_Dpg_CompatibleProductEvaluationBoards_Sec_Expand_Accordion_Btn);

                //--- Expected Result:  COMPATIBLE PRODUCT EVALUATION BOARDS  should have Compatible Product ---//
                test.validateElementIsPresent(driver, Elements.DesignCenter_Dpg_CompatibleProductEvaluationBoards_Sec_Product_Links);

                if (util.CheckElement(driver, Elements.DesignCenter_Dpg_CompatibleProductEvaluationBoards_Sec_Product_Links, 2))
                {
                    //--- Action: Click on a Product model  link ---//
                    action.IOpenLinkInNewTab(driver, Elements.DesignCenter_Dpg_CompatibleProductEvaluationBoards_Sec_Product_Links);
                    Thread.Sleep(2000);

                    //--- Expected Result: It should redirect on Product  detail page ---//
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + pdp_url_format);
                }
            }
        }
    }
}