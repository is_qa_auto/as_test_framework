﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;

namespace AS_Test_Framework.DesignCenter
{
    [TestFixture]
    public class DesignCenter_EvaluationHardwareAndSoftware_ProductEvaluationBoardsAndKits : BaseSetUp
    {
        public DesignCenter_EvaluationHardwareAndSoftware_ProductEvaluationBoardsAndKits() : base() { }

        //--- URLs ---//
        string productEvaluationBoardsAndKits_page_url = "/design-center/evaluation-hardware-and-software/evaluation-boards-kits.html";

        //--- Labels ---//
        string ghost_text = "Enter part number or keyword";

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that Product Evaluation Boards and Kits is Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that Product Evaluation Boards and Kits is Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that Product Evaluation Boards and Kits is Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that Product Evaluation Boards and Kits is Working as Expected in RU Locale")]
        public void DesignCenter_EvaluationHardwareAndSoftware_VerifyEvaluationHardwareAndSoftware(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + productEvaluationBoardsAndKits_page_url);

            //----- R1 > T4_1: Verify Product Evaluation Boards and Kits landing page -----//

            //--- Expected Result: Product evaluation boards and Kits page title should be shown ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_ProductEvaluationBoardsAndKits_Page_Title_Txt);

            //--- Expected Result: Product evaluation boards and Kits page description should be shown ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_ProductEvaluationBoardsAndKits_Desc_Txt);

            //--- Expected Result: Ghost text should be "Enter Part Number or Keyword" ---//
            if (Locale.Equals("en"))
            {
                test.validateString(driver, ghost_text, util.ReturnAttribute(driver, Elements.DesignCenter_ProductEvaluationBoardsAndKits_Search_Txtbox, "placeholder"));
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.DesignCenter_ProductEvaluationBoardsAndKits_Search_Txtbox);
            }

            if (util.CheckElement(driver, Elements.DesignCenter_ProductEvaluationBoardsAndKits_Search_Txtbox, 2))
            {
                //--- Action: Type a partial text on the Product Evaluation Boards and Kits search field ---//
                action.IDeleteValueOnFields(driver, Elements.DesignCenter_ProductEvaluationBoardsAndKits_Search_Txtbox);
                string search_input = "EVAL";
                action.IType(driver, Elements.DesignCenter_ProductEvaluationBoardsAndKits_Search_Txtbox, search_input);

                //--- Expected Result: Auto Suggest dropdown must be displayed. ---//
                test.validateElementIsPresent(driver, Elements.DesignCenter_ProductEvaluationBoardsAndKits_Search_Txtbox_Autosuggest_Dd);
            }
        }
    }
}