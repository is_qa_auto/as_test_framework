﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.DesignCenter
{
    [TestFixture]
    public class DesignCenter_ProcessorsAndDsp : BaseSetUp
    {
        public DesignCenter_ProcessorsAndDsp() : base() { }

        //--- URLs ---//
        string processorsAndDsp_page_url = "/design-center/processors-and-dsp.html";
        string home_page_url = "/index.html";
        string hardware_page_url = "/evaluation-development-hardware.html";
        string technicalLibrary_page_url = "/processors-dsp-technical-library.html";
        string thirdPartyDevelopersProgram_page_url = "/third-party-developers.html";
        string processorsAndMicrocontrollers_page_url = "/processors-microcontrollers.html";

        //--- Labels ---//
        string processorsAndDsp_txt = "Processors & DSP";
        string processorsAndDspResources_txt = "Processors & DSP Resources";
        string hardware_txt = "Hardware";
        string technicalLibrary_txt = "Technical Library";
        string thirdPartyDevelopersProgram_txt = "Third Party Developers Program";

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Breadcrumb is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Breadcrumb is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Breadcrumb is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Breadcrumb is Present and Working as Expected in RU Locale")]
        public void DesignCenter_ProcessorsAndDsp_VerifyBreadcrumb(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + processorsAndDsp_page_url);

            //----- Design Center - Processors and DSP TestPlan > R1 > T1: Verify the Processors and DSP Landing page -----//

            //--- Expected Result: The Print widget should be displayed on the Processors and DSP Landing page ---//
            test.validateElementIsPresent(driver, Elements.Print_Widget_Btn);

            //--- Expected Result: The Save to My Analog widget should be displayed on the Processors and DSP Landing page ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Widget_Btn);

            //--- Expected Result: The Breadcrumbs should be displayed on the Processors and DSP Landing page ---//
            test.validateElementIsPresent(driver, Elements.Breadcrumb_Details);

            if (util.CheckElement(driver, Elements.Breadcrumb_Details, 2))
            {
                //----- Design Center - Processors and DSP TestPlan > R1 > T2: Verify the breadcrumb -----//

                //--- Expected Result: The breadcrumb of the current  page should is just a label name of the page ---//
                test.validateStringIsCorrect(driver, Elements.Breadcrumb_CurrentPage_Txt, util.GetText(driver, Elements.DesignCenter_ProcessorsAndDsp_Page_Title_Txt));

                if (util.CheckElement(driver, Elements.BreadCrumb_Home_Icon, 2) && !Configuration.Browser.Equals("Chrome_HL"))
                {
                    //--- Action: Click on the ADI Home Icon ---//
                    action.IOpenLinkInNewTab(driver, Elements.BreadCrumb_Home_Icon);

                    //--- Expected Result: Home Page should be displayed ---//
                    test.validateStringInstance(driver, driver.Url, home_page_url);
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.BreadCrumb_Home_Icon);
                }
            }
        }

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Left Rail Navigation is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Left Rail Navigation is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Left Rail Navigation is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Left Rail Navigation is Present and Working as Expected in RU Locale")]
        public void DesignCenter_ProcessorsAndDsp_VerifyLeftRailNavigation(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + processorsAndDsp_page_url);

            //----- Design Center - Processors and DSP TestPlan > R1 > T1: Verify the Processors and DSP Landing page -----//

            //--- Expected Result: Left Rail navigation ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_ProcessorsAndDsp_LeftRailNav);

            if (util.CheckElement(driver, Elements.DesignCenter_ProcessorsAndDsp_LeftRailNav, 2))
            {
                //----- Design Center - Processors and DSP TestPlan > R1 > T5: Verify the Left Rail navigation -----//
                if (Locale.Equals("en"))
                {
                    //--- Expected Result: The Processors and DSP should be the currently selected section ---//
                    test.validateStringIsCorrect(driver, Elements.DesignCenter_ProcessorsAndDsp_LeftRailNav_Selected_Sec_Link, processorsAndDsp_txt);
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.DesignCenter_ProcessorsAndDsp_LeftRailNav_Selected_Sec_Link);
                }

                //----- Design Center - Processors and DSP TestPlan > R1 > T1: Verify the Processors and DSP Landing page -----//

                //--- Expected Result: The Sub Category lists should be displayed on the Processors and DSP Landing page ---//
                test.validateElementIsPresent(driver, Elements.DesignCenter_ProcessorsAndDsp_LeftRailNav_SubCat_List_Links);

                //----- Design Center - Processors and DSP TestPlan > R1 > T5: Verify the Left Rail navigation -----//

                //--- Expected Result: The Hardware sub category should be displayed ---//
                test.validateElementIsPresent(driver, Elements.DesignCenter_ProcessorsAndDsp_LeftRailNav_Hardware_Link);

                //--- Expected Result: The Technical Library sub category should be displayed ---//
                test.validateElementIsPresent(driver, Elements.DesignCenter_ProcessorsAndDsp_LeftRailNav_TechnicalLibrary_Link);

                //--- Expected Result: The Third Party Developers Program sub category should be displayed ---//
                test.validateElementIsPresent(driver, Elements.DesignCenter_ProcessorsAndDsp_LeftRailNav_ThirdPartyDevelopersProgram_Link);

                if (util.CheckElement(driver, Elements.DesignCenter_ProcessorsAndDsp_LeftRailNav_Hardware_Link, 2))
                {
                    //--- Action: Click on the Hardware sub category ---//
                    action.IOpenLinkInNewTab(driver, Elements.DesignCenter_ProcessorsAndDsp_LeftRailNav_Hardware_Link);
                    Thread.Sleep(4000);

                    //--- Expected Result: Each section in the left rail should be directed to separate landing page ---//
                    test.validateStringInstance(driver, driver.Url, hardware_page_url);

                    driver.Close();
                    driver.SwitchTo().Window(driver.WindowHandles.First());
                }

                if (util.CheckElement(driver, Elements.DesignCenter_ProcessorsAndDsp_LeftRailNav_TechnicalLibrary_Link, 2))
                {
                    //--- Action: Click on the Technical Library sub category ---//
                    action.IOpenLinkInNewTab(driver, Elements.DesignCenter_ProcessorsAndDsp_LeftRailNav_TechnicalLibrary_Link);
                    Thread.Sleep(1000);

                    //--- Expected Result: Each section in the left rail should be directed to separate landing page ---//
                    test.validateStringInstance(driver, driver.Url, technicalLibrary_page_url);

                    driver.Close();
                    driver.SwitchTo().Window(driver.WindowHandles.First());
                }

                if (util.CheckElement(driver, Elements.DesignCenter_ProcessorsAndDsp_LeftRailNav_ThirdPartyDevelopersProgram_Link, 2))
                {
                    //--- Action: Click on the Third Party Developers Program sub category ---//
                    action.IOpenLinkInNewTab(driver, Elements.DesignCenter_ProcessorsAndDsp_LeftRailNav_ThirdPartyDevelopersProgram_Link);
                    Thread.Sleep(1000);

                    //--- Expected Result: Each section in the left rail should be directed to separate landing page ---//
                    test.validateStringInstance(driver, driver.Url, thirdPartyDevelopersProgram_page_url);
                }
            }
        }

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that Processors & DSP is Working as Expected in EN Locale")]
        //[TestCase("cn", TestName = "Verify that Processors & DSP is Working as Expected in CN Locale")]
        //[TestCase("jp", TestName = "Verify that Processors & DSP is Working as Expected in JP Locale")]
        //[TestCase("ru", TestName = "Verify that Processors & DSP is Working as Expected in RU Locale")]
        public void DesignCenter_VerifyProcessorsAndDsp(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + processorsAndDsp_page_url);

            //----- PROCESSORS & DSP RESOURCES -----//

            //----- Design Center - Processors and DSP TestPlan > R1 > T6: Verify the Processors and DSP Resources -----//

            //--- Expected Result: The View our product listings link should be displayed under the Processors and DSP Resources ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_ProcessorsAndDsp_ViewOurProductListings_Link);

            //--- Expected Result: The EngineerZone Support Community for DSP link should be displayed under the Processors and DSP Resources ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_ProcessorsAndDsp_EngineerZoneSupportCommunityForDsp_Link);

            if (util.CheckElement(driver, Elements.DesignCenter_ProcessorsAndDsp_ViewOurProductListings_Link, 2) && !Configuration.Browser.Equals("Chrome_HL"))
            {
                //--- Action: Click on the 'View our product listings' link ---//
                action.IOpenLinkInNewTab(driver, Elements.DesignCenter_ProcessorsAndDsp_ViewOurProductListings_Link);
                Thread.Sleep(3000);

                //--- Expected Result: The Processors and DSP category page should be displayed ---//
                test.validateStringInstance(driver, driver.Url, processorsAndMicrocontrollers_page_url);

                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles.First());
            }

            if (util.CheckElement(driver, Elements.DesignCenter_ProcessorsAndDsp_SubSecs, 2) && Locale.Equals("en"))
            {
                int pad_subsec_count = util.GetCount(driver, Elements.DesignCenter_ProcessorsAndDsp_SubSecs);

                bool processorsAndDspResources_sec = false;
                bool hardware_sec = false;
                bool technicalLibrary_sec = false;
                bool thirdPartyDevelopersProgram_sec = false;

                for (int pad_ctr = 2; pad_ctr <= pad_subsec_count; pad_ctr++)
                {
                    if (util.GetText(driver, By.CssSelector("div[name$='content_section']>div:nth-of-type(" + pad_ctr + ") * h3[class*='header']")).Equals(processorsAndDspResources_txt))
                    {
                        //----- PROCESSORS & DSP RESOURCES -----//

                        processorsAndDspResources_sec = true;

                        //--- Locators ----//
                        string section_locator = "div[name$='content_section']>div:nth-of-type(" + pad_ctr + ")>div";

                        //----- Design Center - Processors and DSP TestPlan > R1 > T1: Verify the Processors and DSP Landing page -----//

                        //--- Expected Result: The Processors and DSP Resources should be displayed on the Processors and DSP Landing page ---//
                        test.validateElementIsPresent(driver, By.CssSelector(section_locator));
                    }

                    if (util.GetText(driver, By.CssSelector("div[name$='content_section']>div:nth-of-type(" + pad_ctr + ") * h3[class*='header']")).Equals(hardware_txt))
                    {
                        //----- HARDWARE -----//

                        hardware_sec = true;

                        //--- Locators ----//
                        string subcategoryName_txt_locator = "div[name$='content_section']>div:nth-of-type(" + pad_ctr + ") * h3[class*='header']";
                        string viewMore_link_locator = "div[name$='content_section']>div:nth-of-type(" + pad_ctr + ") * a[class$='link']";
                        string linkedLists_locator = "div[name$='content_section']>div:nth-of-type(" + pad_ctr + ") * div[class='resource-list']>ul>li>a";

                        //----- Design Center - Processors and DSP TestPlan > R1 > T7_2: Verify the Hardware section -----//

                        //--- Expected Result: The Hardware subcategory page should contain the subcategory name ---//
                        test.validateElementIsPresent(driver, By.CssSelector(subcategoryName_txt_locator));

                        //--- Expected Result: The Hardware subcategory page should contain the View More link ---//
                        test.validateElementIsPresent(driver, By.CssSelector(viewMore_link_locator));

                        //--- Expected Result: The Hardware subcategory page should contain the linked lists ---//
                        test.validateElementIsPresent(driver, By.CssSelector(linkedLists_locator));

                        if (util.CheckElement(driver, By.CssSelector(viewMore_link_locator), 2) && !Configuration.Browser.Equals("Chrome_HL"))
                        {
                            //--- Action: Click on the View More link under the subcategory ---//
                            action.IOpenLinkInNewTab(driver, By.CssSelector(viewMore_link_locator));

                            //--- Expected Result: The Hardware subcategory page should be displayed ---//
                            test.validateStringInstance(driver, driver.Url, hardware_page_url);

                            driver.Close();
                            driver.SwitchTo().Window(driver.WindowHandles.First());
                        }
                    }

                    if (util.GetText(driver, By.CssSelector("div[name$='content_section']>div:nth-of-type(" + pad_ctr + ") * h3[class*='header']")).Equals(technicalLibrary_txt))
                    {
                        //----- TECHNICAL LIBRARY -----//

                        technicalLibrary_sec = true;

                        //--- Locators ----//
                        string subcategoryName_txt_locator = "div[name$='content_section']>div:nth-of-type(" + pad_ctr + ") * h3[class*='header']";
                        string viewMore_link_locator = "div[name$='content_section']>div:nth-of-type(" + pad_ctr + ") * a[class$='link']";

                        //----- Design Center - Processors and DSP TestPlan > R1 > T7_3: Verify the Technical Library section -----//

                        //--- Expected Result: The Technical Library subcategory page should contain the subcategory name ---//
                        test.validateElementIsPresent(driver, By.CssSelector(subcategoryName_txt_locator));

                        //--- Expected Result: The Technical Library subcategory page should contain the View More link ---//
                        test.validateElementIsPresent(driver, By.CssSelector(viewMore_link_locator));

                        if (util.CheckElement(driver, By.CssSelector(viewMore_link_locator), 2))
                        {
                            //--- Action: Click on the View More link under the subcategory ---//
                            action.IOpenLinkInNewTab(driver, By.CssSelector(viewMore_link_locator));
                            Thread.Sleep(6000);

                            //--- Expected Result: The Technical Library subcategory page should be displayed ---//
                            test.validateStringInstance(driver, driver.Url, technicalLibrary_page_url);

                            driver.Close();
                            driver.SwitchTo().Window(driver.WindowHandles.First());
                        }
                    }

                    if (util.GetText(driver, By.CssSelector("div[name$='content_section']>div:nth-of-type(" + pad_ctr + ") * h3[class*='header']")).Equals(thirdPartyDevelopersProgram_txt))
                    {
                        //----- THIRD PARTY DEVELOPERS PROGRAM -----//

                        thirdPartyDevelopersProgram_sec = true;

                        //--- Locators ----//
                        string subcategoryName_txt_locator = "div[name$='content_section']>div:nth-of-type(" + pad_ctr + ") * h3[class*='header']";
                        string subcategoryDesc_txt_locator = "div[name$='content_section']>div:nth-of-type(" + pad_ctr + ") * p";
                        string viewMore_link_locator = "div[name$='content_section']>div:nth-of-type(" + pad_ctr + ") * a[class$='link']";

                        //----- Design Center - Processors and DSP TestPlan > R1 > T7_4: Verify the 3rd Party Developers section -----//

                        //--- Expected Result: The 3rd Party Developers Program subcategory page should contain the subcategory name ---//
                        test.validateElementIsPresent(driver, By.CssSelector(subcategoryName_txt_locator));

                        //--- Expected Result: The 3rd Party Developers Program subcategory page should contain the description ---//
                        test.validateElementIsPresent(driver, By.CssSelector(subcategoryDesc_txt_locator));

                        //--- Expected Result: The 3rd Party Developers Program subcategory page should contain the View More link ---//
                        test.validateElementIsPresent(driver, By.CssSelector(viewMore_link_locator));

                        if (util.CheckElement(driver, By.CssSelector(viewMore_link_locator), 2))
                        {
                            //--- Action: Click on the View More link under the subcategory ---//
                            action.IOpenLinkInNewTab(driver, By.CssSelector(viewMore_link_locator));
                            Thread.Sleep(1000);

                            //--- Expected Result: The 3rd Party Developers Program subcategory page should be displayed ---//
                            test.validateStringInstance(driver, driver.Url, thirdPartyDevelopersProgram_page_url);

                            driver.Close();
                            driver.SwitchTo().Window(driver.WindowHandles.First());
                        }
                    }

                    if (processorsAndDspResources_sec == true && hardware_sec == true && technicalLibrary_sec == true && thirdPartyDevelopersProgram_sec == true)
                    {
                        break;
                    }
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.DesignCenter_ProcessorsAndDsp_SubSecs);
            }
        }
    }
}