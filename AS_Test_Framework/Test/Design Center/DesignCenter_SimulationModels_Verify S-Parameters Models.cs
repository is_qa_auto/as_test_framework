﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Threading;

namespace AS_Test_Framework.DesignCenter
{
    [TestFixture]
    public class DesignCenter_SimulationModels_SParameters : BaseSetUp
    {
        public DesignCenter_SimulationModels_SParameters() : base() { }

        //--- URLs ---//
        string sParameters_page_url = "/design-center/simulation-models/s-parameters.html";
        string pdp_url_format = "/products/";

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that S-Parameters is Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that S-Parameters is Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that S-Parameters is Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that S-Parameters is Working as Expected in RU Locale")]
        public void DesignCenter_SimulationModels_VerifySParameters(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + sParameters_page_url);

            //----- R4 > T1: Verify Page Title (AL-10662) -----//

            //--- Expected Result: The page title should be properly displayed and same label that was clicked on the category linked list ---//
            test.validateStringIsCorrect(driver, Elements.DesignCenter_SParameters_Page_Title_Txt, util.GetText(driver, Elements.DesignCenter_SimulationModels_LeftRailNav_Selected_Val_Link));

            //----- R4 > T2: Verify S-Parameters  | Files (ACM-2690) -----//

            //--- Expected Result: The Search should be displayed on the Descriptive Summary section ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_SParameters_Search_Txtbox);

            //--- Expected Result: The Default table display should be displayed on the Descriptive Summary section ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_SParameters_Tbl);

            if (util.CheckElement(driver, Elements.DesignCenter_SParameters_Tbl, 2))
            {
                //----- R4 > T3: Verify S-Parameters Files Search Section -----//

                if (util.CheckElement(driver, Elements.DesignCenter_SParameters_Search_Txtbox, 2))
                {
                    //--- Input ---//
                    string search_keyword = "AD";

                    //--- Action: Enter product number in search text field ---//
                    action.IDeleteValueOnFields(driver, Elements.DesignCenter_SParameters_Search_Txtbox);
                    action.IType(driver, Elements.DesignCenter_SParameters_Search_Txtbox, search_keyword);

                    //--- Expected Result: Search result should be displayed on the table ---//
                    test.validateElementIsPresent(driver, Elements.DesignCenter_SParameters_Tbl_Search_Results);

                    action.IDeleteValueOnFields(driver, Elements.DesignCenter_SParameters_Search_Txtbox);
                }

                if (util.CheckElement(driver, Elements.DesignCenter_SParameters_Product_Search_Txtbox, 2))
                {
                    //--- Input ---//
                    string product_search_keyword = "AD";

                    //--- Action: Enter product number in  product field ---//
                    action.IDeleteValueOnFields(driver, Elements.DesignCenter_SParameters_Product_Search_Txtbox);
                    action.IType(driver, Elements.DesignCenter_SParameters_Product_Search_Txtbox, product_search_keyword);

                    //--- Expected Result: Search result should be displayed on the table ---//
                    test.validateElementIsPresent(driver, Elements.DesignCenter_SParameters_Tbl_Search_Results);

                    action.IDeleteValueOnFields(driver, Elements.DesignCenter_SParameters_Product_Search_Txtbox);
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.DesignCenter_SParameters_Product_Search_Txtbox);
                }

                if (util.CheckElement(driver, Elements.DesignCenter_SParameters_Description_Search_Txtbox, 2))
                {
                    //--- Input ---//
                    string desc_search_keyword = "SPDT";

                    //--- Action: Enter any text in description field ---//
                    action.IDeleteValueOnFields(driver, Elements.DesignCenter_SParameters_Description_Search_Txtbox);
                    action.IType(driver, Elements.DesignCenter_SParameters_Description_Search_Txtbox, desc_search_keyword);

                    //--- Expected Result: All product with searched description should be filtered ---//
                    test.validateElementIsPresent(driver, Elements.DesignCenter_SParameters_Tbl_Search_Results);

                    action.IDeleteValueOnFields(driver, Elements.DesignCenter_SParameters_Description_Search_Txtbox);
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.DesignCenter_SParameters_Description_Search_Txtbox);
                }

                if (util.CheckElement(driver, Elements.DesignCenter_SParameters_ModelFile_Search_Txtbox, 2))
                {
                    //--- Input ---//
                    string modelFile_search_keyword = "AD";

                    //--- Action: Enter model file in model file field ---//
                    action.IDeleteValueOnFields(driver, Elements.DesignCenter_SParameters_ModelFile_Search_Txtbox);
                    action.IType(driver, Elements.DesignCenter_SParameters_ModelFile_Search_Txtbox, modelFile_search_keyword);

                    //--- Expected Result: All product with search Model file field should be filtered ---//
                    test.validateElementIsPresent(driver, Elements.DesignCenter_SParameters_Tbl_Search_Results);

                    action.IDeleteValueOnFields(driver, Elements.DesignCenter_SParameters_ModelFile_Search_Txtbox);
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.DesignCenter_SParameters_ModelFile_Search_Txtbox);
                }

                if (util.CheckElement(driver, Elements.DesignCenter_SParameters_Tbl_Product_Links, 2))
                {
                    //----- R4 > T4: Verify links on the table -----//

                    //--- Action: Click on product number ---//
                    action.IOpenLinkInNewTab(driver, Elements.DesignCenter_SParameters_Tbl_Product_Links);
                    Thread.Sleep(4000);

                    //--- Expected Result: Page should be redirected to Product Detail Page ---//
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + pdp_url_format);
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.DesignCenter_SParameters_Tbl_Product_Links);
                }
            }
        }
    }
}