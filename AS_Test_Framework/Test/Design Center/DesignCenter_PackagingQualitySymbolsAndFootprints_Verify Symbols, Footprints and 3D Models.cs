﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;

namespace AS_Test_Framework.DesignCenter
{
    [TestFixture]
    public class DesignCenter_PackagingQualitySymbolsAndFootprints_SymbolsFootprintsAnd3dModels : BaseSetUp
    {
        public DesignCenter_PackagingQualitySymbolsAndFootprints_SymbolsFootprintsAnd3dModels() : base() { }

        //--- URLs ---//
        string symbolsFootprintsAnd3dModels_page_url = "/design-center/packaging-quality-symbols-footprints/symbols-and-footprints.html";
        string symbolsFootprintsAnd3dModels_ltc_genericPart_url = "/design-center/packaging-quality-symbols-footprints/symbols-and-footprints/LTC2630.html";
        string symbolsFootprintsAnd3dModels_adi_genericPart_page_url = "/design-center/packaging-quality-symbols-footprints/symbols-and-footprints/AD7706.html";

        //--- Labels ---//
        string packagingQualitySymbolsAndFootprints_txt = "Packaging, Quality, Symbols & Footprints";
        string symbolsFootprintsAnd3dModels_txt = "Symbols, Footprints & 3D Models";
        string noSearchResults_msg_txt = "No results found for the model";
        string ultraLibrarianReader_txt = "Ultra Librarian Reader";
        string ecadModels_txt = "ECAD Models";
        string bxl_txt = "BXL";
        string zip_txt = "ZIP";
        string requestNewFile_success_msg_txt = "Request sent successfully";

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Breadcrumb is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Breadcrumb is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Breadcrumb is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Breadcrumb is Present and Working as Expected in RU Locale")]
        public void DesignCenter_PackagingQualitySymbolsAndFootprints_SymbolsFootprintsAnd3dModels_VerifyBreadcrumb(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + symbolsFootprintsAnd3dModels_page_url);

            //----- Design Center - Symbols and Footprints TestPlan > R2 > T2: Select all items in the "Left Rail Navigation" section of the page. -----//

            //--- Expected Result: The last part content label of the breadcrumb should have the same info as the page header content. ---//
            test.validateStringIsCorrect(driver, Elements.Breadcrumb_CurrentPage_Txt, util.GetText(driver, Elements.DesignCenter_SymbolsFootprintsAnd3dModels_Page_Title_Txt));
        }

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Left Rail Navigation is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Left Rail Navigation is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Left Rail Navigation is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Left Rail Navigation is Present and Working as Expected in RU Locale")]
        public void DesignCenter_PackagingQualitySymbolsAndFootprints_SymbolsFootprintsAnd3dModels_VerifyLeftRailNavigation(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + symbolsFootprintsAnd3dModels_page_url);

            //----- Design Center - Symbols and Footprints TestPlan > R2 > T2: Select all items in the "Left Rail Navigation" section of the page. -----//

            if (util.CheckElement(driver, Elements.DesignCenter_PackagingQualitySymbolsAndFootprints_LeftRailNav, 2))
            {
                if (Locale.Equals("en"))
                {
                    //--- Expected Result: "Packaging, Quality, Symbols and Footprints" is highlighted in the "Left Rail Navigation" section. ---//
                    test.validateStringIsCorrect(driver, Elements.DesignCenter_PackagingQualitySymbolsAndFootprints_LeftRailNav_Selected_Sec_Link, packagingQualitySymbolsAndFootprints_txt);
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.DesignCenter_PackagingQualitySymbolsAndFootprints_LeftRailNav_Selected_Sec_Link);
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.DesignCenter_PackagingQualitySymbolsAndFootprints_LeftRailNav);
            }
        }

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that Symbols, Footprints & 3D Models is Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that Symbols, Footprints & 3D Models is Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that Symbols, Footprints & 3D Models is Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that Symbols, Footprints & 3D Models is Working as Expected in RU Locale")]
        public void DesignCenter_PackagingQualitySymbolsAndFootprints_VerifySymbolsFootprintsAnd3dModels(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + symbolsFootprintsAnd3dModels_page_url);

            //----- Design Center - Symbols and Footprints TestPlan > R2 > T2: Select all items in the "Left Rail Navigation" section of the page. -----//

            if (Locale.Equals("en"))
            {
                //--- Expected Result: Page header content label should display "Symbols and Footprints". ---//
                test.validateStringIsCorrect(driver, Elements.DesignCenter_PackageIndex_Page_Title_Txt, symbolsFootprintsAnd3dModels_txt);
            }
            else
            {
                //--- Expected Result: "Symbols and Footprints" page header should be displayed. ---//
                test.validateElementIsPresent(driver, Elements.DesignCenter_SymbolsFootprintsAnd3dModels_Page_Title_Txt);
            }

            //----- ULTRA LIBRARIAN READER -----//

            //----- Design Center - Symbols and Footprints TestPlan > R2 > T10: Verify updated Symbols and Footprints page -----//

            //--- Expected Result: First paragraph is displayed ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_SymbolsFootprintsAnd3dModels_UltraLibrarianReader_Desc_Txt);

            //--- Expected Result: Ultra Librarian Reader video section must be displayed ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_SymbolsFootprintsAnd3dModels_UltraLibrarianReader_Video);
        }

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Symbols Search is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Symbols Search is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Symbols Search is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Symbols Search is Present and Working as Expected in RU Locale")]
        public void DesignCenter_PackagingQualitySymbolsAndFootprints_SymbolsFootprintsAnd3dModels_VerifySymbolsSearch(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + symbolsFootprintsAnd3dModels_page_url);

            //----- ECAD MODELS SEARCH -----//

            //----- Design Center - Symbols and Footprints TestPlan > R2 > T3: Verify Symbol Search section -----//

            //--- Expected Result: Content should display the text content. ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_SymbolsFootprintsAnd3dModels_EcadModelsSearch_Desc_Txt);

            if (util.CheckElement(driver, Elements.DesignCenter_SymbolsFootprintsAnd3dModels_EcadModelsSearch_Search_Tbx, 2) && util.CheckElement(driver, Elements.DesignCenter_SymbolsFootprintsAnd3dModels_EcadModelsSearch_Search_Btn, 2))
            {
                //--- Action: Click the magnifying glass icon. ---//
                action.IClick(driver, Elements.DesignCenter_SymbolsFootprintsAnd3dModels_EcadModelsSearch_Search_Btn);

                if (Locale.Equals("en"))
                {
                    //--- Expected Result: "No results found for the model" should be displayed below the input field. ---//
                    test.validateStringIsCorrect(driver, Elements.DesignCenter_SymbolsFootprintsAnd3dModels_NoSearchResults_Txt, noSearchResults_msg_txt);
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.DesignCenter_SymbolsFootprintsAnd3dModels_NoSearchResults_Txt);
                }

                //--- Action: Enter any part number in the input field. ---//
                string invalid_symbolsSearch_input = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";

                action.IDeleteValueOnFields(driver, Elements.DesignCenter_SymbolsFootprintsAnd3dModels_EcadModelsSearch_Search_Tbx);
                action.IType(driver, Elements.DesignCenter_SymbolsFootprintsAnd3dModels_EcadModelsSearch_Search_Tbx, invalid_symbolsSearch_input);

                //--- Action: Click the magnifying glass icon. ---//
                action.IClick(driver, Elements.DesignCenter_SymbolsFootprintsAnd3dModels_EcadModelsSearch_Search_Btn);

                if (Locale.Equals("en"))
                {
                    //--- Expected Result: "No results found for the model <part number entered>" format should be displayed below the input field. ---//
                    test.validateStringIsCorrect(driver, Elements.DesignCenter_SymbolsFootprintsAnd3dModels_NoSearchResults_Txt, noSearchResults_msg_txt + " " + invalid_symbolsSearch_input);
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.DesignCenter_SymbolsFootprintsAnd3dModels_NoSearchResults_Txt);
                }

                //----- SYMBOLS FOR <ENTERED PART NUMBER> -----//

                //--- Action: Enter any part number. ---//
                string valid_symbolsSearch_input = "AD7706";

                action.IDeleteValueOnFields(driver, Elements.DesignCenter_SymbolsFootprintsAnd3dModels_EcadModelsSearch_Search_Tbx);
                action.IType(driver, Elements.DesignCenter_SymbolsFootprintsAnd3dModels_EcadModelsSearch_Search_Tbx, valid_symbolsSearch_input);

                //--- Action: Click the magnifying glass icon or press the <enter> key. ---//
                action.IClick(driver, Elements.DesignCenter_SymbolsFootprintsAnd3dModels_EcadModelsSearch_Search_Btn);

                //--- Expected Result: Symbols table should display the result of the part number corresponding to it. ---//
                test.validateElementIsPresent(driver, Elements.DesignCenter_SymbolsFootprintsAnd3dModels_Symbols_Tbl);

                if (util.CheckElement(driver, Elements.DesignCenter_SymbolsFootprintsAnd3dModels_Symbols_Tbl, 2))
                {
                    //----- Design Center - Symbols and Footprints TestPlan > R2 > T11: Verify "ECAD Models" and "Ultra Librarian Reader" column in ECAD Model Search symbols table (IQ-8603/AL-15355) -----//

                    if (Locale.Equals("en"))
                    {
                        //--- Expected Result: The second column in Symbols table is named 'Ultra Librarian Reader' (IQ-9094/AL-16271) ---//
                        test.validateStringIsCorrect(driver, Elements.DesignCenter_SymbolsFootprintsAnd3dModels_Symbols_Tbl_UltraLibrarianReader_ColHeader_Txt, ultraLibrarianReader_txt);

                        //--- Expected Result: ECAD Model Search symbols table should have additional column "ECAD Models" ---//
                        test.validateStringIsCorrect(driver, Elements.DesignCenter_SymbolsFootprintsAnd3dModels_Symbols_Tbl_EcadModels_ColHeader_Txt, ecadModels_txt);
                    }
                    else
                    {
                        //--- Expected Result: The second column in Symbols table is present (IQ-9094/AL-16271) ---//
                        test.validateElementIsPresent(driver, Elements.DesignCenter_SymbolsFootprintsAnd3dModels_Symbols_Tbl_UltraLibrarianReader_ColHeader_Txt);

                        //--- Expected Result: ECAD Model Search symbols table should have additional column "ECAD Models" ---//
                        test.validateElementIsPresent(driver, Elements.DesignCenter_SymbolsFootprintsAnd3dModels_Symbols_Tbl_EcadModels_ColHeader_Txt);
                    }

                    bool bxl_val = false;
                    bool zip_val = false;

                    int symbolsTbl_row_count = util.GetCount(driver, Elements.DesignCenter_SymbolsFootprintsAnd3dModels_Symbols_Tbl_Rows);

                    for (int symbolsTbl_row_ctr = 1; symbolsTbl_row_ctr <= symbolsTbl_row_count; symbolsTbl_row_ctr++)
                    {
                        if (util.GetText(driver, By.CssSelector("div[class^='symbols-result']>div[class^='symbols']>ul:nth-of-type(" + symbolsTbl_row_ctr + ")>li:nth-of-type(2)>a")).Equals(bxl_txt) && bxl_val == false)
                        {
                            //--- Expected Result: If BXL file is present, ECAD Model column is not empty ---//
                            test.validateElementIsPresent(driver, By.CssSelector("div[class^='symbols-result']>div[class^='symbols']>ul:nth-of-type(" + symbolsTbl_row_ctr + ")>li:nth-of-type(3)>a"));

                            bxl_val = true;
                        }

                        if (util.GetText(driver, By.CssSelector("div[class^='symbols-result']>div[class^='symbols']>ul:nth-of-type(" + symbolsTbl_row_ctr + ")>li:nth-of-type(2)>a")).Equals(zip_txt) && zip_val == false)
                        {
                            //--- Expected Result: IF ZIP file is present, ECAD Model column is empty ---//
                            test.validateElementIsNotPresent(driver, By.CssSelector("div[class^='symbols-result']>div[class^='symbols']>ul:nth-of-type(" + symbolsTbl_row_ctr + ")>li:nth-of-type(3)>a"));

                            zip_val = true;
                        }

                        if (bxl_val == true && zip_val == true)
                        {
                            break;
                        }
                    }

                    //----- REQUEST NEW FILE -----//

                    //----- Design Center - Symbols and Footprints TestPlan > R2 > T2: Select all items in the "Left Rail Navigation" section of the page. -----//

                    //--- Expected Result: "Request New File" section should be displayed ---//
                    test.validateElementIsPresent(driver, Elements.DesignCenter_SymbolsFootprintsAnd3dModels_RequestNewFile_Sec);

                    if (util.CheckElement(driver, Elements.DesignCenter_SymbolsFootprintsAnd3dModels_RequestNewFile_Sec, 2))
                    {
                        //----- Design Center - Symbols and Footprints TestPlan > R2 > T3: Verify Symbol Search section -----//

                        //--- Expected Result: Product Part Number right to "Request New File" label should display based on the searched "item". ---//
                        test.validateStringInstance(driver, util.GetText(driver, Elements.DesignCenter_SymbolsFootprintsAnd3dModels_RequestNewFile_Sec_ProductPartNo_Txt), valid_symbolsSearch_input);

                        if (util.CheckElement(driver, Elements.DesignCenter_SymbolsFootprintsAnd3dModels_RequestNewFile_Sec_YourEmail_Txtbox, 2) && util.CheckElement(driver, Elements.DesignCenter_SymbolsFootprintsAnd3dModels_RequestNewFile_Sec_Send_Btn, 2))
                        {
                            //----- Design Center - Symbols and Footprints TestPlan > R2 > T4: Verify request new file section. -----//

                            //--- Action: Enter spaces in the email field. ---//
                            string invalid_email_input = " ";

                            action.IDeleteValueOnFields(driver, Elements.DesignCenter_SymbolsFootprintsAnd3dModels_RequestNewFile_Sec_YourEmail_Txtbox);
                            action.IType(driver, Elements.DesignCenter_SymbolsFootprintsAnd3dModels_RequestNewFile_Sec_YourEmail_Txtbox, invalid_email_input);

                            //--- Action: Click send button. ---//
                            action.IClick(driver, Elements.DesignCenter_SymbolsFootprintsAnd3dModels_RequestNewFile_Sec_Send_Btn);

                            //--- Expected Result: An error message should show. ---//
                            test.validateAlertIsPresent(driver);

                            //--- Action: Enter an invalid email address. ---//
                            invalid_email_input = "test_email_" + util.RandomString() + ".com";

                            action.IDeleteValueOnFields(driver, Elements.DesignCenter_SymbolsFootprintsAnd3dModels_RequestNewFile_Sec_YourEmail_Txtbox);
                            action.IType(driver, Elements.DesignCenter_SymbolsFootprintsAnd3dModels_RequestNewFile_Sec_YourEmail_Txtbox, invalid_email_input);

                            //--- Action: Click send button. ---//
                            action.IClick(driver, Elements.DesignCenter_SymbolsFootprintsAnd3dModels_RequestNewFile_Sec_Send_Btn);

                            //--- Expected Result: An error message should show. ---//
                            test.validateAlertIsPresent(driver);

                            //----- Design Center - Symbols and Footprints TestPlan > R2 > T3: Verify Symbol Search section -----//

                            //--- Action: Enter an email address ---//
                            string valid_email_input = "test_email_" + util.RandomString() + "@mailinator.com";

                            action.IDeleteValueOnFields(driver, Elements.DesignCenter_SymbolsFootprintsAnd3dModels_RequestNewFile_Sec_YourEmail_Txtbox);
                            action.IType(driver, Elements.DesignCenter_SymbolsFootprintsAnd3dModels_RequestNewFile_Sec_YourEmail_Txtbox, valid_email_input);

                            //--- Action: Click the Send button ---//
                            action.IClick(driver, Elements.DesignCenter_SymbolsFootprintsAnd3dModels_RequestNewFile_Sec_Send_Btn);

                            if (util.CheckElement(driver, Elements.DesignCenter_SymbolsFootprintsAnd3dModels_RequestNewFile_Sec_DialogBox, 2))
                            {
                                if (Locale.Equals("en"))
                                {
                                    //--- Expected Result: A pop up must appear saying "Request Sent Successfully". ---//
                                    test.validateStringIsCorrect(driver, Elements.DesignCenter_SymbolsFootprintsAnd3dModels_RequestNewFile_Sec_DialogBox_Success_Msg_txt, requestNewFile_success_msg_txt);
                                }
                                else
                                {
                                    test.validateElementIsPresent(driver, Elements.DesignCenter_SymbolsFootprintsAnd3dModels_RequestNewFile_Sec_DialogBox_Success_Msg_txt);
                                }

                                //--- Action: Click the X button on the pop up ---//
                                action.IClick(driver, Elements.DesignCenter_SymbolsFootprintsAnd3dModels_RequestNewFile_Sec_DialogBox_X_Btn);

                                //--- Expected Result: The pop-up will be closed. ---//
                                test.validateElementIsNotPresent(driver, Elements.DesignCenter_SymbolsFootprintsAnd3dModels_RequestNewFile_Sec_DialogBox);
                            }
                            else
                            {
                                test.validateElementIsPresent(driver, Elements.DesignCenter_SymbolsFootprintsAnd3dModels_RequestNewFile_Sec_DialogBox);
                            }

                            //----- Design Center - Symbols and Footprints TestPlan > R2 > T5.1: Verify Symbol Search section (AL-10300, IQ-7800/AL-14959) -----//

                            //--- Action: Enter generic part of LTC in the search bar ---//
                            string valid_symbolsSearch_ltc_input = "LTC5552";
                            //string valid_symbolsSearch_ltc_input = "LTC2630";
                            //string valid_symbolsSearch_ltc_input = "LTC3410B";

                            action.IDeleteValueOnFields(driver, Elements.DesignCenter_SymbolsFootprintsAnd3dModels_EcadModelsSearch_Search_Tbx);
                            action.IType(driver, Elements.DesignCenter_SymbolsFootprintsAnd3dModels_EcadModelsSearch_Search_Tbx, valid_symbolsSearch_ltc_input);

                            action.IClick(driver, Elements.DesignCenter_SymbolsFootprintsAnd3dModels_EcadModelsSearch_Search_Btn);

                            //--- Expected Result: It should display the symbol information for the specified generic part ---//
                            test.validateElementIsPresent(driver, Elements.DesignCenter_SymbolsFootprintsAnd3dModels_Symbols_Tbl);

                            //----- Design Center - Symbols and Footprints TestPlan > R2 > T6.1: Verify passing of LTC generic part in the URL -----//

                            //--- Action: Access the given URL ---//
                            action.Navigate(driver, Configuration.Env_Url + Locale + symbolsFootprintsAnd3dModels_ltc_genericPart_url);

                            //--- Expected Result: It should display the symbol information for the specified generic part ---//
                            test.validateElementIsPresent(driver, Elements.DesignCenter_SymbolsFootprintsAnd3dModels_Symbols_Tbl);

                            //----- Design Center - Symbols and Footprints TestPlan > R2 > T6.2: Verify passing of ADI generic part in the URL -----//

                            //--- Action: Access the given URL ---//
                            action.Navigate(driver, Configuration.Env_Url + Locale + symbolsFootprintsAnd3dModels_adi_genericPart_page_url);

                            //--- Expected Result: It should display the symbol information for the specified generic part in the form of a table ---//
                            test.validateElementIsPresent(driver, Elements.DesignCenter_SymbolsFootprintsAnd3dModels_Symbols_Tbl);
                        }
                        else
                        {
                            test.validateElementIsPresent(driver, Elements.DesignCenter_SymbolsFootprintsAnd3dModels_RequestNewFile_Sec_YourEmail_Txtbox);
                            test.validateElementIsPresent(driver, Elements.DesignCenter_SymbolsFootprintsAnd3dModels_RequestNewFile_Sec_Send_Btn);
                        }
                    }
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.DesignCenter_SymbolsFootprintsAnd3dModels_EcadModelsSearch_Search_Tbx);
                test.validateElementIsPresent(driver, Elements.DesignCenter_SymbolsFootprintsAnd3dModels_EcadModelsSearch_Search_Btn);
            }
        }
    }
}