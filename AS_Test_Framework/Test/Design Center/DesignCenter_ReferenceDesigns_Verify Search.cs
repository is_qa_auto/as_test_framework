﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.DesignCenter
{
    [TestFixture]
    public class DesignCenter_ReferenceDesigns_Search : BaseSetUp
    {
        public DesignCenter_ReferenceDesigns_Search() : base() { }

        //--- URLs ---//
        string referenceDesigns_page_url = "/design-center/reference-designs.html";
        string searchDesignCenter_page_url = "/design-center/search.html";
        string referenceDesigns_url_format_url = "/design-center/reference-designs/";
        string pdp_url_format = "/products/";
        string cftl_overview_url = "#rd-overview";
        string cftl_sampleProducts_url = "#rd-sampleproducts";

        //--- Labels ---//
        string relevancy_txt = "Relevancy";
        string referenceDesigns_txt = "Reference Designs";
        string designResources_txt = "Design Resources";
        string productCategories_txt = "Product Categories";
        string designAndIntegrationFiles_txt = "Design & Integration Files";
        string evaluationHardware_txt = "Evaluation Hardware";
        string fpgaHdl_txt = "FPGA/HDL";
        string deviceDrivers_txt = "Device Drivers";

        //--- Values ---//
        int searchResults_count = 10;

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Sections from Reference Designs Search is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Sections from Reference Designs Search is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Sections from Reference Designs Search is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Sections from Reference Designs Search is Present and Working as Expected in RU Locale")]
        public void DesignCenter_ReferenceDesigns_Search_VerifySectionsFromReferenceDesignsSearch(string Locale)
        {
            //--- Action: Go to Reference Design Home page ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + referenceDesigns_page_url);

            //----- Reference Designs Redesign_Test Plan > Desktop - RD_Onsite Search Tab > R1 > T1: Verify Onsite Search Results of Quick Search (NO Search Criteria) (IQ-9332/AL-16284) -----//

            //--- Action: Click the Search button without entering or selecting ANY search criteria ---//
            action.IDeleteValueOnFields(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec_SearchByKeywordOrDesignNumber_InputBx);
            action.IClick(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec_Search_Btn);
            Thread.Sleep(1000);

            //----- DESIGN CENTER - SEARCH RESULTS PAGE -----//
            if (util.CheckElement(driver, Elements.DesignCenter_Search_Sections_Section, 2))
            {
                //--- SECTIONS - SECTION ---//

                if (Locale.Equals("en"))
                {
                    //--- Expected Result: Reference Designs is pre-selected in Sections ---//
                    test.validateStringIsCorrect(driver, Elements.DesignCenter_Search_Selected_Section_Filter, referenceDesigns_txt);
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.DesignCenter_Search_Selected_Section_Filter);
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.DesignCenter_Search_Sections_Section);
            }
        }

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Input Box from Reference Designs Search is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Input Box from Reference Designs Search is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Input Box from Reference Designs Search is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Input Box from Reference Designs Search is Present and Working as Expected in RU Locale")]
        public void DesignCenter_ReferenceDesigns_Search_VerifyInputBoxFromReferenceDesignsSearch(string Locale)
        {
            //--- Action: Go to Reference Design Home page ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + referenceDesigns_page_url);

            //----- Reference Designs Redesign_Test Plan > Desktop - RD_Onsite Search Tab > R1 > T1: Verify Onsite Search Results of Quick Search (NO Search Criteria) (IQ-9332/AL-16284) -----//

            //--- Action: Click the Search button without entering or selecting ANY search criteria ---//
            action.IDeleteValueOnFields(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec_SearchByKeywordOrDesignNumber_InputBx);
            action.IClick(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec_Search_Btn);
            Thread.Sleep(1000);

            //----- DESIGN CENTER - SEARCH RESULTS PAGE -----//
            if (util.CheckElement(driver, Elements.DesignCenter_Search_InputBx, 2))
            {
                //--- SEARCH - INPUT BOX ---//

                //--- Expected Result: An asterisk is displayed in the search textbox ---//
                test.validateStringInstance(driver, "*", util.ReturnAttribute(driver, Elements.DesignCenter_Search_InputBx, "value"));
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.DesignCenter_Search_InputBx);
            }
        }

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Search Order Dropdown from Reference Designs Search is Present and Working as Expected in EN Locale", Category = "Smoke_ADIWeb")]
        [TestCase("cn", TestName = "Verify that the Search Order Dropdown from Reference Designs Search is Present and Working as Expected in CN Locale", Category = "Smoke_ADIWeb")]
        [TestCase("jp", TestName = "Verify that the Search Order Dropdown from Reference Designs Search is Present and Working as Expected in JP Locale", Category = "Smoke_ADIWeb")]
        [TestCase("ru", TestName = "Verify that the Search Order Dropdown from Reference Designs Search is Present and Working as Expected in RU Locale")]
        public void DesignCenter_ReferenceDesigns_Search_VerifySearchOrderDropdownFromReferenceDesignsSearch(string Locale)
        {
            string initial_steps = "1. Go to " + Configuration.Env_Url + Locale + referenceDesigns_page_url
                              +"<br>2. Click Search button";
            string scenario = "";

            //--- Action: Go to Reference Design Home page ---//
            action.INavigate(driver, Configuration.Env_Url + Locale + referenceDesigns_page_url);

            //----- Reference Designs Redesign_Test Plan > Desktop - RD_Onsite Search Tab > R1 > T1: Verify Onsite Search Results of Quick Search (NO Search Criteria) (IQ-9332/AL-16284) -----//

            //--- Action: Click the Search button without entering or selecting ANY search criteria ---//
            action.IDeleteValueOnFields(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec_SearchByKeywordOrDesignNumber_InputBx);
            action.IClick(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec_Search_Btn);
            Thread.Sleep(1000);

            //--- Expected Result: The Sort Order dropdown is displayed ---//
            scenario = " Verify that Search Order dropdown is present in reference design search";
            test.validateElementIsPresentv2(driver, Elements.DesignCenter_Search_SearchOrder_Dd,scenario, initial_steps);

            //----- DESIGN CENTER - SEARCH RESULTS PAGE -----//
            if (util.CheckElement(driver, Elements.DesignCenter_Search_SearchOrder_Dd, 2))
            {
                //--- SEARCH ORDER - DROPDOWN ---//

                if (Locale.Equals("en"))
                {
                    //--- Expected Result: The Search results are pre-sorted based on "Relevancy" ---//
                    scenario = "Verify that the default value of Search Order dropdown is correct";
                    test.validateSelectedValueIsCorrectv2(driver, Elements.DesignCenter_Search_SearchOrder_Dd, relevancy_txt, scenario, initial_steps);

                    //----- Design Center - Landing Page TestPlan > Test Case Title: Verify the Sort Menu (AL-6759) -----//

                    //--- Action: Click on the Sort menu ---//

                    //--- Expected Result: Newest will be available on the Dropdown ---//
                    scenario = "Verify that \"Newest\" value of Search Order dropdown is present";
                    test.validateElementIsPresentv2(driver, Elements.DesignCenter_Search_SearchOrder_Dd_Menu_Newest, scenario, initial_steps);

                    //--- Expected Result: Oldest will be available on the Dropdown ---//
                    scenario = "Verify that \"Oldest\" value of Search Order dropdown is present";
                    test.validateElementIsPresentv2(driver, Elements.DesignCenter_Search_SearchOrder_Dd_Menu_Oldest, scenario, initial_steps);

                    //--- Expected Result: Relevancy will be available on the Dropdown ---//
                    scenario = "Verify that \"Relevancy\" value of Search Order dropdown is present";
                    test.validateElementIsPresentv2(driver, Elements.DesignCenter_Search_SearchOrder_Dd_Menu_Relevancy, scenario, initial_steps);
                }
            }
        }

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Search Results from Reference Designs Search is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Search Results from Reference Designs Search is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Search Results from Reference Designs Search is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Search Results from Reference Designs Search is Present and Working as Expected in RU Locale")]
        public void DesignCenter_ReferenceDesigns_Search_VerifySearchResultsFromReferenceDesignsSearch(string Locale)
        {
            //--- Action: Go to Reference Design Home page ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + referenceDesigns_page_url);

            //----- Reference Designs Redesign_Test Plan > Desktop - RD_Onsite Search Tab > R1 > T1: Verify Onsite Search Results of Quick Search (NO Search Criteria) (IQ-9332/AL-16284) -----//

            //--- Action: Click the Search button without entering or selecting ANY search criteria ---//
            action.IDeleteValueOnFields(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec_SearchByKeywordOrDesignNumber_InputBx);
            action.IClick(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec_Search_Btn);
            Thread.Sleep(1000);

            if (driver.Url.Contains("analog.com/" + Locale + searchDesignCenter_page_url))
            {
                //--- Expected Result: The System displays the Reference Designs search results page (with all results) ---//
                test.validateElementIsPresent(driver, Elements.DesignCenter_Search_SearchResults_Items);

                if (util.CheckElement(driver, Elements.DesignCenter_Search_SearchResults_Items, 2))
                {
                    //----- SEARCH RESULTS - ITEMS -----//

                    //--- Expected Result: Each page in the search result displays up to 10 results ---//
                    test.validateCountIsEqual(driver, searchResults_count, util.GetCount(driver, Elements.DesignCenter_Search_SearchResults_Items));

                    //--- Expected Result: The search results are in a collapsed state ---//
                    test.validateElementIsNotPresent(driver, Elements.DesignCenter_Search_Expanded_SearchResults_Items);

                    if (!util.CheckElement(driver, Elements.DesignCenter_Search_Expanded_SearchResults_Items, 2))
                    {
                        //--- Action: Hover on the collapsed container ---//
                        action.IMouseOverTo(driver, Elements.DesignCenter_Search_SearchResults_Items);

                        //--- Expected Result: The background color of the collapsed container is changed to f4fdfe ---//
                        test.validateColorIsCorrect(driver, util.GetCssValue(driver, By.CssSelector("div[class='search-results-items']>div>div[class^='search-results-item']>div:nth-of-type(1)"), "background-color"), "#F4FDFE");
                    }

                    //--- Expected Result: There are three (3) columns of information in the collapsed state: ---//
                    if (Locale.Equals("en"))
                    {
                        //--- 1. Reference Designs ---//
                        test.validateStringIsCorrect(driver, Elements.DesignCenter_Search_SearchResults_Items_ReferenceDesigns_Col_Header_Txt, referenceDesigns_txt);

                        //--- 2. Design Resources ---//
                        test.validateStringIsCorrect(driver, Elements.DesignCenter_Search_SearchResults_Items_DesignResources_Col_Header_Txt, designResources_txt);

                        //--- 3. Product Categories ---//
                        test.validateStringIsCorrect(driver, Elements.DesignCenter_Search_SearchResults_Items_ProductCategories_Col_Header_Txt, productCategories_txt);
                    }
                    else
                    {
                        test.validateElementIsPresent(driver, Elements.DesignCenter_Search_SearchResults_Items_ReferenceDesigns_Col_Header_Txt);
                        test.validateElementIsPresent(driver, Elements.DesignCenter_Search_SearchResults_Items_DesignResources_Col_Header_Txt);
                        test.validateElementIsPresent(driver, Elements.DesignCenter_Search_SearchResults_Items_ProductCategories_Col_Header_Txt);
                    }

                    //--- REFERENCE DESIGN NAME - LINK -----//

                    //--- Expected Result: The Reference Designs column includes the Reference Design Name ---//
                    test.validateElementIsPresent(driver, Elements.DesignCenter_Search_SearchResults_Items_ReferenceDesign_Name_Link);

                    if (util.CheckElement(driver, Elements.DesignCenter_Search_SearchResults_Items_ReferenceDesign_Name_Link, 2))
                    {
                        string referenceDesign_name = util.GetText(driver, Elements.DesignCenter_Search_SearchResults_Items_ReferenceDesign_Name_Link);

                        //--- Action: Click a Reference Design Name hyperlink ---//
                        action.IOpenLinkInNewTab(driver, Elements.DesignCenter_Search_SearchResults_Items_ReferenceDesign_Name_Link);
                        Thread.Sleep(4000);

                        //--- Expected Result: The System displays the Reference Design page ---//
                        test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + referenceDesigns_url_format_url);

                        if (driver.Url.Contains("analog.com/" + Locale + referenceDesigns_url_format_url))
                        {
                            //--- Expected Result: The Reference Design Name in the search results is the same as the Reference Design Name in the Reference Design page ---//
                            test.validateStringIsCorrect(driver, Elements.CFTL_No_Txt, referenceDesign_name);
                        }

                        driver.Close();
                        driver.SwitchTo().Window(driver.WindowHandles.First());
                    }

                    if (util.CheckElement(driver, Elements.DesignCenter_Search_SearchResults_Items_DesignResources_Links, 2) && Locale.Equals("en"))
                    {
                        int searchResults_items_count = util.GetCount(driver, Elements.DesignCenter_Search_SearchResults_Items);

                        for (int searchResults_items_ctr = 2; searchResults_items_ctr <= searchResults_items_count; searchResults_items_ctr++)
                        {
                            if (util.CheckElement(driver, By.CssSelector("div[class='search-results-items']>div>div[class^='search-results-item']:nth-of-type(" + searchResults_items_ctr + ") * ul[class='design-resources']"), 2))
                            {
                                int designResources_links_count = util.GetCount(driver, By.CssSelector("div[class='search-results-items']>div>div[class^='search-results-item']:nth-of-type(" + searchResults_items_ctr + ") * ul[class='design-resources']>li>a"));

                                bool designAndIntegrationFiles_link = false;
                                bool evaluationHardware_link = false;
                                bool fpgaHdl_link = false;
                                bool deviceDrivers_link = false;

                                for (int designResources_items_ctr = 1; designResources_items_ctr <= designResources_links_count; designResources_items_ctr++)
                                {
                                    if (designAndIntegrationFiles_link == true && evaluationHardware_link == true && fpgaHdl_link == true && deviceDrivers_link == true)
                                    {
                                        break;
                                    }
                                    else if (util.GetText(driver, By.CssSelector("div[class='search-results-items']>div>div[class^='search-results-item']:nth-of-type(" + searchResults_items_ctr + ") * ul[class='design-resources']>li:nth-of-type(" + designResources_items_ctr + ")>a")).Contains(designAndIntegrationFiles_txt))
                                    {
                                        //--- DESIGN & INTEGRATION FILES - LINK ---//
                                        designAndIntegrationFiles_link = true;
                                        string designAndIntegrationFiles_link_locator = "div[class='search-results-items']>div>div[class^='search-results-item']:nth-of-type(" + searchResults_items_ctr + ") * ul[class='design-resources']>li:nth-of-type(" + designResources_items_ctr + ")>a";

                                        //--- Action: Click a Design & Integration Files hyperlink ---//
                                        action.IOpenLinkInNewTab(driver, By.CssSelector(designAndIntegrationFiles_link_locator));
                                        Thread.Sleep(3000);

                                        //--- Expected Result: The System displays the Overview (Design Resources) section of a Reference Design page ---//
                                        test.validateStringInstance(driver, driver.Url, cftl_overview_url);

                                        driver.Close();
                                        driver.SwitchTo().Window(driver.WindowHandles.First());
                                    }
                                    else if (util.GetText(driver, By.CssSelector("div[class='search-results-items']>div>div[class^='search-results-item']:nth-of-type(" + searchResults_items_ctr + ") * ul[class='design-resources']>li:nth-of-type(" + designResources_items_ctr + ")>a")).Contains(evaluationHardware_txt))
                                    {
                                        //--- EVALUATION HARDWARE - LINK ---//
                                        evaluationHardware_link = true;
                                        string evaluationHardware_link_locator = "div[class='search-results-items']>div>div[class^='search-results-item']:nth-of-type(" + searchResults_items_ctr + ") * ul[class='design-resources']>li:nth-of-type(" + designResources_items_ctr + ")>a";

                                        //--- Action: Click an Evaluation Hardware hyperlink ---//
                                        action.IOpenLinkInNewTab(driver, By.CssSelector(evaluationHardware_link_locator));
                                        Thread.Sleep(2000);

                                        //--- Expected Result: The System displays the Sample & Buy (Evaluation Boards) table of a Reference Design page ---//
                                        test.validateStringInstance(driver, driver.Url, cftl_sampleProducts_url);

                                        driver.Close();
                                        driver.SwitchTo().Window(driver.WindowHandles.First());
                                    }
                                    else if (util.GetText(driver, By.CssSelector("div[class='search-results-items']>div>div[class^='search-results-item']:nth-of-type(" + searchResults_items_ctr + ") * ul[class='design-resources']>li:nth-of-type(" + designResources_items_ctr + ")>a")).Contains(fpgaHdl_txt))
                                    {
                                        //--- FPGA/HDL - LINK ---//
                                        fpgaHdl_link = true;
                                        string fpgaHdl_link_locator = "div[class='search-results-items']>div>div[class^='search-results-item']:nth-of-type(" + searchResults_items_ctr + ") * ul[class='design-resources']>li:nth-of-type(" + designResources_items_ctr + ")>a";

                                        //--- Action: Click an FPGA/HDL hyperlink ---//
                                        action.IOpenLinkInNewTab(driver, By.CssSelector(fpgaHdl_link_locator));
                                        Thread.Sleep(2000);

                                        //--- Expected Result: The System displays the Overview (Design Resources) section of a Reference Design page ---//
                                        test.validateStringInstance(driver, driver.Url, cftl_overview_url);

                                        driver.Close();
                                        driver.SwitchTo().Window(driver.WindowHandles.First());
                                    }
                                    else if (util.GetText(driver, By.CssSelector("div[class='search-results-items']>div>div[class^='search-results-item']:nth-of-type(" + searchResults_items_ctr + ") * ul[class='design-resources']>li:nth-of-type(" + designResources_items_ctr + ")>a")).Contains(deviceDrivers_txt))
                                    {
                                        //--- DEVICE DRIVERS - LINK ---//
                                        deviceDrivers_link = true;
                                        string deviceDrivers_link_locator = "div[class='search-results-items']>div>div[class^='search-results-item']:nth-of-type(" + searchResults_items_ctr + ") * ul[class='design-resources']>li:nth-of-type(" + designResources_items_ctr + ")>a";

                                        //--- Action: Click a Device Drivers hyperlink ---//
                                        action.IOpenLinkInNewTab(driver, By.CssSelector(deviceDrivers_link_locator));
                                        Thread.Sleep(2000);

                                        //--- Expected Result: The System displays the Overview (Design Resources) section of a Reference Design page ---//
                                        test.validateStringInstance(driver, driver.Url, cftl_overview_url);

                                        driver.Close();
                                        driver.SwitchTo().Window(driver.WindowHandles.First());
                                    }
                                }

                                break;
                            }
                        }
                    }

                    //--- Action: Click in any blank space on a particular search result ---//
                    action.IClick(driver, Elements.DesignCenter_Search_SearchResults_Items);
                    Thread.Sleep(2000);

                    //--- Expected Result: The search result will expand ---//
                    test.validateElementIsPresent(driver, Elements.DesignCenter_Search_Expanded_SearchResults_Items);

                    if (util.CheckElement(driver, Elements.DesignCenter_Search_Expanded_SearchResults_Items, 2))
                    {
                        if (util.CheckElement(driver, Elements.DesignCenter_Search_SearchResults_Items_PartsUsed_Links, 2))
                        {
                            //--- Action: Click a Product hyperlink in the Parts Used section ---//
                            action.IOpenLinkInNewTab(driver, Elements.DesignCenter_Search_SearchResults_Items_PartsUsed_Links);
                            Thread.Sleep(4000);

                            //--- Expected Result: The System displays the corresponding Product Detail page ---//
                            test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + pdp_url_format);
                        }
                    }
                }
            }
            else
            {
                test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + searchDesignCenter_page_url);
            }
        }
    }
}