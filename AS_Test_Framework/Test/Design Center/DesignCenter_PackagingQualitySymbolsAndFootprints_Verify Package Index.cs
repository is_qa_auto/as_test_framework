﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Linq;

namespace AS_Test_Framework.DesignCenter
{
    [TestFixture]
    public class DesignCenter_PackagingQualitySymbolsAndFootprints_PackageIndex : BaseSetUp
    {
        public DesignCenter_PackagingQualitySymbolsAndFootprints_PackageIndex() : base() { }

        //--- URLs ---//
        string packageIndex_page_url = "/design-center/packaging-quality-symbols-footprints/package-index.html";
        string packageResources_page_url = "/package-resources.html";
        string analogDevicesQualityAndReliabilityProgram_page_url = "/quality-reliability.html";
        string symbolsFootprintsAnd3dModels_page_url = "/symbols-and-footprints.html";

        //--- Labels ---//
        string packageIndex_txt = "Package Index";

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Breadcrumb is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Breadcrumb is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Breadcrumb is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Breadcrumb is Present and Working as Expected in RU Locale")]
        public void DesignCenter_PackagingQualitySymbolsAndFootprints_PackageIndex_VerifyBreadcrumb(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + packageIndex_page_url);

            //----- Design Center - Packaging TestPlan > R1 > T2: Verify "Breadcrumbs" -----//

            //--- Expected Result: You should be able to see the visited page. ---//
            test.validateElementIsPresent(driver, Elements.Breadcrumb_Details);

            if (util.CheckElement(driver, Elements.Breadcrumb_Details, 2))
            {
                //--- Expected Result: The current page in The breadcrumbs hierarchy should have The same info as with The page header. ---//
                test.validateStringIsCorrect(driver, Elements.Breadcrumb_CurrentPage_Txt, util.GetText(driver, Elements.DesignCenter_PackageIndex_Page_Title_Txt));
            }
        }

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Left Rail Navigation is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Left Rail Navigation is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Left Rail Navigation is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Left Rail Navigation is Present and Working as Expected in RU Locale")]
        public void DesignCenter_PackagingQualitySymbolsAndFootprints_PackageIndex_VerifyLeftRailNavigation(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + packageIndex_page_url);

            //----- Design Center - Packaging TestPlan > R1 > T3: Verify "Left Rail Navigation" -----//

            if (Locale.Equals("en"))
            {
                //--- Expected Result: "Package Index" at the left rail should be highlighted. ---//
                test.validateStringIsCorrect(driver, Elements.DesignCenter_PackagingQualitySymbolsAndFootprints_LeftRailNav_Selected_Val_Link, packageIndex_txt);
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.DesignCenter_PackagingQualitySymbolsAndFootprints_LeftRailNav_Selected_Val_Link);
            }

            //--- Expected Result: The Package Index sub-category should be displayed. ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_PackagingQualitySymbolsAndFootprints_LeftRailNav_PackageIndex_Link);

            //--- Expected Result: The Package Resources sub-category should be displayed. ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_PackagingQualitySymbolsAndFootprints_LeftRailNav_PackageResources_Link);

            //--- Expected Result: The Quality & Reliability sub-category should be displayed. ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_PackagingQualitySymbolsAndFootprints_LeftRailNav_QualityAndReliability_Link);

            //--- Expected Result: The Symbols & Footprints sub-category should be displayed. ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_PackagingQualitySymbolsAndFootprints_LeftRailNav_SymbolsFootprintsAnd3dModels_Link);

            if (util.CheckElement(driver, Elements.DesignCenter_PackagingQualitySymbolsAndFootprints_LeftRailNav_PackageResources_Link, 2) && !Configuration.Browser.Equals("Chrome_HL"))
            {
                //--- Action: Select "Package Resource" sub-category. ---//
                action.IOpenLinkInNewTab(driver, Elements.DesignCenter_PackagingQualitySymbolsAndFootprints_LeftRailNav_PackageResources_Link);

                //--- Expected Result: Package Resources page is shown. ---//
                test.validateStringInstance(driver, driver.Url, packageResources_page_url);

                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles.First());
            }

            if (util.CheckElement(driver, Elements.DesignCenter_PackagingQualitySymbolsAndFootprints_LeftRailNav_QualityAndReliability_Link, 2) && !Configuration.Browser.Equals("Chrome_HL"))
            {
                //--- Action: Select "Quality and Reliability"  sub-category. ---//
                action.IOpenLinkInNewTab(driver, Elements.DesignCenter_PackagingQualitySymbolsAndFootprints_LeftRailNav_QualityAndReliability_Link);

                //--- Expected Result: Quality & Reliability page is shown. ---//
                test.validateStringInstance(driver, driver.Url, analogDevicesQualityAndReliabilityProgram_page_url);

                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles.First());
            }

            if (util.CheckElement(driver, Elements.DesignCenter_PackagingQualitySymbolsAndFootprints_LeftRailNav_SymbolsFootprintsAnd3dModels_Link, 2) && !Configuration.Browser.Equals("Chrome_HL"))
            {
                //--- Action: Select "Symbols and Footprints" sub-category. ---//
                action.IOpenLinkInNewTab(driver, Elements.DesignCenter_PackagingQualitySymbolsAndFootprints_LeftRailNav_SymbolsFootprintsAnd3dModels_Link);

                //--- Expected Result: Symbols and Footprints page is shown. ---//
                test.validateStringInstance(driver, driver.Url, symbolsFootprintsAnd3dModels_page_url);

                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles.First());
            }

            if (util.CheckElement(driver, Elements.DesignCenter_PackagingQualitySymbolsAndFootprints_LeftRailNav_PackageIndex_Link, 2) && !Configuration.Browser.Equals("Chrome_HL"))
            {
                //--- Action: Select "Index" sub-category. ---//
                action.IOpenLinkInNewTab(driver, Elements.DesignCenter_PackagingQualitySymbolsAndFootprints_LeftRailNav_PackageIndex_Link);

                //--- Expected Result: Package Index page is shown. ---//
                test.validateStringInstance(driver, driver.Url, packageIndex_page_url);
            }
        }

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that Package Index is Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that Package Index is Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that Package Index is Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that Package Index is Working as Expected in RU Locale")]
        public void DesignCenter_PackagingQualitySymbolsAndFootprints_VerifyPackageIndex(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + packageIndex_page_url);

            //----- Design Center - Packaging TestPlan > R1 > T1: Verify "Package Index" page -----//

            //--- Expected Result: The Description / Descriptive Summary should be displayed in the page. ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_PackageIndex_Desc_Txt);

            //--- Expected Result: The Sub-Category header should be shown (Package Category). should be displayed in the page. ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_PackageIndex_SubCat_Header_Txts);

            //--- Expected Result: The "View More" links under each sub-category header. should be displayed in the page. ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_PackageIndex_SubCat_ViewMore_Links);

            //--- Expected Result: The "Link List" should be displayed in the page. ---//           
            test.validateElementIsPresent(driver, Elements.DesignCenter_PackageIndex_SubCat_Links);
        }
    }
}