﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.DesignCenter
{
    [TestFixture]
    public class DesignCenter_EvaluationHardwareAndSoftware_EvaluationAndDevelopmentPlatforms_Mems : BaseSetUp
    {
        public DesignCenter_EvaluationHardwareAndSoftware_EvaluationAndDevelopmentPlatforms_Mems() : base() { }

        //--- URLs ---//
        string mems_page_url = "/design-center/evaluation-hardware-and-software/evaluation-development-platforms/inertial-mems-sensor-evaluation-tools.html";
        string evalBoard_url_format = "/evaluation-boards-kits/";
        string pdp_url_format = "/products/";

        //--- Labels ---//
        string controllerBoards_txt = "Controller Boards";
        string compatibleProductEvaluationBoards_txt = "COMPATIBLE PRODUCT EVALUATION BOARDS";

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that Inertial MEMS Sensor Evaluation Tools is Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that Inertial MEMS Sensor Evaluation Tools is Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that Inertial MEMS Sensor Evaluation Tools is Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that Inertial MEMS Sensor Evaluation Tools is Working as Expected in RU Locale")]
        public void DesignCenter_EvaluationHardwareAndSoftware_EvaluationAndDevelopmentPlatforms_VerifyMems(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + mems_page_url);

            //----- R1 > T5_6: Verify Inertial MEMS Sensor Evaluation Tools page of Evaluation Platform page -----//

            //--- Expected Result: Page Title should be shown ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_Mems_Page_Title_Txt);

            //--- Expected Result: Description of the page should be shown ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_Mems_Desc_Txt);
        }

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Controller Boards Section is Present and Working as Expected in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Controller Boards Section is Present and Working as Expected in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Controller Boards Section is Present and Working as Expected in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Controller Boards Section is Present and Working as Expected in RU Locale")]
        public void DesignCenter_EvaluationHardwareAndSoftware_EvaluationAndDevelopmentPlatforms_VerifyControllerBoardsSection(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + mems_page_url);

            //----- CONTROLLER BOARDS -----//

            //----- R1 > T5_6: Verify Inertial MEMS Sensor Evaluation Tools page of Evaluation Platform page -----//

            if (util.CheckElement(driver, Elements.DesignCenter_Mems_Accordions, 2) && Locale.Equals("en"))
            {
                int mems_subsec_count = util.GetCount(driver, Elements.DesignCenter_Mems_SubSecs);

                for (int mems_ctr = 2; mems_ctr <= mems_subsec_count; mems_ctr++)
                {
                    if (util.GetText(driver, By.CssSelector("div[class$='column-content']>div:nth-of-type(" + mems_ctr + ") * div[class='accordion-title'] * h3")).Equals(controllerBoards_txt))
                    {
                        //--- Locators ---//
                        string accordion_title_txt_locator = "div[class$='column-content']>div:nth-of-type(" + mems_ctr + ") * div[class='accordion-title'] * h3";
                        string subAccordion_expand_btn_locator = "div[class$='column-content']>div:nth-of-type(" + mems_ctr + ") * a[class='expand-icon']";
                        string subAccordion_collapse_btn_locator = "div[class$='column-content']>div:nth-of-type(" + mems_ctr + ") * a[class='close-icon']";
                        string expanded_subAccordion_locator = "div[class$='column-content']>div:nth-of-type(" + mems_ctr + ") * div[class='section-bar']";
                        string subAccordion_view_btn_locator = "div[class$='column-content']>div:nth-of-type(" + mems_ctr + ") * button";

                        //--- Expected Result: Controller Boards section title and items should be shown ---//
                        test.validateElementIsPresent(driver, By.CssSelector(accordion_title_txt_locator));

                        if (util.CheckElement(driver, By.CssSelector(subAccordion_expand_btn_locator), 2))
                        {
                            //--- Action: Click on an arrow down of any contreller boards ---//
                            action.IClick(driver, By.CssSelector(subAccordion_expand_btn_locator));

                            //--- Expected Result: Details in bullet or paragraph format and Image of the Controller board should be shown ---//
                            test.validateElementIsPresent(driver, By.CssSelector(expanded_subAccordion_locator));

                            //--- Expected Result: Arrow down will change to arrow up ---//
                            test.validateElementIsPresent(driver, By.CssSelector(subAccordion_collapse_btn_locator));

                            if (util.CheckElement(driver, By.CssSelector(expanded_subAccordion_locator), 2))
                            {
                                if (util.CheckElement(driver, By.CssSelector(subAccordion_view_btn_locator), 2))
                                {
                                    //--- Action: Click on view button of any controller board ---//
                                    action.IOpenLinkInNewTab(driver, By.CssSelector(subAccordion_view_btn_locator));
                                    Thread.Sleep(1000);

                                    //--- Expected Result: It should redirect on Evaluation Board Detail page ---//
                                    test.validateStringInstance(driver, driver.Url, evalBoard_url_format);

                                    driver.Close();
                                    driver.SwitchTo().Window(driver.WindowHandles.First());
                                }

                                if (util.CheckElement(driver, By.CssSelector(subAccordion_collapse_btn_locator), 2))
                                {
                                    //--- Action: Click on Arrow up of an Controller Board ---//
                                    action.IClick(driver, By.CssSelector(subAccordion_collapse_btn_locator));

                                    //--- Expected Result: Details of the controller board should be hidden ---//
                                    test.validateElementIsNotPresent(driver, By.CssSelector(expanded_subAccordion_locator));
                                }
                                else
                                {
                                    test.validateElementIsPresent(driver, By.CssSelector(subAccordion_collapse_btn_locator));
                                }
                            }
                        }
                        else
                        {
                            test.validateElementIsPresent(driver, By.CssSelector(subAccordion_expand_btn_locator));
                        }

                        break;
                    }
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.DesignCenter_Mems_Accordions);
            }
        }

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the COMPATIBLE PRODUCT EVALUATION BOARDS Section is Present and Working as Expected in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the COMPATIBLE PRODUCT EVALUATION BOARDS Section is Present and Working as Expected in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the COMPATIBLE PRODUCT EVALUATION BOARDS Section is Present and Working as Expected in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the COMPATIBLE PRODUCT EVALUATION BOARDS Section is Present and Working as Expected in RU Locale")]
        public void DesignCenter_EvaluationHardwareAndSoftware_EvaluationAndDevelopmentPlatforms_VerifyCompatibleProductEvaluationBoardsSection(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + mems_page_url);

            //----- COMPATIBLE PRODUCT EVALUATION BOARDS -----//

            //----- R1 > T5_6: Verify Inertial MEMS Sensor Evaluation Tools page of Evaluation Platform page -----//

            if (util.CheckElement(driver, Elements.DesignCenter_Mems_Accordions, 2) && Locale.Equals("en"))
            {
                int mems_subsec_count = util.GetCount(driver, Elements.DesignCenter_Mems_SubSecs);

                for (int mems_ctr = 2; mems_ctr <= mems_subsec_count; mems_ctr++)
                {
                    if (util.GetText(driver, By.CssSelector("div[class$='column-content']>div:nth-of-type(" + mems_ctr + ") * h3[class^='header']")).Equals(compatibleProductEvaluationBoards_txt))
                    {
                        //--- Locators ---//
                        string sec_desc_txt_locator = "div[class$='column-content']>div:nth-of-type(" + mems_ctr + ") * div[class$='description']";
                        string accordion_recommendedController_txt_locator = "div[class$='column-content']>div:nth-of-type(" + mems_ctr + ") * h5[class='pull-right']";
                        string accordion_expand_btn_locator = "div[class$='column-content']>div:nth-of-type(" + mems_ctr + ") * a[class='expand']";
                        string accordion_catName_txt_locator = "div[class$='column-content']>div:nth-of-type(" + mems_ctr + ") * div[class^='section-bar'] * h5";
                        string accordion_product_links_locator = "div[class$='column-content']>div:nth-of-type(" + mems_ctr + ") * a[href*='" + pdp_url_format + "']";

                        //--- Expected Result: COMPATIBLE CIRCUITS FROM THE LAB™ EVALUATION BOARDS section should have Section description ---//
                        test.validateElementIsPresent(driver, By.CssSelector(sec_desc_txt_locator));

                        //--- Expected Result: COMPATIBLE CIRCUITS FROM THE LAB™ EVALUATION BOARDS section should have Recommended Controller ---//
                        test.validateElementIsPresent(driver, By.CssSelector(accordion_recommendedController_txt_locator));

                        //--- Expected Result: COMPATIBLE CIRCUITS FROM THE LAB™ EVALUATION BOARDS section should have Arrow down ---//
                        test.validateElementIsPresent(driver, By.CssSelector(accordion_expand_btn_locator));

                        //--- Expected Result: COMPATIBLE CIRCUITS FROM THE LAB™ EVALUATION BOARDS section should have Category Name ---//
                        test.validateElementIsPresent(driver, By.CssSelector(accordion_catName_txt_locator));

                        if (util.CheckElement(driver, By.CssSelector(accordion_expand_btn_locator), 2))
                        {
                            action.IClick(driver, By.CssSelector(accordion_expand_btn_locator));

                            //--- Expected Result: COMPATIBLE CIRCUITS FROM THE LAB EVALUATION BOARDS  should have Compatible Product ---//
                            test.validateElementIsPresent(driver, By.CssSelector(accordion_product_links_locator));

                            if (util.CheckElement(driver, By.CssSelector(accordion_product_links_locator), 2))
                            {
                                //--- Action: Click on a Product model  link ---//
                                action.IOpenLinkInNewTab(driver, By.CssSelector(accordion_product_links_locator));
                                Thread.Sleep(2000);

                                //--- Expected Result: It should redirect on Product  detail page ---//
                                test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + pdp_url_format);
                            }
                        }

                        break;
                    }
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.DesignCenter_Mems_Accordions);
            }
        }
    }
}