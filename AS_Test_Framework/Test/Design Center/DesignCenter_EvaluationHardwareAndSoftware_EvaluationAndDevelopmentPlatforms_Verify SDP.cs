﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.DesignCenter
{
    [TestFixture]
    public class DesignCenter_EvaluationHardwareAndSoftware_EvaluationAndDevelopmentPlatforms_Sdp : BaseSetUp
    {
        public DesignCenter_EvaluationHardwareAndSoftware_EvaluationAndDevelopmentPlatforms_Sdp() : base() { }

        //--- URLs ---//
        string sdp_page_url = "/design-center/evaluation-hardware-and-software/evaluation-development-platforms/sdp.html";
        string evalBoard_url_format = "/evaluation-boards-kits/";
        string pdp_url_format = "/products/";
        string cftl_url_format = "/circuits-from-the-lab/";
        string compatibleProductEvaluationBoards_sec_url = "#Compatible-Boards";
        string circuitFromTheLab_sec_url = "#Circuit-From-Lab";

        //--- Labels ---//
        string controllerBoards_txt = "Controller Boards";
        string interposerBoards_txt = "Interposer Boards";
        string daughterBoards_txt = "Daughter Boards";
        string compatibleProductEvaluationBoards_txt = "COMPATIBLE PRODUCT EVALUATION BOARDS";
        string compatibleCircuitsFromTheLabEvaluationBoards_txt = "COMPATIBLE CIRCUITS FROM THE LAB™ EVALUATION BOARDS";

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that System Demonstration Platform (SDP) is Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that System Demonstration Platform (SDP) is Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that System Demonstration Platform (SDP) is Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that System Demonstration Platform (SDP) is Working as Expected in RU Locale")]
        public void DesignCenter_EvaluationHardwareAndSoftware_EvaluationAndDevelopmentPlatforms_VerifySdp(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + sdp_page_url);

            //----- R1 > T5_5: Verify System Demonstration Platform (SDP) Page  of Evaluation Platform page -----//

            if (util.CheckElement(driver, Elements.DesignCenter_Sdp_ViewCompatibleEvaluationBoards_Btn, 2))
            {
                //--- Action: Click on View Compatible Evaluation Boards button ---//
                action.IClick(driver, Elements.DesignCenter_Sdp_ViewCompatibleEvaluationBoards_Btn);

                //--- Expected Result: It should redirect on Compatible Evaluation Board section ---//
                test.validateStringInstance(driver, driver.Url, compatibleProductEvaluationBoards_sec_url);
            }
        }

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Controller Boards Section is Present and Working as Expected in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Controller Boards Section is Present and Working as Expected in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Controller Boards Section is Present and Working as Expected in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Controller Boards Section is Present and Working as Expected in RU Locale")]
        public void DesignCenter_EvaluationHardwareAndSoftware_EvaluationAndDevelopmentPlatforms_VerifyControllerBoardsSection(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + sdp_page_url);

            //----- CONTROLLER BOARDS -----//

            //----- R1 > T5_5: Verify System Demonstration Platform (SDP) Page  of Evaluation Platform page -----//

            if (util.CheckElement(driver, Elements.DesignCenter_Sdp_Accordions, 2) && Locale.Equals("en"))
            {
                int sdp_subsec_count = util.GetCount(driver, Elements.DesignCenter_Sdp_SubSecs);

                for (int sdp_ctr = 1; sdp_ctr <= sdp_subsec_count; sdp_ctr++)
                {
                    if (util.GetText(driver, By.CssSelector("div[class$='column-content']>div:nth-of-type(" + sdp_ctr + ") * div[class='accordion-title'] * h3")).Equals(controllerBoards_txt))
                    {
                        //--- Locators ---//
                        string accordion_title_txt_locator = "div[class$='column-content']>div:nth-of-type(" + sdp_ctr + ") * div[class='accordion-title'] * h3";
                        string subAccordion_title_txt_locator = "div[class$='column-content']>div:nth-of-type(" + sdp_ctr + ") * div[class^='section-bar'] * h5";
                        string subAccordion_expand_btn_locator = "div[class$='column-content']>div:nth-of-type(" + sdp_ctr + ") * a[class='expand-icon']";
                        string subAccordion_collapse_btn_locator = "div[class$='column-content']>div:nth-of-type(" + sdp_ctr + ") * a[class='close-icon']";
                        string expanded_subAccordion_locator = "div[class$='column-content']>div:nth-of-type(" + sdp_ctr + ") * div[class='section-bar']";
                        string subAccordion_view_btn_locator = "div[class$='column-content']>div:nth-of-type(" + sdp_ctr + ") * button";

                        //--- Expected Result: Controller Board Title should be shown ---//
                        test.validateElementIsPresent(driver, By.CssSelector(accordion_title_txt_locator));

                        //--- Expected Result: Controller  Boards Items should have a Title ---//
                        test.validateElementIsPresent(driver, By.CssSelector(subAccordion_title_txt_locator));

                        //--- Expected Result: Controller  Boards Items should have a arrow down ---//
                        test.validateElementIsPresent(driver, By.CssSelector(subAccordion_expand_btn_locator));

                        if (util.CheckElement(driver, By.CssSelector(subAccordion_expand_btn_locator), 2))
                        {
                            //--- Action: Click on arrow down of any Controller Boards items ---//
                            action.IClick(driver, By.CssSelector(subAccordion_expand_btn_locator));

                            //--- Expected Result: Controller  board items should be maximize ---//
                            test.validateElementIsPresent(driver, By.CssSelector(expanded_subAccordion_locator));

                            //--- Expected Result: Arrow down will change to arrow up ---//
                            test.validateElementIsPresent(driver, By.CssSelector(subAccordion_collapse_btn_locator));

                            if (util.CheckElement(driver, By.CssSelector(expanded_subAccordion_locator), 2))
                            {
                                if (util.CheckElement(driver, By.CssSelector(subAccordion_view_btn_locator), 2))
                                {
                                    //--- Action: Click on any Controller  Boards view button ---//
                                    action.IOpenLinkInNewTab(driver, By.CssSelector(subAccordion_view_btn_locator));
                                    Thread.Sleep(1000);

                                    //--- Expected Result: It should redirect on its Evaluation board detail page ---//
                                    test.validateStringInstance(driver, driver.Url, evalBoard_url_format);

                                    driver.Close();
                                    driver.SwitchTo().Window(driver.WindowHandles.First());
                                }

                                if (util.CheckElement(driver, By.CssSelector(subAccordion_collapse_btn_locator), 2))
                                {
                                    //--- Action: Click on arrow up of any Controller Boards boards ---//
                                    action.IClick(driver, By.CssSelector(subAccordion_collapse_btn_locator));

                                    //--- Expected Result: The InterPoser Boards should be hidden ---//
                                    test.validateElementIsNotPresent(driver, By.CssSelector(expanded_subAccordion_locator));

                                    //--- Expected Result: Arrow will change to arrow down ---//
                                    test.validateElementIsPresent(driver, By.CssSelector(subAccordion_expand_btn_locator));
                                }
                                else
                                {
                                    test.validateElementIsPresent(driver, By.CssSelector(subAccordion_collapse_btn_locator));
                                }
                            }
                        }

                        break;
                    }
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.DesignCenter_Sdp_Accordions);
            }
        }

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Interposer Boards Section is Present and Working as Expected in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Interposer Boards Section is Present and Working as Expected in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Interposer Boards Section is Present and Working as Expected in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Interposer Boards Section is Present and Working as Expected in RU Locale")]
        public void DesignCenter_EvaluationHardwareAndSoftware_EvaluationAndDevelopmentPlatforms_VerifyInterposerBoardsSection(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + sdp_page_url);

            //----- INTERPOSER BOARDS -----//

            //----- R1 > T5_5: Verify System Demonstration Platform (SDP) Page  of Evaluation Platform page -----//

            if (util.CheckElement(driver, Elements.DesignCenter_Sdp_Accordions, 2) && Locale.Equals("en"))
            {
                int sdp_subsec_count = util.GetCount(driver, Elements.DesignCenter_Sdp_SubSecs);

                for (int sdp_ctr = 1; sdp_ctr <= sdp_subsec_count; sdp_ctr++)
                {
                    if (util.GetText(driver, By.CssSelector("div[class$='column-content']>div:nth-of-type(" + sdp_ctr + ") * div[class='accordion-title'] * h3")).Equals(interposerBoards_txt))
                    {
                        //--- Locators ---//
                        string accordion_title_txt_locator = "div[class$='column-content']>div:nth-of-type(" + sdp_ctr + ") * div[class='accordion-title'] * h3";
                        string subAccordion_title_txt_locator = "div[class$='column-content']>div:nth-of-type(" + sdp_ctr + ") * div[class^='section-bar'] * h5";
                        string subAccordion_expand_btn_locator = "div[class$='column-content']>div:nth-of-type(" + sdp_ctr + ") * a[class='expand-icon']";
                        string subAccordion_collapse_btn_locator = "div[class$='column-content']>div:nth-of-type(" + sdp_ctr + ") * a[class='close-icon']";
                        string expanded_subAccordion_locator = "div[class$='column-content']>div:nth-of-type(" + sdp_ctr + ") * div[class='section-bar']";
                        string subAccordion_view_btn_locator = "div[class$='column-content']>div:nth-of-type(" + sdp_ctr + ") * button";

                        //--- Expected Result: InterPoser Boards Title should be shown ---//
                        test.validateElementIsPresent(driver, By.CssSelector(accordion_title_txt_locator));

                        //--- Expected Result: InterPoser Boards Items should have a Title ---//
                        test.validateElementIsPresent(driver, By.CssSelector(subAccordion_title_txt_locator));

                        //--- Expected Result: InterPoser Boards Items should have a arrow down ---//
                        test.validateElementIsPresent(driver, By.CssSelector(subAccordion_expand_btn_locator));

                        if (util.CheckElement(driver, By.CssSelector(subAccordion_expand_btn_locator), 2))
                        {
                            //--- Action: Click on any arrow down of  InterPoser Boards items ---//
                            action.IClick(driver, By.CssSelector(subAccordion_expand_btn_locator));

                            //--- Expected Result: InterPoser  board items should be maximize ---//
                            test.validateElementIsPresent(driver, By.CssSelector(expanded_subAccordion_locator));

                            //--- Expected Result: Arrow down will change to arrow up ---//
                            test.validateElementIsPresent(driver, By.CssSelector(subAccordion_collapse_btn_locator));

                            if (util.CheckElement(driver, By.CssSelector(expanded_subAccordion_locator), 2))
                            {
                                if (util.CheckElement(driver, By.CssSelector(subAccordion_view_btn_locator), 2))
                                {
                                    //--- Action: Click on any InterPoser Boards view button ---//
                                    action.IOpenLinkInNewTab(driver, By.CssSelector(subAccordion_view_btn_locator));
                                    Thread.Sleep(2000);

                                    //--- Expected Result: It should redirect on the InterPoser  Boards Detail Page ---//
                                    test.validateStringInstance(driver, driver.Url, evalBoard_url_format);

                                    driver.Close();
                                    driver.SwitchTo().Window(driver.WindowHandles.First());
                                }

                                if (util.CheckElement(driver, By.CssSelector(subAccordion_collapse_btn_locator), 2))
                                {
                                    //--- Action: Click on arrow up of any InterPoser Boards boards ---//
                                    action.IClick(driver, By.CssSelector(subAccordion_collapse_btn_locator));

                                    //--- Expected Result: The InterPoser Boards should be hidden ---//
                                    test.validateElementIsNotPresent(driver, By.CssSelector(expanded_subAccordion_locator));

                                    //--- Expected Result: Arrow will change to arrow down ---//
                                    test.validateElementIsPresent(driver, By.CssSelector(subAccordion_expand_btn_locator));
                                }
                                else
                                {
                                    test.validateElementIsPresent(driver, By.CssSelector(subAccordion_collapse_btn_locator));
                                }
                            }
                        }

                        break;
                    }
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.DesignCenter_Sdp_Accordions);
            }
        }

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Daughter Boards Section is Present and Working as Expected in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Daughter Boards Section is Present and Working as Expected in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Daughter Boards Section is Present and Working as Expected in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Daughter Boards Section is Present and Working as Expected in RU Locale")]
        public void DesignCenter_EvaluationHardwareAndSoftware_EvaluationAndDevelopmentPlatforms_VerifyDaughterBoardsSection(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + sdp_page_url);

            //----- DAUGHTER BOARDS -----//

            //----- R1 > T5_5: Verify System Demonstration Platform (SDP) Page  of Evaluation Platform page -----//

            if (util.CheckElement(driver, Elements.DesignCenter_Sdp_Accordions, 2) && Locale.Equals("en"))
            {
                int sdp_subsec_count = util.GetCount(driver, Elements.DesignCenter_Sdp_SubSecs);

                for (int sdp_ctr = 1; sdp_ctr <= sdp_subsec_count; sdp_ctr++)
                {
                    if (util.GetText(driver, By.CssSelector("div[class$='column-content']>div:nth-of-type(" + sdp_ctr + ") * div[class='accordion-title'] * h3")).Equals(daughterBoards_txt))
                    {
                        //--- Locators ---//
                        string accordion_title_txt_locator = "div[class$='column-content']>div:nth-of-type(" + sdp_ctr + ") * div[class='accordion-title'] * h3";
                        string subAccordion_title_txt_locator = "div[class$='column-content']>div:nth-of-type(" + sdp_ctr + ") * div[class^='section-bar'] * h5";
                        string subAccordion_expand_btn_locator = "div[class$='column-content']>div:nth-of-type(" + sdp_ctr + ") * a[class='expand-icon']";
                        string subAccordion_collapse_btn_locator = "div[class$='column-content']>div:nth-of-type(" + sdp_ctr + ") * a[class='close-icon']";
                        string expanded_subAccordion_locator = "div[class$='column-content']>div:nth-of-type(" + sdp_ctr + ") * div[class='section-bar']";
                        string subAccordion_view_btn_locator = "div[class$='column-content']>div:nth-of-type(" + sdp_ctr + ") * button";

                        //--- Expected Result: Daughter Boards Title should be shown ---//
                        test.validateElementIsPresent(driver, By.CssSelector(accordion_title_txt_locator));

                        //--- Expected Result: Daughter Boards Items should have a Title ---//
                        test.validateElementIsPresent(driver, By.CssSelector(subAccordion_title_txt_locator));

                        //--- Expected Result: Daughter Boards Items should have a arrow down on the right ---//
                        test.validateElementIsPresent(driver, By.CssSelector(subAccordion_expand_btn_locator));

                        if (util.CheckElement(driver, By.CssSelector(subAccordion_expand_btn_locator), 2))
                        {
                            //--- Action: Click on arrow down of  Daughter Boards items ---//
                            action.IClick(driver, By.CssSelector(subAccordion_expand_btn_locator));

                            //--- Expected Result: Daughter  board items should be maximize ---//
                            test.validateElementIsPresent(driver, By.CssSelector(expanded_subAccordion_locator));

                            //--- Expected Result: Arrow down will change to arrow up ---//
                            test.validateElementIsPresent(driver, By.CssSelector(subAccordion_collapse_btn_locator));

                            if (util.CheckElement(driver, By.CssSelector(expanded_subAccordion_locator), 2))
                            {
                                if (util.CheckElement(driver, Elements.DesignCenter_Sdp_ViewSDPCompatibleProductEvaluationBoards_Link, 2))
                                {
                                    //--- Action: Click on View SDP Compatible Reference Circuit button ---//
                                    action.IClick(driver, Elements.DesignCenter_Sdp_ViewSDPCompatibleProductEvaluationBoards_Link);

                                    //--- Expected Result: It should anchor down on COMPATIBLE CIRCUITS FROM THE LAB EVALUATION BOARDS section. ---//
                                    test.validateStringInstance(driver, driver.Url, compatibleProductEvaluationBoards_sec_url);
                                }

                                action.IClick(driver, By.CssSelector("div[class$='column-content']>div:nth-of-type(5) * div[class^='adi-accordion']>div:nth-last-of-type(1) * a[class='expand-icon']"));

                                if (util.CheckElement(driver, Elements.DesignCenter_Sdp_ViewSDPCompatibleReferenceCircuits_Link, 2))
                                {
                                    //--- Action: Click on View SDP Compatible Product Evaluation Boards ---//
                                    action.IClick(driver, Elements.DesignCenter_Sdp_ViewSDPCompatibleReferenceCircuits_Link);

                                    //--- Expected Result: It should anchor down on COMPATIBLE PRODUCT EVALUATION BOARDS section. ---//
                                    test.validateStringInstance(driver, driver.Url, circuitFromTheLab_sec_url);
                                }
                            }
                        }

                        break;
                    }
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.DesignCenter_Sdp_Accordions);
            }
        }

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the COMPATIBLE PRODUCT EVALUATION BOARDS Section is Present and Working as Expected in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the COMPATIBLE PRODUCT EVALUATION BOARDS Section is Present and Working as Expected in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the COMPATIBLE PRODUCT EVALUATION BOARDS Section is Present and Working as Expected in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the COMPATIBLE PRODUCT EVALUATION BOARDS Section is Present and Working as Expected in RU Locale")]
        public void DesignCenter_EvaluationHardwareAndSoftware_EvaluationAndDevelopmentPlatforms_VerifyCompatibleProductEvaluationBoardsSection(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + sdp_page_url);

            //----- COMPATIBLE PRODUCT EVALUATION BOARDS -----//

            //----- R1 > T5_5: Verify System Demonstration Platform (SDP) Page  of Evaluation Platform page -----//

            if (util.CheckElement(driver, Elements.DesignCenter_Sdp_Accordions, 2) && Locale.Equals("en"))
            {
                int sdp_subsec_count = util.GetCount(driver, Elements.DesignCenter_Sdp_SubSecs);

                for (int sdp_ctr = 1; sdp_ctr <= sdp_subsec_count; sdp_ctr++)
                {
                    if (util.GetText(driver, By.CssSelector("div[class$='column-content']>div:nth-of-type(" + sdp_ctr + ") * h3[class^='header']")).Equals(compatibleProductEvaluationBoards_txt))
                    {
                        //--- Locators ---//
                        string sec_desc_txt_locator = "div[class$='column-content']>div:nth-of-type(" + sdp_ctr + ") * div[class$='description']";
                        string accordion_recommendedController_txt_locator = "div[class$='column-content']>div:nth-of-type(" + sdp_ctr + ") * h5[class='pull-right']";
                        string accordion_expand_btn_locator = "div[class$='column-content']>div:nth-of-type(" + sdp_ctr + ") * a[class='expand']";
                        string accordion_catName_txt_locator = "div[class$='column-content']>div:nth-of-type(" + sdp_ctr + ") * div[class^='section-bar'] * h5";
                        string accordion_product_links_locator = "div[class$='column-content']>div:nth-of-type(" + sdp_ctr + ") * a[href*='" + pdp_url_format + "']";
                        
                        //--- Expected Result: COMPATIBLE CIRCUITS FROM THE LAB™ EVALUATION BOARDS section should have Section description ---//
                        test.validateElementIsPresent(driver, By.CssSelector(sec_desc_txt_locator));

                        //--- Expected Result: COMPATIBLE CIRCUITS FROM THE LAB™ EVALUATION BOARDS section should have Recommended Controller ---//
                        test.validateElementIsPresent(driver, By.CssSelector(accordion_recommendedController_txt_locator));

                        //--- Expected Result: COMPATIBLE CIRCUITS FROM THE LAB™ EVALUATION BOARDS section should have Arrow down ---//
                        test.validateElementIsPresent(driver, By.CssSelector(accordion_expand_btn_locator));

                        //--- Expected Result: COMPATIBLE CIRCUITS FROM THE LAB™ EVALUATION BOARDS section should have Category Name ---//
                        test.validateElementIsPresent(driver, By.CssSelector(accordion_catName_txt_locator));

                        if (util.CheckElement(driver, By.CssSelector(accordion_expand_btn_locator), 2))
                        {
                            action.IClick(driver, By.CssSelector(accordion_expand_btn_locator));

                            //--- Expected Result: COMPATIBLE CIRCUITS FROM THE LAB EVALUATION BOARDS  should have Compatible Product ---//
                            test.validateElementIsPresent(driver, By.CssSelector(accordion_product_links_locator));

                            if (util.CheckElement(driver, By.CssSelector(accordion_product_links_locator), 2))
                            {
                                //--- Action: Click on a Product model  link ---//
                                action.IOpenLinkInNewTab(driver, By.CssSelector(accordion_product_links_locator));
                                Thread.Sleep(2000);
                            
                                //--- Expected Result: It should redirect on Product  detail page ---//
                                test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + pdp_url_format);
                            }
                        }
               
                        break;
                    }
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.DesignCenter_Sdp_Accordions);
            }
        }

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the COMPATIBLE CIRCUITS FROM THE LAB EVALUATION BOARDS Section is Present and Working as Expected in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the COMPATIBLE CIRCUITS FROM THE LAB EVALUATION BOARDS Section is Present and Working as Expected in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the COMPATIBLE CIRCUITS FROM THE LAB EVALUATION BOARDS Section is Present and Working as Expected in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the COMPATIBLE CIRCUITS FROM THE LAB EVALUATION BOARDS Section is Present and Working as Expected in RU Locale")]
        public void DesignCenter_EvaluationHardwareAndSoftware_EvaluationAndDevelopmentPlatforms_VerifyCompatibleCircuitsFromTheLabEvaluationBoardsSection(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + sdp_page_url);

            //----- COMPATIBLE CIRCUITS FROM THE LAB™ EVALUATION BOARDS -----//

            //----- R1 > T5_5: Verify System Demonstration Platform (SDP) Page  of Evaluation Platform page -----//

            if (util.CheckElement(driver, Elements.DesignCenter_Sdp_Accordions, 2) && Locale.Equals("en"))
            {
                int sdp_subsec_count = util.GetCount(driver, Elements.DesignCenter_Sdp_SubSecs);

                for (int sdp_ctr = 1; sdp_ctr <= sdp_subsec_count; sdp_ctr++)
                {
                    if (util.GetText(driver, By.CssSelector("div[class$='column-content']>div:nth-of-type(" + sdp_ctr + ") * h3[class^='header']")).Equals(compatibleCircuitsFromTheLabEvaluationBoards_txt))
                    {
                        //--- Locators ---//
                        string sec_desc_txt_locator = "div[class$='column-content']>div:nth-of-type(" + sdp_ctr + ") * div[class$='description']";
                        string accordion_recommendedController_txt_locator = "div[class$='column-content']>div:nth-of-type(" + sdp_ctr + ") * h5[class='pull-right']";
                        string accordion_expand_btn_locator = "div[class$='column-content']>div:nth-of-type(" + sdp_ctr + ") * a[class='expand']";
                        string accordion_appName_txt_locator = "div[class$='column-content']>div:nth-of-type(" + sdp_ctr + ") * div[class^='section-bar'] * h5";
                        string accordion_cftl_links_locator = "div[class$='column-content']>div:nth-of-type(" + sdp_ctr + ") * a[href*='" + cftl_url_format + "']";

                        //--- Expected Result: COMPATIBLE CIRCUITS FROM THE LAB EVALUATION BOARDS  should have Section description ---//
                        test.validateElementIsPresent(driver, By.CssSelector(sec_desc_txt_locator));

                        //--- Expected Result: COMPATIBLE CIRCUITS FROM THE LAB EVALUATION BOARDS  should have Recommended Controller ---//
                        test.validateElementIsPresent(driver, By.CssSelector(accordion_recommendedController_txt_locator));

                        //--- Expected Result: COMPATIBLE CIRCUITS FROM THE LAB EVALUATION BOARDS  should have Arrow down ---//
                        test.validateElementIsPresent(driver, By.CssSelector(accordion_expand_btn_locator));

                        //--- Expected Result: COMPATIBLE CIRCUITS FROM THE LAB EVALUATION BOARDS  should have Application Name ---//
                        test.validateElementIsPresent(driver, By.CssSelector(accordion_appName_txt_locator));
                        
                        if (util.CheckElement(driver, By.CssSelector(accordion_expand_btn_locator), 2))
                        {
                            action.IClick(driver, By.CssSelector(accordion_expand_btn_locator));

                            //--- Expected Result: COMPATIBLE PRODUCT EVALUATION BOARDS  should have Compatible CFTL ---//
                            test.validateElementIsPresent(driver, By.CssSelector(accordion_cftl_links_locator));

                            if (util.CheckElement(driver, By.CssSelector(accordion_cftl_links_locator), 2))
                            {
                                //--- Action: Click on a CFTL link ---//
                                action.IOpenLinkInNewTab(driver, By.CssSelector(accordion_cftl_links_locator));
                                Thread.Sleep(2000);

                                //--- Expected Result: It should redirect on CFTL  detail page ---//
                                test.validateStringInstance(driver, driver.Url, cftl_url_format);
                            }
                        }

                        break;
                    }
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.DesignCenter_Sdp_Accordions);
            }
        }
    }
}