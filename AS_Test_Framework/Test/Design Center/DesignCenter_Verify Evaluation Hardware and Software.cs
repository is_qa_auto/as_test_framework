﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.DesignCenter
{
    [TestFixture]
    public class DesignCenter_EvaluationHardwareAndSoftware : BaseSetUp
    {
        public DesignCenter_EvaluationHardwareAndSoftware() : base() { }

        //--- URLs ---//
        string evaluationHardwareAndSoftware_page_url = "/design-center/evaluation-hardware-and-software.html";
        string productEvaluationBoardsAndKits_page_url = "/evaluation-boards-kits.html";
        string evaluationAndDevelopmentPlatforms_page_url = "/evaluation-development-platforms.html";
        string ced_page_url = "/ced.html";
        string dpg_page_url = "/dpg.html";
        string sdp_page_url = "/sdp.html";

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that Evaluation Hardware & Software is Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that Evaluation Hardware & Software is Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that Evaluation Hardware & Software is Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that Evaluation Hardware & Software is Working as Expected in RU Locale")]
        public void DesignCenter_VerifyEvaluationHardwareAndSoftware(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + evaluationHardwareAndSoftware_page_url);

            //----- R1 > T2_1: Verify Evaluation  Board Landing page content -----//

            //--- Expected Result: Evaluation board landing page should contain the Title of Evaluation Hardware & Software landing page ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_EvaluationHardwareAndSoftware_Page_Title_Txt);

            //--- Expected Result: Evaluation board landing page should contain the Introductory Description of Evaluation Hardware & Software landing page  ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_EvaluationHardwareAndSoftware_Desc_Txt);

            //--- Expected Result: Evaluation board landing page should contain the Evaluation Hardware & Software sub-section's title ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_EvaluationHardwareAndSoftware_Subsec_Title_Txt);

            //--- Expected Result: Evaluation board landing page should contain the Evaluation Hardware & Software sub-section's description  ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_EvaluationHardwareAndSoftware_Subsec_Desc_Txt);

            //----- PRODUCT EVALUATION BOARDS AND KITS -----//

            //----- R1 > T3: Verify Sub-sections of Evaluation Hardware & Software landing page -----//

            //--- Expected Result: Product Evaluation Boards and Kits should have a view more link on description ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_EvaluationHardwareAndSoftware_ProductEvaluationBoardsAndKits_Sec_ViewMore_Link);

            if (util.CheckElement(driver, Elements.DesignCenter_EvaluationHardwareAndSoftware_ProductEvaluationBoardsAndKits_Sec_ViewMore_Link, 2) && !Configuration.Browser.Equals("Chrome_HL"))
            {
                //--- Action: Click on the View More link  ---//
                action.IOpenLinkInNewTab(driver, Elements.DesignCenter_EvaluationHardwareAndSoftware_ProductEvaluationBoardsAndKits_Sec_ViewMore_Link);
                Thread.Sleep(4000);

                //--- Expected Result: It should redirect on Product Evaluation Boards and Kits landing page ---//
                test.validateStringInstance(driver, driver.Url, productEvaluationBoardsAndKits_page_url);

                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles.First());
            }

            //----- EVALUATION & DEVELOPMENT PLATFORMS -----//

            //--- Expected Result: Evaluation Platforms should have a View More links ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_EvaluationHardwareAndSoftware_EvaluationAndDevelopmentPlatforms_Sec_ViewMore_Link);

            if (util.CheckElement(driver, Elements.DesignCenter_EvaluationHardwareAndSoftware_EvaluationAndDevelopmentPlatforms_Sec_ViewMore_Link, 2) && !Configuration.Browser.Equals("Chrome_HL"))
            {
                //--- Action: Click on the View More link ---//
                action.IOpenLinkInNewTab(driver, Elements.DesignCenter_EvaluationHardwareAndSoftware_EvaluationAndDevelopmentPlatforms_Sec_ViewMore_Link);

                //--- Expected Result: It should redirect on Product Evaluation Platforms sub-section landing page ---//
                test.validateStringInstance(driver, driver.Url, evaluationAndDevelopmentPlatforms_page_url);

                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles.First());
            }

            //----- R1 > T2_1: Verify Evaluation  Board Landing page content -----//

            //--- Expected Result: Evaluation board landing page should contain the Evaluation Platforms items ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_EvaluationHardwareAndSoftware_EvaluationAndDevelopmentPlatforms_Sec_Links);

            if (util.CheckElement(driver, Elements.DesignCenter_EvaluationHardwareAndSoftware_Ced_Link, 2) && !Configuration.Browser.Equals("Chrome_HL"))
            {
                //----- R1 > T5_2: Verify Converter Evaluation and Development Board (CED) of Evaluation Platform -----//

                //--- Action: On Evaluation Hardware & Software page click on Converter Evaluation and Development Board (CED) of Evaluation Platform section ---//
                action.IOpenLinkInNewTab(driver, Elements.DesignCenter_EvaluationHardwareAndSoftware_Ced_Link);

                //--- Expected Result: It should redirect on Converter Evaluation and Development Board (CED) on Evaluation Platform  page ---//
                test.validateStringInstance(driver, driver.Url, ced_page_url);

                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles.First());
            }

            if (util.CheckElement(driver, Elements.DesignCenter_EvaluationHardwareAndSoftware_Dpg_Link, 2) && !Configuration.Browser.Equals("Chrome_HL"))
            {
                //----- R1 > T5_3: Verify Data Pattern Generator (DPG) High-Speed DAC of Evaluation Platform page -----//

                //--- Action: On Evaluation Hardware & Software page click on Data Pattern Generator (DPG) High-Speed DAC Evaluation Platform  of Evaluation Platform section ---//
                action.IOpenLinkInNewTab(driver, Elements.DesignCenter_EvaluationHardwareAndSoftware_Dpg_Link);

                //--- Expected Result: It should redirect on  Data Pattern Generator (DPG) High-Speed DAC Evaluation Platform   page ---//
                test.validateStringInstance(driver, driver.Url, dpg_page_url);

                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles.First());
            }

            if (util.CheckElement(driver, Elements.DesignCenter_EvaluationHardwareAndSoftware_Sdp_Link, 2) && !Configuration.Browser.Equals("Chrome_HL"))
            {
                //----- R1 > T5_5: Verify System Demonstration Platform (SDP) Page  of Evaluation Platform page -----//

                //--- Action: On Evaluation Hardware & Software page click on System Demonstration Platform (SDP) of  Evaluation Platform section ---//
                action.IOpenLinkInNewTab(driver, Elements.DesignCenter_EvaluationHardwareAndSoftware_Sdp_Link);

                //--- Expected Result: It should redirect on SDP Page ---//
                test.validateStringInstance(driver, driver.Url, sdp_page_url);
            }
        }

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Left Rail Navigation is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Left Rail Navigation is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Left Rail Navigation is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Left Rail Navigation is Present and Working as Expected in RU Locale")]
        public void DesignCenter_EvaluationHardwareAndSoftware_VerifyLeftRailNavigation(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + evaluationHardwareAndSoftware_page_url);

            //----- R1 > T2_2: Verify Left Rail Navigation -----//

            //--- Expected Result: Evaluation Hardware & Software  should be set to active state ---//
            //--- Expected Result: Evaluation board landing page should contain the Left rail navigation  Evaluation Hardware & Software ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_EvaluationHardwareAndSoftware_LeftRailNav_Sec);

            if (util.CheckElement(driver, Elements.DesignCenter_EvaluationHardwareAndSoftware_LeftRailNav_Sec, 2))
            {
                //----- R1 > T2_1: Verify Evaluation  Board Landing page content -----//

                //--- Expected Result: Evaluation board landing page should contain the Left rail navigation  Evaluation Hardware & Software  sub- section ---//
                test.validateElementIsPresent(driver, Elements.DesignCenter_EvaluationHardwareAndSoftware_LeftRailNav_Subsec_Links);

                if (util.CheckElement(driver, Elements.DesignCenter_EvaluationHardwareAndSoftware_LeftRailNav_Subsec_Links, 2))
                {
                    if (util.CheckElement(driver, Elements.DesignCenter_EvaluationHardwareAndSoftware_LeftRailNav_ProductEvaluationBoardsAndKits_Link, 2) && !Configuration.Browser.Equals("Chrome_HL"))
                    {
                        //----- R1 > T4_1: Verify Product Evaluation Boards and Kits landing page -----//

                        //--- Action: Click on the Product Evaluation Boards and Kits on left rail navigation ---//
                        action.IOpenLinkInNewTab(driver, Elements.DesignCenter_EvaluationHardwareAndSoftware_LeftRailNav_ProductEvaluationBoardsAndKits_Link);

                        //--- Expected Result: It should redirect on Product Evaluation Boards and Kits landing page ---//
                        test.validateStringInstance(driver, driver.Url, productEvaluationBoardsAndKits_page_url);

                        driver.Close();
                        driver.SwitchTo().Window(driver.WindowHandles.First());
                    }
                    else
                    {
                        test.validateElementIsPresent(driver, Elements.DesignCenter_EvaluationHardwareAndSoftware_LeftRailNav_ProductEvaluationBoardsAndKits_Link);
                    }

                    if (util.CheckElement(driver, Elements.DesignCenter_EvaluationHardwareAndSoftware_LeftRailNav_EvaluationAndDevelopmentPlatforms_Link, 2))
                    {
                        //----- R1 > T5_1: Verify Product Evaluation Platforms landing page -----//

                        //--- Action: Click on the  Evaluation Platforms on left rail navigation ---//
                        action.IOpenLinkInNewTab(driver, Elements.DesignCenter_EvaluationHardwareAndSoftware_LeftRailNav_EvaluationAndDevelopmentPlatforms_Link);
                        Thread.Sleep(1000);

                        //--- Expected Result: It should redirect on Product Evaluation Platforms landing page ---//
                        test.validateStringInstance(driver, driver.Url, evaluationAndDevelopmentPlatforms_page_url);
                    }
                    else
                    {
                        test.validateElementIsPresent(driver, Elements.DesignCenter_EvaluationHardwareAndSoftware_LeftRailNav_EvaluationAndDevelopmentPlatforms_Link);
                    }
                }
            }
        }
    }
}