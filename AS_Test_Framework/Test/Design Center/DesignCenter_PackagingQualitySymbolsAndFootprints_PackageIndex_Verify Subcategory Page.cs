﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;

namespace AS_Test_Framework.DesignCenter
{
    [TestFixture]
    public class DesignCenter_PackagingQualitySymbolsAndFootprints_PackageIndex_SubcategoryPage : BaseSetUp
    {
        public DesignCenter_PackagingQualitySymbolsAndFootprints_PackageIndex_SubcategoryPage() : base() { }

        //--- URLs ---//
        string packageIndex_page_url = "/design-center/packaging-quality-symbols-footprints/package-index.html";
        string packageIndex_subCategory_page_url = "/design-center/packaging-quality-symbols-footprints/package-index/bga-ball-grid-array.html";
        string packageIndex_subCategory_page_url_format = "/design-center/packaging-quality-symbols-footprints/package-index/";

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Package Index - Subcategory Page is Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Package Index - Subcategory Page is Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Package Index - Subcategory Page is Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Package Index - Subcategory Page is Working as Expected in RU Locale")]
        public void DesignCenter_PackagingQualitySymbolsAndFootprints_PackageIndex_VerifySubcategoryPage(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + packageIndex_page_url);

            if (util.CheckElement(driver, Elements.DesignCenter_PackageIndex_SubCat_ViewMore_Links, 2))
            {
                //----- Design Center - Packaging TestPlan > R2 > T2: Verify "View More" links -----//

                //--- Action: Click all "View More" link under each Sub-Category (Package Type Category). ---//
                action.IOpenLinkInNewTab(driver, Elements.DesignCenter_PackageIndex_SubCat_ViewMore_Links);

                if (!driver.Url.Contains(packageIndex_subCategory_page_url_format))
                {
                    driver.Navigate().GoToUrl(Configuration.Env_Url + Locale + packageIndex_subCategory_page_url);
                }

                //--- Expected Result: <Package Category> page should be shown as the header. ---//
                test.validateElementIsPresent(driver, Elements.DesignCenter_PackageIndex_Page_Title_Txt);

                //--- Expected Result: All types under The Package Category should be displayed as Sub-header. ---//
                test.validateElementIsPresent(driver, Elements.DesignCenter_PackageIndex_SubCat_Header_Txts);

                //--- Expected Result: Description / Descriptive Summary of The type of Package should be displayed ---//
                test.validateElementIsPresent(driver, Elements.DesignCenter_PackageIndex_SubCat_Desc_Txts);

                //--- Expected Result: "View More" Links should be should be shown beside the description / descriptive summary. ---//
                test.validateElementIsPresent(driver, Elements.DesignCenter_PackageIndex_SubCat_ViewMore_Links);
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.DesignCenter_PackageIndex_SubCat_ViewMore_Links);
            }
        }
    }
}