﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Threading;

namespace AS_Test_Framework.DesignCenter
{
    [TestFixture]
    public class DesignCenter_SimulationModels_MathWorksBehavioralModels : BaseSetUp
    {
        public DesignCenter_SimulationModels_MathWorksBehavioralModels() : base() { }

        //--- URLs ---//
        string mathWorksBehavioralModels_page_url = "/design-center/simulation-models/mathworks-behavioral-models.html";
        string pdp_url_format = "/products/";

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that MathWorks Behavioral Models is Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that MathWorks Behavioral Models is Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that MathWorks Behavioral Models is Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that MathWorks Behavioral Models is Working as Expected in RU Locale")]
        public void DesignCenter_SimulationModels_VerifyMathWorksBehavioralModels(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + mathWorksBehavioralModels_page_url);

            //----- R6 > T1: Verify Page Title (AL-10662) -----//

            //--- Expected Result: The page title should be properly displayed and same label that was clicked on the category linked list ---//
            test.validateStringIsCorrect(driver, Elements.DesignCenter_MathWorksBehavioralModels_Page_Title_Txt, util.GetText(driver, Elements.DesignCenter_SimulationModels_LeftRailNav_Selected_Val_Link));

            //----- R6 > T2: Verify MathWorks Behavioral Models Files (ACM-2690) -----//

            //--- Expected Result: The Search should be displayed on the Descriptive Summary section ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_MathWorksBehavioralModels_Search_Txtbox);

            //--- Expected Result: The Default table display should be displayed on the Descriptive Summary section ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_MathWorksBehavioralModels_Tbl);

            if (util.CheckElement(driver, Elements.DesignCenter_MathWorksBehavioralModels_Tbl, 2))
            {
                //----- R6 > T3: Verify MathWorks Behavioral Models Files Search Section -----//

                if (util.CheckElement(driver, Elements.DesignCenter_MathWorksBehavioralModels_Search_Txtbox, 2))
                {
                    //--- Input ---//
                    string search_keyword = "AD";

                    //--- Action: Enter product number in search text field ---//
                    action.IDeleteValueOnFields(driver, Elements.DesignCenter_MathWorksBehavioralModels_Search_Txtbox);
                    action.IType(driver, Elements.DesignCenter_MathWorksBehavioralModels_Search_Txtbox, search_keyword);

                    //--- Expected Result: Search result should be displayed on the table ---//
                    test.validateElementIsPresent(driver, Elements.DesignCenter_MathWorksBehavioralModels_Tbl_Search_Results);

                    action.IDeleteValueOnFields(driver, Elements.DesignCenter_MathWorksBehavioralModels_Search_Txtbox);
                }

                if (util.CheckElement(driver, Elements.DesignCenter_MathWorksBehavioralModels_Product_Search_Txtbox, 2))
                {
                    //--- Input ---//
                    string product_search_keyword = "AD";

                    //--- Action: Enter product number in  product field ---//
                    action.IDeleteValueOnFields(driver, Elements.DesignCenter_MathWorksBehavioralModels_Product_Search_Txtbox);
                    action.IType(driver, Elements.DesignCenter_MathWorksBehavioralModels_Product_Search_Txtbox, product_search_keyword);

                    //--- Expected Result: Search result should be displayed on the table ---//
                    test.validateElementIsPresent(driver, Elements.DesignCenter_MathWorksBehavioralModels_Tbl_Search_Results);

                    action.IDeleteValueOnFields(driver, Elements.DesignCenter_MathWorksBehavioralModels_Product_Search_Txtbox);
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.DesignCenter_MathWorksBehavioralModels_Product_Search_Txtbox);
                }

                if (util.CheckElement(driver, Elements.DesignCenter_MathWorksBehavioralModels_Description_Search_Txtbox, 2))
                {
                    //--- Input ---//
                    string desc_search_keyword = "MSPS";

                    //--- Action: Enter any text in description field ---//
                    action.IDeleteValueOnFields(driver, Elements.DesignCenter_MathWorksBehavioralModels_Description_Search_Txtbox);
                    action.IType(driver, Elements.DesignCenter_MathWorksBehavioralModels_Description_Search_Txtbox, desc_search_keyword);

                    //--- Expected Result: All product with searched description should be filtered ---//
                    test.validateElementIsPresent(driver, Elements.DesignCenter_MathWorksBehavioralModels_Tbl_Search_Results);

                    action.IDeleteValueOnFields(driver, Elements.DesignCenter_MathWorksBehavioralModels_Description_Search_Txtbox);
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.DesignCenter_MathWorksBehavioralModels_Description_Search_Txtbox);
                }

                if (util.CheckElement(driver, Elements.DesignCenter_MathWorksBehavioralModels_ModelFile_Search_Txtbox, 2))
                {
                    //--- Input ---//
                    string modelFile_search_keyword = "AD";

                    //--- Action: Enter model file in model file field ---//
                    action.IDeleteValueOnFields(driver, Elements.DesignCenter_MathWorksBehavioralModels_ModelFile_Search_Txtbox);
                    action.IType(driver, Elements.DesignCenter_MathWorksBehavioralModels_ModelFile_Search_Txtbox, modelFile_search_keyword);

                    //--- Expected Result: All product with search Model file field should be filtered ---//
                    test.validateElementIsPresent(driver, Elements.DesignCenter_MathWorksBehavioralModels_Tbl_Search_Results);

                    action.IDeleteValueOnFields(driver, Elements.DesignCenter_MathWorksBehavioralModels_ModelFile_Search_Txtbox);
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.DesignCenter_MathWorksBehavioralModels_ModelFile_Search_Txtbox);
                }

                if (util.CheckElement(driver, Elements.DesignCenter_MathWorksBehavioralModels_Tbl_Product_Links, 2))
                {
                    if (Locale.Equals("en"))
                    {
                        //----- R6 > T4: Verify links on the table -----//

                        //--- Action: Click on product number ---//
                        action.IOpenLinkInNewTab(driver, Elements.DesignCenter_MathWorksBehavioralModels_Tbl_Product_Links);
                        Thread.Sleep(8000);

                        //--- Expected Result: Page should be redirected to Product Detail Page ---//
                        test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + pdp_url_format);
                    }
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.DesignCenter_MathWorksBehavioralModels_Tbl_Product_Links);
                }
            }
        }
    }
}