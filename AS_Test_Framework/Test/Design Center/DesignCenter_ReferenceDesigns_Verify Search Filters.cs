﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.DesignCenter
{
    [TestFixture]
    public class DesignCenter_ReferenceDesigns_SearchFilters : BaseSetUp
    {
        public DesignCenter_ReferenceDesigns_SearchFilters() : base() { }

        //--- URLs ---//
        string referenceDesigns_page_url = "/design-center/reference-designs.html";

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Filters from Reference Designs Search is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Filters from Reference Designs Search is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Filters from Reference Designs Search is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Filters from Reference Designs Search is Present and Working as Expected in RU Locale")]
        public void DesignCenter_ReferenceDesigns_SearchFilters_VerifyFiltersFromReferenceDesignsSearch(string Locale)
        {
            //--- Action: Go to Reference Design Home page ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + referenceDesigns_page_url);

            //----- Reference Designs Redesign_Test Plan > Desktop - RD_Onsite Search Tab > R1 > T1: Verify Onsite Search Results of Quick Search (NO Search Criteria) (IQ-9332/AL-16284) -----//

            //--- Action: Click the Search button without entering or selecting ANY search criteria ---//
            action.IDeleteValueOnFields(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec_SearchByKeywordOrDesignNumber_InputBx);
            action.IClick(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec_Search_Btn);
            Thread.Sleep(1000);

            //----- DESIGN CENTER - SEARCH RESULTS PAGE -----//
            if (util.CheckElement(driver, Elements.DesignCenter_Search_Filters_Section, 2))
            {
                //--- FILTERS - SECTION ---//

                //--- Expected Result: NO filter is pre-selected ---//
                test.validateElementIsNotPresent(driver, Elements.DesignCenter_Search_Selected_Filter_Filter_Cb);
                test.validateElementIsNotPresent(driver, Elements.DesignCenter_Search_Selected_Filter_Filter_Rb);
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.DesignCenter_Search_Filters_Section);
            }
        }

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", "All", TestName = "Verify that the Markets Filter is Present and Working as Expected after Selecting All in the Markets and Technology Solutions Dropdown from the Reference Designs Search in EN Locale")]
        [TestCase("cn", "全部", TestName = "Verify that the Markets Filter is Present and Working as Expected after Selecting All in the Markets and Technology Solutions Dropdown from the Reference Designs Search in CN Locale")]
        [TestCase("jp", "All", TestName = "Verify that the Markets Filter is Present and Working as Expected after Selecting All in the Markets and Technology Solutions Dropdown from the Reference Designs Search in JP Locale")]
        [TestCase("ru", "все", TestName = "Verify that the Markets Filter is Present and Working as Expected after Selecting All in the Markets and Technology Solutions Dropdown from the Reference Designs Search in RU Locale")]
        public void DesignCenter_ReferenceDesigns_SearchFilters_VerifyMarketsFilterAfterSelectingAllInMarketAndTechnologySolutionsDropdownFromReferenceDesignsSearch(string Locale, string All_Txt)
        {
            //--- Action: Go to Reference Design Home page ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + referenceDesigns_page_url);

            //----- Reference Designs Redesign_Test Plan > Desktop - RD_Onsite Search Tab > R1 > T18: Verify Onsite Search Results of Quick Search (All Markets and Technology Solutions search) (IQ-9332/AL-16284) -----//

            action.IClick(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec_MarketsAndTechnologySolutions_Dd_Btn);
            if (util.GetText(driver, By.CssSelector("li[data-facetcategory^='market']>ul>li:nth-of-type(1) * span")).Equals(All_Txt) || util.GetText(driver, By.CssSelector("li[data-facetcategory^='market']>ul>li:nth-of-type(1) * span")).Equals("All"))
            {
                //--- Action: Select All from the Markets and Technology Solutions dropdown ---//
                action.IClick(driver, By.CssSelector("li[data-facetcategory^='market']>ul>li:nth-of-type(1)"));

                action.IDeleteValueOnFields(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec_SearchByKeywordOrDesignNumber_InputBx);

                //--- Action: Click the Search button ---//
                action.IClick(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec_Search_Btn);
                Thread.Sleep(1000);

                //----- DESIGN CENTER - SEARCH RESULTS PAGE -----//
                if (util.CheckElement(driver, Elements.DesignCenter_Search_Filters_Section, 2))
                {
                    //--- FILTERS - SECTION ---//

                    //--- Expected Result: NO filter is pre-selected ---//
                    test.validateElementIsNotPresent(driver, Elements.DesignCenter_Search_Selected_Filter_Filter_Cb);
                    test.validateElementIsNotPresent(driver, Elements.DesignCenter_Search_Selected_Filter_Filter_Rb);

                    if (!util.CheckElement(driver, Elements.DesignCenter_Search_Selected_Filter_Filter_Cb, 2) && !util.CheckElement(driver, Elements.DesignCenter_Search_Selected_Filter_Filter_Rb, 2))
                    {
                        //--- MARKETS - DROPDOWN ---//
                        if (!util.CheckElement(driver, Elements.DesignCenter_Search_Filters_Section_Expanded_Markets_Dd, 2))
                        {
                            action.IClick(driver, Elements.DesignCenter_Search_Filters_Section_Markets_Dd_Btn);
                        }

                        //--- Expected Result: The number of results for each facet option is displayed ---//
                        test.validateElementIsPresent(driver, Elements.DesignCenter_Search_Filters_Section_Markets_Dd_Categories_ResultsCount_Lbl);

                        //--- TECHNOLOGIES - DROPDOWN ---//
                        if (!util.CheckElement(driver, Elements.DesignCenter_Search_Filters_Section_Expanded_Technologies_Dd, 2))
                        {
                            action.IClick(driver, Elements.DesignCenter_Search_Filters_Section_Technologies_Dd_Btn);
                        }

                        //--- Expected Result: The number of results for each facet option is displayed ---//
                        test.validateElementIsPresent(driver, Elements.DesignCenter_Search_Filters_Section_Technologies_Dd_Categories_ResultsCount_Lbl);
                    }
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.DesignCenter_Search_Filters_Section);
                }
            }
        }

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Markets Filter is Present and Working as Expected after Selecting a Market Value in the Markets and Technology Solutions Dropdown from the Reference Designs Search in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Markets Filter is Present and Working as Expected after Selecting a Market Value in the Markets and Technology Solutions Dropdown from the Reference Designs Search in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Markets Filter is Present and Working as Expected after Selecting a Market Value in the Markets and Technology Solutions Dropdown from the Reference Designs Search in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Markets Filter is Present and Working as Expected after Selecting a Market Value in the Markets and Technology Solutions Dropdown from the Reference Designs Search in RU Locale")]
        public void DesignCenter_ReferenceDesigns_SearchFilters_VerifyMarketsFilterAfterSelectingAMarketValueInMarketAndTechnologySolutionsDropdownFromReferenceDesignsSearch(string Locale)
        {
            //--- Action: Go to Reference Design Home page ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + referenceDesigns_page_url);

            //----- Reference Designs Redesign_Test Plan > Desktop - RD_Onsite Search Tab > R1 > T19: Verify Onsite Search Results of Quick Search (Markets search) (IQ-9332/AL-16284) -----//

            string marketsAndTechnologySolutions_dd_selected_value_locator = "li[data-facetcategory^='market']>ul>li:nth-of-type(2)";

            //--- Action: Select a Market from the Markets and Technology Solutions dropdown ---//
            action.IClick(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec_MarketsAndTechnologySolutions_Dd_Btn);
            string marketsAndTechnologySolutions_dd_selected_value = util.GetText(driver, By.CssSelector(marketsAndTechnologySolutions_dd_selected_value_locator));
            action.IClick(driver, By.CssSelector(marketsAndTechnologySolutions_dd_selected_value_locator));

            action.IDeleteValueOnFields(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec_SearchByKeywordOrDesignNumber_InputBx);

            //--- Action: Click the Search button ---//
            action.IClick(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec_Search_Btn);
            Thread.Sleep(1000);

            //----- DESIGN CENTER - SEARCH RESULTS PAGE -----//

            //--- MARKETS - DROPDOWN ---//
            if (!util.CheckElement(driver, Elements.DesignCenter_Search_Filters_Section_Expanded_Markets_Dd, 2))
            {
                action.IClick(driver, Elements.DesignCenter_Search_Filters_Section_Markets_Dd_Btn);
            }

            if (util.CheckElement(driver, Elements.DesignCenter_Search_Selected_Filter_Filter_Rb, 2))
            {
                //--- Expected Result: The first and second-level Market facets for the selected Market (in Step 1) are pre-selected in Filters ---//
                test.validateStringIsCorrect(driver, Elements.DesignCenter_Search_Selected_Filter_Filter_Rb_Lbl, marketsAndTechnologySolutions_dd_selected_value);
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.DesignCenter_Search_Selected_Filter_Filter_Rb);
            }
        }

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Technologies Filter is Present and Working as Expected after Selecting a Technology Value in the Markets and Technology Solutions Dropdown from the Reference Designs Search in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Technologies Filter is Present and Working as Expected after Selecting a Technology Value in the Markets and Technology Solutions Dropdown from the Reference Designs Search in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Technologies Filter is Present and Working as Expected after Selecting a Technology Value in the Markets and Technology Solutions Dropdown from the Reference Designs Search in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Technologies Filter is Present and Working as Expected after Selecting a Technology Value in the Markets and Technology Solutions Dropdown from the Reference Designs Search in RU Locale")]
        public void DesignCenter_ReferenceDesigns_SearchFilters_VerifyTechnologiesFilterAfterSelectingATechnologyValueInMarketAndTechnologySolutionsDropdownFromReferenceDesignsSearch(string Locale)
        {
            //--- Action: Go to Reference Design Home page ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + referenceDesigns_page_url);

            //----- Reference Designs Redesign_Test Plan > Desktop - RD_Onsite Search Tab > R1 > 20: Verify Onsite Search Results of Quick Search (Technology Solutions search) (IQ-9332/AL-16284) -----//

            string marketsAndTechnologySolutions_dd_selected_value_locator = "li[data-facetcategory^='market']>ul>li:nth-last-of-type(1)";

            //--- Action: Select a Technology Solutions from the Markets and Technology Solutions dropdown ---//
            action.IClick(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec_MarketsAndTechnologySolutions_Dd_Btn);
            string marketsAndTechnologySolutions_dd_selected_value = util.GetText(driver, By.CssSelector(marketsAndTechnologySolutions_dd_selected_value_locator));
            action.IClick(driver, By.CssSelector(marketsAndTechnologySolutions_dd_selected_value_locator));

            action.IDeleteValueOnFields(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec_SearchByKeywordOrDesignNumber_InputBx);

            //--- Action: Click the Search button---//
            action.IClick(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec_Search_Btn);
            Thread.Sleep(1000);

            //----- DESIGN CENTER - SEARCH RESULTS PAGE -----//

            //--- TECHNOLOGIES - DROPDOWN ---//
            if (!util.CheckElement(driver, Elements.DesignCenter_Search_Filters_Section_Expanded_Technologies_Dd, 2))
            {
                action.IClick(driver, Elements.DesignCenter_Search_Filters_Section_Technologies_Dd_Btn);
            }

            if (util.CheckElement(driver, Elements.DesignCenter_Search_Selected_Filter_Filter_Rb, 2))
            {
                //--- Expected Result: The Technology facet for the selected Technology (in Step 1) is pre-selected in Filters ---//
                test.validateStringIsCorrect(driver, Elements.DesignCenter_Search_Selected_Filter_Filter_Rb_Lbl, marketsAndTechnologySolutions_dd_selected_value);
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.DesignCenter_Search_Selected_Filter_Filter_Rb);
            }
        }

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", "All", TestName = "Verify that the Design Resources Filter is Present and Working as Expected after Selecting All in the Design Resources Dropdown from the Reference Designs Search in EN Locale")]
        [TestCase("cn", "全部", TestName = "Verify that the Design Resources Filter is Present and Working as Expected after Selecting All in the Design Resources Dropdown from the Reference Designs Search in CN Locale")]
        [TestCase("jp", "All", TestName = "Verify that the Design Resources Filter is Present and Working as Expected after Selecting All in the Design Resources Dropdown from the Reference Designs Search in JP Locale")]
        [TestCase("ru", "все", TestName = "Verify that the Design Resources Filter is Present and Working as Expected after Selecting All in the Design Resources Dropdown from the Reference Designs Search in RU Locale")]
        public void DesignCenter_ReferenceDesigns_SearchFilters_VerifyDesignResourcesFilterAfterSelectingAllInDesignResourcesDropdownFromReferenceDesignsSearch(string Locale, string All_Txt)
        {
            //--- Action: Go to Reference Design Home page ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + referenceDesigns_page_url);

            //----- Reference Designs Redesign_Test Plan > Desktop - RD_Onsite Search Tab > R1 > T33: Verify Onsite Search Results of Quick Search (ALL Design Resources search) (IQ-9332/AL-16284) -----//

            action.IClick(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec_DesignResources_Dd_Btn);
            if (util.GetText(driver, By.CssSelector("li[data-facetcategory^='design_resource']>ul>li:nth-of-type(1) * span")).Equals(All_Txt) || util.GetText(driver, By.CssSelector("li[data-facetcategory^='design_resource']>ul>li:nth-of-type(1) * span")).Equals("All"))
            {
                //--- Action: Select ALL from the Design Resources dropdown ---//
                action.IClick(driver, By.CssSelector("li[data-facetcategory^='design_resource']>ul>li:nth-of-type(1)"));

                action.IDeleteValueOnFields(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec_SearchByKeywordOrDesignNumber_InputBx);

                //--- Action: Click the Search button ---//
                action.IClick(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec_Search_Btn);
                Thread.Sleep(2000);

                //----- DESIGN CENTER - SEARCH RESULTS PAGE -----//
                if (util.CheckElement(driver, Elements.DesignCenter_Search_Filters_Section, 2))
                {
                    //--- FILTERS - SECTION ---//

                    //--- Expected Result: NO filter is pre-selected ---//
                    test.validateElementIsNotPresent(driver, Elements.DesignCenter_Search_Selected_Filter_Filter_Cb);
                    test.validateElementIsNotPresent(driver, Elements.DesignCenter_Search_Selected_Filter_Filter_Rb);

                    if (!util.CheckElement(driver, Elements.DesignCenter_Search_Selected_Filter_Filter_Cb, 2) && !util.CheckElement(driver, Elements.DesignCenter_Search_Selected_Filter_Filter_Rb, 2))
                    {
                        //--- DESIGN RESOURCES - DROPDOWN ---//
                        if (!util.CheckElement(driver, Elements.DesignCenter_Search_Filters_Section_Expanded_DesignResources_Dd, 2))
                        {
                            action.IClick(driver, Elements.DesignCenter_Search_Filters_Section_DesignResources_Dd_Btn);
                        }

                        //--- Expected Result: The number of results for each facet option is displayed ---//
                        test.validateElementIsPresent(driver, Elements.DesignCenter_Search_Filters_Section_DesignResources_Dd_Categories_ResultsCount_Lbl);
                    }
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.DesignCenter_Search_Filters_Section);
                }
            }
        }

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Design Resources Filter is Present and Working as Expected after Selecting a Value in the Design Resources Dropdown from the Reference Designs Search in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Design Resources Filter is Present and Working as Expected after Selecting a Value in the Design Resources Dropdown from the Reference Designs Search in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Design Resources Filter is Present and Working as Expected after Selecting a Value in the Design Resources Dropdown from the Reference Designs Search in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Design Resources Filter is Present and Working as Expected after Selecting a Value in the Design Resources Dropdown from the Reference Designs Search in RU Locale")]
        public void DesignCenter_ReferenceDesigns_SearchFilters_VerifyDesignResourcesFiltersAfterSelectingAValueInDesignResourcesDropdownFromReferenceDesignsSearch(string Locale)
        {
            //--- Action: Go to Reference Design Home page ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + referenceDesigns_page_url);

            //----- Reference Designs Redesign_Test Plan > Desktop - RD_Onsite Search Tab > R1 > T34: Verify Onsite Search Results of Quick Search (Design Resources search) (IQ-9332/AL-16284) -----//

            string designResources_dd_selected_value_locator = "li[data-facetcategory^='design_resource']>ul>li:nth-of-type(2)";

            //--- Action: Select a Design Resource from the Design Resources dropdown ---//
            action.IClick(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec_DesignResources_Dd_Btn);
            string designResources_dd_selected_value = util.GetText(driver, By.CssSelector(designResources_dd_selected_value_locator));
            action.IClick(driver, By.CssSelector(designResources_dd_selected_value_locator));

            action.IDeleteValueOnFields(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec_SearchByKeywordOrDesignNumber_InputBx);

            //--- Action: Click the Search button ---//
            action.IClick(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec_Search_Btn);
            Thread.Sleep(1000);

            //----- DESIGN CENTER - SEARCH RESULTS PAGE -----//

            //--- DESIGN RESOURCES - DROPDOWN ---//
            if (!util.CheckElement(driver, Elements.DesignCenter_Search_Filters_Section_Expanded_DesignResources_Dd, 2))
            {
                action.IClick(driver, Elements.DesignCenter_Search_Filters_Section_DesignResources_Dd_Btn);
            }

            if (util.CheckElement(driver, Elements.DesignCenter_Search_Selected_Filter_Filter_Cb, 2))
            {
                //--- Expected Result: The Design Resource facet for the selected Design Resource (in Step 1) is pre-selected in Filters ---//
                test.validateStringIsCorrect(driver, Elements.DesignCenter_Search_Selected_Filter_Filter_Cb_Lbl, designResources_dd_selected_value);
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.DesignCenter_Search_Selected_Filter_Filter_Cb);
            }
        }

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", "All", TestName = "Verify that the Product Categories Filter is Present and Working as Expected after Selecting All in the Product Categories Dropdown from the Reference Designs Search in EN Locale")]
        [TestCase("cn", "全部", TestName = "Verify that the Product Categories Filter is Present and Working as Expected after Selecting All in the Product Categories Dropdown from the Reference Designs Search in CN Locale")]
        [TestCase("jp", "All", TestName = "Verify that the Product Categories Filter is Present and Working as Expected after Selecting All in the Product Categories Dropdown from the Reference Designs Search in JP Locale")]
        [TestCase("ru", "все", TestName = "Verify that the Product Categories Filter is Present and Working as Expected after Selecting All in the Product Categories Dropdown from the Reference Designs Search in RU Locale")]
        public void DesignCenter_ReferenceDesigns_SearchFilters_VerifyProductCategoriesFilterAfterSelectingAllInProductCategoriesDropdownFromReferenceDesignsSearch(string Locale, string All_Txt)
        {
            //--- Action: Go to Reference Design Home page ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + referenceDesigns_page_url);

            //----- Reference Designs Redesign_Test Plan > Desktop - RD_Onsite Search Tab > R1 > T73: Verify Onsite Search Results of Quick Search (All Product Categories search) (IQ-9332/AL-16284) -----//

            action.IClick(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec_ProductCategories_Dd_Btn);
            if (util.GetText(driver, By.CssSelector("li[data-facetcategory^='prod_cat']>ul>li:nth-of-type(1) * span")).Equals(All_Txt) || util.GetText(driver, By.CssSelector("li[data-facetcategory^='prod_cat']>ul>li:nth-of-type(1) * span")).Equals("All"))
            {
                //--- Action: Select All from the Product Categories dropdown ---//
                action.IClick(driver, By.CssSelector("li[data-facetcategory^='prod_cat']>ul>li:nth-of-type(1)"));

                action.IDeleteValueOnFields(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec_SearchByKeywordOrDesignNumber_InputBx);

                //--- Action: Click the Search button ---//
                action.IClick(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec_Search_Btn);
                Thread.Sleep(1000);

                //----- DESIGN CENTER - SEARCH RESULTS PAGE -----//
                if (util.CheckElement(driver, Elements.DesignCenter_Search_Filters_Section, 2))
                {
                    //--- FILTERS - SECTION ---//

                    //--- Expected Result: NO filter is pre-selected ---//
                    test.validateElementIsNotPresent(driver, Elements.DesignCenter_Search_Selected_Filter_Filter_Cb);
                    test.validateElementIsNotPresent(driver, Elements.DesignCenter_Search_Selected_Filter_Filter_Rb);

                    if (!util.CheckElement(driver, Elements.DesignCenter_Search_Selected_Filter_Filter_Cb, 2) && !util.CheckElement(driver, Elements.DesignCenter_Search_Selected_Filter_Filter_Rb, 2))
                    {
                        //--- PRODUCT CATEGORIES - DROPDOWN ---//
                        if (!util.CheckElement(driver, Elements.DesignCenter_Search_Filters_Section_Expanded_ProductCategories_Dd, 2))
                        {
                            action.IClick(driver, Elements.DesignCenter_Search_Filters_Section_ProductCategories_Dd_Btn);
                        }

                        //--- Expected Result: The number of results for each facet option is displayed ---//
                        test.validateElementIsPresent(driver, Elements.DesignCenter_Search_Filters_Section_ProductCategories_Dd_Categories_ResultsCount_Lbl);
                    }
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.DesignCenter_Search_Filters_Section);
                }
            }
        }

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Product Categories Filter is Present and Working as Expected after Selecting a Value in the Product Categories Dropdown from the Reference Designs Search in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Product Categories Filter is Present and Working as Expected after Selecting a Value in the Product Categories Dropdown from the Reference Designs Search in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Product Categories Filter is Present and Working as Expected after Selecting a Value in the Product Categories Dropdown from the Reference Designs Search in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Product Categories Filter is Present and Working as Expected after Selecting a Value in the Product Categories Dropdown from the Reference Designs Search in RU Locale")]
        public void DesignCenter_ReferenceDesigns_SearchFilters_VerifyProductCategoriesFilterAfterSelectingAValueInProductCategoriesDropdownFromReferenceDesignsSearch(string Locale)
        {
            //--- Action: Go to Reference Design Home page ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + referenceDesigns_page_url);

            //----- Reference Designs Redesign_Test Plan > Desktop - RD_Onsite Search Tab > R1 > T74: Verify Onsite Search Results of Quick Search (Product Categories search) (IQ-9332/AL-16284) -----//

            string productCategories_dd_selected_value_locator = "li[data-facetcategory^='prod_cat']>ul>li:nth-of-type(2)";

            //--- Action: Select a Product Category from the Product Categories dropdown ---//
            action.IClick(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec_ProductCategories_Dd_Btn);
            string productCategories_dd_selected_value = util.GetText(driver, By.CssSelector(productCategories_dd_selected_value_locator));
            action.IClick(driver, By.CssSelector(productCategories_dd_selected_value_locator));

            action.IDeleteValueOnFields(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec_SearchByKeywordOrDesignNumber_InputBx);

            //--- Action: Click the Search button ---//
            action.IClick(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec_Search_Btn);
            Thread.Sleep(1000);

            //----- DESIGN CENTER - SEARCH RESULTS PAGE -----//

            //--- PRODUCT CATEGORIES - DROPDOWN ---//
            if (!util.CheckElement(driver, Elements.DesignCenter_Search_Filters_Section_Expanded_ProductCategories_Dd, 2))
            {
                action.IClick(driver, Elements.DesignCenter_Search_Filters_Section_ProductCategories_Dd_Btn);
            }

            if (util.CheckElement(driver, Elements.DesignCenter_Search_Selected_Filter_Filter_Rb, 2))
            {
                //--- Expected Result: The number of results for each facet option is displayed ---//
                test.validateStringIsCorrect(driver, Elements.DesignCenter_Search_Selected_Filter_Filter_Rb_Lbl, productCategories_dd_selected_value);
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.DesignCenter_Search_Selected_Filter_Filter_Rb);
            }
        }
    }
}