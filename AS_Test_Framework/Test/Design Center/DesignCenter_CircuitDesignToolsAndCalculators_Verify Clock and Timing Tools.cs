﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;

namespace AS_Test_Framework.DesignCenter
{
    [TestFixture]
    public class DesignCenter_CircuitDesignToolsAndCalculators_ClockAndTimingTools : BaseSetUp
    {
        public DesignCenter_CircuitDesignToolsAndCalculators_ClockAndTimingTools() : base() { }

        //--- URLs ---//
        string clockAndTimingTools_page_url = "/design-center/design-tools-and-calculators/clock-and-timing-tools.html";

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that Clock and Timing Tools is Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that Clock and Timing Tools is Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that Clock and Timing Tools is Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that Clock and Timing Tools is Working as Expected in RU Locale")]
        public void DesignCenter_CircuitDesignToolsAndCalculators_VerifyClockAndTimingTools(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + clockAndTimingTools_page_url);

            //----- R5 > T3: Validate Clock and Timing Tools -----//

            //--- Expected Result: ADIsimCLK is featured in the page ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_ClockAndTimingTools_AdiSimClk_Sec);

            if (util.CheckElement(driver, Elements.DesignCenter_ClockAndTimingTools_AdiSimClk_Sec, 2))
            {
                //--- Expected Result: ADIsimCLK Button is Present ---//
                test.validateElementIsPresent(driver, Elements.DesignCenter_ClockAndTimingTools_AdiSimClk_Btn);

                //--- Expected Result: ADIsimCLK image is displayed ---//
                test.validateElementIsPresent(driver, Elements.DesignCenter_ClockAndTimingTools_AdiSimClk_Img);
            }

            //--- Expected Result: The items are listed in the page has description ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_ClockAndTimingTools_DesignTools_Links);
            test.validateElementIsPresent(driver, Elements.DesignCenter_ClockAndTimingTools_DesignTools_Desc_Txts);
        }
    }
}