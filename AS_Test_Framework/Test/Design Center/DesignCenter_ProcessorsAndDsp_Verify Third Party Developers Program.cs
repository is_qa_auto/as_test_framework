﻿using AS_Test_Framework.Util;
using NUnit.Framework;
using System.Threading;
//using System.Collections.Generic;

namespace AS_Test_Framework.DesignCenter
{
    [TestFixture]
    public class DesignCenter_ProcessorsAndDsp_ThirdPartyDevelopersProgram : BaseSetUp
    {
        public DesignCenter_ProcessorsAndDsp_ThirdPartyDevelopersProgram() : base() { }

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Old Third Party Developer Company URLs are Redirecting to the Correct Page in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Old Third Party Developer Company URLs are Redirecting to the Correct Page in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Old Third Party Developer Company URLs are Redirecting to the Correct Page in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Old Third Party Developer Company URLs are Redirecting to the Correct Page in RU Locale")]
        public void DesignCenter_ProcessorsAndDsp_ThirdPartyDevelopersProgram_VerifyRedirectionOfOldThirdPartyDeveloperCompanyUrls(string Locale)
        {
            //----- Design Center - Processors and DSP TestPlan > R5 > T3: Verify the third party developer companies NEW Alliances URLs. -----//

            //----- ADSP Consulting, LLC -----//

            string old_url = "/design-center/processors-and-dsp/third-party-developers/adsp-consulting--llc.html";
            string new_alliances_url = "/about-adi/alliances/partner/adsp-consulting-llc.html";

            //--- Action: Go to ADSP Consulting, LLC old URL ---//
            driver.Manage().Cookies.DeleteAllCookies();
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl(Configuration.Env_Url + Locale + old_url);

            //--- Expected Result: It should now redirect to new Alliances URL ---//
            test.validateStringInstance(driver, driver.Url, Configuration.Env_Url + Locale + new_alliances_url);

            //----- AeVee Laboratories -----//

            old_url = "/design-center/processors-and-dsp/third-party-developers/aevee-laboratories.html";
            new_alliances_url = "/about-adi/alliances/partner/aevee-laboratories.html";

            //--- Action: Go to AeVee Laboratories old URL ---//
            driver.Manage().Cookies.DeleteAllCookies();
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl(Configuration.Env_Url + Locale + old_url);

            //--- Expected Result: It should now redirect to new Alliances URL ---//
            test.validateStringInstance(driver, driver.Url, Configuration.Env_Url + Locale + new_alliances_url);

            //----- AMI Technologies -----//

            old_url = "/design-center/processors-and-dsp/third-party-developers/ami-technologies.html";
            new_alliances_url = "/about-adi/alliances/partner/ami-technologies.html";

            //--- Action: Go to AMI Technologies old URL ---//
            driver.Manage().Cookies.DeleteAllCookies();
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl(Configuration.Env_Url + Locale + old_url);

            //--- Expected Result: It should now redirect to new Alliances URL ---//
            test.validateStringInstance(driver, driver.Url, Configuration.Env_Url + Locale + new_alliances_url);

            //----- AR Bayer DSP Systeme GmbH -----//

            old_url = "/design-center/processors-and-dsp/third-party-developers/ar-bayer-dsp-systeme-gmbh.html";
            new_alliances_url = "/about-adi/alliances/partner/ar-bayer-dsp-systeme-gmbh.html";

            //--- Action: Go to AR Bayer DSP Systeme GmbH old URL ---//
            driver.Manage().Cookies.DeleteAllCookies();
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl(Configuration.Env_Url + Locale + old_url);

            //--- Expected Result: It should now redirect to new Alliances URL ---//
            test.validateStringInstance(driver, driver.Url, Configuration.Env_Url + Locale + new_alliances_url);

            //----- Arcturus -----//

            old_url = "/design-center/processors-and-dsp/third-party-developers/arcturus.html";
            new_alliances_url = "/about-adi/alliances/partner/arcturus.html";

            //--- Action: Go to Arcturus old URL ---//
            driver.Manage().Cookies.DeleteAllCookies();
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl(Configuration.Env_Url + Locale + old_url);
            Thread.Sleep(1000);

            //--- Expected Result: It should now redirect to new Alliances URL ---//
            test.validateStringInstance(driver, driver.Url, Configuration.Env_Url + Locale + new_alliances_url);

            //----- Arkamys -----//

            old_url = "/design-center/processors-and-dsp/third-party-developers/arkamys.html";
            new_alliances_url = "/about-adi/alliances/partner/arkamys.html";

            //--- Action: Go to Arkamys old URL ---//
            driver.Manage().Cookies.DeleteAllCookies();
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl(Configuration.Env_Url + Locale + old_url);

            //--- Expected Result: It should now redirect to new Alliances URL ---//
            test.validateStringInstance(driver, driver.Url, Configuration.Env_Url + Locale + new_alliances_url);

            //----- Beijing Azure Vision Technology Inc -----//

            old_url = "/design-center/processors-and-dsp/third-party-developers/beijing-azure-vision-technology-inc.html";
            new_alliances_url = "/about-adi/alliances/partner/beijing-azure-vision-technology-inc.html";

            //--- Action: Go to Beijing Azure Vision Technology Inc. old URL ---//
            driver.Manage().Cookies.DeleteAllCookies();
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl(Configuration.Env_Url + Locale + old_url);

            //--- Expected Result: It should now redirect to new Alliances URL ---//
            test.validateStringInstance(driver, driver.Url, Configuration.Env_Url + Locale + new_alliances_url);

            //----- Callido -----//

            old_url = "/design-center/processors-and-dsp/third-party-developers/callido.html";
            new_alliances_url = "/about-adi/alliances/partner/callido.html";

            //--- Action: Go to Callido old URL ---//
            driver.Manage().Cookies.DeleteAllCookies();
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl(Configuration.Env_Url + Locale + old_url);

            //--- Expected Result: It should now redirect to new Alliances URL ---//
            test.validateStringInstance(driver, driver.Url, Configuration.Env_Url + Locale + new_alliances_url);

            //----- Danville Signal Processing -----//

            old_url = "/design-center/processors-and-dsp/third-party-developers/danville-signal-processing.html";
            new_alliances_url = "/about-adi/alliances/partner/danville-signal-processing.html";

            //--- Action: Go to Danville Signal Processing old URL ---//
            driver.Manage().Cookies.DeleteAllCookies();
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl(Configuration.Env_Url + Locale + old_url);

            //--- Expected Result: It should now redirect to new Alliances URL ---//
            test.validateStringInstance(driver, driver.Url, Configuration.Env_Url + Locale + new_alliances_url);

            //----- Design Interface Ltd  -----//

            old_url = "/design-center/processors-and-dsp/third-party-developers/design-interface-ltd.html";
            new_alliances_url = "/about-adi/alliances/partner/design-interface-ltd.html";

            //--- Action: Go to Design Interface Ltd old URL ---//
            driver.Manage().Cookies.DeleteAllCookies();
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl(Configuration.Env_Url + Locale + old_url);

            //--- Expected Result: It should now redirect to new Alliances URL ---//
            test.validateStringInstance(driver, driver.Url, Configuration.Env_Url + Locale + new_alliances_url);

            //----- Dexcel Electronics Designs (P) Ltd -----//

            old_url = "/design-center/processors-and-dsp/third-party-developers/dexcel-electronics-designs--p--ltd.html";
            new_alliances_url = "/about-adi/alliances/partner/dexcel-electronics-designs-p-ltd.html";

            //--- Action: Go to Dexcel Electronics Designs (P) Ltd old URL ---//
            driver.Manage().Cookies.DeleteAllCookies();
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl(Configuration.Env_Url + Locale + old_url);

            //--- Expected Result: It should now redirect to new Alliances URL ---//
            test.validateStringInstance(driver, driver.Url, Configuration.Env_Url + Locale + new_alliances_url);

            //----- DSP Algorithms -----//

            old_url = "/design-center/processors-and-dsp/third-party-developers/dsp-algorithms.html";
            new_alliances_url = "/about-adi/alliances/partner/dsp-algorithms.html";

            //--- Action: Go to DSP Algorithms old URL ---//
            driver.Manage().Cookies.DeleteAllCookies();
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl(Configuration.Env_Url + Locale + old_url);

            //--- Expected Result: It should now redirect to new Alliances URL ---//
            test.validateStringInstance(driver, driver.Url, Configuration.Env_Url + Locale + new_alliances_url);

            //----- DUNE -----//

            old_url = "/design-center/processors-and-dsp/third-party-developers/dune.html";
            new_alliances_url = "/about-adi/alliances/partner/dune.html";

            //--- Action: Go to DUNE old URL ---//
            driver.Manage().Cookies.DeleteAllCookies();
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl(Configuration.Env_Url + Locale + old_url);

            //--- Expected Result: It should now redirect to new Alliances URL ---//
            test.validateStringInstance(driver, driver.Url, Configuration.Env_Url + Locale + new_alliances_url);

            //----- Gastager Systemtechnik GmbH -----//

            old_url = "/design-center/processors-and-dsp/third-party-developers/gastager-systemtechnik-gmbh.html";
            new_alliances_url = "/about-adi/alliances/partner/gastager-systemtechnik-gmbh.html";

            //--- Action: Go to Gastager Systemtechnik GmbH old URL ---//
            driver.Manage().Cookies.DeleteAllCookies();
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl(Configuration.Env_Url + Locale + old_url);

            //--- Expected Result: It should now redirect to new Alliances URL ---//
            test.validateStringInstance(driver, driver.Url, Configuration.Env_Url + Locale + new_alliances_url);

            //----- HUBER SIGNAL PROCESSING -----//

            old_url = "/design-center/processors-and-dsp/third-party-developers/huber-signal-processing.html";
            new_alliances_url = "/about-adi/alliances/partner/huber-signal-processing.html";

            //--- Action: Go to HUBER SIGNAL PROCESSING old URL ---//
            driver.Manage().Cookies.DeleteAllCookies();
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl(Configuration.Env_Url + Locale + old_url);

            //--- Expected Result: It should now redirect to new Alliances URL ---//
            test.validateStringInstance(driver, driver.Url, Configuration.Env_Url + Locale + new_alliances_url);

            //----- Ignitarium Technology Solutions Pvt Ltd -----//

            old_url = "/design-center/processors-and-dsp/third-party-developers/Ignitarium-Technology-Solutions.html";
            new_alliances_url = "/about-adi/alliances/partner/ignitarium-technology-solutions-pvt-ltd.html";

            //--- Action: Go to Ignitarium Technology Solutions Pvt Ltd old URL ---//
            driver.Manage().Cookies.DeleteAllCookies();
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl(Configuration.Env_Url + Locale + old_url);

            //--- Expected Result: It should now redirect to new Alliances URL ---//
            test.validateStringInstance(driver, driver.Url, Configuration.Env_Url + Locale + new_alliances_url);

            //----- Kaztek Systems -----//

            old_url = "/design-center/processors-and-dsp/third-party-developers/kaztek-systems.html";
            new_alliances_url = "/about-adi/alliances/partner/kaztek-systems.html";

            //--- Action: Go to Kaztek Systems old URL ---//
            driver.Manage().Cookies.DeleteAllCookies();
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl(Configuration.Env_Url + Locale + old_url);

            //--- Expected Result: It should now redirect to new Alliances URL ---//
            test.validateStringInstance(driver, driver.Url, Configuration.Env_Url + Locale + new_alliances_url);

            //----- Signal Processing Consultants, Inc. -----//

            old_url = "/design-center/processors-and-dsp/third-party-developers/signal-processing-consultants--inc.html";
            new_alliances_url = "/about-adi/alliances/partner/signal-processing-consultants-inc.html";

            //--- Action: Go to Signal Processing Consultants, Inc. old URL ---//
            driver.Manage().Cookies.DeleteAllCookies();
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl(Configuration.Env_Url + Locale + old_url);

            //--- Expected Result: It should now redirect to new Alliances URL ---//
            test.validateStringInstance(driver, driver.Url, Configuration.Env_Url + Locale + new_alliances_url);

            //----- Super Computing Systems AG -----//

            old_url = "/design-center/processors-and-dsp/third-party-developers/super-computing-systems-ag.html";
            new_alliances_url = "/about-adi/alliances/partner/super-computing-systems-ag.html";

            //--- Action: Go to Super Computing Systems AG old URL ---//
            driver.Manage().Cookies.DeleteAllCookies();
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl(Configuration.Env_Url + Locale + old_url);

            //--- Expected Result: It should now redirect to new Alliances URL ---//
            test.validateStringInstance(driver, driver.Url, Configuration.Env_Url + Locale + new_alliances_url);

            //----- Sysacom R&D plus inc. -----//

            old_url = "/design-center/processors-and-dsp/third-party-developers/sysacom-r-d-plus-inc.html";
            new_alliances_url = "/about-adi/alliances/partner/sysacom-rd-plus-inc.html";

            //--- Action: Go to Sysacom R&D plus inc. old URL ---//
            driver.Manage().Cookies.DeleteAllCookies();
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl(Configuration.Env_Url + Locale + old_url);

            //--- Expected Result: It should now redirect to new Alliances URL ---//
            test.validateStringInstance(driver, driver.Url, Configuration.Env_Url + Locale + new_alliances_url);

            //----- voice INTER connect -----//

            old_url = "/design-center/processors-and-dsp/third-party-developers/voice-inter-connect.html";
            new_alliances_url = "/about-adi/alliances/partner/voice-inter-connect.html";

            //--- Action: Go to voice INTER connect old URL ---//
            driver.Manage().Cookies.DeleteAllCookies();
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl(Configuration.Env_Url + Locale + old_url);

            //--- Expected Result: It should now redirect to new Alliances URL ---//
            test.validateStringInstance(driver, driver.Url, Configuration.Env_Url + Locale + new_alliances_url);

            #region //--- Using FOR EACH ---//

            //Dictionary<string, string> ThirdPartyDeveloperCompany_Urls = new Dictionary<string, string>();

            ////--- ADSP Consulting, LLC ---//
            //ThirdPartyDeveloperCompany_Urls.Add("/design-center/processors-and-dsp/third-party-developers/adsp-consulting--llc.html", "/about-adi/alliances/partner/adsp-consulting-llc.html");

            ////--- AeVee Laboratories ---//
            //ThirdPartyDeveloperCompany_Urls.Add("/design-center/processors-and-dsp/third-party-developers/aevee-laboratories.html", "/about-adi/alliances/partner/aevee-laboratories.html");

            ////--- AMI Technologies ---//
            //ThirdPartyDeveloperCompany_Urls.Add("/design-center/processors-and-dsp/third-party-developers/ami-technologies.html", "/about-adi/alliances/partner/ami-technologies.html");

            ////--- AR Bayer DSP Systeme GmbH ---//
            //ThirdPartyDeveloperCompany_Urls.Add("/design-center/processors-and-dsp/third-party-developers/ar-bayer-dsp-systeme-gmbh.html", "/about-adi/alliances/partner/ar-bayer-dsp-systeme-gmbh.html");

            ////--- Arcturus ---//
            //ThirdPartyDeveloperCompany_Urls.Add("/design-center/processors-and-dsp/third-party-developers/arcturus.html", "/about-adi/alliances/partner/arcturus.html");

            ////--- Arkamys ---//
            //ThirdPartyDeveloperCompany_Urls.Add("/design-center/processors-and-dsp/third-party-developers/arkamys.html", "/about-adi/alliances/partner/arkamys.html");

            ////--- Beijing Azure Vision Technology Inc ---//
            //ThirdPartyDeveloperCompany_Urls.Add("/design-center/processors-and-dsp/third-party-developers/beijing-azure-vision-technology-inc.html", "/about-adi/alliances/partner/beijing-azure-vision-technology-inc.html");

            ////--- Callido ---//
            //ThirdPartyDeveloperCompany_Urls.Add("/design-center/processors-and-dsp/third-party-developers/callido.html", "/about-adi/alliances/partner/callido.html");

            ////--- Danville Signal Processing ---//
            //ThirdPartyDeveloperCompany_Urls.Add("/design-center/processors-and-dsp/third-party-developers/danville-signal-processing.html", "/about-adi/alliances/partner/danville-signal-processing.html");

            ////--- Design Interface Ltd ---//
            //ThirdPartyDeveloperCompany_Urls.Add("/design-center/processors-and-dsp/third-party-developers/design-interface-ltd.html", "/about-adi/alliances/partner/design-interface-ltd.html");

            ////--- Dexcel Electronics Designs (P) Ltd ---//
            //ThirdPartyDeveloperCompany_Urls.Add("/design-center/processors-and-dsp/third-party-developers/dexcel-electronics-designs--p--ltd.html", "/about-adi/alliances/partner/dexcel-electronics-designs-p-ltd.html");

            ////--- DSP Algorithms ---//
            //ThirdPartyDeveloperCompany_Urls.Add("/design-center/processors-and-dsp/third-party-developers/dsp-algorithms.html", "/about-adi/alliances/partner/dsp-algorithms.html");

            ////--- DUNE ---//
            //ThirdPartyDeveloperCompany_Urls.Add("/design-center/processors-and-dsp/third-party-developers/dune.html", "/about-adi/alliances/partner/dune.html");

            ////--- Gastager Systemtechnik GmbH ---//
            //ThirdPartyDeveloperCompany_Urls.Add("/design-center/processors-and-dsp/third-party-developers/gastager-systemtechnik-gmbh.html", "/about-adi/alliances/partner/gastager-systemtechnik-gmbh.html");

            ////--- HUBER SIGNAL PROCESSING ---//
            //ThirdPartyDeveloperCompany_Urls.Add("/design-center/processors-and-dsp/third-party-developers/huber-signal-processing.html", "/about-adi/alliances/partner/huber-signal-processing.html");

            ////--- Ignitarium Technology Solutions Pvt Ltd ---//
            //ThirdPartyDeveloperCompany_Urls.Add("/design-center/processors-and-dsp/third-party-developers/Ignitarium-Technology-Solutions.html", "/about-adi/alliances/partner/ignitarium-technology-solutions-pvt-ltd.html");

            ////--- Kaztek Systems ---//
            //ThirdPartyDeveloperCompany_Urls.Add("/design-center/processors-and-dsp/third-party-developers/kaztek-systems.html", "/about-adi/alliances/partner/kaztek-systems.html");

            ////--- Signal Processing Consultants, Inc. ---//
            //ThirdPartyDeveloperCompany_Urls.Add("/design-center/processors-and-dsp/third-party-developers/signal-processing-consultants--inc.html", "/about-adi/alliances/partner/signal-processing-consultants-inc.html");

            ////--- Super Computing Systems AG ---//
            //ThirdPartyDeveloperCompany_Urls.Add("/design-center/processors-and-dsp/third-party-developers/super-computing-systems-ag.html", "/about-adi/alliances/partner/super-computing-systems-ag.html");

            ////--- Sysacom R&D plus inc. ---//
            //ThirdPartyDeveloperCompany_Urls.Add("/design-center/processors-and-dsp/third-party-developers/sysacom-r-d-plus-inc.html", "/about-adi/alliances/partner/sysacom-rd-plus-inc.html");

            ////--- voice INTER connect ---//
            //ThirdPartyDeveloperCompany_Urls.Add("/design-center/processors-and-dsp/third-party-developers/voice-inter-connect.html", "/about-adi/alliances/partner/voice-inter-connect.html");

            //foreach (KeyValuePair<string, string> value in ThirdPartyDeveloperCompany_Urls)
            //{
            //    //----- Design Center - Processors and DSP TestPlan > R5 > T3: Verify the third party developer companies NEW Alliances URLs. -----//

            //    string old_url = value.Key;
            //    string new_alliances_url = value.Value;

            //    //--- Action: Go to old URL ---//
            //    driver.Manage().Cookies.DeleteAllCookies();
            //    driver.Manage().Window.Maximize();
            //    driver.Navigate().GoToUrl(Configuration.Env_Url + Locale + old_url);

            //    //--- Expected Result: It should now redirect to new Alliances URL ---//
            //    test.validateStringInstance(driver, driver.Url, Configuration.Env_Url + Locale + new_alliances_url);
            //}
            #endregion
        }
    }
}