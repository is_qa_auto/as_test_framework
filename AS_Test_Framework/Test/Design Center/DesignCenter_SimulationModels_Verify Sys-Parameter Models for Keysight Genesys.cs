﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Threading;

namespace AS_Test_Framework.DesignCenter
{
    [TestFixture]
    public class DesignCenter_SimulationModels_SysParameterModelsForKeysightGenesys : BaseSetUp
    {
        public DesignCenter_SimulationModels_SysParameterModelsForKeysightGenesys() : base() { }

        //--- URLs ---//
        string sysParameterModelsForKeysightGenesys_page_url = "/design-center/simulation-models/sys-parameter-models-keysight-genesys.html";

        //--- Labels ---//
        string modelLibraryVersionChecker_txt = "Model Library Version Checker";

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that Sys-Parameter Models for Keysight Genesys is Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that Sys-Parameter Models for Keysight Genesys is Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that Sys-Parameter Models for Keysight Genesys is Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that Sys-Parameter Models for Keysight Genesys is Working as Expected in RU Locale")]
        public void DesignCenter_SimulationModels_VerifySysParameterModelsForKeysightGenesys(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + sysParameterModelsForKeysightGenesys_page_url);
            Thread.Sleep(2000);

            //----- R8 > T1: Verify the Title page -----//

            //--- Expected Result: The page title should be displayed ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_SysParameterModelsForKeysightGenesys_Page_Title_Txt);

            //----- R8 > T2: Verify the content of the page -----//

            //--- Expected Result: Video in new design should be displayed ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_SysParameterModelsForKeysightGenesys_Video);

            //----- R8 > T3: Verify the buttons and Links in the page -----//

            //--- Expected Result: "Download" link should be displayed ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_SysParameterModelsForKeysightGenesys_Download_Link);

            if (util.CheckElement(driver, Elements.DesignCenter_SysParameterModelsForKeysightGenesys_DownloadNow_Btn, 2))
            {
                //--- Action: Click the download now orange button ---//
                action.IOpenLinkInNewTab(driver, Elements.DesignCenter_SysParameterModelsForKeysightGenesys_DownloadNow_Btn);
                Thread.Sleep(3000);

                if (Locale.Equals("en"))
                {
                    //--- Expected Result: It will redirect to Model Library Version Checker page ---//
                    test.validateWindowTitleIsCorrect(driver, modelLibraryVersionChecker_txt);

                    if (driver.Title.Contains(modelLibraryVersionChecker_txt))
                    {
                        //--- Expected Result: Model Library Version Checker Title should be available ---//
                        test.validateElementIsPresent(driver, Elements.DesignCenter_SysParameterModelsForKeysightGenesys_ModelLibraryVerChecker_Title_Txt);

                        //--- Expected Result: The latest version of the Keysight models library is v2017.4.3. Click here to download. text should be available ---//
                        test.validateElementIsPresent(driver, Elements.DesignCenter_SysParameterModelsForKeysightGenesys_ModelLibraryVerChecker_Desc_Txt);

                        //--- Expected Result: "Here" link should be displayed ---//
                        test.validateElementIsPresent(driver, Elements.DesignCenter_SysParameterModelsForKeysightGenesys_ModelLibraryVerChecker_Here_Link);
                    }
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.DesignCenter_SysParameterModelsForKeysightGenesys_DownloadNow_Btn);
            }
        }
    }
}