﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Threading;

namespace AS_Test_Framework.DesignCenter
{
    [TestFixture]
    public class DesignCenter_SimulationModels_BsdlModels : BaseSetUp
    {
        public DesignCenter_SimulationModels_BsdlModels() : base() { }

        //--- URLs ---//
        string bsdlModels_page_url = "/design-center/simulation-models/bsdl-model-files.html";
        string pdp_url_format = "/products/";

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that BSDL Models is Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that BSDL Models is Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that BSDL Models is Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that BSDL Models is Working as Expected in RU Locale")]
        public void DesignCenter_SimulationModels_VerifyBsdlModels(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + bsdlModels_page_url);

            //----- R2 > T1: Verify Page Title (AL-10662) -----//

            //--- Expected Result: The page title should be properly displayed and same label that was clicked on the category linked list ---//
            test.validateStringIsCorrect(driver, Elements.DesignCenter_BsdlModels_Page_Title_Txt, util.GetText(driver, Elements.DesignCenter_SimulationModels_LeftRailNav_Selected_Val_Link));

            //----- R2 > T2: Verify BSDL Model  | Files (ACM-2690) -----//

            //--- Expected Result: The Search should be displayed on the Descriptive Summary section ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_BsdlModels_Search_Txtbox);

            //--- Expected Result: The Default table display should be displayed on the Descriptive Summary section ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_BsdlModels_Tbl);

            if (util.CheckElement(driver, Elements.DesignCenter_BsdlModels_Tbl, 2))
            {
                //----- R2 > T3: Verify BDSL Model| Files Search Section -----//

                if (util.CheckElement(driver, Elements.DesignCenter_BsdlModels_Search_Txtbox, 2))
                {
                    //--- Input ---//
                    string search_keyword = "AD";

                    //--- Action: Enter product number in search text field ---//
                    action.IDeleteValueOnFields(driver, Elements.DesignCenter_BsdlModels_Search_Txtbox);
                    action.IType(driver, Elements.DesignCenter_BsdlModels_Search_Txtbox, search_keyword);

                    //--- Expected Result: Search result should be displayed on the table ---//
                    test.validateElementIsPresent(driver, Elements.DesignCenter_BsdlModels_Tbl_Search_Results);

                    action.IDeleteValueOnFields(driver, Elements.DesignCenter_BsdlModels_Search_Txtbox);
                }

                if (util.CheckElement(driver, Elements.DesignCenter_BsdlModels_Product_Search_Txtbox, 2))
                {
                    //--- Input ---//
                    string product_search_keyword = "AD";

                    //--- Action: Enter product number in  product field ---//
                    action.IDeleteValueOnFields(driver, Elements.DesignCenter_BsdlModels_Product_Search_Txtbox);
                    action.IType(driver, Elements.DesignCenter_BsdlModels_Product_Search_Txtbox, product_search_keyword);

                    //--- Expected Result: Search result should be displayed on the table ---//
                    test.validateElementIsPresent(driver, Elements.DesignCenter_BsdlModels_Tbl_Search_Results);

                    action.IDeleteValueOnFields(driver, Elements.DesignCenter_BsdlModels_Product_Search_Txtbox);
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.DesignCenter_BsdlModels_Product_Search_Txtbox);
                }

                if (util.CheckElement(driver, Elements.DesignCenter_BsdlModels_Description_Search_Txtbox, 2))
                {
                    //--- Input ---//
                    string desc_search_keyword = "SHARC";

                    //--- Action: Enter any text in description field ---//
                    action.IDeleteValueOnFields(driver, Elements.DesignCenter_BsdlModels_Description_Search_Txtbox);
                    action.IType(driver, Elements.DesignCenter_BsdlModels_Description_Search_Txtbox, desc_search_keyword);

                    //--- Expected Result: All product with searched description should be filtered ---//
                    test.validateElementIsPresent(driver, Elements.DesignCenter_BsdlModels_Tbl_Search_Results);

                    action.IDeleteValueOnFields(driver, Elements.DesignCenter_BsdlModels_Description_Search_Txtbox);
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.DesignCenter_BsdlModels_Description_Search_Txtbox);
                }

                if (util.CheckElement(driver, Elements.DesignCenter_BsdlModels_ModelFile_Search_Txtbox, 2))
                {
                    //--- Input ---//
                    string modelFile_search_keyword = "AD";

                    //--- Action: Enter model file in model file field ---//
                    action.IDeleteValueOnFields(driver, Elements.DesignCenter_BsdlModels_ModelFile_Search_Txtbox);
                    action.IType(driver, Elements.DesignCenter_BsdlModels_ModelFile_Search_Txtbox, modelFile_search_keyword);

                    //--- Expected Result: All product with search Model file field should be filtered ---//
                    test.validateElementIsPresent(driver, Elements.DesignCenter_BsdlModels_Tbl_Search_Results);

                    action.IDeleteValueOnFields(driver, Elements.DesignCenter_BsdlModels_ModelFile_Search_Txtbox);
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.DesignCenter_BsdlModels_ModelFile_Search_Txtbox);
                }

                if (util.CheckElement(driver, Elements.DesignCenter_BsdlModels_Tbl_Product_Links, 2))
                {
                    //----- R2 > T4: Verify links on the table -----//

                    //--- Action: Click on product number ---//
                    action.IOpenLinkInNewTab(driver, Elements.DesignCenter_BsdlModels_Tbl_Product_Links);
                    Thread.Sleep(4000);

                    //--- Expected Result: Page should be redirected to Product Detail Page ---//
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + pdp_url_format);
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.DesignCenter_BsdlModels_Tbl_Product_Links);
                }
            }
        }
    }
}