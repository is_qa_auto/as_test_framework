﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;

namespace AS_Test_Framework.DesignCenter
{
    [TestFixture]
    public class DesignCenter_EvaluationHardwareAndSoftware_EvaluationAndDevelopmentPlatforms_Ced : BaseSetUp
    {
        public DesignCenter_EvaluationHardwareAndSoftware_EvaluationAndDevelopmentPlatforms_Ced() : base() { }

        //--- URLs ---//
        string ced_page_url = "/design-center/evaluation-hardware-and-software/evaluation-development-platforms/ced.html";

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that Converter Evaluation and Development Board is Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that Converter Evaluation and Development Board is Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that Converter Evaluation and Development Board is Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that Converter Evaluation and Development Board is Working as Expected in RU Locale")]
        public void DesignCenter_EvaluationHardwareAndSoftware_EvaluationAndDevelopmentPlatforms_VerifyCed(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + ced_page_url);

            //----- R1 > T5_2: Verify Converter Evaluation and Development Board (CED) of Evaluation Platform -----//

            //--- Expected Result: Page Title of the page should be shown ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_Ced_Page_Title_Txt);

            //--- Expected Result: Description of the page should be shown ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_Ced_Desc_Txt);

            //--- Expected Result: Verify CED Image should be shown ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_Ced_Img);
        }  
    }
}