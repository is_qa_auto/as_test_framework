﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;

namespace AS_Test_Framework.DesignCenter
{
    [TestFixture]
    public class DesignCenter_LandingPage : BaseSetUp
    {
        public DesignCenter_LandingPage() : base() { }

        //--- URLs ---//
        string designCenter_landing_page_url = "/design-center.html";
        string designCenter_searchResults_page_url = "/design-center/search.html";

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Landing Page is Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Landing Page is Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Landing Page is Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Landing Page is Working as Expected in RU Locale")]
        public void DesignCenter_VerifyLandingPage(string Locale)
        {
            //--- Action: Access direct url ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + designCenter_landing_page_url);

            //----- Design Center - Landing Page TestPlan > Test Case Title: Verify the Design Center Landing Page -----//

            //--- Expected Result: The Title should be dsplayed ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_LandingPage_Title_Label);

            //--- Expected Result: The Description should be dsplayed ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_LandingPage_Description_Label);
        }

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Search is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Search is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Search is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Search is Present and Working as Expected in RU Locale")]
        public void DesignCenter_LandingPage_VerifySearchSection(string Locale)
        {
            //--- Action: Access direct url ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + designCenter_landing_page_url);

            //----- Design Center - Landing Page TestPlan > Test Case Title: Verify the Search section -----//

            //--- Expected Result: The Input box should be dsplayed ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_LandingPage_Search_InputBox);

            //--- Expected Result: The Magnifying glass icon should be dsplayed ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_LandingPage_Search_Button);

            if (Locale.Equals("en"))
            {
                //--- Expected Result: The Ghost text should be dsplayed ---//
                test.validateString(driver, "Search within Design Center", util.ReturnAttribute(driver, Elements.DesignCenter_LandingPage_Search_InputBox, "placeholder"));
            }

            //----- Design Center - Landing Page TestPlan > Test Case Title: Search a Design Center using the magnifying glass icon -----//

            //--- Action: Enter value on the Input field ---//
            string search_input = "adsp-ac573";
            action.IDeleteValueOnFields(driver, Elements.DesignCenter_LandingPage_Search_InputBox);
            action.IType(driver, Elements.DesignCenter_LandingPage_Search_InputBox, search_input);

            //--- Action: Click on the magnifying glass icon ---//
            action.IClick(driver, Elements.DesignCenter_LandingPage_Search_Button);

            //--- Expected Result: The user should be navigated to the Design Center Search Results page ---//
            test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + designCenter_searchResults_page_url);

            if (util.CheckElement(driver, Elements.DesignCenter_Search_SearchSuggestions_Section, 2))
            {
                //----- Design Center - Landing Page TestPlan > Test Case Title: Verify that "Show results instead for" is being displayed (AL-5714) -----//

                //--- Expected Result: "Show results instead for" will be displayed. ---//
                if (Locale.Equals("en"))
                {
                    test.validateString(driver, "Showing results for", util.GetText(driver, Elements.DesignCenter_Search_ShowingResultsFor_Label).Substring(0, util.GetText(driver, Elements.DesignCenter_Search_ShowingResultsFor_Label).LastIndexOf(" ") < 0 ? 0 : util.GetText(driver, Elements.DesignCenter_Search_ShowingResultsFor_Label).LastIndexOf(" ")));
                    test.validateString(driver, "Show results instead for" + " " + search_input, util.GetText(driver, Elements.DesignCenter_Search_ShowResultsInsteadFor_Label));
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.DesignCenter_Search_ShowingResultsFor_Label);
                    test.validateElementIsPresent(driver, Elements.DesignCenter_Search_ShowResultsInsteadFor_Label);
                }
            }
        }
    }
}