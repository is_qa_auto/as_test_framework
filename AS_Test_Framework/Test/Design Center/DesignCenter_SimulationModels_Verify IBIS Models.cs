﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.DesignCenter
{
    [TestFixture]
    public class DesignCenter_SimulationModels_IbisModels : BaseSetUp
    {
        public DesignCenter_SimulationModels_IbisModels() : base() { }

        //--- URLs ---//
        string ibisModels_page_url = "/design-center/simulation-models/ibis-models.html";
        string pdp_url_format = "/products/";

        //--- Labels ---//
        string ibisLicenseAgreement_txt = "License agreement for IBIS Models";

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that IBIS Models is Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that IBIS Models is Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that IBIS Models is Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that IBIS Models is Working as Expected in RU Locale")]
        public void DesignCenter_SimulationModels_VerifyIbisModels(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + ibisModels_page_url);

            //----- R3 > T1: Verify Page Title (AL-10662) -----//

            //--- Expected Result: The page title should be properly displayed and same label that was clicked on the category linked list ---//
            test.validateStringIsCorrect(driver, Elements.DesignCenter_IbisModels_Page_Title_Txt, util.GetText(driver, Elements.DesignCenter_SimulationModels_LeftRailNav_Selected_Val_Link));

            //----- R3 > T2: Verify IBIS Model  | Files (AL-9912, ACM-2690) -----//

            //--- Expected Result: The Search should be displayed on the Descriptive Summary section ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_IbisModels_Search_Txtbox);

            //--- Expected Result: The Default table display should be displayed on the Descriptive Summary section ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_IbisModels_Tbl);

            if (util.CheckElement(driver, Elements.DesignCenter_IbisModels_Tbl, 2))
            {
                //----- R3 > T3: Verify IBIS Model| Files Search Section -----//

                if (util.CheckElement(driver, Elements.DesignCenter_IbisModels_Search_Txtbox, 2))
                {
                    //--- Input ---//
                    string search_keyword = "AD";

                    //--- Action: Enter product number in search text field ---//
                    action.IDeleteValueOnFields(driver, Elements.DesignCenter_IbisModels_Search_Txtbox);
                    action.IType(driver, Elements.DesignCenter_IbisModels_Search_Txtbox, search_keyword);

                    //--- Expected Result: Search result should be displayed on the table ---//
                    test.validateElementIsPresent(driver, Elements.DesignCenter_IbisModels_Tbl_Search_Results);

                    action.IDeleteValueOnFields(driver, Elements.DesignCenter_IbisModels_Search_Txtbox);
                }

                if (util.CheckElement(driver, Elements.DesignCenter_IbisModels_Product_Search_Txtbox, 2))
                {
                    //--- Input ---//
                    string product_search_keyword = "AD";

                    //--- Action: Enter product number in  product field ---//
                    action.IDeleteValueOnFields(driver, Elements.DesignCenter_IbisModels_Product_Search_Txtbox);
                    action.IType(driver, Elements.DesignCenter_IbisModels_Product_Search_Txtbox, product_search_keyword);

                    //--- Expected Result: Search result should be displayed on the table ---//
                    test.validateElementIsPresent(driver, Elements.DesignCenter_IbisModels_Tbl_Search_Results);

                    action.IDeleteValueOnFields(driver, Elements.DesignCenter_IbisModels_Product_Search_Txtbox);
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.DesignCenter_IbisModels_Product_Search_Txtbox);
                }

                if (util.CheckElement(driver, Elements.DesignCenter_IbisModels_Description_Search_Txtbox, 2))
                {
                    //--- Input ---//
                    string desc_search_keyword = "SHARC";

                    //--- Action: Enter any text in description field ---//
                    action.IDeleteValueOnFields(driver, Elements.DesignCenter_IbisModels_Description_Search_Txtbox);
                    action.IType(driver, Elements.DesignCenter_IbisModels_Description_Search_Txtbox, desc_search_keyword);

                    //--- Expected Result: All product with searched description should be filtered ---//
                    test.validateElementIsPresent(driver, Elements.DesignCenter_IbisModels_Tbl_Search_Results);

                    action.IDeleteValueOnFields(driver, Elements.DesignCenter_IbisModels_Description_Search_Txtbox);
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.DesignCenter_IbisModels_Description_Search_Txtbox);
                }

                if (util.CheckElement(driver, Elements.DesignCenter_IbisModels_ModelFile_Search_Txtbox, 2))
                {
                    //--- Input ---//
                    string modelFile_search_keyword = "AD";

                    //--- Action: Enter model file in model file field ---//
                    action.IDeleteValueOnFields(driver, Elements.DesignCenter_IbisModels_ModelFile_Search_Txtbox);
                    action.IType(driver, Elements.DesignCenter_IbisModels_ModelFile_Search_Txtbox, modelFile_search_keyword);

                    //--- Expected Result: All product with search Model file field should be filtered ---//
                    test.validateElementIsPresent(driver, Elements.DesignCenter_IbisModels_Tbl_Search_Results);

                    action.IDeleteValueOnFields(driver, Elements.DesignCenter_IbisModels_ModelFile_Search_Txtbox);
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.DesignCenter_IbisModels_ModelFile_Search_Txtbox);
                }

                if (util.CheckElement(driver, Elements.DesignCenter_IbisModels_Tbl_Product_Links, 2))
                {
                    //----- R3 > T4: Verify links on the table -----//

                    //--- Action: Click on product number ---//
                    action.IOpenLinkInNewTab(driver, Elements.DesignCenter_IbisModels_Tbl_Product_Links);
                    Thread.Sleep(5000);

                    //--- Expected Result: Page should be redirected to Product Detail Page ---//
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + pdp_url_format);

                    driver.Close();
                    driver.SwitchTo().Window(driver.WindowHandles.First());
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.DesignCenter_IbisModels_Tbl_Product_Links);
                }

                if (util.CheckElement(driver, Elements.DesignCenter_IbisModels_Tbl_ModelFile_Links, 2))
                {
                    if (Locale.Equals("en"))
                    {
                        //----- R3 > T4: Verify links on the table -----//

                        //--- Action: Click on the files under model file ---//
                        action.IOpenLinkInNewTab(driver, Elements.DesignCenter_IbisModels_Tbl_ModelFile_Links);
                        Thread.Sleep(1000);

                        //--- Expected Result: IBIS License agreement page should be opened in new tab ---//
                        test.validateWindowTitleIsCorrect(driver, ibisLicenseAgreement_txt);
                    }
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.DesignCenter_IbisModels_Tbl_ModelFile_Links);
                }
            }
        }
    }
}