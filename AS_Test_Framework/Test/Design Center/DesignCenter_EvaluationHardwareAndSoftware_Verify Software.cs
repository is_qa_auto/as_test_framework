﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.DesignCenter
{
    [TestFixture]
    public class DesignCenter_EvaluationHardwareAndSoftware_Software : BaseSetUp
    {
        public DesignCenter_EvaluationHardwareAndSoftware_Software() : base() { }

        //--- URLs ---//
        string software_page_url = "/design-center/evaluation-hardware-and-software/software.html";
        string home_page_url = "/index.html";
        string algorithmicAddInAndPlugInSoftwareModules_page_url = "/algorithmic-add-in-plug-in-software-modules.html";
        string blackfinSoftwareModules_page_url = "/blackfin-software-modules.html";
        string sharcSoftwareModules_page_url = "/sharc-software-modules.html";
        string codeExamples_page_url = "/code-examples.html";
        string ccesExamples_page_url = "/all_examples.html";
        string addsp218x_adsp219x_codeExamples_page_url = "/adsp-218x-219x-code-examples.html";
        string tigerSharcProcessorCodeExamples_page_url = "/tigersharc-code-examples.html";
        string middleware_page_url = "/middleware.html";
        string softwareAndToolsAnomaliesSearch_page_url = "/software-tools-anomalies-search.html";

        //--- Labels ---//
        string algorithmicAddInAndPlugInSoftwareModules_txt = "Algorithmic, Add-in and Plug-in Software Modules";
        string codeExamples_txt = "Code Examples";
        string middleware_txt = "Middleware";
        string softwareAndToolsAnomaliesSearch_txt = "Software and Tools Anomalies Search";

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Breadcrumb is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Breadcrumb is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Breadcrumb is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Breadcrumb is Present and Working as Expected in RU Locale")]
        public void DesignCenter_EvaluationHardwareAndSoftware_Software_VerifyBreadcrumb(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + software_page_url);

            //----- Design Center - Processors and DSP TestPlan > R2 > T1: Verify the Software section -----//

            //--- Expected Result: The Software subcategory page should contain the Print widget ---//
            test.validateElementIsPresent(driver, Elements.Print_Widget_Btn);

            //--- Expected Result: The Software subcategory page should contain the My Analog widget ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Widget_Btn);

            //--- Expected Result: The Software subcategory page should contain the Breadcrumbs ---//
            test.validateElementIsPresent(driver, Elements.Breadcrumb_Details);

            if (util.CheckElement(driver, Elements.Breadcrumb_Details, 2))
            {
                //----- Design Center - Processors and DSP TestPlan > R2 > T2: Verify the breadcrumb -----//

                //--- Expected Result: The breadcrumb of the current  page should is just a label name of the page ---//
                test.validateStringIsCorrect(driver, Elements.Breadcrumb_CurrentPage_Txt, util.GetText(driver, Elements.DesignCenter_Software_Page_Title_Txt));

                if (util.CheckElement(driver, Elements.BreadCrumb_Home_Icon, 2) && !Configuration.Browser.Equals("Chrome_HL"))
                {
                    //--- Action: Click on the ADI Home Icon ---//
                    action.IOpenLinkInNewTab(driver, Elements.BreadCrumb_Home_Icon);

                    //--- Expected Result: Home Page should be displayed ---//
                    test.validateStringInstance(driver, driver.Url, home_page_url);
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.BreadCrumb_Home_Icon);
                }
            }
        }

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that Software is Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that Software is Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that Software is Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that Software is Working as Expected in RU Locale")]
        public void DesignCenter_EvaluationHardwareAndSoftware_VerifySoftware(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + software_page_url);

            //----- FIND THE RIGHT TOOLS FOR YOUR NEEDS -----//

            //----- Design Center - Processors and DSP TestPlan > R1 > T1: Verify the Processors and DSP Resources -----//

            //--- Expected Result: The Software subcategory page should contain the Search section ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_Software_Search_Sec);

            if (util.CheckElement(driver, Elements.DesignCenter_Software_Search_Sec, 2))
            {
                //----- Design Center - Processors and DSP TestPlan > R1 > T7_1: Verify the Search functionality (Find the right tools for your needs) -----//

                //--- Action: Enter identifier or keyword ---//
                string search_input = "cros";

                action.IDeleteValueOnFields(driver, Elements.DesignCenter_Software_Search_Txtbox);
                action.IType(driver, Elements.DesignCenter_Software_Search_Txtbox, search_input);

                //--- Expected Result: Search suggestion/predictive search will be displayed. ---//
                test.validateElementIsPresent(driver, Elements.DesignCenter_Software_Predictive_Search);

                action.IDeleteValueOnFields(driver, Elements.DesignCenter_Software_Search_Txtbox);
            }

            if (util.CheckElement(driver, Elements.DesignCenter_Software_SubSecs, 2) && Locale.Equals("en"))
            {
                int software_subSec_count = util.GetCount(driver, Elements.DesignCenter_Software_SubSecs);

                bool algorithmicAddInAndPlugInSoftwareModules_sec = false;
                bool codeExamples_sec = false;
                bool middleware_sec = false;
                bool softwareAndToolsAnomaliesSearch_sec = false;

                for (int software_ctr = 1; software_ctr <= software_subSec_count; software_ctr++)
                {
                    if (util.GetText(driver, By.CssSelector("div[name$='content_section']>div:nth-last-of-type(" + software_ctr + ") * h3[class*='header']")).Equals(algorithmicAddInAndPlugInSoftwareModules_txt))
                    {
                        //----- ALGORITHMIC, ADD-IN AND PLUG-IN SOFTWARE MODULES -----//

                        algorithmicAddInAndPlugInSoftwareModules_sec = true;

                        //--- Locators ----//
                        string subCategory_sec_locator = "div[name$='content_section']>div:nth-last-of-type(" + software_ctr + ")";
                        string viewMore_link_locator = "div[name$='content_section']>div:nth-last-of-type(" + software_ctr + ") * a[class$='link']";

                        //----- Design Center - Processors and DSP TestPlan > R1 > T5: Verify the Software subcategory linked lists -----//

                        //--- Expected Result: The Algorithmic, Add-in and Plug-in Software Modules linked lists should be available on Software subcategory page---//
                        test.validateElementIsPresent(driver, By.CssSelector(subCategory_sec_locator));

                        if (util.CheckElement(driver, By.CssSelector(viewMore_link_locator), 2))
                        {
                            //----- Design Center - Processors and DSP TestPlan > R2 > T6_1: Verify the Algorithmic, Add-in and Plug-in Software Modules page -----//

                            //--- Action: Click on the View More link under the Algorithmic, Add-in and Plug-in Software Modules linkedlist ---//
                            action.IOpenLinkInNewTab(driver, By.CssSelector(viewMore_link_locator));
                            Thread.Sleep(1000);

                            //--- Expected Result: The Algorithmic, Add-in and Plug-in Software Modules page should be displayed ---//
                            test.validateStringInstance(driver, driver.Url, algorithmicAddInAndPlugInSoftwareModules_page_url);

                            if (driver.Url.Contains(algorithmicAddInAndPlugInSoftwareModules_page_url))
                            {
                                if (util.CheckElement(driver, Elements.DesignCenter_AlgorithmicAddInAndPlugInSoftwareModules_AlgorithmicSoftwareModulesForBlackfinProcessors_Link, 2))
                                {
                                    //--- Action: Click on the Algorithmic Software Modules for Blackfin Processors link ---//
                                    action.IOpenLinkInNewTab(driver, Elements.DesignCenter_AlgorithmicAddInAndPlugInSoftwareModules_AlgorithmicSoftwareModulesForBlackfinProcessors_Link);
                                    Thread.Sleep(1000);

                                    //--- Expected Result: The Blackfin Software Modules page should be displayed ---//
                                    test.validateStringInstance(driver, driver.Url, blackfinSoftwareModules_page_url);

                                    driver.Close();
                                    driver.SwitchTo().Window(driver.WindowHandles.Last());
                                }
                                else
                                {
                                    test.validateElementIsPresent(driver, Elements.DesignCenter_AlgorithmicAddInAndPlugInSoftwareModules_AlgorithmicSoftwareModulesForBlackfinProcessors_Link);
                                }

                                if (util.CheckElement(driver, Elements.DesignCenter_AlgorithmicAddInAndPlugInSoftwareModules_AlgorithmicSoftwareModulesForSharcProcessors_Link, 2))
                                {
                                    //--- Action: Click on the Algorithmic Software Modules for SHARC Processors link ---//
                                    action.IOpenLinkInNewTab(driver, Elements.DesignCenter_AlgorithmicAddInAndPlugInSoftwareModules_AlgorithmicSoftwareModulesForSharcProcessors_Link);
                                    Thread.Sleep(1000);

                                    //--- Expected Result: The SHARC Software Modules page should be displayed ---//
                                    test.validateStringInstance(driver, driver.Url, sharcSoftwareModules_page_url);

                                    driver.Close();
                                    driver.SwitchTo().Window(driver.WindowHandles.Last());
                                }
                                else
                                {
                                    test.validateElementIsPresent(driver, Elements.DesignCenter_AlgorithmicAddInAndPlugInSoftwareModules_AlgorithmicSoftwareModulesForSharcProcessors_Link);
                                }
                            }

                            driver.Close();
                            driver.SwitchTo().Window(driver.WindowHandles.First());
                        }
                        else
                        {
                            test.validateElementIsPresent(driver, By.CssSelector(viewMore_link_locator));
                        }
                    }

                    if (util.GetText(driver, By.CssSelector("div[name$='content_section']>div:nth-last-of-type(" + software_ctr + ") * h3[class*='header']")).Equals(codeExamples_txt))
                    {
                        //----- CODE EXAMPLES -----//

                        codeExamples_sec = true;

                        //--- Locators ----//
                        string subCategory_sec_locator = "div[name$='content_section']>div:nth-last-of-type(" + software_ctr + ")";
                        string viewMore_link_locator = "div[name$='content_section']>div:nth-last-of-type(" + software_ctr + ") * a[class$='link']";

                        //----- Design Center - Processors and DSP TestPlan > R1 > T5: Verify the Software subcategory linked lists -----//

                        //--- Expected Result: The Code Examples linked lists should be available on Software subcategory page---//
                        test.validateElementIsPresent(driver, By.CssSelector(subCategory_sec_locator));

                        if (util.CheckElement(driver, By.CssSelector(viewMore_link_locator), 2))
                        {
                            //----- Design Center - Processors and DSP TestPlan > R2 > T6_2: Verify the Code Examples page -----//

                            //--- Action: Click on the View More link under the Code Examples linkedlist ---//
                            action.IOpenLinkInNewTab(driver, By.CssSelector(viewMore_link_locator));
                            Thread.Sleep(1000);

                            //--- Expected Result: The Code Examples page should be displayed ---//
                            test.validateStringInstance(driver, driver.Url, codeExamples_page_url);

                            if (driver.Url.Contains(codeExamples_page_url))
                            {
                                if (util.CheckElement(driver, Elements.DesignCenter_CodeExamples_BlackfinAndSharcCodeExamplesForCcesSoftware_Link, 2))
                                {
                                    //--- Action: Click on the Blackfin and SHARC code examples for CrossCore® Software link ---//
                                    action.IOpenLinkInNewTab(driver, Elements.DesignCenter_CodeExamples_BlackfinAndSharcCodeExamplesForCcesSoftware_Link);
                                    Thread.Sleep(2000);

                                    //--- Expected Result: The CrossCore® Embedded Studio (CCES) Examples page should be displayed ---//
                                    test.validateStringInstance(driver, driver.Url, ccesExamples_page_url);

                                    driver.Close();
                                    driver.SwitchTo().Window(driver.WindowHandles.Last());
                                }
                                else
                                {
                                    test.validateElementIsPresent(driver, Elements.DesignCenter_CodeExamples_BlackfinAndSharcCodeExamplesForCcesSoftware_Link);
                                }

                                if (util.CheckElement(driver, Elements.DesignCenter_CodeExamples_Adsp21xxCodeExamplesForVisualDsp_Link, 2))
                                {
                                    //--- Action: Click on the ADSP-21xx code examples for VisualDSP++ link ---//
                                    action.IOpenLinkInNewTab(driver, Elements.DesignCenter_CodeExamples_Adsp21xxCodeExamplesForVisualDsp_Link);
                                    Thread.Sleep(1000);

                                    //--- Expected Result: The ADSP-218x/ADSP-219x Code Examples page should be displayed ---//
                                    test.validateStringInstance(driver, driver.Url, addsp218x_adsp219x_codeExamples_page_url);

                                    driver.Close();
                                    driver.SwitchTo().Window(driver.WindowHandles.Last());
                                }
                                else
                                {
                                    test.validateElementIsPresent(driver, Elements.DesignCenter_CodeExamples_Adsp21xxCodeExamplesForVisualDsp_Link);
                                }

                                if (util.CheckElement(driver, Elements.DesignCenter_CodeExamples_TigerSharcCodeExamplesForVisualDsp_Link, 2))
                                {
                                    //--- Action: Click on the TigerSHARC code examples for VisualDSP++ link ---//
                                    action.IOpenLinkInNewTab(driver, Elements.DesignCenter_CodeExamples_TigerSharcCodeExamplesForVisualDsp_Link);
                                    Thread.Sleep(1000);

                                    //--- Expected Result: The TigerSHARC Processor Code Examples page should be displayed ---//
                                    test.validateStringInstance(driver, driver.Url, tigerSharcProcessorCodeExamples_page_url);

                                    driver.Close();
                                    driver.SwitchTo().Window(driver.WindowHandles.Last());
                                }
                                else
                                {
                                    test.validateElementIsPresent(driver, Elements.DesignCenter_CodeExamples_TigerSharcCodeExamplesForVisualDsp_Link);
                                }
                            }

                            driver.Close();
                            driver.SwitchTo().Window(driver.WindowHandles.First());
                        }
                        else
                        {
                            test.validateElementIsPresent(driver, By.CssSelector(viewMore_link_locator));
                        }
                    }

                    if (util.GetText(driver, By.CssSelector("div[name$='content_section']>div:nth-last-of-type(" + software_ctr + ") * h3[class*='header']")).Equals(middleware_txt))
                    {
                        //----- MIDDLEWARE -----//

                        middleware_sec = true;

                        //--- Locators ----//
                        string subCategory_sec_locator = "div[name$='content_section']>div:nth-last-of-type(" + software_ctr + ")";
                        string viewMore_link_locator = "div[name$='content_section']>div:nth-last-of-type(" + software_ctr + ") * a[class$='link']";

                        //----- Design Center - Processors and DSP TestPlan > R1 > T5: Verify the Software subcategory linked lists -----//

                        //--- Expected Result: The Middleware linked lists should be available on Software subcategory page---//
                        test.validateElementIsPresent(driver, By.CssSelector(subCategory_sec_locator));

                        if (util.CheckElement(driver, By.CssSelector(viewMore_link_locator), 2))
                        {
                            //----- Design Center - Processors and DSP TestPlan > R2 > T6_3: Verify the Middleware page -----//

                            //--- Action: Click on the View More link under the Middleware linkedlist ---//
                            action.IOpenLinkInNewTab(driver, By.CssSelector(viewMore_link_locator));
                            Thread.Sleep(2000);

                            //--- Expected Result: The Middleware page should be displayed ---//
                            test.validateStringInstance(driver, driver.Url, middleware_page_url);

                            if (driver.Url.Contains(middleware_page_url))
                            {
                                if (util.CheckElement(driver, Elements.DesignCenter_Middleware_Links, 2))
                                {
                                    //--- Action: Click on any Middleware ---//
                                    action.IOpenLinkInNewTab(driver, Elements.DesignCenter_Middleware_Links);
                                    Thread.Sleep(1000);

                                    //--- Expected Result: The detail page of the Middleware should be displayed ---//
                                    test.validateStringInstance(driver, driver.Url, "/software/");

                                    driver.Close();
                                    driver.SwitchTo().Window(driver.WindowHandles.Last());
                                }
                                else
                                {
                                    test.validateElementIsPresent(driver, Elements.DesignCenter_Middleware_Links);
                                }
                            }

                            driver.Close();
                            driver.SwitchTo().Window(driver.WindowHandles.First());
                        }
                        else
                        {
                            test.validateElementIsPresent(driver, By.CssSelector(viewMore_link_locator));
                        }
                    }

                    if (util.GetText(driver, By.CssSelector("div[name$='content_section']>div:nth-last-of-type(" + software_ctr + ") * h3[class*='header']")).Equals(softwareAndToolsAnomaliesSearch_txt))
                    {
                        //----- SOFTWARE AND TOOLS ANOMALIES SEARCH -----//

                        softwareAndToolsAnomaliesSearch_sec = true;

                        //--- Locators ----//
                        string subCategory_sec_locator = "div[name$='content_section']>div:nth-last-of-type(" + software_ctr + ")";
                        string viewMore_link_locator = "div[name$='content_section']>div:nth-last-of-type(" + software_ctr + ") * a[class$='link']";

                        //----- Design Center - Processors and DSP TestPlan > R1 > T5: Verify the Software subcategory linked lists -----//

                        //--- Expected Result: The Software and Tools Anomalies Search linked lists should be available on Software subcategory page---//
                        test.validateElementIsPresent(driver, By.CssSelector(subCategory_sec_locator));

                        if (util.CheckElement(driver, By.CssSelector(viewMore_link_locator), 2))
                        {
                            //----- Design Center - Processors and DSP TestPlan > R2 > T6_4: Verify the Software and Tools Anomalies Search page -----//

                            //--- Action: Click on the View More link on the Software and Tools Anomalies Search linked list ---//
                            action.IOpenLinkInNewTab(driver, By.CssSelector(viewMore_link_locator));
                            Thread.Sleep(1000);

                            //--- Expected Result: The Software and Tools Anomalies Search page should be displayed ---//
                            test.validateStringInstance(driver, driver.Url, softwareAndToolsAnomaliesSearch_page_url);

                            driver.Close();
                            driver.SwitchTo().Window(driver.WindowHandles.First());
                        }
                        else
                        {
                            test.validateElementIsPresent(driver, By.CssSelector(viewMore_link_locator));
                        }
                    }

                    if (algorithmicAddInAndPlugInSoftwareModules_sec == true && codeExamples_sec == true && middleware_sec == true && softwareAndToolsAnomaliesSearch_sec == true)
                    {
                        break;
                    }
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.DesignCenter_Software_SubSecs);
            }
        }
    }
}