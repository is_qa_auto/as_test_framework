﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.DesignCenter
{
    [TestFixture]
    public class DesignCenter_ReferenceDesigns : BaseSetUp
    {
        public DesignCenter_ReferenceDesigns() : base() { }

        //--- URLs ---//
        string referenceDesigns_page_url = "/design-center/reference-designs.html";
        string searchDesignCenter_page_url = "/design-center/search.html";
        string referenceDesigns_url_format_url = "/design-center/reference-designs/";
        string cftl_url_format = "/circuits-from-the-lab/";

        //--- Labels ---//
        string referenceDesigns_txt = "Reference Designs";
        string newest_txt = "Newest";
        string driversAndReferenceCode_txt = "Drivers & Reference Code";

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that Reference Designs is Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that Reference Designs is Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that Reference Designs is Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that Reference Designs is Working as Expected in RU Locale")]
        public void DesignCenter_VerifyReferenceDesigns(string Locale)
        {
            //--- Action: Go to Reference Design Home page ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + referenceDesigns_page_url);

            ////----- Reference Designs Redesign_Test Plan > Desktop - RD_New Component Tab > R1 > T9: Verify if Header is available -----// --> NOTE: Already covered in the Header Scripts.

            ////--- Expected Result: Header displayed ---//
            //test.validateElementIsPresent(driver, Elements.Header);

            //----- Reference Designs Redesign_Test Plan > Desktop - RD_New Component Tab > R1 > T1: Verify the Reference Design title Header if display -----//

            //--- Expected Result: Reference Design Title Header displayed ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_ReferenceDesigns_Page_Title_Txt);

            //----- Reference Designs Redesign_Test Plan > Desktop - RD_New Component Tab > R1 > T2: Verify the description below the Reference Design title header if display -----//

            //--- Expected Result: Description displayed ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_ReferenceDesigns_Desc_Txt);

            ////----- Reference Designs Redesign_Test Plan > Desktop - RD_New Component Tab > R1 > T8: Verify if footer is available -----// --> NOTE: Already covered in the Footer Scripts.

            ////--- Expected Result: Footer displayed ---//
            //test.validateElementIsPresent(driver, Elements.Footer);
        }

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Search by Keyword or Design Number is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Search by Keyword or Design Number is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Search by Keyword or Design Number is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Search by Keyword or Design Number is Present and Working as Expected in RU Locale")]
        public void DesignCenter_ReferenceDesigns_VerifySearchByKeywordOrDesignNumber(string Locale)
        {
            //----- Reference Designs Redesign_Test Plan > Desktop - RD_New Component Tab > R1 > T3: Verify the search section if available -----//

            //--- Action: Go to Reference Design Home page ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + referenceDesigns_page_url);

            //--- Expected Result: Search seaction available ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec);

            if (util.CheckElement(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec, 2))
            {
                //----- SEARCH BY KEYWORD OR DESIGN NUMBER -----//

                //----- Reference Designs Redesign_Test Plan > Desktop - Desktop - RD_Quick Search > R1 > T1: Verify Search section in Reference Design page: -----//

                //--- Expected Result: Search by Keyword or Design Number textbox and enabled ---//
                test.validateElementIsEnabled(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec_SearchByKeywordOrDesignNumber_InputBx);

                //--- Expected Result: Search button is displayed ---//
                test.validateElementIsPresent(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec_Search_Btn);

                if (util.CheckElement(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec_SearchByKeywordOrDesignNumber_InputBx, 2))
                {
                    //----- Reference Designs Redesign_Test Plan > Desktop - Desktop - RD_Quick Search > R1 > T3: Verify Search function with keyword query -----//

                    //--- Action: Enter keyword query in textbox ---//
                    string search_input = "CN0";

                    action.IDeleteValueOnFields(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec_SearchByKeywordOrDesignNumber_InputBx);
                    action.IType(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec_SearchByKeywordOrDesignNumber_InputBx, search_input);

                    //--- Expected Result: Auto-suggest will appear after entering at least 3 characters ---//
                    test.validateElementIsPresent(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec_SearchByKeywordOrDesignNumber_AutoSuggest);

                    if (util.CheckElement(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec_SearchByKeywordOrDesignNumber_AutoSuggest_Items, 2))
                    {
                        //----- Reference Designs Redesign_Test Plan > Desktop - Desktop - RD_Quick Search > R1 > T4: Verify auto-suggest functionality -----//

                        //--- Action: Click an auto-suggested item ---//
                        action.IOpenLinkInNewTab(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec_SearchByKeywordOrDesignNumber_AutoSuggest_Items);
                        Thread.Sleep(2000);

                        //--- Expected Result: User is redirected to page ---//
                        test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + referenceDesigns_url_format_url);

                        driver.Close();
                        driver.SwitchTo().Window(driver.WindowHandles.First());
                    }

                    action.IDeleteValueOnFields(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec_SearchByKeywordOrDesignNumber_InputBx);
                }

                if (util.CheckElement(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec_Search_Btn, 2))
                {
                    //----- Reference Designs Redesign_Test Plan > Desktop - RD_New Component Tab > R2 > T14: Verify if the Search button is clickable -----//

                    //--- Action: Click the mouse ---//
                    action.IClick(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec_Search_Btn);
                    Thread.Sleep(1000);

                    //--- Expected Result: The page redirected to search result. ---//
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + searchDesignCenter_page_url);
                }
            }
        }

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Search by Filters is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Search by Filters is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Search by Filters is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Search by Filters is Present and Working as Expected in RU Locale")]
        public void DesignCenter_ReferenceDesigns_VerifySearchByFilters(string Locale)
        {
            //--- Action: Go to Reference Design Home page ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + referenceDesigns_page_url);
            Thread.Sleep(1000);

            if (util.CheckElement(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec, 2))
            {
                //----- SEARCH BY FILTERS – OR APPLY FILTERS TO YOUR KEYWORD SEARCH -----//

                //--- MARKETS AND TECHNOLOGY SOLUTIONS - DROPDOWN ---//

                //----- Reference Designs Redesign_Test Plan > Desktop - Desktop - RD_Quick Search > R1 > T5: Verify facets/filters dropdown -----//

                //--- Expected Result: The Markets and Technology Solutions dropdown is displayed ---//
                test.validateElementIsPresent(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec_MarketsAndTechnologySolutions_Dd);

                if (util.CheckElement(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec_MarketsAndTechnologySolutions_Dd, 2))
                {
                    //----- Reference Designs Redesign_Test Plan > Desktop - RD_New Component Tab > R2 > T3: Verify if the Market and Technology Solution dropdown field is enable -----//

                    //--- Action: Click the Market and Technology Solution dropdown field. ---//
                    action.IClick(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec_MarketsAndTechnologySolutions_Dd_Btn);

                    //--- Expected Result: Dropdown menu displayed ---//
                    test.validateElementIsPresent(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec_MarketsAndTechnologySolutions_Dd_Menu);

                    if (util.CheckElement(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec_MarketsAndTechnologySolutions_Dd_Menu, 2))
                    {
                        //----- Reference Designs Redesign_Test Plan > Desktop - Desktop - RD_Quick Search > R1 > T7: Verify Search function with Markets and Technology Solutions facet/filter -----//

                        //--- Expected Result: Section divider is displayed ---//
                        test.validateElementIsPresent(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec_MarketsAndTechnologySolutions_Dd_Menu_SecDivider);

                        //----- Reference Designs Redesign_Test Plan > Desktop - RD_New Component Tab > R2 > T5: Verify if the user can select an option in Market and Technology Solution dropdown field. -----//
                        string marketsAndTechnologySolutions_value = util.GetText(driver, By.CssSelector("li[data-facetcategory^='market']>ul>li:nth-of-type(1)"));

                        //--- Action: Click any option ---//
                        action.IClick(driver, By.CssSelector("li[data-facetcategory^='market']>ul>li:nth-of-type(1)"));

                        //--- Expected Result: The option(Automotive) clicked displayed in  Market and Technology Solution dropdown ---//
                        test.validateStringIsCorrect(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec_MarketsAndTechnologySolutions_Dd_Sel_Val, marketsAndTechnologySolutions_value);

                        //----- Reference Designs Redesign_Test Plan > Desktop - Desktop - RD_Quick Search > R1 > T5: Verify facets/filters dropdown -----//

                        //--- Expected Result: Dropdown is closed ---//
                        test.validateElementIsNotPresent(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec_MarketsAndTechnologySolutions_Dd_Menu);
                    }
                }

                //--- DESIGN RESOURCES - DROPDOWN ---//

                //----- Reference Designs Redesign_Test Plan > Desktop - Desktop - RD_Quick Search > R1 > T5: Verify facets/filters dropdown -----//

                //--- Expected Result: The Design Resources dropdowns is displayed ---//
                test.validateElementIsPresent(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec_DesignResources_Dd);

                if (util.CheckElement(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec_DesignResources_Dd, 2))
                {
                    //----- Reference Designs Redesign_Test Plan > Desktop - RD_New Component Tab > R2 > T6: Verify if the Design Resource dropdown field is enable -----//

                    //--- Action: Click the Design Resource dropdown field. ---//
                    action.IClick(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec_DesignResources_Dd_Btn);

                    //--- Expected Result: Dropdown menu displayed ---//
                    test.validateElementIsPresent(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec_DesignResources_Dd_Menu);

                    if (util.CheckElement(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec_DesignResources_Dd_Menu, 2))
                    {
                        //----- Reference Designs Redesign_Test Plan > Desktop - RD_New Component Tab > R2 > T8: Verify if the user can select an option in Design Resource dropdown field. -----//
                        string designResources_value = util.GetText(driver, By.CssSelector("li[data-facetcategory^='design_resource']>ul>li:nth-of-type(1)"));

                        //--- Action: Click any option ---//
                        action.IClick(driver, By.CssSelector("li[data-facetcategory^='design_resource']>ul>li:nth-of-type(1)"));

                        //--- Expected Result: The option clicked displayed inDesign Resource dropdown ---//
                        test.validateStringIsCorrect(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec_DesignResources_Dd_Sel_Val, designResources_value);

                        if (util.CheckElement(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec_MarketsAndTechnologySolutions_Dd, 2))
                        {
                            //----- Reference Designs Redesign_Test Plan > Desktop - Desktop - RD_Quick Search > R1 > T5: Verify facets/filters dropdown -----//

                            //--- Action: Click a dropdown ---//
                            action.IClick(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec_DesignResources_Dd_Btn);

                            //--- Action: Click outside the dropdown ---//
                            action.IClick(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec_MarketsAndTechnologySolutions_Dd_Btn);

                            //--- Expected Result: Dropdown is closed ---//
                            test.validateElementIsNotPresent(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec_DesignResources_Dd_Menu);
                        }
                    }
                }

                //--- PRODUCT CATEGORIES - DROPDOWN ---//

                //----- Reference Designs Redesign_Test Plan > Desktop - Desktop - RD_Quick Search > R1 > T5: Verify facets/filters dropdown -----//

                //--- Expected Result: The Product Categories dropdown is displayed ---//
                test.validateElementIsPresent(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec_ProductCategories_Dd);

                if (util.CheckElement(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec_ProductCategories_Dd, 2))
                {
                    //----- Reference Designs Redesign_Test Plan > Desktop - RD_New Component Tab > R2 > T9: Verify if the Product Categories dropdown field is enable -----//

                    //--- Action: Click the Product Categories dropdown field. ---//
                    action.IClick(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec_ProductCategories_Dd_Btn);

                    //--- Expected Result: Dropdown menu displayed ---//
                    test.validateElementIsPresent(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec_ProductCategories_Dd_Menu);

                    if (util.CheckElement(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec_ProductCategories_Dd_Menu, 2))
                    {
                        //----- Reference Designs Redesign_Test Plan > Desktop - RD_New Component Tab > R2 > T11: Verify if the user can select an option in Product Categories dropdown field. -----//
                        string productCategories_value = util.GetText(driver, By.CssSelector("li[data-facetcategory^='prod_cat']>ul>li:nth-of-type(1)"));

                        //--- Action: Click any option ---//
                        action.IClick(driver, By.CssSelector("li[data-facetcategory^='prod_cat']>ul>li:nth-of-type(1)"));

                        //--- Expected Result: The option clicked displayed in Product Categories dropdown ---//
                        test.validateStringIsCorrect(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec_ProductCategories_Dd_Sel_Val, productCategories_value);

                        //----- Reference Designs Redesign_Test Plan > Desktop - RD_New Component Tab > R2 > T12: Verify the sub-menu dropdown in Product Categories dropdown -----//

                        //--- Action: Click the Product Categories dropdown field. ---//
                        action.IClick(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec_ProductCategories_Dd_Btn);

                        //--- Action: Hover the mouse in any Product Categories option ---//
                        action.IMouseOverTo(driver, By.CssSelector("li[data-facetcategory^='prod_cat']>ul>li:nth-of-type(2)"));

                        //--- Expected Result: sub-menu displayed ---//
                        test.validateElementIsPresent(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec_ProductCategories_Dd_SubMenu);

                        if (util.CheckElement(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec_ProductCategories_Dd_SubMenu, 2))
                        {
                            action.IClick(driver, By.CssSelector("li[data-facetcategory^='prod_cat']>ul>li:nth-of-type(2)"));
                        }

                        //----- Reference Designs Redesign_Test Plan > Desktop - Desktop - RD_Quick Search > R1 > T5: Verify facets/filters dropdown -----//

                        //--- Action: Click a dropdown ---//
                        action.IClick(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec_ProductCategories_Dd_Btn);

                        //--- Action: Click the same dropdown ---//
                        action.IClick(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec_ProductCategories_Dd_Btn);

                        //--- Expected Result: Dropdown is closed ---//
                        test.validateElementIsNotPresent(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec_ProductCategories_Dd_Menu);
                    }
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec);
            }
        }

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the New Designs Section is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the New Designs Section is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the New Designs Section is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the New Designs Section is Present and Working as Expected in RU Locale")]
        public void DesignCenter_ReferenceDesigns_VerifyNewDesignsSection(string Locale)
        {
            //----- Reference Designs Redesign_Test Plan > Desktop - RD_New Component Tab > R1 > T4: Verify the New Designs section if available -----//

            //--- Action: Go to Reference Design Home page ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + referenceDesigns_page_url);

            //--- Expected Result: New Design section available ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_ReferenceDesigns_NewDesigns_Sec);

            if (util.CheckElement(driver, Elements.DesignCenter_ReferenceDesigns_NewDesigns_Sec, 2))
            {
                //----- Reference Designs Redesign_Test Plan > Desktop - RD_New Component Tab > R3 > T2: Verify if the most recently launch has a image -----//

                //--- Expected Result: Image(card) displayed ---//
                test.validateElementIsPresent(driver, Elements.DesignCenter_ReferenceDesigns_NewDesigns_Sec_MostRecentlyLaunched_Item_Img);

                if (util.CheckElement(driver, Elements.DesignCenter_ReferenceDesigns_NewDesigns_Sec_MostRecentlyLaunched_Item, 2) && !Configuration.Browser.Equals("Chrome_HL"))
                {
                    //----- Reference Designs Redesign_Test Plan > Desktop - RD_New Component Tab > R3 > T3: Verify if the entired card is clickable -----//

                    //--- Action: Click the most recently launch ---//
                    action.IOpenLinkInNewTab(driver, Elements.DesignCenter_ReferenceDesigns_NewDesigns_Sec_MostRecentlyLaunched_Item);
                    //Thread.Sleep(2000);

                    //--- Expected Result: The card is clickable ---//
                    test.validateStringInstance(driver, driver.Url, cftl_url_format);

                    driver.Close();
                    driver.SwitchTo().Window(driver.WindowHandles.First());
                }

                if (util.CheckElement(driver, Elements.DesignCenter_ReferenceDesigns_NewDesigns_Sec_Items, 2))
                {
                    //----- Reference Designs Redesign_Test Plan > Desktop - RD_New Component Tab > R3 > T1: Verify the number of item display in New Design Section -----//

                    //--- Expected Result: It should only 6 item displayed not included the Card ---//
                    test.validateCountIsEqual(driver, 6, util.GetCount(driver, Elements.DesignCenter_ReferenceDesigns_NewDesigns_Sec_Items));

                    //----- Reference Designs Redesign_Test Plan > Desktop - RD_New Component Tab > R3 > T5: Verify if the Reference Design name display  -----//

                    //--- Expected Result: Reference Design Name display ---//
                    test.validateElementIsPresent(driver, Elements.DesignCenter_ReferenceDesigns_NewDesigns_Sec_Item_ReferenceDesign_Name_Link);

                    //----- Reference Designs Redesign_Test Plan > Desktop - RD_New Component Tab > R3 > T6: Verify if the Reference Design short description display -----//

                    //--- Expected Result: Reference Design short description displayed ---//
                    test.validateElementIsPresent(driver, Elements.DesignCenter_ReferenceDesigns_NewDesigns_Sec_Item_ReferenceDesign_ShortDesc_Txt);

                    //----- Reference Designs Redesign_Test Plan > Desktop - RD_New Component Tab > R3 > T7: Verify if the Reference Design name is clickable -----//

                    //--- Action: Click the Reference name of any item ---//
                    action.IOpenLinkInNewTab(driver, Elements.DesignCenter_ReferenceDesigns_NewDesigns_Sec_Item_ReferenceDesign_Name_Link);
                    Thread.Sleep(3000);

                    //--- Expected Result: The Reference design open ---//
                    test.validateStringInstance(driver, driver.Url, cftl_url_format);

                    driver.Close();
                    driver.SwitchTo().Window(driver.WindowHandles.First());
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.DesignCenter_ReferenceDesigns_NewDesigns_Sec_Items);
                }

                //----- Reference Designs Redesign_Test Plan > Desktop - RD_New Component Tab > R3 > T10: Verify if the "VIEW ALL REFERENCE DESIGNS" button display -----//

                //--- Expected Result: "VIEW ALL REFERENCE DESIGNS" button displayed ---//
                test.validateElementIsPresent(driver, Elements.DesignCenter_ReferenceDesigns_NewDesigns_ViewAllNewReferenceDesigns_Btn);

                if (util.CheckElement(driver, Elements.DesignCenter_ReferenceDesigns_NewDesigns_ViewAllNewReferenceDesigns_Btn, 2))
                {
                    //----- Reference Designs Redesign_Test Plan > Desktop - RD_New Component Tab > R3 > T11: Verify if the  "VIEW ALL REFERENCE DESIGNS" button is clickable -----//

                    //--- Action: Click the  "VIEW ALL REFERENCE DESIGNS" button ---//
                    action.IOpenLinkInNewTab(driver, Elements.DesignCenter_ReferenceDesigns_NewDesigns_ViewAllNewReferenceDesigns_Btn);
                    Thread.Sleep(5000);

                    //--- Expected Result: The page redirected to search result page. ---//
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + searchDesignCenter_page_url);

                    if (driver.Url.Contains("analog.com/" + Locale + searchDesignCenter_page_url))
                    {
                        //----- SEARCH DESIGN CENTER - PAGE -----//

                        if (Locale.Equals("en"))
                        {
                            //----- Reference Designs Redesign_Test Plan > Desktop - RD_3rd Party > R1 > T3: Verify New Reference Design Search Result page -----//

                            //--- Expected Result: "Reference Design" filter is selected under Sections ---//
                            test.validateStringIsCorrect(driver, Elements.DesignCenter_Search_Selected_Section_Filter, referenceDesigns_txt);

                            //----- Reference Designs Redesign_Test Plan > Desktop - RD_Onsite Search Tab > R1 > T1: Verify Onsite Search Results of Quick Search (NO Search Criteria) (IQ-9332/AL-16284) -----//

                            //--- Expected Result: The Search results are pre-sorted based on "Newest" ---//
                            test.validateSelectedValueIsCorrect(driver, Elements.DesignCenter_Search_SearchOrder_Dd, newest_txt);
                        }

                        //----- Reference Designs Redesign_Test Plan > Desktop - RD_Onsite Search Tab > R1 > T1: Verify Onsite Search Results of Quick Search (NO Search Criteria) (IQ-9332/AL-16284) -----//

                        //--- Expected Result: NO filter is pre-selected ---//
                        test.validateElementIsNotPresent(driver, Elements.DesignCenter_Search_Selected_Filter_Filter_Cb);
                        test.validateElementIsNotPresent(driver, Elements.DesignCenter_Search_Selected_Filter_Filter_Rb);

                        //--- Expected Result: An asterisk is displayed in the search textbox ---//
                        test.validateStringInstance(driver, "*", util.ReturnAttribute(driver, Elements.DesignCenter_Search_InputBx, "value"));
                     
                        //--- Action: Enter search criteria and select Filter facets same in Quick Search ---//
                        string search_input = "CN";

                        action.IDeleteValueOnFields(driver, Elements.DesignCenter_Search_InputBx);
                        action.IType(driver, Elements.DesignCenter_Search_InputBx, search_input);
                        action.IClick(driver, Elements.DesignCenter_Search_Search_Icon);
                        Thread.Sleep(3000);
                        //--- Expected Result: The System displays the search results ---//
                        test.validateElementIsPresent(driver, Elements.DesignCenter_Search_SearchResults_Items);
                    }
                }
            }
        }

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Drivers & Reference Code Section is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Drivers & Reference Code Section is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Drivers & Reference Code Section is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Drivers & Reference Code Section is Present and Working as Expected in RU Locale")]
        public void DesignCenter_ReferenceDesigns_VerifyDriversAndReferenceCodeSection(string Locale)
        {
            //----- Reference Designs Redesign_Test Plan > Desktop - RD_New Component Tab > R1 > T4: Verify the Drivers & Reference Code section if available -----//

            //--- Action: Go to Reference Design Home page ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + referenceDesigns_page_url);

            if (util.CheckElement(driver, Elements.DesignCenter_ReferenceDesigns_SubSecs, 2))
            {
                int rd_subsec_count = util.GetCount(driver, Elements.DesignCenter_ReferenceDesigns_SubSecs);

                for (int rd_ctr = 2; rd_ctr <= rd_subsec_count; rd_ctr++)
                {
                    if (util.GetText(driver, By.CssSelector("div[class='content']>div>div:nth-last-of-type(" + rd_ctr + ")>div[name*='designcenter']>div>div>h3[class*='header']")).Equals(driversAndReferenceCode_txt) && Locale.Equals("en"))
                    {
                        //--- Locators ----//
                        string section_locator = "div[class='content']>div>div:nth-last-of-type(" + rd_ctr + ")>div[name*='designcenter']>div>div";

                        //--- Expected Result: Drivers & Reference Code section available ---//
                        test.validateElementIsPresent(driver, By.CssSelector(section_locator));

                        if (util.CheckElement(driver, By.CssSelector(section_locator), 2))
                        {
                            //--- Locators ----//
                            string titleHeader_txt_locator = "div[class='content']>div>div:nth-last-of-type(" + rd_ctr + ")>div[name*='designcenter']>div>div>h3[class*='header']";
                            string shortDesc_txt_locator = "div[class='content']>div>div:nth-last-of-type(" + rd_ctr + ")>div[name*='designcenter']>div>div>p";

                            //----- Reference Designs Redesign_Test Plan > Desktop - RD_New Component Tab > R6 > T: Verify the Title Header -----//

                            //--- Expected Result: Drivers & Reference Code displayed ---//
                            test.validateElementIsPresent(driver, By.CssSelector(titleHeader_txt_locator));

                            //----- Reference Designs Redesign_Test Plan > Desktop - RD_New Component Tab > R6 > T: Verify the short description -----//

                            //--- Expected Result: Short description displayed ---//
                            test.validateElementIsPresent(driver, By.CssSelector(shortDesc_txt_locator));
                        }

                        break;
                    }
                }

                //----- Reference Designs Redesign_Test Plan > Desktop - RD_New Component Tab > R6 > T: Verify the FPGA HDL Code -----//

                //--- Expected Result: FPGA HDL CODE is displayed ---//
                test.validateElementIsPresent(driver, Elements.DesignCenter_ReferenceDesigns_FpgaHdlCode_Link);

                //----- Reference Designs Redesign_Test Plan > Desktop - RD_New Component Tab > R6 > T: Verify the Linux Device Drivers -----//

                //--- Expected Result: Linux Device Drivers is displayed ---//
                test.validateElementIsPresent(driver, Elements.DesignCenter_ReferenceDesigns_LinuxDeviceDrivers_Link);

                //----- Reference Designs Redesign_Test Plan > Desktop - RD_New Component Tab > R6 > T: Verify the No-OS Drivers -----//

                //--- Expected Result: No-OS Drivers is displayed ---//
                test.validateElementIsPresent(driver, Elements.DesignCenter_ReferenceDesigns_NoOsDrivers_Link);
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.DesignCenter_ReferenceDesigns_SubSecs);
            }
        }
    }
}