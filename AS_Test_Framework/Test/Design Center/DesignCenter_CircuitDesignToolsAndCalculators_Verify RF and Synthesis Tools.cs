﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;

namespace AS_Test_Framework.DesignCenter
{
    [TestFixture]
    public class DesignCenter_CircuitDesignToolsAndCalculators_RfAndSynthesisTools : BaseSetUp
    {
        public DesignCenter_CircuitDesignToolsAndCalculators_RfAndSynthesisTools() : base() { }

        //--- URLs ---//
        string rfAndSynthesisTools_page_url = "/design-center/design-tools-and-calculators/rf-and-synthesis-tools.html";
        string vrmsDbmDbuDbvCalculators = "/design-center/interactive-design-tools/dbconvert.html";

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that RF & Synthesis Tools is Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that RF & Synthesis Tools is Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that RF & Synthesis Tools is Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that RF & Synthesis Tools is Working as Expected in RU Locale")]
        public void DesignCenter_CircuitDesignToolsAndCalculators_VerifyRfAndSynthesisTools(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + rfAndSynthesisTools_page_url);

            //----- R5 > T6: Validate RF & Synthesis Tools -----//

            //--- Expected Result: The items are listed in the page has description ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_RfAndSynthesisTools_DesignTools_Links);
            test.validateElementIsPresent(driver, Elements.DesignCenter_RfAndSynthesisTools_DesignTools_Desc_Txts);
        }

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that Utilities:VRMS/dBm/dBu/dBV calculators is Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that Utilities:VRMS/dBm/dBu/dBV calculators is Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that Utilities:VRMS/dBm/dBu/dBV calculators is Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that Utilities:VRMS/dBm/dBu/dBV calculators is Working as Expected in RU Locale")]
        public void DesignCenter_CircuitDesignToolsAndCalculators_VerifyVrmsDbmDbuDbvCalculators(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + vrmsDbmDbuDbvCalculators);

            //----- R5 > T6: Validate RF & Synthesis Tools -----//

            //--- Expected Result: MyAnalog Strip is present in the tools page. ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Widget_Btn);
        }
    }
}