﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.DesignCenter
{
    [TestFixture]
    public class DesignCenter_LeftRailNavigation : BaseSetUp
    {
        public DesignCenter_LeftRailNavigation() : base() { }

        //--- URLs ---//
        string symbolsFootprintsAnd3dModels_page_url = "/design-center/packaging-quality-symbols-footprints/symbols-and-footprints.html";
        string circuitDesignToolsAndCalculators_page_url = "/design-tools-and-calculators.html";
        string simulationModels_page_url = "/simulation-models.html";
        string referenceDesigns_page_url = "/reference-designs.html";
        string evaluationHardwareAndSoftware_page_url = "/evaluation-hardware-and-software.html";
        string processorsAndDsp_page_url = "/processors-and-dsp.html";
        string packagingQualitySymbolsAndFootprints_page_url = "/packaging-quality-symbols-footprints.html";

        //--- Labels ---//
        string circuitDesignToolsAndCalculators_txt = "Circuit Design Tools & Calculators";
        string simulationModels_txt = "Simulation Models";
        string evaluationHardwareAndSoftware_txt = "Evaluation Hardware & Software";
        string processorsAndDsp_txt = "Processors & DSP";
        string packagingQualitySymbolsAndFootprints_txt = "Packaging, Quality, Symbols & Footprints";

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Left Rail Navigation is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Left Rail Navigation is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Left Rail Navigation is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Left Rail Navigation is Present and Working as Expected in RU Locale")]
        public void DesignCenter_VerifyLeftRailNavigation(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + symbolsFootprintsAnd3dModels_page_url);

            if (util.CheckElement(driver, Elements.DesignCenter_PackagingQualitySymbolsAndFootprints_LeftRailNav, 2))
            {
                //----- Design Center - Symbols and Footprints TestPlan > R2 > T1: Select all items in the "Left Rail Navigation" section of the page. -----//

                if (util.CheckElement(driver, Elements.DesignCenter_LeftRailNav_CircuitDesignToolsAndCalculators_Link, 2))
                {
                    //--- Action: Click "Circuit Design Tools & Calculators". ---//
                    action.IOpenLinkInNewTab(driver, Elements.DesignCenter_LeftRailNav_CircuitDesignToolsAndCalculators_Link);

                    if (Locale.Equals("en"))
                    {
                        //--- Expected Result: "Circuit Design Tools & Calculators" is highlighted. ---//
                        test.validateStringIsCorrect(driver, Elements.DesignCenter_LeftRailNav_Selected_Sec_Link, circuitDesignToolsAndCalculators_txt);
                    }
                    else
                    {
                        test.validateElementIsPresent(driver, Elements.DesignCenter_LeftRailNav_Selected_Sec_Link);
                    }

                    //--- Expected Result: "Circuit Design Tools & Calculators" page should be opened. ---//
                    test.validateStringInstance(driver, driver.Url, circuitDesignToolsAndCalculators_page_url);

                    driver.Close();
                    driver.SwitchTo().Window(driver.WindowHandles.First());
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.DesignCenter_LeftRailNav_CircuitDesignToolsAndCalculators_Link);
                }

                if (util.CheckElement(driver, Elements.DesignCenter_LeftRailNav_SimulationModels_Link, 2))
                {
                    //--- Action: Click "Simulations and Models". ---//
                    action.IOpenLinkInNewTab(driver, Elements.DesignCenter_LeftRailNav_SimulationModels_Link);

                    if (Locale.Equals("en"))
                    {
                        //--- Expected Result: "Simulations and Models" is highlighted. ---//
                        test.validateStringIsCorrect(driver, Elements.DesignCenter_LeftRailNav_Selected_Sec_Link, simulationModels_txt);
                    }
                    else
                    {
                        test.validateElementIsPresent(driver, Elements.DesignCenter_LeftRailNav_Selected_Sec_Link);
                    }

                    //--- Expected Result: "Simulations and Models" page should be opened. ---//
                    test.validateStringInstance(driver, driver.Url, simulationModels_page_url);

                    driver.Close();
                    driver.SwitchTo().Window(driver.WindowHandles.First());
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.DesignCenter_LeftRailNav_SimulationModels_Link);
                }

                if (util.CheckElement(driver, Elements.DesignCenter_LeftRailNav_ReferenceDesigns_Link, 2))
                {
                    //--- Action: Click "Reference Designs". ---//
                    action.IOpenLinkInNewTab(driver, Elements.DesignCenter_LeftRailNav_ReferenceDesigns_Link);
                    Thread.Sleep(2000);

                    //--- Expected Result: "Reference Designs" page should be opened. ---//
                    test.validateStringInstance(driver, driver.Url, referenceDesigns_page_url);

                    driver.Close();
                    driver.SwitchTo().Window(driver.WindowHandles.First());
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.DesignCenter_LeftRailNav_ReferenceDesigns_Link);
                }

                if (util.CheckElement(driver, Elements.DesignCenter_LeftRailNav_EvaluationHardwareAndSoftware_Link, 2))
                {
                    //--- Action: Click "Evaluation Hardware & Software". ---//
                    action.IOpenLinkInNewTab(driver, Elements.DesignCenter_LeftRailNav_EvaluationHardwareAndSoftware_Link);

                    if (Locale.Equals("en"))
                    {
                        //--- Expected Result: "Evaluation Hardware & Software" is highlighted. ---//
                        test.validateStringIsCorrect(driver, Elements.DesignCenter_LeftRailNav_Selected_Sec_Link, evaluationHardwareAndSoftware_txt);
                    }
                    else
                    {
                        test.validateElementIsPresent(driver, Elements.DesignCenter_LeftRailNav_Selected_Sec_Link);
                    }

                    //--- Expected Result: "Evaluation Hardware & Software" page should be opened. ---//
                    test.validateStringInstance(driver, driver.Url, evaluationHardwareAndSoftware_page_url);

                    driver.Close();
                    driver.SwitchTo().Window(driver.WindowHandles.First());
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.DesignCenter_LeftRailNav_EvaluationHardwareAndSoftware_Link);
                }

                if (util.CheckElement(driver, Elements.DesignCenter_LeftRailNav_ProcessorsAndDsp_Link, 2))
                {
                    //--- Action: Click "Processors and DSP". ---//
                    action.IOpenLinkInNewTab(driver, Elements.DesignCenter_LeftRailNav_ProcessorsAndDsp_Link);

                    if (Locale.Equals("en"))
                    {
                        //--- Expected Result: "Processors and DSP" is highlighted. ---//
                        test.validateStringIsCorrect(driver, Elements.DesignCenter_LeftRailNav_Selected_Sec_Link, processorsAndDsp_txt);
                    }
                    else
                    {
                        test.validateElementIsPresent(driver, Elements.DesignCenter_LeftRailNav_Selected_Sec_Link);
                    }

                    //--- Expected Result: "Processors and DSP" page should be opened. ---//
                    test.validateStringInstance(driver, driver.Url, processorsAndDsp_page_url);

                    //--- Expected Result: page Header should be displayed. ---//

                    driver.Close();
                    driver.SwitchTo().Window(driver.WindowHandles.First());
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.DesignCenter_LeftRailNav_ProcessorsAndDsp_Link);
                }

                if (util.CheckElement(driver, Elements.DesignCenter_LeftRailNav_PackagingQualitySymbolsAndFootprints_Link, 2))
                {
                    //--- Action: Click "Packaging, Quality, Symbols and Footprints". ---//
                    action.IOpenLinkInNewTab(driver, Elements.DesignCenter_LeftRailNav_PackagingQualitySymbolsAndFootprints_Link);

                    if (Locale.Equals("en"))
                    {
                        //--- Expected Result: "Packaging, Quality, Symbols and Footprints" page should be opened. ---//
                        test.validateStringIsCorrect(driver, Elements.DesignCenter_LeftRailNav_Selected_Sec_Link, packagingQualitySymbolsAndFootprints_txt);
                    }
                    else
                    {
                        test.validateElementIsPresent(driver, Elements.DesignCenter_LeftRailNav_Selected_Sec_Link);
                    }

                    //--- Expected Result: "Circuit Design Tools & Calculators" page should be opened. ---//
                    test.validateStringInstance(driver, driver.Url, packagingQualitySymbolsAndFootprints_page_url);

                    //--- Expected Result: page Header should be displayed. ---//
                    test.validateElementIsPresent(driver, Elements.DesignCenter_PackagingQualitySymbolsAndFootprints_Page_Title_Txt);
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.DesignCenter_LeftRailNav_PackagingQualitySymbolsAndFootprints_Link);
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.DesignCenter_PackagingQualitySymbolsAndFootprints_LeftRailNav);
            }
        }
    }
}