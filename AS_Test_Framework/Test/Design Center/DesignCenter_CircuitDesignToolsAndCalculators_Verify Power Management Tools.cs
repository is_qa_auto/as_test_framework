﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;

namespace AS_Test_Framework.DesignCenter
{
    [TestFixture]
    public class DesignCenter_CircuitDesignToolsAndCalculators_PowerManagementTools : BaseSetUp
    {
        public DesignCenter_CircuitDesignToolsAndCalculators_PowerManagementTools() : base() { }

        //--- URLs ---//
        string powerManagementTools_page_url = "/design-center/design-tools-and-calculators/power-management-tools.html";

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that Power Management Tools is Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that Power Management Tools is Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that Power Management Tools is Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that Power Management Tools is Working as Expected in RU Locale")]
        public void DesignCenter_CircuitDesignToolsAndCalculators_VerifyPowerManagementTools(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + powerManagementTools_page_url);

            //----- R5 > T5: Validate Power Management Tools -----//

            //--- Expected Result: The items are listed in the page has description ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_PowerManagementTools_DesignTools_Links);
            test.validateElementIsPresent(driver, Elements.DesignCenter_PowerManagementTools_DesignTools_Desc_Txts);
        }
    }
}