﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.DesignCenter
{
    [TestFixture]
    public class DesignCenter_PackagingQualitySymbolsAndFootprints_PackageResources : BaseSetUp
    {
        public DesignCenter_PackagingQualitySymbolsAndFootprints_PackageResources() : base() { }

        //--- URLs ---//
        string packageResources_page_url = "/design-center/packaging-quality-symbols-footprints/package-resources.html";
        string keyPackageInformation_page_url = "/keypackageinformation.html";
        string materialDeclarations_page_url = "/material-declarations.html";

        //--- Labels ---//
        string packagingQualitySymbolsAndFootprints_txt = "Packaging, Quality, Symbols & Footprints";
        string packageResources_txt = "Package Resources";

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Breadcrumb is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Breadcrumb is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Breadcrumb is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Breadcrumb is Present and Working as Expected in RU Locale")]
        public void DesignCenter_PackagingQualitySymbolsAndFootprints_PackageResources_VerifyBreadcrumb(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + packageResources_page_url);

            //----- Design Center - Packaging TestPlan > R4 > T1: Verify "Package Resources" page. -----//

            //--- Expected Result: The Breadcrumbs (History of page(s) visited should be displayed in the page. ---//
            test.validateElementIsPresent(driver, Elements.Breadcrumb_Details);

            if (util.CheckElement(driver, Elements.Breadcrumb_Details, 2))
            {
                //----- Design Center - Packaging TestPlan > R4 > T2: Verify "Breadcrumbs". -----//

                //--- Expected Result: The current Page in The Breadcrumbs hierarchy should have The same info as with The Page header. ---//
                test.validateStringIsCorrect(driver, Elements.Breadcrumb_CurrentPage_Txt, util.GetText(driver, Elements.DesignCenter_PackageResources_Page_Title_Txt));
            }

            //----- Design Center - Packaging TestPlan > R4 > T4_1: Verify Widget. -----//

            //--- Expected Result: The Print Icon + Print link should be displayed. ---//
            test.validateElementIsPresent(driver, Elements.Print_Widget_Btn);

            //--- Expected Result: The My Analog Icon + My Analog link. should be displayed. ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Widget_Btn);
        }

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Left Rail Navigation is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Left Rail Navigation is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Left Rail Navigation is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Left Rail Navigation is Present and Working as Expected in RU Locale")]
        public void DesignCenter_PackagingQualitySymbolsAndFootprints_PackageResources_VerifyLeftRailNavigation(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + packageResources_page_url);

            //----- Design Center - Packaging TestPlan > R4 > T1: Verify "Package Resources" page. -----//

            //--- Expected Result: The Left Rail Navigation should be displayed in the page. ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_PackagingQualitySymbolsAndFootprints_LeftRailNav);

            if (util.CheckElement(driver, Elements.DesignCenter_PackagingQualitySymbolsAndFootprints_LeftRailNav, 2))
            {
                //----- Design Center - Packaging TestPlan > R4 > T5: Verify "Left Rail Navigation". -----//
                if (Locale.Equals("en"))
                {
                    //--- Expected Result: "Packaging, Quality, Symbols and Footprint" at the left rail should be highlighted. ---//
                    test.validateStringIsCorrect(driver, Elements.DesignCenter_PackagingQualitySymbolsAndFootprints_LeftRailNav_Selected_Sec_Link, packagingQualitySymbolsAndFootprints_txt);
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.DesignCenter_PackagingQualitySymbolsAndFootprints_LeftRailNav_Selected_Sec_Link);
                }

                //----- Design Center - Packaging TestPlan > R4 > T3: Verify "Left Rail Navigation". -----//
                if (Locale.Equals("en"))
                {
                    //--- Expected Result: "Package Resources" at the left rail should be highlighted. ---//
                    test.validateStringIsCorrect(driver, Elements.DesignCenter_PackagingQualitySymbolsAndFootprints_LeftRailNav_Selected_Val_Link, packageResources_txt);
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.DesignCenter_PackagingQualitySymbolsAndFootprints_LeftRailNav_Selected_Val_Link);
                }
            }         
        }

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that Package Resources is Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that Package Resources is Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that Package Resources is Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that Package Resources is Working as Expected in RU Locale")]
        public void DesignCenter_PackagingQualitySymbolsAndFootprints_VerifyPackageResources(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + packageResources_page_url);

            //----- Design Center - Packaging TestPlan > R3 > T1: Verify Page Title -----//

            //--- Expected Result: Page title should be displayed. ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_PackageResources_Page_Title_Txt);

            //----- Design Center - Packaging TestPlan > R3 > T2: Verify Description / Descriptive Summary section. -----//

            //--- Expected Result: Description should be shown ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_PackageResources_Desc_Txt);

            //----- Design Center - Packaging TestPlan > R4 > T6: Verify "Link list". -----//

            //--- Expected Result: The Solder Profile per Jedec Standards should be shown in the page. ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_PackageResources_SolderProfilePerJedecStandards_Link);

            //--- Expected Result: The Key Package Information should be shown in the page. ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_PackageResources_KeyPackageInformation_Link);

            //--- Expected Result: The Full Material Declaration RoHS Search should be shown in the page. ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_PackageResources_FullMaterialDeclarationRohsSearch_Link);

            //--- Expected Result: The IPC Standards should be shown in the page. ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_PackageResources_IpcStandards_Link);

            //----- Design Center - Packaging TestPlan > R3 > T4: Verify the "Application Notes" & "Tutorials" sections -----//

            //--- Expected Result: Title should be displayed ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_PackageResources_SubCat_Header_Txts);

            //--- Expected Result: Resource links or link list should be displayed ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_PackageResources_SubCat_Links);

            if (util.CheckElement(driver, Elements.DesignCenter_PackageResources_KeyPackageInformation_Link, 2) && !Configuration.Browser.Equals("Chrome_HL"))
            {
                //----- Design Center - Packaging TestPlan > R3 > T3: Verify link contents -----//

                //--- Action: Select "Key Package Information" link. ---//
                action.IOpenLinkInNewTab(driver, Elements.DesignCenter_PackageResources_KeyPackageInformation_Link);

                //--- Expected Result: "Key Package Information" page is shown. ---//
                test.validateStringInstance(driver, driver.Url, keyPackageInformation_page_url);

                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles.First());
            }

            if (util.CheckElement(driver, Elements.DesignCenter_PackageResources_FullMaterialDeclarationRohsSearch_Link, 2) && !Configuration.Browser.Equals("Chrome_HL"))
            {
                //----- Design Center - Packaging TestPlan > R3 > T3: Verify link contents -----//

                //--- Action: Select "Material Declarations" link. ---//
                action.IOpenLinkInNewTab(driver, Elements.DesignCenter_PackageResources_FullMaterialDeclarationRohsSearch_Link);

                //--- Expected Result: "Material Declarations" page is shown. ---//
                test.validateStringInstance(driver, driver.Url, materialDeclarations_page_url);
            }
        }
    }
}