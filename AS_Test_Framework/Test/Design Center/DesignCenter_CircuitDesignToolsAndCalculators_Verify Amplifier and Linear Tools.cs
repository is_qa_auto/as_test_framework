﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;

namespace AS_Test_Framework.DesignCenter
{
    [TestFixture]
    public class DesignCenter_CircuitDesignToolsAndCalculators_AmplifierAndLinearTools : BaseSetUp
    {
        public DesignCenter_CircuitDesignToolsAndCalculators_AmplifierAndLinearTools() : base() { }

        //--- URLs ---//
        string amplifierAndLinearTools_page_url = "/design-center/design-tools-and-calculators/amplifier-and-linear-tools.html";

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that Amplifier and Linear Tools is Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that Amplifier and Linear Tools is Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that Amplifier and Linear Tools is Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that Amplifier and Linear Tools is Working as Expected in RU Locale")]
        public void DesignCenter_DesignCenter_CircuitDesignToolsAndCalculators_VerifyAmplifierAndLinearTools(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + amplifierAndLinearTools_page_url);

            //----- R1 > T2: Validate Amplifier and Linear Tools -----//

            //--- Expected Result: The items are listed in the page has description ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_AmplifierAndLinearTools_DesignTools_Links);
            test.validateElementIsPresent(driver, Elements.DesignCenter_AmplifierAndLinearTools_DesignTools_Desc_Txts);

            if (util.CheckElement(driver, Elements.MyAnalog_Widget_Btn, 2))
            {
                //--- Action: Click myAnalog on the breadcrumb. ---//
                //action.IClick(driver, Elements.MyAnalog_Widget_Btn);
                action.ILogInViaMyAnalogWidget(driver, "aries.sorosoro@analog.com", "Test_1234");
                //--- Expected Result: It should be display the login segment. (AL-9336) ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_SaveToMyAnalog_Widget);
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.MyAnalog_Widget_Btn);
            }
        }
    }
}