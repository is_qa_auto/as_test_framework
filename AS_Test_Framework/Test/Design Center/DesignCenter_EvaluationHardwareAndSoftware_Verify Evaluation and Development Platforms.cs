﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;

namespace AS_Test_Framework.DesignCenter
{
    [TestFixture]
    public class DesignCenter_EvaluationHardwareAndSoftware_EvaluationAndDevelopmentPlatforms : BaseSetUp
    {
        public DesignCenter_EvaluationHardwareAndSoftware_EvaluationAndDevelopmentPlatforms() : base() { }

        //--- URLs ---//
        string evaluationAndDevelopmentPlatforms_page_url = "/design-center/evaluation-hardware-and-software/evaluation-development-platforms.html";

        //--- Labels ---//
        string ced_txt = "Converter Evaluation and Development Board (CED)";
        string dpg_txt = "Data Pattern Generator (DPG) High-Speed DAC Evaluation Platform";
        string sdp_txt = "System Demonstration Platform (SDP)";
        string mems_txt = "Inertial MEMS Sensor Evaluation Tools";

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that Evaluation and Development Platforms is Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that Evaluation and Development Platforms is Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that Evaluation and Development Platforms is Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that Evaluation and Development Platforms is Working as Expected in RU Locale")]
        public void DesignCenter_EvaluationHardwareAndSoftware_VerifyEvaluationHardwareAndSoftware(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + evaluationAndDevelopmentPlatforms_page_url);

            //----- R1 > T5_1: Verify Product Evaluation Platforms landing page -----//

            //--- Expected Result: Product evaluation platforms page title should be shown properly ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_EvaluationAndDevelopmentPlatforms_Page_Title_Txt);

            //--- Expected Result: Product evaluation platforms description should be shown properly ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_EvaluationAndDevelopmentPlatforms_Desc_Txt);

            //--- Expected Result: The content should be shown on  Product Evaluation Platforms page ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_ProcessorsAndDsp_SubSecs);

            if (util.CheckElement(driver, Elements.DesignCenter_ProcessorsAndDsp_SubSecs, 2) && Locale.Equals("en"))
            {
                int eadp_subsec_count = util.GetCount(driver, Elements.DesignCenter_ProcessorsAndDsp_SubSecs);

                for (int eadp_ctr = 2; eadp_ctr <= eadp_subsec_count; eadp_ctr++)
                {
                    if (util.GetText(driver, By.CssSelector("div[name$='content_section']>div:nth-of-type(" + eadp_ctr + ") * h3[class*='header']")).Equals(ced_txt) || util.GetText(driver, By.CssSelector("div[name$='content_section']>div:nth-of-type(" + eadp_ctr + ") * h3[class*='header']")).Equals(dpg_txt) || util.GetText(driver, By.CssSelector("div[name$='content_section']>div:nth-of-type(" + eadp_ctr + ") * h3[class*='header']")).Equals(sdp_txt) || util.GetText(driver, By.CssSelector("div[name$='content_section']>div:nth-of-type(" + eadp_ctr + ") * h3[class*='header']")).Equals(mems_txt))
                    {
                        //--- Expected Result: Title should be show on  Converter Evaluation and Development Board (CED) ---//
                        //--- Expected Result: Title should be show on  Data Pattern Generator (DPG) High-Speed DAC Evaluation Platform ---//
                        //--- Expected Result: Title should be show on  System Demonstration Platform (SDP) ---//
                        //--- Expected Result: Title should be show onInertial MEMS Sensor Evaluation Tools ---//
                        test.validateElementIsPresent(driver, By.CssSelector("div[name$='content_section']>div:nth-of-type(" + eadp_ctr + ") * h3[class*='header']"));

                        //--- Expected Result: Partial description should be show on  Converter Evaluation and Development Board (CED) ---//
                        //--- Expected Result: Partial description should be show on  Data Pattern Generator (DPG) High-Speed DAC Evaluation Platform ---//
                        //--- Expected Result: Partial description should be show on  System Demonstration Platform (SDP) ---//
                        //--- Expected Result: Partial description hould be show onInertial MEMS Sensor Evaluation Tools ---//
                        test.validateElementIsPresent(driver, By.CssSelector("div[name$='content_section']>div:nth-of-type(" + eadp_ctr + ") * p"));
                    }
                }
            }

            //--- Expected Result: View more link should be show on  Converter Evaluation and Development Board (CED) ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_EvaluationAndDevelopmentPlatforms_Ced_Sec_ViewMore_Link);

            //--- Expected Result: View more link should be show on  Data Pattern Generator (DPG) High-Speed DAC Evaluation Platform ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_EvaluationAndDevelopmentPlatforms_Dpg_Sec_ViewMore_Link);

            //--- Expected Result: View more link should be show on  System Demonstration Platform (SDP) ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_EvaluationAndDevelopmentPlatforms_Sdp_Sec_ViewMore_Link);

            if (!Locale.Equals("ru"))
            {
                //--- Expected Result: View more link should be show onInertial MEMS Sensor Evaluation Tools ---//
                test.validateElementIsPresent(driver, Elements.DesignCenter_EvaluationAndDevelopmentPlatforms_Mems_Sec_ViewMore_Link);
            }
        }  
    }
}