﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Threading;

namespace AS_Test_Framework.DesignCenter
{
    [TestFixture]
    public class DesignCenter_ProcessorsAndDsp_TechnicalLibrary : BaseSetUp
    {
        public DesignCenter_ProcessorsAndDsp_TechnicalLibrary() : base() { }

        //--- URLs ---//
        string technicalLibrary_page_url = "/design-center/processors-and-dsp/processors-dsp-technical-library.html";
        string home_page_url = "/index.html";

        //--- Labels ---//
        string ghost_text = "Keyword";

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Breadcrumb is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Breadcrumb is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Breadcrumb is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Breadcrumb is Present and Working as Expected in RU Locale")]
        public void DesignCenter_ProcessorsAndDsp_TechnicalLibrary_VerifyBreadcrumb(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + technicalLibrary_page_url);

            //----- Design Center - Processors and DSP TestPlan > R4 > T1: Verify the Hardware section -----//

            //--- Expected Result: The Technical Library subcategory page should contain the Print widget ---//
            test.validateElementIsPresent(driver, Elements.Print_Widget_Btn);

            //--- Expected Result: The Technical Library subcategory page should contain the My Analog widget ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Widget_Btn);

            //--- Expected Result: The Technical Library subcategory page should contain the Breadcrumbs ---//
            test.validateElementIsPresent(driver, Elements.Breadcrumb_Details);

            if (util.CheckElement(driver, Elements.Breadcrumb_Details, 2))
            {
                //----- Design Center - Processors and DSP TestPlan > R4 > T2: Verify the breadcrumb -----//

                //--- Expected Result: The breadcrumb of the current  page should is just a label name of the page ---//
                test.validateStringIsCorrect(driver, Elements.Breadcrumb_CurrentPage_Txt, util.GetText(driver, Elements.DesignCenter_TechnicalLibrary_Page_Title_Txt));

                if (util.CheckElement(driver, Elements.BreadCrumb_Home_Icon, 2))
                {
                    //--- Action: Click on the ADI Home Icon ---//
                    action.IOpenLinkInNewTab(driver, Elements.BreadCrumb_Home_Icon);
                    Thread.Sleep(2000);

                    //--- Expected Result: Home Page should be displayed ---//
                    test.validateStringInstance(driver, driver.Url, home_page_url);
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.BreadCrumb_Home_Icon);
                }
            }
        }

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that Technical Library is Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that Technical Library is Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that Technical Library is Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that Technical Library is Working as Expected in RU Locale")]
        public void DesignCenter_ProcessorsAndDsp_VerifyTechnicalLibrary(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + technicalLibrary_page_url);

            //----- Design Center - Processors and DSP TestPlan > R4 > T1: Verify the Hardware section -----//

            //--- Expected Result: The Technical Library subcategory page should contain the subcategory name ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_TechnicalLibrary_Links);

            //----- DOCUMENTATION SEARCH -----//

            //----- Design Center - Processors and DSP TestPlan > R4 > T1: Verify the Hardware section -----//

            //--- Expected Result: The Technical Library subcategory page should contain the Search section ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_TechnicalLibrary_DocumentationSearch_Sec);

            if (util.CheckElement(driver, Elements.DesignCenter_TechnicalLibrary_DocumentationSearch_Sec, 2))
            {
                //----- Design Center - Processors and DSP TestPlan > R4 > T5_1: Verify the Search Input field with invalid keyword -----//

                //--- Expected Result: Ghost text should be displayed as "Keyword" ---//
                if (Locale.Equals("en"))
                {
                    test.validateString(driver, ghost_text, util.ReturnAttribute(driver, Elements.DesignCenter_TechnicalLibrary_DocumentationSearch_Txtbox, "placeholder"));
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.DesignCenter_TechnicalLibrary_DocumentationSearch_Txtbox);
                }

                if (util.CheckElement(driver, Elements.DesignCenter_TechnicalLibrary_DocumentationSearch_Txtbox, 2) && util.CheckElement(driver, Elements.DesignCenter_TechnicalLibrary_DocumentationSearch_Btn, 2))
                {
                    //----- Design Center - Processors and DSP TestPlan > R4 > T5_2: Verify the Search Input field with a valid keyword -----//

                    //--- Action: Enter a complete reference circuit number ---//
                    string documentationSearch_input = "ADSP";

                    action.IDeleteValueOnFields(driver, Elements.DesignCenter_TechnicalLibrary_DocumentationSearch_Txtbox);
                    action.IType(driver, Elements.DesignCenter_TechnicalLibrary_DocumentationSearch_Txtbox, documentationSearch_input);

                    //--- Expected Result: Autosuggest is displayed (IQ-8915/AL-15218) ---//
                    test.validateElementIsPresent(driver, Elements.DesignCenter_TechnicalLibrary_DocumentationSearch_AutoSuggest);

                    action.IDeleteValueOnFields(driver, Elements.DesignCenter_TechnicalLibrary_DocumentationSearch_Txtbox);
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.DesignCenter_TechnicalLibrary_DocumentationSearch_Txtbox);
                    test.validateElementIsPresent(driver, Elements.DesignCenter_TechnicalLibrary_DocumentationSearch_Btn);
                }

                //----- Design Center - Processors and DSP TestPlan > R4 > T6: Verify the Search Filters (AL-11114) -----//

                //--- Expected Result: The Product Categories dropdowns should be displayed on the Browse by Specific Category ---//
                test.validateElementIsPresent(driver, Elements.DesignCenter_TechnicalLibrary_DocumentationSearch_ProductCategories_Dd);

                if (util.CheckElement(driver, Elements.DesignCenter_TechnicalLibrary_DocumentationSearch_ProductCategories_Dd, 2))
                {
                    //--- Action: Click on the Product Categories dropdown ---//
                    action.IClick(driver, Elements.DesignCenter_TechnicalLibrary_DocumentationSearch_ProductCategories_Dd_Btn);

                    //--- Expected Result: The list of Product Categories should be displayed on the dropdown ---//
                    test.validateElementIsPresent(driver, Elements.DesignCenter_TechnicalLibrary_DocumentationSearch_ProductCategories_Dd_Menu);
                }
            }
        }
    }
}