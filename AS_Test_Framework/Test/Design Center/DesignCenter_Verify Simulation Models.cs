﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.DesignCenter
{
    [TestFixture]
    public class DesignCenter_SimulationModels : BaseSetUp
    {
        public DesignCenter_SimulationModels() : base() { }

        //--- URLs ---//
        string simulationModels_page_url = "/design-center/simulation-models.html";
        string simulationModels_url = "simulation-models";
        string bsdlModels_page_url = "/bsdl-model-files.html";
        string ibisModels_page_url = "/ibis-models.html";
        string sParameters_page_url = "/s-parameters.html";
        string spiceModels_page_url = "/spice-models.html";
        string sysParameterModelsForKeysightGenesys_page_url = "/sys-parameter-models-keysight-genesys.html";

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Breadcrumb is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Breadcrumb is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Breadcrumb is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Breadcrumb is Present and Working as Expected in RU Locale")]
        public void DesignCenter_SimulationModels_VerifyBreadcrumb(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + simulationModels_page_url);

            //----- Design Center - Simulations and Models TestPlan > R1 > T2_2: Verify Print link -----//

            //--- Expected Result: The Print link is displayed ---//
            test.validateElementIsPresent(driver, Elements.Print_Widget_Btn);
        }

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that Simulation Models is Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that Simulation Models is Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that Simulation Models is Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that Simulation Models is Working as Expected in RU Locale")]
        public void DesignCenter_VerifySimulationModels(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + simulationModels_page_url);

            //----- Design Center - Symbols and Footprints TestPlan > R2 > T1: Select all items in the "Left Rail Navigation" section of the page. -----//

            //--- Expected Result: page Header should be displayed. ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_SimulationModels_Page_Title_Txt);

            //----- Design Center - Simulations and Models TestPlan > R1 > T5: Verify the sub-category section -----//

            //--- Expected Result: The sub category  section should contain the sub category name ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_SimulationModels_SubCat_Name_Txts);

            //--- Expected Result: The sub category  section should contain the descriptiion ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_SimulationModels_SubCat_Desc_Txts);

            //--- Expected Result: The sub category  section should contain the View More link ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_SimulationModels_SubCat_ViewMore_Links);

            if (util.CheckElement(driver, Elements.DesignCenter_SimulationModels_SubCat_ViewMore_Links, 2))
            {
                //--- Action: Click on the View More link under the subcategory ---//
                action.IOpenLinkInNewTab(driver, Elements.DesignCenter_SimulationModels_SubCat_ViewMore_Links);
                Thread.Sleep(1000);

                //--- Expected Result: The category detail page page should be displayed ---//
                test.validateStringInstance(driver, driver.Url, simulationModels_url);
            }
        }

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Left Rail Navigation is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Left Rail Navigation is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Left Rail Navigation is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Left Rail Navigation is Present and Working as Expected in RU Locale")]
        public void DesignCenter_SimulationModels_VerifyLeftRailNavigation(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + simulationModels_page_url);

            //----- Design Center - Simulations and Models TestPlan > R1 > T3: Verify the Left Rail navigation (AL-10408) -----//

            //--- Expected Result: Simulation and Models  from the left rail should be highlighted ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_SimulationModels_LeftRailNav_Sec);

            if (util.CheckElement(driver, Elements.DesignCenter_SimulationModels_LeftRailNav_Sec, 2))
            {
                //--- Expected Result: Evaluation board landing page should contain the Left rail navigation  Simulation Models  sub- section ---//
                test.validateElementIsPresent(driver, Elements.DesignCenter_SimulationModels_LeftRailNav_Subsec_Links);

                if (util.CheckElement(driver, Elements.DesignCenter_SimulationModels_LeftRailNav_Subsec_Links, 2))
                {
                    //--- Expected Result: The BSDL Model Files sub category should be displayed ---//
                    test.validateElementIsPresent(driver, Elements.DesignCenter_SimulationModels_LeftRailNav_BsdlModels_Link);

                    if (util.CheckElement(driver, Elements.DesignCenter_SimulationModels_LeftRailNav_BsdlModels_Link, 2))
                    {
                        //--- Action: Click on the sub category link ---//
                        action.IOpenLinkInNewTab(driver, Elements.DesignCenter_SimulationModels_LeftRailNav_BsdlModels_Link);
                        Thread.Sleep(4000);

                        //--- Expected Result: Each sub category in the left rail should be directed to separate landing page ---//
                        test.validateStringInstance(driver, driver.Url, bsdlModels_page_url);

                        driver.Close();
                        driver.SwitchTo().Window(driver.WindowHandles.First());
                    }

                    //--- Expected Result: The IBIS Models sub category should be displayed ---//
                    test.validateElementIsPresent(driver, Elements.DesignCenter_SimulationModels_LeftRailNav_IbisModels_Link);

                    if (util.CheckElement(driver, Elements.DesignCenter_SimulationModels_LeftRailNav_IbisModels_Link, 2))
                    {
                        //--- Action: Click on the sub category link ---//
                        action.IOpenLinkInNewTab(driver, Elements.DesignCenter_SimulationModels_LeftRailNav_IbisModels_Link);
                        Thread.Sleep(1000);

                        //--- Expected Result: Each sub category in the left rail should be directed to separate landing page ---//
                        test.validateStringInstance(driver, driver.Url, ibisModels_page_url);

                        driver.Close();
                        driver.SwitchTo().Window(driver.WindowHandles.First());
                    }

                    //--- Expected Result: The S-Parameters sub category should be displayed ---//
                    test.validateElementIsPresent(driver, Elements.DesignCenter_SimulationModels_LeftRailNav_SParameters_Link);

                    if (util.CheckElement(driver, Elements.DesignCenter_SimulationModels_LeftRailNav_SParameters_Link, 2))
                    {
                        //--- Action: Click on the sub category link ---//
                        action.IOpenLinkInNewTab(driver, Elements.DesignCenter_SimulationModels_LeftRailNav_SParameters_Link);
                        Thread.Sleep(1000);

                        //--- Expected Result: Each sub category in the left rail should be directed to separate landing page ---//
                        test.validateStringInstance(driver, driver.Url, sParameters_page_url);

                        driver.Close();
                        driver.SwitchTo().Window(driver.WindowHandles.First());
                    }

                    //--- Expected Result: The SPICE Models sub category should be displayed ---//
                    test.validateElementIsPresent(driver, Elements.DesignCenter_SimulationModels_LeftRailNav_SpiceModels_Link);

                    if (util.CheckElement(driver, Elements.DesignCenter_SimulationModels_LeftRailNav_SpiceModels_Link, 2))
                    {
                        //--- Action: Click on the sub category link ---//
                        action.IOpenLinkInNewTab(driver, Elements.DesignCenter_SimulationModels_LeftRailNav_SpiceModels_Link);
                        Thread.Sleep(1000);

                        //--- Expected Result: Each sub category in the left rail should be directed to separate landing page ---//
                        test.validateStringInstance(driver, driver.Url, spiceModels_page_url);

                        driver.Close();
                        driver.SwitchTo().Window(driver.WindowHandles.First());
                    }

                    //--- Expected Result: The Sys-Parameter Models for Keysight Genesys sub category should be displayed ---//
                    test.validateElementIsPresent(driver, Elements.DesignCenter_SimulationModels_LeftRailNav_SysParameterModelsForKeysightGenesys_Link);

                    if (util.CheckElement(driver, Elements.DesignCenter_SimulationModels_LeftRailNav_SysParameterModelsForKeysightGenesys_Link, 2))
                    {
                        //--- Action: Click on the sub category link ---//
                        action.IOpenLinkInNewTab(driver, Elements.DesignCenter_SimulationModels_LeftRailNav_SysParameterModelsForKeysightGenesys_Link);
                        Thread.Sleep(2000);

                        //--- Expected Result: Each sub category in the left rail should be directed to separate landing page ---//
                        test.validateStringInstance(driver, driver.Url, sysParameterModelsForKeysightGenesys_page_url);
                    }
                }
            }
        }
    }
}