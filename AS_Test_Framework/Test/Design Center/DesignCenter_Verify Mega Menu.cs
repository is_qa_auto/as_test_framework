﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.DesignCenter
{
    [TestFixture]
    public class DesignCenter_MegaMenu : BaseSetUp
    {
        public DesignCenter_MegaMenu() : base() { }

        //--- URLs ---//
        string home_page_url = "/index.html";
        string circuitsFromTheLab_page_url = "/design-center/reference-designs/circuits-from-the-lab.html";
        string referenceDesigns_page_url = "/design-center/reference-designs.html";
        string driversAndReferenceCode_page_url = "/drivers-reference-code.html";
        string circuitDesignToolsAndCalculators_page_url = "/design-tools-and-calculators.html";
        string circuitDesignToolsAndCalculators_subcategory_page_url = "/design-tools-and-calculators/";
        string evaluationHardwareAndSoftware_page_url = "/evaluation-hardware-and-software.html";
        string evaluationHardwareAndSoftware_subcategory_page_url = "/evaluation-hardware-and-software/";
        string simulationModels_page_url = "/simulation-models.html";
        string simulationModels_subcategory_page_url = "/simulation-models/";
        string packagingQualitySymbolsAndFootprints_page_url = "/packaging-quality-symbols-footprints.html";
        string packagingQualitySymbolsAndFootprints_subcategory_page_url = "/packaging-quality-symbols-footprints/";
        string processorsAndDsp_page_url = "/processors-and-dsp.html";
        string processorsAndDsp_subcategory_page_url = "/processors-and-dsp/";

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Design Center Mega Menu is Present and Working as Expected in the Home Page as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Design Center Mega Menu is Present and Working as Expected in the Home Page as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Design Center Mega Menu is Present and Working as Expected in the Home Page as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Design Center Mega Menu is Present and Working as Expected in the Home Page as Expected in RU Locale")]
        public void DesignCenter_MegaMenu_VerifyDesignCenterMegaMenuInHomePage(string Locale)
        {
            //--- Action: Go to Analog Homepage ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + home_page_url);
            Thread.Sleep(3000);

            //----- Reference Designs Redesign_Test Plan > Desktop-Mega Menu_Design Center Tab > R1 > T1: Verify Mega Menu Redesign - Design Center (Navigation & Layout) (IQ-9337/AL-16398) -----//

            //--- Action: Hover over the Design Center tab ---//
            action.IMouseOverTo(driver, Elements.MainNavigation_DesignCenterTab);
            Thread.Sleep(2000);

            //--- Expected Result: The Design Center tab is in hover state (Font color is White - #ffffff) ---//
            test.validateColorIsCorrect(driver, util.GetCssValue(driver, Elements.MainNavigation_DesignCenterTab, "text-decoration-color"), "#FFFFFF");

            //--- Expected Result: The Design Center tab is in hover state (Tab color is Vivid orange - #ff7201) ---//
            test.validateColorIsCorrect(driver, util.GetCssValue(driver, Elements.MainNavigation_DesignCenterTab, "background-color"), "#FF7201");

            //--- Action: Click the Design Center tab ---//
            if (!util.CheckElement(driver, Elements.MainNavigation_DesignCenterTab_Expanded, 2))
            {
                action.IClick(driver, Elements.MainNavigation_DesignCenterTab);
            }

            //----- Reference Designs Redesign_Test Plan > Desktop - RD_3rd Party Tab > R1 > T1: Verify 3rd Party Reference Design -----//

            //--- Expected Result: Design Center tab is expanded ---//
            test.validateElementIsPresent(driver, Elements.MainNavigation_DesignCenterTab_Expanded);

            if (util.CheckElement(driver, Elements.MainNavigation_DesignCenterTab_Expanded, 2))
            {
                //----- Reference Designs Redesign_Test Plan > Desktop-Mega Menu_Design Center Tab > R1 > T1: Verify Mega Menu Redesign - Design Center (Navigation & Layout) (IQ-9337/AL-16398) -----//

                //--- Expected Result: The Design Center tab is in active state (Font color is White - #ffffff) ---//
                test.validateColorIsCorrect(driver, util.GetCssValue(driver, Elements.MainNavigation_DesignCenterTab, "text-decoration-color"), "#FFFFFF");

                //--- Expected Result: The Design Center tab is in active state (Tab color is Vivid orange - #ff7201) ---//
                test.validateColorIsCorrect(driver, util.GetCssValue(driver, Elements.MainNavigation_DesignCenterTab, "background-color"), "#FF7201");

                //--- Expected Result: Each section has a hyperlinked header ---//
                test.validateElementIsPresent(driver, Elements.MainNavigation_DesignCenterTab_Category_Links);

                if (util.CheckElement(driver, Elements.MainNavigation_DesignCenterTab_Category_Links, 2))
                {
                    //----- Reference Designs Redesign_Test Plan > Desktop-Mega Menu_Design Center Tab > R1 > T2: Verify Mega Menu Redesign - Design Center (Reference Designs section) (IQ-9337/AL-16398) -----//

                    //--- Action: Hover over the Reference Designs label ---//
                    action.IMouseOverTo(driver, Elements.MainNavigation_DesignCenterTab_Category_Links);

                    //--- Expected Result: The section (vertical line), Reference Designs label and label underline is in hover state (Font color is Vivid orange - #ff7200) ---//
                    test.validateColorIsCorrect(driver, util.GetCssValue(driver, Elements.MainNavigation_DesignCenterTab_Category_Links, "text-decoration-color"), "#FF7200");
                }

                //----- Reference Designs Redesign_Test Plan > Desktop-Mega Menu_Design Center Tab > R1 > T1: Verify Mega Menu Redesign - Design Center (Navigation & Layout) (IQ-9337/AL-16398) -----//

                //--- Expected Result: Each section has a hyperlinked sublinks ---//
                test.validateElementIsPresent(driver, Elements.MainNavigation_DesignCenterTab_SubCategory_Links);

                //--- Expected Result: There are two (2) featured sections in the top "half" of the panel ---//
                test.validateCountIsEqual(driver, 2, util.GetCount(driver, Elements.MainNavigation_DesignCenterTab_Featured_Secs));

                //----- Reference Designs Redesign_Test Plan > Desktop-Mega Menu_Design Center Tab > R1 > T1: Verify Mega Menu Redesign - Design Center (Navigation & Layout) (IQ-9337/AL-16398) -----//

                //--- Action: Click the Design Center tab again ---//
                if (util.CheckElement(driver, Elements.MainNavigation_DesignCenterTab_Expanded, 2))
                {
                    action.IClick(driver, Elements.MainNavigation_DesignCenterTab);
                }

                //--- Expected Result: The Design Center tab's active state is disabled ---//
                test.validateElementIsNotPresent(driver, Elements.MainNavigation_DesignCenterTab_Expanded);
            }
        }

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Design Center Mega Menu is Present and Working as Expected in a Design Center - Category Page as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Design Center Mega Menu is Present and Working as Expected in a Design Center - Category Page as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Design Center Mega Menu is Present and Working as Expected in a Design Center - Category Page as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Design Center Mega Menu is Present and Working as Expected in a Design Center - Category Page as Expected in RU Locale")]
        public void DesignCenter_MegaMenu_VerifyDesignCenterMegaMenuInADesignCenterCategoryPage(string Locale)
        {
            //----- Reference Designs Redesign_Test Plan > Desktop-Mega Menu_Design Center Tab > R1 > T2: Verify Mega Menu Redesign - Design Center (Reference Designs section) (IQ-9337/AL-16398) -----/

            //--- Action: Go to a Design Center - Category Page ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + referenceDesigns_page_url);

            //--- Expected Result: The Design Center tab remains in active state (Font color is White - #ffffff) ---//
            test.validateColorIsCorrect(driver, util.GetCssValue(driver, Elements.MainNavigation_DesignCenterTab, "text-decoration-color"), "#FFFFFF");

            //--- Expected Result: The Design Center tab remains in active state (Tab color is Vivid orange - #ff7200) ---//
            test.validateColorIsCorrect(driver, util.GetCssValue(driver, Elements.MainNavigation_DesignCenterTab, "background-color"), "#FF7200");

            //--- Action: Click the Education tab ---//
            if (!util.CheckElement(driver, Elements.MainNavigation_EducationTab_Expanded, 2))
            {
                action.IClick(driver, Elements.MainNavigation_EducationTab);
            }

            //--- Expected Result: The Design Center tab remains in active state (Font color is White - #ffffff) ---//
            test.validateColorIsCorrect(driver, util.GetCssValue(driver, Elements.MainNavigation_DesignCenterTab, "text-decoration-color"), "#FFFFFF");

            //--- Expected Result: The Design Center tab remains in active state (Tab color is Vivid orange - #ff7200) ---//
            test.validateColorIsCorrect(driver, util.GetCssValue(driver, Elements.MainNavigation_DesignCenterTab, "background-color"), "#FF7200");

            //--- Action: Click the Design Center tab ---//
            if (!util.CheckElement(driver, Elements.MainNavigation_DesignCenterTab_Expanded, 2))
            {
                action.IClick(driver, Elements.MainNavigation_DesignCenterTab);
            }

            if (util.CheckElement(driver, Elements.MainNavigation_DesignCenterTab_SubCategory_Links, 2))
            {
                //--- Action: Hover over the Drivers & Reference Code label ---//
                action.IMouseOverTo(driver, Elements.MainNavigation_DesignCenterTab_SubCategory_Links);

                //--- Expected Result: The section (vertical line), Drivers & Reference Code label and label underline is in hover state (Font color is Vivid orange - #ff7200) ---//
                test.validateColorIsCorrect(driver, util.GetCssValue(driver, Elements.MainNavigation_DesignCenterTab_SubCategory_Links, "text-decoration-color"), "#FF7200");
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.MainNavigation_DesignCenterTab_SubCategory_Links);
            }
        }

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Design Center Mega Menu is Present and Working as Expected in a Design Center - Subcategory Page as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Design Center Mega Menu is Present and Working as Expected in a Design Center - Subcategory Page as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Design Center Mega Menu is Present and Working as Expected in a Design Center - Subcategory Page as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Design Center Mega Menu is Present and Working as Expected in a Design Center - Subcategory Page as Expected in RU Locale")]
        public void DesignCenter_MegaMenu_VerifyDesignCenterMegaMenuInADesignCenterSubCategoryPage(string Locale)
        {
            //----- Reference Designs Redesign_Test Plan > Desktop-Mega Menu_Design Center Tab > R1 > T2: Verify Mega Menu Redesign - Design Center (Reference Designs section) (IQ-9337/AL-16398) -----/

            //--- Action: Go to a Design Center - Subcategory Page ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + circuitsFromTheLab_page_url);

            //--- Expected Result: The Design Center tab remains in active state (Font color is White - #ffffff) ---//
            test.validateColorIsCorrect(driver, util.GetCssValue(driver, Elements.MainNavigation_DesignCenterTab, "text-decoration-color"), "#FFFFFF");

            //--- Expected Result: The Design Center tab remains in active state (Tab color is Vivid orange - #ff7200) ---//
            test.validateColorIsCorrect(driver, util.GetCssValue(driver, Elements.MainNavigation_DesignCenterTab, "background-color"), "#FF7200");

            //--- Action: Click the Education tab ---//
            if (!util.CheckElement(driver, Elements.MainNavigation_EducationTab_Expanded, 2))
            {
                action.IClick(driver, Elements.MainNavigation_EducationTab);
            }

            //--- Expected Result: The Design Center tab remains in active state (Font color is White - #ffffff) ---//
            test.validateColorIsCorrect(driver, util.GetCssValue(driver, Elements.MainNavigation_DesignCenterTab, "text-decoration-color"), "#FFFFFF");

            //--- Expected Result: The Design Center tab remains in active state (Tab color is Vivid orange - #ff7200) ---//
            test.validateColorIsCorrect(driver, util.GetCssValue(driver, Elements.MainNavigation_DesignCenterTab, "background-color"), "#FF7200");

            //--- Action: Click the Design Center tab ---//
            if (!util.CheckElement(driver, Elements.MainNavigation_DesignCenterTab_Expanded, 2))
            {
                action.IClick(driver, Elements.MainNavigation_DesignCenterTab);
            }

            if (util.CheckElement(driver, Elements.MainNavigation_DesignCenterTab_SubCategory_Links, 2))
            {
                //--- Action: Hover over the FPGA and Processors Compatible Reference Designs label ---//
                action.IMouseOverTo(driver, Elements.MainNavigation_DesignCenterTab_SubCategory_Links);

                //--- Expected Result: The section (vertical line), FPGA and Processors Compatible Reference Designs label and label underline is in hover state (Font color is Vivid orange - #ff7200) ---//
                test.validateColorIsCorrect(driver, util.GetCssValue(driver, Elements.MainNavigation_DesignCenterTab_SubCategory_Links, "text-decoration-color"), "#FF7200");
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.MainNavigation_DesignCenterTab_SubCategory_Links);
            }
        }

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Links are Working as Expected as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Links are Working as Expected as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Links are Working as Expected as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Links are Working as Expected as Expected in RU Locale")]
        public void DesignCenter_MegaMenu_VerifyDesignCenterCategoryLinks(string Locale)
        {
            //--- Action: Access Homepage---//
            action.Navigate(driver, Configuration.Env_Url + Locale + home_page_url);

            //--- Action: Click on the Design Center tab on the main navigation ---//
            if (!util.CheckElement(driver, Elements.MainNavigation_DesignCenterTab_Expanded, 2))
            {
                action.IClick(driver, Elements.MainNavigation_DesignCenterTab);
            }

            //----- REFERENCE DESIGNS -----//

            if (util.CheckElement(driver, Elements.MainNavigation_DesignCenterTab_ReferenceDesigns_Link, 1))
            {
                //----- Design Center - Landing Page TestPlan > Test Case Title: Verify Design Center Category Links (AL-5612) -----//

                //--- Action: Click on the Reference Designs Category link ---//
                action.IOpenLinkInNewTab(driver, Elements.MainNavigation_DesignCenterTab_ReferenceDesigns_Link);

                //--- Expected Result: The Reference Designs category Landing page should be displayed ---//
                test.validateStringInstance(driver, driver.Url, referenceDesigns_page_url);

                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles.Last());
            }

            if (util.CheckElement(driver, Elements.MainNavigation_DesignCenterTab_DriversAndReferenceCode_Link, 1))
            {
                //----- Design Center - Landing Page TestPlan > Test Case Title: Verify Reference Designs -----//

                //--- Action: Click on Drivers & Reference Code ---//
                action.IOpenLinkInNewTab(driver, Elements.MainNavigation_DesignCenterTab_DriversAndReferenceCode_Link);

                //--- Expected Result: Drivers & Reference Code landing page should be displayed ---//
                test.validateStringInstance(driver, driver.Url, driversAndReferenceCode_page_url);

                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles.Last());
            }

            //----- CIRCUIT DESIGN TOOLS & CALCULATORS -----//

            if (util.CheckElement(driver, Elements.MainNavigation_DesignCenterTab_CircuitDesignToolsAndCalculators_Link, 1))
            {
                //----- Design Center - Landing Page TestPlan > Test Case Title: Verify Design Center Category Links (AL-5612) -----//

                //--- Action: Click on the Design Tools ang Calculators category link ---//
                action.IOpenLinkInNewTab(driver, Elements.MainNavigation_DesignCenterTab_CircuitDesignToolsAndCalculators_Link);

                //--- Expected Result: The Design Tools and Calculators category Landing page should be displayed ---//
                test.validateStringInstance(driver, driver.Url, circuitDesignToolsAndCalculators_page_url);

                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles.Last());
            }

            if (util.CheckElement(driver, Elements.MainNavigation_DesignCenterTab_CircuitDesignToolsAndCalculators_Subcategory_Links, 1))
            {
                //----- Design Center - Landing Page TestPlan > Test Case Title: Verify Evaluation Hardware & Software -----//

                //--- Action: Click on Circuit Design Tools & Calculators Subcategory Link ---//
                action.IOpenLinkInNewTab(driver, Elements.MainNavigation_DesignCenterTab_CircuitDesignToolsAndCalculators_Subcategory_Links);

                //--- Expected Result: Circuit Design Tools & Calculators Subcategory Page should be displayed ---//
                test.validateStringInstance(driver, driver.Url, circuitDesignToolsAndCalculators_subcategory_page_url);

                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles.Last());
            }

            //----- EVALUATION HARDWARE & SOFTWARE -----//

            if (util.CheckElement(driver, Elements.MainNavigation_DesignCenterTab_EvaluationHardwareAndSoftware_Link, 1))
            {
                //----- Design Center - Landing Page TestPlan > Test Case Title: Verify Design Center Category Links (AL-5612) -----//

                //--- Action: Click on theEvaluation Hardware & Software Category link ---//
                action.IOpenLinkInNewTab(driver, Elements.MainNavigation_DesignCenterTab_EvaluationHardwareAndSoftware_Link);

                //--- Expected Result: The Evaluation Hardware & Software category Landing page should be displayed ---//
                test.validateStringInstance(driver, driver.Url, evaluationHardwareAndSoftware_page_url);

                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles.Last());
            }

            if (util.CheckElement(driver, Elements.MainNavigation_DesignCenterTab_EvaluationHardwareAndSoftware_Subcategory_Links, 1))
            {
                //----- Design Center - Landing Page TestPlan > Test Case Title: Verify the Design Tools Category -----//

                //--- Action: Click on Evaluation Hardware & Software Subcategory Link ---//
                action.IOpenLinkInNewTab(driver, Elements.MainNavigation_DesignCenterTab_EvaluationHardwareAndSoftware_Subcategory_Links);

                //--- Expected Result: Evaluation Hardware & Software Subcategory Page should be displayed ---//
                test.validateStringInstance(driver, driver.Url, evaluationHardwareAndSoftware_subcategory_page_url);

                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles.Last());
            }

            //----- SIMULATION MODELS -----//

            if (util.CheckElement(driver, Elements.MainNavigation_DesignCenterTab_SimulationModels_Link, 1))
            {
                //----- Design Center - Landing Page TestPlan > Test Case Title: Verify Design Center Category Links (AL-5612) -----//

                //--- Action: Click on the Models and Simulations Category link ---//
                action.IOpenLinkInNewTab(driver, Elements.MainNavigation_DesignCenterTab_SimulationModels_Link);

                //--- Expected Result: The Models and Simulations category Landing page should be displayed ---//
                test.validateStringInstance(driver, driver.Url, simulationModels_page_url);

                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles.Last());
            }

            if (util.CheckElement(driver, Elements.MainNavigation_DesignCenterTab_SimulationModels_Subcategory_Links, 1))
            {
                //----- Design Center - Landing Page TestPlan > Test Case Title: Verify Simulation and Models (AL-10408) -----//

                //--- Action: Click on Simulation Models Subcategory Link ---//
                action.IOpenLinkInNewTab(driver, Elements.MainNavigation_DesignCenterTab_SimulationModels_Subcategory_Links);

                //--- Expected Result: Simulation Models Subcategory Page should be displayed ---//
                test.validateStringInstance(driver, driver.Url, simulationModels_subcategory_page_url);

                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles.Last());
            }

            //----- PACKAGING, QUALITY, SYMBOLS & FOOTPRINTS -----//

            if (util.CheckElement(driver, Elements.MainNavigation_DesignCenterTab_PackagingQualitySymbolsAndFootprints_Link, 1))
            {
                //----- Design Center - Landing Page TestPlan > Test Case Title: Verify Design Center Category Links (AL-5612) -----//

                //--- Action: Click on thePackages, Quality, Symbols & Footprints Category link ---//
                action.IOpenLinkInNewTab(driver, Elements.MainNavigation_DesignCenterTab_PackagingQualitySymbolsAndFootprints_Link);

                //--- Expected Result: The Packages, Quality, Symbols & Footprints category Landing page should be displayed ---//
                test.validateStringInstance(driver, driver.Url, packagingQualitySymbolsAndFootprints_page_url);

                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles.Last());
            }

            if (util.CheckElement(driver, Elements.MainNavigation_DesignCenterTab_PackagingQualitySymbolsAndFootprints_Subcategory_Links, 1))
            {
                //----- Design Center - Landing Page TestPlan > Test Case Title: Verify Packages, Quality, Symbols & Footprints -----//

                //--- Action: Click on Packaging, Quality, Symbols & Footprints Subcategory Link ---//
                action.IOpenLinkInNewTab(driver, Elements.MainNavigation_DesignCenterTab_PackagingQualitySymbolsAndFootprints_Subcategory_Links);

                //--- Expected Result: Packaging, Quality, Symbols & Footprints Subcategory Page should be displayed ---//
                test.validateStringInstance(driver, driver.Url, packagingQualitySymbolsAndFootprints_subcategory_page_url);

                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles.Last());
            }

            //----- PROCESSORS & DSP -----//

            if (util.CheckElement(driver, Elements.MainNavigation_DesignCenterTab_ProcessorsAndDsp_Link, 1))
            {
                //----- Design Center - Landing Page TestPlan > Test Case Title: Verify Design Center Category Links (AL-5612) -----//

                //--- Action: Click on the Processors and DSP Category link ---//
                action.IOpenLinkInNewTab(driver, Elements.MainNavigation_DesignCenterTab_ProcessorsAndDsp_Link);

                //--- Expected Result: The Processors and DSP cateogy landing page should be displayed ---//
                test.validateStringInstance(driver, driver.Url, processorsAndDsp_page_url);

                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles.Last());
            }

            if (util.CheckElement(driver, Elements.MainNavigation_DesignCenterTab_ProcessorsAndDsp_Subcategory_Links, 1))
            {
                //----- Design Center - Landing Page TestPlan > Test Case Title: Verify Processors and DSP -----//

                //--- Action: Click on Processors & DSP Subcategory Link ---//
                action.IOpenLinkInNewTab(driver, Elements.MainNavigation_DesignCenterTab_ProcessorsAndDsp_Subcategory_Links);

                //--- Expected Result: Processors & DSP Subcategory Page should be displayed ---//
                test.validateStringInstance(driver, driver.Url, processorsAndDsp_subcategory_page_url);

                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles.Last());
            }
        }
    }
}