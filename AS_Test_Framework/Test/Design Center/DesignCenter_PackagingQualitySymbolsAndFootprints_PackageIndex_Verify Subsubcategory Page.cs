﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.DesignCenter
{
    [TestFixture]
    public class DesignCenter_PackagingQualitySymbolsAndFootprints_PackageIndex_SubsubcategoryPage : BaseSetUp
    {
        public DesignCenter_PackagingQualitySymbolsAndFootprints_PackageIndex_SubsubcategoryPage() : base() { }

        //--- URLs ---//
        string packageIndex_page_url = "/design-center/packaging-quality-symbols-footprints/package-index.html";
        string packageIndex_subSubCategory_page_url = "/design-center/packaging-quality-symbols-footprints/package-index/bga-ball-grid-array/cbga-ceramic.html";
        string packageIndex_subCategory_page_url_format = "/design-center/packaging-quality-symbols-footprints/package-index/";

        //--- Labels ---//
        string package_txt = "Package";
        string outline_txt = "Outline";
        string materialInformation_txt = "Material Information";
        string pdf_txt = "PDF";

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Package Index - Subsubcategory Page is Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Package Index - Subsubcategory Page is Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Package Index - Subsubcategory Page is Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Package Index - Subsubcategory Page is Working as Expected in RU Locale")]
        public void DesignCenter_PackagingQualitySymbolsAndFootprints_PackageIndex_VerifySubsubcategoryPage(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + packageIndex_page_url);

            if (util.CheckElement(driver, Elements.DesignCenter_PackageIndex_SubCat_ViewMore_Links, 2))
            {
                //--- Action: Click all "View More" link under each Sub-Category (Package Type Category). ---//
                action.IOpenLinkInNewTab(driver, Elements.DesignCenter_PackageIndex_SubCat_ViewMore_Links);

                if (util.CheckElement(driver, Elements.DesignCenter_PackageIndex_SubCat_ViewMore_Links, 2))
                {
                    //----- Design Center - Packaging TestPlan > R2 > T2: Verify "View More" links -----//

                    //--- Action: Click "View More" link each package type under the each package type category. ---//
                    action.IOpenLinkInNewTab(driver, Elements.DesignCenter_PackageIndex_SubCat_ViewMore_Links);

                    if (!driver.Url.Contains(packageIndex_subCategory_page_url_format))
                    {
                        driver.Navigate().GoToUrl(Configuration.Env_Url + Locale + packageIndex_subSubCategory_page_url);
                    }

                    //--- Expected Result: The Left rail navigation should be displayed in each page ---//
                    test.validateElementIsPresent(driver, Elements.DesignCenter_PackagingQualitySymbolsAndFootprints_LeftRailNav);

                    //--- Expected Result: The page Header should be displayed in each page ---//
                    test.validateElementIsPresent(driver, Elements.DesignCenter_PackageIndex_Page_Title_Txt);

                    //--- Expected Result: The Descriptive Summary / Description should be displayed in each page ---//
                    test.validateElementIsPresent(driver, Elements.DesignCenter_PackageIndex_Desc_Txt);

                    if (Locale.Equals("en"))
                    {
                        //--- Expected Result: The Packages Table with header column "Package" should be displayed in each page ---//
                        test.validateStringIsCorrect(driver, Elements.DesignCenter_PackageIndex_SubSubCat_Package_Col_Header_Txt, package_txt);

                        //--- Expected Result: The Packages Table with header columns "Outline" should be displayed in each page ---//
                        test.validateStringIsCorrect(driver, Elements.DesignCenter_PackageIndex_SubSubCat_Outline_Col_Header_Txt, outline_txt);

                        //--- Expected Result: The Packages Table with header column "Material Information" should be displayed in each page ---//
                        test.validateStringIsCorrect(driver, Elements.DesignCenter_PackageIndex_SubSubCat_MaterialInfo_Col_Header_Txt, materialInformation_txt);
                    }
                    else
                    {
                        test.validateElementIsPresent(driver, Elements.DesignCenter_PackageIndex_SubSubCat_Package_Col_Header_Txt);
                        test.validateElementIsPresent(driver, Elements.DesignCenter_PackageIndex_SubSubCat_Outline_Col_Header_Txt);
                        test.validateElementIsPresent(driver, Elements.DesignCenter_PackageIndex_SubSubCat_MaterialInfo_Col_Header_Txt);
                    }

                    //--- Expected Result: Package Details will have the Package Title ---//
                    test.validateElementIsPresent(driver, Elements.DesignCenter_PackageIndex_SubSubCat_Package_Col_Val_Txts);

                    //--- Expected Result: Package Details will have the Link to download outline PDF  ---//
                    test.validateElementIsPresent(driver, Elements.DesignCenter_PackageIndex_SubSubCat_Outline_Col_Val_Links);

                    if (util.CheckElement(driver, Elements.DesignCenter_PackageIndex_SubSubCat_Outline_Col_Val_Links, 2))
                    {
                        //----- Design Center - Packaging TestPlan > R2 > T3: Verify "Package Type" links (IQ-8986/AL-15613) -----//

                        //--- Expected Result: Download links must be displayed as "PDF" (all upper case) ---//
                        test.validateStringIsCorrect(driver, Elements.DesignCenter_PackageIndex_SubSubCat_Outline_Col_Val_Links, pdf_txt);

                        if (!Configuration.Browser.Equals("Chrome_HL"))
                        {
                            //----- Design Center - Packaging TestPlan > R2 > T5: Verify link to download outline PDF -----//

                            //--- Action: Click "pdf" link under the Outline column in each package title. ---//
                            action.IOpenLinkInNewTab(driver, Elements.DesignCenter_PackageIndex_SubSubCat_Outline_Col_Val_Links);
                            Thread.Sleep(1000);

                            //--- Expected Result: A pdf file should open corresponding to the package selected. ---//
                            test.validateStringInstance(driver, driver.Url, ".pdf");

                            driver.Close();
                            driver.SwitchTo().Window(driver.WindowHandles.Last());
                        }
                    }

                    if (util.CheckElement(driver, Elements.DesignCenter_PackageIndex_SubSubCat_MaterialInfo_Col_Val_Links, 2) && !Configuration.Browser.Equals("Chrome_HL"))
                    {
                        //----- Design Center - Packaging TestPlan > R2 > T6: Verify link to download Material Information PDF -----//

                        //--- Action: Click "pdf" link under the Material Information column in each package title. ---//
                        action.IOpenLinkInNewTab(driver, Elements.DesignCenter_PackageIndex_SubSubCat_MaterialInfo_Col_Val_Links);

                        //--- Expected Result: A pdf file should open corresponding to the package selected. ---//
                        test.validateStringInstance(driver, driver.Url, ".pdf");
                    }
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.DesignCenter_PackageIndex_SubCat_ViewMore_Links);
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.DesignCenter_PackageIndex_SubCat_ViewMore_Links);
            }
        }
    }
}