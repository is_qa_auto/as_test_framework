﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.DesignCenter
{
    [TestFixture]
    public class DesignCenter_CircuitDesignToolsAndCalculators_LtSpice : BaseSetUp
    {
        public DesignCenter_CircuitDesignToolsAndCalculators_LtSpice() : base() { }

        //--- URLs ---//
        string ltSpice_page_url = "/design-center/design-tools-and-calculators/ltspice-simulator.html";
        string searchResults_page_url = "/search.html";

        //--- Labels ---//
        string downloadLtSpice_note = "*date displayed reflects the most recent upload date";
        string newest_txt = "newest";

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Download LTspice Section is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Download LTspice Section is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Download LTspice Section is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Download LTspice Section is Present and Working as Expected in RU Locale")]
        public void DesignCenter_CircuitDesignToolsAndCalculators_VerifyDownloadLtSpiceSection(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + ltSpice_page_url);

            //----- R5 > T2: Verify download of LTspice -----//

            //--- Expected Result: Download buttons/links are displayed ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_LtSpice_DownloadForWindows_Btn);
            test.validateElementIsPresent(driver, Elements.DesignCenter_LtSpice_DownloadForMac_Btn);

            //----- R5 > T1: Verify the updates in  the LTspice FT component -----//

            //--- Expected Result: The LTspice simulation software/s in the Download LTspice section has an asterisk (*) after the "Updated on <Date>". ---//
            test.validateStringInstance(driver, util.GetText(driver, Elements.DesignCenter_LtSpice_DownloadForWindows_UpdatedDate_Txt), "*");
            test.validateStringInstance(driver, util.GetText(driver, Elements.DesignCenter_LtSpice_DownloadForMac_UpdatedDate_Txt), "*");

            //--- Expected Result: The below given text is added: ---//
            //--- *date displayed reflects the most recent upload date ---//
            if (Locale.Equals("en"))
            {
                test.validateStringIsCorrect(driver, Elements.DesignCenter_LtSpice_DownloadLtSpice_Note_Txt, downloadLtSpice_note);
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.DesignCenter_LtSpice_DownloadLtSpice_Note_Txt);
            }
        }

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the LTspice Technical Articles & Videos Section is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the LTspice Technical Articles & Videos Section is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the LTspice Technical Articles & Videos Section is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the LTspice Technical Articles & Videos Section is Present and Working as Expected in RU Locale")]
        public void DesignCenter_CircuitDesignToolsAndCalculators_VerifyLtSpiceTechnicalArticlesAndVideosSection(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + ltSpice_page_url);

            //----- R5 > T9: Verify that the search results are sorted by "Newest" after clicking Technical Videos and Technical Articles links -----//

            //--- Action: Click View our Technical Articles  link ---//
            if (util.CheckElement(driver, Elements.DesignCenter_LtSpice_ViewOurTechnicalArticles_Link, 2))
            {
                action.IOpenLinkInNewTab(driver, Elements.DesignCenter_LtSpice_ViewOurTechnicalArticles_Link);
                Thread.Sleep(1000);

                //--- Expected Result: User will be redirected to the search results ---//
                test.validateStringInstance(driver, driver.Url, searchResults_page_url);

                //--- Expected Result: search results are sorted by "Newest" ---//
                if (Locale.Equals("en"))
                {
                    test.validateSelectedValueIsCorrect(driver, Elements.GlobalSearchResults_Sort_Menu, newest_txt);
                }

                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles.First());
            }

            //--- Action: Click View our Technical Videos link ---//
            if (util.CheckElement(driver, Elements.DesignCenter_LtSpice_ViewOurTechnicalVideos_Link, 2))
            {
                action.IOpenLinkInNewTab(driver, Elements.DesignCenter_LtSpice_ViewOurTechnicalVideos_Link);
                Thread.Sleep(1000);

                //--- Expected Result: User will be redirected to the search results ---//
                test.validateStringInstance(driver, driver.Url, searchResults_page_url);

                //--- Expected Result: search results are sorted by "Newest" ---//
                if (Locale.Equals("en"))
                {
                    test.validateSelectedValueIsCorrect(driver, Elements.GlobalSearchResults_Sort_Menu, newest_txt);
                }
            }
        }
    }
}