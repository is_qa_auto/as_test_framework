﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;

namespace AS_Test_Framework.DesignCenter
{
    [TestFixture]
    public class DesignCenter_CircuitDesignToolsAndCalculators_DataConverterTools : BaseSetUp
    {
        public DesignCenter_CircuitDesignToolsAndCalculators_DataConverterTools() : base() { }

        //--- URLs ---//
        string dataConverterTools_page_url = "/design-center/design-tools-and-calculators/converter-tools.html";
        string thirdPartyTools_page_url = "/design-center/design-tools-and-calculators/converter-tools.html";

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that Data Converter Tools is Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that Data Converter Tools is Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that Data Converter Tools is Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that Data Converter Tools is Working as Expected in RU Locale")]
        public void DesignCenter_CircuitDesignToolsAndCalculators_VerifyDataConverterTools(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + dataConverterTools_page_url);

            //----- R5 > T4: Validate Data Converter Tools -----//

            //--- Expected Result: The items are listed in the page has description ---//
            test.validateElementIsPresent(driver, Elements.DesignCenter_DataConverterTools_DesignTools_Links);
            test.validateElementIsPresent(driver, Elements.DesignCenter_DataConverterTools_DesignTools_Desc_Txts);
        }

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that Third Party Tools Link is Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that Third Party Tools Link is Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that Third Party Tools Link is Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that Third Party Tools Link is Working as Expected in RU Locale")]
        public void DesignCenter_CircuitDesignToolsAndCalculators_VerifyThirdPartyToolsLinkRedirection(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + thirdPartyTools_page_url);

            //----- R5 > T4: Validate Data Converter Tools -----//

            //--- Expected Result: Page should redirect to the Data Converter Tools page ---//
            test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + dataConverterTools_page_url);
        }
    }
}