﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System;
using System.Threading;
using OpenQA.Selenium;
using System.Linq;

namespace AS_Test_Framework.Header
{

    //[TestFixture]
    public class Homepage_VerifMyHistoryTab : BaseSetUp
    {
        public Homepage_VerifMyHistoryTab() : base() { }

        //[Test, Category("Header"), Category("Core"), Retry(2)]
        //[TestCase("en", "Products Viewed", "Categories Viewed", "Pages Viewed", "Products viewed will be listed here.", "Categories viewed will be listed here.", TestName = "Verify myHistory tab and its default section is present for EN Locale")]
        //[TestCase("cn", "产品回顾", "分类回顾", "页面回顾", "您所浏览过的产品会显示在这里。", "您所浏览过的分类会显示在这里。", TestName = "Verify myHistory tab and its default section is present for CN Locale")]
        //[TestCase("jp", "製品ページ", "製品カテゴリー・ページ", "その他のページ", "閲覧された製品が表示されます。", "閲覧された製品カテゴリーが表示されます。", TestName = "Verify myHistory tab and its default section is present for JP Locale")]
        //[TestCase("ru", "Просмотренные продукты", "Просмотренные категории", "Просмотренные страницы", "Здесь будет выведен список просмотренных продуктов", "Здесь будет выведен список просмотренных категорий", TestName = "Verify myHistory tab and its default section is present for RU Locale")]

        public void Homepage_VerifyGlobalSearchTextField(string Locale, string ProdViewed, string CatViewed, string PagesViewed, string DefaultProdViewed, string DefaultCatViewed)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/index.html");
            Assert.Multiple(() =>
            {
                test.validateElementIsPresent(driver, Elements.MainNavigation_MyHistoryTab);
                action.IClick(driver, Elements.MainNavigation_MyHistoryTab);
                test.validateElementIsPresent(driver, By.CssSelector("div[class='myhistory menu content expanded']"));
                test.validateStringIsCorrect(driver, By.CssSelector("div[class='myhistory menu content expanded']>div>div>div[class='col-md-4']:nth-child(1)>div[class='header1']"), ProdViewed);
                test.validateStringIsCorrect(driver, By.CssSelector("div[class='myhistory menu content expanded']>div>div>div[class='col-md-4']:nth-child(2)>div[class='header1']"), CatViewed);
                test.validateStringIsCorrect(driver, By.CssSelector("div[class='myhistory menu content expanded']>div>div>div[class='col-md-4']:nth-child(3)>div[class='header1']"), PagesViewed);
                test.validateStringIsCorrect(driver, By.CssSelector("div[class='myhistory menu content expanded']>div>div>div[class='col-md-4']:nth-child(1)>ul>li"), DefaultProdViewed);
                test.validateStringIsCorrect(driver, By.CssSelector("div[class='myhistory menu content expanded']>div>div>div[class='col-md-4']:nth-child(2)>ul>li"), DefaultCatViewed);

            });
        }

        //[Test, Category("Header"), Category("Core"), Retry(2)]
        //[TestCase("en", TestName = "Verify that visited Product will be converted to product short links + data sheet links and available in the product viewed list in EN Locale")]
        //[TestCase("cn", TestName = "Verify that visited Product will be converted to product short links + data sheet links and available in the product viewed list in CN Locale")]
        //[TestCase("jp", TestName = "Verify that visited Product will be converted to product short links + data sheet links and available in the product viewed list in JP Locale")]
        //[TestCase("ru", TestName = "Verify that visited Product will be converted to product short links + data sheet links and available in the product viewed list in RU Locale")]
        public void Homepage_VerifyProductInMyHistoryTab(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/index.html");
            driver.Navigate().GoToUrl(Configuration.Env_Url + Locale + "/ad7960");
            action.IClick(driver, Elements.MainNavigation_MyHistoryTab);
            test.validateStringIsCorrect(driver, By.CssSelector("div[class='myhistory menu content expanded']>div>div>div[class='col-md-4']:nth-child(1)>ul>li>a:nth-of-type(1)"), "AD7960");
            test.validateStringIsCorrect(driver, By.CssSelector("div[class='myhistory menu content expanded']>div>div>div[class='col-md-4']:nth-child(1)>ul>li>a:nth-of-type(2)"), "Data Sheet");
            action.IClick(driver, By.CssSelector("div[class='myhistory menu content expanded']>div>div>div[class='col-md-4']:nth-child(1)>ul>li>a:nth-child(1)"));
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/products/ad7960.html");
            if (!Configuration.Browser.Equals("Chrome_HL")) { 
                action.IClick(driver, Elements.MainNavigation_MyHistoryTab);
                action.IClick(driver, By.CssSelector("div[class='myhistory menu content expanded']>div>div>div[class='col-md-4']:nth-child(1)>ul>li>a:nth-of-type(2)"));
                driver.SwitchTo().Window(driver.WindowHandles.Last());
                if (Locale.Equals("cn"))
                {
                    test.validateScreenByUrl(driver, Configuration.Env_Url + "media/" + Locale + "/technical-documentation/data-sheets/AD7960_cn.pdf");
                }
                else
                {
                    test.validateScreenByUrl(driver, Configuration.Env_Url + "media/en/technical-documentation/data-sheets/AD7960.pdf");
                }
            }
        }

        //[Test, Category("Header"), Category("Core"), Retry(2)]
        //[TestCase("en", TestName = "Verify that visited Obsolete Product will be available in the product viewed list in EN Locale")]
        //[TestCase("cn", TestName = "Verify that visited Obsolete Product will be available in the product viewed list in CN Locale")]
        //[TestCase("jp", TestName = "Verify that visited Obsolete Product will be available in the product viewed list in JP Locale")]
        //[TestCase("ru", TestName = "Verify that visited Obsolete Product will be available in the product viewed list in RU Locale")]
        public void Homepage_VerifyObsoleteProductInMyHistoryTab(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/index.html");
            driver.Navigate().GoToUrl(Configuration.Env_Url + Locale + "/ad872");
            action.IClick(driver, Elements.MainNavigation_MyHistoryTab);
            test.validateStringIsCorrect(driver, By.CssSelector("div[class='myhistory menu content expanded']>div>div>div[class='col-md-4']:nth-child(1)>ul>li>a:nth-of-type(1)"), "AD872");
        }

        //[Test, Category("Header"), Category("Core"), Retry(2)]
        //[TestCase("en", "Amplifiers", TestName = "Verify that visited Product Category will be available in the categories viewed list in EN Locale")]
        //[TestCase("cn", "放大器", TestName = "Verify that visited Product Category will be available in the categories viewed list in CN Locale")]
        //[TestCase("jp", "アンプ", TestName = "Verify that visited Product Category will be available in the categories viewed list in JP Locale")]
        //[TestCase("ru", "Усилители", TestName = "Verify that visited Product Category will be available in the categories viewed list in RU Locale")]
        public void Homepage_VerifyProductCategoryInMyHistoryTab(string Locale, string ProdCatLabel)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/index.html");
            driver.Navigate().GoToUrl(Configuration.Env_Url + Locale + "/amplifiers");
            action.IClick(driver, Elements.MainNavigation_MyHistoryTab);
            test.validateStringIsCorrect(driver, By.CssSelector("div[class='myhistory menu content expanded']>div>div>div[class='col-md-4']:nth-child(2)>ul>li:nth-of-type(1)>a"), ProdCatLabel);
            action.IClick(driver, By.CssSelector("div[class='myhistory menu content expanded']>div>div>div[class='col-md-4']:nth-child(2)>ul>li:nth-of-type(1)>a"));
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/products/amplifiers.html");
        }

        //[Test, Category("Header"), Category("Core"), Retry(2)]
        //[TestCase("en", TestName = "Verify that other pages will be available in the pages viewed list in EN Locale")]
        //[TestCase("cn", TestName = "Verify that other pages will be available in the pages viewed list in CN Locale")]
        //[TestCase("jp", TestName = "Verify that other pages will be available in the pages viewed list in JP Locale")]
        //[TestCase("ru", TestName = "Verify that other pages will be available in the pages viewed list in RU Locale")]
        public void Homepage_VerifyOtherPagesInMyHistoryTab(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/index.html");
            driver.Navigate().GoToUrl(Configuration.Env_Url + Locale + "/about-adi.html");
            driver.Navigate().GoToUrl(Configuration.Env_Url + Locale + "/ADSWT-CCES");
            driver.Navigate().GoToUrl(Configuration.Env_Url + Locale + "/about-adi/partners.html");
            driver.Navigate().GoToUrl(Configuration.Env_Url + Locale + "/about-adi/quality-reliability.html");
            if (Locale.Equals("cn")) {
                driver.Navigate().GoToUrl(Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx?locale=zh");
            }
            else
            {
                driver.Navigate().GoToUrl(Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx?locale=" + Locale);
            }
            driver.Navigate().GoToUrl(Configuration.Env_Url + Locale + "/design-center.html");

            action.IClick(driver, Elements.MainNavigation_MyHistoryTab);
            test.validateString(driver, "5", util.GetCount(driver, By.CssSelector("div[class='myhistory menu content expanded']>div>div>div[class='col-md-4']:nth-child(3)>ul>li")).ToString());

            action.IClick(driver, Elements.MainNavigation_MyAnalogTab);
            action.IClick(driver, Elements.LoginToMyAnalog_Button);
            action.ILogin(driver, "aries.sorosoro@analog.com", "Test_1234");
            action.IClick(driver, Elements.MainNavigation_MyHistoryTab);
            test.validateString(driver, "5", util.GetCount(driver, By.CssSelector("div[class='myhistory menu content expanded']>div>div>div[class='col-md-4']:nth-child(3)>ul>li")).ToString());

        }


    }
}
