﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;

namespace AS_Test_Framework.Header
{

    [TestFixture]
    public class Homepage_ShortHeader_VerifyLanguageIcon : BaseSetUp
    {
        public Homepage_ShortHeader_VerifyLanguageIcon() : base() { }
        [Test, Category("Header"), Category("Core"), Retry(2)]
        [TestCase(TestName = "Verify that Regional Menu is present in short header and working as expected")]
        public void Homepage_ShortHeader_VerifyRegionalLink()
        {
            action.Navigate(driver, Configuration.Env_Url + "en/index.html");
            Assert.Multiple(() =>
            {
                test.validateElementIsPresent(driver, Elements.Regional_Link);
                action.IClick(driver, Elements.Regional_Link);
                test.validateElementIsPresent(driver, Elements.Regional_Menu);
                test.validateElementIsPresent(driver, Elements.Language_Menu);
            });
        }

        [Test, Category("Header"), Category("Core"), Retry(2)]
        [TestCase(TestName = "Verify that User can go to CN locale by clicking 简体中文 in language menu")]
        public void Homepage_ShortHeader_VerifyLanguage_Menu_CN()
        {
            action.Navigate(driver, Configuration.Env_Url + "en/index.html");
            action.IClick(driver, Elements.Regional_Link);
            action.IClick(driver, Elements.Language_Menu_Chinese);
            test.validateScreenByUrl(driver, Configuration.Env_Url + "cn/index.html");
        }

        [Test, Category("Header"), Category("Core"), Retry(2)]
        [TestCase(TestName = "Verify that User can go to JP locale by clicking 日本語 in language menu")]
        public void Homepage_ShortHeader_VerifyLanguage_Menu_JP()
        {
            action.Navigate(driver, Configuration.Env_Url + "en/index.html");
            action.IClick(driver, Elements.Regional_Link);
            action.IClick(driver, Elements.Language_Menu_Japan);
            test.validateScreenByUrl(driver, Configuration.Env_Url + "jp/index.html");
        }

        [Test, Category("Header"), Category("Core"), Retry(2)]
        [TestCase(TestName = "Verify that User can go to RU locale by clicking Руccкий in language menu")]
        public void Homepage_ShortHeader_VerifyLanguage_Menu_RU()
        {
            action.Navigate(driver, Configuration.Env_Url + "en/index.html");
            action.IClick(driver, Elements.Regional_Link);
            action.IClick(driver, Elements.Language_Menu_Russian);
            test.validateScreenByUrl(driver, Configuration.Env_Url + "ru/index.html");
        }

        [Test, Category("Header"), Category("Core"), Retry(2)]
        [TestCase(TestName = "Verify \"NoFollow\" On Language Links in Header Nav")]
        public void Homepage_VerifyRelFollowAttributeInLanguage() {

            action.Navigate(driver, Configuration.Env_Url + "en/about-adi/news-room/press-releases/2014/10/10/08/46/03-25-13-smallest-mems-microphone-designed.html");
            action.IClick(driver, Elements.Regional_Link);
            test.validateString(driver, "nofollow", util.ReturnAttribute(driver, Elements.Language_Menu_Chinese, "rel"));
            action.Navigate(driver, Configuration.Env_Url + "en/AD7960");
            action.IClick(driver, Elements.Regional_Link);
            test.validateElementIsNotPresent(driver, By.CssSelector("li[class='insert-language js-insert-language open']>div[class='dropdown-menu']>ul[class='setLang']>li>a[rel='nofollow']"));
        }
    }

}
