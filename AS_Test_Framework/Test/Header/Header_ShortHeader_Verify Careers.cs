﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;

namespace AS_Test_Framework.Header
{

    [TestFixture]
    public class Homepage_ShortHeader_VerifyCareers : BaseSetUp
    {
        public Homepage_ShortHeader_VerifyCareers() : base() { }

        [Test, Category("Header"), Category("Core"), Retry(2)]
        [TestCase("en", "https://careers.analog.com/", TestName = "Verify that Careers link is present in short header and working as expected in EN Locale")]
        [TestCase("cn", "https://careers.analog.com/", TestName = "Verify that Careers link is present in short header and working as expected in CN Locale")]
        [TestCase("jp", "https://careers.analog.com/", TestName = "Verify that Careers link is present in short header and working as expected in JP Locale")]
        [TestCase("ru", "https://careers.analog.com/", TestName = "Verify that Careers link is present in short header and working as expected in RU Locale")]
        public void Homepage_ShortHeader_VerifyCareersLink(string locale, string exp)
        {
            if (locale.Equals("jp")) {
                exp = Configuration.Env_Url + locale + "/about-adi/careers.html";
            }
            action.Navigate(driver, Configuration.Env_Url + locale + "/index.html");
            Assert.Multiple(() =>
            {
                test.validateElementIsPresent(driver, Elements.Careers_Link);
                if (Configuration.Environment.Equals("production"))
                {
                    action.IClick(driver, Elements.Careers_Link);
                    test.validateScreenByUrl(driver, exp);
                }
            });
        }

    }

}
