﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System;
using System.Threading;

namespace AS_Test_Framework.Header
{

    [TestFixture]
    public class Homepage_VerifyGlobalSearch : BaseSetUp
    {
        public Homepage_VerifyGlobalSearch() : base() { }

        [Test, Category("Header"), Category("Core"), Retry(2)]
        [TestCase("en", "Search", TestName = "Verify that global search is present and working as expected in Homepage for EN Locale")]
        [TestCase("cn", "搜索产品型号或关键词", TestName = "Verify that global search is present and working as expected in Homepage for CN Locale")]
        [TestCase("jp", "キーワード／製品検索", TestName = "Verify that global search is present and working as expected in Homepage for JP Locale")]
        [TestCase("ru", "Поиск", TestName = "Verify that global search is present and working as expected in Homepage for RU Locale")]
        public void Homepage_VerifyGlobalSearchTextField(string Locale, string GhostText)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/index.html");
            Assert.Multiple(() =>
            {
                test.validateElementIsPresent(driver, Elements.GlobalSearchTextField);
                test.validateString(driver, GhostText, util.ReturnAttribute(driver, Elements.GlobalSearchTextField, "placeholder"));
                test.validateElementIsNotPresent(driver, Elements.XRefLink);
                action.IClick(driver, Elements.GlobalSearchTextField);
                test.validateElementIsPresent(driver, Elements.XRefLink);
                action.IType(driver, Elements.GlobalSearchTextField, "AD7960");
                test.validateElementIsPresent(driver, Elements.GlobalSearch_ClearButton);
                action.IClick(driver, Elements.GlobalSearch_ClearButton);
                test.validateString(driver, string.Empty, util.ReturnAttribute(driver, Elements.GlobalSearchTextField, "value"));
                action.IType(driver, Elements.GlobalSearchTextField, "AD7960");
                test.validateElementIsPresent(driver, Elements.GlobalSearch_Auto_Complete);
                action.IClick(driver, Elements.GlobalSearch_SearchIcon);
                test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/search.html?q=AD7960");

            });
        }
    }

}
