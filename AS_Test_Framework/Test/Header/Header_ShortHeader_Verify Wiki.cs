﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;

namespace AS_Test_Framework.Header
{

    [TestFixture]
    public class Homepage_ShortHeader_VerifyWiki : BaseSetUp
    {
        public Homepage_ShortHeader_VerifyWiki() : base(){ }

        [Test, Category("Header"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Core Page - Verify that wiki link is present in short header and working as expected in EN Locale")]
        [TestCase("cn", TestName = "Core Page - Verify that wiki link is present in short header and working as expected in CN Locale")]
        [TestCase("jp", TestName = "Core Page - Verify that wiki link is present in short header and working as expected in JP Locale")]
        [TestCase("ru", TestName = "Core Page - Verify that wiki link is present in short header and working as expected in RU Locale")]
        public void Homepage_ShortHeader_VerifyWikiLink(string locale)
        {
            action.Navigate(driver, Configuration.Env_Url + locale + "/index.html");
            Assert.Multiple(() =>
            {
                test.validateElementIsPresent(driver, Elements.Wiki_Link);
                if (Configuration.Environment.Equals("production"))
                { 
                    action.IClick(driver, Elements.Wiki_Link);
                    test.validateScreenByUrl(driver, "https://wiki.analog.com/");
                }
            });
        }

        [Test, Category("Header"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Non Core Page - Verify that wiki link is present in short header and working as expected in EN Locale")]
        [TestCase("zh", TestName = "Non Core Page - Verify that wiki link is present in short header and working as expected in CN Locale")]
        [TestCase("jp", TestName = "Non Core Page - Verify that wiki link is present in short header and working as expected in JP Locale")]
        [TestCase("ru", TestName = "Non Core Page - Verify that wiki link is present in short header and working as expected in RU Locale")]
        public void Homepage_ShortHeader_VerifyWikiLinkinNonCore(string locale)
        {
            action.Navigate(driver, Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx?locale=" + locale);
            Assert.Multiple(() =>
            {
                test.validateElementIsPresent(driver, Elements.Wiki_Link);
                if (Configuration.Environment.Equals("production"))
                {
                    action.IClick(driver, Elements.Wiki_Link);
                    test.validateScreenByUrl(driver, "https://wiki.analog.com/");
                }
            });
        }

        [Test, Category("Header"), Category("Core"), Retry(2)]
        [TestCase(TestName = "Verify link upon hovering myAnalog and Login widgets in Wiki page")]
        public void Homepage_ShortHeader_VerifyWikiLinkUrl()
        {
            if (Configuration.Environment.Equals("production"))
            { 
                action.Navigate(driver, "https://wiki.analog.com/");
                Assert.Multiple(() =>
                {
                    test.validateString(driver, "https://my.analog.com/", util.ReturnAttribute(driver, By.CssSelector("a[class='adi_topbar-link adi_topbar-link--light adi_topbar-link--myanalog-logo']"), "href"));
                    test.validateString(driver, "https://wiki.analog.com/start?do=login&sectok=", util.ReturnAttribute(driver, By.CssSelector("a[class='adi_topbar-link adi_topbar-link--light adi_topbar-link--myanalog-login action login']"), "href"));
                });
            }
        }
    }

}
