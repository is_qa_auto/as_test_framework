﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;

namespace AS_Test_Framework.Header
{

    [TestFixture]
    public class Homepage_ShortHeader_VerifyRegionalIcon : BaseSetUp
    {
        public Homepage_ShortHeader_VerifyRegionalIcon() : base() { }

        [Test, Category("Header"), Category("Core"), Retry(2)]
        [TestCase(TestName = "Verify that Regional Links are in Alphabetical Order")]
        public void Homepage_ShortHeader_VerifyRegional_Menu_Order()
        {
            action.Navigate(driver, Configuration.Env_Url + "en/index.html");
            action.IClick(driver, Elements.Regional_Link);

            string[] journal_volume = new string[4];
            for (int x = 2, y = 0; y <= 3; x++, y++)
            {
                journal_volume[y] = util.GetText(driver, By.CssSelector("li[class='insert-language js-insert-language open']>div[class='dropdown-menu']>ul:nth-child(1)>li:nth-child(" + x + ")>a"));
            }
            
            util.IsAscendingOrder(journal_volume);
            test.validateStringIfInAscenading(journal_volume);
        }

        [Test, Category("Header"), Category("Core"), Retry(2)]
        [TestCase(TestName = "Verify that User can go to ADI - India Homepage by clicking India in Regional Menu")]
        public void Homepage_ShortHeader_VerifyRegional_Menu_India()
        {
            action.Navigate(driver, Configuration.Env_Url + "en/index.html");
            action.IClick(driver, Elements.Regional_Link);
            action.IClick(driver, Elements.Regional_Menu_India);
            test.validateStringInstance(driver, driver.Url, "analog.com/en/landing-pages/001/india-welcome.html");
        }

        [Test, Category("Header"), Category("Core"), Retry(2)]
        [TestCase(TestName = "Verify that User can go to ADI - Korea Homepage by clicking Korea in Regional Menu")]
        public void Homepage_ShortHeader_VerifyRegional_Menu_Korea()
        {
            action.Navigate(driver, Configuration.Env_Url + "en/index.html");
            action.IClick(driver, Elements.Regional_Link);
            action.IClick(driver, Elements.Regional_Menu_Korea);
            test.validateStringInstance(driver, driver.Url, "analog.com/en/landing-pages/001/korea-welcome.html");
        }

        [Test, Category("Header"), Category("Core"), Retry(2)]
        [TestCase(TestName = "Verify that User can go to ADI - Singapore Homepage by clicking Singapore in Regional Menu")]
        public void Homepage_ShortHeader_VerifyRegional_Menu_Singapore()
        {
            action.Navigate(driver, Configuration.Env_Url + "en/index.html");
            action.IClick(driver, Elements.Regional_Link);
            action.IClick(driver, Elements.Regional_Menu_Singapore);
            test.validateStringInstance(driver, driver.Url, "analog.com/en/landing-pages/001/singapore-welcome.html");
        }

        [Test, Category("Header"), Category("Core"), Retry(2)]
        [TestCase(TestName = "Verify that User can go to ADI - Taiwan Homepage by clicking Taiwan in Regional Menu")]
        public void Homepage_ShortHeader_VerifyRegional_Menu_Taiwan()
        {
            action.Navigate(driver, Configuration.Env_Url + "en/index.html");
            action.IClick(driver, Elements.Regional_Link);
            action.IClick(driver, Elements.Regional_Menu_Taiwan);
            test.validateStringInstance(driver, driver.Url, "analog.com/en/landing-pages/001/taiwan-welcome.html");
        }

    }

}
