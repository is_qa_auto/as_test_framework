﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System;
using System.Threading;
using OpenQA.Selenium;
using System.Linq;

namespace AS_Test_Framework.Header
{

    [TestFixture]
    public class Homepage_MainNavigationMenu_VerifyEducationTab : BaseSetUp
    {
        public Homepage_MainNavigationMenu_VerifyEducationTab() : base() { }

        /*********Education Tab*********/
        [Test, Category("Header"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Core - Verify that Education tab in Main Navigation Menu is present and working as expected for EN Locale")]
        [TestCase("cn", TestName = "Core - Verify that Education tab in Main Navigation Menu is present and working as expected for CN Locale")]
        [TestCase("jp", TestName = "Core - Verify that Education tab in Main Navigation Menu is present and working as expected for JP Locale")]
        [TestCase("ru", TestName = "Core - Verify that Education tab in Main Navigation Menu is present and working as expected for RU Locale")]
        public void Homepage_VerifyEducationsTab(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/index.html");
            test.validateElementIsPresent(driver, Elements.MainNavigation_EducationTab);
            action.IMouseOverTo(driver, Elements.MainNavigation_EducationTab);
            test.validateColorIsCorrect(driver, util.GetCssValue(driver, Elements.MainNavigation_EducationTab, "background-color"), "#7c4a8a");
            action.IClick(driver, Elements.MainNavigation_EducationTab);
            test.validateColorIsCorrect(driver, util.GetCssValue(driver, Elements.MainNavigation_EducationTab, "background-color"), "#7c4a8a");
            test.validateElementIsPresent(driver, By.CssSelector("div[class='education menu content expanded']"));
            test.validateElementIsPresent(driver, By.CssSelector("div[class='education__section']"));
            action.IClick(driver, Elements.MainNavigation_Company);
            test.validateElementIsNotPresent(driver, By.CssSelector("div[class='education menu content expanded']"));
        }

        [Test, Category("Header"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Core - Verify Education tab behavior when clicking a link/other tabs for EN Locale")]
        [TestCase("cn", TestName = "Core - Verify Education tab behavior when clicking a link/other tabs for CN Locale")]
        [TestCase("jp", TestName = "Core - Verify Education tab behavior when clicking a link/other tabs for JP Locale")]
        [TestCase("ru", TestName = "Core - Verify Education tab behavior when clicking a link/other tabs for RU Locale")]
        public void Homepage_VerifyCompanyTab2(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/index.html");
            action.IClick(driver, Elements.MainNavigation_EducationTab);

            action.IClick(driver, By.CssSelector("div[class='education menu content expanded'] * div[class='education__nav__parent']:nth-child(1)>div[class='header3']>a"));
            test.validateColorIsCorrect(driver, util.GetCssValue(driver, Elements.MainNavigation_EducationTab, "background-color"), "#7c4a8a");
            action.IClick(driver, Elements.MainNavigation_ApplicationsTab);
            test.validateColorIsCorrect(driver, util.GetCssValue(driver, Elements.MainNavigation_EducationTab, "background-color"), "#7c4a8a");

            action.IClick(driver, Elements.MainNavigation_EducationTab);
            action.IClick(driver, By.CssSelector("div[class='education menu content expanded'] * div[class='education__nav__parent']:nth-child(2)>div[class='header3']>a"));
            test.validateColorIsCorrect(driver, util.GetCssValue(driver, Elements.MainNavigation_EducationTab, "background-color"), "#7c4a8a");
            action.IClick(driver, Elements.MainNavigation_ApplicationsTab);
            test.validateColorIsCorrect(driver, util.GetCssValue(driver, Elements.MainNavigation_EducationTab, "background-color"), "#7c4a8a");

            action.IClick(driver, Elements.MainNavigation_EducationTab);
            action.IClick(driver, By.CssSelector("div[class='education menu content expanded'] * div[class='education__nav__parent']:nth-child(3)>div[class='header3']>a"));
            test.validateColorIsCorrect(driver, util.GetCssValue(driver, Elements.MainNavigation_EducationTab, "background-color"), "#7c4a8a");
            action.IClick(driver, Elements.MainNavigation_ApplicationsTab);
            test.validateColorIsCorrect(driver, util.GetCssValue(driver, Elements.MainNavigation_EducationTab, "background-color"), "#7c4a8a");

        }
    }

}
