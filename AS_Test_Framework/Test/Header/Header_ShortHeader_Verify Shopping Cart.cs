﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Globalization;
using System;
using System.Collections.Generic;

namespace AS_Test_Framework.Header
{

    [TestFixture]
    public class Homepage_ShortHeader_VerifyShoppingCart : BaseSetUp
    {
        public Homepage_ShortHeader_VerifyShoppingCart() : base() { }
        [Test, Category("Header"), Category("Core")]
        [TestCase("en", "ShoppingCartPage.aspx", "Select Shipping Country/Region", TestName = "Verify that shopping cart icon is present in short header and working as expected in EN Locale")]
        [TestCase("cn", "ShoppingCartPage.aspx?locale=zh", "CHINA", TestName = "Verify that shopping cart icon is present in short header and working as expected in CN Locale")]
        [TestCase("jp", "ShoppingCartPage.aspx?locale=jp", "JAPAN", TestName = "Verify that shopping cart icon is present in short header and working as expected in JP Locale")]
        [TestCase("ru", "ShoppingCartPage.aspx?locale=ru", "Select Shipping Country/Region", TestName = "Verify that shopping cart icon is present in short header and working as expected in RU Locale")]
        public void Homepage_ShortHeader_VerifyShoppingCartCLink(string locale, string SC_Url, string SelectedCountry)
        {
            //Assert.Multiple(() =>
            //{
            action.Navigate(driver, Configuration.Env_Url + locale + "/index.html");
            test.validateElementIsPresent(driver, Elements.Cart_Icon);
            action.IClick(driver, Elements.Cart_Icon);
            test.validateStringInstance(driver, driver.Url, SC_Url);
            if (locale.Equals("cn") || locale.Equals("jp"))
            {
                test.validateStringIsCorrect(driver, Elements.Shopping_Cart_Country_Dropdown, SelectedCountry);
            }
            else {
                test.validateStringIsCorrect(driver, Elements.Shopping_Cart_Country_Dropdown, util.GetCurrentCountry(driver));
            }
            //});
        }
            
    }

}
