﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;

namespace AS_Test_Framework.Header
{

    [TestFixture]
    public class Homepage_VerifyAnalogLogo : BaseSetUp
    {
        public Homepage_VerifyAnalogLogo() : base() { }

        [Test, Category("Header"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Core Page - Verify that Analog Logo is present in header and working as expected in EN Locale", Category = "Smoke_ADIWeb")]
        [TestCase("cn", TestName = "Core Page - Verify that Analog Logo is present in header and working as expected in CN Locale", Category = "Smoke_ADIWeb")]
        [TestCase("jp", TestName = "Core Page - Verify that Analog Logo is present in header and working as expected in JP Locale", Category = "Smoke_ADIWeb")]
        [TestCase("ru", TestName = "Core Page - Verify that Analog Logo is present in header and working as expected in RU Locale")]
        public void Homepage_ShortHeader_VerifyAnalogLogo(string locale)
        {
            string initial_steps = "1. Go to " + Configuration.Env_Url + locale + "/AD7960";
            string scenario = "Verify that Analog Logo is present in analog.com";

            action.INavigate(driver, Configuration.Env_Url + locale + "/AD7960");
            Assert.Multiple(() =>
            {
                test.validateElementIsPresentv2(driver, Elements.AnalogLog, scenario, initial_steps);
                action.IClick(driver, Elements.AnalogLog);
                scenario = "Verify that clicking analog logo will redirect user to homepage";
                test.validateScreenByUrlv2(driver, Configuration.Env_Url + locale + "/index.html", scenario, initial_steps + "<br>2. Click Analog Logo");
            });
        }


        [Test, Category("Header"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Non Core Page - Verify that Analog Logo is present in header and working as expected in EN Locale Shopping Cart")]
        [TestCase("zh", TestName = "Non Core Page - Verify that Analog Logo is present in header and working as expected in CN Locale Shopping Cart")]
        [TestCase("jp", TestName = "Non Core Page - Verify that Analog Logo is present in header and working as expected in JP Locale Shopping Cart")]
        [TestCase("ru", TestName = "Non Core Page - Verify that Analog Logo is present in header and working as expected in RU Locale Shopping Cart")]
        public void Homepage_ShortHeader_VerifyAnalogLogoInNonCore(string locale)
        {
            action.Navigate(driver, Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet","corpnt") + "ShoppingCartPage.aspx?locale=" + locale);
            Assert.Multiple(() =>
            {
                test.validateElementIsPresent(driver, Elements.AnalogLog);
                action.IClick(driver, Elements.AnalogLog);
                if (locale.Equals("zh"))
                {
                    test.validateScreenByUrl(driver, Configuration.Env_Url + "cn/index.html");
                }
                else { 
                    test.validateScreenByUrl(driver, Configuration.Env_Url + locale + "/index.html");
                }
            });
        }
    }

}
