﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;

namespace AS_Test_Framework.Header
{

    [TestFixture]
    public class Homepage_VerifyBreadCrumbs : BaseSetUp
    {
        public Homepage_VerifyBreadCrumbs() : base() { }

        [Test, Category("Header"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that Home icon in breadcrumbs is present in EN Locale")]
        [TestCase("cn", TestName = "Verify that Home icon in breadcrumbs is present in CN Locale")]
        [TestCase("jp", TestName = "Verify that Home icon in breadcrumbs is present in JP Locale")]
        [TestCase("ru", TestName = "Verify that Home icon in breadcrumbs is present in RU Locale")]
        public void Homepage_ShortHeader_VerifyAnalogLogo(string locale)
        {
            action.Navigate(driver, Configuration.Env_Url + locale + "/products/amplifiers.html");
            Assert.Multiple(() =>
            {
                test.validateElementIsPresent(driver, Elements.BreadCrumb_Home_Icon);
                test.validateStringInstance(driver, util.ReturnAttribute(driver,Elements.BreadCrumb_Home_Icon, "href"), "analog.com/" + locale + "/index.html");
                test.validateString(driver, "Home", util.ReturnAttribute(driver, Elements.BreadCrumb_Home_Icon, "alt"));

            });
        }
    }

}
