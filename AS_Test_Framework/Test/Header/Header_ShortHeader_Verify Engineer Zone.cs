﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;

namespace AS_Test_Framework.Header
{

    [TestFixture]
    public class Homepage_ShortHeader_VerifyEngineerZone : BaseSetUp
    {
        public Homepage_ShortHeader_VerifyEngineerZone() : base() { }

        [Test, Category("Header"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Core Page - Verify that Engineer Zone link is present in short header and working as expected in EN Locale")]
        [TestCase("cn", TestName = "Core Page - Verify that Engineer Zone link is present in short header and working as expected in CN Locale")]
        [TestCase("jp", TestName = "Core Page - Verify that Engineer Zone link is present in short header and working as expected in JP Locale")]
        [TestCase("ru", TestName = "Core Page - Verify that Engineer Zone link is present in short header and working as expected in RU Locale")]
        public void Homepage_ShortHeader_VerifyEZLink(string locale)
        {
            action.Navigate(driver, Configuration.Env_Url + locale + "/index.html");
            Assert.Multiple(() =>
            {
                test.validateElementIsPresent(driver, Elements.EZ_Link);
                if (Configuration.Environment.Equals("production"))
                {
                    action.IClick(driver, Elements.EZ_Link);
                    if (locale.Equals("cn"))
                    {
                        test.validateScreenByUrl(driver, "https://ez.analog.com/cn");
                    }
                    else
                    {
                        test.validateScreenByUrl(driver, "https://ez.analog.com/");
                    }
                }
            });
        }

        [Test, Category("Header"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Non Core Page - Verify that Engineer Zone link is present in short header and working as expected in EN Locale")]
        [TestCase("zh", TestName = "Non Core Page - Verify that Engineer Zone link is present in short header and working as expected in CN Locale")]
        [TestCase("jp", TestName = "Non Core Page - Verify that Engineer Zone link is present in short header and working as expected in JP Locale")]
        [TestCase("ru", TestName = "Non Core Page - Verify that Engineer Zone link is present in short header and working as expected in RU Locale")]
        public void Homepage_ShortHeader_VerifyEZLinkInNonCore(string locale)
        {
            action.Navigate(driver, Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet","corpnt") + "ShoppingCartPage.aspx?locale=" + locale);
            Assert.Multiple(() =>
            {
                test.validateElementIsPresent(driver, Elements.EZ_Link);
                if (Configuration.Environment.Equals("production")) { 
                    action.IClick(driver, Elements.EZ_Link);
                    if (locale.Equals("zh"))
                    {
                        test.validateScreenByUrl(driver, "https://ez.analog.com/cn");
                    } else
                    { 
                        test.validateScreenByUrl(driver, "https://ez.analog.com/");
                    }
                }
            });
        }
    }

}
