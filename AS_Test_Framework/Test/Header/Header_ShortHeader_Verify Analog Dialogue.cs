﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;

namespace AS_Test_Framework.Header
{

    [TestFixture]
    public class Homepage_ShortHeader_VerifyAnalogDialogue : BaseSetUp
    {
        public Homepage_ShortHeader_VerifyAnalogDialogue() : base() { }
        [Test, Category("Header"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Core Page - Verify that Analog Dialogue link is present in short header and working as expected in EN Locale")]
        [TestCase("cn", TestName = "Core Page - Verify that Analog Dialogue link is present in short header and working as expected in CN Locale")]
        [TestCase("jp", TestName = "Core Page - Verify that Analog Dialogue link is present in short header and working as expected in JP Locale")]
        [TestCase("ru", TestName = "Core Page - Verify that Analog Dialogue link is present in short header and working as expected in RU Locale")]
        public void Homepage_ShortHeader_VerifyAnalogDialogueLink(string locale)
        {
            action.Navigate(driver, Configuration.Env_Url + locale + "/index.html");

            Assert.Multiple(() =>
            {
                test.validateElementIsPresent(driver, Elements.AD_Link);
                action.IClick(driver, element: Elements.AD_Link);
                test.validateScreenByUrl(driver, Configuration.Env_Url + locale + "/analog-dialogue.html");
            });
            
        }

        [Test, Category("Header"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Non Core Page - Verify that Analog Dialogue link is present in short header and working as expected in EN Locale")]
        [TestCase("zh", TestName = "Non Core Page - Verify that Analog Dialogue link is present in short header and working as expected in CN Locale")]
        [TestCase("jp", TestName = "Non Core Page - Verify that Analog Dialogue link is present in short header and working as expected in JP Locale")]
        [TestCase("ru", TestName = "Non Core Page - Verify that Analog Dialogue link is present in short header and working as expected in RU Locale")]
        public void Homepage_ShortHeader_VerifyAnalogDialogueLinkInNonCore(string locale)
        {
            action.Navigate(driver, Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet","corpnt") + "ShoppingCartPage.aspx?locale=" + locale);

            Assert.Multiple(() =>
            {
                test.validateElementIsPresent(driver, Elements.AD_Link);
                action.IClick(driver, element: Elements.AD_Link);
                if (locale.Equals("zh"))
                {
                    test.validateScreenByUrl(driver, Configuration.Env_Url + "cn/analog-dialogue.html");
                }
                else
                {
                    test.validateScreenByUrl(driver, Configuration.Env_Url + locale + "/analog-dialogue.html");
                }
            });

        }

    }

}
