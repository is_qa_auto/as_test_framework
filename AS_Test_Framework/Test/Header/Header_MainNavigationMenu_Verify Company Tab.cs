﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System;
using System.Threading;
using OpenQA.Selenium;
using System.Linq;

namespace AS_Test_Framework.Header
{

    [TestFixture]
    public class Homepage_MainNavigationMenu_VerifyCompanyTab : BaseSetUp
    {
        public Homepage_MainNavigationMenu_VerifyCompanyTab() : base() { }

        //--- URLs ---//
        string corporateSocialResponsibility_page_url = "/corporate-social-responsibility.html";
        string corporateSocialResponsibility_subcategory_page_url_format = "/corporate-social-responsibility/our-values.html";

        /*********Company Tab*********/
        [Test, Category("Header"), Category("Core"), Retry(2)]
        [TestCase("en", "About ADI", "Corporate Social Responsibility", "Investor Relations", TestName = "Core - Verify that Company tab in Main Navigation Menu is present and working as expected for EN Locale")]
        [TestCase("cn", "关于ADI", "企业社会责任", "投资者关系", TestName = "Core - Verify that Company tab in Main Navigation Menu is present and working as expected for CN Locale")]
        [TestCase("jp", "ADIについて", "企業の社会的責任", "投資家向け情報／IR", TestName = "Core - Verify that Company tab in Main Navigation Menu is present and working as expected for JP Locale")]
        [TestCase("ru", "О компании Analog Devices", "Корпоративная социальная ответственность", "Информация для инвесторов", TestName = "Core - Verify that Company tab in Main Navigation Menu is present and working as expected for RU Locale")]
        public void Homepage_VerifyCompanyTab1(string Locale, string AboutADI_Trans, string CorpSocial_Trans, string IR_Trans)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/index.html");
            test.validateElementIsPresent(driver, Elements.MainNavigation_ApplicationsTab);
            action.IMouseOverTo(driver, Elements.MainNavigation_Company);
            Console.WriteLine(util.GetCssValue(driver, Elements.MainNavigation_Company, "background-color"));
            test.validateColorIsCorrect(driver, util.GetCssValue(driver, Elements.MainNavigation_Company, "background-color"), "#0067b9");
            action.IClick(driver, Elements.MainNavigation_Company);
            test.validateColorIsCorrect(driver, util.GetCssValue(driver, Elements.MainNavigation_Company, "background-color"), "#0067b9");
            test.validateElementIsPresent(driver, Elements.MainNavigation_CompanyTab_Expanded);

            test.validateStringIsCorrect(driver, By.CssSelector("div[class='company menu content expanded'] * section[class='company__section'] * div[class='company__nav__parent']:nth-child(1)>div[class='header3']>a"), AboutADI_Trans);
            test.validateElementIsPresent(driver, By.CssSelector("div[class='company menu content expanded'] * section[class='company__section'] * div[class='company__nav__parent']:nth-child(1)>ul>li>a"));

            test.validateStringIsCorrect(driver, Elements.MainNavigation_CompanyTab_CorporateSocialResponsibility_Link, CorpSocial_Trans);
            test.validateElementIsPresent(driver, By.CssSelector("div[class='company menu content expanded'] * section[class='company__section'] * div[class='company__nav__parent']:nth-child(2)>ul>li>a"));

            test.validateStringIsCorrect(driver, By.CssSelector("div[class='company menu content expanded'] * section[class='company__section'] * div[class='company__nav__parent']:nth-child(3)>div[class='header3']>a"), IR_Trans);
            test.validateElementIsPresent(driver, By.CssSelector("div[class='company menu content expanded'] * section[class='company__section'] * div[class='company__nav__parent']:nth-child(3)>ul>li>a"));

            action.IClick(driver, Elements.MainNavigation_Company);
            test.validateElementIsNotPresent(driver, Elements.MainNavigation_CompanyTab_Expanded);

            action.IClick(driver, Elements.MainNavigation_Company);
            action.IClick(driver, Elements.MainNavigation_ApplicationsTab);
            test.validateElementIsNotPresent(driver, Elements.MainNavigation_CompanyTab_Expanded);
        }

        [Test, Category("Header"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Core - Verify Company tab behavior when clicking a link/other tabs for EN Locale", Category = "Smoke_ADIWeb")]
        [TestCase("cn", TestName = "Core - Verify Company tab behavior when clicking a link/other tabs for CN Locale", Category = "Smoke_ADIWeb")]
        [TestCase("jp", TestName = "Core - Verify Company tab behavior when clicking a link/other tabs for JP Locale", Category = "Smoke_ADIWeb")]
        [TestCase("ru", TestName = "Core - Verify Company tab behavior when clicking a link/other tabs for RU Locale")]
        public void Homepage_VerifyCompanyTab2(string Locale)
        {
            string initial_steps = "1. Go to " + Configuration.Env_Url + Locale + "/index.html";
            string scenario = "";

            action.INavigate(driver, Configuration.Env_Url + Locale + "/index.html");
            action.IClick(driver, Elements.MainNavigation_Company);
            string LinkName = util.GetText(driver, By.CssSelector("div[class='company menu content expanded'] * section[class='company__section'] * div[class='company__nav__parent']:nth-child(1)>div[class='header3']>a"));
            action.IClick(driver, By.CssSelector("div[class='company menu content expanded'] * section[class='company__section'] * div[class='company__nav__parent']:nth-child(1)>div[class='header3']>a"));

            scenario = "Verify that company tab is still highlighted (blue) when clicking a link in company flyout";
            test.validateColorIsCorrectv2(driver, util.GetCssValue(driver, Elements.MainNavigation_Company, "background-color"), "#0067b9", scenario, initial_steps + "<br>2. Click company tab in main menu navigation<br>3. Click \"" + LinkName + "\" link", Elements.MainNavigation_Company);

            action.IClick(driver, Elements.MainNavigation_ApplicationsTab);
            scenario = "Verify that company tab is still highlighted (blue) when clicking a new tab in main menu navigation";
            test.validateColorIsCorrectv2(driver, util.GetCssValue(driver, Elements.MainNavigation_Company, "background-color"), "#0067b9", scenario, initial_steps + "<br>2. Click company tab in main menu navigation<br>3. Click \"" + LinkName + "\" link<br>4. Click Applications tab", Elements.MainNavigation_Company);


            action.IClick(driver, Elements.MainNavigation_Company);

            //----- Header and Footer TestPlan > Test Case Title: Verify the COMPANY tab - Corporate Social Responsibility page (IQ-9251/AL-16141) -----//
            string current_url = driver.Url;

            //--- Action: Click the Corporate Social Responsibility label ---//
            action.IOpenLinkInNewTab(driver, Elements.MainNavigation_CompanyTab_CorporateSocialResponsibility_Link);
            //--- Expected Result: The System displays the Corporate Social Responsibility page ---//
            scenario = "Verify that user will be redirected to correct page when clicking corporate social responsibility link";
            test.validateScreenByUrlv2(driver, Configuration.Env_Url + Locale + "/company" + corporateSocialResponsibility_page_url, scenario, initial_steps + "<br>2. Click Company Tab<br>3. Click Corporate Social Responsibility link");

            scenario = "Verify that company tab is still highlighted (blue) after clicking corporate social responsibility link";
            test.validateColorIsCorrectv2(driver, util.GetCssValue(driver, Elements.MainNavigation_Company, "background-color"), "#0067b9", scenario, initial_steps + "<br>2. Click Company Tab<br>3. Click Corporate Social Responsibility link", Elements.MainNavigation_Company);
            //action.IClick(driver, Elements.MainNavigation_ApplicationsTab);
            //test.validateColorIsCorrect(driver, util.GetCssValue(driver, Elements.MainNavigation_Company, "background-color"), "#0067b9");

            if (!util.CheckElement(driver, Elements.MainNavigation_CompanyTab_Expanded, 1))
            {
                //--- Action: Click the COMPANY tab ---//
                action.IClick(driver, Elements.MainNavigation_Company);
            }

            if (util.CheckElement(driver, Elements.MainNavigation_CompanyTab_CorporateSocialResponsibility_Subcategory_Links, 1))
            {
                LinkName = util.GetText(driver, Elements.MainNavigation_CompanyTab_CorporateSocialResponsibility_Subcategory_Links);
                //--- Action: Click the Corporate Social Responsibility - Subcategory label ---//
                action.IOpenLinkInNewTab(driver, Elements.MainNavigation_CompanyTab_CorporateSocialResponsibility_Subcategory_Links);

                //--- Expected Result: The System displays the Corporate Social Responsibility - Subcategory page (IQ-9251/AL-16141) ---//
                scenario = "Verify that user will be redirected to correct page when clicking any sub-category link under the corporate social responsibility";
                test.validateScreenByUrlv2(driver, Configuration.Env_Url + Locale + "/company" + corporateSocialResponsibility_subcategory_page_url_format, scenario, initial_steps + "<br>2. Click Company Tab<br>3. Click \"" + LinkName + "\" under the Corporate Social Responsibility section");
            }
        }
    }
}