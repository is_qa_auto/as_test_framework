﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System;
using System.Threading;
using OpenQA.Selenium;
using System.Linq;

namespace AS_Test_Framework.Header
{

    [TestFixture]
    public class Homepage_MainNavigationMenu_VerifyProductsTab : BaseSetUp
    {
        public Homepage_MainNavigationMenu_VerifyProductsTab() : base() { }

        /*********Products Tab**********/
        [Test, Category("Header"), Category("Core")]
        [TestCase("en", TestName = "Core - Verify that Products tab in Main Navigation Menu is present and working as expected for EN Locale", Category = "Smoke_ADIWeb")]
        [TestCase("cn", TestName = "Core - Verify that Products tab in Main Navigation Menu is present and working as expected for CN Locale", Category = "Smoke_ADIWeb")]
        [TestCase("jp", TestName = "Core - Verify that Products tab in Main Navigation Menu is present and working as expected for JP Locale", Category = "Smoke_ADIWeb")]
        [TestCase("ru", TestName = "Core - Verify that Products tab in Main Navigation Menu is present and working as expected for RU Locale")]
        public void Homepage_VerifyProductsTab(string Locale)
        {
            string initial_steps = "1. Go to " + Configuration.Env_Url + Locale + "/index.html";
            string scenario = "";

            action.INavigate(driver, Configuration.Env_Url + Locale + "/index.html");
            scenario = "Verify that Products tab is present in Main Menu Navigation";
            test.validateElementIsPresentv2(driver, Elements.MainNavigation_ProductsTab, scenario, initial_steps);

            action.IMouseOverTo(driver, Elements.MainNavigation_ProductsTab);
            scenario = "Verify that Products Tab will change its color when hovered";
            test.validateColorIsCorrectv2(driver, util.GetCssValue(driver, Elements.MainNavigation_ProductsTab, "background-color"), "#009fbd", scenario, initial_steps + "<br>2. Mouse hover to Products Tab in main menu navigation", Elements.MainNavigation_ProductsTab);
            
            action.IClick(driver, Elements.MainNavigation_ProductsTab);
            scenario = "Verify that products tab will expand when clicked.";
            test.validateElementIsPresentv2(driver, By.CssSelector("div[class='products menu content expanded']"), scenario, initial_steps + "<br>2. Click on Products Tab");

            scenario = "Verify that Previously visited category section is present in Products category fly out";
            test.validateElementIsPresentv2(driver, Elements.Products_Tab_Prev_Visited_Cat_Section, scenario, initial_steps + "<br>2. Click on Products Tab");

            scenario = "Verify that new product section is present in Products category fly out";
            test.validateElementIsPresentv2(driver, Elements.Products_Tab_New_Prod_Section, scenario, initial_steps + "<br>2. Click on Products Tab");

            scenario = "Verify that search section is present in Products category fly out";
            test.validateElementIsPresentv2(driver, Elements.Products_Tab_Search, scenario, initial_steps + "<br>2. Click on Products Tab");

            scenario = "Verify that view all new products button is present in Products category fly out";
            test.validateElementIsPresentv2(driver, Elements.Products_Tab_ViewAll_Btn, scenario, initial_steps + "<br>2. Click on Products Tab");

            scenario = "Verify that new products link is present in the left side of the product category fly out";
            test.validateElementIsPresentv2(driver, By.CssSelector("ul[class='categories']>li>a>span"), scenario, initial_steps + "<br>2. Click on Products Tab");

            action.IClick(driver, Elements.MainNavigation_ProductsTab);
            scenario = "Verify that product category fly out will collapsed when user clicks on it again";
            test.validateElementIsNotPresentv2(driver, By.CssSelector("div[class='products menu content expanded']"), scenario, initial_steps + "<br>2. Click on Products Tab<br>3. Click on products tab again");

            action.IClick(driver, Elements.MainNavigation_ProductsTab);
            action.IClick(driver, Elements.MainNavigation_Company);
            scenario = "Verify that product category fly out will collapsed when clicking other main menu tab";
            test.validateElementIsNotPresentv2(driver, By.CssSelector("div[class='products menu content expanded']"), scenario, initial_steps + "<br>2. Click on Products Tab<br>3. Click on company tab");

            action.IClick(driver, Elements.MainNavigation_ProductsTab);
            scenario = "Verify that list of categories is present in the left side of product category fly out";
            test.validateElementIsPresentv2(driver, By.CssSelector("ul[class='categories']"), scenario, initial_steps + "<br>2. Click on Products Tab");

            //test.validateElementIsPresent(driver, By.CssSelector("div[class='outline-card new-products']>ul"), scenario, initial_steps + );
            string categoryName = util.GetText(driver, By.CssSelector("div[class='products menu content expanded'] * ul>li>a"));
            action.IClick(driver, By.CssSelector("div[class='products menu content expanded'] * ul>li>a"));
            scenario = "Verify that Sub-Category will be displayed when user clicks on any category listed on the left side of product category fly-out";
            test.validateElementIsPresentv2(driver, By.CssSelector("div[class='results four column']"), scenario, initial_steps + "<br>2. Click on Products Tab<br3. Click category - \"" + categoryName + "\"");

            action.IClick(driver, By.CssSelector("ul[class='categories']>li>a>span"));
            scenario = "Verify that user will be redirected to New Product landing page when clicking new product links in product category fly-out";
            test.validateScreenByUrlv2(driver, Configuration.Env_Url + Locale + "/products/landing-pages/new-products-listing.html", scenario, initial_steps + "<br>2. Click on Products Tab<br3. Click new products link");
        }

        /*********Removing as per ticket ALA-13937 this is a content related functionality*********/
        //[Test, Category("Header"), Category("Core"), Retry(2)]
        //[TestCase("en", "Industrial Ethernet", TestName = "Core - Verify Sub Category column for Category that doesnt have a Sub Category for EN Locale")]
        //[TestCase("cn", "工业以太网", TestName = "Core - Verify Sub Category column for Category that doesnt have a Sub Category for CN Locale")]
        //[TestCase("jp", "工業用 イーサネット （Ethernet）", TestName = "Core - Verify Sub Category column for Category that doesnt have a Sub Category for JP Locale")]
        //[TestCase("ru", "Промышленный Ethernet", TestName = "Core - Verify Sub Category column for Category that doesnt have a Sub Category for RU Locale")]
        //public void Homepage_VerifyDisplayForCatThatDoesntHaveASubCat(string Locale, string IndustrialEthernet)
        //{
        //    action.Navigate(driver, Configuration.Env_Url + Locale + "/index.html");
        //    action.IClick(driver, Elements.MainNavigation_ProductsTab);
        //    action.IClick(driver, By.CssSelector("a[data-category='" + IndustrialEthernet + "']"));
        //    Assert.Multiple(() =>
        //    {
        //        //no longer applicable -- test.validateElementIsPresent(driver, By.CssSelector("p[class='no-children']"));
        //        //no longer applicable -- test.validateElementIsPresent(driver, By.CssSelector("div[class='featured']>a[class='view-all']"));
        //        action.IClick(driver, By.CssSelector("p[class='no-children']>a"));
        //        //no longer applicable -- test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/products/industrial-ethernet.html");
        //    });
        //}

        [Test, Category("Header"), Category("Core")]
        [TestCase("en", TestName = "Core - Verify the Product Categories Search under the PRODUCTS Tab for EN Locale")]
        [TestCase("cn", TestName = "Core - Verify the Product Categories Search under the PRODUCTS Tab for CN Locale")]
        [TestCase("jp", TestName = "Core - Verify the Product Categories Search under the PRODUCTS Tab for JP Locale")]
        [TestCase("ru", TestName = "Core - Verify the Product Categories Search under the PRODUCTS Tab for RU Locale")]
        public void Homepage_VerifySearchinProductsTab(string Locale)
        {
            string Inv_Search_Kw = "Invalid Category";
            action.Navigate(driver, Configuration.Env_Url + Locale + "/index.html");
            action.IClick(driver, Elements.MainNavigation_ProductsTab);
            action.IType(driver, Elements.Products_Tab_Search, Inv_Search_Kw);
            //search in big category search box
            test.validateElementIsPresent(driver, By.CssSelector("div[class='no-results']>h2"));
            test.validateElementIsPresent(driver, By.CssSelector("div[class='no-results']>p>a"));
            action.IDeleteValueOnFields(driver, By.CssSelector("div[class='search large active'] * input"));
            action.IType(driver, By.CssSelector("div[class*='search large'] * input"), "!#$%");
            test.validateElementIsPresent(driver, By.CssSelector("div[class='no-results']>h2"));
            action.IDeleteValueOnFields(driver, By.CssSelector("div[class='search large active'] * input"));
            action.IType(driver, By.CssSelector("div[class*='search large'] * input"), "a");
            test.validateElementIsNotPresent(driver, By.CssSelector("div[class*='results']"));
            action.IDeleteValueOnFields(driver, By.CssSelector("div[class='search large'] * input"));
            action.IType(driver, By.CssSelector("div[class*='search large'] * input"), "amplifiers");
            test.validateElementIsPresent(driver, By.CssSelector("div[class*='results']"));
             //search in category search in left nav
            action.IClick(driver, By.CssSelector("ul[class='categories']>li[class='category']>a"));
            action.IType(driver, Elements.Products_Tab_SmallSearch, "a");
            test.validateElementIsNotPresent(driver, By.CssSelector("div[class*='search large'] * input"));
            action.IDeleteValueOnFields(driver, Elements.Products_Tab_SmallSearch);
            action.IType(driver, Elements.Products_Tab_SmallSearch, "am");
            test.validateElementIsPresent(driver, By.CssSelector("div[class*='results']"));
            action.IClick(driver, By.CssSelector("ul[class='categories']>li[class='category']>a"));
            action.IType(driver, Elements.Products_Tab_SmallSearch, "!@");
            test.validateElementIsPresent(driver, By.CssSelector("div[class='no-results']>h2"));
            test.validateElementIsPresent(driver, By.CssSelector("div[class='no-results']>p"));

        }

        [Test, Category("Header"), Category("Core"), Retry(2)]
        [TestCase("en", "Parametric Table", TestName = "Core - Verify Parametric table Help Icon and Tool tip in every Products category for EN Locale")]
        [TestCase("cn", "产品选型表", TestName = "Core - Verify Parametric table Help Icon and Tool tip in every Products category for CN Locale")]
        [TestCase("jp", "製品セレクション・テーブル", TestName = "Core - Verify Parametric table Help Icon and Tool tip in every Products category for JP Locale")]
        [TestCase("ru", "Параметрическая таблица", TestName = "Core - Verify Parametric table Help Icon and Tool tip in every Products category for RU Locale")]
        public void Homepage_VerifyPSTHelperIcon(string Locale, string PSTlabel)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/index.html");
            action.IClick(driver, Elements.MainNavigation_ProductsTab);
            action.IClick(driver, By.CssSelector("div[class='products menu content expanded'] * ul>li>a"));
            Thread.Sleep(5000);
            Assert.Multiple(() =>
            {
                test.validateElementIsPresent(driver, By.CssSelector("div[class='pst-help right']"));
                test.validateStringIsCorrect(driver, By.CssSelector("div[class='pst-help right']>span[class='header']"), PSTlabel);
                action.IClick(driver, By.CssSelector("div[class='category first-level parent'] * a[data-category='PST']"));
                test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + "/parametricsearch");
            });
        }

        [Test, Category("Header"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "NonCore - Verify menu nav should be closed upon clicking outside the flyout menu or anywhere in Non-core pages for EN Locale")]
        [TestCase("zh", TestName = "NonCore - Verify menu nav should be closed upon clicking outside the flyout menu or anywhere in Non-core pages for CN Locale")]
        [TestCase("jp", TestName = "NonCore - Verify menu nav should be closed upon clicking outside the flyout menu or anywhere in Non-core pages for JP Locale")]
        [TestCase("ru", TestName = "NonCore - Verify menu nav should be closed upon clicking outside the flyout menu or anywhere in Non-core pages for RU Locale")]
        public void Homepage_VerifyMegaMenuNavInNonCore(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet","corpnt") +  "/ShoppingCartPage.aspx?locale=" + Locale);
            action.IClick(driver, Elements.MainNavigation_ProductsTab);
            Thread.Sleep(5000);
            action.IClick(driver, Elements.GlobalSearchTextField);
            test.validateElementIsNotPresent(driver, By.CssSelector("div[class='products menu content expanded']"));
            action.IClick(driver, Elements.MainNavigation_ProductsTab);
            action.IClick(driver, By.CssSelector("div[class='products menu content expanded'] * ul[class='categories']>li>a"));
            action.IClick(driver, By.CssSelector("div[class='main']>div[class='results four column'] a:nth-child(2)"));
            test.validateColorIsCorrect(driver, util.GetCssValue(driver, Elements.MainNavigation_ProductsTab, "background-color"), "#009fbd");
        }
    }

}
