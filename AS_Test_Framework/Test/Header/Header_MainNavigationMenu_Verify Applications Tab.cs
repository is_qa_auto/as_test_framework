﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System;
using System.Threading;
using OpenQA.Selenium;
using System.Linq;

namespace AS_Test_Framework.Header
{

    [TestFixture]
    public class Homepage_MainNavigationMenu_VerifyApplicationsTab : BaseSetUp
    {
        public Homepage_MainNavigationMenu_VerifyApplicationsTab() : base() { }

        /*********Applications Tab*********/
        [Test, Category("Header"), Category("Core")]
        [TestCase("en", "Technology Solutions", TestName = "Core - Verify that Applications tab in Main Navigation Menu is present and working as expected for EN Locale")]
        [TestCase("cn", "技术", TestName = "Core - Verify that Applications tab in Main Navigation Menu is present and working as expected for CN Locale")]
        [TestCase("jp", "テクノロジー", TestName = "Core - Verify that Applications tab in Main Navigation Menu is present and working as expected for JP Locale")]
        [TestCase("ru", "Технологии", TestName = "Core - Verify that Applications tab in Main Navigation Menu is present and working as expected for RU Locale")]
        public void Homepage_VerifyApplicationsTab(string Locale, string Tehcno_Soln_Link)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/index.html");
            
            test.validateElementIsPresent(driver, Elements.MainNavigation_ApplicationsTab);
            action.IMouseOverTo(driver, Elements.MainNavigation_ApplicationsTab);
            test.validateColorIsCorrect(driver, util.GetCssValue(driver, Elements.MainNavigation_ApplicationsTab, "background-color"), "#1d4056");
            action.IClick(driver, Elements.MainNavigation_ApplicationsTab);
            test.validateElementIsPresent(driver, By.CssSelector("div[class='applications menu content expanded']"));
            test.validateColorIsCorrect(driver, util.GetCssValue(driver, Elements.MainNavigation_ApplicationsTab, "background-color"), "#1d4056");

            /*****Removed. <LI> is not properly arranged*****/
            //if (Locale.Equals("en"))
            //{ 
            //    int MarketSubCount = util.GetCount(driver, By.CssSelector("div[class='applications menu content expanded'] * div[class='row']:nth-child(1)>div[class='applications__nav__parent col-md-12']>ul>li"));
            //    object[] MarketSub = new object[MarketSubCount];
            //    for (int x = 1, y = 0; x <= MarketSubCount; x++, y++)
            //    {
            //        MarketSub[y] = util.GetText(driver, By.CssSelector("div[class='applications menu content expanded'] * div[class='row']:nth-child(1)>div[class='applications__nav__parent col-md-12']>ul>li:nth-child(" + x + ")>a")).Trim();
            //        Console.WriteLine(MarketSub[y]);
            //    }
            //    test.validateStringIfInAscenading(MarketSub);

            //    int TechSubCount = util.GetCount(driver, By.CssSelector("div[class='applications menu content expanded'] * div[class='row']:nth-child(2)>div[class='applications__nav__parent col-md-12']>ul>li"));
            //    object[] TechSub = new object[TechSubCount];
            //    for (int x = 1, y = 0; x <= TechSubCount; x++, y++)
            //    {
            //        TechSub[y] = util.GetText(driver, By.CssSelector("div[class='applications menu content expanded'] * div[class='row']:nth-child(2)>div[class='applications__nav__parent col-md-12']>ul>li:nth-child(" + x + ")>a")).Trim();
            //        Console.WriteLine(TechSub[y]);
            //    }
            //    test.validateStringIfInAscenading(TechSub);
            //}
            action.IClick(driver, Elements.MainNavigation_Company);
            test.validateElementIsNotPresent(driver, By.CssSelector("div[class='applications menu content expanded']"));

        }

        [Test, Category("Header"), Category("Core")]
        [TestCase("en", TestName = "Core - Verify Application tab behavior when clicking a link/other tabs for EN Locale")]
        [TestCase("cn", TestName = "Core - Verify Application tab behavior when clicking a link/other tabs for CN Locale")]
        [TestCase("jp", TestName = "Core - Verify Application tab behavior when clicking a link/other tabs for JP Locale")]
        [TestCase("ru", TestName = "Core - Verify Application tab behavior when clicking a link/other tabs for RU Locale")]
        public void Homepage_VerifyApplicationTab2(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/index.html");
            action.IClick(driver, Elements.MainNavigation_ApplicationsTab);
            /***Market Link****/
            action.IClick(driver, By.CssSelector("div[class='applications menu content expanded'] * div[class='row']:nth-child(1)>div[class='applications__nav__parent col-md-12']>div[class='header3']>a"));
            test.validateColorIsCorrect(driver, util.GetCssValue(driver, Elements.MainNavigation_ApplicationsTab, "background-color"), "#1e4056");
            action.IClick(driver, Elements.MainNavigation_DesignCenterTab);
            test.validateColorIsCorrect(driver, util.GetCssValue(driver, Elements.MainNavigation_ApplicationsTab, "background-color"), "#1e4056");
            action.IClick(driver, Elements.MainNavigation_ApplicationsTab);
            action.IClick(driver, By.CssSelector("div[class='applications menu content expanded'] * div[class='row']:nth-child(1)>div[class='applications__nav__parent col-md-12']>ul>li>a"));
            test.validateColorIsCorrect(driver, util.GetCssValue(driver, Elements.MainNavigation_ApplicationsTab, "background-color"), "#1e4056");

            /***Technology Solutions Link****/
            action.IClick(driver, Elements.MainNavigation_ApplicationsTab);
            action.IClick(driver, By.CssSelector("div[class='applications menu content expanded'] * div[class='row']:nth-child(2)>div[class='applications__nav__parent col-md-12']>div[class='header3']>a"));
            test.validateColorIsCorrect(driver, util.GetCssValue(driver, Elements.MainNavigation_ApplicationsTab, "background-color"), "#1e4056");
            action.IClick(driver, Elements.MainNavigation_DesignCenterTab);
            test.validateColorIsCorrect(driver, util.GetCssValue(driver, Elements.MainNavigation_ApplicationsTab, "background-color"), "#1e4056");
            action.IClick(driver, Elements.MainNavigation_ApplicationsTab);
            action.IClick(driver, By.CssSelector("div[class='applications menu content expanded'] * div[class='row']:nth-child(2)>div[class='applications__nav__parent col-md-12']>ul>li>a"));
            test.validateColorIsCorrect(driver, util.GetCssValue(driver, Elements.MainNavigation_ApplicationsTab, "background-color"), "#1e4056");

        }
    }

}
