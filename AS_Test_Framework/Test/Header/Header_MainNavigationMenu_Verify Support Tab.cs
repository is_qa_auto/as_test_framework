﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System;
using System.Threading;
using OpenQA.Selenium;
using System.Linq;

namespace AS_Test_Framework.Header
{

    [TestFixture]
    public class Homepage_MainNavigationMenu_VerifySupportTab : BaseSetUp
    {
        public Homepage_MainNavigationMenu_VerifySupportTab() : base() { }

        /*********Support Tab*********/
        [Test, Category("Header"), Category("Core"), Retry(2)]
        [TestCase("en", "Sales", "Customer Service", TestName = "Core - Verify that Support tab in Main Navigation Menu is present and working as expected for EN Locale")]
        [TestCase("cn", "销售", "客户服务", TestName = "Core - Verify that Support tab in Main Navigation Menu is present and working as expected for CN Locale")]
        [TestCase("jp", "購入", "カスタマーサービス", TestName = "Core - Verify that Support tab in Main Navigation Menu is present and working as expected for JP Locale")]
        [TestCase("ru", "Офисы продаж", "Служба поддержки заказчиков", TestName = "Core - Verify that Support tab in Main Navigation Menu is present and working as expected for RU Locale")]
        public void Homepage_VerifySupportTab(string Locale, string Sales_Section, string Customer_Service_Section)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/index.html");
            test.validateElementIsPresent(driver, Elements.MainNavigation_SupportTab);
            action.IMouseOverTo(driver, Elements.MainNavigation_SupportTab);
            test.validateColorIsCorrect(driver, util.GetCssValue(driver, Elements.MainNavigation_SupportTab, "background-color"), "#25a449");
            action.IClick(driver, Elements.MainNavigation_SupportTab);
            test.validateColorIsCorrect(driver, util.GetCssValue(driver, Elements.MainNavigation_SupportTab, "background-color"), "#25a449");
            test.validateElementIsPresent(driver, By.CssSelector("div[class='support menu content expanded']"));
            action.IClick(driver, Elements.MainNavigation_ProductsTab);
            test.validateElementIsNotPresent(driver, By.CssSelector("div[class='support menu content expanded']"));
        }
    }
}
