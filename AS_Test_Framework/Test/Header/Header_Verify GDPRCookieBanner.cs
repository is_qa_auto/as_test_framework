﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System;
using System.Threading;
using OpenQA.Selenium;

namespace AS_Test_Framework.Header
{

    [TestFixture]
    public class Homepage_VerifyGDPRCookieBanner : BaseSetUp
    {
        public Homepage_VerifyGDPRCookieBanner() : base() { }

        [Test, Category("Header"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that GDPR Cookie Banner is present and working as expected for first time user in EN Locale")]
        [TestCase("cn", TestName = "Verify that GDPR Cookie Banner is present and working as expected for first time user in CN Locale")]
        [TestCase("jp", TestName = "Verify that GDPR Cookie Banner is present and working as expected for first time user in JP Locale")]
        [TestCase("ru", TestName = "Verify that GDPR Cookie Banner is present and working as expected for first time user in RU Locale")]
        public void Homepage_VerifyGDPRLinksAndButtons(string Locale)
        {
            driver.Navigate().GoToUrl(Configuration.Env_Url + Locale + "/index.html");
            Assert.Multiple(() =>
            {
                test.validateElementIsPresent(driver, Elements.GDPR_Banner_Success_Btn);
                test.validateElementIsPresent(driver, Elements.GDPR_Cookie_Details_Link);
                test.validateElementIsPresent(driver, Elements.GDPR_Privacy_Pol_Link);
                action.IClick(driver, Elements.GDPR_Cookie_Details_Link);
                test.validateElementIsPresent(driver, By.CssSelector("div[id='cookie-consent-container'] * div[class='long-description']"));
                test.validateElementIsPresent(driver, Elements.GDPR_Privacy_Decline_Link);
                action.IClick(driver, By.CssSelector("div[id='cookie-consent-container'] * div[class='long-description']>a[class='btn btn-success']"));
                test.validateElementIsNotPresent(driver, By.CssSelector("div[id='cookie-consent-container']>div[class='modal-dialog']"));
            });
        }


        [Test, Category("Header"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify non-core application that requires log in")]
        public void Homepage_VerifyGDPRInNonCore(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/about-adi/alliances/about-members-lounge.html");
            action.IClick(driver, By.CssSelector("div[class='row campaign fca-row one-columnspot top-spacing-32 bottom-spacing-32 bg-grey'] * div[class='clearfix rte']>p:nth-child(3)>a"));
            action.ILogin(driver, "aries.sorosoro@analog.com", "Test_1234");
            Assert.Multiple(() =>
            {
                test.validateElementIsNotPresent(driver, By.CssSelector("div[id='cookie-consent-container']>div[class='modal-dialog']"));
            });
        }

    }

}
