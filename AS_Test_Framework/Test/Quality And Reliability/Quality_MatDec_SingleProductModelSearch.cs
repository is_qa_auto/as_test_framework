﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Threading;
using System;

namespace AS_Test_Framework.QualityAndReliability
{
    [TestFixture]
    public class Quality_MatDec_SingleProductModelSearch : BaseSetUp
    {
        public Quality_MatDec_SingleProductModelSearch() : base() { }

        string Url = "/about-adi/quality-reliability/material-declarations.html";

        [Test, Category("Quality And Reliability"), Category("NonCore")]
        [TestCase("en", TestName = "Verify the Single Product Models Search when user search using invalid inputs (blank and less than 4 char)")]
        public void QnR_MatDec_VerifyInvalidInputs(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + Url);
            driver.SwitchTo().Frame(driver.FindElement(By.CssSelector("strong>iframe")));
            action.IClick(driver, Elements.QnR_MatDec_ModelSearch_Single_RadioBtn);
            test.validateElementIsNotPresent(driver, Elements.QnR_MatDec_ModelSearch_Single_Error);
            action.IClick(driver, Elements.QnR_MatDec_ModelSearch_Search_Btn);
            test.validateStringIsCorrect(driver, Elements.QnR_MatDec_ModelSearch_Single_Error, "A minimum of four characters is required.");

            action.IType(driver, Elements.QnR_MatDec_ModelSearch_Multiple_TextArea, "ADD");
            action.IClick(driver, Elements.QnR_MatDec_ModelSearch_Search_Btn);
            test.validateStringIsCorrect(driver, Elements.QnR_MatDec_ModelSearch_Single_Error, "A minimum of four characters is required.");
        }

        [Test, Category("Quality And Reliability"), Category("NonCore")]
        [TestCase("en", TestName = "Verify the Single Product Models Search when user search using valid Product number")]
        public void QnR_MatDec_VerifyValidInputs(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + Url);
            driver.SwitchTo().Frame(driver.FindElement(By.CssSelector("strong>iframe")));
            string input = "AD7690";
            action.IClick(driver, Elements.QnR_MatDec_ModelSearch_Single_RadioBtn);
            action.IType(driver, Elements.QnR_MatDec_ModelSearch_Single_TextField, input);
            action.IClick(driver, Elements.QnR_MatDec_ModelSearch_Search_Btn);
            Thread.Sleep(2000);
            test.validateScreenByUrl(driver, Configuration.Env_Url.Replace("www", "quality").Replace("cldnet", "corpnt") + "default.aspx?locale=" + Locale);
            test.validateStringIsCorrect(driver, By.CssSelector("div[class='container page-title '] h1"), "Material Declaration Search Results");
            test.validateElementIsPresent(driver, Elements.QnR_MatDec_ModelSearch_Result_Table);
        }

        [Test, Category("Quality And Reliability"), Category("NonCore")]
        [TestCase("en", TestName = "Verify the Single Product Models Search when user search using keyword that has no results")]
        public void QnR_MatDec_VerifyNoResultFound(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + Url);
            driver.SwitchTo().Frame(driver.FindElement(By.CssSelector("strong>iframe")));
            action.IClick(driver, Elements.QnR_MatDec_ModelSearch_Single_RadioBtn);
            action.IType(driver, Elements.QnR_MatDec_ModelSearch_Single_TextField, "AD7705ZZZZZ");
            action.IClick(driver, Elements.QnR_MatDec_ModelSearch_Search_Btn);
            Thread.Sleep(2000);
            test.validateString(driver, "No Result | Analog Devices", driver.Title);
            test.validateStringIsCorrect(driver, Elements.QnR_MatDec_ModelSearch_NoResult_Message, "No results were found for your search term \"AD7705ZZZZZ\"");

        }
    }
}