﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;

namespace AS_Test_Framework.QualityAndReliability
{
    [TestFixture]
    public class Quality_Reliability_ReliabilityProgram : BaseSetUp
    {
        public Quality_Reliability_ReliabilityProgram() : base() { }

        string Url = "/about-adi/quality-reliability/reliability-program.html";

        [Test, Category("Quality And Reliability"), Category("NonCore")]
        [TestCase("en", "Reliability Program", TestName = "Verify that user can navigate to Reliability program page via \"About ADI\" from the header in EN Locale")]
        [TestCase("cn", "可靠性项目", TestName = "Verify that user can navigate to Reliability program page via \"About ADI\" from the header in CN Locale")]
        [TestCase("jp", "信頼性プログラム", TestName = "Verify that user can to Reliability program page navigate via \"About ADI\" from the header in JP Locale")]
        [TestCase("ru", "Программа надежности", TestName = "Verify that user can navigate to Reliability program page via \"About ADI\" from the header in RU Locale")]
        public void QnR_ReliabilityProgram_VerifyNavigationToReliabilityProgramPage(string Locale, string ActiveLeftNav)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/about-adi.html");
            action.IClick(driver, By.CssSelector("a[name*='quality-reliability']"));
            action.IClick(driver, By.CssSelector("a[name*='reliability-program']"));
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + Url);
        }

        [Test, Category("Quality And Reliability"), Category("NonCore")]
        [TestCase("en", "About ADI", "Quality & Reliability", "Reliability Program", TestName = "Verify that breadcrumbs is present in Reliability Program page for EN Locale")]
        [TestCase("cn", "关于ADI", "质量和可靠性", "可靠性项目", TestName = "Verify that breadcrumbs is present in Reliability Program page for CN Locale")]
        [TestCase("jp", "ADIについて", "品質＆信頼性", "信頼性プログラム", TestName = "Verify that breadcrumbs is present in Reliability Program page for JP Locale")]
        [TestCase("ru", "О компании Analog Devices", "Качество и надежность", "Программа надежности", TestName = "Verify that breadcrumbs is present in Reliability Program page for RU Locale")]
        public void QnR_ReliabilityProgram_VerifyBreadCrumbs1(string Locale, string FirstLevelCat, string SecondLevelCat, string CurrentPage)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + Url);
            test.validateElementIsPresent(driver, Elements.Applications_BreadCrumbs);
            test.validateStringIsCorrect(driver, By.CssSelector("div[name='adi_breadcrumb'] ol>li:nth-child(2)>span>a"), FirstLevelCat);
            test.validateStringIsCorrect(driver, By.CssSelector("div[name='adi_breadcrumb'] ol>li:nth-child(3)>span>a"), SecondLevelCat);
            test.validateStringIsCorrect(driver, By.CssSelector("div[name='adi_breadcrumb'] li[class='active']>span"), CurrentPage);
            action.IClick(driver, By.CssSelector("div[name='adi_breadcrumb'] ol>li:nth-child(2)>span>a"));
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/about-adi.html");
            driver.Navigate().Back();
            action.IClick(driver, By.CssSelector("div[name='adi_breadcrumb'] ol>li:nth-child(3)>span>a"));
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/about-adi/quality-reliability.html");
            driver.Navigate().Back();
            test.validateElementIsPresent(driver, Elements.Applications_BreadCrumbs_Home);
            action.IClick(driver, Elements.Applications_BreadCrumbs_Home);
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/index.html");
        }

        [Test, Category("Quality And Reliability"), Category("NonCore")]
        [TestCase("en", "Reliability Program", TestName = "Verify that Reliability program diagram is present and correct in EN Locale")]
        [TestCase("cn", "可靠性项目", TestName = "Verify that Reliability program diagram is present and correct  in CN Locale")]
        [TestCase("jp", "信頼性プログラム", TestName = "Verify that Reliability program diagram is present and correct in JP Locale")]
        [TestCase("ru", "Программа надежности", TestName = "Verify that Reliability program diagram is present and correct in RU Locale")]
        public void QnR_ReliabilityProgram_VerifyReliabilityProgramDiagram(string Locale, string ActiveLeftNav)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + Url);
            test.validateElementIsPresent(driver, Elements.QnR_AssemblyPackage_ReliabilityBuild);
            test.validateElementIsPresent(driver, Elements.QnR_AssemblyPackage_ReliabilityBuild_List);
            test.validateElementIsPresent(driver, Elements.QnR_AssemblyPackage_ReliabilityProduct);
            test.validateElementIsPresent(driver, Elements.QnR_AssemblyPackage_ReliabilityProduct_List);
            test.validateElementIsPresent(driver, Elements.QnR_AssemblyPackage_ReliabilityValid);
            test.validateElementIsPresent(driver, Elements.QnR_AssemblyPackage_ReliabilityValid_List);
        }
    }
}