﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;

namespace AS_Test_Framework.QualityAndReliability
{
    [TestFixture]
    public class Quality_LandingPage : BaseSetUp
    {
        public Quality_LandingPage() : base() { }

        string Url = "/about-adi/quality-reliability.html";

        [Test, Category("Quality And Reliability"), Category("NonCore")]
        [TestCase("en", "Quality & Reliability", TestName = "Verify left nav is present and all links are working in Quality and Reliability Landing Page for EN Locale")]
        [TestCase("cn", "质量和可靠性", TestName = "Verify left nav is present and all links are working in Quality and Reliability Landing Page for CN Locale")]
        [TestCase("jp", "品質＆信頼性", TestName = "Verify left nav is present and all links are working in Quality and Reliability Landing Page for JP Locale")]
        [TestCase("ru", "Качество и надежность", TestName = "Verify left nav is present and all links are working in Quality and Reliability Landing Page for RU Locale")]
        public void QnR_ReliabilityData_VerifyLandingPageLeftNavigation(string Locale, string ActiveLeftNav)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + Url);
            string[] LeftNavLinks_en = {
                "/about-adi/quality-reliability/material-declarations.html",
                "/about-adi/quality-reliability/quality-certificates.html",
                "/about-adi/quality-reliability/quality-systems.html",
                "/about-adi/quality-reliability/reliability-data.html",
                "/about-adi/quality-reliability/reliability-program.html"
            };

            test.validateElementIsPresent(driver, By.CssSelector("div[name='adi_threecolumn_leftnavigation_section']"));
            test.validateStringIsCorrect(driver, By.CssSelector("div[name='adi_threecolumn_leftnavigation_section']>ul>li[class='active']>a"), ActiveLeftNav);

            for (int x = 1, y = 0; util.CheckElement(driver, By.CssSelector("div[name='adi_threecolumn_leftnavigation_section']>ul>li[class='active']>ul>li:nth-child(" + x + ")"), 5); x++, y++)
            {
                action.IClick(driver, By.CssSelector("div[name='adi_threecolumn_leftnavigation_section']>ul>li[class='active']>ul>li:nth-child(" + x + ")>a"));
                test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + LeftNavLinks_en[y]);
                driver.Navigate().Back();
            }

        }
    }
}