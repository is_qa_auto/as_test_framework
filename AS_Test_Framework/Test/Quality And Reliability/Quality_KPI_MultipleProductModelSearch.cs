﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Threading;
using System;

namespace AS_Test_Framework.QualityAndReliability
{
    [TestFixture]
    public class Quality_KPI_MultipleProductModelSearch : BaseSetUp
    {
        public Quality_KPI_MultipleProductModelSearch() : base() { }

        string Url = "/design-center/packaging-quality-symbols-footprints/package-resources/keypackageinformation.html";

        [Test, Category("Quality And Reliability"), Category("NonCore")]
        [TestCase("en", TestName = "Verify the Multiple Product Models Search when user search using invalid inputs (blank and less than 4 char)")]
        public void QnR_KPI_VerifyInvalidInputs(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + Url);
            driver.SwitchTo().Frame(driver.FindElement(By.CssSelector("div[id='rte-body']>iframe")));
            /****validate search text  guide inside textarea****/
            test.validateStringIsCorrect(driver, Elements.QnR_MatDec_ModelSearch_Multiple_TextArea, "Enter full model part number.\r\nEnter multiple model part numbers on separate lines.");

            /****validate clicking search without keyword****/
            action.IClick(driver, Elements.QnR_MatDec_ModelSearch_Search_Btn);
            test.validateStringIsCorrect(driver, Elements.QnR_MatDec_ModelSearch_Multiple_Error, "A minimum of four characters is required.");
        }


        [Test, Category("Quality And Reliability"), Category("NonCore")]
        [TestCase("en", TestName = "Verify the Multiple Product Models Search when user search using keyword that has no results")]
        public void QnR_KPI_VerifyNoResultFound(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + Url);
            driver.SwitchTo().Frame(driver.FindElement(By.CssSelector("div[id='rte-body']>iframe")));
            action.IType(driver, Elements.QnR_MatDec_ModelSearch_Multiple_TextArea, "AD7705ZZZZZ");
            action.IClick(driver, Elements.QnR_MatDec_ModelSearch_Search_Btn);
            test.validateElementIsNotPresent(driver, Elements.QnR_MatDec_ModelSearch_LTCModal);
            test.validateScreenByUrl(driver, Configuration.Env_Url.Replace("www", "quality").Replace("cldnet", "corpnt") + "KPINoResults.aspx?locale=" + Locale + "&model=AD7705ZZZZZ");
            test.validateString(driver, "No Result | Analog Devices", driver.Title);
            test.validateStringIsCorrect(driver, Elements.QnR_MatDec_ModelSearch_NoResult_Message, "No results were found for your search term \"AD7705ZZZZZ\"");
            test.validateElementIsPresent(driver, Elements.QnR_MatDec_ModelSearch_SERP_RunNew_Btn);
            action.IClick(driver, Elements.QnR_MatDec_ModelSearch_SERP_RunNew_Btn);
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + Url);
            driver.Navigate().Back();
            action.IClick(driver, By.CssSelector("span[id='spnSuggestion'] p>a"));
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/support/technical-support.html");

        }

        [Test, Category("Quality And Reliability"), Category("NonCore")]
        [TestCase("en", TestName = "Verify the Multiple Product Models Search when user search using keyword that return only a single result")]
        public void QnR_KPI_VerifySingleResult(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + Url);
            driver.SwitchTo().Frame(driver.FindElement(By.CssSelector("div[id='rte-body']>iframe")));
            string input = "AD7705BNZ";
            action.IType(driver, Elements.QnR_MatDec_ModelSearch_Multiple_TextArea, input);
            action.IClick(driver, Elements.QnR_MatDec_ModelSearch_Search_Btn);
            test.validateScreenByUrl(driver, Configuration.Env_Url.Replace("www", "quality").Replace("cldnet", "corpnt") + "kpidefault.aspx?locale=" + Locale );
            test.validateElementIsNotPresent(driver, Elements.QnR_MatDec_ModelSearch_LTCModal);
            test.validateElementIsPresent(driver, Elements.QnR_MatDec_ModelSearch_SERP_GoToProduct_Btn);
            test.validateElementIsPresent(driver, Elements.QnR_MatDec_ModelSearch_SERP_MatDec_Link);
            test.validateElementIsPresent(driver, By.CssSelector("span[class='roshLink pull-left']>a:nth-child(2)"));
            action.IClick(driver, Elements.QnR_MatDec_ModelSearch_SERP_GoToProduct_Btn);
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/products/ad7705.html");
            driver.Navigate().Back();
            action.IClick(driver, Elements.QnR_MatDec_ModelSearch_SERP_MatDec_Link);
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + Url);
            driver.Navigate().Back();
            action.IClick(driver, By.CssSelector("span[class='roshLink pull-left']>a:nth-child(2)"));
            test.validateStringInstance(driver, driver.Url, "analog.com/kpidefault.aspx?locale=" + Locale + "#MSLTag");
            action.IClick(driver, By.CssSelector("span[class='roshLink pull-left']>a:nth-child(1)"));
            test.validateStringInstance(driver, driver.Url, "analog.com/kpidefault.aspx?locale=" + Locale + "#disclaimer");
        }

        [Test, Category("Quality And Reliability"), Category("NonCore")]
        [TestCase("en", TestName = "Verify the Multiple Product Models Search when user search using valid Product numbers in separate lines")]
        public void QnR_KPI_VerifyValidInputsInMultipleLine(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + Url);
            driver.SwitchTo().Frame(driver.FindElement(By.CssSelector("div[id='rte-body']>iframe")));
            string input = "AD7688\r\nAD7689\r\nAD7690";
            action.IType(driver, Elements.QnR_MatDec_ModelSearch_Multiple_TextArea, input);
            action.IClick(driver, Elements.QnR_MatDec_ModelSearch_Search_Btn);
            test.validateScreenByUrl(driver, Configuration.Env_Url.Replace("www", "quality").Replace("cldnet", "corpnt") + "kpidefault.aspx?locale=" + Locale);
            test.validateStringIsCorrect(driver, By.CssSelector("div[class='container page-title '] h1"), "Key Package Information Search Results");
            test.validateElementIsPresent(driver, Elements.QnR_KPI_ModelSearch_SERP_Result_Tbl);
            test.validateElementIsPresent(driver, Elements.QnR_MatDec_ModelSearch_SERP_RunNew);
            action.IClick(driver, Elements.QnR_MatDec_ModelSearch_SERP_RunNew);
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + Url);
        }

        [Test, Category("Quality And Reliability"), Category("NonCore")]
        [TestCase("en", TestName = "Verify the Multiple Product Models Search when user search using valid and invalid Product numbers in separate lines")]
        public void QnR_KPI_VerifyValidAndInvalidInputsInMultipleLine(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + Url);
            driver.SwitchTo().Frame(driver.FindElement(By.CssSelector("div[id='rte-body']>iframe")));
            string input = "AD7688\r\nAD7689ZZZZZZ\r\nAD7690";
            action.IType(driver, Elements.QnR_MatDec_ModelSearch_Multiple_TextArea, input);
            action.IClick(driver, Elements.QnR_MatDec_ModelSearch_Search_Btn);
            test.validateScreenByUrl(driver, Configuration.Env_Url.Replace("www", "quality").Replace("cldnet", "corpnt") + "kpidefault.aspx?locale=" + Locale );
            test.validateStringIsCorrect(driver, Elements.QnR_MatDec_ModelSearch_Multiple_ListError, "One or more of the part numbers in the list was not found in the search.");
            test.validateElementIsPresent(driver, Elements.QnR_KPI_ModelSearch_SERP_Result_Tbl);
            test.validateElementIsPresent(driver, Elements.QnR_MatDec_ModelSearch_SERP_ViewProducts_Link);
            action.IClick(driver, Elements.QnR_MatDec_ModelSearch_SERP_ViewProducts_Link);
            test.validateElementIsPresent(driver, Elements.QnR_MatDec_ModelSearch_SERP_InvalidModels);
            test.validateElementIsPresent(driver, Elements.QnR_MatDec_ModelSearch_SERP_Suggestions);
            action.IClick(driver, By.CssSelector("span[id='spnSuggestion'] p>a"));
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/support/technical-support.html");

        }

        //[Test, Category("Quality And Reliability"), Category("NonCore")]
        //[TestCase("en", TestName = "Verify the Multiple Product Models Search when user search using keyword that return only a single result")]
        //public void QnR_MatDec_VerifySingleResult(string Locale)
        //{
        //    action.Navigate(driver, Configuration.Env_Url + Locale + Url);
        //    driver.SwitchTo().Frame(driver.FindElement(By.CssSelector("strong>iframe")));
        //    string input = "AD7705BNZ";
        //    action.IType(driver, Elements.QnR_MatDec_ModelSearch_Multiple_TextArea, input);
        //    action.IClick(driver, Elements.QnR_MatDec_ModelSearch_Search_Btn);
        //    test.validateScreenByUrl(driver, Configuration.Env_Url.Replace("www", "quality").Replace("cldnet", "corpnt") + "default.aspx?locale=" + Locale);
        //    test.validateElementIsNotPresent(driver, Elements.QnR_MatDec_ModelSearch_LTCModal);
        //    test.validateElementIsPresent(driver, Elements.QnR_MatDec_ModelSearch_SERP_GoToProduct_Btn);
        //    test.validateElementIsPresent(driver, Elements.QnR_MatDec_ModelSearch_SERP_MatDec_Link);
        //    action.IClick(driver, Elements.QnR_MatDec_ModelSearch_SERP_GoToProduct_Btn);
        //    test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/products/ad7705.html");
        //    driver.Navigate().Back();
        //    action.IClick(driver, Elements.QnR_MatDec_ModelSearch_SERP_MatDec_Link);
        //    test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + Url);
        //}

        //[Test, Category("Quality And Reliability"), Category("NonCore")]
        //[TestCase("en", TestName = "Verify the Multiple Product Models Search when user search using LTC keyword")]
        //public void QnR_MatDec_VerifyLTCProducts(string Locale)
        //{
        //    action.Navigate(driver, Configuration.Env_Url + Locale + Url);
        //    driver.SwitchTo().Frame(driver.FindElement(By.CssSelector("strong>iframe")));
        //    string input = "LTC2\r\nAD7705BNZ";
        //    action.IType(driver, Elements.QnR_MatDec_ModelSearch_Multiple_TextArea, input);
        //    action.IClick(driver, Elements.QnR_MatDec_ModelSearch_Search_Btn);
        //    test.validateElementIsPresent(driver, Elements.QnR_MatDec_ModelSearch_LTCModal);
        //    action.IClick(driver, Elements.QnR_MatDec_ModelSearch_LTCModal_CloseBtn);
        //    test.validateElementIsNotPresent(driver, Elements.QnR_MatDec_ModelSearch_LTCModal);
        //}
    }
}