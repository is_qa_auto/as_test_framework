﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Threading;
using System;

namespace AS_Test_Framework.QualityAndReliability
{
    [TestFixture]
    public class Quality_MatDec_MultipleProductModelSearch : BaseSetUp
    {
        public Quality_MatDec_MultipleProductModelSearch() : base() { }

        string Url = "/about-adi/quality-reliability/material-declarations.html";

        [Test, Category("Quality And Reliability"), Category("NonCore")]
        [TestCase("en", TestName = "Verify the Multiple Product Models Search when user search using invalid inputs (blank and less than 4 char)")]
        public void QnR_MatDec_VerifyInvalidInputs(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + Url);
            driver.SwitchTo().Frame(driver.FindElement(By.CssSelector("strong>iframe")));
            action.IClick(driver, Elements.QnR_MatDec_ModelSearch_Search_Btn);
            test.validateStringIsCorrect(driver, Elements.QnR_MatDec_ModelSearch_Multiple_Error, "A minimum of four characters is required.");
            action.IType(driver, Elements.QnR_MatDec_ModelSearch_Multiple_TextArea, "ADD");
            action.IClick(driver, Elements.QnR_MatDec_ModelSearch_Search_Btn);
            test.validateString(driver, "ErrorPage | Analog Devices", driver.Title);
            driver.Navigate().Back();
            driver.SwitchTo().Frame(driver.FindElement(By.CssSelector("strong>iframe")));
            action.IDeleteValueOnFields(driver, Elements.QnR_MatDec_ModelSearch_Multiple_TextArea);
            action.IType(driver, Elements.QnR_MatDec_ModelSearch_Multiple_TextArea, "!\r\n?\r\n<><>");
            action.IClick(driver, Elements.QnR_MatDec_ModelSearch_Search_Btn);
            test.validateString(driver, "ErrorPage | Analog Devices", driver.Title);

        }


        [Test, Category("Quality And Reliability"), Category("NonCore")]
        [TestCase("en", TestName = "Verify the Multiple Product Models Search when user search using keyword that has no results")]
        public void QnR_MatDec_VerifyNoResultFound(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + Url);
            driver.SwitchTo().Frame(driver.FindElement(By.CssSelector("strong>iframe")));
            action.IType(driver, Elements.QnR_MatDec_ModelSearch_Multiple_TextArea, "AD7705ZZZZZ");
            action.IClick(driver, Elements.QnR_MatDec_ModelSearch_Search_Btn);
            test.validateElementIsNotPresent(driver, Elements.QnR_MatDec_ModelSearch_LTCModal);
            test.validateString(driver, "No Result | Analog Devices", driver.Title);
            test.validateStringIsCorrect(driver, Elements.QnR_MatDec_ModelSearch_NoResult_Message, "No results were found for your search term \"AD7705ZZZZZ\"");

        }

        [Test, Category("Quality And Reliability"), Category("NonCore")]
        [TestCase("en", TestName = "Verify the Multiple Product Models Search when user search using valid Product numbers in separate lines")]
        public void QnR_MatDec_VerifyValidInputsInMultipleLine(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + Url);
            driver.SwitchTo().Frame(driver.FindElement(By.CssSelector("strong>iframe")));
            string input = "AD7688\r\nAD7689\r\nAD7690";
            action.IType(driver, Elements.QnR_MatDec_ModelSearch_Multiple_TextArea, input);
            action.IClick(driver, Elements.QnR_MatDec_ModelSearch_Search_Btn);
            test.validateScreenByUrl(driver, Configuration.Env_Url.Replace("www", "quality").Replace("cldnet", "corpnt") + "default.aspx?locale=" + Locale);
            test.validateStringIsCorrect(driver, By.CssSelector("div[class='container page-title '] h1"), "Material Declaration Search Results");
            test.validateElementIsPresent(driver, Elements.QnR_MatDec_ModelSearch_Result_Table);
        }

        [Test, Category("Quality And Reliability"), Category("NonCore")]
        [TestCase("en", TestName = "Verify the Multiple Product Models Search when user search using valid and invalid Product numbers in separate lines")]
        public void QnR_MatDec_VerifyValidAndInvalidInputsInMultipleLine(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + Url);
            driver.SwitchTo().Frame(driver.FindElement(By.CssSelector("strong>iframe")));
            string input = "AD7688\r\nAD7689ZZZZZZ\r\nAD7690";
            action.IType(driver, Elements.QnR_MatDec_ModelSearch_Multiple_TextArea, input);
            action.IClick(driver, Elements.QnR_MatDec_ModelSearch_Search_Btn);
            test.validateScreenByUrl(driver, Configuration.Env_Url.Replace("www", "quality").Replace("cldnet", "corpnt") + "default.aspx?locale=" + Locale);
            test.validateStringIsCorrect(driver, Elements.QnR_MatDec_ModelSearch_Multiple_ListError, "One or more of the part numbers in the list was not found in the search.");
            test.validateElementIsPresent(driver, Elements.QnR_MatDec_ModelSearch_Result_Table);
            
        }

        [Test, Category("Quality And Reliability"), Category("NonCore")]
        [TestCase("en", TestName = "Verify the Multiple Product Models Search when user search using keyword that return only a single result")]
        public void QnR_MatDec_VerifySingleResult(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + Url);
            driver.SwitchTo().Frame(driver.FindElement(By.CssSelector("strong>iframe")));
            string input = "AD7705BNZ";
            action.IType(driver, Elements.QnR_MatDec_ModelSearch_Multiple_TextArea, input);
            action.IClick(driver, Elements.QnR_MatDec_ModelSearch_Search_Btn);
            test.validateScreenByUrl(driver, Configuration.Env_Url.Replace("www", "quality").Replace("cldnet", "corpnt") + "default.aspx?locale=" + Locale);
            test.validateElementIsNotPresent(driver, Elements.QnR_MatDec_ModelSearch_LTCModal);
            test.validateElementIsPresent(driver, Elements.QnR_MatDec_ModelSearch_SERP_GoToProduct_Btn);
            test.validateElementIsPresent(driver, Elements.QnR_MatDec_ModelSearch_SERP_MatDec_Link);
            action.IClick(driver, Elements.QnR_MatDec_ModelSearch_SERP_GoToProduct_Btn);
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/products/ad7705.html");
            driver.Navigate().Back();
            action.IClick(driver, Elements.QnR_MatDec_ModelSearch_SERP_MatDec_Link);
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + Url);
        }

        [Test, Category("Quality And Reliability"), Category("NonCore")]
        [TestCase("en", TestName = "Verify the Multiple Product Models Search when user search using LTC keyword")]
        public void QnR_MatDec_VerifyLTCProducts(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + Url);
            driver.SwitchTo().Frame(driver.FindElement(By.CssSelector("strong>iframe")));
            string input = "LTC4373\r\nAD7705BNZ";
            action.IType(driver, Elements.QnR_MatDec_ModelSearch_Multiple_TextArea, input);
            action.IClick(driver, Elements.QnR_MatDec_ModelSearch_Search_Btn);
            test.validateElementIsPresent(driver, Elements.QnR_MatDec_ModelSearch_LTCModal);
            action.IClick(driver, Elements.QnR_MatDec_ModelSearch_LTCModal_CloseBtn);
            test.validateElementIsNotPresent(driver, Elements.QnR_MatDec_ModelSearch_LTCModal);
        }
    }
}