﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Threading;
using System;

namespace AS_Test_Framework.QualityAndReliability
{
    [TestFixture]
    public class Quality_QualityCertifficates : BaseSetUp
    {
        public Quality_QualityCertifficates() : base() { }

        string Url = "/about-adi/quality-reliability/quality-certificates.html";

        [Test, Category("Quality And Reliability"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that List of Certificates are present in Quality Certificates page for EN Locale")]
        [TestCase("cn", TestName = "Verify that List of Certificates are present in Quality Certificates page for CN Locale")]
        [TestCase("jp", TestName = "Verify that List of Certificates are present in Quality Certificates page for JP Locale")]
        [TestCase("ru", TestName = "Verify that List of Certificates are present in Quality Certificates page for RU Locale")]
        public void QnR_QualityCertificates_VerifyLists(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + Url);
            test.validateElementIsPresent(driver, By.CssSelector("div[name='adi_education_default_listview'] ul>li"));
        }

        [Test, Category("Quality And Reliability"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that Certificate componenent is complete for EN Locale")]
        [TestCase("cn", TestName = "Verify that Certificate componenent is complete for CN Locale")]
        [TestCase("jp", TestName = "Verify that Certificate componenent is complete for JP Locale")]
        [TestCase("ru", TestName = "Verify that Certificate componenent is complete for RU Locale")]
        public void QnR_QualityCertificates_VerifyCertificateComponent(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/about-adi/quality-reliability/quality-certificates/iso-9001-certificates/ad-chelmsford-ma.html");
            test.validateElementIsPresent(driver, By.CssSelector("div[name='adi_threecolumn_content_section'] h2"));
            test.validateElementIsPresent(driver, By.CssSelector("div[name='adi_threecolumn_content_section'] div[id='rte-body'] img"));
            test.validateElementIsPresent(driver, By.CssSelector("div[name='adi_threecolumn_content_section'] div[id='rte-body'] p>a"));

        }
    }
}