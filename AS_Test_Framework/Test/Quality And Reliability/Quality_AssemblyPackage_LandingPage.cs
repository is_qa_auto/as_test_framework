﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Threading;
using System;

namespace AS_Test_Framework.QualityAndReliability
{
    [TestFixture]
    public class Quality_AssemblyPackage_LandingPage : BaseSetUp
    {
        public Quality_AssemblyPackage_LandingPage() : base() { }

        string Url = "/about-adi/quality-reliability/reliability-data/assembly-package-process-data.html";

        [Test, Category("Quality And Reliability"), Category("NonCore")]
        [TestCase("en", "Assembly/Package Process Data", TestName = "Verify Assembly/Package Process Data page components are present for EN Locale")]
        [TestCase("cn", "组装/封装工艺数据", TestName = "Verify Assembly/Package Process Data page components are present for CN Locale")]
        [TestCase("jp", "Assembly/Package Process Data", TestName = "Verify Assembly/Package Process Data page components are present for JP Locale")]
        [TestCase("ru", "Результаты испытаний устройств в корпусе/после сборки", TestName = "Verify Assembly/Package Process Data page components are present for RU Locale")]
        public void QnR_ReliabilityData_VerifyAssemblyPackageElements(string Locale, string ActiveLeftNav)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + Url);
            test.validateStringIsCorrect(driver, By.CssSelector("div[name='adi_threecolumn_centercolumn_section'] ul>li[class='active']>a"), ActiveLeftNav);
           
            driver.SwitchTo().Frame(driver.FindElement(By.CssSelector("div[id='rte-body'] iframe")));

            test.validateElementIsPresent(driver, Elements.QnR_AssemblyPackage_TestName_DD);
            test.validateElementIsPresent(driver, Elements.QnR_AssemblyPackage_PackageFam_DD);
            test.validateElementIsPresent(driver, Elements.QnR_AssemblyPackage_Submit_Btn);
        }
    }
}