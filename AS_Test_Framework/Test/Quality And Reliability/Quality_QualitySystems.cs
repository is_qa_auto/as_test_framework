﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Threading;
using System;

namespace AS_Test_Framework.QualityAndReliability
{
    [TestFixture]
    public class Quality_QualitySystems : BaseSetUp
    {
        public Quality_QualitySystems() : base() { }

        string Url = "/about-adi/quality-reliability/quality-systems.html";

        [Test, Category("Quality And Reliability"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that List of Qualtiy Systems are present in Quality Certificates page for EN Locale")]
        [TestCase("cn", TestName = "Verify that List of Qualtiy Systems are present in Quality Certificates page for CN Locale")]
        [TestCase("jp", TestName = "Verify that List of Qualtiy Systems are present in Quality Certificates page for JP Locale")]
        [TestCase("ru", TestName = "Verify that List of Qualtiy Systems are present in Quality Certificates page for RU Locale")]
        public void QnR_QualitySystem_VerifyLists(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + Url);
            test.validateElementIsPresent(driver, By.CssSelector("div[name='adi_threecolumn_content_section'] ul>li"));
        }

        [Test, Category("Quality And Reliability"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that the user can navigate on different Quality Systems from the 2nd Column for EN Locale")]
        [TestCase("cn", TestName = "Verify that the user can navigate on different Quality Systems from the 2nd Column for CN Locale")]
        [TestCase("jp", TestName = "Verify that the user can navigate on different Quality Systems from the 2nd Column for JP Locale")]
        [TestCase("ru", TestName = "Verify that the user can navigate on different Quality Systems from the 2nd Column for RU Locale")]
        public void QnR_QualitySystem_VerifyQualitySystemLinkVia2ndColumn(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + Url);
            test.validateElementIsPresent(driver, By.CssSelector("div[class='col-md-3 column-secondary hidden-xs'] ul"));
            string[] QualSys = {
                                    "/about-adi/quality-reliability/quality-systems/product-and-process-development.html",
                                    "/about-adi/quality-reliability/quality-systems/product-manufacturing.html",
                                    "/about-adi/quality-reliability/quality-systems/post-sales-support.html",
                                    "/about-adi/quality-reliability/quality-systems/quality-management-systems.html"
                                };

            for (int x = 1, y = 0; util.CheckElement(driver, By.CssSelector("div[class='col-md-3 column-secondary hidden-xs'] ul>li:nth-child(" + x + ")"), 5); x++, y++) {
                action.IClick(driver, By.CssSelector("div[class='col-md-3 column-secondary hidden-xs'] ul>li:nth-child(" + x + ")>a"));
                test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + QualSys[y]);
            }
        }

        [Test, Category("Quality And Reliability"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that the user can navigate on different Quality Systems via View More Link for EN Locale")]
        [TestCase("cn", TestName = "Verify that the user can navigate on different Quality Systems via View More Link for CN Locale")]
        [TestCase("jp", TestName = "Verify that the user can navigate on different Quality Systems via View More Link for JP Locale")]
        [TestCase("ru", TestName = "Verify that the user can navigate on different Quality Systems via View More Link for RU Locale")]
        public void QnR_QualitySystem_VerifyQualitySystemLinkViaViewMoreLink(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + Url);
            test.validateElementIsPresent(driver, By.CssSelector("div[class='col-md-3 column-secondary hidden-xs'] ul"));
            string[] QualSys = {
                                    "/about-adi/quality-reliability/quality-systems/product-and-process-development.html",
                                    "/about-adi/quality-reliability/quality-systems/product-manufacturing.html",
                                    "/about-adi/quality-reliability/quality-systems/post-sales-support.html",
                                    "/about-adi/quality-reliability/quality-systems/quality-management-systems.html"
                                };

            for (int x = 1, y = 0; util.CheckElement(driver, By.CssSelector("div[name='adi_threecolumn_content_section'] ul>li:nth-child(" + x + ")"), 5); x++, y++)
            {
                action.IClick(driver, By.CssSelector("div[name='adi_threecolumn_content_section'] ul>li:nth-child(" + x + ") a"));
                test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + QualSys[y]);
                driver.Navigate().Back();
            }
        }
    }
}