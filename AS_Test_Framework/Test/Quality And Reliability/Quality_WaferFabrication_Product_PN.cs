﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Threading;
using System;

namespace AS_Test_Framework.QualityAndReliability
{
    [TestFixture]
    public class Quality_WaferFabrication_Product_PN : BaseSetUp
    {
        public Quality_WaferFabrication_Product_PN() : base() { }

        string Url = "/about-adi/quality-reliability/reliability-data/wafer-fabrication-data.html";

        [Test, Category("Quality And Reliability"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that the user can do query via Product P/N drop down")]
        public void QnR_ReliabilityData_VerifyProductPN(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + Url);
            driver.SwitchTo().Frame(driver.FindElement(By.CssSelector("div[id='rte-body'] iframe")));

            action.IClick(driver, Elements.QnR_WaferFabrication_ProductPn_Submit_Button);
            test.validateString(driver, "Select a value from Product dropdown", util.GetAlertMessageThenAccept(driver));
            
            /****Update for AL-17620*****/
            string selectedValue = "AD7960";
            action.IType(driver, Elements.QnR_WaferFabrication_ProductPn_TxtField, selectedValue);
            action.IClick(driver, Elements.QnR_WaferFabrication_ProductPn_Submit_Button);
            Thread.Sleep(10000);
            test.validateStringIsCorrect(driver, Elements.QnR_WaferFabrication_ProductPn_Result_Label, "Product P/N:"+ selectedValue);
            test.validateElementIsPresent(driver, Elements.QnR_WaferFabrication_ProcessTech_Result_FirstTbl);
            test.validateElementIsPresent(driver, Elements.QnR_WaferFabrication_ProcessTech_Recalculate_Btn);
            test.validateElementIsPresent(driver, Elements.QnR_WaferFabrication_ProcessTech_Temperature_TextField);
            test.validateElementIsPresent(driver, Elements.QnR_WaferFabrication_ProcessTech_Result_SecondTbl);
            test.validateElementIsPresent(driver, Elements.QnR_WaferFabrication_BackToTop_Link);
        }

        [Test, Category("Quality And Reliability"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that the user can Recalculate in the Product P/N results page")]
        public void QnR_ReliabilityData_VerifyProductPnRecalculate(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + Url);
            driver.SwitchTo().Frame(driver.FindElement(By.CssSelector("div[id='rte-body'] iframe")));
            /****Update for AL-17620****/
            //string selectedValue = util.GetAllValueInDropdownThenSelectOne(driver, Elements.QnR_WaferFabrication_ProductPn_DD, 2);
            //action.ISelectFromDropdown(driver, Elements.QnR_WaferFabrication_ProductPn_DD, selectedValue);
            string selectedValue = "AD7960";
            action.IType(driver, Elements.QnR_WaferFabrication_ProductPn_TxtField, selectedValue);
            action.IClick(driver, Elements.QnR_WaferFabrication_ProductPn_Submit_Button);
            Thread.Sleep(5000);
            string DefaultTemp = util.GetText(driver, By.CssSelector("div[id='divTotal0'] tr:nth-child(4)>td>span[id='accTemp0_0']"));

            action.IType(driver, Elements.QnR_WaferFabrication_ProcessTech_Temperature_TextField, "999");
            action.IClick(driver, Elements.QnR_WaferFabrication_ProcessTech_Recalculate_Btn);
            Thread.Sleep(5000);
            test.validateElementIsPresent(driver, Elements.QnR_WaferFabrication_ProcessTech_ErrorMessage);

            //action.IType(driver, Elements.QnR_WaferFabrication_ProcessTech_Temperature_TextField, "zzz");
            //action.IClick(driver, Elements.QnR_WaferFabrication_ProcessTech_Recalculate_Btn);
            //Thread.Sleep(5000);
            //test.validateStringIsCorrect(driver, By.CssSelector("div[id='divTotal0'] tr:nth-child(4)>td>span[id='accTemp0_0']"), "100");

            action.IType(driver, Elements.QnR_WaferFabrication_ProcessTech_Temperature_TextField, "100");
            action.IClick(driver, Elements.QnR_WaferFabrication_ProcessTech_Recalculate_Btn);
            Thread.Sleep(5000);
            test.validateStringIsCorrect(driver,By.CssSelector("div[id='divTotal0'] tr:nth-child(4)>td>span[id='accTemp0_0']"), "100");

            //action.IType(driver, Elements.QnR_WaferFabrication_ProcessTech_Temperature_TextField, "2.8");
            //action.IClick(driver, Elements.QnR_WaferFabrication_ProcessTech_Recalculate_Btn);
            //Thread.Sleep(5000);
            //test.validateStringIsCorrect(driver, By.CssSelector("div[id='divTotal0'] tr:nth-child(4)>td>span[id='accTemp0_0']"), "2.8");

            action.IType(driver, Elements.QnR_WaferFabrication_ProcessTech_Temperature_TextField, "-17");
            action.IClick(driver, Elements.QnR_WaferFabrication_ProcessTech_Recalculate_Btn);
            Thread.Sleep(5000);
            test.validateStringIsCorrect(driver, By.CssSelector("div[id='divTotal0'] tr:nth-child(4)>td>span[id='accTemp0_0']"), "-17");

            action.IType(driver, Elements.QnR_WaferFabrication_ProcessTech_Temperature_TextField, "0");
            action.IClick(driver, Elements.QnR_WaferFabrication_ProcessTech_Recalculate_Btn);
            Thread.Sleep(5000);
            test.validateStringIsCorrect(driver, By.CssSelector("div[id='divTotal0'] tr:nth-child(4)>td>span[id='accTemp0_0']"), DefaultTemp);
        }
    }
}
 