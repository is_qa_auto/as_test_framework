﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Threading;
using System;

namespace AS_Test_Framework.QualityAndReliability
{
    [TestFixture]
    public class Quality_MatDec_SearchResultPage : BaseSetUp
    {
        public Quality_MatDec_SearchResultPage() : base() { }

        string Url = "/about-adi/quality-reliability/material-declarations.html";

        [Test, Category("Quality And Reliability"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that \"Go to Product Page\" component is present and working as expected")]
        public void QnR_MatDec_VerifyGoToProduct(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + Url);
            driver.SwitchTo().Frame(driver.FindElement(By.CssSelector("strong>iframe")));
            string input = "AD7705";
            action.IType(driver, Elements.QnR_MatDec_ModelSearch_Multiple_TextArea, input);
            action.IClick(driver, Elements.QnR_MatDec_ModelSearch_Search_Btn);
            test.validateStringIsCorrect(driver, Elements.QnR_MatDec_ModelSearch_SERP_GoToProductPage_Label, "Go to Product Page");
            test.validateElementIsPresent(driver, Elements.QnR_MatDec_ModelSearch_SERP_GoToProductPage_DD);
            test.validateElementIsPresent(driver, Elements.QnR_MatDec_ModelSearch_SERP_GoToProductPage_Go_Btn);
            string GetSelected = util.GetSelectedDropdownValue(driver, Elements.QnR_MatDec_ModelSearch_SERP_GoToProductPage_DD);
            action.IClick(driver, Elements.QnR_MatDec_ModelSearch_SERP_GoToProductPage_Go_Btn);
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/products/" + GetSelected.ToLower() + ".html");
        }

        [Test, Category("Quality And Reliability"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that \"View Products\" component is present and working as expected")]
        public void QnR_MatDec_VerifyViewProducts(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + Url);
            driver.SwitchTo().Frame(driver.FindElement(By.CssSelector("strong>iframe")));
            string input = "AD7688\r\nAD7689ZZZZZZ\r\nAD7690";
            action.IType(driver, Elements.QnR_MatDec_ModelSearch_Multiple_TextArea, input);
            action.IClick(driver, Elements.QnR_MatDec_ModelSearch_Search_Btn);
            test.validateElementIsPresent(driver, Elements.QnR_MatDec_ModelSearch_SERP_ViewProducts_Link);
            action.IClick(driver, Elements.QnR_MatDec_ModelSearch_SERP_ViewProducts_Link);
            test.validateElementIsPresent(driver, Elements.QnR_MatDec_ModelSearch_SERP_InvalidModels);
            test.validateElementIsPresent(driver, Elements.QnR_MatDec_ModelSearch_SERP_Suggestions);
        }

        [Test, Category("Quality And Reliability"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that \"Disclaimer\" link is present and working as expected")]
        public void QnR_MatDec_VerifyDisclaimer(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + Url);
            driver.SwitchTo().Frame(driver.FindElement(By.CssSelector("strong>iframe")));
            string input = "AD7690";
            action.IType(driver, Elements.QnR_MatDec_ModelSearch_Multiple_TextArea, input);
            action.IClick(driver, Elements.QnR_MatDec_ModelSearch_Search_Btn);
            test.validateElementIsPresent(driver, Elements.QnR_MatDec_ModelSearch_SERP_Disclaimer_Link);
            action.IClick(driver, Elements.QnR_MatDec_ModelSearch_SERP_Disclaimer_Link);
            test.validateScreenByUrl(driver, Configuration.Env_Url.Replace("www", "quality").Replace("cldnet", "corpnt") + "default.aspx?locale=" + Locale + "#disclaimer");
        }

        [Test, Category("Quality And Reliability"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that pagination in result table is present and working as expected")]
        public void QnR_MatDec_VerifyResultTablePagination(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + Url);
            driver.SwitchTo().Frame(driver.FindElement(By.CssSelector("strong>iframe")));
            string input = "AD79";
            action.IType(driver, Elements.QnR_MatDec_ModelSearch_Multiple_TextArea, input);
            action.IClick(driver, Elements.QnR_MatDec_ModelSearch_Search_Btn);
            test.validateElementIsPresent(driver, Elements.QnR_MatDec_ModelSearch_SERP_Pagination_First);
            test.validateElementIsPresent(driver, Elements.QnR_MatDec_ModelSearch_SERP_Pagination_Next);
            test.validateElementIsPresent(driver, Elements.QnR_MatDec_ModelSearch_SERP_Pagination_Prev);
            test.validateElementIsPresent(driver, Elements.QnR_MatDec_ModelSearch_SERP_Pagination_Last);
            string originalPageCount = util.GetText(driver, Elements.QnR_MatDec_ModelSearch_SERP_Pagination_PageIndicator);
            string lastPageCount = util.GetText(driver, Elements.QnR_MatDec_ModelSearch_SERP_Pagination_PageIndicator).Split('f')[1].Trim();
            /***verify the next page button***/
            action.IClick(driver, Elements.QnR_MatDec_ModelSearch_SERP_Pagination_Next);
            test.validateStringIsCorrect(driver, Elements.QnR_MatDec_ModelSearch_SERP_Pagination_PageIndicator, "Page 2 of " + lastPageCount);
            
            /***verify the previous page button***/
            action.IClick(driver, Elements.QnR_MatDec_ModelSearch_SERP_Pagination_Prev);
            test.validateStringIsCorrect(driver, Elements.QnR_MatDec_ModelSearch_SERP_Pagination_PageIndicator, originalPageCount);
            
            /***verify the last page button***/
            action.IClick(driver, Elements.QnR_MatDec_ModelSearch_SERP_Pagination_Last);
            test.validateStringIsCorrect(driver, Elements.QnR_MatDec_ModelSearch_SERP_Pagination_PageIndicator, "Page " + lastPageCount + " of " + lastPageCount);

            /***verify the previous page button***/
            action.IClick(driver, Elements.QnR_MatDec_ModelSearch_SERP_Pagination_First);
            test.validateStringIsCorrect(driver, Elements.QnR_MatDec_ModelSearch_SERP_Pagination_PageIndicator, originalPageCount);
        }

        [Test, Category("Quality And Reliability"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that no of result displayed dropdown in result table is present and working as expected")]
        public void QnR_MatDec_VerifyResultTableDisplay(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + Url);
            driver.SwitchTo().Frame(driver.FindElement(By.CssSelector("strong>iframe")));
            string input = "AD79";
            action.IType(driver, Elements.QnR_MatDec_ModelSearch_Multiple_TextArea, input);
            action.IClick(driver, Elements.QnR_MatDec_ModelSearch_Search_Btn);

            test.validateSelectedValueIsCorrect(driver, Elements.QnR_MatDec_Display_DD, "10");
            action.ISelectFromDropdown(driver, Elements.QnR_MatDec_Display_DD, "20");
            Thread.Sleep(1500);
            test.validateCountIsEqual(driver, util.GetCount(driver, By.CssSelector("div[id='proSearchResult'] * div[id='jfg_body']>div[id='jfg_body_left'] * tbody>tr")), Int32.Parse(util.GetSelectedDropdownValue(driver, Elements.QnR_MatDec_Display_DD)));

            action.IClick(driver, By.CssSelector("input[id='chkbxRemove0']"));
            action.IClick(driver, Elements.QnR_MatDec_ModelSearch_SERP_RemoveRows_Btn);
            test.validateCountIsEqual(driver, util.GetCount(driver, By.CssSelector("div[id='proSearchResult'] * div[id='jfg_body']>div[id='jfg_body_left'] * tbody>tr")), Int32.Parse(util.GetSelectedDropdownValue(driver, Elements.QnR_MatDec_Display_DD)));

        }

        [Test, Category("Quality And Reliability"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that remove button in result table is present and working as expected")]
        public void QnR_MatDec_VerifyRemoveRow(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + Url);
            driver.SwitchTo().Frame(driver.FindElement(By.CssSelector("strong>iframe")));
            string input = "AD79";
            action.IType(driver, Elements.QnR_MatDec_ModelSearch_Multiple_TextArea, input);
            action.IClick(driver, Elements.QnR_MatDec_ModelSearch_Search_Btn);

            //test.validateSelectedValueIsCorrect(driver, Elements.QnR_MatDec_Display_DD, "10");
            //action.ISelectFromDropdown(driver, Elements.QnR_MatDec_Display_DD, "20");
            //Thread.Sleep(1500);
            //test.validateCountIsEqual(driver, util.GetCount(driver, By.CssSelector("div[id='proSearchResult'] * div[id='jfg_body']>div[id='jfg_body_left'] * tbody>tr")), Int32.Parse(util.GetSelectedValue(driver, Elements.QnR_MatDec_Display_DD)));
            int OriginalTotalCount = Int32.Parse(util.GetText(driver, Elements.QnR_MatDec_ModelSearch_SERP_Pagination_ShowCount).Split(' ')[3]);
            
            action.IClick(driver, Elements.QnR_MatDec_ModelSearch_SERP_RemoveRows_Btn);
            test.validateString(driver, "Please select a model to remove.", util.GetAlertMessageThenAccept(driver).Trim());
            test.validateStringIsCorrect(driver, Elements.QnR_MatDec_ModelSearch_SERP_Pagination_ShowCount, "Showing 1-10 of " + OriginalTotalCount + " Product Matches");

            string FirstMatch = util.GetText(driver, By.CssSelector("div[id='jfg_body_left']>table>tbody>tr:nth-child(1)>td:nth-child(3) a"));
            action.IClick(driver, By.CssSelector("input[id='chkbxRemove0']"));
            action.IClick(driver, Elements.QnR_MatDec_ModelSearch_SERP_RemoveRows_Btn);
            test.validateStringIsCorrect(driver, Elements.QnR_MatDec_ModelSearch_SERP_Pagination_ShowCount, "Showing 1-10 of " + (OriginalTotalCount - 1) + " Product Matches");
            
        }


        [Test, Category("Quality And Reliability"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that the run new search link is present and working as expected")]
        public void QnR_MatDec_VerifyRunNew(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + Url);
            driver.SwitchTo().Frame(driver.FindElement(By.CssSelector("strong>iframe")));
            string input = "AD79";
            action.IType(driver, Elements.QnR_MatDec_ModelSearch_Multiple_TextArea, input);
            action.IClick(driver, Elements.QnR_MatDec_ModelSearch_Search_Btn);
            test.validateElementIsPresent(driver, Elements.QnR_MatDec_ModelSearch_SERP_RunNew);
            action.IClick(driver, Elements.QnR_MatDec_ModelSearch_SERP_RunNew);
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + Url);
        }
    }
}