﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;

namespace AS_Test_Framework.QualityAndReliability
{
    [TestFixture]
    public class Quality_Reliability_ReliabilityData : BaseSetUp
    {
        public Quality_Reliability_ReliabilityData() : base() { }

        string Url = "/about-adi/quality-reliability/reliability-data.html";

        [Test, Category("Quality And Reliability"), Category("NonCore")]
        [TestCase("en", "Reliability Data", "Wafer Fabrication Data", "Assembly/Package Process Data", "Reliability Data Pack", TestName = "Verify that Reliabiltiy Data Landing Page in EN Locale")]
        public void QnR_ReliabilityData_VerifyNavigationToReliabilityDataPage(string Locale, string ActiveLeftNav, string WaferFab_Label, string Assembly_Label, string ReliabilityDataPack_Label)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + Url);
            test.validateStringIsCorrect(driver, By.CssSelector("div[name='adi_threecolumn_leftnavigation_section']>ul>li[class='active']>ul>li>a[class='selected']"), ActiveLeftNav);

            /****wafer fabrication****/
            test.validateStringIsCorrect(driver, By.CssSelector("div[id='rte-body']>p:nth-child(1)>strong"), WaferFab_Label);
            test.validateElementIsPresent(driver, By.CssSelector("div[id='rte-body']>p:nth-child(3)>a"));

            /****wafer fabrication****/
            test.validateStringIsCorrect(driver, By.CssSelector("div[id='rte-body']>p:nth-child(5)>strong"), Assembly_Label);
            test.validateElementIsPresent(driver, By.CssSelector("div[id='rte-body']>p:nth-child(7)>a"));
            
        //    test.validateStringIsCorrect(driver, By.CssSelector("div[id='rte-body']>p:nth-child(9)>strong"), ReliabilityDataPack_Label);
        //    test.validateElementIsPresent(driver, By.CssSelector("div[id='rte-body']>p:nth-child(10)>a"));
        }

        [Test, Category("Quality And Reliability"), Category("NonCore")]
        [TestCase("en", "Reliability Data", "About ADI", "Quality & Reliability", TestName = "Verify that user can see the Breadcrumbs and accessible in EN Locale")]
        public void QnR_ReliabilityData_VerifyBreadcrumbs(string Locale, string CurrentPage, string FirstLevelCat, string SecondLevelCat)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + Url);
            test.validateElementIsPresent(driver, Elements.Applications_BreadCrumbs);
            test.validateStringIsCorrect(driver, By.CssSelector("div[name='adi_breadcrumb'] li[class='active']>span"), CurrentPage);
            test.validateStringIsCorrect(driver, By.CssSelector("div[name='adi_breadcrumb'] ol>li:nth-child(2)>span>a"), FirstLevelCat);
            test.validateStringIsCorrect(driver, By.CssSelector("div[name='adi_breadcrumb'] ol>li:nth-child(3)>span>a"), SecondLevelCat);

            action.IClick(driver, By.CssSelector("div[name='adi_breadcrumb'] ol>li:nth-child(2)>span>a"));
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/about-adi.html");
            driver.Navigate().Back();

            action.IClick(driver, By.CssSelector("div[name='adi_breadcrumb'] ol>li:nth-child(3)>span>a"));
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/about-adi/quality-reliability.html");
            driver.Navigate().Back();

            test.validateElementIsPresent(driver, Elements.Applications_BreadCrumbs_Home);
            action.IClick(driver, Elements.Applications_BreadCrumbs_Home);
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/index.html");
        }

        [Test, Category("Quality And Reliability"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that links in second column are present and working in EN Locale")]
        public void QnR_ReliabilityData_VerifySecondColumn(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + Url);

            test.validateElementIsPresent(driver, Elements.QnR_ReliabilityData_WaferFabrication_Link);
            test.validateElementIsPresent(driver, Elements.QnR_ReliabilityData_AssemblyPackage_Link);

            action.IClick(driver, Elements.QnR_ReliabilityData_WaferFabrication_Link);
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/about-adi/quality-reliability/reliability-data/wafer-fabrication-data.html");
            driver.Navigate().Back();

            action.IClick(driver, Elements.QnR_ReliabilityData_AssemblyPackage_Link);
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/about-adi/quality-reliability/reliability-data/assembly-package-process-data.html");
        }
    }
}