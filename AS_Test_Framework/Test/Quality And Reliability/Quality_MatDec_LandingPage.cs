﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Threading;
using System;

namespace AS_Test_Framework.QualityAndReliability
{
    [TestFixture]
    public class Quality_MatDec_LandingPage : BaseSetUp
    {
        public Quality_MatDec_LandingPage() : base() { }

        string Url = "/about-adi/quality-reliability/material-declarations.html";

        [Test, Category("Quality And Reliability"), Category("NonCore")]
        [TestCase("en", "Material Declarations", TestName = "Verify that user can navigate to Material Declaration page via \"About ADI\" from the header in EN Locale")]
        [TestCase("cn", "材料声明", TestName = "Verify that user can navigate to Material Declaration page via \"About ADI\" from the header in CN Locale")]
        [TestCase("jp", "成分表", TestName = "Verify that user can navigate to Material Declaration page via \"About ADI\" from the header in JP Locale")]
        [TestCase("ru", "Используемые материалы", TestName = "Verify that user can navigate to Material Declaration page via \"About ADI\" from the header in RU Locale")]
        public void QnR_MatDec_VerifyNavigationToMaterialDeclarationPage(string Locale, string ActiveLeftNav)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/about-adi.html");
            action.IClick(driver, By.CssSelector("a[name*='quality-reliability']"));
            action.IClick(driver, By.CssSelector("a[name*='material-declarations']"));
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + Url);
            test.validateStringIsCorrect(driver, By.CssSelector("div[name='adi_threecolumn_leftnavigation_section']>ul>li[class='active']>ul>li>a[class='selected']"), ActiveLeftNav);
        }

        [Test, Category("Quality And Reliability"), Category("NonCore")]
        [TestCase("en", "About ADI", "Quality & Reliability", "Material Declarations", TestName = "Verify that breadcrumbs is present in Material Declaration page for EN Locale")]
        [TestCase("cn", "关于ADI", "质量和可靠性", "材料声明", TestName = "Verify that breadcrumbs is present in Material Declaration page for CN Locale")]
        [TestCase("jp", "ADIについて", "品質＆信頼性", "成分表", TestName = "Verify that breadcrumbs is present in Material Declaration page for JP Locale")]
        [TestCase("ru", "О компании Analog Devices", "Качество и надежность", "Используемые материалы", TestName = "Verify that breadcrumbs is present in Material Declaration page for RU Locale")]
        public void QnR_MatDec_VerifyBreadCrumbs1(string Locale, string FirstLevelCat, string SecondLevelCat, string CurrentPage)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + Url);
            test.validateElementIsPresent(driver, Elements.Applications_BreadCrumbs);
            test.validateStringIsCorrect(driver, By.CssSelector("div[name='adi_breadcrumb'] ol>li:nth-child(2)>span>a"), FirstLevelCat);
            test.validateStringIsCorrect(driver, By.CssSelector("div[name='adi_breadcrumb'] ol>li:nth-child(3)>span>a"), SecondLevelCat);
            test.validateStringIsCorrect(driver, By.CssSelector("div[name='adi_breadcrumb'] li[class='active']>span"), CurrentPage);
            action.IClick(driver, By.CssSelector("div[name='adi_breadcrumb'] ol>li:nth-child(2)>span>a"));
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/about-adi.html");
            driver.Navigate().Back();
            action.IClick(driver, By.CssSelector("div[name='adi_breadcrumb'] ol>li:nth-child(3)>span>a"));
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/about-adi/quality-reliability.html");
            driver.Navigate().Back();
            test.validateElementIsPresent(driver, Elements.Applications_BreadCrumbs_Home);
            action.IClick(driver, Elements.Applications_BreadCrumbs_Home);
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/index.html");
        }

        [Test, Category("Quality And Reliability"), Category("NonCore")]
        [TestCase("en", TestName = "Verify Material Declaration components are present complete")]
        public void QnR_MatDec_VerifyComponents(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + Url);
            test.validateElementIsPresent(driver, Elements.QnR_MatDec_OtherInformation_Link);
            test.validateElementIsPresent(driver, Elements.QnR_MatDec_Disclaimer_Link);

            driver.SwitchTo().Frame(driver.FindElement(By.CssSelector("strong>iframe")));
            test.validateElementIsNotPresent(driver, Elements.QnR_MatDec_ModelSearch_Multiple_Error);

            /****Multiple Product Model Search Component*****/
            test.validateStringIsCorrect(driver, Elements.QnR_MatDec_ModelSearch_Header, "Product Model Search");
            test.validateElementIsPresent(driver, Elements.QnR_MatDec_ModelSearch_Multiple_RadioBtn);
            test.validateElementIsPresent(driver, Elements.QnR_MatDec_ModelSearch_Multiple_Help);
            test.validateElementIsPresent(driver, Elements.QnR_MatDec_ModelSearch_Multiple_TextArea);
            test.validateStringIsCorrect(driver, Elements.QnR_MatDec_ModelSearch_Multiple_TextArea, "Enter full model part number.\r\nEnter multiple model part numbers on separate lines.");
            test.validateElementIsPresent(driver, Elements.QnR_MatDec_ModelSearch_Single_RadioBtn);
            test.validateElementIsPresent(driver, Elements.QnR_MatDec_ModelSearch_Single_Help);
            action.IClick(driver, Elements.QnR_MatDec_ModelSearch_Single_RadioBtn);
            test.validateElementIsPresent(driver, Elements.QnR_MatDec_ModelSearch_Single_TextField);

            test.validateElementIsPresent(driver, Elements.QnR_MatDec_ModelSearch_Search_Btn);
        }



    }
}