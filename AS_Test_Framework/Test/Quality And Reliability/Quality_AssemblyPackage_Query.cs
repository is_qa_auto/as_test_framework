﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Threading;
using System;

namespace AS_Test_Framework.QualityAndReliability
{
    [TestFixture]
    public class Quality_AssemblyPackage_Query : BaseSetUp
    {
        public Quality_AssemblyPackage_Query() : base() { }

        string Url = "/about-adi/quality-reliability/reliability-data/assembly-package-process-data.html";

        [Test, Category("Quality And Reliability"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that the user can Access the Test Name and Package Familty drop downs and do query")]
        public void QnR_ReliabilityData_VerifyAssemblyPackageQuery(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + Url);
            driver.SwitchTo().Frame(driver.FindElement(By.CssSelector("div[id='rte-body'] iframe")));

            action.IClick(driver, Elements.QnR_AssemblyPackage_Submit_Btn);
            test.validateString(driver, "Both dropdowns cannot have 'All' values", util.GetAlertMessageThenAccept(driver));
            string SelectedTestName = util.GetAllValueInDropdownThenSelectOne(driver, Elements.QnR_AssemblyPackage_TestName_DD, 2);
            string SelectedPackageFamName = util.GetAllValueInDropdownThenSelectOne(driver, Elements.QnR_AssemblyPackage_PackageFam_DD, 2);
            action.ISelectFromDropdown(driver, Elements.QnR_AssemblyPackage_TestName_DD, SelectedTestName);
            action.ISelectFromDropdown(driver, Elements.QnR_AssemblyPackage_PackageFam_DD, SelectedPackageFamName);
            action.IClick(driver, Elements.QnR_AssemblyPackage_Submit_Btn);
            test.validateStringIsCorrect(driver, By.CssSelector("div[class='lead']>strong:nth-of-type(1)"), "Test Name: " + SelectedTestName);
            test.validateStringIsCorrect(driver, By.CssSelector("div[class='lead']>strong:nth-of-type(2)"), "Package Family: " + SelectedPackageFamName);
            test.validateStringInstance(driver, util.GetText(driver, By.CssSelector("div[class='qua-col-md-12']>p")), "Total Sample Size");
            test.validateElementIsPresent(driver, Elements.QnR_AssemblyPackage_ModifiedDate);
            test.validateElementIsPresent(driver, Elements.QnR_AssemblyPackage_Result_Tbl);
        }


        [Test, Category("Quality And Reliability"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that the user can Access the Test Name and Package Familty drop downs and do invalid query (no results)")]
        public void QnR_ReliabilityData_VerifyAssemblyPackageQuery2(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + Url);
            driver.SwitchTo().Frame(driver.FindElement(By.CssSelector("div[id='rte-body'] iframe")));
            string SelectedTestName = util.GetAllValueInDropdownThenSelectOne(driver, Elements.QnR_AssemblyPackage_TestName_DD, 2);
            string SelectedPackageFamName = util.GetAllValueInDropdownThenSelectOne(driver, Elements.QnR_AssemblyPackage_PackageFam_DD, 5);
            action.ISelectFromDropdown(driver, Elements.QnR_AssemblyPackage_TestName_DD, SelectedTestName);
            action.ISelectFromDropdown(driver, Elements.QnR_AssemblyPackage_PackageFam_DD, SelectedPackageFamName);
            action.IClick(driver, Elements.QnR_AssemblyPackage_Submit_Btn);
            test.validateStringIsCorrect(driver, By.CssSelector("div[id='HTMLData']"), "No records were found based on your search criteria. Please click here to try again.");
            test.validateElementIsPresent(driver, By.CssSelector("div[id='HTMLData']>a"));
            action.IClick(driver, By.CssSelector("div[id='HTMLData']>a"));
            test.validateElementIsPresent(driver, Elements.QnR_AssemblyPackage_TestName_DD);
        }


        [Test, Category("Quality And Reliability"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that the user can do query by either TestName or Package Family")]
        public void QnR_ReliabilityData_VerifyAssemblyPackageQuery3(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + Url);
            driver.SwitchTo().Frame(driver.FindElement(By.CssSelector("div[id='rte-body'] iframe")));
            string SelectedTestName = util.GetAllValueInDropdownThenSelectOne(driver, Elements.QnR_AssemblyPackage_TestName_DD, 2);
            action.ISelectFromDropdown(driver, Elements.QnR_AssemblyPackage_TestName_DD, SelectedTestName);
            action.IClick(driver, Elements.QnR_AssemblyPackage_Submit_Btn);
            Thread.Sleep(15000);
            test.validateStringIsCorrect(driver, By.CssSelector("div[class='lead']>strong:nth-of-type(1)"), "Test Name: " + SelectedTestName);
            test.validateStringIsCorrect(driver, By.CssSelector("div[class='lead']>strong:nth-of-type(2)"), "Package Family: All Packages");
            test.validateElementIsPresent(driver, Elements.QnR_AssemblyPackage_Result_Tbl);

            action.Navigate(driver, Configuration.Env_Url + Locale + Url);
            driver.SwitchTo().Frame(driver.FindElement(By.CssSelector("div[id='rte-body'] iframe")));
            string SelectedPackageFamName = util.GetAllValueInDropdownThenSelectOne(driver, Elements.QnR_AssemblyPackage_PackageFam_DD, 5);
            action.ISelectFromDropdown(driver, Elements.QnR_AssemblyPackage_PackageFam_DD, SelectedPackageFamName);
            action.IClick(driver, Elements.QnR_AssemblyPackage_Submit_Btn);
            Thread.Sleep(15000);
            test.validateStringIsCorrect(driver, By.CssSelector("div[class='lead']>strong:nth-of-type(1)"), "Test Name: All Tests");
            test.validateStringIsCorrect(driver, By.CssSelector("div[class='lead']>strong:nth-of-type(2)"), "Package Family: " + SelectedPackageFamName);
            test.validateElementIsPresent(driver, Elements.QnR_AssemblyPackage_Result_Tbl);
        }
    }
}