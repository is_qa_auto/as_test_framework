﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Threading;
using System;
using System.Linq;

namespace AS_Test_Framework.QualityAndReliability
{
    [TestFixture]
    public class Quality_KPI_LandingPage : BaseSetUp
    {
        public Quality_KPI_LandingPage() : base() { }

        string Url = "/design-center/packaging-quality-symbols-footprints/package-resources/keypackageinformation.html";

        [Test, Category("Quality And Reliability"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that user can navigate to KPI page via \"Design Center\" in EN Locale")]
        [TestCase("cn", TestName = "Verify that user can navigate to KPI page via \"Design Center\" in CN Locale")]
        [TestCase("jp", TestName = "Verify that user can navigate to KPI page via \"Design Center\" in JP Locale")]
        [TestCase("ru", TestName = "Verify that user can navigate to KPI page via \"Design Center\" in RU Locale")]
        public void QnR_KPI_VerifyNavigationToKPIPage(string Locale)
        {
            /*****via Design Center - Package Resources***/
            action.Navigate(driver, Configuration.Env_Url + Locale + "/index.html");
            action.IClick(driver, Elements.MainNavigation_DesignCenterTab);
            action.IClick(driver, By.CssSelector("div[class='designcenter menu content expanded'] div[class='row featured-section-sub']>div>div:nth-child(3) ul>li:nth-child(3)>a"));

            if (Locale.Equals("cn") || Locale.Equals("ru")) {
                action.IClick(driver, By.CssSelector("div[name='adi_twocolumn_content_section'] div[id='rte-body']>ul>li:nth-child(2)>a"));
            }
            else { 
            action.IClick(driver, By.CssSelector("div[name='adi_twocolumn_content_section'] div[id='rte-body']>ul>li:nth-child(4)>a"));
            }
            if (Locale.Equals("ru"))
            {
                driver.SwitchTo().Window(driver.WindowHandles.Last());
                test.validateStringInstance(driver,driver.Url, "analog.com/" + Locale + Url);
            }
            else {
                test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + Url);
            }
        }

        [Test, Category("Quality And Reliability"), Category("NonCore")]
        [TestCase("en", "Design Center", "Packaging, Quality, Symbols & Footprints", "Package Resources", "KeyPackageInformation", TestName = "Verify that breadcrumbs is present in KPI page for EN Locale")]
        [TestCase("cn", "设计资源", "封装、质量、原理图符号和尺寸", "封装资源", "重要封装信息", TestName = "Verify that breadcrumbs is present in KPI page for CN Locale")]
        [TestCase("jp", "設計支援", "パッケージング、クオリティ、シンボル ＆ フットプリント", "Package Resources", "KeyPackageInformation", TestName = "Verify that breadcrumbs is present in KPI page for JP Locale")]
        [TestCase("ru", "Ресурсы", "Корпуса, качество, обозначения компонентов и шаблоны посадочных мест на печатной плате", "Информация об исполнении ИС", "Ключевые характеристики корпусов", TestName = "Verify that breadcrumbs is present in KPI page for RU Locale")]
        public void QnR_KPI_VerifyBreadCrumbs1(string Locale, string FirstLevelCat, string SecondLevelCat, string ThirdLevelCat, string CurrentPage)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + Url);
            test.validateElementIsPresent(driver, Elements.Applications_BreadCrumbs);
            test.validateStringIsCorrect(driver, By.CssSelector("div[name='adi_breadcrumb'] ol>li:nth-child(2)>span>a"), FirstLevelCat);
            test.validateStringIsCorrect(driver, By.CssSelector("div[name='adi_breadcrumb'] ol>li:nth-child(3)>span>a"), SecondLevelCat);
            test.validateStringIsCorrect(driver, By.CssSelector("div[name='adi_breadcrumb'] ol>li:nth-child(4)>span>a"), ThirdLevelCat);
            test.validateStringIsCorrect(driver, By.CssSelector("div[name='adi_breadcrumb'] li[class='active']>span"), CurrentPage);
        }

        [Test, Category("Quality And Reliability"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that Material Search page link and Package Index Link is present in KPI Page")]
        public void QnR_KPI_VerifyComponents(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + Url);
            action.IClick(driver, By.CssSelector("div[id='rte-body']>a:nth-of-type(1)"));
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/about-adi/quality-reliability/material-declarations.html");
            driver.Navigate().Back();
            action.IClick(driver, By.CssSelector("div[id='rte-body']>a:nth-of-type(3)"));
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/design-center/packaging-quality-symbols-footprints/package-index.html");
        }
    }
}