﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Threading;
using System;

namespace AS_Test_Framework.QualityAndReliability
{
    [TestFixture]
    public class Quality_WaferFabrication_LandingPage : BaseSetUp
    {
        public Quality_WaferFabrication_LandingPage() : base() { }

        string Url = "/about-adi/quality-reliability/reliability-data/wafer-fabrication-data.html";

        [Test, Category("Quality And Reliability"), Category("NonCore")]
        [TestCase("en", "Wafer Fabrication Data", TestName = "Verify wafer fabrication page components are present for EN Locale")]
        [TestCase("cn", "晶圆制造数据", TestName = "Verify wafer fabrication page components are present for CN Locale")]
        [TestCase("jp", "Wafer Fabrication Data", TestName = "Verify wafer fabrication page components are present for JP Locale")]
        [TestCase("ru", "Результаты испытаний при изготовлении пластин", TestName = "Verify wafer fabrication page components are present for RU Locale")]
        public void QnR_ReliabilityData_VerifyWaferFabricationElements(string Locale, string ActiveLeftNav)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + Url);
            test.validateStringIsCorrect(driver, By.CssSelector("div[name='adi_threecolumn_centercolumn_section'] ul>li[class='active']>a"), ActiveLeftNav);
            test.validateStringIsCorrect(driver, Elements.QnR_SubCategory_heading, ActiveLeftNav);

            driver.SwitchTo().Frame(driver.FindElement(By.CssSelector("div[id='rte-body'] iframe")));

            test.validateElementIsPresent(driver, Elements.QnR_WaferFabrication_DataSummary_Table);
            test.validateElementIsPresent(driver, Elements.QnR_WaferFabrication_ReliabilityCalc_Link);

            test.validateStringIsCorrect(driver, By.CssSelector("table>tbody>tr>td:nth-child(1) h3[class='panel-title']"), "Product P/N");
            //test.validateStringIsCorrect(driver, By.CssSelector("table>tbody>tr>td:nth-child(3) h3[class='panel-title']"), "Product P/N");

        }

        [Test, Category("Quality And Reliability"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that the user can navigate to Details of Reliability Calculations page in EN Locale")]
        [TestCase("cn", TestName = "Verify that the user can navigate to Details of Reliability Calculations page in CN Locale")]
        [TestCase("jp", TestName = "Verify that the user can navigate to Details of Reliability Calculations page in JP Locale")]
        [TestCase("ru", TestName = "Verify that the user can navigate to Details of Reliability Calculations page in RU Locale")]
        public void QnR_ReliabilityData_VerifyDataCalculationLink(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + Url);
            driver.SwitchTo().Frame(driver.FindElement(By.CssSelector("div[id='rte-body'] iframe")));
            action.IClick(driver, Elements.QnR_WaferFabrication_ReliabilityCalc_Link);
            test.validateScreenByUrl(driver, Configuration.Env_Url + "en/about-adi/quality-reliability/reliability-data/wafer-fabrication-data/details-of-reliability-calculations.html");

        }
    }
}