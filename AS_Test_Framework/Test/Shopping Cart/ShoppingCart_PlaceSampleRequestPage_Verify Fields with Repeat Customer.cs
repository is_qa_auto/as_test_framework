﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Threading;

namespace AS_Test_Framework.ShoppingCart
{
    [TestFixture]
    public class ShoppingCart_PlaceSampleRequestPage_VerifyFieldsWithRepeatCustomer : BaseSetUp
    {
        public ShoppingCart_PlaceSampleRequestPage_VerifyFieldsWithRepeatCustomer() : base() { }

        //--- Login Credentials ---//
        string biz_username = "marvin.bebe@analog.com";
        string biz_password = "Test_1234";

        [Test, Category("Shopping Cart"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that the Fields are Present and Working as Expected after Adding a Model for Purchase using a Repeat Customer in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Fields are Present and Working as Expected after Adding a Model for Purchase using a Repeat Customer in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Fields are Present and Working as Expected after Adding a Model for Purchase using a Repeat Customer in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Fields are Present and Working as Expected after Adding a Model for Purchase using a Repeat Customer in RU Locale")]
        public void ShoppingCart_VerifyFieldsWithRepeatCustomerAfterAddingAModelForSample(string Locale)
        {
            //--- Shopping Cart URL ---//
            string sc_page_url = Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx";
            if (Locale.Equals("jp") || Locale.Equals("ru"))
            {
                sc_page_url = sc_page_url + "?locale=" + Locale;
            }
            else if (Locale.Equals("cn") || Locale.Equals("zh"))
            {
                sc_page_url = sc_page_url + "?locale=zh";
            }

            action.Navigate(driver, sc_page_url);
            action.ILoginViaLogInLink(driver, biz_username, biz_password);

            util.RemoveAllModelsInYourCartTable(driver);

            //--- Action: Get Test Data ---//
            string adi_model = util.GetAdiModel();

            action.IAddModelToSample(driver, adi_model);

            action.IClick(driver, Elements.Shopping_Cart_Checkout_Button);
            Thread.Sleep(3000);

            action.GoToAboutYourProjectPage(driver);

            action.IPopulateFieldsInAboutYourProjectPage(driver);

            //----- R10 > T2: Verify the Place Order button on the Place Sample Request page -----//

            //--- Expected Result: The following section should be displayed: Your Shopping Cart ---//
            test.validateElementIsPresent(driver, Elements.Shopping_Cart_PlaceOrder_YourShoppingCart_Tbl);
        }
    }
}