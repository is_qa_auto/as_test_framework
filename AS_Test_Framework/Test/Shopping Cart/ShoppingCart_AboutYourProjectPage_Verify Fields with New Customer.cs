﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Threading;
using OpenQA.Selenium;

namespace AS_Test_Framework.ShoppingCart
{
    [TestFixture]
    public class ShoppingCart_AboutYourProjectPage_VerifyFieldsWithNewCustomer : BaseSetUp
    {
        public ShoppingCart_AboutYourProjectPage_VerifyFieldsWithNewCustomer() : base() { }

        //--- Login Credentials ---//
        string newCust_username = "sc_t3st_new_555@mailinator.com";
        string newCust_password = "Test_1234";

        [Test, Category("Shopping Cart"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that the Fields are Present and Working as Expected after Adding a Model for Sample using a New Customer in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Fields are Present and Working as Expected after Adding a Model for Sample using a New Customer in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Fields are Present and Working as Expected after Adding a Model for Sample using a New Customer in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Fields are Present and Working as Expected after Adding a Model for Sample using a New Customer in RU Locale")]
        public void ShoppingCart_VerifyFieldsWithNewCustomerAfterAddingAModelForForSample(string Locale)
        {
            if (!Util.Configuration.Environment.Equals("production"))
            {
                //--- Shopping Cart URL ---//
                string sc_page_url = Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx";
                if (Locale.Equals("jp") || Locale.Equals("ru"))
                {
                    sc_page_url = sc_page_url + "?locale=" + Locale;
                }
                else if (Locale.Equals("cn") || Locale.Equals("zh"))
                {
                    sc_page_url = sc_page_url + "?locale=zh";
                }

                action.Navigate(driver, sc_page_url);
                action.ILoginViaLogInLink(driver, newCust_username, newCust_password);

                util.RemoveAllModelsInYourCartTable(driver);

                //--- Action: Get Test Data ---//
                string adi_model = util.GetAdiModel();

                action.IAddModelToSample(driver, adi_model);

                action.IClick(driver, Elements.Shopping_Cart_Checkout_Button);
                Thread.Sleep(3000);

                action.GoToAboutYourProjectPage(driver);

                //----- R8 > T2: Verify About Your Work or Studies section for new user -----//

                //--- Action: Select value on the 'How will you be using the samples provided?' question ---//
                action.IClick(driver, Elements.Shopping_Cart_AboutYourProject_SampleUsageUS_Dd);
                action.IClick(driver, By.CssSelector("select[id='ddlSampleUsageUS']>option:nth-child(2)"));

                //--- Expected Result: The following questions should be displayed: What is your organization's URL? ---//
                if (!Locale.Equals("jp"))
                {
                    test.validateElementIsPresent(driver, Elements.Shopping_Cart_AboutYourProject_OrgUrl_Txt);
                    test.validateElementIsPresent(driver, Elements.Shopping_Cart_AboutYourProject_OrgUrl_Tbx);
                }

                //--- Expected Result: The following questions should be displayed: What is your preferred distributor? ---//
                test.validateElementIsPresent(driver, Elements.Shopping_Cart_AboutYourProject_PrefDistributor_Txt);
                test.validateElementIsPresent(driver, Elements.Shopping_Cart_AboutYourProject_PrefDistributor_Dd);

                //--- Action: Change the value of the 'How will you be using the samples provided?' question to Personal/Hobbyist Use or Other ---//
                action.IClick(driver, Elements.Shopping_Cart_AboutYourProject_SampleUsageUS_Dd);
                action.IClick(driver, By.CssSelector("select[id='ddlSampleUsageUS']>option:nth-last-child(1)"));

                //--- Expected Result: The following question should be displayed: What is your preferred distributor ? ---//
                test.validateElementIsPresent(driver, Elements.Shopping_Cart_AboutYourProject_PrefDistributor_Txt);
                test.validateElementIsPresent(driver, Elements.Shopping_Cart_AboutYourProject_PrefDistributor_Dd);
            }
        }
    }
}