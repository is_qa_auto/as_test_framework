﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;

namespace AS_Test_Framework.ShoppingCart
{
    [TestFixture]
    public class ShoppingCart_ShoppingCartPage_VerifySelectShippingCountryDropdown : BaseSetUp
    {
        public ShoppingCart_ShoppingCartPage_VerifySelectShippingCountryDropdown() : base() { }

        //--- URLs ---//
        string homePage_url = "/index.html";
        string salesAndDistribution_page_url = "/sales-distribution.html";

        //--- Labels ---//
        string invalidCountry_errMsg = "Online purchasing and samples are not available for the country selected or your account. View Sales and Distributor map for other options.";

        [Test, Category("Shopping Cart"), Category("NonCore")]
        [TestCase("en", TestName = "Verify Selecting a Country where Sampling is Not Allowed as Anonymous User in EN Locale")]
        //[TestCase("cn", TestName = "Verify Selecting a Country where Sampling is Not Allowed as Anonymous User in CN Locale")]
        //[TestCase("jp", TestName = "Verify Selecting a Country where Sampling is Not Allowed as Anonymous User in JP Locale")]
        //[TestCase("ru", TestName = "Verify Selecting a Country where Sampling is Not Allowed as Anonymous User in RU Locale")]
        public void ShoppingCart_VerifySelectingACountryWhereSamplingIsNotAllowedAsAnonymousUser(string Locale)
        {
            //--- Shopping Cart URL ---//
            string sc_page_url = Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx";
            if (Locale.Equals("jp") || Locale.Equals("ru"))
            {
                sc_page_url = sc_page_url + "?locale=" + Locale;
            }
            else if (Locale.Equals("cn") || Locale.Equals("zh"))
            {
                sc_page_url = sc_page_url + "?locale=zh";
            }

            action.Navigate(driver, sc_page_url);

            Assert.Multiple(() =>
            {
                /*****Changes for AL-17509*****/
                //----- R4 > T2_1: Verify selected default country for cleared browser cookie -----//
                if (Locale.Equals("en"))
                {
                    test.validateStringIsCorrect(driver, Elements.Shopping_Cart_SelectShippingCountry_ErrorMsg, "Please select your country/region.");
                }
                else if (Locale.Equals("ru"))
                {
                    test.validateElementIsPresent(driver, Elements.Shopping_Cart_SelectShippingCountry_ErrorMsg);
                }

                //----- R4 > T3: Verify selected country where sampling is not allowed -----//

                //--- Select a country where sampling is not allowed (e.g. PHILIPPINESS) ---//
                action.IClick(driver, Elements.Shopping_Cart_Country_Dropdown);
                action.IClick(driver, By.CssSelector("li[value='PH']>a"));

                if (Locale.Equals("en"))
                {
                    test.validateStringIsCorrect(driver, Elements.SC_ErrorMessage_Label, invalidCountry_errMsg);
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.SC_ErrorMessage_Label);
                }

                //--- Action: Click on the View Sales and Distributor map link ---//
                action.IOpenLinkInNewTab(driver, Elements.Shopping_Cart_ViewSalesAndDistributorMap_Link);

                //--- Expected Result: The page redirects to Sales and Distribution page (AL-7115, AL-7479)---//
                test.validateStringInstance(driver, driver.Url, salesAndDistribution_page_url);
            });
        }

        [Test, Category("Shopping Cart"), Category("NonCore")]
        [TestCase("en", TestName = "Verify Selecting a Blocked Country as Anonymous User in EN Locale")]
        //[TestCase("cn", TestName = "Verify Selecting a Blocked Country as Anonymous User in CN Locale")]
        //[TestCase("jp", TestName = "Verify Selecting a Blocked Country as Anonymous User in JP Locale")]
        //[TestCase("ru", TestName = "Verify Selecting a Blocked Country as Anonymous User in RU Locale")]
        public void ShoppingCart_VerifySelectingABlockedCountryAsAnonymousUser(string Locale)
        {
            //--- Shopping Cart URL ---//
            string sc_page_url = Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx";
            if (Locale.Equals("jp") || Locale.Equals("ru"))
            {
                sc_page_url = sc_page_url + "?locale=" + Locale;
            }
            else if (Locale.Equals("cn") || Locale.Equals("zh"))
            {
                sc_page_url = sc_page_url + "?locale=zh";
            }

            action.Navigate(driver, sc_page_url);

            Assert.Multiple(() =>
            {
                //----- R4 > T4: Verify selected country where country is blocked -----//

                //--- Select a country where sampling is blocked (e.g. GUAM) ---//
                action.IClick(driver, Elements.Shopping_Cart_Country_Dropdown);
                action.IClick(driver, By.CssSelector("li[value='GU']>a"));

                if (Locale.Equals("en"))
                {
                    test.validateStringIsCorrect(driver, Elements.SC_ErrorMessage_Label, invalidCountry_errMsg);
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.SC_ErrorMessage_Label);
                }

                //----- R4 > T2_2: Verify selected default country for saved browser cookie -----//
                string country_selected = util.GetText(driver, Elements.Shopping_Cart_Country_Dropdown);

                //--- Access other pages on the site. (e.g. Analog Home Page)  ---//
                driver.Navigate().GoToUrl(Configuration.Env_Url + Locale + homePage_url);

                //--- User access again Shopping Cart page ---//
                driver.Navigate().GoToUrl(sc_page_url);

                test.validateStringIsCorrect(driver, Elements.Shopping_Cart_Country_Dropdown, country_selected);
            });
        }
    }
}