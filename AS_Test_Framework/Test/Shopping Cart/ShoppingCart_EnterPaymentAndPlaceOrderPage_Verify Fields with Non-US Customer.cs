﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Threading;

namespace AS_Test_Framework.ShoppingCart
{
    [TestFixture]
    public class ShoppingCart_EnterPaymentAndPlaceOrderPage_VerifyFieldsWithNonUsCustomer : BaseSetUp
    {
        public ShoppingCart_EnterPaymentAndPlaceOrderPage_VerifyFieldsWithNonUsCustomer() : base() { }

        //--- Login Credentials ---//
        string nonUs_username = "test_account_india1@mailinator.com";
        string nonUs_password = "Test_1234";

        [Test, Category("Shopping Cart"), Category("NonCore")]
        [TestCase("en", "Please Select", TestName = "Verify that the Fields are Present and Working as Expected after Adding a Model for Purchase using a Non-US Customer in EN Locale")]
        //[TestCase("cn", "请选择", TestName = "Verify that the Fields are Present and Working as Expected after Adding a Model for Purchase using a Non-US Customer in CN Locale")]
        //[TestCase("jp", "選択してください。", TestName = "Verify that the Fields are Present and Working as Expected after Adding a Model for Purchase using a Non-US Customer in JP Locale")]
        //[TestCase("ru", "Please Select", TestName = "Verify that the Fields are Present and Working as Expected after Adding a Model for Purchase using a Non-US Customer in RU Locale")]
        public void ShoppingCart_VerifyFieldsWithNonUsCustomerAfterAddingAModelForPurchase(string Locale, string PleaseSelect_Label)
        {
            //--- Shopping Cart URL ---//
            string sc_page_url = Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx";
            if (Locale.Equals("jp") || Locale.Equals("ru"))
            {
                sc_page_url = sc_page_url + "?locale=" + Locale;
            }
            else if (Locale.Equals("cn") || Locale.Equals("zh"))
            {
                sc_page_url = sc_page_url + "?locale=zh";
            }

            //--- Action: Access Shopping Cart page ---//
            action.Navigate(driver, sc_page_url);

            //--- Action: Login as registered user ---//
            action.ILoginViaLogInLink(driver, nonUs_username, nonUs_password);

            util.RemoveAllModelsInYourCartTable(driver);

            //--- Action: Get Test Data ---//
            string adi_model = util.GetAdiModel();

            //--- Action: User add products for purchase on the Cart ---//
            action.IAddModelToPurchase(driver, adi_model);

            //--- Action: User clicks Checkout ---//
            action.IClick(driver, Elements.Shopping_Cart_Checkout_Button);
            Thread.Sleep(3000);

            //--- Action: Complete all the mandatory fields on the Shipping Address page and click on the Next button ---//
            //--- Action: Complete all the mandatory fields on the Billing Address page and click on the Next button ---//
            action.GoToAboutYourProjectPage(driver);

            //--- Action: Complete all the mandatory fields on the About Your project pages and click on the Next button ---//
            action.IPopulateFieldsInAboutYourProjectPage(driver);

            //----- ECommerce TestPlan > Test Case Title: Verify Shipping Delivery method for Non-US accounts -----//

            //--- Expected Result: Single option should be preselected / defaulted on the Shipping Delivery method dropdown ---//
            //test.validateSelectedValueIsCorrect(driver, Elements.Shopping_Cart_PlaceOrder_ShippingMethod_Dd, "DHL WORLDWIDE");
            test.validateStringChanged(driver, util.GetSelectedDropdownValue(driver, Elements.Shopping_Cart_PlaceOrder_ShippingMethod_Dd), PleaseSelect_Label);
        }
    }
}