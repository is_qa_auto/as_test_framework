﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;

namespace AS_Test_Framework.ShoppingCart
{
    [TestFixture]
    public class ShoppingCart_ShoppingCartPage_VerifyAddingAModelForSample : BaseSetUp
    {
        public ShoppingCart_ShoppingCartPage_VerifyAddingAModelForSample() : base() { }

        //--- Login Credentials ---//
        string biz_username = "marvin.bebe@analog.com";
        string biz_password = "Test_1234";
        string nonBiz_username = "qa_automation_user2@mailinator.com";
        string nonBiz_password = "Test_1234";

        [Test, Category("Shopping Cart"), Category("NonCore")]
        [TestCase("en", TestName = "Verify Adding a Model for Sample using a Business Email in EN Locale")]
        //[TestCase("cn", TestName = "Verify Adding a Model for Sample using a Business Email in CN Locale")]
        //[TestCase("jp", TestName = "Verify Adding a Model for Sample using a Business Email in JP Locale")]
        //[TestCase("ru", TestName = "Verify Adding a Model for Sample using a Business Email in RU Locale")]
        public void ShoppingCart_VerifyAddingModelForSampleUsingABusinessEmail(string Locale)
        {
            //--- Shopping Cart URL ---//
            string sc_page_url = Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx";
            if (Locale.Equals("jp") || Locale.Equals("ru"))
            {
                sc_page_url = sc_page_url + "?locale=" + Locale;
            }
            else if (Locale.Equals("cn") || Locale.Equals("zh"))
            {
                sc_page_url = sc_page_url + "?locale=zh";
            }

            action.Navigate(driver, sc_page_url);
            action.ILoginViaLogInLink(driver, biz_username, biz_password);

            util.RemoveAllModelsInYourCartTable(driver);

            string adi_model = util.GetAdiModel();
            action.IAddModelToSample(driver, adi_model);

            //----- R2 > T4_2: Verify model number field with model that is sampleable -----//
            test.validateStringIsCorrect(driver, Elements.Shopping_Cart_AddedModel, adi_model.ToUpper());
        }

        [Test, Category("Shopping Cart"), Category("NonCore")]
        [TestCase("en", TestName = "Verify Adding a Model for Sample using a Non-business Email in EN Locale")]
        //[TestCase("cn", TestName = "Verify Adding a Model for Sample using a Non-business Email in CN Locale")]
        //[TestCase("jp", TestName = "Verify Adding a Model for Sample using a Non-business Email in JP Locale")]
        //[TestCase("ru", TestName = "Verify Adding a Model for Sample using a Non-business Email in RU Locale")]
        public void ShoppingCart_VerifyAddingModelForSampleUsingANonBusinessEmail(string Locale)
        {
            if (Util.Configuration.Environment.Equals("production"))
            {
                //--- Shopping Cart URL ---//
                string sc_page_url = Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx";
                if (Locale.Equals("jp") || Locale.Equals("ru"))
                {
                    sc_page_url = sc_page_url + "?locale=" + Locale;
                }
                else if (Locale.Equals("cn") || Locale.Equals("zh"))
                {
                    sc_page_url = sc_page_url + "?locale=zh";
                }

                action.Navigate(driver, sc_page_url);
                action.ILoginViaLogInLink(driver, nonBiz_username, nonBiz_password);

                util.RemoveAllModelsInYourCartTable(driver);

                string adi_model = util.GetAdiModel();
                action.IAddModelToSample(driver, adi_model);

                //----- R2 > T4_2: Verify model number field with model that is sampleable -----//
                if (Locale.Equals("en"))
                {
                    test.validateStringIsCorrect(driver, Elements.SC_SampleAbuse_WarningMessage_Label, "We apologize that we cannot process your request containing one or more samples from a non-business email address (such as AOL, 163.com, etc.). To request samples please register using a valid business email address or use an existing corporate email registered with ADI. View Sales and Distributor map or contact Customer Service for other options.");
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.SC_SampleAbuse_WarningMessage_Label);
                }
            }
        }

        [Test, Category("Shopping Cart"), Category("NonCore")]
        [TestCase("en", TestName = "Verify Adding an HMC Model for Sample as Anonymous User in EN Locale")]
        //[TestCase("cn", TestName = "Verify Adding an HMC Model for Sample as Anonymous User in CN Locale")]
        //[TestCase("jp", TestName = "Verify Adding an HMC Model for Sample as Anonymous User in JP Locale")]
        //[TestCase("ru", TestName = "Verify Adding an HMC Model for Sample as Anonymous User in RU Locale")]
        public void ShoppingCart_VerifyAddingAnHmcModelForSampleUsingAsAnonymousUser(string Locale)
        {
            //--- Shopping Cart URL ---//
            string sc_page_url = Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx";
            if (Locale.Equals("jp") || Locale.Equals("ru"))
            {
                sc_page_url = sc_page_url + "?locale=" + Locale;
            }
            else if (Locale.Equals("cn") || Locale.Equals("zh"))
            {
                sc_page_url = sc_page_url + "?locale=zh";
            }

            //----- R28 > T1: Verify if hmc sample redirected third-party website for order processing (IQ-8942/AL-15986) -----//

            //--- Action: go to shopping cart page. ---//
            action.Navigate(driver, sc_page_url);

            //--- Action: Select any country on the Select Shipping Country dropdown (e.g. UNITED STATES) ---//
            action.IClick(driver, Elements.Shopping_Cart_Country_Dropdown);
            action.IClick(driver, By.CssSelector("li[value='US']>a"));

            util.RemoveAllModelsInYourCartTable(driver);

            //--- Action: Enter model number. ---//
            //--- Action: Tick Sample radiobox ---//
            //--- Action: Click Add to cart button ---//
            string hmc_model = util.GetHmcModel();
            action.IAddModelToSample(driver, hmc_model);

            //--- Expected Result: Warning message displayed ---//
            if (Locale.Equals("en"))
            {
                //test.validateStringIsCorrect(driver, Elements.SC_ErrorMessage_Label, "This product is not available through the standard analog.com sample process, to sample this product, request HMC product sample");
                test.validateStringIsCorrect(driver, Elements.SC_ErrorMessage_Label, hmc_model.ToUpper() + " is not available for sample on www.analog.com. Go to " + hmc_model.ToUpper() + " product page to view other options.");
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.SC_ErrorMessage_Label);
            }
        }
    }
}