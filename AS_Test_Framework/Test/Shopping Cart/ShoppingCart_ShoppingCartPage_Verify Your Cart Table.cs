﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Threading;

namespace AS_Test_Framework.ShoppingCart
{
    [TestFixture]
    public class ShoppingCart_ShoppingCartPage_VerifyYourCartTable : BaseSetUp
    {
        public ShoppingCart_ShoppingCartPage_VerifyYourCartTable() : base() { }

        //--- Login Credentials ---//
        string biz_username = "marvin.bebe@analog.com";
        string biz_password = "Test_1234";

        [Test, Category("Shopping Cart"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that the Your Cart Table is Present and Working as Anonymous User in EN Locale")]
        //[TestCase("ru", TestName = "Verify that the Your Cart Table is Present and Working as Anonymous User in RU Locale")]
        public void ShoppingCart_VerifyYourCartTableAsAnonymousUser(string Locale)
        {
            //--- Shopping Cart URL ---//
            string sc_page_url = Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx";
            if (Locale.Equals("jp") || Locale.Equals("ru"))
            {
                sc_page_url = sc_page_url + "?locale=" + Locale;
            }
            else if (Locale.Equals("cn") || Locale.Equals("zh"))
            {
                sc_page_url = sc_page_url + "?locale=zh";
            }

            action.Navigate(driver, sc_page_url);

            Assert.Multiple(() =>
            {

                /*****Changes for AL-17509*****/
                //----- R4 > T2_1 > S3: Verify the Add to Cart section is not displayed. -----//
                test.validateElementIsNotPresent(driver, Elements.Shopping_Cart_AddToCart_Sec);

                ////----- R4 > T5 > S1: The Add to Cart section should be displayed -----//

                //--- Select any country on the Select Shipping Country dropdown (e.g. UNITED STATES) ---//
                action.IClick(driver, Elements.Shopping_Cart_Country_Dropdown);
                action.IClick(driver, By.CssSelector("li[value='US']>a"));

                test.validateElementIsPresent(driver, Elements.Shopping_Cart_AddToCart_Sec);
            });
        }

        [Test, Category("Shopping Cart"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that the Remove Button is Present and Working as Expected using a Business Email in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Remove Button is Present and Working as Expected using a Business Email in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Remove Button is Present and Working as Expected using a Business Email in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Remove Button is Present and Working as Expected using a Business Email in RU Locale")]
        public void ShoppingCart_VerifyRemoveButtonUsingABusinessEmail(string Locale)
        {
            //--- Shopping Cart URL ---//
            string sc_page_url = Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx";
            if (Locale.Equals("jp") || Locale.Equals("ru"))
            {
                sc_page_url = sc_page_url + "?locale=" + Locale;
            }
            else if (Locale.Equals("cn") || Locale.Equals("zh"))
            {
                sc_page_url = sc_page_url + "?locale=zh";
            }

            action.Navigate(driver, sc_page_url);
            action.ILoginViaLogInLink(driver, biz_username, biz_password);

            util.RemoveAllModelsInYourCartTable(driver);

            //----- R2 > T16: Verify the Remove link -----//

            //--- Action: Get Test Data ---//
            string adi_model = util.GetAdiModel(); ;

            action.IAddModelToPurchase(driver, adi_model);

            //--- Click on the Remove link ---//
            action.IClick(driver, Elements.Shopping_Cart_Remove_Btn);
            Thread.Sleep(2000);

            test.validateElementIsNotPresent(driver, By.CssSelector("table[class$='cart-table'] * tr:nth-of-type(2)"));
        }
    }
}