﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Threading;

namespace AS_Test_Framework.ShoppingCart
{
    [TestFixture]
    public class ShoppingCart_OrderConfirmationPage_VerifyPlaceOrderPerEacModel : BaseSetUp
    {
        public ShoppingCart_OrderConfirmationPage_VerifyPlaceOrderPerEacModel() : base() { }

        //--- Login Credentials ---//
        string username = "sc_t3st_cn_777@mailinator.com";
        string password = "Test_1234";

        //--- URLs ---//
        string orderConfirmation_url = "OrderConfirmation.aspx";

        [Test, Category("Shopping Cart"), Category("NonCore")]
        [TestCase("cn", TestName = "Verify that the Order Confirmation Page is Present and Working as Expected after Placing 1 ADI Model for Sample, 1 ADI Model for Purchase, 1 LTC Model for Sample and 1 LTC Model for Purchase  using a Chinese Account in CN Locale")]
        public void ShoppingCart_VerifyOneAdiModelSample_OneAdiModelPurchase_OneLtcModelSample_AndOneLtcModelPurchase_UsingAChineseAccount(string Locale)
        {
            if (!Util.Configuration.Environment.Equals("production") && ((Locale.Equals("cn") || Locale.Equals("zh"))))
            {
                //--- Shopping Cart URL ---//
                string sc_page_url = Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx";
                if (Locale.Equals("jp") || Locale.Equals("ru"))
                {
                    sc_page_url = sc_page_url + "?locale=" + Locale;
                }
                else if (Locale.Equals("cn") || Locale.Equals("zh"))
                {
                    sc_page_url = sc_page_url + "?locale=zh";
                }

                action.Navigate(driver, sc_page_url);

                action.ILoginViaLogInLink(driver, username, password);

                util.RemoveAllModelsInYourCartTable(driver);

                //----- R20 > T14 > S2: Sample and Purchase LTC product + Sample and Purchase AD product -----//

                //--- Action: Get Test Data ---//
                string adi_model = util.GetAdiModel();
                string ltc_model = util.GetLtcModel();

                action.IAddModelToSample(driver, adi_model);
                action.IAddModelToPurchase(driver, adi_model);
                action.IAddModelToSample(driver, ltc_model);
                action.IAddModelToPurchase(driver, ltc_model);

                action.IClick(driver, Elements.Shopping_Cart_Checkout_Button);
                Thread.Sleep(3000);

                action.IPopulateFieldsInShippingAddressPage(driver);

                action.IPopulateFieldsInBillingAddressPage(driver);

                action.IPopulateFieldsInAboutYourProjectPage(driver);

                action.IPopulateFieldsInEnterPaymentAndPlaceOrderPage(driver);

                if (driver.Url.Contains("analog.com/" + orderConfirmation_url + "?locale="))
                {
                    //--- Expected Result: In Order Confirmation page there should  have 2 order number one for both LTC purchase and AD purchase and 1 for AD sample ---//
                    test.validateCountIsEqual(driver, 2, util.GetCount(driver, Elements.Shopping_Cart_OrderConfirmation_OrderRefNos));

                    //--- Cancel Orders in myAnalog ---//
                    action.ICancelOrdersInMyAnalog(driver, username, password);
                }
                else
                {
                    if (Locale.Equals("cn") || Locale.Equals("zh"))
                    {
                        test.validateStringInstance(driver, driver.Url, "analog.com/" + orderConfirmation_url + "?locale=zh");
                    }
                    else
                    {
                        test.validateStringInstance(driver, driver.Url, "analog.com/" + orderConfirmation_url + "?locale=" + Locale);
                    }
                }
            }
        }
    }
}