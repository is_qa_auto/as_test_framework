﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Threading;

namespace AS_Test_Framework.ShoppingCart
{
    [TestFixture]
    public class ShoppingCart_AboutYourProjectPage_VerifyFieldsWithNonUsCustomer : BaseSetUp
    {
        public ShoppingCart_AboutYourProjectPage_VerifyFieldsWithNonUsCustomer() : base() { }

        //--- Login Credentials ---//
        string nonUs_username = "test_account_india1@mailinator.com";
        string nonUs_password = "Test_1234";

        ////--- Labels ---//
        //string nuclear_errMsg = "Your order privilege has been blocked. We cannot currently sell online our products for nuclear, missile, chemical and biological or military applications/facilities.";

        //[Test, Category("Shopping Cart"), Category("NonCore")]
        //[TestCase("en", TestName = "Verify that the Fields are Present and Working as Expected after Adding a Model for Purchase using a Non-Us Customer in EN Locale")]
        ////[TestCase("cn", TestName = "Verify that the Fields are Present and Working as Expected after Adding a Model for Purchase using a Non-Us Customer in CN Locale")]
        ////[TestCase("jp", TestName = "Verify that the Fields are Present and Working as Expected after Adding a Model for Purchase using a Non-Us Customer in JP Locale")]
        ////[TestCase("ru", TestName = "Verify that the Fields are Present and Working as Expected after Adding a Model for Purchase using a Non-Us Customer in RU Locale")]
        //public void ShoppingCart_VerifyFieldsWithNonUsCustomerAfterAddingAModelForForPurchase(string Locale)
        //{
        //    //--- Shopping Cart URL ---//
        //    string sc_page_url = Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx";
        //    if (Locale.Equals("jp") || Locale.Equals("ru"))
        //    {
        //        sc_page_url = sc_page_url + "?locale=" + Locale;
        //    }
        //    else if (Locale.Equals("cn") || Locale.Equals("zh"))
        //    {
        //        sc_page_url = sc_page_url + "?locale=zh";
        //    }

        //    action.Navigate(driver, sc_page_url);
        //    action.ILoginViaLogInLink(driver, nonUs_username, nonUs_password);

        //    util.RemoveAllModelsInYourCartTable(driver);

        //    //--- Action: Get Test Data ---//
        //    string adi_model = util.GetAdiModel();

        //    action.IAddModelToPurchase(driver, adi_model);

        //    action.IClick(driver, Elements.Shopping_Cart_Checkout_Button);
        //    Thread.Sleep(3000);

        //    action.GoToAboutYourProjectPage(driver);

        //    //----- R25 > T1.3: Verify questions for India, China, Europian Country  (Purchase) -----//

        //    //--- Expected Result: The "Which of the following best describes your design phase?" questions is displayed ---//
        //    test.validateElementIsPresent(driver, Elements.Shopping_Cart_AboutYourProject_DesignPhase_Dd);

        //    //--- Expected Result: The "What is your best estimate of when the product will go into production?" questions is displayed ---//
        //    test.validateElementIsPresent(driver, Elements.Shopping_Cart_AboutYourProject_BestEstimate_Dd);

        //    //--- Expected Result: The "What is your best estimate of how many components you will use on a yearly basis?" questions is displayed ---//
        //    test.validateElementIsPresent(driver, Elements.Shopping_Cart_AboutYourProject_ComponentQty_Dd);

        //    //--- Expected Result: The "What is the application / end use of these products?" questions is displayed ---//
        //    test.validateElementIsPresent(driver, Elements.Shopping_Cart_AboutYourProject_EndUse_Dd);

        //    //--- Expected Result: The "Are you buying these components for a nuclear, a missile, chemical & biological, or military application or facility?" questions is displayed ---//
        //    test.validateElementIsPresent(driver, Elements.Shopping_Cart_AboutYourProject_Nuclear_Rbs);

        //    //--- Expected Result: The "Will you be re-exporting this product outside of your country without putting it in an application?" questions is displayed ---//
        //    test.validateElementIsPresent(driver, Elements.Shopping_Cart_AboutYourProject_ReExporting_Rbs);

        //    if (util.CheckElement(driver, Elements.Shopping_Cart_AboutYourProject_SamplesQuestions_Sec, 2))
        //    {
        //        //--- * Occupation Dropdown (AL-14727: UI Changes for shopping cart changes for leads information) ---//
        //        action.IClick(driver, Elements.Shopping_Cart_AboutYourProject_Occupation_Dd);
        //        action.IClick(driver, By.CssSelector("div[class='adi-tree dropdown-menu']>ul>li:nth-child(1) * i"));
        //        action.IClick(driver, By.CssSelector("div[class='adi-tree dropdown-menu']>ul>li:nth-child(1)>ul>li:nth-child(1)>a"));

        //        Dictionary<string, string> select_value = new Dictionary<string, string>();

        //        if (util.CheckElement(driver, Elements.Shopping_Cart_AboutYourProject_SampleUsageUS_Dd, 10))
        //        {
        //            //--- *How will you be using the samples provided? Dropdown ---//
        //            select_value.Add("select[id='ddlSampleUsageUS']", "Academic Use");

        //            //--- *What is your preferred distributor? Dropdown ---//
        //            select_value.Add("select[id='distributor']", "Arrow");
        //        }

        //        //--- * Which of the following best describes your design phase? Dropdown ---//
        //        select_value.Add("select[id='designphase']", "Requirements Definition");

        //        //--- * What is your best estimate of when the product will go into production? Dropdown ---//
        //        select_value.Add("select[id='bestestimate']", "0-3 months");

        //        //--- * What is your best estimate of how many components you will use on a yearly basis? Dropdown ---//
        //        select_value.Add("select[id='componentqty']", "1-999");

        //        foreach (KeyValuePair<string, string> value in select_value)
        //        {
        //            action.ISelectFromDropdown(driver, By.CssSelector(value.Key), value.Value);
        //        }
        //    }

        //    //--- *What is the application / end use of these products? Dropdown ---//
        //    action.IClick(driver, Elements.Shopping_Cart_AboutYourProject_EndUse_Dd);
        //    action.IClick(driver, By.CssSelector("a[title='Aerospace & Defense']"));
        //    action.IClick(driver, By.CssSelector("a[title='Radar']"));

        //    //--- Are you buying these components for a nuclear, a missile, chemical & biological, or military application or facility? ---//
        //    if (util.CheckElement(driver, Elements.Shopping_Cart_AboutYourProject_NonUSQuestions_Sec, 2))
        //    {
        //        //--- Action: Select "Yes" on the 'Are you buying these components for a nuclear, a missile, chemical & biological, or military application or facility?' question  ---//
        //        action.IClick(driver, Elements.Shopping_Cart_AboutYourProject_Nuclear_Yes_Rb);
        //    }

        //    //--- *Will you be re-exporting this product outside of your country without putting it in an application? ---//

        //    //--- Reexporting - No Radio Button ---//
        //    action.IClick(driver, Elements.Shopping_Cart_AboutYourProject_ReExporting_No_Rb);

        //    //--- Next Button ---//
        //    action.IClick(driver, Elements.Shopping_Cart_Footer_Next_Button);

        //    //--- Expected Result: The error message should be displayed ----//
        //    if (Locale.Equals("en"))
        //    {
        //        test.validateStringIsCorrect(driver, Elements.SC_ErrorMessage_Label, nuclear_errMsg);
        //    }
        //    else
        //    {
        //        test.validateElementIsPresent(driver, Elements.SC_ErrorMessage_Label);
        //    }
        //}

        [Test, Category("Shopping Cart"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that the Fields are Present and Working as Expected after Adding a Model for Sample using a Non-Us Customer in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Fields are Present and Working as Expected after Adding a Model for Sample using a Non-Us Customer in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Fields are Present and Working as Expected after Adding a Model for Sample using a Non-Us Customer in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Fields are Present and Working as Expected after Adding a Model for Sample using a Non-Us Customer in RU Locale")]
        public void ShoppingCart_VerifyFieldsWithNonUsCustomerAfterAddingAModelForForSample(string Locale)
        {
            if (!Util.Configuration.Environment.Equals("production"))
            {
                //--- Shopping Cart URL ---//
                string sc_page_url = Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx";
                if (Locale.Equals("jp") || Locale.Equals("ru"))
                {
                    sc_page_url = sc_page_url + "?locale=" + Locale;
                }
                else if (Locale.Equals("cn") || Locale.Equals("zh"))
                {
                    sc_page_url = sc_page_url + "?locale=zh";
                }

                action.Navigate(driver, sc_page_url);
                action.ILoginViaLogInLink(driver, nonUs_username, nonUs_password);

                util.RemoveAllModelsInYourCartTable(driver);

                //--- Action: Get Test Data ---//
                string adi_model = util.GetAdiModel();

                action.IAddModelToSample(driver, adi_model);

                action.IClick(driver, Elements.Shopping_Cart_Checkout_Button);
                Thread.Sleep(3000);

                action.GoToAboutYourProjectPage(driver);

                //----- R25 > T1.1: Verify questions for India, China, Europian Country  (Sample) -----//

                //--- Expected Result: The "Which of the following best describes your design phase?" questions is displayed ---//
                test.validateElementIsPresent(driver, Elements.Shopping_Cart_AboutYourProject_DesignPhase_Dd);

                //--- Expected Result: The "What is your best estimate of when the product will go into production?" questions is displayed ---//
                test.validateElementIsPresent(driver, Elements.Shopping_Cart_AboutYourProject_BestEstimate_Dd);

                //--- Expected Result: The "What is your best estimate of how many components you will use on a yearly basis?" questions is displayed ---//
                test.validateElementIsPresent(driver, Elements.Shopping_Cart_AboutYourProject_ComponentQty_Dd);

                //--- Expected Result: The "What is the application / end use of these products?" questions is displayed ---//
                test.validateElementIsPresent(driver, Elements.Shopping_Cart_AboutYourProject_EndUse_Dd);

                //--- Expected Result: The "Are you buying these components for a nuclear, a missile, chemical & biological, or military application or facility?" questions is displayed ---//
                test.validateElementIsPresent(driver, Elements.Shopping_Cart_AboutYourProject_Nuclear_Rbs);

                //--- Expected Result: The "Will you be re-exporting this product outside of your country without putting it in an application?" questions is displayed ---//
                test.validateElementIsPresent(driver, Elements.Shopping_Cart_AboutYourProject_ReExporting_Rbs);
            }
        }
    }
}