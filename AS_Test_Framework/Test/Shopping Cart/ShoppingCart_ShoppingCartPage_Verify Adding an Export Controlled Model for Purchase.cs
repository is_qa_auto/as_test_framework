﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;

namespace AS_Test_Framework.ShoppingCart
{
    [TestFixture]
    public class ShoppingCart_ShoppingCartPage_VerifyAddingAnExportControlledModelForPurchase : BaseSetUp
    {
        public ShoppingCart_ShoppingCartPage_VerifyAddingAnExportControlledModelForPurchase() : base() { }

        //--- Login Credentials ---//
        string biz_username = "marvin.bebe@analog.com";
        string biz_password = "Test_1234";
        string nonBiz_username = "qa_automation_user2@mailinator.com";
        string nonBiz_password = "Test_1234";

        //--- Test Data ---//
        string exportControlled_model = "AD9208BBPZ-3000";
        //string exportControlled_model = "AD9162BBCAZ";

        //--- Labels ---//
        string exportControlledModel_errMsg = " is controlled for export. ADI policy does not permit export controlled products to be ordered or sampled on line. View sales & distributor map to contact local sales or distribution office for options. (Error : 021)";

        [Test, Category("Shopping Cart"), Category("NonCore")]
        [TestCase("en", TestName = "Verify Adding an Export Controlled Model for Purchase using a Business Email in EN Locale")]
        //[TestCase("cn", TestName = "Verify Adding an Export Controlled Model for Purchase using a Business Email in CN Locale")]
        //[TestCase("jp", TestName = "Verify Adding an Export Controlled Model for Purchase using a Business Email in JP Locale")]
        //[TestCase("ru", TestName = "Verify Adding an Export Controlled Model for Purchase using a Business Email in RU Locale")]
        public void ShoppingCart_VerifyAddingAnExportControlledModelForPurchaseUsingABusinessEmail(string Locale)
        {
            //--- Shopping Cart URL ---//
            string sc_page_url = Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx";
            if (Locale.Equals("jp") || Locale.Equals("ru"))
            {
                sc_page_url = sc_page_url + "?locale=" + Locale;
            }
            else if (Locale.Equals("cn") || Locale.Equals("zh"))
            {
                sc_page_url = sc_page_url + "?locale=zh";
            }

            action.Navigate(driver, sc_page_url);
            action.ILoginViaLogInLink(driver, biz_username, biz_password);

            util.RemoveAllModelsInYourCartTable(driver);

            //----- R2 > T5: Verify the Message for Parts that are Export Controlled -----//
            action.IAddModelToPurchase(driver, exportControlled_model);

            if (Locale.Equals("en"))
            {
                test.validateStringIsCorrect(driver, Elements.SC_ErrorMessage_Label, exportControlled_model.ToUpper() + exportControlledModel_errMsg);
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.SC_ErrorMessage_Label);
            }
        }

        [Test, Category("Shopping Cart"), Category("NonCore")]
        [TestCase("en", TestName = "Verify Adding an Export Controlled Model for Purchase using a Non-business Email in EN Locale")]
        //[TestCase("cn", TestName = "Verify Adding an Export Controlled Model for Purchase using a Non-business Email in CN Locale")]
        //[TestCase("jp", TestName = "Verify Adding an Export Controlled Model for Purchase using a Non-business Email in JP Locale")]
        //[TestCase("ru", TestName = "Verify Adding an Export Controlled Model for Purchase using a Non-business Email in RU Locale")]
        public void ShoppingCart_VerifyAddingAnExportControlledModelForPurchaseUsingANonBusinessEmail(string Locale)
        {
            //--- Shopping Cart URL ---//
            string sc_page_url = Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx";
            if (Locale.Equals("jp") || Locale.Equals("ru"))
            {
                sc_page_url = sc_page_url + "?locale=" + Locale;
            }
            else if (Locale.Equals("cn") || Locale.Equals("zh"))
            {
                sc_page_url = sc_page_url + "?locale=zh";
            }

            action.Navigate(driver, sc_page_url);
            action.ILoginViaLogInLink(driver, nonBiz_username, nonBiz_password);

            util.RemoveAllModelsInYourCartTable(driver);

            //----- R2 > T5: Verify the Message for Parts that are Export Controlled -----//
            action.IAddModelToPurchase(driver, exportControlled_model);

            if (Locale.Equals("en"))
            {
                test.validateStringIsCorrect(driver, Elements.SC_ErrorMessage_Label, exportControlled_model.ToUpper() + exportControlledModel_errMsg);
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.SC_ErrorMessage_Label);
            }
        }

        //[Test, Category("Shopping Cart"), Category("NonCore")] //---> PENDING.The Error Message is Not Displaying.Still confirming with Manual Testing Team if there's a Logged Ticket for this.
        //[TestCase("en", TestName = "Verify Adding an Export Controlled Model for Purchase using as Anonymous User in EN Locale")]
        ////[TestCase("cn", TestName = "Verify Adding an Export Controlled Model for Purchase using as Anonymous User in CN Locale")]
        ////[TestCase("jp", TestName = "Verify Adding an Export Controlled Model for Purchase using as Anonymous User in JP Locale")]
        ////[TestCase("ru", TestName = "Verify Adding an Export Controlled Model for Purchase using as Anonymous User in RU Locale")]
        //public void ShoppingCart_VerifyAddingAnExportControlledModelForPurchaseUsingAsAnonymousUser(string Locale)
        //{
        //    //--- Shopping Cart URL ---//
        //    string sc_page_url = Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx";
        //    if (Locale.Equals("jp") || Locale.Equals("ru"))
        //    {
        //        sc_page_url = sc_page_url + "?locale=" + Locale;
        //    }
        //    else if (Locale.Equals("cn") || Locale.Equals("zh"))
        //    {
        //        sc_page_url = sc_page_url + "?locale=zh";
        //    }

        //    //--- Test Data ---//
        //    string exportControlled_model = "AD9162BBCAZ";
        //    //string exportControlled_model = "AD9208BBPZ-3000";

        //    action.Navigate(driver, sc_page_url);

        //    //--- Select a Shipping Country ---//
        //    if (!Locale.Equals("en"))
        //    {
        //        action.IClick(driver, By.CssSelector("li[value='" + Locale.ToUpper() + "']"));
        //    }
        //    else
        //    {
        //        action.IClick(driver, By.CssSelector("li[value='US']"));
        //    }

        //    util.RemoveAllModelsInYourCartTable(driver);

        //    //----- R2 > T5: Verify the Message for Parts that are Export Controlled -----//
        //    action.IAddModelToPurchase(driver, exportControlled_model);

        //    if (Locale.Equals("en"))
        //    {
        //        test.validateStringIsCorrect(driver, Elements.SC_ErrorMessage_Label, exportControlled_model.ToUpper() + exportControlledModel_errMsg);
        //    }
        //    else
        //    {
        //        test.validateElementIsPresent(driver, Elements.SC_ErrorMessage_Label);
        //    }
        //}
    }
}