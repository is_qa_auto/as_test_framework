﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;

namespace AS_Test_Framework.ShoppingCart
{
    [TestFixture]
    public class ShoppingCart_ShoppingCartPage_VerifyAddingAModelForPurchase : BaseSetUp
    {
        public ShoppingCart_ShoppingCartPage_VerifyAddingAModelForPurchase() : base() { }

        //--- Login Credentials ---//
        string biz_username = "marvin.bebe@analog.com";
        string biz_password = "Test_1234";
        string nonBiz_username = "qa_automation_user2@mailinator.com";
        string nonBiz_password = "Test_1234";

        //--- Labels ---//
        string alreadyExistingModel_errMsg = "Your shopping cart already contains model ";

        [Test, Category("Shopping Cart"), Category("NonCore")]
        [TestCase("en", TestName = "Verify Adding a Model with Trailing Spaces for Purchase using a Business Email in EN Locale")]
        //[TestCase("cn", TestName = "Verify Adding a Model with Trailing Spaces for Purchase using a Business Email in CN Locale")]
        //[TestCase("jp", TestName = "Verify Adding a Model with Trailing Spaces for Purchase using a Business Email in JP Locale")]
        //[TestCase("ru", TestName = "Verify Adding a Model with Trailing Spaces for Purchase using a Business Email in RU Locale")]
        public void ShoppingCart_VerifyAddingModelWithTrailingSpacesForPurchaseUsingABusinessEmail(string Locale)
        {
            //--- Shopping Cart URL ---//
            string sc_page_url = Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx";
            if (Locale.Equals("jp") || Locale.Equals("ru"))
            {
                sc_page_url = sc_page_url + "?locale=" + Locale;
            }
            else if (Locale.Equals("cn") || Locale.Equals("zh"))
            {
                sc_page_url = sc_page_url + "?locale=zh";
            }

            action.Navigate(driver, sc_page_url);
            action.ILoginViaLogInLink(driver, biz_username, biz_password);

            util.RemoveAllModelsInYourCartTable(driver);

            Assert.Multiple(() =>
            {
                //----- R2 > T14: Verify Adding Models that has Trailing Spaces (AL-10649/IQ-3724) -----//

                //----- R2 > T4_3: Verify model number field with model that is purchaseable -----//
                string adi_model = util.GetAdiModel();
                action.IAddModelToPurchase(driver, adi_model + " ");

                test.validateStringIsCorrect(driver, Elements.Shopping_Cart_AddedModel, adi_model.ToUpper());

                //----- R2 > T10: Verify Add to Cart button where product to be added is already existing on the cart -----//
                action.IAddModelToPurchase(driver, adi_model);

                if (Locale.Equals("en"))
                {
                    test.validateStringIsCorrect(driver, Elements.SC_ErrorMessage_Label, alreadyExistingModel_errMsg + adi_model.ToUpper());
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.SC_ErrorMessage_Label);
                }
            });
        }

        [Test, Category("Shopping Cart"), Category("NonCore")]
        [TestCase("en", TestName = "Verify Adding a Model with Trailing Spaces for Purchase using a Non-business Email in EN Locale")]
        //[TestCase("cn", TestName = "Verify Adding a Model with Trailing Spaces for Purchase using a Non-business Email in CN Locale")]
        //[TestCase("jp", TestName = "Verify Adding a Model with Trailing Spaces for Purchase using a Non-business Email in JP Locale")]
        //[TestCase("ru", TestName = "Verify Adding a Model with Trailing Spaces for Purchase using a Non-business Email in RU Locale")]
        public void ShoppingCart_VerifyAddingModelWithTrailingSpacesForPurchaseUsingANonBusinessEmail(string Locale)
        {
            //--- Shopping Cart URL ---//
            string sc_page_url = Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx";
            if (Locale.Equals("jp") || Locale.Equals("ru"))
            {
                sc_page_url = sc_page_url + "?locale=" + Locale;
            }
            else if (Locale.Equals("cn") || Locale.Equals("zh"))
            {
                sc_page_url = sc_page_url + "?locale=zh";
            }

            action.Navigate(driver, sc_page_url);
            action.ILoginViaLogInLink(driver, nonBiz_username, nonBiz_password);

            util.RemoveAllModelsInYourCartTable(driver);

            Assert.Multiple(() =>
            {
                //----- R3 > T2_1: Verify adding model number for purchase -----//
                string adi_model = util.GetAdiModel();
                action.IAddModelToPurchase(driver, adi_model + " ");

                test.validateStringIsCorrect(driver, Elements.Shopping_Cart_AddedModel, adi_model.ToUpper());

                //----- R3 > T4: Verify Adding Models that has Trailing Spaces (AL-10649/IQ-3724) -----//
                action.IAddModelToPurchase(driver, adi_model);

                if (Locale.Equals("en"))
                {
                    test.validateStringIsCorrect(driver, Elements.SC_ErrorMessage_Label, alreadyExistingModel_errMsg + adi_model.ToUpper());
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.SC_ErrorMessage_Label);
                }
            });
        }
    }
}