﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;

namespace AS_Test_Framework.ShoppingCart
{
    [TestFixture]
    public class ShoppingCart_ShoppingCartPage_VerifyAddingAModelForPurchaseFromAnEvalBoardPage : BaseSetUp
    {
        public ShoppingCart_ShoppingCartPage_VerifyAddingAModelForPurchaseFromAnEvalBoardPage() : base() { }

        //--- Test Data ---//
        string evalBoard = "EVAL-HMC313";

        [Test, Category("Shopping Cart"), Category("NonCore")]
        [TestCase("en", TestName = "Verify Adding an Export Controlled Model for Purchase from an Eval Board Page as Anonymous User in EN Locale")]
        //[TestCase("cn", TestName = "Verify Adding an Export Controlled Model for Purchase from an Eval Board Page as Anonymous User in CN Locale")]
        //[TestCase("jp", TestName = "Verify Adding an Export Controlled Model for Purchase from an Eval Board Page as Anonymous User in JP Locale")]
        //[TestCase("ru", TestName = "Verify Adding an Export Controlled Model for Purchase from an Eval Board Page as Anonymous User in RU Locale")]
        public void ShoppingCart_VerifyAddingAnExportControlledModelForPurchaseFromAnEvalBoardPageAsAnonymousUser(string Locale)
        {
            //----- R26 > T1: Verify that the model/part is correctly added to the cart (IQ-8123/AA-891) -----//

            //----- EVAL BOARD -----//

            action.Navigate(driver, Configuration.Env_Url + Locale + "/" + evalBoard);

            //--- Action: Select "UNITED STATES" in Select a country Dropdown ---//
            action.IClick(driver, By.CssSelector("li[class='dropdown dropup']>a"));
            action.IClick(driver, By.CssSelector("a[data-id='US']"));

            //--- Action: Click check inventory ---//
            action.IClick(driver, Elements.EvalBoard_CheckInventory_Btn);

            //--- Action: Get the Value of the Model in the 1st Row ---//
            string evalBoard_model = util.GetText(driver, By.CssSelector("div[class$='view-inventory'] * tr:nth-child(1) td[data-id='ModelName']"));

            //--- Action: Select an item (1st Row) and attempt to add it to the cart ---//
            action.IClick(driver, By.CssSelector("div[class$='view-inventory'] * tr:nth-child(1) input[class='check-add-to-cart']"));

            //--- Action: Click the Add to cart Button ---//
            action.IClick(driver, Elements.EvalBoard_AddToCart_Btn);

            //----- SHOPPING CART -----//

            string sc_page_url = Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx";
            if (Locale.Equals("jp") || Locale.Equals("ru"))
            {
                sc_page_url = sc_page_url + "?locale=" + Locale;
            }
            else if (Locale.Equals("cn") || Locale.Equals("zh"))
            {
                sc_page_url = sc_page_url + "?locale=zh";
            }

            if (!driver.Url.Contains(sc_page_url))
            {
                driver.Navigate().GoToUrl(sc_page_url);
            }

            //--- Expected Result: Model/part is correctly added to the cart ---//
            test.validateStringInstance(driver, evalBoard_model, util.GetText(driver, Elements.Shopping_Cart_AddedModel));
        }
    }
}