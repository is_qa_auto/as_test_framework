﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.ShoppingCart
{
    [TestFixture]
    public class ShoppingCart_ShoppingCartPage_VerifyCustomerServiceSection : BaseSetUp
    {
        public ShoppingCart_ShoppingCartPage_VerifyCustomerServiceSection() : base() { }

        //--- Login Credentials ---//
        string biz_username = "marvin.bebe@analog.com";
        string biz_password = "Test_1234";
        string nonBiz_username = "qa_automation_user2@mailinator.com";
        string nonBiz_password = "Test_1234";

        //--- URLs ---//
        string customerServiceSupport_form_url = "analog.com/Form_Pages/support/customerService.aspx";
        string customerServiceSupport_china_form_url = "analog.com/Form_Pages/support/customerService_china.aspx";
        string viewShippingRatesAndOptionsForBuyingOnline_page_url = "/support/customer-service-resources/customer-service/view-shipping-options-rates.html";
        string buyProducts_page_url = "/support/customer-service-resources/sales/buy-products.html";
        string samplesCountryRegionAvailabilityList_page_url = "/support/customer-service-resources/sales/samples_country_availability_list.html";
        string findSaleOfficeAndDistributor_page_url = "/support/customer-service-resources/sales/find-sale-office-distributor.html";
        string salesAndDistribution_cn_page_url = "/about-adi/landing-pages/002/sales-and-distributors.html";
        string salesAndDistribution_jp_page_url = "/about-adi/landing-pages/003/jp-sales-and-disti.html";
        string sampleProducts_page_url = "/support/customer-service-resources/sales/sample-products.html";

        [Test, Category("Shopping Cart"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that the Customer Service Section is Present and Working as Expected using a Business Email in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Customer Service Section is Present and Working as Expected using a Business Email in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Customer Service Section is Present and Working as Expected using a Business Email in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Customer Service Section is Present and Working as Expected using a Business Email in RU Locale")]
        public void ShoppingCart_VerifyCustomerServiceSectionUsingABusinessEmail(string Locale)
        {
            //--- Shopping Cart URL ---//
            string sc_page_url = Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx";
            if (Locale.Equals("jp") || Locale.Equals("ru"))
            {
                sc_page_url = sc_page_url + "?locale=" + Locale;
            }
            else if (Locale.Equals("cn") || Locale.Equals("zh"))
            {
                sc_page_url = sc_page_url + "?locale=zh";
            }

            action.Navigate(driver, sc_page_url);
            Thread.Sleep(500);

            action.ILoginViaLogInLink(driver, biz_username, biz_password);

            Assert.Multiple(() =>
            {
                //----- R2 > T12: Verify the Customer Service section -----//

                //--- Click on the Customer Service link ---//
                if (!Locale.Equals("jp"))
                {
                    action.IClick(driver, Elements.Shopping_Cart_CS_Link);
                    Thread.Sleep(1000);

                    SwitchToLastTab_IfPageOpenedInANewTab(sc_page_url);
                    if (Locale.Equals("cn") || Locale.Equals("zh"))
                    {
                        test.validateStringInstance(driver, driver.Url, customerServiceSupport_china_form_url);
                    }
                    else
                    {
                        test.validateStringInstance(driver, driver.Url, customerServiceSupport_form_url);
                    }
                    ReturnToDefaultUrl_IfPageOpenedInSameTab(sc_page_url);
                }

                //--- Verify Buying Online and Sampling Online Description ---//
                test.validateElementIsPresent(driver, Elements.Shopping_Cart_AbtBuyingOL_Txt);
                test.validateElementIsPresent(driver, Elements.Shopping_Cart_AbtSamplingOL_Txt);

                //--- Click selected countries link on Buying Online description ---//
                action.IClick(driver, Elements.Shopping_Cart_AbtBuyingOL_SelectedCountriesRgn_Link);
                SwitchToLastTab_IfPageOpenedInANewTab(sc_page_url);
                test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + viewShippingRatesAndOptionsForBuyingOnline_page_url);
                ReturnToDefaultUrl_IfPageOpenedInSameTab(sc_page_url);

                //--- Click on the click here link on Buying Online description ---//
                action.IClick(driver, Elements.Shopping_Cart_AbtBuyingOL_ClickHere_Link);
                SwitchToLastTab_IfPageOpenedInANewTab(sc_page_url);
                test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + buyProducts_page_url);
                ReturnToDefaultUrl_IfPageOpenedInSameTab(sc_page_url);

                //--- Click on selected countries link on Sampling Online description ---//
                action.IClick(driver, Elements.Shopping_Cart_AbtSamplingOL_SelectedCountriesRgn_Link);
                SwitchToLastTab_IfPageOpenedInANewTab(sc_page_url);
                test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + samplesCountryRegionAvailabilityList_page_url);
                ReturnToDefaultUrl_IfPageOpenedInSameTab(sc_page_url);

                //--- Click on authorized distributors link on Sampling Online description ---//
                action.IClick(driver, Elements.Shopping_Cart_AbtSamplingOL_AuthDistr_Link);
                SwitchToLastTab_IfPageOpenedInANewTab(sc_page_url);
                if (Locale.Equals("cn") || Locale.Equals("zh"))
                {
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + salesAndDistribution_cn_page_url);
                }
                else if (Locale.Equals("jp"))
                {
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + salesAndDistribution_jp_page_url);
                }
                else
                {
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + findSaleOfficeAndDistributor_page_url);
                }
                ReturnToDefaultUrl_IfPageOpenedInSameTab(sc_page_url);

                //--- Click on the click here link on Sampling Online description ---//
                action.IClick(driver, Elements.Shopping_Cart_AbtSamplingOL_ClickHere_Link);
                SwitchToLastTab_IfPageOpenedInANewTab(sc_page_url);
                test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + sampleProducts_page_url);
            });
        }

        [Test, Category("Shopping Cart"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that the Customer Service Section is Present and Working as Expected using a Non-business Email in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Customer Service Section is Present and Working as Expected using a Non-business Email in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Customer Service Section is Present and Working as Expected using a Non-business Email in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Customer Service Section is Present and Working as Expected using a Non-business Email in RU Locale")]
        public void ShoppingCart_VerifyCustomerServiceSectionUsingANonBusinessEmail(string Locale)
        {
            //--- Shopping Cart URL ---//
            string sc_page_url = Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx";
            if (Locale.Equals("jp") || Locale.Equals("ru"))
            {
                sc_page_url = sc_page_url + "?locale=" + Locale;
            }
            else if (Locale.Equals("cn") || Locale.Equals("zh"))
            {
                sc_page_url = sc_page_url + "?locale=zh";
            }

            action.Navigate(driver, sc_page_url);
            action.ILoginViaLogInLink(driver, nonBiz_username, nonBiz_password);

            Assert.Multiple(() =>
            {
                //----- R3 > T1: Verify the Customer Service section -----//

                //--- Click on the Customer Service link ---//
                if (!Locale.Equals("jp"))
                {
                    action.IClick(driver, Elements.Shopping_Cart_CS_Link);
                    Thread.Sleep(1000);

                    SwitchToLastTab_IfPageOpenedInANewTab(sc_page_url);
                    if (Locale.Equals("cn") || Locale.Equals("zh"))
                    {
                        test.validateStringInstance(driver, driver.Url, customerServiceSupport_china_form_url);
                    }
                    else
                    {
                        test.validateStringInstance(driver, driver.Url, customerServiceSupport_form_url);
                    }
                    ReturnToDefaultUrl_IfPageOpenedInSameTab(sc_page_url);
                }

                //--- Verify Buying Online and Sampling Online Description ---//
                test.validateElementIsPresent(driver, Elements.Shopping_Cart_AbtBuyingOL_Txt);
                test.validateElementIsPresent(driver, Elements.Shopping_Cart_AbtSamplingOL_Txt);

                //--- Click selected countries link on Buying Online description ---//
                action.IClick(driver, Elements.Shopping_Cart_AbtBuyingOL_SelectedCountriesRgn_Link);
                SwitchToLastTab_IfPageOpenedInANewTab(sc_page_url);
                test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + viewShippingRatesAndOptionsForBuyingOnline_page_url);
                ReturnToDefaultUrl_IfPageOpenedInSameTab(sc_page_url);

                //--- Click on the click here link on Buying Online description ---//
                action.IClick(driver, Elements.Shopping_Cart_AbtBuyingOL_ClickHere_Link);
                SwitchToLastTab_IfPageOpenedInANewTab(sc_page_url);
                test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + buyProducts_page_url);
                ReturnToDefaultUrl_IfPageOpenedInSameTab(sc_page_url);

                //--- Click on selected countries link on Sampling Online description ---//
                action.IClick(driver, Elements.Shopping_Cart_AbtSamplingOL_SelectedCountriesRgn_Link);
                SwitchToLastTab_IfPageOpenedInANewTab(sc_page_url);
                test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + samplesCountryRegionAvailabilityList_page_url);
                ReturnToDefaultUrl_IfPageOpenedInSameTab(sc_page_url);

                //--- Click on authorized distributors link on Sampling Online description ---//
                action.IClick(driver, Elements.Shopping_Cart_AbtSamplingOL_AuthDistr_Link);
                SwitchToLastTab_IfPageOpenedInANewTab(sc_page_url);
                if (Locale.Equals("cn") || Locale.Equals("zh"))
                {
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + salesAndDistribution_cn_page_url);
                }
                else if (Locale.Equals("jp"))
                {
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + salesAndDistribution_jp_page_url);
                }
                else
                {
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + findSaleOfficeAndDistributor_page_url);
                }
                ReturnToDefaultUrl_IfPageOpenedInSameTab(sc_page_url);

                //--- Click on the click here link on Sampling Online description ---//
                action.IClick(driver, Elements.Shopping_Cart_AbtSamplingOL_ClickHere_Link);
                SwitchToLastTab_IfPageOpenedInANewTab(sc_page_url);
                test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + sampleProducts_page_url);
            });
        }

        [Test, Category("Shopping Cart"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that the Customer Service Section is Present and Working as Expected using as Anonymous User in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Customer Service Section is Present and Working as Expected using as Anonymous User in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Customer Service Section is Present and Working as Expected using as Anonymous User in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Customer Service Section is Present and Working as Expected using as Anonymous User in RU Locale")]
        public void ShoppingCart_VerifyCustomerServiceSectionUsingAsAnonymousUser(string Locale)
        {
            //--- Shopping Cart URL ---//
            string sc_page_url = Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx";
            if (Locale.Equals("jp") || Locale.Equals("ru"))
            {
                sc_page_url = sc_page_url + "?locale=" + Locale;
            }
            else if (Locale.Equals("cn") || Locale.Equals("zh"))
            {
                sc_page_url = sc_page_url + "?locale=zh";
            }

            action.Navigate(driver, sc_page_url);

            Assert.Multiple(() =>
            {
                //----- R4 > T2_1 > S4: Verify the description section for About Buying Online the Hyperlink showing as selected countries/region if display. -----//
                test.validateElementIsPresent(driver, Elements.Shopping_Cart_AbtBuyingOL_SelectedCountriesRgn_Link);

                //----- R4 > T2_1 > S5: Verify the description section for About Sampling Online the Hyperlink showing as selected countries/region if display -----//
                test.validateElementIsPresent(driver, Elements.Shopping_Cart_AbtSamplingOL_SelectedCountriesRgn_Link);
            });
        }

        public void SwitchToLastTab_IfPageOpenedInANewTab(string url)
        {
            if (driver.Url.Contains(url))
            {
                driver.SwitchTo().Window(driver.WindowHandles.Last());
            }
        }

        public void ReturnToDefaultUrl_IfPageOpenedInSameTab(string url)
        {
            if (!driver.Url.Contains(url))
            {
                driver.Navigate().GoToUrl(url);
            }
        }
    }
}