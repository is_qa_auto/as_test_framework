﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.ShoppingCart
{
    [TestFixture]
    public class ShoppingCart_EnterPaymentAndPlaceOrderPage_VerifyButtonsAndLinks : BaseSetUp
    {
        public ShoppingCart_EnterPaymentAndPlaceOrderPage_VerifyButtonsAndLinks() : base() { }

        //--- Login Credentials ---//
        string biz_username = "marvin.bebe@analog.com";
        string biz_password = "Test_1234";

        //--- URLs ---//
        string viewShippingRatesAndOptionsForBuyingOnline_page_url = "/support/customer-service-resources/customer-service/view-shipping-options-rates.html";
        string customerServiceSupport_form_url = "analog.com/Form_Pages/support/customerService.aspx";
        string customerServiceSupport_china_form_url = "analog.com/Form_Pages/support/customerService_china.aspx";
        string termsAndConditions_page_url = "/terms-and-conditions.html";

        [Test, Category("Shopping Cart"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that the Buttons and Links are Present and Working as Expected after Adding a Model for Purchase in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Buttons and Links are Present and Working as Expected after Adding a Model for Purchase in CN Locale")]
        //[TestCase("ru", TestName = "Verify that the Buttons and Links are Present and Working as Expected after Adding a Model for Purchase in RU Locale")]
        public void ShoppingCart_VerifyButtonsAndLinksAfterAddingAModelForPurchase(string Locale)
        {
            //--- Shopping Cart URL ---//
            string sc_page_url = Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx";
            if (Locale.Equals("jp") || Locale.Equals("ru"))
            {
                sc_page_url = sc_page_url + "?locale=" + Locale;
            }
            else if (Locale.Equals("cn") || Locale.Equals("zh"))
            {
                sc_page_url = sc_page_url + "?locale=zh";
            }

            action.Navigate(driver, sc_page_url);
            action.ILoginViaLogInLink(driver, biz_username, biz_password);

            util.RemoveAllModelsInYourCartTable(driver);

            //--- Action: Get Test Data ---//
            string adi_model = util.GetAdiModel();

            action.IAddModelToPurchase(driver, adi_model);

            action.IClick(driver, Elements.Shopping_Cart_Checkout_Button);
            Thread.Sleep(4000);

            action.GoToAboutYourProjectPage(driver);

            action.IPopulateFieldsInAboutYourProjectPage(driver);

            if (Locale.Equals("en"))
            {
                //----- R9 > T2: Verify View Shipping Options and Estimated Shipping Costs link -----//

                //--- Expected Result: The View Shipping Options and Estimated Shipping Costs Links must be Present ---//
                test.validateElementIsPresent(driver, Elements.Shopping_Cart_PlaceOrder_ViewShippingOptionsAndEstimatedShippingCosts_Link);

                if (util.CheckElement(driver, Elements.Shopping_Cart_PlaceOrder_ViewShippingOptionsAndEstimatedShippingCosts_Link, 2))
                {
                    //--- Action: Click on the View Shipping Options and Estimated Shipping Costs link ---//
                    action.IOpenLinkInNewTab(driver, Elements.Shopping_Cart_PlaceOrder_ViewShippingOptionsAndEstimatedShippingCosts_Link);
                    Thread.Sleep(4000);

                    //--- Expected Result: The Customer Service Support form should be displayed in a new tab/window. ----//
                    test.validateStringInstance(driver, driver.Url, viewShippingRatesAndOptionsForBuyingOnline_page_url);

                    driver.Close();
                    driver.SwitchTo().Window(driver.WindowHandles.First());
                }

                //----- R9 > T5: Verify Your Shopping Cart section within the Enter Payment and Place Order page -----//
                Assert.Multiple(() =>
                {
                    //--- Expected Result: The products previously added on the Shopping cart ---//
                    test.validateElementIsPresent(driver, Elements.Shopping_Cart_PlaceOrder_ProductNo_Link);

                    if (util.CheckElement(driver, Elements.Shopping_Cart_PlaceOrder_ProductNo_Link, 2))
                    {
                        //--- Action: Click on the product number hyperlink ---//
                        action.IOpenLinkInNewTab(driver, Elements.Shopping_Cart_PlaceOrder_ProductNo_Link);
                        Thread.Sleep(6000);

                        //--- Expected Result: The product page should be displayed ---//
                        test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + "/products/");

                        driver.Close();
                        driver.SwitchTo().Window(driver.WindowHandles.First());
                    }
                });

                //----- ECommerce TestPlan > Test Case Title: Verify the link of "Terms & Conditions' (IQ-9815/AL-16990) -----//
                action.IMouseOverTo(driver, Elements.Footer);

                //--- Expected Result: The Terms & Conditions Link must be Present ---//
                test.validateElementIsPresent(driver, Elements.Shopping_Cart_PlaceOrder_TermsAndConditions_Link);

                if (util.CheckElement(driver, Elements.Shopping_Cart_PlaceOrder_TermsAndConditions_Link, 2))
                {
                    //--- Action: Click "Terms & Conditions' link ---//
                    action.IOpenLinkInNewTab(driver, Elements.Shopping_Cart_PlaceOrder_TermsAndConditions_Link);

                    //--- Expected Result: The page redirected to " https://www.analog.com/en/support/customer-service-resources/sales/terms-and-conditions.html" ---//
                    test.validateStringInstance(driver, driver.Url, termsAndConditions_page_url);

                    driver.Close();
                    driver.SwitchTo().Window(driver.WindowHandles.First());
                }
            }

            if (Locale.Equals("en") || Locale.Equals("cn") || Locale.Equals("zh"))
            {
                //----- R9 > T7: Verify the Customer Service link on the Enter Payment and Place Order page -----//
                action.IMouseOverTo(driver, Elements.Footer);

                Assert.Multiple(() =>
                {
                    //--- Expected Result: The Customer Service >> Link must be Present ---//
                    test.validateElementIsPresent(driver, Elements.Shopping_Cart_CustomerService_Link);

                    if (util.CheckElement(driver, Elements.Shopping_Cart_CustomerService_Link, 2))
                    {
                        //--- Action: Click on the Customer Service link ---//
                        action.IOpenLinkInNewTab(driver, Elements.Shopping_Cart_CustomerService_Link);
                        Thread.Sleep(1000);

                        //--- Expected Result: The Customer Service Support form should be displayed in a new tab/window. ----//
                        if (Locale.Equals("cn") || Locale.Equals("zh"))
                        {
                            test.validateStringInstance(driver, driver.Url, customerServiceSupport_china_form_url);
                        }
                        else
                        {
                            test.validateStringInstance(driver, driver.Url, customerServiceSupport_form_url);
                        }

                        driver.Close();
                        driver.SwitchTo().Window(driver.WindowHandles.First());
                    }
                });
            }

            if (Locale.Equals("en"))
            {
                //----- R9 > T5: Verify Your Shopping Cart section within the Enter Payment and Place Order page -----//
                Assert.Multiple(() =>
                {
                    //--- Expected Result: The Edit Cart button should be displayed ---//
                    test.validateElementIsPresent(driver, Elements.Shopping_Cart_PlaceOrder_EditCart_Btn);

                    if (util.CheckElement(driver, Elements.Shopping_Cart_PlaceOrder_EditCart_Btn, 2))
                    {
                        //--- Action: Click on the Edit Cart button ---//
                        action.IClick(driver, Elements.Shopping_Cart_PlaceOrder_EditCart_Btn);

                        //--- Expected Result: The Shopping cart page should be displayed. ---//
                        test.validateStringInstance(driver, driver.Url, sc_page_url);
                    }
                });
            }
        }
    }
}