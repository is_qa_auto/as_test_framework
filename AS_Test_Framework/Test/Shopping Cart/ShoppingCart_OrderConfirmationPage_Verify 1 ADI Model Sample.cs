﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.ShoppingCart
{
    [TestFixture]
    public class ShoppingCart_OrderConfirmation_VerifyOneAdiModelSample : BaseSetUp
    {
        public ShoppingCart_OrderConfirmation_VerifyOneAdiModelSample() : base() { }

        //--- Login Credentials ---//
        string username = "myanalog_t3st_999@mailinator.com";
        string password = "Test_1234";

        //--- URLs ---//
        string sampleRequestConfirmation_url = "OrderConfirmation.aspx";
        string myanalogOrders_url = "/app/orders";
        string customerServiceSupport_form_url = "analog.com/Form_Pages/support/customerService.aspx";
        string customerServiceSupport_china_form_url = "analog.com/Form_Pages/support/customerService_china.aspx";

        //--- Labels ---//
        string sample_orderConfirmation_emailSubj = "Analog Devices: We have received your sample request";
        string sample_orderConfirmation_msg_prefix = "Your sample request has been submitted. Your order reference number is:";
        string sample_orderConfirmation_msg_suffix = "Your sample request has been received, you will receive an order acknowledgement email.";

        [Test, Category("Shopping Cart"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that the Order Confirmation Page is Present and Working as Expected after Placing 1 ADI Model for Sample in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Order Confirmation Page is Present and Working as Expected after Placing 1 Sample ADI Model for Sample in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Order Confirmation Page is Present and Working as Expected after Placing 1 Sample ADI Model for Sample in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Order Confirmation Page is Present and Working as Expected after Placing 1 Sample ADI Model for Sample in RU Locale")]
        public void ShoppingCart_VerifyOneAdiModelSample(string Locale)
        {
            if (Locale.Equals("en"))
            {
                //--- Shopping Cart URL ---//
                string sc_page_url = Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx";
                if (Locale.Equals("jp") || Locale.Equals("ru"))
                {
                    sc_page_url = sc_page_url + "?locale=" + Locale;
                }
                else if (Locale.Equals("cn") || Locale.Equals("zh"))
                {
                    sc_page_url = sc_page_url + "?locale=zh";
                }

                action.Navigate(driver, sc_page_url);

                if (Util.Configuration.Environment.Equals("production"))
                {
                    username = "marvin.bebe@analog.com";
                }
                action.ILoginViaLogInLink(driver, username, password);

                util.RemoveAllModelsInYourCartTable(driver);

                //--- Action: Get Test Data ---//
                string adi_model = util.GetAdiModel();

                action.IAddModelToSample(driver, adi_model);

                action.IClick(driver, Elements.Shopping_Cart_Checkout_Button);
                Thread.Sleep(3000);

                action.IPopulateFieldsInShippingAddressPage(driver);

                action.IPopulateFieldsInAboutYourProjectPage(driver);

                //--- Action: Click on the Place Order button ---//
                action.IPopulateFieldsInPlaceSampleRequestPage(driver);

                //----- R10 > T2: Verify the Place Order button on the Place Sample Request page -----//

                //--- Expected Result: The Sample Request Confirmation page should be displayed. ---//
                if (Locale.Equals("cn") || Locale.Equals("zh"))
                {
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + sampleRequestConfirmation_url + "?locale=zh");
                }
                else
                {
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + sampleRequestConfirmation_url + "?locale=" + Locale);
                }

                if (driver.Url.Contains("analog.com/" + sampleRequestConfirmation_url + "?locale="))
                {
                    if (username.Contains("@mailinator.com"))
                    {
                        //----- R11 > T1_1: Place a Sample Order -----//

                        //--- Action: Validate the email from buyonline.customerservice@analog.com is sent to the email address of the user ---//
                        action.IOpenNewTab(driver);
                        action.ICheckMailinatorEmail(driver, username);
                        Thread.Sleep(10000);

                        //--- Expected Result: The email from buyonline.customerservice@analog.com with title 'Analog Devices: We have received your sample request' should be displayed properly ---//
                        test.validateStringInstance(driver, util.GetText(driver, Elements.Mailinator_LatestEmail_Subj), sample_orderConfirmation_emailSubj);

                        driver.Close();
                        driver.SwitchTo().Window(driver.WindowHandles.First());
                    }

                    //----- R21 > T1: Sample ADI Model only -----//

                    //--- Expected Result: On Sample Request Confirmation page, the message displayed should be correct ---//
                    if (Locale.Equals("en"))
                    {
                        int line_no = 1;
                        var orderConfirmation_msg_lines = util.GetText(driver, Elements.Shopping_Cart_OrderConfirmation_SamplesOnly_Msg).Split("\r\n".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                        foreach (var line in orderConfirmation_msg_lines)
                        {
                            if (line_no == 1)
                            {
                                test.validateStringInstance(driver, line, sample_orderConfirmation_msg_prefix /*+ util.GetText(driver, Elements.Shopping_Cart_OrderConfirmation_Sample_OrderRefNo)*/);
                            }
                            else if (line_no == 2)
                            {
                                test.validateStringInstance(driver, line, sample_orderConfirmation_msg_suffix);
                            }

                            line_no++;
                        }
                    }
                    else
                    {
                        test.validateElementIsPresent(driver, Elements.Shopping_Cart_OrderConfirmation_SamplesOnly_Msg);
                    }

                    //----- R11 > T4_1 > S2: Verify the Sample Request Confirmation page contents -----//

                    //--- Expected Result: The following elements should be displayed ---//

                    //--- Order Reference Number ---//
                    test.validateElementIsPresent(driver, Elements.Shopping_Cart_OrderConfirmation_Sample_OrderRefNo);

                    if (util.CheckElement(driver, Elements.Shopping_Cart_OrderConfirmation_Sample_OrderRefNo, 1))
                    {
                        //--- Action: Click the order Reference Number ---//
                        action.IOpenLinkInNewTab(driver, Elements.Shopping_Cart_OrderConfirmation_Sample_OrderRefNo);
                        Thread.Sleep(3000);

                        //--- Expected Result: The page should navigate to My Order page ---//
                        if (Locale.Equals("cn") || Locale.Equals("zh"))
                        {
                            test.validateStringInstance(driver, driver.Url, "analog.com/zh" + myanalogOrders_url);
                        }
                        else
                        {
                            test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + myanalogOrders_url);
                        }

                        driver.Close();
                        driver.SwitchTo().Window(driver.WindowHandles.First());
                    }

                    //--- View Order Status link ---//
                    test.validateElementIsPresent(driver, Elements.Shopping_Cart_OrderConfirmation_ViewOrderStatus_Link);
                    if (util.CheckElement(driver, Elements.Shopping_Cart_OrderConfirmation_ViewOrderStatus_Link, 2))
                    {
                        //--- Action: Click on Order Reference number link, and View order status in Order Confirmation page ---//
                        action.IOpenLinkInNewTab(driver, Elements.Shopping_Cart_OrderConfirmation_ViewOrderStatus_Link);
                        Thread.Sleep(3000);

                        if (driver.Url.Contains("b2clogin"))
                        {
                            action.ILogin(driver, username, password);
                            Thread.Sleep(1000);
                        }

                        //--- Expected Result: The My Orders page should be displayed. ----//
                        if (Locale.Equals("cn") || Locale.Equals("zh"))
                        {
                            test.validateStringInstance(driver, driver.Url, "analog.com/zh" + myanalogOrders_url);
                        }
                        else
                        {
                            test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + myanalogOrders_url);
                        }

                        driver.Close();
                        driver.SwitchTo().Window(driver.WindowHandles.First());
                    }

                    //--- Model # and details of Order ---//
                    test.validateElementIsPresent(driver, Elements.Shopping_Cart_PlaceOrder_YourShoppingCart_Tbl);

                    //--- Customer Service link ---//
                    test.validateElementIsPresent(driver, Elements.Shopping_Cart_CustomerService_Link);
                    if (util.CheckElement(driver, Elements.Shopping_Cart_CustomerService_Link, 2))
                    {
                        //--- Action: Click on the Customer Service link ---//
                        action.IOpenLinkInNewTab(driver, Elements.Shopping_Cart_CustomerService_Link);
                        Thread.Sleep(1000);

                        //--- Expected Result: The Customer Service Support form should be displayed in a new tab/window. ----//
                        if (Locale.Equals("cn") || Locale.Equals("zh"))
                        {
                            test.validateStringInstance(driver, driver.Url, customerServiceSupport_china_form_url);
                        }
                        else
                        {
                            test.validateStringInstance(driver, driver.Url, customerServiceSupport_form_url);
                        }

                        driver.Close();
                        driver.SwitchTo().Window(driver.WindowHandles.First());
                    }

                    //--- Cancel Orders in myAnalog ---//
                    action.ICancelOrdersInMyAnalog(driver, username, password);
                }
            }
        }
    }
}