﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.ShoppingCart
{
    [TestFixture]
    public class ShoppingCart_ShoppingCartPage_VerifyUpdateButtonAfterAddingAModelForSample : BaseSetUp
    {
        public ShoppingCart_ShoppingCartPage_VerifyUpdateButtonAfterAddingAModelForSample() : base() { }

        //--- Login Credentials ---//
        string biz_username = "marvin.bebe@analog.com";
        string biz_password = "Test_1234";

        //--- Labels ---//
        string biz_invalidQuantity_errMsg = "You are currently allowed to sample up to a quantity of 10 pieces per model. Please adjust quantity or buy additional.";
        string anon_invalidQuantity_errMsg = "Log in for personalized sample limits.";

        [Test, Category("Shopping Cart"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that the Update Button is Present and Working as Expected after Adding a Model for Sample using a Business Email in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Update Button is Present and Working as Expected after Adding a Model for Sample using a Business Email in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Update Button is Present and Working as Expected after Adding a Model for Sample using a Business Email in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Update Button is Present and Working as Expected after Adding a Model for Sample using a Business Email in RU Locale")]
        public void ShoppingCart_VerifyUpdateButtonAfterAddingAModelForSampleABusinessEmail(string Locale)
        {
            //--- Shopping Cart URL ---//
            string sc_page_url = Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx";
            if (Locale.Equals("jp") || Locale.Equals("ru"))
            {
                sc_page_url = sc_page_url + "?locale=" + Locale;
            }
            else if (Locale.Equals("cn") || Locale.Equals("zh"))
            {
                sc_page_url = sc_page_url + "?locale=zh";
            }

            action.Navigate(driver, sc_page_url);
            action.ILoginViaLogInLink(driver, biz_username, biz_password);

            util.RemoveAllModelsInYourCartTable(driver);

            //--- Action: Get Test Data ---//
            string adi_model = util.GetAdiModel();

            action.IAddModelToSample(driver, adi_model);

            //----- R2 > T7_3: Verify the Update button with a quantity that is more than the allowed sample quantity -----//

            //--- Expected Result: The part should be added on the Cart with only 1 quantity ---//
            test.validateString(driver, util.ReturnAttribute(driver, Elements.Shopping_Cart_Qty_Tbx, "value"), "1");

            //--- Input ---//
            string update_input = "12";

            //--- Action: Update the quantity ---//
            action.IDeleteValueOnFields(driver, Elements.Shopping_Cart_Qty_Tbx);
            action.IType(driver, Elements.Shopping_Cart_Qty_Tbx, update_input);

            //--- Action: Click on the Update button ---//
            action.IClick(driver, Elements.Shopping_Cart_Update_Btn);
            Thread.Sleep(3000);

            //--- Expected Result: The error message should be displayed ---//
            if (Locale.Equals("en"))
            {
                test.validateStringIsCorrect(driver, Elements.SC_QtyCol_ErrorMessage_Line1_Label, biz_invalidQuantity_errMsg);
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.SC_QtyCol_ErrorMessage_Line1_Label);
            }

            //--- Action: Get the Value of the Added Model ---//
            string addedModel_value = util.GetText(driver, Elements.Shopping_Cart_AddedModel);

            //--- Action: Click buy additional link on returned message  ---//
            action.IClick(driver, Elements.Shopping_Cart_BuyAddtl_Link);
            Thread.Sleep(1000);

            //--- Expected Result: Same Product should be added in the cart with quantity is equal to 1 ---//
            test.validateStringInstance(driver, util.GetText(driver, By.CssSelector("table[class$='cart-table'] * tr:nth-of-type(2) * a[id$='ModelNbr']")), addedModel_value);
            test.validateStringInstance(driver, util.ReturnAttribute(driver, By.CssSelector("table[class$='cart-table'] * tr:nth-of-type(2) * input[id$='Quantity']"), "value"), "1");

            //--- Expected Result: Sample quantity should remain as 12. ---//
            test.validateStringInstance(driver, util.ReturnAttribute(driver, By.CssSelector("table[class$='cart-table'] * tr:nth-of-type(3) * input[id$='Quantity']"), "value"), update_input);

            //--- Action: Click on the Update button ---//
            action.IClick(driver, Elements.Shopping_Cart_Update_Btn);
            Thread.Sleep(3000);

            //--- Expected Result: The error message should be displayed ---//
            if (Locale.Equals("en"))
            {
                test.validateStringIsCorrect(driver, Elements.SC_QtyCol_ErrorMessage_Line1_Label, biz_invalidQuantity_errMsg);
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.SC_QtyCol_ErrorMessage_Line1_Label);
            }

            //--- Input ---//
            update_input = "10";

            //--- Action: Update the sample quantity ---//
            action.IDeleteValueOnFields(driver, By.CssSelector("table[class$='cart-table'] * tr:nth-of-type(3) * input[name$= 'Quantity']"));
            action.IType(driver, By.CssSelector("table[class$='cart-table'] * tr:nth-of-type(3) * input[name$= 'Quantity']"), update_input);

            //--- Action: Click on the Update button ---//
            action.IClick(driver, Elements.Shopping_Cart_Update_Btn);
            Thread.Sleep(6000);

            //--- Expected Result: The error message should be removed. ---//
            test.validateElementIsNotPresent(driver, Elements.SC_QtyCol_ErrorMessage_Line1_Label);

            //--- Expected Result: The order quantity below should be displayed on the cart ---//
            test.validateStringInstance(driver, util.ReturnAttribute(driver, By.CssSelector("table[class$='cart-table'] * tr:nth-of-type(2) * input[id$='Quantity']"), "value"), "1");
            test.validateStringInstance(driver, util.ReturnAttribute(driver, By.CssSelector("table[class$='cart-table'] * tr:nth-of-type(3) * input[id$='Quantity']"), "value"), update_input);
        }

        [Test, Category("Shopping Cart"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that the Update Button is Present and Working as Expected after Adding a Model for Sample using as Anonymous User in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Update Button is Present and Working as Expected after Adding a Model for Sample using as Anonymous User in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Update Button is Present and Working as Expected after Adding a Model for Sample using as Anonymous User in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Update Button is Present and Working as Expected after Adding a Model for Sample using as Anonymous User in RU Locale")]
        public void ShoppingCart_VerifyUpdateButtonAfterAddingAModelForSampleAsAnonymousUser(string Locale)
        {
            //--- Shopping Cart URL ---//
            string sc_page_url = Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx";
            if (Locale.Equals("jp") || Locale.Equals("ru"))
            {
                sc_page_url = sc_page_url + "?locale=" + Locale;
            }
            else if (Locale.Equals("cn") || Locale.Equals("zh"))
            {
                sc_page_url = sc_page_url + "?locale=zh";
            }

            action.Navigate(driver, sc_page_url);

            //--- Select any country on the Select Shipping Country dropdown (e.g. UNITED STATES) ---//
            action.IClick(driver, Elements.Shopping_Cart_Country_Dropdown);
            action.IClick(driver, By.CssSelector("li[value='US']>a"));

            util.RemoveAllModelsInYourCartTable(driver);

            //--- Action: Get Test Data ---//
            string adi_model = util.GetAdiModel();

            action.IAddModelToSample(driver, adi_model);

            //----- R4 > T6_3: Verify the Update button with a quantity that is more than the allowed sample quantity -----//

            //--- Input ---//
            string update_input = "12";

            //--- Action: Update the quantity ---//
            action.IDeleteValueOnFields(driver, Elements.Shopping_Cart_Qty_Tbx);
            action.IType(driver, Elements.Shopping_Cart_Qty_Tbx, update_input);

            //--- Action: Click on the Update button ---//
            action.IClick(driver, Elements.Shopping_Cart_Update_Btn);
            Thread.Sleep(3000);

            //--- Expected Result: The error message should be displayed ---//
            if (Locale.Equals("en"))
            {
                test.validateStringIsCorrect(driver, Elements.SC_QtyCol_ErrorMessage_Line1_Label, anon_invalidQuantity_errMsg);
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.SC_QtyCol_ErrorMessage_Line1_Label);
            }

            //--- Action: Click on the Log in link on returned message ---//
            action.IClick(driver, Elements.Shopping_Cart_LogIn_Link);

            //--- Expected Result: The login page should be displayed. ---//
            if (driver.Url.Contains(sc_page_url))
            {
                driver.SwitchTo().Window(driver.WindowHandles.Last());
            }
            test.validateStringInstance(driver, driver.Url, "b2clogin.com");
        }
    }
}