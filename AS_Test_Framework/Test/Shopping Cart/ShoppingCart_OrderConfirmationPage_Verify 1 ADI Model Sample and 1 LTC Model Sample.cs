﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System;
using System.Threading;

namespace AS_Test_Framework.ShoppingCart
{
    [TestFixture]
    public class ShoppingCart_OrderConfirmation_VerifyOneAdiModelSample_AndOneLtcModelSample : BaseSetUp
    {
        public ShoppingCart_OrderConfirmation_VerifyOneAdiModelSample_AndOneLtcModelSample() : base() { }

        //--- Login Credentials ---//
        string username = "myanalog_t3st_0rd3rs_555@mailinator.com";
        string password = "Test_1234";

        //--- URLs ---//
        string orderConfirmation_url = "OrderConfirmation.aspx";

        //--- Labels ---//
        string sample_orderConfirmation_msg_prefix = "Your sample request has been submitted. Your order reference number is:";
        string sample_orderConfirmation_msg_suffix = "Your sample request has been received, you will receive an order acknowledgement email.";

        [Test, Category("Shopping Cart"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that the Order Confirmation Page is Present and Working as Expected after Placing 1 ADI Model for Sample and 1 LTC Model for Purchase in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Order Confirmation Page is Present and Working as Expected after Placing 1 ADI Model for Sample and 1 LTC Model for Purchase in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Order Confirmation Page is Present and Working as Expected after Placing 1 ADI Model for Sample and 1 LTC Model for Purchase in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Order Confirmation Page is Present and Working as Expected after Placing 1 ADI Model for Sample and 1 LTC Model for Purchase in RU Locale")]
        public void ShoppingCart_VerifyOneAdiModelSample_AndOneLtcModelSample(string Locale)
        {
            if (Locale.Equals("en"))
            {
                //--- Shopping Cart URL ---//
                string sc_page_url = Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx";
                if (Locale.Equals("jp") || Locale.Equals("ru"))
                {
                    sc_page_url = sc_page_url + "?locale=" + Locale;
                }
                else if (Locale.Equals("cn") || Locale.Equals("zh"))
                {
                    sc_page_url = sc_page_url + "?locale=zh";
                }

                action.Navigate(driver, sc_page_url);

                if (Util.Configuration.Environment.Equals("production"))
                {
                    username = "marvin.bebe@analog.com";
                }
                action.ILoginViaLogInLink(driver, username, password);

                util.RemoveAllModelsInYourCartTable(driver);

                //----- R20 > T6 > S1: Process LTC product and AD product to sample -----//

                //--- Action: Get Test Data ---//
                string adi_model = util.GetAdiModel();
                string ltc_model = util.GetLtcModel();

                action.IAddModelToSample(driver, adi_model);
                action.IAddModelToSample(driver, ltc_model);

                action.IClick(driver, Elements.Shopping_Cart_Checkout_Button);
                Thread.Sleep(3000);

                action.IPopulateFieldsInShippingAddressPage(driver);

                action.IPopulateFieldsInAboutYourProjectPage(driver);

                action.IPopulateFieldsInPlaceSampleRequestPage(driver);

                if (driver.Url.Contains("analog.com/" + orderConfirmation_url + "?locale="))
                {
                    //----- R21 > T5: Sample ADI Model and Sample LTC Model -----//

                    //--- Expected Result: On Sample Request Confirmation page, the message should be correct ---//
                    if (Locale.Equals("en"))
                    {
                        int line_no = 1;
                        var orderConfirmation_msg_lines = util.GetText(driver, Elements.Shopping_Cart_OrderConfirmation_SamplesOnly_Msg).Split("\r\n".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                        foreach (var line in orderConfirmation_msg_lines)
                        {
                            if (line_no == 1)
                            {
                                //--- Order Confirmation Message - Line #1 ---//
                                test.validateStringInstance(driver, line, sample_orderConfirmation_msg_prefix /*+ util.GetText(driver, Elements.Shopping_Cart_OrderConfirmation_Sample_OrderRefNo)*/);
                            }
                            else if (line_no == 2)
                            {
                                //--- Order Confirmation Message - Line #2 ---//
                                test.validateStringInstance(driver, line, sample_orderConfirmation_msg_suffix);
                            }

                            line_no++;
                        }
                    }
                    else
                    {
                        test.validateElementIsPresent(driver, Elements.Shopping_Cart_OrderConfirmation_SamplesOnly_Msg);
                    }

                    //--- Expected Result: In Order Confirmation page there should only have 1 order number for both LTC and AD product sample. ---//
                    test.validateCountIsEqual(driver, 1, util.GetCount(driver, Elements.Shopping_Cart_OrderConfirmation_Sample_OrderRefNo));

                    //--- Cancel Orders in myAnalog ---//
                    action.ICancelOrdersInMyAnalog(driver, username, password);
                }
                else
                {
                    if (Locale.Equals("cn") || Locale.Equals("zh"))
                    {
                        test.validateStringInstance(driver, driver.Url, "analog.com/" + orderConfirmation_url + "?locale=zh");
                    }
                    else
                    {
                        test.validateStringInstance(driver, driver.Url, "analog.com/" + orderConfirmation_url + "?locale=" + Locale);
                    }
                }
            }
        }
    }
}