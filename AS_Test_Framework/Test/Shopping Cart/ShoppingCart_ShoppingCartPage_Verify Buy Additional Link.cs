﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Threading;

namespace AS_Test_Framework.ShoppingCart
{
    [TestFixture]
    public class ShoppingCart_ShoppingCartPage_VerifyBuyAdditionalLink : BaseSetUp
    {
        public ShoppingCart_ShoppingCartPage_VerifyBuyAdditionalLink() : base() { }

        //--- Login Credentials ---//
        string biz_username = "marvin.bebe@analog.com";
        string biz_password = "Test_1234";

        //--- Labels ---//
        string alreadyExistingModel_errMsg = "Your shopping cart already contains model ";

        [Test, Category("Shopping Cart"), Category("NonCore")]
        [TestCase("en", "Sample", TestName = "Verify that the Buy Additional Link is Present and Working as Expected when Model to be Added is Not Yet Present using a Business Email in EN Locale")]
        //[TestCase("cn", "样片", TestName = "Verify that the Buy Additional Link is Present and Working as Expected when Model to be Added is Not Yet Present using a Business Email in CN Locale")]
        //[TestCase("jp", "サンプル", TestName = "Verify that the Buy Additional Link is Present and Working as Expected when Model to be Added is Not Yet Present using a Business Email in JP Locale")]
        //[TestCase("ru", "Sample", TestName = "Verify that the Buy Additional Link is Present and Working as Expected when Model to be Added is Not Yet Present using a Business Email in RU Locale")]
        public void ShoppingCart_VerifyBuyAdditionalLinkWhenModelToBeAddedIsNotYetPresentUsingABusinessEmail(string Locale, string Sample_Label)
        {
            //--- Shopping Cart URL ---//
            string sc_page_url = Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx";
            if (Locale.Equals("jp") || Locale.Equals("ru"))
            {
                sc_page_url = sc_page_url + "?locale=" + Locale;
            }
            else if (Locale.Equals("cn") || Locale.Equals("zh"))
            {
                sc_page_url = sc_page_url + "?locale=zh";
            }

            action.Navigate(driver, sc_page_url);
            action.ILoginViaLogInLink(driver, biz_username, biz_password);

            util.RemoveAllModelsInYourCartTable(driver);

            //--- Action: Get Test Data ---//
            string adi_model = util.GetAdiModel();

            action.IAddModelToSample(driver, adi_model);

            string addedModel_value = null;

            //----- R2 > T8_1: Verify Buy Additional link where product to be added is not yet present on the cart -----//

            //--- Expected Result: Buy Additional link appears under the quantity textbox. ---//
            test.validateElementIsPresent(driver, Elements.Shopping_Cart_BuyAddtl_Link);

            if (util.CheckElement(driver, Elements.Shopping_Cart_BuyAddtl_Link, 2))
            {
                //--- Action: Get the Value of the Added Model ---//
                addedModel_value = util.GetText(driver, Elements.Shopping_Cart_AddedModel);

                //--- Action: Click on Buy Additional link. ---//
                action.IClick(driver, Elements.Shopping_Cart_BuyAddtl_Link);
                Thread.Sleep(1000);

                //--- Expected Result: Same Product should be added is available for purchase ---//
                test.validateStringChanged(driver, util.GetText(driver, By.CssSelector("table[class$='cart-table'] * tr:nth-of-type(2) * span[id$='PackingOptionAndQuantity']")), Sample_Label);

                //--- Expected Result: Same Product should be added in the cart with quantity is equal to 1 ---//
                test.validateStringInstance(driver, util.GetText(driver, By.CssSelector("table[class$='cart-table'] * tr:nth-of-type(2) * a[id$='ModelNbr']")), addedModel_value);
                test.validateStringInstance(driver, util.ReturnAttribute(driver, By.CssSelector("table[class$='cart-table'] * tr:nth-of-type(2) * input[id$='Quantity']"), "value"), "1");
            }
        }

        [Test, Category("Shopping Cart"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that the Buy Additional Link is Present and Working as Expected when Model to be Added is Already Present using a Business Email in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Buy Additional Link is Present and Working as Expected when Model to be Added is Already Present using a Business Email in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Buy Additional Link is Present and Working as Expected when Model to be Added is Already Present using a Business Email in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Buy Additional Link is Present and Working as Expected when Model to be Added is Already Present using a Business Email in RU Locale")]
        public void ShoppingCart_VerifyBuyAdditionalLinkWhenModelToBeAddedIsAlreadyPresentUsingABusinessEmail(string Locale)
        {
            //--- Shopping Cart URL ---//
            string sc_page_url = Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx";
            if (Locale.Equals("jp") || Locale.Equals("ru"))
            {
                sc_page_url = sc_page_url + "?locale=" + Locale;
            }
            else if (Locale.Equals("cn") || Locale.Equals("zh"))
            {
                sc_page_url = sc_page_url + "?locale=zh";
            }

            action.Navigate(driver, sc_page_url);
            action.ILoginViaLogInLink(driver, biz_username, biz_password);

            util.RemoveAllModelsInYourCartTable(driver);

            string adi_model = util.GetAdiModel();
            action.IAddModelToPurchase(driver, adi_model);
            Thread.Sleep(5000);
            action.IAddModelToSample(driver, adi_model);

            //----- R2 > T8_2: Verify Buy Additional link where product to be added as sample is already present on the cart as product for purchase -----//

            //--- Expected Result: Buy Additional link should be displayed under the quantity textbox. ---//
            test.validateElementIsPresent(driver, Elements.Shopping_Cart_BuyAddtl_Link);

            if (util.CheckElement(driver, Elements.Shopping_Cart_BuyAddtl_Link, 2))
            {
                //--- Action: Click on Buy Additional link. ---//
                action.IClick(driver, Elements.Shopping_Cart_BuyAddtl_Link);
                Thread.Sleep(1000);

                //--- Expected Result: The error message below should be displayed ---//
                if (Locale.Equals("en"))
                {
                    test.validateStringIsCorrect(driver, Elements.SC_ErrorMessage_Label, alreadyExistingModel_errMsg + adi_model.ToUpper());
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.SC_ErrorMessage_Label);
                }
            }
        }
    }
}