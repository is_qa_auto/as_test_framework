﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Threading;

namespace AS_Test_Framework.ShoppingCart
{
    [TestFixture]
    public class ShoppingCart_EnterPaymentAndPlaceOrderPage_VerifyPromoPricing : BaseSetUp
    {
        public ShoppingCart_EnterPaymentAndPlaceOrderPage_VerifyPromoPricing() : base() { }

        //--- Login Credentials ---//
        string biz_username = "marvin.bebe@analog.com";
        string biz_password = "Test_1234";

        //--- Labels ---//
        string invalidOfferCode_errMsg = "The offer code entered is not valid for your purchase. (Error : 022)";

        [Test, Category("Shopping Cart"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that the Promo Pricing is Present and Working as Expected after Adding a Model for Purchase in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Promo Pricing is Present and Working as Expected after Adding a Model for Purchase in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Promo Pricing is Present and Working as Expected after Adding a Model for Purchase in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Promo Pricing is Present and Working as Expected after Adding a Model for Purchase in RU Locale")]
        public void ShoppingCart_VerifyPromoPricingAfterAddingAModelForPurchase(string Locale)
        {
            //--- Shopping Cart URL ---//
            string sc_page_url = Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx";
            if (Locale.Equals("jp") || Locale.Equals("ru"))
            {
                sc_page_url = sc_page_url + "?locale=" + Locale;
            }
            else if (Locale.Equals("cn") || Locale.Equals("zh"))
            {
                sc_page_url = sc_page_url + "?locale=zh";
            }

            action.Navigate(driver, sc_page_url);
            action.ILoginViaLogInLink(driver, biz_username, biz_password);

            util.RemoveAllModelsInYourCartTable(driver);

            //--- Action: Get Test Data ---//
            string adi_model = util.GetAdiModel();

            action.IAddModelToPurchase(driver, adi_model);

            action.IClick(driver, Elements.Shopping_Cart_Checkout_Button);
            Thread.Sleep(3000);

            action.GoToAboutYourProjectPage(driver);

            action.IPopulateFieldsInAboutYourProjectPage(driver);

            //----- R9 > T7: Verify the Customer Service link on the Enter Payment and Place Order page -----//

            //--- Inputs ---//
            string invalid_offerCode_input = "WEBPROMO12";

            //--- Action: Enter invalid Offer Code ---//
            action.IDeleteValueOnFields(driver, Elements.Shopping_Cart_PlaceOrder_OfferCode_Tbx);
            action.IType(driver, Elements.Shopping_Cart_PlaceOrder_OfferCode_Tbx, invalid_offerCode_input);

            //--- Action: Click on the Apply button ---//
            action.IClick(driver, Elements.Shopping_Cart_PlaceOrder_Apply_Btn);

            if (util.CheckElement(driver, Elements.Shopping_Cart_PlaceOrder_YourShoppingCart_Tbl, 2))
            {
                action.IMouseOverTo(driver, Elements.Shopping_Cart_PlaceOrder_YourShoppingCart_Tbl);
            }

            //--- Expected Result: The error message should be displayed ---//
            if (Locale.Equals("en"))
            {
                test.validateStringIsCorrect(driver, Elements.Shopping_Cart_PlaceOrder_InvalidOfferCode_ErrMsg, invalidOfferCode_errMsg);
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.Shopping_Cart_PlaceOrder_InvalidOfferCode_ErrMsg);
            }
        }
    }
}