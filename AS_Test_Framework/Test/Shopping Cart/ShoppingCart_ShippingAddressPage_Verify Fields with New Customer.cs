﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Threading;
using OpenQA.Selenium;

namespace AS_Test_Framework.ShoppingCart
{
    [TestFixture]
    public class ShoppingCart_ShippingAddressPage_VerifyFieldsWithNewCustomer : BaseSetUp
    {
        public ShoppingCart_ShippingAddressPage_VerifyFieldsWithNewCustomer() : base() { }

        //--- Login Credentials ---//
        string newCust_username = "sc_t3st_new_555@mailinator.com";
        string newCust_password = "Test_1234";

        //--- Labels ---//
        string orgName_note = "To avoid customs clearance issues, please use your full organization name without abbreviations";

        [Test, Category("Shopping Cart"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that the Fields are Present and Working as Expected after Adding a Model for Purchase using a New Customer in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Fields are Present and Working as Expected after Adding a Model for Purchase using a New Customer in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Fields are Present and Working as Expected after Adding a Model for Purchase using a New Customer in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Fields are Present and Working as Expected after Adding a Model for Purchase using a New Customer in RU Locale")]
        public void ShoppingCart_VerifyFieldsWithNewCustomerAfterAddingAModelForForPurchase(string Locale)
        {
            //--- Shopping Cart URL ---//
            string sc_page_url = Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx";
            if (Locale.Equals("jp") || Locale.Equals("ru"))
            {
                sc_page_url = sc_page_url + "?locale=" + Locale;
            }
            else if (Locale.Equals("cn") || Locale.Equals("zh"))
            {
                sc_page_url = sc_page_url + "?locale=zh";
            }

            action.Navigate(driver, sc_page_url);
            action.ILoginViaLogInLink(driver, newCust_username, newCust_password);

            util.RemoveAllModelsInYourCartTable(driver);

            //--- Action: Get Test Data ---//
            string adi_model = util.GetAdiModel();

            action.IAddModelToPurchase(driver, adi_model);

            action.IClick(driver, Elements.Shopping_Cart_Checkout_Button);
            Thread.Sleep(3000);

            //----- R5 > T6: Verify text added under organization name text box -----//

            //--- Expected Result: The message should display ----//
            if (Locale.Equals("en"))
            {
                test.validateStringIsCorrect(driver, Elements.Shopping_Cart_OrgName_Note_Txt, orgName_note);
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.Shopping_Cart_OrgName_Note_Txt);
            }

            //----- R5 > T1_1: Verify the Shipping Address page fields with new customer -----//

            Assert.Multiple(() =>
            {
                //--- Expected Result: Some of the fields on the Shipping Address page should be prepopulated based on the details available in the Account Information page ---//

                //--- Organization/Company Name ---//
                //test.validateStringIsCorrect(driver, Elements.Shopping_Cart_OrgName_Tbx, "Test Order");
                test.validateFieldIsNotEmpty(driver, Elements.Shopping_Cart_OrgName_Tbx);

                //--- First Name ---//
                //test.validateStringIsCorrect(driver, Elements.Shopping_Cart_FirstName_Tbx, "Test Order");
                test.validateFieldIsNotEmpty(driver, Elements.Shopping_Cart_FirstName_Tbx);

                //--- Last Name ---//
                //test.validateStringIsCorrect(driver, Elements.Shopping_Cart_LastName_Tbx, "Test Order");
                test.validateFieldIsNotEmpty(driver, Elements.Shopping_Cart_LastName_Tbx);

                //--- Country/Region Dropdown ---//
                test.validateSelectedValueIsCorrect(driver, Elements.Shopping_Cart_CountryRegion_Dd, "UNITED STATES");

                //--- Expected Result: All fields on the Shipping Address page should be editable. ---//
                test.validateElementIsEnabled(driver, Elements.Shopping_Cart_OrgName_Tbx);
                test.validateElementIsEnabled(driver, Elements.Shopping_Cart_FirstName_Tbx);
                test.validateElementIsEnabled(driver, Elements.Shopping_Cart_LastName_Tbx);
                test.validateElementIsEnabled(driver, Elements.Shopping_Cart_AddressLine1_Tbx);
                test.validateElementIsEnabled(driver, Elements.Shopping_Cart_AddressLine2_Tbx);
                test.validateElementIsEnabled(driver, Elements.Shopping_Cart_AddressLine3_Tbx);
                test.validateElementIsEnabled(driver, Elements.Shopping_Cart_City_Tbx);
                test.validateElementIsEnabled(driver, Elements.Shopping_Cart_StateProvince_Dd);
                test.validateElementIsEnabled(driver, Elements.Shopping_Cart_ZipPostalCode_Tbx);
                //test.validateElementIsEnabled(driver, Elements.Shopping_Cart_CountryRegion_Dd); --> NOTE: This was commented-out for the Changes in AL-18254: Web Orders/BOL allowing controlled ECCNs to be purchased
                test.validateElementIsEnabled(driver, Elements.Shopping_Cart_Telephone_Tbx);
            });

            //--- Action: Click on the Next button without completing the required fields ----//
            action.IDeleteValueOnFields(driver, Elements.Shopping_Cart_OrgName_Tbx);
            action.IDeleteValueOnFields(driver, Elements.Shopping_Cart_FirstName_Tbx);
            action.IDeleteValueOnFields(driver, Elements.Shopping_Cart_LastName_Tbx);
            action.IDeleteValueOnFields(driver, Elements.Shopping_Cart_AddressLine1_Tbx);
            action.IDeleteValueOnFields(driver, Elements.Shopping_Cart_AddressLine2_Tbx);
            action.IDeleteValueOnFields(driver, Elements.Shopping_Cart_AddressLine3_Tbx);
            action.IDeleteValueOnFields(driver, Elements.Shopping_Cart_City_Tbx);
            //action.IClick(driver, Elements.Shopping_Cart_CountryRegion_Dd); --> NOTE: This was commented-out for the Changes in AL-18254: Web Orders/BOL allowing controlled ECCNs to be purchased
            //action.IClick(driver, By.CssSelector("select[id$='Country']>option:nth-child(1)")); --> NOTE: This was commented-out for the Changes in AL-18254: Web Orders/BOL allowing controlled ECCNs to be purchased
            action.IClick(driver, Elements.Shopping_Cart_StateProvince_Dd);
            action.IClick(driver, By.CssSelector("select[id$='State']>option:nth-child(1)"));
            action.IDeleteValueOnFields(driver, Elements.Shopping_Cart_ZipPostalCode_Tbx);
            action.IDeleteValueOnFields(driver, Elements.Shopping_Cart_Telephone_Tbx);

            action.IClick(driver, Elements.Shopping_Cart_Heading_Next_Button);
            Thread.Sleep(1000);

            Assert.Multiple(() =>
            {
                //--- Expected Result: The error message for each blank field should be displayed below the input fields ---//
                test.validateElementIsPresent(driver, Elements.Shopping_Cart_ShippingAddress_OrgName_ErrMsg);
                test.validateElementIsPresent(driver, Elements.Shopping_Cart_ShippingAddress_FirstName_ErrMsg);
                test.validateElementIsPresent(driver, Elements.Shopping_Cart_ShippingAddress_LastName_ErrMsg);
                test.validateElementIsPresent(driver, Elements.Shopping_Cart_ShippingAddress_AddressLine1_ErrMsg);
                test.validateElementIsPresent(driver, Elements.Shopping_Cart_ShippingAddress_City_ErrMsg);
                test.validateElementIsPresent(driver, Elements.Shopping_Cart_ShippingAddress_StateProvince_ErrMsg);
                test.validateElementIsPresent(driver, Elements.Shopping_Cart_ShippingAddress_ZipPostalCode_ErrMsg);
                //test.validateElementIsPresent(driver, Elements.Shopping_Cart_ShippingAddress_CountryRegion_ErrMsg); --> NOTE: This was commented-out for the Changes in AL-18254: Web Orders/BOL allowing controlled ECCNs to be purchased
                test.validateElementIsPresent(driver, Elements.Shopping_Cart_ShippingAddress_Telephone_ErrMsg);
                test.validateElementIsPresent(driver, Elements.Shopping_Cart_ShippingAddress_TaxOption_ErrMsg);
            });
        }

        [Test, Category("Shopping Cart"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that the Fields are Present and Working as Expected after Adding a Model for Sample using a New Customer in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Fields are Present and Working as Expected after Adding a Model for Sample using a New Customer in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Fields are Present and Working as Expected after Adding a Model for Sample using a New Customer in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Fields are Present and Working as Expected after Adding a Model for Sample using a New Customer in RU Locale")]
        public void ShoppingCart_VerifyFieldsWithNewCustomerAfterAddingAModelForSample(string Locale)
        {
            if (!Util.Configuration.Environment.Equals("production"))
            {
                //--- Shopping Cart URL ---//
                string sc_page_url = Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx";
                if (Locale.Equals("jp") || Locale.Equals("ru"))
                {
                    sc_page_url = sc_page_url + "?locale=" + Locale;
                }
                else if (Locale.Equals("cn") || Locale.Equals("zh"))
                {
                    sc_page_url = sc_page_url + "?locale=zh";
                }

                action.Navigate(driver, sc_page_url);
                action.ILoginViaLogInLink(driver, newCust_username, newCust_password);

                util.RemoveAllModelsInYourCartTable(driver);

                //--- Action: Get Test Data ---//
                string adi_model = util.GetAdiModel();

                action.IAddModelToSample(driver, adi_model);

                action.IClick(driver, Elements.Shopping_Cart_Checkout_Button);
                Thread.Sleep(4000);

                //----- R5 > T6: Verify text added under organization name text box -----//

                //--- Expected Result: The message should display ----//
                if (Locale.Equals("en"))
                {
                    test.validateStringIsCorrect(driver, Elements.Shopping_Cart_OrgName_Note_Txt, orgName_note);
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.Shopping_Cart_OrgName_Note_Txt);
                }

                //----- R5 > T1_1: Verify the Shipping Address page fields with new customer -----//

                Assert.Multiple(() =>
                {
                    //--- Expected Result: Some of the fields on the Shipping Address page should be prepopulated based on the details available in the Account Information page ---//

                    //--- Organization/Company Name ---//
                    //test.validateStringIsCorrect(driver, Elements.Shopping_Cart_OrgName_Tbx, "Test Order");
                    test.validateFieldIsNotEmpty(driver, Elements.Shopping_Cart_OrgName_Tbx);

                    //--- First Name ---//
                    //test.validateStringIsCorrect(driver, Elements.Shopping_Cart_FirstName_Tbx, "Test Order");
                    test.validateFieldIsNotEmpty(driver, Elements.Shopping_Cart_FirstName_Tbx);

                    //--- Last Name ---//
                    //test.validateStringIsCorrect(driver, Elements.Shopping_Cart_LastName_Tbx, "Test Order");
                    test.validateFieldIsNotEmpty(driver, Elements.Shopping_Cart_LastName_Tbx);

                    //--- Country/Region Dropdown ---//
                    test.validateSelectedValueIsCorrect(driver, Elements.Shopping_Cart_CountryRegion_Dd, "UNITED STATES");

                    //--- Expected Result: All fields on the Shipping Address page should be editable. ---//
                    test.validateElementIsEnabled(driver, Elements.Shopping_Cart_OrgName_Tbx);
                    test.validateElementIsEnabled(driver, Elements.Shopping_Cart_FirstName_Tbx);
                    test.validateElementIsEnabled(driver, Elements.Shopping_Cart_LastName_Tbx);
                    test.validateElementIsEnabled(driver, Elements.Shopping_Cart_AddressLine1_Tbx);
                    test.validateElementIsEnabled(driver, Elements.Shopping_Cart_AddressLine2_Tbx);
                    test.validateElementIsEnabled(driver, Elements.Shopping_Cart_AddressLine3_Tbx);
                    test.validateElementIsEnabled(driver, Elements.Shopping_Cart_City_Tbx);
                    test.validateElementIsEnabled(driver, Elements.Shopping_Cart_StateProvince_Dd);
                    test.validateElementIsEnabled(driver, Elements.Shopping_Cart_ZipPostalCode_Tbx);
                    //test.validateElementIsEnabled(driver, Elements.Shopping_Cart_CountryRegion_Dd); --> NOTE: This was commented-out for the Changes in AL-18254: Web Orders/BOL allowing controlled ECCNs to be purchased
                    test.validateElementIsEnabled(driver, Elements.Shopping_Cart_Telephone_Tbx);
                });

                //--- Action: Click on the Next button without completing the required fields ----//
                action.IDeleteValueOnFields(driver, Elements.Shopping_Cart_OrgName_Tbx);
                action.IDeleteValueOnFields(driver, Elements.Shopping_Cart_FirstName_Tbx);
                action.IDeleteValueOnFields(driver, Elements.Shopping_Cart_LastName_Tbx);
                action.IDeleteValueOnFields(driver, Elements.Shopping_Cart_AddressLine1_Tbx);
                action.IDeleteValueOnFields(driver, Elements.Shopping_Cart_AddressLine2_Tbx);
                action.IDeleteValueOnFields(driver, Elements.Shopping_Cart_AddressLine3_Tbx);
                action.IDeleteValueOnFields(driver, Elements.Shopping_Cart_City_Tbx);
                //action.IClick(driver, Elements.Shopping_Cart_CountryRegion_Dd); --> NOTE: This was commented-out for the Changes in AL-18254: Web Orders/BOL allowing controlled ECCNs to be purchased
                //action.IClick(driver, By.CssSelector("select[id$='Country']>option:nth-child(1)")); --> NOTE: This was commented-out for the Changes in AL-18254: Web Orders/BOL allowing controlled ECCNs to be purchased
                action.IClick(driver, Elements.Shopping_Cart_StateProvince_Dd);
                action.IClick(driver, By.CssSelector("select[id$='State']>option:nth-child(1)"));
                action.IDeleteValueOnFields(driver, Elements.Shopping_Cart_ZipPostalCode_Tbx);
                action.IDeleteValueOnFields(driver, Elements.Shopping_Cart_Telephone_Tbx);

                action.IClick(driver, Elements.Shopping_Cart_Heading_Next_Button);
                Thread.Sleep(1000);

                Assert.Multiple(() =>
                {
                    //--- Expected Result: The error message for each blank field should be displayed below the input fields ---//
                    test.validateElementIsPresent(driver, Elements.Shopping_Cart_ShippingAddress_OrgName_ErrMsg);
                    test.validateElementIsPresent(driver, Elements.Shopping_Cart_ShippingAddress_FirstName_ErrMsg);
                    test.validateElementIsPresent(driver, Elements.Shopping_Cart_ShippingAddress_LastName_ErrMsg);
                    test.validateElementIsPresent(driver, Elements.Shopping_Cart_ShippingAddress_AddressLine1_ErrMsg);
                    test.validateElementIsPresent(driver, Elements.Shopping_Cart_ShippingAddress_City_ErrMsg);
                    test.validateElementIsPresent(driver, Elements.Shopping_Cart_ShippingAddress_StateProvince_ErrMsg);
                    test.validateElementIsPresent(driver, Elements.Shopping_Cart_ShippingAddress_ZipPostalCode_ErrMsg);
                    //test.validateElementIsPresent(driver, Elements.Shopping_Cart_ShippingAddress_CountryRegion_ErrMsg); --> NOTE: This was commented-out for the Changes in AL-18254: Web Orders/BOL allowing controlled ECCNs to be purchased
                    test.validateElementIsPresent(driver, Elements.Shopping_Cart_ShippingAddress_Telephone_ErrMsg);
                });
            }
        }
    }
}