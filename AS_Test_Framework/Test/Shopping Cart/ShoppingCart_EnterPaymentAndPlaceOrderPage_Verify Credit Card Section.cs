﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Threading;

namespace AS_Test_Framework.ShoppingCart
{
    [TestFixture]
    public class ShoppingCart_EnterPaymentAndPlaceOrderPage_VerifyCreditCardSection : BaseSetUp
    {
        public ShoppingCart_EnterPaymentAndPlaceOrderPage_VerifyCreditCardSection() : base() { }

        //--- Login Credentials ---//
        string username = "marvin.bebe@analog.com";
        string password = "Test_1234";

        [Test, Category("Shopping Cart"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that the Credit Card Section is Present and Working as Expected after Adding a Model for Purchase in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Credit Card Section is Present and Working as Expected after Adding a Model for Purchase in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Credit Card Section is Present and Working as Expected after Adding a Model for Purchase in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Credit Card Section is Present and Working as Expected after Adding a Model for Purchase in RU Locale")]
        public void ShoppingCart_VerifyCreditCardSectionAfterAddingAModelForPurchase(string Locale)
        {
            //--- Shopping Cart URL ---//
            string sc_page_url = Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx";
            if (Locale.Equals("jp") || Locale.Equals("ru"))
            {
                sc_page_url = sc_page_url + "?locale=" + Locale;
            }
            else if (Locale.Equals("cn") || Locale.Equals("zh"))
            {
                sc_page_url = sc_page_url + "?locale=zh";
            }

            action.Navigate(driver, sc_page_url);
            action.ILoginViaLogInLink(driver, username, password);

            util.RemoveAllModelsInYourCartTable(driver);

            //--- Action: Get Test Data ---//
            string adi_ModelNo = util.GetAdiModel();

            action.IAddModelToPurchase(driver, adi_ModelNo);

            action.IClick(driver, Elements.Shopping_Cart_Checkout_Button);
            Thread.Sleep(3000);

            action.GoToAboutYourProjectPage(driver);

            action.IPopulateFieldsInAboutYourProjectPage(driver);

            //if (util.GetCount(driver, By.CssSelector("select[id='ddlShippingMethod']>option")) > 2)
            //{
            //    action.ISelectFromDropdown(driver, Elements.Shopping_Cart_PlaceOrder_ShippingMethod_Dd, "F1");
            //}
            //else
            //{
            //    action.IClick(driver, By.CssSelector("select[id='ddlShippingMethod']>option:nth-child(2)"));
            //}
            //Thread.Sleep(500);

            action.IMouseOverTo(driver, Elements.Shopping_Cart_PlaceOrder_Payment_Sec);
            Thread.Sleep(500);

            driver.SwitchTo().Frame(driver.FindElement(By.CssSelector("iframe[class='fit-to-parent']")));

            //----- R9 > T6: Verify Payment section within the Enter Payment and Place Order page -----//

            //--- Expected Result: The following elements should be displayed ---//

            //--- Card number text field ---//
            test.validateElementIsPresent(driver, Elements.Shopping_Cart_PlaceOrder_Cc_CardNo_Tbx);

            //--- Card Holder Name Field ---//
            test.validateElementIsPresent(driver, Elements.Shopping_Cart_PlaceOrder_Cc_CardHolderName_Tbx);

            //--- Expiration date Field ---//
            test.validateElementIsPresent(driver, Elements.Shopping_Cart_PlaceOrder_Cc_Expiry_Tbx);

            //--- CVV ---//
            test.validateElementIsPresent(driver, Elements.Shopping_Cart_PlaceOrder_Cc_Cvv_Tbx);

            //----- R9 > T6: Verify Master Card with 2 series (AL-11422) -----//

            //--- Inputs ---//
            string cardNo_input = "2223000010476510";
            string cardName_input = "Test Auto";
            string expiry_input = "12/" + System.DateTime.Now.ToString("yy");
            string cvv_input = "316";

            //--- Action: Enter Card Number ---//
            action.IDeleteValueOnFields(driver, Elements.Shopping_Cart_PlaceOrder_Cc_CardNo_Tbx);
            action.IType(driver, Elements.Shopping_Cart_PlaceOrder_Cc_CardNo_Tbx, cardNo_input);

            //--- Action: Enter Card Name ---//
            action.IDeleteValueOnFields(driver, Elements.Shopping_Cart_PlaceOrder_Cc_CardHolderName_Tbx);
            action.IType(driver, Elements.Shopping_Cart_PlaceOrder_Cc_CardHolderName_Tbx, cardName_input);

            //--- Action: Enter Expiration Date ---//
            action.IDeleteValueOnFields(driver, Elements.Shopping_Cart_PlaceOrder_Cc_Expiry_Tbx);
            action.IType(driver, Elements.Shopping_Cart_PlaceOrder_Cc_Expiry_Tbx, expiry_input);

            //--- Action: Enter CVV ---//
            action.IDeleteValueOnFields(driver, Elements.Shopping_Cart_PlaceOrder_Cc_Cvv_Tbx);
            action.IType(driver, Elements.Shopping_Cart_PlaceOrder_Cc_Cvv_Tbx, cvv_input);

            action.IClick(driver, Elements.Shopping_Cart_PlaceOrder_Cc_Submit_Btn);
            Thread.Sleep(1000);

            test.validateElementIsPresent(driver, Elements.Shopping_Cart_PlaceOrder_Submitted_Cc_Sec);
        }

        [Test, Category("Shopping Cart"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that the Input Validations are Working as Expected after Adding a Model for Purchase in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Input Validations are Working as Expected after Adding a Model for Purchase in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Input Validations are Working as Expected after Adding a Model for Purchase in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Input Validations are Working as Expected after Adding a Model for Purchase in RU Locale")]
        public void ShoppingCart_VerifyInputValidationsAfterAddingAModelForPurchase(string Locale)
        {
            //--- Shopping Cart URL ---//
            string sc_page_url = Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx";
            if (Locale.Equals("jp") || Locale.Equals("ru"))
            {
                sc_page_url = sc_page_url + "?locale=" + Locale;
            }
            else if (Locale.Equals("cn") || Locale.Equals("zh"))
            {
                sc_page_url = sc_page_url + "?locale=zh";
            }

            action.Navigate(driver, sc_page_url);
            action.ILoginViaLogInLink(driver, username, password);

            util.RemoveAllModelsInYourCartTable(driver);

            //--- Action: Get Test Data ---//
            string adi_ModelNo = util.GetAdiModel();

            action.IAddModelToPurchase(driver, adi_ModelNo);

            action.IClick(driver, Elements.Shopping_Cart_Checkout_Button);
            Thread.Sleep(3000);

            action.GoToAboutYourProjectPage(driver);

            action.IPopulateFieldsInAboutYourProjectPage(driver);

            //if (util.GetCount(driver, By.CssSelector("select[id='ddlShippingMethod']>option")) > 2)
            //{
            //    action.ISelectFromDropdown(driver, Elements.Shopping_Cart_PlaceOrder_ShippingMethod_Dd, "F1");
            //}
            //else
            //{
            //    action.IClick(driver, By.CssSelector("select[id='ddlShippingMethod']>option:nth-child(2)"));
            //}
            //Thread.Sleep(500);

            action.IMouseOverTo(driver, Elements.Shopping_Cart_PlaceOrder_Payment_Sec);
            Thread.Sleep(500);

            driver.SwitchTo().Frame(driver.FindElement(By.CssSelector("iframe[class='fit-to-parent']")));

            //----- R14 > T2_1: Verify the CVV / CID text field with invalid data for Visa and Master Card cardholders where valid value is 3 digits -----//

            //--- Inputs ---//
            string cardNo_input = "4111111111111111";
            string cardName_input = "Test Auto";
            string expiry_input = "12/" + System.DateTime.Now.ToString("yy");
            string cvv_specialChar_input = "$";

            //--- Action: Enter Card Number ---//
            action.IDeleteValueOnFields(driver, Elements.Shopping_Cart_PlaceOrder_Cc_CardNo_Tbx);
            action.IType(driver, Elements.Shopping_Cart_PlaceOrder_Cc_CardNo_Tbx, cardNo_input);

            //--- Action: Enter Card Name ---//
            action.IDeleteValueOnFields(driver, Elements.Shopping_Cart_PlaceOrder_Cc_CardHolderName_Tbx);
            action.IType(driver, Elements.Shopping_Cart_PlaceOrder_Cc_CardHolderName_Tbx, cardName_input);

            //--- Action: Enter Expiration Date ---//
            action.IDeleteValueOnFields(driver, Elements.Shopping_Cart_PlaceOrder_Cc_Expiry_Tbx);
            action.IType(driver, Elements.Shopping_Cart_PlaceOrder_Cc_Expiry_Tbx, expiry_input);

            //--- Action: Enter CVV value with invalid number of digits ---//
            action.IDeleteValueOnFields(driver, Elements.Shopping_Cart_PlaceOrder_Cc_Cvv_Tbx);
            action.IType(driver, Elements.Shopping_Cart_PlaceOrder_Cc_Cvv_Tbx, cvv_specialChar_input);

            //--- Expected Result: The entered value should not be accepted ---//
            test.validateStringInstance(driver, "", util.ReturnAttribute(driver, Elements.Shopping_Cart_PlaceOrder_Cc_Cvv_Tbx, "value"));

            //----- R14 > T2_1: Verify the CVV / CID text field with invalid data for American Express cardholders where valid value is 4 digits -----//

            //--- Inputs ---//
            cardNo_input = "5454545454545450";
            cardName_input = "Test Auto";
            expiry_input = "12/" + System.DateTime.Now.ToString("yy");
            string cvv_invalid_input = "1";
            cvv_specialChar_input = "#";

            //--- Action: Enter Card Number ---//
            action.IDeleteValueOnFields(driver, Elements.Shopping_Cart_PlaceOrder_Cc_CardNo_Tbx);
            action.IType(driver, Elements.Shopping_Cart_PlaceOrder_Cc_CardNo_Tbx, cardNo_input);

            //--- Action: Enter Card Name ---//
            action.IDeleteValueOnFields(driver, Elements.Shopping_Cart_PlaceOrder_Cc_CardHolderName_Tbx);
            action.IType(driver, Elements.Shopping_Cart_PlaceOrder_Cc_CardHolderName_Tbx, cardName_input);

            //--- Action: Enter Expiration Date ---//
            action.IDeleteValueOnFields(driver, Elements.Shopping_Cart_PlaceOrder_Cc_Expiry_Tbx);
            action.IType(driver, Elements.Shopping_Cart_PlaceOrder_Cc_Expiry_Tbx, expiry_input);

            //--- Action: Enter CVV with invalid number of digits ---//
            action.IDeleteValueOnFields(driver, Elements.Shopping_Cart_PlaceOrder_Cc_Cvv_Tbx);
            action.IType(driver, Elements.Shopping_Cart_PlaceOrder_Cc_Cvv_Tbx, cvv_invalid_input);

            //--- Expected Result: An error message should be displayed ---//
            test.validateElementIsPresent(driver, Elements.Shopping_Cart_PlaceOrder_Cc_InvalidCvv_ErrMsg);

            //--- Action: Enter CVV value with special character ---//
            action.IDeleteValueOnFields(driver, Elements.Shopping_Cart_PlaceOrder_Cc_Cvv_Tbx);
            action.IType(driver, Elements.Shopping_Cart_PlaceOrder_Cc_Cvv_Tbx, cvv_specialChar_input);

            //--- Expected Result: The entered value should not be accepted ---//
            test.validateStringInstance(driver, "", util.ReturnAttribute(driver, Elements.Shopping_Cart_PlaceOrder_Cc_Cvv_Tbx, "value"));

            //----- ECommerce TestPlan > Test Case Title: Verify Payment section within the Enter Payment and Place Order page -----//
            action.IDeleteValueOnFields(driver, Elements.Shopping_Cart_PlaceOrder_Cc_CardNo_Tbx);
            action.IDeleteValueOnFields(driver, Elements.Shopping_Cart_PlaceOrder_Cc_Expiry_Tbx);
            action.IDeleteValueOnFields(driver, Elements.Shopping_Cart_PlaceOrder_Cc_Cvv_Tbx);

            //--- Inputs ---//
            string cardNo_invalid_input = "1234567891234567";

            //--- Action: Enter invalid card number ---//
            action.IDeleteValueOnFields(driver, Elements.Shopping_Cart_PlaceOrder_Cc_CardNo_Tbx);
            action.IType(driver, Elements.Shopping_Cart_PlaceOrder_Cc_CardNo_Tbx, cardNo_invalid_input);

            //--- Action: Tick the checkbox for Terms and Conditions ---//
            action.IClick(driver, Elements.AcceptSCTerms);

            //--- Action: Click on the Place Order button ---//
            action.IClick(driver, By.CssSelector("input[id='btnNext2']"));

            //--- Expected Result: The error message below should be displayed: Card number is invalid. ---//
            test.validateElementIsPresent(driver, Elements.Shopping_Cart_PlaceOrder_Cc_InvalidCardNo_ErrMsg);
        }
    }
}