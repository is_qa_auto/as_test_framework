﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.ShoppingCart
{
    [TestFixture]
    public class ShoppingCart_ShoppingCartPage_AddingANotSampleableModelForSample : BaseSetUp
    {
        public ShoppingCart_ShoppingCartPage_AddingANotSampleableModelForSample() : base() { }

        //--- Login Credentials ---//
        string biz_username = "marvin.bebe@analog.com";
        string biz_password = "Test_1234";

        //--- Test Data ---//
        string invalid_notSampleable_model = "abcd";
        string valid_notSampleable_model = "AD7705";

        [Test, Category("Shopping Cart"), Category("NonCore")]
        [TestCase("en", TestName = "Verify Adding an Invalid Not Sampleable Model for Sample using a Business Email in EN Locale")]
        //[TestCase("cn", TestName = "Verify Adding an Invalid Not Sampleable Model for Sample using a Business Email in CN Locale")]
        //[TestCase("jp", TestName = "Verify Adding an Invalid Not Sampleable Model for Sample using a Business Email in JP Locale")]
        //[TestCase("ru", TestName = "Verify Adding an Invalid Not Sampleable Model for Sample using a Business Email in RU Locale")]
        public void ShoppingCart_VerifyAddingAnInvalidNotSampleableModelForSampleUsingABusinessEmail(string Locale)
        {
            //--- Shopping Cart URL ---//
            string sc_page_url = Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx";
            if (Locale.Equals("jp") || Locale.Equals("ru"))
            {
                sc_page_url = sc_page_url + "?locale=" + Locale;
            }
            else if (Locale.Equals("cn") || Locale.Equals("zh"))
            {
                sc_page_url = sc_page_url + "?locale=zh";
            }

            action.Navigate(driver, sc_page_url);
            action.ILoginViaLogInLink(driver, biz_username, biz_password);

            util.RemoveAllModelsInYourCartTable(driver);

            //----- R2 > T4_1: Verify model number field with model that is not sampleable -----//
            action.IAddModelToSample(driver, invalid_notSampleable_model);

            if (Locale.Equals("en"))
            {
                test.validateStringIsCorrect(driver, Elements.SC_ErrorMessage_Label, "No models were found for " + invalid_notSampleable_model.ToUpper() + ". Please re-enter or try site search to find products. (Error : 001)");
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.SC_ErrorMessage_Label);
            }
        }

        [Test, Category("Shopping Cart"), Category("NonCore")]
        [TestCase("en", TestName = "Verify Adding an Invalid Not Sampleable Model for Sample as Anonymous User in EN Locale")]
        //[TestCase("cn", TestName = "Verify Adding an Invalid Not Sampleable Model for Sample as Anonymous User in CN Locale")]
        //[TestCase("jp", TestName = "Verify Adding an Invalid Not Sampleable Model for Sample as Anonymous User in JP Locale")]
        //[TestCase("ru", TestName = "Verify Adding an Invalid Not Sampleable Model for Sample as Anonymous User in RU Locale")]
        public void ShoppingCart_VerifyAddingAnInvalidNotSampleableModelForSampleAsAnonymousUser(string Locale)
        {
            //--- Shopping Cart URL ---//
            string sc_page_url = Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx";
            if (Locale.Equals("jp") || Locale.Equals("ru"))
            {
                sc_page_url = sc_page_url + "?locale=" + Locale;
            }
            else if (Locale.Equals("cn") || Locale.Equals("zh"))
            {
                sc_page_url = sc_page_url + "?locale=zh";
            }

            //----- R4 > T5 > S1-S5: Verify model number field with model that is not sampleable -----//

            action.Navigate(driver, sc_page_url);

            //--- Select any country on the Select Shipping Country dropdown (e.g. UNITED STATES) ---//
            action.IClick(driver, Elements.Shopping_Cart_Country_Dropdown);
            action.IClick(driver, By.CssSelector("li[value='US']>a"));

            util.RemoveAllModelsInYourCartTable(driver);

            action.IAddModelToSample(driver, invalid_notSampleable_model);

            Assert.Multiple(() =>
            {
                if (Locale.Equals("en"))
                {
                    test.validateStringIsCorrect(driver, Elements.SC_ErrorMessage_Label, invalid_notSampleable_model.ToUpper() + " is not available for sample on www.analog.com. Go to " + invalid_notSampleable_model.ToUpper() + " product page to view other options.");
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.SC_ErrorMessage_Label);
                }

                if (!Locale.Equals("jp"))
                {
                    //--- Click on the model number hyperlink ---//
                    action.IClick(driver, Elements.SC_ErrorMessage_ProductPage_Link);

                    //--- The Analog error page should be displayed ---//
                    driver.SwitchTo().Window(driver.WindowHandles.Last());
                    test.validateStringInstance(driver, driver.Title, "Page Not Found");
                }
            });
        }

        [Test, Category("Shopping Cart"), Category("NonCore")]
        [TestCase("en", TestName = "Verify Adding a Valid Not Sampleable Model for Sample as Anonymous User in EN Locale")]
        //[TestCase("cn", TestName = "Verify Adding a Valid Not Sampleable Model for Sample as Anonymous User in CN Locale")]
        //[TestCase("jp", TestName = "Verify Adding a Valid Not Sampleable Model for Sample as Anonymous User in JP Locale")]
        //[TestCase("ru", TestName = "Verify Adding a Valid Not Sampleable Model for Sample as Anonymous User in RU Locale")]
        public void ShoppingCart_VerifyAddingAValidNotSampleableModelForSampleAsAnonymousUser(string Locale)
        {
            //--- Shopping Cart URL ---//
            string sc_page_url = Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx";
            if (Locale.Equals("jp") || Locale.Equals("ru"))
            {
                sc_page_url = sc_page_url + "?locale=" + Locale;
            }
            else if (Locale.Equals("cn") || Locale.Equals("zh"))
            {
                sc_page_url = sc_page_url + "?locale=zh";
            }

            //----- R4 > T5 > S6-S9: Verify model number field with model that is not sampleable -----//

            action.Navigate(driver, sc_page_url);

            //--- Select any country on the Select Shipping Country dropdown (e.g. UNITED STATES) ---//
            action.IClick(driver, Elements.Shopping_Cart_Country_Dropdown);
            action.IClick(driver, By.CssSelector("li[value='US']>a"));

            util.RemoveAllModelsInYourCartTable(driver);

            action.IAddModelToSample(driver, valid_notSampleable_model);

            Assert.Multiple(() =>
            {
                if (Locale.Equals("en"))
                {
                    test.validateStringIsCorrect(driver, Elements.SC_ErrorMessage_Label, valid_notSampleable_model.ToUpper() + " is not available for sample on www.analog.com. Go to " + valid_notSampleable_model.ToUpper() + " product page to view other options.");
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.SC_ErrorMessage_Label);
                }

                //--- Click on the product number hyperlink ---//
                action.IOpenLinkInNewTab(driver, Elements.SC_ErrorMessage_ProductPage_Link);

                //--- The Product page should be displayed in new tab/window ---//
                test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + "/products/");
            });
        }
    }
}