﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;

namespace AS_Test_Framework.ShoppingCart
{
    [TestFixture]
    public class ShoppingCart_ShoppingCartPage_VerifyShowCompanionProductsLink : BaseSetUp
    {
        public ShoppingCart_ShoppingCartPage_VerifyShowCompanionProductsLink() : base() { }

        //--- Login Credentials ---//
        string biz_username = "marvin.bebe@analog.com";
        string biz_password = "Test_1234";

        [Test, Category("Shopping Cart"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that the Show Companion Products Link is Present and Working as Expected using a Business Email in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Show Companion Products Link is Present and Working as Expected using a Business Email in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Show Companion Products Link is Present and Working as Expected using a Business Email in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Show Companion Products Link is Present and Working as Expected using a Business Email in RU Locale")]
        public void ShoppingCart_VerifyShowCompanionProductsLinkUsingABusinessEmail(string Locale)
        {
            //--- Shopping Cart URL ---//
            string sc_page_url = Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx";
            if (Locale.Equals("jp") || Locale.Equals("ru"))
            {
                sc_page_url = sc_page_url + "?locale=" + Locale;
            }
            else if (Locale.Equals("cn") || Locale.Equals("zh"))
            {
                sc_page_url = sc_page_url + "?locale=zh";
            }

            action.Navigate(driver, sc_page_url);
            action.ILoginViaLogInLink(driver, biz_username, biz_password);

            util.RemoveAllModelsInYourCartTable(driver);

            //----- R2 > T10: Verify the Show Companion Products link (AL-7433) -----//

            //--- Action: Get Test Data ---//
            string adi_model = util.GetAdiModel();

            action.IAddModelToPurchase(driver, adi_model);

            Assert.Multiple(() =>
            {
                //--- Expected Result: The link 'Show Companion Products' should be displayed below the product number and description. ---//
                test.validateElementIsPresent(driver, Elements.Shopping_Cart_ShwCompanionProducts_Link);

                //--- Action: Click on the 'Show Companion Products' link ---//
                action.IClick(driver, Elements.Shopping_Cart_ShwCompanionProducts_Link);

                //--- Expected Result: The Suggested Companion Products light box should be displayed. ---//
                test.validateElementIsPresent(driver, Elements.Shopping_Cart_ShwCompanionProducts_LtBx);
            });
        }
    }
}