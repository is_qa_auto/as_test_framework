﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Threading;

namespace AS_Test_Framework.ShoppingCart
{
    [TestFixture]
    public class ShoppingCart_OrderConfirmation_VerifyOneLtcModelPurchase_AndOneAdiModelPurchase : BaseSetUp
    {
        public ShoppingCart_OrderConfirmation_VerifyOneLtcModelPurchase_AndOneAdiModelPurchase() : base() { }

        //--- Login Credentials ---//
        string username = "myanalog_t3st_111@mailinator.com";
        string password = "Test_1234";

        //--- URLs ---//
        string orderConfirmation_url = "OrderConfirmation.aspx";

        [Test, Category("Shopping Cart"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that the Order Confirmation Page is Present and Working as Expected after Placing 1 LTC Model for Purchase and 1 ADI Model for Purchase in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Order Confirmation Page is Present and Working as Expected after Placing 1 LTC Model for Purchase and 1 ADI Model for Purchase in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Order Confirmation Page is Present and Working as Expected after Placing 1 LTC Model for Purchase and 1 ADI Model for Purchase in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Order Confirmation Page is Present and Working as Expected after Placing 1 LTC Model for Purchase and 1 ADI Model for Purchase in RU Locale")]
        public void ShoppingCart_VerifyOneLtcModelPurchase_AndOneAdiModelPurchase(string Locale)
        {
            if (!Util.Configuration.Environment.Equals("production") && Locale.Equals("en"))
            {
                //--- Shopping Cart URL ---//
                string sc_page_url = Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx";
                if (Locale.Equals("jp") || Locale.Equals("ru"))
                {
                    sc_page_url = sc_page_url + "?locale=" + Locale;
                }
                else if (Locale.Equals("cn") || Locale.Equals("zh"))
                {
                    sc_page_url = sc_page_url + "?locale=zh";
                }

                action.Navigate(driver, sc_page_url);

                action.ILoginViaLogInLink(driver, username, password);

                util.RemoveAllModelsInYourCartTable(driver);

                //----- R20 > T7 > S1: Process LTC product and AD product to purchase -----//

                //--- Action: Get Test Data ---//
                string ltc_model = util.GetLtcModel();
                string adi_model = util.GetAdiModel();

                action.IAddModelToPurchase(driver, ltc_model);
                action.IAddModelToPurchase(driver, adi_model);

                action.IClick(driver, Elements.Shopping_Cart_Checkout_Button);
                Thread.Sleep(3000);

                action.IPopulateFieldsInShippingAddressPage(driver);

                action.IPopulateFieldsInBillingAddressPage(driver);

                action.IPopulateFieldsInAboutYourProjectPage(driver);

                action.IPopulateFieldsInEnterPaymentAndPlaceOrderPage(driver);

                if (driver.Url.Contains("analog.com/" + orderConfirmation_url + "?locale="))
                {
                    //--- Expected Result: In Order Confirmation page there should only have 1 order number for both LTC and AD product purchase ---//
                    test.validateCountIsEqual(driver, 1, util.GetCount(driver, Elements.Shopping_Cart_OrderConfirmation_Purchase_OrderRefNos));

                    //--- Cancel Orders in myAnalog ---//
                    action.ICancelOrdersInMyAnalog(driver, username, password);
                }
                else
                {
                    if (Locale.Equals("cn") || Locale.Equals("zh"))
                    {
                        test.validateStringInstance(driver, driver.Url, "analog.com/" + orderConfirmation_url + "?locale=zh");
                    }
                    else
                    {
                        test.validateStringInstance(driver, driver.Url, "analog.com/" + orderConfirmation_url + "?locale=" + Locale);
                    }
                }
            }
        }
    }
}