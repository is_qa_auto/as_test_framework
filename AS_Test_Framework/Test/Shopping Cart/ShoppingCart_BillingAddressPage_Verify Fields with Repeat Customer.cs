﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Threading;

namespace AS_Test_Framework.ShoppingCart
{
    [TestFixture]
    public class ShoppingCart_BillingAddressPage_VerifyFieldsWithRepeatCustomer : BaseSetUp
    {
        public ShoppingCart_BillingAddressPage_VerifyFieldsWithRepeatCustomer() : base() { }

        //--- Login Credentials ---//
        string repeatCust_username = "marvin.bebe@analog.com";
        string repeatCust_password = "Test_1234";

        //--- Labels ---//
        string orgName_note = "To avoid customs clearance issues, please use your full organization name without abbreviations";

        [Test, Category("Shopping Cart"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that the Fields are Present and Working as Expected after Adding a Model for Purchase using a Repeat Customer in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Fields are Present and Working as Expected after Adding a Model for Purchase using a Repeat Customer in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Fields are Present and Working as Expected after Adding a Model for Purchase using a Repeat Customer in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Fields are Present and Working as Expected after Adding a Model for Purchase using a Repeat Customer in RU Locale")]
        public void ShoppingCart_VerifyFieldsWithRepeatCustomerAfterAddingAModelForForPurchase(string Locale)
        {
            //--- Shopping Cart URL ---//
            string sc_page_url = Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx";
            if (Locale.Equals("jp") || Locale.Equals("ru"))
            {
                sc_page_url = sc_page_url + "?locale=" + Locale;
            }
            else if (Locale.Equals("cn") || Locale.Equals("zh"))
            {
                sc_page_url = sc_page_url + "?locale=zh";
            }

            action.Navigate(driver, sc_page_url);
            action.ILoginViaLogInLink(driver, repeatCust_username, repeatCust_password);

            util.RemoveAllModelsInYourCartTable(driver);

            //--- Action: Get Test Data ---//
            string adi_model = util.GetAdiModel();

            action.IAddModelToPurchase(driver, adi_model);

            action.IClick(driver, Elements.Shopping_Cart_Checkout_Button);
            Thread.Sleep(3000);

            if (util.CheckElement(driver, Elements.Shopping_Cart_ShippingAddress_PrevAdd_Sec, 2))
            {
                action.GoToBillingAddressPage(driver);

                //----- R5 > T7: Verify text added under organization name text box -----//

                //--- Expected Result: The message should display ----//
                if (Locale.Equals("en"))
                {
                    test.validateStringIsCorrect(driver, Elements.Shopping_Cart_OrgName_Note_Txt, orgName_note);
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.Shopping_Cart_OrgName_Note_Txt);
                }

                //----- R5 > T2_2: Verify Billing Address page for repeat customer -----//

                //--- Expected Result: The 'Use my shipping address as my billing address' checkbox should not be displayed. ---//
                test.validateElementIsNotPresent(driver, Elements.Shopping_Cart_BillingAddress_UseMyShippingAddAsMyBillingAdd_Cb);

                //--- Expected Result: Country dropdown on billing address for repeat user should be disabled ---//
                test.validateElementIsNotEnabled(driver, Elements.Shopping_Cart_CountryRegion_Dd);

                //--- Expected Result: The disclaimer message should be displayed on the upper portion of the billing address page ---//
                test.validateElementIsPresent(driver, Elements.Shopping_Cart_BillingAddress_Disclaimer_Txt);

                //----- ECommerce TestPlan > Test Case Title: Verify Billing Address page mandatory fields -----//

                //--- Expected Result: All input fields on the Shipping Address page should be editable ---//
                test.validateElementIsEnabled(driver, Elements.Shopping_Cart_OrgName_Tbx);
                test.validateElementIsEnabled(driver, Elements.Shopping_Cart_FirstName_Tbx);
                test.validateElementIsEnabled(driver, Elements.Shopping_Cart_LastName_Tbx);
                test.validateElementIsEnabled(driver, Elements.Shopping_Cart_AddressLine1_Tbx);
                test.validateElementIsEnabled(driver, Elements.Shopping_Cart_AddressLine2_Tbx);
                test.validateElementIsEnabled(driver, Elements.Shopping_Cart_AddressLine3_Tbx);
                test.validateElementIsEnabled(driver, Elements.Shopping_Cart_City_Tbx);
                test.validateElementIsEnabled(driver, Elements.Shopping_Cart_StateProvince_Dd);
                test.validateElementIsEnabled(driver, Elements.Shopping_Cart_ZipPostalCode_Tbx);
                test.validateElementIsEnabled(driver, Elements.Shopping_Cart_Telephone_Tbx);
            }
        }

        [Test, Category("Shopping Cart"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that the Input Validations are Working as Expected after Adding a Model for Purchase in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Input Validations are Working as Expected after Adding a Model for Purchase in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Input Validations are Working as Expected after Adding a Model for Purchase in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Input Validations are  Working as Expected after Adding a Model for Purchase in RU Locale")]
        public void ShoppingCart_VerifyInputValidationsAfterAddingAModelForForPurchase(string Locale)
        {
            //--- Shopping Cart URL ---//
            string sc_page_url = Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx";
            if (Locale.Equals("jp") || Locale.Equals("ru"))
            {
                sc_page_url = sc_page_url + "?locale=" + Locale;
            }
            else if (Locale.Equals("cn") || Locale.Equals("zh"))
            {
                sc_page_url = sc_page_url + "?locale=zh";
            }

            action.Navigate(driver, sc_page_url);
            action.ILoginViaLogInLink(driver, repeatCust_username, repeatCust_password);

            util.RemoveAllModelsInYourCartTable(driver);

            //--- Action: Get Test Data ---//
            string adi_model = util.GetAdiModel();

            //--- Action: User add products for Purchase on the Cart ---//
            action.IAddModelToPurchase(driver, adi_model);

            //--- Action: Click the Checkout Button ---//
            action.IClick(driver, Elements.Shopping_Cart_Checkout_Button);
            Thread.Sleep(3000);

            //--- Action: Complete all the mandatory fields on the Shipping Address page and click on the Next button ---//
            action.GoToBillingAddressPage(driver);

            //----- ECommerce TestPlan > Test Case Title: Verify Billing Address page mandatory fields -----//

            //--- Action: Click on the Next button without completing ALL the required fields ---//
            action.IDeleteValueOnFields(driver, Elements.Shopping_Cart_OrgName_Tbx);
            action.IDeleteValueOnFields(driver, Elements.Shopping_Cart_FirstName_Tbx);
            action.IDeleteValueOnFields(driver, Elements.Shopping_Cart_LastName_Tbx);
            action.IDeleteValueOnFields(driver, Elements.Shopping_Cart_AddressLine1_Tbx);
            action.IDeleteValueOnFields(driver, Elements.Shopping_Cart_AddressLine2_Tbx);
            action.IDeleteValueOnFields(driver, Elements.Shopping_Cart_AddressLine3_Tbx);
            action.IDeleteValueOnFields(driver, Elements.Shopping_Cart_City_Tbx);
            action.IClick(driver, Elements.Shopping_Cart_StateProvince_Dd);
            action.IClick(driver, By.CssSelector("select[id$='State']>option:nth-child(1)"));
            action.IDeleteValueOnFields(driver, Elements.Shopping_Cart_ZipPostalCode_Tbx);
            action.IDeleteValueOnFields(driver, Elements.Shopping_Cart_Telephone_Tbx);
            action.IClick(driver, Elements.Shopping_Cart_Heading_Next_Button);

            //--- Expected Result: The error message below should be displayed ---//
            test.validateElementIsPresent(driver, Elements.Shopping_Cart_BillingAddress_OrgName_ErrMsg);
            test.validateElementIsPresent(driver, Elements.Shopping_Cart_BillingAddress_FirstName_ErrMsg);
            test.validateElementIsPresent(driver, Elements.Shopping_Cart_BillingAddress_LastName_ErrMsg);
            test.validateElementIsPresent(driver, Elements.Shopping_Cart_BillingAddress_AddressLine1_ErrMsg);
            test.validateElementIsPresent(driver, Elements.Shopping_Cart_BillingAddress_City_ErrMsg);
            test.validateElementIsPresent(driver, Elements.Shopping_Cart_BillingAddress_StateProvince_ErrMsg);
            test.validateElementIsPresent(driver, Elements.Shopping_Cart_BillingAddress_ZipPostalCode_ErrMsg);
            test.validateElementIsPresent(driver, Elements.Shopping_Cart_BillingAddress_Telephone_ErrMsg);
        }
    }
}