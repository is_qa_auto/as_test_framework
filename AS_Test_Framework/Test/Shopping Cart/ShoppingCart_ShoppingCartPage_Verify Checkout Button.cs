﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Threading;

namespace AS_Test_Framework.ShoppingCart
{
    [TestFixture]
    public class ShoppingCart_ShoppingCartPage_VerifyCheckoutButton : BaseSetUp
    {
        public ShoppingCart_ShoppingCartPage_VerifyCheckoutButton() : base() { }

        //--- Login Credentials ---//
        string biz_username = "marvin.bebe@analog.com";
        string biz_password = "Test_1234";

        //--- URLs ---//
        string shippingInformation_url = "ShippingAddress.aspx";

        [Test, Category("Shopping Cart"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that the Checkout Button is Present and Works as Expected using a Business Email in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Checkout Button is Present and Works as Expected using a Business Email in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Checkout Button is Present and Works as Expected using a Business Email in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Checkout Button is Present and Works as Expected using a Business Email in RU Locale")]
        public void ShoppingCart_VerifyCheckoutButtonUsingABusinessEmail(string Locale)
        {
            //--- Shopping Cart URL ---//
            string sc_page_url = Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx";
            if (Locale.Equals("jp") || Locale.Equals("ru"))
            {
                sc_page_url = sc_page_url + "?locale=" + Locale;
            }
            else if (Locale.Equals("cn") || Locale.Equals("zh"))
            {
                sc_page_url = sc_page_url + "?locale=zh";
            }

            action.Navigate(driver, sc_page_url);
            action.ILoginViaLogInLink(driver, biz_username, biz_password);

            util.RemoveAllModelsInYourCartTable(driver);

            //--- Action: Get Test Data ---//
            string adi_model = util.GetAdiModel();

            action.IAddModelToSample(driver, adi_model);

            //----- R2 > T15: Verify Checkout button -----//

            //--- Action: Click on the Checkout button ----//
            action.IClick(driver, Elements.Shopping_Cart_Checkout_Button);
            Thread.Sleep(4000);

            //--- Expected Result: The Enter Shipping Address page should be displayed ----//
            if (Locale.Equals("cn") || Locale.Equals("zh"))
            {
                test.validateStringInstance(driver, driver.Url, "analog.com/" + shippingInformation_url + "?locale=zh");
            }
            else
            {
                test.validateStringInstance(driver, driver.Url, "analog.com/" + shippingInformation_url + "?locale=" + Locale);
            }
        }

        //----- NOTE: This was commented out because the Checkout Button is Disabled if the User is Not Logged In -----//
        //[Test, Category("Shopping Cart"), Category("NonCore")]
        //[TestCase("en", TestName = "Verify that the Checkout Button is Present and Works as Expected using as Anonymous User in EN Locale")]
        ////[TestCase("cn", TestName = "Verify that the Checkout Button is Present and Works as Expected using as Anonymous User in CN Locale")]
        ////[TestCase("jp", TestName = "Verify that the Checkout Button is Present and Works as Expected using as Anonymous User in JP Locale")]
        ////[TestCase("ru", TestName = "Verify that the Checkout Button is Present and Works as Expected using as Anonymous User in RU Locale")]
        //public void ShoppingCart_VerifyCheckoutButtonUsingAsAnonymousUser(string Locale)
        //{
        //    //--- Shopping Cart URL ---//
        //    string sc_page_url = Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx";
        //    if (Locale.Equals("jp") || Locale.Equals("ru"))
        //    {
        //        sc_page_url = sc_page_url + "?locale=" + Locale;
        //    }
        //    else if (Locale.Equals("cn") || Locale.Equals("zh"))
        //    {
        //        sc_page_url = sc_page_url + "?locale=zh";
        //    }

        //    action.Navigate(driver, sc_page_url);

        //    //--- Select any country on the Select Shipping Country dropdown (e.g. UNITED STATES) ---//
        //    action.IClick(driver, Elements.Shopping_Cart_Country_Dropdown);
        //    action.IClick(driver, By.CssSelector("li[value='US']>a"));

        //    util.RemoveAllModelsInYourCartTable(driver);

        //    //--- Action: Get Test Data ---//
        //    string adi_model = util.GetAdiModel();
        //    action.IAddModelToSample(driver, adi_model);

        //    //----- R4 > T19: Verify Checkout button -----//

        //    //--- Action: Click on the Checkout button ----//
        //    action.IClick(driver, Elements.Shopping_Cart_Checkout_Button);
        //    Thread.Sleep(4000);

        //    //--- Expected Result: The login page should be displayed ----//
        //    test.validateStringInstance(driver, driver.Url, "b2clogin.com");

        //    if (driver.Url.Contains("b2clogin.com"))
        //    {
        //        //--- Action: Login using valid account ----//
        //        action.ILogin(driver, biz_username, biz_password);
        //        Thread.Sleep(1000);

        //        //--- Expected Result: The Shipping Address page should be displayed ----//
        //        if (Locale.Equals("cn") || Locale.Equals("zh"))
        //        {
        //            test.validateStringInstance(driver, driver.Url, "analog.com/" + shippingInformation_url + "?locale=zh");
        //        }
        //        else
        //        {
        //            test.validateStringInstance(driver, driver.Url, "analog.com/" + shippingInformation_url + "?locale=" + Locale);
        //        }
        //    }
        //}
    }
}