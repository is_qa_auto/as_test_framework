﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Threading;
using OpenQA.Selenium;

namespace AS_Test_Framework.ShoppingCart
{
    [TestFixture]
    public class ShoppingCart_AboutYourProjectPage_VerifyFieldsWithRepeatCustomer : BaseSetUp
    {
        public ShoppingCart_AboutYourProjectPage_VerifyFieldsWithRepeatCustomer() : base() { }

        //--- Login Credentials (Repeat Customer) ---//
        string repeatCust_username = "marvin.bebe@analog.com";
        string repeatCust_password = "Test_1234";

        //--- Login Credentials (New Customer) ---//
        string newCust_username = "sc_t3st_new_555@mailinator.com";
        string newCust_password = "Test_1234";

        ////--- URLs ---//
        //string placeOrder_url = "PlaceOrder.aspx";

        [Test, Category("Shopping Cart"), Category("NonCore")]
        [TestCase("en", "Please Select", TestName = "Verify that the Fields are Present and Working as Expected after Adding a Model for Purchase using a Repeat Customer in EN Locale")]
        //[TestCase("cn", "请选择", TestName = "Verify that the Fields are Present and Working as Expected after Adding a Model for Purchase using a Repeat Customer in CN Locale")]
        //[TestCase("jp", "選択してください。", TestName = "Verify that the Fields are Present and Working as Expected after Adding a Model for Purchase using a Repeat Customer in JP Locale")]
        //[TestCase("ru", "Please Select", TestName = "Verify that the Fields are Present and Working as Expected after Adding a Model for Purchase using a Repeat Customer in RU Locale")]
        public void ShoppingCart_VerifyFieldsWithRepeatCustomerAfterAddingAModelForForPurchase(string Locale, string PleaseSelect_Label)
        {
            //--- Shopping Cart URL ---//
            string sc_page_url = Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx";
            if (Locale.Equals("jp") || Locale.Equals("ru"))
            {
                sc_page_url = sc_page_url + "?locale=" + Locale;
            }
            else if (Locale.Equals("cn") || Locale.Equals("zh"))
            {
                sc_page_url = sc_page_url + "?locale=zh";
            }

            action.Navigate(driver, sc_page_url);
            action.ILoginViaLogInLink(driver, repeatCust_username, repeatCust_password);

            util.RemoveAllModelsInYourCartTable(driver);

            //--- Action: Get Test Data ---//
            string adi_model = util.GetAdiModel();

            action.IAddModelToPurchase(driver, adi_model);

            action.IClick(driver, Elements.Shopping_Cart_Checkout_Button);
            Thread.Sleep(3000);

            action.GoToAboutYourProjectPage(driver);

            //----- ECommerce TestPlan > Test Case Title: Verify Occupation upon update in shopping  cart and myanalog (Purchase) (IQ-7750) -----//

            //--- Expected Result: Occupation field is prepopulated with the value same as that in the myanalog ---//
            test.validateStringChanged(driver, util.GetText(driver, Elements.Shopping_Cart_AboutYourProject_Occupation_Dd), PleaseSelect_Label);

            //----- R25 > T2.3: Verify questions for US Country (Purchase) -----//

            //--- Expected Result: The "Which of the following best describes your design phase?" questions is displayed ---//
            test.validateElementIsPresent(driver, Elements.Shopping_Cart_AboutYourProject_DesignPhase_Dd);

            //--- Expected Result: The "What is your best estimate of when the product will go into production?" questions is displayed ---//
            test.validateElementIsPresent(driver, Elements.Shopping_Cart_AboutYourProject_BestEstimate_Dd);

            //--- Expected Result: The "What is your best estimate of how many components you will use on a yearly basis?" questions is displayed ---//
            test.validateElementIsPresent(driver, Elements.Shopping_Cart_AboutYourProject_ComponentQty_Dd);

            //--- Expected Result: The "What is the application / end use of these products?" questions is displayed ---//
            test.validateElementIsPresent(driver, Elements.Shopping_Cart_AboutYourProject_EndUse_Dd);

            //--- Expected Result: The "Will you be re-exporting this product outside of your country without putting it in an application?" questions is displayed ---//
            test.validateElementIsPresent(driver, Elements.Shopping_Cart_AboutYourProject_ReExporting_Rbs);

            Assert.Multiple(() =>
            {
                ////----- R7 > T4: Verify the Application dropdown -----//
                //test.validateElementIsPresent(driver, Elements.Shopping_Cart_AboutYourProject_EndUse_Dd);

                //----- R7 > T2: Verify 'Will you be re-exporting this product outside of your country without putting it in an application?' section -----//

                //--- Action: Select "Yes" on the 'Will you be re-exporting this product outside of your country without putting it in an application?' question ---//
                action.IClick(driver, Elements.Shopping_Cart_AboutYourProject_ReExporting_Yes_Rb);

                //--- Expected Result: The following questions should be displayed: To what country will you be re-exporting your project? ---//
                test.validateElementIsPresent(driver, Elements.Shopping_Cart_AboutYourProject_ReExportingCountry_Txt);

                //--- Expected Result: The Country dropdown list should be displayed. ---//
                test.validateElementIsPresent(driver, Elements.Shopping_Cart_AboutYourProject_ReExportingCountry_Dd);

                //--- Action: Change the value of the 'Will you be re-exporting this product outside of your country without putting it in an application?' question to "No" ---//
                action.IClick(driver, Elements.Shopping_Cart_AboutYourProject_ReExporting_No_Rb);

                //--- Expected Result: The Country dropdown list should not be displayed. ---//
                test.validateElementIsNotPresent(driver, Elements.Shopping_Cart_AboutYourProject_ReExportingCountry_Dd);
            });
        }

        [Test, Category("Shopping Cart"), Category("NonCore")]
        [TestCase("en", "Please Select", TestName = "Verify that the Fields are Present and Working as Expected after Adding a Model for Sample using a Repeat Customer in EN Locale")]
        //[TestCase("cn", "请选择", TestName = "Verify that the Fields are Present and Working as Expected after Adding a Model for Sample using a Repeat Customer in CN Locale")]
        //[TestCase("jp", "選択してください。", TestName = "Verify that the Fields are Present and Working as Expected after Adding a Model for Sample using a Repeat Customer in JP Locale")]
        //[TestCase("ru", "Please Select", TestName = "Verify that the Fields are Present and Working as Expected after Adding a Model for Sample using a Repeat Customer in RU Locale")]
        public void ShoppingCart_VerifyFieldsWithRepeatCustomerAfterAddingAModelForForSample(string Locale, string PleaseSelect_Label)
        {
            if (!Util.Configuration.Environment.Equals("production"))
            {
                //--- Shopping Cart URL ---//
                string sc_page_url = Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx";
                if (Locale.Equals("jp") || Locale.Equals("ru"))
                {
                    sc_page_url = sc_page_url + "?locale=" + Locale;
                }
                else if (Locale.Equals("cn") || Locale.Equals("zh"))
                {
                    sc_page_url = sc_page_url + "?locale=zh";
                }

                action.Navigate(driver, sc_page_url);
                action.ILoginViaLogInLink(driver, repeatCust_username, repeatCust_password);

                util.RemoveAllModelsInYourCartTable(driver);

                //--- Action: Get Test Data ---//
                string adi_model = util.GetAdiModel();

                action.IAddModelToSample(driver, adi_model);

                action.IClick(driver, Elements.Shopping_Cart_Checkout_Button);
                Thread.Sleep(3000);

                action.GoToAboutYourProjectPage(driver);

                //----- ECommerce TestPlan > Test Case Title: Verify Occupation upon update in shopping  cart and myanalog (Sample) (IQ-7750) -----//

                //--- Expected Result: Occupation field is prepopulated with the value same as that in the myanalog ---//
                test.validateStringChanged(driver, util.GetText(driver, Elements.Shopping_Cart_AboutYourProject_Occupation_Dd), PleaseSelect_Label);

                //----- R25 > T2.1: Verify questions for US Country (Sample) -----//

                //--- Expected Result: The "Which of the following best describes your design phase?" questions is displayed ---//
                test.validateElementIsPresent(driver, Elements.Shopping_Cart_AboutYourProject_DesignPhase_Dd);

                //--- Expected Result: The "What is your best estimate of when the product will go into production?" questions is displayed ---//
                test.validateElementIsPresent(driver, Elements.Shopping_Cart_AboutYourProject_BestEstimate_Dd);

                //--- Expected Result: The "What is your best estimate of how many components you will use on a yearly basis?" questions is displayed ---//
                test.validateElementIsPresent(driver, Elements.Shopping_Cart_AboutYourProject_ComponentQty_Dd);

                //--- Expected Result: The "What is the application / end use of these products?" questions is displayed ---//
                test.validateElementIsPresent(driver, Elements.Shopping_Cart_AboutYourProject_EndUse_Dd);

                //--- Expected Result: The "Will you be re-exporting this product outside of your country without putting it in an application?" questions is displayed ---//
                test.validateElementIsPresent(driver, Elements.Shopping_Cart_AboutYourProject_ReExporting_Rbs);

                //--- Action: Select "Yes" on the 'Will you be re-exporting this product outside of your country without putting it in an application?' question ---//
                action.IClick(driver, Elements.Shopping_Cart_AboutYourProject_ReExporting_Yes_Rb);

                Assert.Multiple(() =>
                {
                    //--- Expected Result: The following questions should be displayed: To what country will you be re-exporting your project? ---//
                    test.validateElementIsPresent(driver, Elements.Shopping_Cart_AboutYourProject_ReExportingCountry_Txt);

                    //--- Expected Result: The Country dropdown list should be displayed. ---//
                    test.validateElementIsPresent(driver, Elements.Shopping_Cart_AboutYourProject_ReExportingCountry_Dd);

                    //--- Action: Change the value of the 'Will you be re-exporting this product outside of your country without putting it in an application?' question to "No" ---//
                    action.IClick(driver, Elements.Shopping_Cart_AboutYourProject_ReExporting_No_Rb);

                    //--- Expected Result: The Country dropdown list should not be displayed. ---//
                    test.validateElementIsNotPresent(driver, Elements.Shopping_Cart_AboutYourProject_ReExportingCountry_Dd);
                });
            }
        }

        [Test, Category("Shopping Cart"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that the Input Validations are Working as Expected after Adding a Model for Purchase in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Input Validations are Working as Expected after Adding a Model for Purchase in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Input Validations are Working as Expected after Adding a Model for Purchase in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Input Validations are Working as Expected after Adding a Model for Purchase in RU Locale")]
        public void ShoppingCart_VerifyInputValidationsAfterAddingAModelForForPurchase(string Locale)
        {
            //--- Shopping Cart URL ---//
            string sc_page_url = Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx";
            if (Locale.Equals("jp") || Locale.Equals("ru"))
            {
                sc_page_url = sc_page_url + "?locale=" + Locale;
            }
            else if (Locale.Equals("cn") || Locale.Equals("zh"))
            {
                sc_page_url = sc_page_url + "?locale=zh";
            }

            //--- Action: Access Shopping Cart page ---//
            action.Navigate(driver, sc_page_url);

            //--- Action: Login as registered user ---//
            action.ILoginViaLogInLink(driver, newCust_username, newCust_password);

            util.RemoveAllModelsInYourCartTable(driver);

            //--- Action: Get Test Data ---//
            string adi_model = util.GetAdiModel();

            //--- Action: User add products on the Cart ---//

            //--- Action: Click on the Purchase button ---//
            action.IAddModelToPurchase(driver, adi_model);

            //--- Action: Click on the Checkout button ---//
            action.IClick(driver, Elements.Shopping_Cart_Checkout_Button);
            Thread.Sleep(3000);

            //--- Action: Complete all the mandatory fields on the Shipping Address page and click on the Next button ---//
            //--- Action: Complete all the mandatory fields on the Billing Address page and click on the Next button ---//
            action.GoToAboutYourProjectPage(driver);

            //----- ECommerce TestPlan > Test Case Title: Verify About Your project page with no data -----//

            //--- Action: Click on the Next button without completing the required fields ---//
            action.IClick(driver, By.CssSelector("input[id*='Next']"));

            //--- Expected Result: The error message should be displayed ---//
            //--- Occupation: "This field is required" ---//
            test.validateElementIsPresent(driver, Elements.Shopping_Cart_AboutYourProject_Occupation_ErrMsg);

            //--- Expected Result: The error message should be displayed ---//
            //--- Which of the following best describes your design phase?: "This field is required" ---//
            test.validateElementIsPresent(driver, Elements.Shopping_Cart_AboutYourProject_DesignPhase_ErrMsg);

            //--- Expected Result: The error message should be displayed ---//
            //--- What is your best estimate of when the product will go into production?: "This field is required" ---//
            test.validateElementIsPresent(driver, Elements.Shopping_Cart_AboutYourProject_BestEstimate_ErrMsg);

            //--- Expected Result: The error message should be displayed ---//
            //--- What is your best estimate of how many components you will use on a yearly basis?: "This field is required" ---//
            test.validateElementIsPresent(driver, Elements.Shopping_Cart_AboutYourProject_ComponentQty_ErrMsg);

            //--- Expected Result: The error message should be displayed ---//
            //--- What is the application / end use of these products?: "This field is required" ---//
            test.validateElementIsPresent(driver, Elements.Shopping_Cart_AboutYourProject_EndUse_ErrMsg);

            //--- Expected Result: The error message should be displayed ---//
            //--- Will you be re-exporting this product outside of your country without putting it in an application?: "This field is required" ---//
            test.validateElementIsPresent(driver, Elements.Shopping_Cart_AboutYourProject_ReExporting_ErrMsg);
        }
    }
}