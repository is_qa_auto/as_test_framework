﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;

namespace AS_Test_Framework.ShoppingCart
{
    [TestFixture]
    public class ShoppingCart_ShoppingCartPage_VerifyFindMoreProductsButton : BaseSetUp
    {
        public ShoppingCart_ShoppingCartPage_VerifyFindMoreProductsButton() : base() { }

        //--- Login Credentials ---//
        string biz_username = "marvin.bebe@analog.com";
        string biz_password = "Test_1234";

        [Test, Category("Shopping Cart"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that the Find More Products Button is Present and Redirects to the Products Page using a Business Email in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Find More Products Button is Present and Redirects to the Products Page using a Business Email in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Find More Products Button is Present and Redirects to the Products Page using a Business Email in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Find More Products Button is Present and Redirects to the Products Page using a Business Email in RU Locale")]
        public void ShoppingCart_VerifyFindMoreProductsButtonUsingABusinessEmail(string Locale)
        {
            //--- Shopping Cart URL ---//
            string sc_page_url = Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx";
            if (Locale.Equals("jp") || Locale.Equals("ru"))
            {
                sc_page_url = sc_page_url + "?locale=" + Locale;
            }
            else if (Locale.Equals("cn") || Locale.Equals("zh"))
            {
                sc_page_url = sc_page_url + "?locale=zh";
            }

            action.Navigate(driver, sc_page_url);
            action.ILoginViaLogInLink(driver, biz_username, biz_password);

            Assert.Multiple(() =>
            {
                //----- R2 > T3: Verify the Find More products button -----//
                test.validateElementIsPresent(driver, Elements.Shopping_Cart_FindMoreProducts_Btn);

                action.IClick(driver, Elements.Shopping_Cart_FindMoreProducts_Btn);
                test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + "/products.html");
            });
        }
    }
}