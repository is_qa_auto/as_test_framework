﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Threading;

namespace AS_Test_Framework.ShoppingCart
{
    [TestFixture]
    public class ShoppingCart_PlaceSampleRequestPage_VerifyPreviousAndPlaceOrderButtons : BaseSetUp
    {
        public ShoppingCart_PlaceSampleRequestPage_VerifyPreviousAndPlaceOrderButtons() : base() { }

        //--- Login Credentials ---//
        string username = "marvin.bebe@analog.com";
        string password = "Test_1234";

        //--- URLs ---//
        string projectDetails_url = "AboutYourProject.aspx";
        string placeOrder_url = "PlaceOrder.aspx";

        //--- Labels ---//
        string tnc_errMsg = "Please review and accept \"Terms & Conditions\" by selecting checkbox";

        [Test, Category("Shopping Cart"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that the Previous and Place Order Buttons are Present and Working as Expected after Adding a Model for Sample in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Previous and Place Order Buttons are Present and Working as Expected after Adding a Model for Sample in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Previous and Place Order Buttons are Present and Working as Expected after Adding a Model for Sample in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Previous and Place Order Buttons are Present and Working as Expected after Adding a Model for Sample in RU Locale")]
        public void ShoppingCart_VerifyPreviousAndPlaceOrderButtonsAfterAddingAModelForSample(string Locale)
        {
            //--- Shopping Cart URL ---//
            string sc_page_url = Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx";
            if (Locale.Equals("jp") || Locale.Equals("ru"))
            {
                sc_page_url = sc_page_url + "?locale=" + Locale;
            }
            else if (Locale.Equals("cn") || Locale.Equals("zh"))
            {
                sc_page_url = sc_page_url + "?locale=zh";
            }

            action.Navigate(driver, sc_page_url);
            action.ILoginViaLogInLink(driver, username, password);

            util.RemoveAllModelsInYourCartTable(driver);

            //--- Action: Get Test Data ---//
            string adi_model = util.GetAdiModel();

            action.IAddModelToSample(driver, adi_model);

            action.IClick(driver, Elements.Shopping_Cart_Checkout_Button);
            Thread.Sleep(3000);

            action.GoToAboutYourProjectPage(driver);

            action.IPopulateFieldsInAboutYourProjectPage(driver);

            if (driver.Url.Contains(placeOrder_url))
            {
                //----- R24 > T1: Verify Shopping Delago cloud Development -----//

                //--- Expected Result: No card section displayed ---//
                test.validateElementIsNotPresent(driver, Elements.Shopping_Cart_PlaceOrder_Cc_Sec);

                //----- R10 > T2: Verify the Place Order button on the Place Sample Request page -----//

                //--- Action: Click on the Place Order button without ticking the checkbox for Terms and Conditions ---//
                action.IClick(driver, Elements.Shopping_Cart_Footer_Next_Button);
                Assert.Multiple(() =>
                {
                    //--- Expected Result: The error message should be displayed opposite to the blank field(s) and dropdown(s): Please review and accept "Terms & Conditions" by selecting checkbox ---//
                    if (Locale.Equals("en"))
                    {
                        test.validateStringIsCorrect(driver, Elements.Shopping_Cart_PlaceOrder_tNC_ErrMsg, tnc_errMsg);
                    }
                    else
                    {
                        test.validateElementIsPresent(driver, Elements.Shopping_Cart_PlaceOrder_tNC_ErrMsg);
                    }

                    //--- Action: Tick the checkbox for Terms and Conditions ---//
                    action.ICheck(driver, Elements.AcceptSCTerms);

                    //--- Expected Result: The error message should be removed. ---//
                    test.validateElementIsNotPresent(driver, Elements.Shopping_Cart_PlaceOrder_tNC_ErrMsg);
                });

                //----- R8 > T3: Verify 'Will you be re-exporting this product outside of your country without putting it in an application?' section -----//

                //--- Action: Click on the Previous button ---//
                action.IClick(driver, Elements.Shopping_Cart_Prev_Button);

                //--- Expected Result: The About Your Project page should be displayed. ---//
                if (Locale.Equals("cn") || Locale.Equals("zh"))
                {
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + projectDetails_url + "?locale=zh");
                }
                else
                {
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + projectDetails_url + "?locale=" + Locale);
                }
            }
            else
            {
                //--- Validation if the Page is in the Sample Request Page ---//
                if (Locale.Equals("cn") || Locale.Equals("zh"))
                {
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + placeOrder_url + "?locale=zh");
                }
                else
                {
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + placeOrder_url + "?locale=" + Locale);
                }
            }
        }
    }
}