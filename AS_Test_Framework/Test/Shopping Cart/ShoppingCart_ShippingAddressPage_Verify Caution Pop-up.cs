﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Threading;

namespace AS_Test_Framework.ShoppingCart
{
    [TestFixture]
    public class ShoppingCart_ShippingAddressPage_VerifyCautionPopup : BaseSetUp
    {
        public ShoppingCart_ShippingAddressPage_VerifyCautionPopup() : base() { }

        //--- Login Credentials ---//
        string jp_username = "sc_t3st_jp_555@mailinator.com";
        string jp_password = "Test_1234";

        [Test, Category("Shopping Cart"), Category("NonCore")]
        [TestCase("jp", TestName = "Verify that the Caution Pop-up is Present and Working as Expected after Adding a Model for Sample using an Japanese User in JP Locale")]
        public void ShoppingCart_VerifyCautionPopupsAfterAddingAModelForSample(string Locale)
        {
            if (!Util.Configuration.Environment.Equals("production") && Locale.Equals("jp"))
            {
                //--- Shopping Cart URL ---//
                string sc_page_url = Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx";
                if (Locale.Equals("jp") || Locale.Equals("ru"))
                {
                    sc_page_url = sc_page_url + "?locale=" + Locale;
                }
                else if (Locale.Equals("cn") || Locale.Equals("zh"))
                {
                    sc_page_url = sc_page_url + "?locale=zh";
                }

                //----- JP Tab > R14 > T1: Verify caution pop-up on Shipping form (Sample) (IQ-6129/AL-13166) -----//

                //--- Action: Go to Shopping Cart ---//
                action.Navigate(driver, sc_page_url);

                //--- Action: Login with valid credentials using Japanese account ---//
                action.ILoginViaLogInLink(driver, jp_username, jp_password);

                util.RemoveAllModelsInYourCartTable(driver);

                //--- Action: Get Test Data ---//
                string adi_model = util.GetAdiModel();

                //--- Action: Process Sample ---//
                action.IAddModelToSample(driver, adi_model);

                //--- Action: Check out ---//
                action.IClick(driver, Elements.Shopping_Cart_Checkout_Button);
                Thread.Sleep(7000);

                //--- Expected Result: In the shipping address page you will be getting one pop-up ----//
                test.validateElementIsPresent(driver, Elements.Shopping_Cart_Caution_PopUp);
            }
        }

        //[Test, Category("Shopping Cart"), Category("NonCore")]
        //[TestCase("jp", TestName = "Verify that the Caution Pop-up is Present and Working as Expected after Adding Models for Purchase and Sample using an Japanese User in JP Locale")]
        //public void ShoppingCart_VerifyCautionPopupsAfterAddingModelsForPurchaseAndSample(string Locale)
        //{
        //    if (!Util.Configuration.Environment.Equals("production") && Locale.Equals("jp"))
        //    {
        //        //--- Shopping Cart URL ---//
        //        string sc_page_url = Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx";
        //        if (Locale.Equals("jp") || Locale.Equals("ru"))
        //        {
        //            sc_page_url = sc_page_url + "?locale=" + Locale;
        //        }
        //        else if (Locale.Equals("cn") || Locale.Equals("zh"))
        //        {
        //            sc_page_url = sc_page_url + "?locale=zh";
        //        }

        //        //----- JP Tab > R14 > T1: Verify caution pop-up on Shipping form (Purchase and Sample ) (IQ-6129/AL-13166) -----//

        //        //--- Action: Go to Shopping Cart ---//
        //        action.Navigate(driver, sc_page_url);

        //        //--- Action: Login with valid credentials using Japanese account ---//
        //        action.ILoginViaLogInLink(driver, jp_username, jp_password);

        //        util.RemoveAllModelsInYourCartTable(driver);

        //        //--- Action: Get Test Data ---//
        //        string adi_model = util.GetAdiModel();

        //        //--- Action: Process Purchase ---//
        //        action.IAddModelToPurchase(driver, adi_model);

        //        //--- Action: Process Sample ---//
        //        action.IAddModelToSample(driver, adi_ModelNo2);

        //        //--- Action: Check out ---//
        //        action.IClick(driver, Elements.Shopping_Cart_Checkout_Button);
        //        Thread.Sleep(7000);

        //        //--- Expected Result: In the shipping address page you will be getting one pop-up ----//
        //        test.validateElementIsPresent(driver, Elements.Shopping_Cart_Caution_PopUp);
        //    }
        //}
    }
}