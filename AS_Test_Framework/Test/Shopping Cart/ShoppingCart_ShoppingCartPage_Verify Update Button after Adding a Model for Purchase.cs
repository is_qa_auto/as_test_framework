﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.ShoppingCart
{
    [TestFixture]
    public class ShoppingCart_ShoppingCartPage_VerifyUpdateButtonAfterAddingAModelForPurchase : BaseSetUp
    {
        public ShoppingCart_ShoppingCartPage_VerifyUpdateButtonAfterAddingAModelForPurchase() : base() { }

        //--- Login Credentials ---//
        string biz_username = "aries.sorosoro@analog.com";
        string biz_password = "Test_1234";

        //--- URLs ---//
        string customerServiceSupport_form_url = "Form_Pages/support/customerService.aspx";
        string customerServiceSupport_china_form_url = "Form_Pages/support/customerService_china.aspx";

        //--- Labels ---//
        string invalidQuantity_errMsg = ": Please update the Total Quantity to be a multiple of the packing quantity of";
        string biz_zeroQuantity_line1_errMsg = " does not have pricing setup for quantity entered.";
        string biz_zeroQuantity_line2_errMsg = "Please adjust the quantity or request quote.";
        string anon_zeroQuantity_line1_errMsg = ": quantity must be greater than 0.";
        string anon_zeroQuantity_line2_errMsg = "Please adjust the quantity.";

        [Test, Category("Shopping Cart"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that the Update Button is Present and Working as Expected after Adding an ADI Model for Purchase using a Business Email in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Update Button is Present and Working as Expected after Adding an ADI Model for Purchase using a Business Email in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Update Button is Present and Working as Expected after Adding an ADI Model for Purchase using a Business Email in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Update Button is Present and Working as Expected after Adding an ADI Model for Purchase using a Business Email in RU Locale")]
        public void ShoppingCart_VerifyUpdateButtonAfterAddingAnAdiModelForPurchaseABusinessEmail(string Locale)
        {
            //--- Shopping Cart URL ---//
            string sc_page_url = Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx";
            if (Locale.Equals("jp") || Locale.Equals("ru"))
            {
                sc_page_url = sc_page_url + "?locale=" + Locale;
            }
            else if (Locale.Equals("cn") || Locale.Equals("zh"))
            {
                sc_page_url = sc_page_url + "?locale=zh";
            }

            action.Navigate(driver, sc_page_url);
            action.ILoginViaLogInLink(driver, biz_username, biz_password);

            util.RemoveAllModelsInYourCartTable(driver);

            //--- Action: Get Test Data ---//
            string adi_model = "AD7915BRMZ";

            action.IAddModelToPurchase(driver, adi_model);

            //----- R2 > T7_2: Verify the Update button with a quantity not multiple to the packing quantity -----//

            //--- Action: Get the Multiple Quantity ---//
            string multipleQuantity_value = util.ExtractNumber(driver, Elements.Shopping_Cart_MultipleQuantity_Label);

            //--- Input ---//
            string update_input = "10";

            //--- Action: Update the quantity ---//
            action.IDeleteValueOnFields(driver, Elements.Shopping_Cart_Qty_Tbx);
            action.IType(driver, Elements.Shopping_Cart_Qty_Tbx, update_input);

            //--- Action: Click on the Update button ---//
            action.IClick(driver, Elements.Shopping_Cart_Update_Btn);

            //--- Expected Result: The error message should be displayed ---//
            if (Locale.Equals("en"))
            {
                test.validateStringIsCorrect(driver, Elements.SC_QtyCol_ErrorMessage_Line1_Label, adi_model.ToUpper() + invalidQuantity_errMsg + " " + multipleQuantity_value + ".");
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.SC_QtyCol_ErrorMessage_Line1_Label);
            }

            //----- R2 > T7_1: Verify the Update button with 0 quantity value -----//

            //--- Input ---//
            update_input = "0";

            //--- Action: Update the quantity ---//
            action.IDeleteValueOnFields(driver, Elements.Shopping_Cart_Qty_Tbx);
            action.IType(driver, Elements.Shopping_Cart_Qty_Tbx, update_input);

            //--- Action: Click on the Update button ---//
            action.IClick(driver, Elements.Shopping_Cart_Update_Btn);
            Thread.Sleep(3000);

            //--- Expected Result: The error message should be displayed ---//
            if (Locale.Equals("en"))
            {
                test.validateStringIsCorrect(driver, Elements.SC_QtyCol_ErrorMessage_Line1_Label, adi_model.ToUpper() + biz_zeroQuantity_line1_errMsg);
                test.validateStringIsCorrect(driver, Elements.SC_QtyCol_ErrorMessage_Line2_Label, biz_zeroQuantity_line2_errMsg);
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.SC_QtyCol_ErrorMessage_Line1_Label);
                test.validateElementIsPresent(driver, Elements.SC_QtyCol_ErrorMessage_Line2_Label);
            }

            //--- Action: Click on the request quote hyperlink ---//
            action.IClick(driver, Elements.Shopping_Cart_ReqQte_Link);

            //--- Expected Result: The Customer Service Support form should be displayed in a new tab/window. ---//
            if (driver.Url.Contains(sc_page_url))
            {
                driver.SwitchTo().Window(driver.WindowHandles.Last());

                if (Locale.Equals("cn") || Locale.Equals("zh"))
                {
                    test.validateStringInstance(driver, driver.Url, customerServiceSupport_china_form_url);
                }
                else
                {
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + customerServiceSupport_form_url);
                }

                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles.First());
            }
            else if (!driver.Url.Contains(sc_page_url))
            {
                if (Locale.Equals("cn") || Locale.Equals("zh"))
                {
                    test.validateStringInstance(driver, driver.Url, customerServiceSupport_china_form_url);
                }
                else
                {
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + customerServiceSupport_form_url);
                }

                driver.Navigate().GoToUrl(sc_page_url);
            }

            //----- R2 > T7_4: Verify the Update button with a valid quantity -----//
            if (Locale.Equals("en"))
            {
                string subtotal_value = util.GetText(driver, Elements.Shopping_Cart_SubTot_Text);

                //--- Input ---//
                update_input = "50";

                //--- Action: Update the quantity ---//
                action.IDeleteValueOnFields(driver, Elements.Shopping_Cart_Qty_Tbx);
                action.IType(driver, Elements.Shopping_Cart_Qty_Tbx, update_input);

                //--- Action: Click on the Update button ---//
                action.IClick(driver, Elements.Shopping_Cart_Update_Btn);
                Thread.Sleep(4000);

                //--- Expected Result: The quantity should be updated ---//
                test.validateString(driver, util.ReturnAttribute(driver, Elements.Shopping_Cart_Qty_Tbx, "value"), update_input);

                //--- Expected Result: The amount should be updated ---//
                test.validateStringChanged(driver, util.GetText(driver, Elements.Shopping_Cart_SubTot_Text), subtotal_value);
            }
        }

        [Test, Category("Shopping Cart"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that the Update Button is Present and Working as Expected after Adding a an ADI Model for Purchase using as Anonymous User in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Update Button is Present and Working as Expected after Adding an ADI Model for Purchase using as Anonymous User in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Update Button is Present and Working as Expected after Adding an ADI Model for Purchase using as Anonymous User in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Update Button is Present and Working as Expected after Adding an ADI Model for Purchase using as Anonymous User in RU Locale")]
        public void ShoppingCart_VerifyUpdateButtonAfterAddingAnAdiModelForPurchaseAsAnonymousUser(string Locale)
        {
            //--- Shopping Cart URL ---//
            string sc_page_url = Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx";
            if (Locale.Equals("jp") || Locale.Equals("ru"))
            {
                sc_page_url = sc_page_url + "?locale=" + Locale;
            }
            else if (Locale.Equals("cn") || Locale.Equals("zh"))
            {
                sc_page_url = sc_page_url + "?locale=zh";
            }

            action.Navigate(driver, sc_page_url);

            //--- Select any country on the Select Shipping Country dropdown (e.g. UNITED STATES) ---//
            action.IClick(driver, Elements.Shopping_Cart_Country_Dropdown);
            action.IClick(driver, By.CssSelector("li[value='US']>a"));

            util.RemoveAllModelsInYourCartTable(driver);

            //--- Action: Get Test Data ---//
            string adi_model = "AD7915BRMZ";

            action.IAddModelToPurchase(driver, adi_model);

            //----- R4 > T6_2: Verify the Update button with a quantity not multiple to the packing quantity -----//

            //--- Action: Get the Default Quantity Value ---//
            string default_quantity_value = util.GetInputBoxValue(driver, Elements.Shopping_Cart_Qty_Tbx);

            //--- Action: Get the Multiple Quantity ---//
            string multipleQuantity_value = util.ExtractNumber(driver, Elements.Shopping_Cart_MultipleQuantity_Label);

            //--- Input ---//
            string update_input = "10";

            //--- Action: Update the quantity ---//
            action.IDeleteValueOnFields(driver, Elements.Shopping_Cart_Qty_Tbx);
            action.IType(driver, Elements.Shopping_Cart_Qty_Tbx, update_input);

            //--- Action: Click on the Update button ---//
            action.IClick(driver, Elements.Shopping_Cart_Update_Btn);

            //--- Expected Result: The error message should be displayed ---//
            if (Locale.Equals("en"))
            {
                test.validateStringIsCorrect(driver, Elements.SC_QtyCol_ErrorMessage_Line1_Label, adi_model.ToUpper() + invalidQuantity_errMsg + " " + multipleQuantity_value + ".");
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.SC_QtyCol_ErrorMessage_Line1_Label);
            }

            //----- R4 > T6_1: Verify the Update button with 0 quantity value (IQ-4738/AL-12277) -----//

            //--- Input ---//
            update_input = "0";

            //--- Action: Update the quantity ---//
            action.IDeleteValueOnFields(driver, Elements.Shopping_Cart_Qty_Tbx);
            action.IType(driver, Elements.Shopping_Cart_Qty_Tbx, update_input);

            //--- Action: Click on the Update button ---//
            action.IClick(driver, Elements.Shopping_Cart_Update_Btn);
            Thread.Sleep(3000);

            //--- Expected Result: The error message should be displayed ---//
            if (Locale.Equals("en"))
            {
                test.validateStringIsCorrect(driver, Elements.SC_QtyCol_ErrorMessage_Line1_Label, adi_model.ToUpper() + anon_zeroQuantity_line1_errMsg);
                test.validateStringIsCorrect(driver, Elements.SC_QtyCol_ErrorMessage_Line2_Label, anon_zeroQuantity_line2_errMsg);
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.SC_QtyCol_ErrorMessage_Line1_Label);
                test.validateElementIsPresent(driver, Elements.SC_QtyCol_ErrorMessage_Line2_Label);
            }

            //----- R2 > T6_4: Verify the Update button with a valid quantity -----//

            string subtotal_value = util.GetText(driver, Elements.Shopping_Cart_SubTot_Text);

            //--- Input ---//
            update_input = (Int32.Parse(default_quantity_value) * 2).ToString();

            //--- Action: Update the quantity ---//
            action.IDeleteValueOnFields(driver, Elements.Shopping_Cart_Qty_Tbx);
            action.IType(driver, Elements.Shopping_Cart_Qty_Tbx, update_input);

            //--- Action: Click on the Update button ---//
            action.IClick(driver, Elements.Shopping_Cart_Update_Btn);
            Thread.Sleep(4000);

            //--- Expected Result: The quantity should be updated ---//
            test.validateString(driver, util.ReturnAttribute(driver, Elements.Shopping_Cart_Qty_Tbx, "value"), update_input);

            //--- Expected Result: The amount should be updated ---//
            test.validateStringChanged(driver, util.GetText(driver, Elements.Shopping_Cart_SubTot_Text), subtotal_value);
        }

        [Test, Category("Shopping Cart"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that the Update Button is Present and Working as Expected after Adding an HMC Model for Purchase using a Business Email in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Update Button is Present and Working as Expected after Adding an HMC Model for Purchase using a Business Email in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Update Button is Present and Working as Expected after Adding an HMC Model for Purchase using a Business Email in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Update Button is Present and Working as Expected after Adding an HMC Model for Purchase using a Business Email in RU Locale")]
        public void ShoppingCart_VerifyUpdateButtonAfterAddingAnHmcModelForPurchaseABusinessEmail(string Locale)
        {
            //--- Shopping Cart URL ---//
            string sc_page_url = Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx";
            if (Locale.Equals("jp") || Locale.Equals("ru"))
            {
                sc_page_url = sc_page_url + "?locale=" + Locale;
            }
            else if (Locale.Equals("cn") || Locale.Equals("zh"))
            {
                sc_page_url = sc_page_url + "?locale=zh";
            }

            action.Navigate(driver, sc_page_url);
            action.ILoginViaLogInLink(driver, biz_username, biz_password);

            util.RemoveAllModelsInYourCartTable(driver);

            //--- Action: Get Test Data ---//
            string hmc_model = "HMC1106";

            //--- Action: Enter 'HMC1106' in the model # textbox ---//
            //--- Action: Tick the Purchase radio button ---//
            //--- Action: Click the Add to Cart button ---//
            action.IAddModelToPurchase(driver, hmc_model);

            //----- ECommerce TestPlan > Test Case Title: Verify removed limit for HMC parts (IQ-9637/AL-16784)-----//

            //--- Action: Update Quantity of the model to 5000 ---//
            string update_input = "5000";
            action.IDeleteValueOnFields(driver, Elements.Shopping_Cart_Qty_Tbx);
            action.IType(driver, Elements.Shopping_Cart_Qty_Tbx, update_input);

            //--- Action: Click the Update button ---//
            action.IClick(driver, Elements.Shopping_Cart_Update_Btn);

            //--- Expected Result: The System displays an Error message '<Model#>: quantity of <Quantity> exceeds maximum quantity of 4999. Please adjust the quantity.' below the Quantity textbox ---//
            if (Locale.Equals("en"))
            {
                test.validateStringIsCorrect(driver, Elements.SC_QtyCol_ErrorMessage_Line1_Label, hmc_model.ToUpper() + ": quantity of " + update_input + " exceeds maximum quantity of 4999.");
                test.validateStringIsCorrect(driver, Elements.SC_QtyCol_ErrorMessage_Line2_Label, anon_zeroQuantity_line2_errMsg);
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.SC_QtyCol_ErrorMessage_Line1_Label);
                test.validateElementIsPresent(driver, Elements.SC_QtyCol_ErrorMessage_Line2_Label);
            }
        }
    }
}