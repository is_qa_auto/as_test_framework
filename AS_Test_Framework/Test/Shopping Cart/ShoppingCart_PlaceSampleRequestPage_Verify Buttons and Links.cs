﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Threading;
using System.Linq;

namespace AS_Test_Framework.ShoppingCart
{
    [TestFixture]
    public class ShoppingCart_PlaceSampleRequestPage_VerifyButtonsAndLinks : BaseSetUp
    {
        public ShoppingCart_PlaceSampleRequestPage_VerifyButtonsAndLinks() : base() { }

        //--- Login Credentials ---//
        string biz_username = "marvin.bebe@analog.com";
        string biz_password = "Test_1234";

        [Test, Category("Shopping Cart"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that the Buttons and Links are Present and Working as Expected after Adding a Model for Sample in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Buttons and Links are Present and Working as Expected after Adding a Model for Sample in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Buttons and Links are Present and Working as Expected after Adding a Model for Sample in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Buttons and Links are Present and Working as Expected after Adding a Model for Sample in RU Locale")]
        public void ShoppingCart_VerifyButtonsAndLinksAfterAddingAModelForSample(string Locale)
        {
            //--- Shopping Cart URL ---//
            string sc_page_url = Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx";
            if (Locale.Equals("jp") || Locale.Equals("ru"))
            {
                sc_page_url = sc_page_url + "?locale=" + Locale;
            }
            else if (Locale.Equals("cn") || Locale.Equals("zh"))
            {
                sc_page_url = sc_page_url + "?locale=zh";
            }

            action.Navigate(driver, sc_page_url);
            action.ILoginViaLogInLink(driver, biz_username, biz_password);

            util.RemoveAllModelsInYourCartTable(driver);

            //--- Action: Get Test Data ---//
            string adi_model = util.GetAdiModel();

            action.IAddModelToSample(driver, adi_model);

            action.IClick(driver, Elements.Shopping_Cart_Checkout_Button);
            Thread.Sleep(3000);

            action.GoToAboutYourProjectPage(driver);

            action.IPopulateFieldsInAboutYourProjectPage(driver);

            //----- R10 > T1: Verify the Place Sample Request page -----//

            Assert.Multiple(() =>
            {
                //--- Expected Result: The Product No. Links must be Present ---//
                test.validateElementIsPresent(driver, Elements.Shopping_Cart_PlaceOrder_ProductNo_Link);

                if (util.CheckElement(driver, Elements.Shopping_Cart_PlaceOrder_ProductNo_Link, 2))
                {
                    //--- Action: Click on the product number hyperlink ---//
                    action.IOpenLinkInNewTab(driver, Elements.Shopping_Cart_PlaceOrder_ProductNo_Link);
                    Thread.Sleep(6000);

                    //--- Expected Result: The product page should be displayed ---//
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + "/products/");

                    driver.Close();
                    driver.SwitchTo().Window(driver.WindowHandles.First());
                }
            });

            Assert.Multiple(() =>
            {
                //--- Expected Result: The Edit Cart button must be Present ---//
                test.validateElementIsPresent(driver, Elements.Shopping_Cart_PlaceOrder_EditCart_Btn);

                if (util.CheckElement(driver, Elements.Shopping_Cart_PlaceOrder_EditCart_Btn, 2))
                {
                    //--- Action: Click on the Edit Cart button ---//
                    action.IClick(driver, Elements.Shopping_Cart_PlaceOrder_EditCart_Btn);

                    //--- Expected Result: The Shopping cart page should be displayed. ---//
                    test.validateStringInstance(driver, driver.Url, sc_page_url);
                }
            });
        }
    }
}