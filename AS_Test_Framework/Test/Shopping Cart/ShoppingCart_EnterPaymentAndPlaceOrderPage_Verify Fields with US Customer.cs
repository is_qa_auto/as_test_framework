﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Threading;
using OpenQA.Selenium;

namespace AS_Test_Framework.ShoppingCart
{
    [TestFixture]
    public class ShoppingCart_EnterPaymentAndPlaceOrderPage_VerifyFieldsWithUsCustomer : BaseSetUp
    {
        public ShoppingCart_EnterPaymentAndPlaceOrderPage_VerifyFieldsWithUsCustomer() : base() { }

        //--- Login Credentials ---//
        string us_username = "marvin.bebe@analog.com";
        string us_password = "Test_1234";

        [Test, Category("Shopping Cart"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that the Fields are Present and Working as Expected after Adding a Model for Purchase using a US Customer in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Fields are Present and Working as Expected after Adding a Model for Purchase using a US Customer in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Fields are Present and Working as Expected after Adding a Model for Purchase using a US Customer in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Fields are Present and Working as Expected after Adding a Model for Purchase using a US Customer in RU Locale")]
        public void ShoppingCart_VerifyFieldsWithUsCustomerAfterAddingAModelForPurchase(string Locale)
        {
            //--- Shopping Cart URL ---//
            string sc_page_url = Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx";
            if (Locale.Equals("jp") || Locale.Equals("ru"))
            {
                sc_page_url = sc_page_url + "?locale=" + Locale;
            }
            else if (Locale.Equals("cn") || Locale.Equals("zh"))
            {
                sc_page_url = sc_page_url + "?locale=zh";
            }

            action.Navigate(driver, sc_page_url);
            action.ILoginViaLogInLink(driver, us_username, us_password);

            util.RemoveAllModelsInYourCartTable(driver);

            //--- Action: Get Test Data ---//
            string adi_model = util.GetAdiModel();

            action.IAddModelToPurchase(driver, adi_model);

            action.IClick(driver, Elements.Shopping_Cart_Checkout_Button);
            Thread.Sleep(3000);

            action.GoToAboutYourProjectPage(driver);

            action.IPopulateFieldsInAboutYourProjectPage(driver);

            //----- R9 > T3_1: Verify Shipping Delivery method for US accounts -----//

            //--- Expected Result: Multiple options should be listed on the Shipping Delivery method dropdown ---//
            test.validateCountIsGreaterOrEqual(driver, util.GetCount(driver, By.CssSelector("select[id='ddlShippingMethod']>option")), 3);

            //----- R9 > T1_1: Verify the Enter Payment and Place Order page -----//

            //--- Expected Result: The following sections should be displayed: ---//

            //--- Delivery Method ---//
            test.validateElementIsPresent(driver, Elements.Shopping_Cart_PlaceOrder_ShippingMethod_Dd);

            //--- Offer Code ---//
            test.validateElementIsPresent(driver, Elements.Shopping_Cart_PlaceOrder_OfferCode_Tbx);

            //--- Your Shopping Cart ---//
            test.validateElementIsPresent(driver, Elements.Shopping_Cart_PlaceOrder_YourShoppingCart_Tbl);

            //--- Payment ---//
            test.validateElementIsPresent(driver, Elements.Shopping_Cart_PlaceOrder_Payment_Sec);
        }
    }
}