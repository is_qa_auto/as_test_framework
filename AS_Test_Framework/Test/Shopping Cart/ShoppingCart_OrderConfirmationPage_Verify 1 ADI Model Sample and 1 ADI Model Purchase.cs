﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.ShoppingCart
{
    [TestFixture]
    public class ShoppingCart_OrderConfirmation_VerifyOneAdiModelSample_AndOneAdiModelPurchase : BaseSetUp
    {
        public ShoppingCart_OrderConfirmation_VerifyOneAdiModelSample_AndOneAdiModelPurchase() : base() { }

        //--- Login Credentials ---//
        string username = "myanalog_t3st_999@mailinator.com";
        string password = "Test_1234";

        //--- URLs ---//
        string orderConfirmation_url = "OrderConfirmation.aspx";

        //--- Labels ---//
        string sampleAndpurchase_orderConfirmation_emailSubj = "Analog Devices: We have received your sample and purchase request";
        string[] orderConfirmation_msg = { "Your order has been submitted. Free samples and purchased items are processed and shipped separately.",
                                        "Sampled items - Order reference number:",
                                        "Your order has been submitted. Free samples and purchased items are processed and shipped separately.",
                                        "Purchased items - Order reference number:",
                                        "When your order has been scheduled, you will receive an order acknowledgement email for purchased items." };

        [Test, Category("Shopping Cart"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that the Order Confirmation Page is Present and Working as Expected after Placing 1 ADI Model for Sample and 1 ADI Model for Purchase in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Order Confirmation Page is Present and Working as Expected after Placing 1 ADI Model for Sample and 1 ADI Model for Purchase in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Order Confirmation Page is Present and Working as Expected after Placing 1 ADI Model for Sample and 1 ADI Model for Purchase in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Order Confirmation Page is Present and Working as Expected after Placing 1 ADI Model for Sample and 1 ADI Model for Purchase in RU Locale")]
        public void ShoppingCart_VerifyOneAdiModelSample_AndOneAdiModelPurchase(string Locale)
        {
            if (!Util.Configuration.Environment.Equals("production") && Locale.Equals("en"))
            {
                //--- Shopping Cart URL ---//
                string sc_page_url = Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx";
                if (Locale.Equals("jp") || Locale.Equals("ru"))
                {
                    sc_page_url = sc_page_url + "?locale=" + Locale;
                }
                else if (Locale.Equals("cn") || Locale.Equals("zh"))
                {
                    sc_page_url = sc_page_url + "?locale=zh";
                }

                action.Navigate(driver, sc_page_url);

                action.ILoginViaLogInLink(driver, username, password);

                util.RemoveAllModelsInYourCartTable(driver);

                //----- R11 > T1_3: Place a sample and Purchase products in a single order -----//

                //--- Action: Get Test Data ---//
                string adi_model = util.GetAdiModel();

                //--- Action: User add product for sample on the Cart ---//
                action.IAddModelToSample(driver, adi_model);

                //--- Action: User add product for purchase on the Cart ---//
                action.IAddModelToPurchase(driver, adi_model);

                //--- Action: Repeat Requirement 12: T1_2 - Step 2 to 11 ---//
                action.IClick(driver, Elements.Shopping_Cart_Checkout_Button);
                Thread.Sleep(3000);

                action.IPopulateFieldsInShippingAddressPage(driver);

                action.IPopulateFieldsInBillingAddressPage(driver);

                action.IPopulateFieldsInAboutYourProjectPage(driver);

                action.IPopulateFieldsInEnterPaymentAndPlaceOrderPage(driver);

                if (driver.Url.Contains("analog.com/" + orderConfirmation_url + "?locale="))
                {
                    if (username.Contains("@mailinator.com"))
                    {
                        //--- Action: Validate the email from buyonline.customerservice@analog.com is sent to the email address of the user ---//
                        action.IOpenNewTab(driver);
                        action.ICheckMailinatorEmail(driver, username);

                        //--- Expected Result: The email from buyonline.customerservice@analog.com with title 'Analog Devices: We have received your sample and purchase request' should be displayed properly ---//
                        test.validateStringInstance(driver, util.GetText(driver, Elements.Mailinator_LatestEmail_Subj), sampleAndpurchase_orderConfirmation_emailSubj);

                        driver.Close();
                        driver.SwitchTo().Window(driver.WindowHandles.First());
                    }

                    //----- R21 > T6: Sample ADI Model and Purchase ADI Model -----//

                    //--- Expected Result: On Sample Request Confirmation page, the message should be correct ---//
                    if (Locale.Equals("en"))
                    {
                        for (int paragraph_count = 1, msg_count = 0; paragraph_count <= 3; paragraph_count++, msg_count++)
                        {
                            if (paragraph_count == 1)
                            {
                                //--- Order Confirmation Message - Line #1 ---//                            
                                test.validateStringInstance(driver, util.GetText(driver, By.CssSelector("div[id='divMixedOrder']>p:nth-child(" + paragraph_count + ")")), orderConfirmation_msg[msg_count]);
                            }

                            //--- Sampled Items Message ---//
                            else if (paragraph_count == 2)
                            {
                                //--- Order Confirmation Message - Line #2 ---//

                                int sampledItems_lineNo = 1;
                                var sampledItems_msg = util.GetText(driver, By.CssSelector("div[id='divMixedOrder']>p:nth-child(" + paragraph_count + ")")).Split("\r\n".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

                                foreach (var sampledItems_line in sampledItems_msg)
                                {
                                    if (sampledItems_lineNo == 1)
                                    {
                                        test.validateStringInstance(driver, sampledItems_line, orderConfirmation_msg[msg_count] /*+ util.GetText(driver, Elements.Shopping_Cart_OrderConfirmation_Sample_OrderRefNo)*/);
                                    }
                                    else if (sampledItems_lineNo == 2)
                                    {
                                        test.validateStringInstance(driver, sampledItems_line, orderConfirmation_msg[msg_count + 1]);
                                    }

                                    sampledItems_lineNo++;
                                }
                            }

                            //--- Purchased Items Message ---//
                            else if (paragraph_count == 3)
                            {
                                //--- Order Confirmation Message - Line #3 ---//

                                int purchasedItems_lineNo = 1;
                                var purchasedItems_msg = util.GetText(driver, By.CssSelector("div[id='divMixedOrder']>p:nth-child(" + paragraph_count + ")")).Split("\r\n".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

                                foreach (var purchasedItems_line in purchasedItems_msg)
                                {
                                    if (purchasedItems_lineNo == 1)
                                    {
                                        test.validateStringInstance(driver, purchasedItems_line, orderConfirmation_msg[msg_count + 1] /*+ util.GetText(driver, Elements.Shopping_Cart_OrderConfirmation_Purchase_OrderRefNo)*/);
                                    }
                                    else if (purchasedItems_lineNo == 2)
                                    {
                                        test.validateStringInstance(driver, purchasedItems_line, orderConfirmation_msg[msg_count + 2]);
                                    }

                                    purchasedItems_lineNo++;
                                }
                            }
                        }
                    }
                    else
                    {
                        test.validateElementIsPresent(driver, Elements.Shopping_Cart_OrderConfirmation_Mixed_Msg);
                        test.validateElementIsPresent(driver, Elements.Shopping_Cart_OrderConfirmation_Mixed_SampledItems_Msg);
                        test.validateElementIsPresent(driver, Elements.Shopping_Cart_OrderConfirmation_Mixed_PurchasedItems_Msg);
                    }

                    //--- Expected Result: The Order Confirmation page should be displayed with 1 order for Sample and 1 order for Purchase ---//
                    test.validateCountIsEqual(driver, 1, util.GetCount(driver, Elements.Shopping_Cart_OrderConfirmation_Sample_OrderRefNo));
                    test.validateCountIsEqual(driver, 1, util.GetCount(driver, Elements.Shopping_Cart_OrderConfirmation_Purchase_OrderRefNo));

                    //--- Cancel Orders in myAnalog ---//
                    action.ICancelOrdersInMyAnalog(driver, username, password);
                }
                else
                {
                    if (Locale.Equals("cn") || Locale.Equals("zh"))
                    {
                        test.validateStringInstance(driver, driver.Url, "analog.com/" + orderConfirmation_url + "?locale=zh");
                    }
                    else
                    {
                        test.validateStringInstance(driver, driver.Url, "analog.com/" + orderConfirmation_url + "?locale=" + Locale);
                    }
                }
            }
        }
    }
}