﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Threading;
using OpenQA.Selenium;

namespace AS_Test_Framework.ShoppingCart
{
    [TestFixture]
    public class ShoppingCart_EnterPaymentAndPlaceOrderPage_VerifyPreviousAndPlaceOrderButtons : BaseSetUp
    {
        public ShoppingCart_EnterPaymentAndPlaceOrderPage_VerifyPreviousAndPlaceOrderButtons() : base() { }

        //--- Login Credentials ---//
        string username = "marvin.bebe@analog.com";
        string password = "Test_1234";

        //--- URLs ---//
        string projectDetails_url = "AboutYourProject.aspx";
        string placeOrder_url = "PlaceOrder.aspx";

        [Test, Category("Shopping Cart"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that the Previous and Place Order Buttons are Present and Working as Expected after Adding a Model for Purchase in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Previous and Place Order Buttons are Present and Working as Expected after Adding a Model for Purchase in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Previous and Place Order Buttons are Present and Working as Expected after Adding a Model for Purchase in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Previous and Place Order Buttons are Present and Working as Expected after Adding a Model for Purchase in RU Locale")]
        public void ShoppingCart_VerifyPreviousAndPlaceOrderButtonsAfterAddingAModelForPurchase(string Locale)
        {
            //--- Shopping Cart URL ---//
            string sc_page_url = Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx";
            if (Locale.Equals("jp") || Locale.Equals("ru"))
            {
                sc_page_url = sc_page_url + "?locale=" + Locale;
            }
            else if (Locale.Equals("cn") || Locale.Equals("zh"))
            {
                sc_page_url = sc_page_url + "?locale=zh";
            }

            action.Navigate(driver, sc_page_url);
            action.ILoginViaLogInLink(driver, username, password);

            util.RemoveAllModelsInYourCartTable(driver);

            //--- Action: Get Test Data ---//
            string adi_model = util.GetAdiModel();

            action.IAddModelToPurchase(driver, adi_model);

            action.IClick(driver, Elements.Shopping_Cart_Checkout_Button);
            Thread.Sleep(4000);

            action.GoToAboutYourProjectPage(driver);

            action.IPopulateFieldsInAboutYourProjectPage(driver);

            if (driver.Url.Contains(placeOrder_url))
            {
                //----- ECommerce TestPlan > Test Case Title: Verify the Enter Payment and Place Order page with incomplete data -----//

                //--- Action: Click on the Place Order button without completing the required fields ---//
                action.IClick(driver, By.CssSelector("input[id*='Next']"));

                Assert.Multiple(() =>
                {
                    //----- ECommerce TestPlan > Test Case Title: Verify Shopping Delago cloud Development -----//

                    //--- Expected Result: Validation error for delivery method dropdown was displayed ---//
                    if (Locale.Equals("en"))
                    {
                        test.validateStringIsCorrect(driver, Elements.Shopping_Cart_PlaceOrder_ShippingMethod_ErrMsg, "This field is required");
                    }
                    else
                    {
                        test.validateElementIsPresent(driver, Elements.Shopping_Cart_PlaceOrder_ShippingMethod_ErrMsg);
                    }

                    //----- ECommerce TestPlan > Test Case Title: Verify the Enter Payment and Place Order page with incomplete data -----//

                    //--- Expected Result: The error message below should be displayed opposite to the blank field(s) and dropdown(s): ---//
                    //--- Payment Information: Payment information is required ---//
                    if (Locale.Equals("en"))
                    {
                        test.validateStringIsCorrect(driver, Elements.Shopping_Cart_PlaceOrder_PaymentDetails_ErrMsg, "Payment information is required");
                    }
                    else
                    {
                        test.validateElementIsPresent(driver, Elements.Shopping_Cart_PlaceOrder_PaymentDetails_ErrMsg);
                    }

                    //----- ECommerce TestPlan > Test Case Title: Verify Shopping Delago cloud Development -----//

                    //--- Expected Result: The error message should be displayed opposite to the blank field(s) and dropdown(s): Please review and accept "Terms & Conditions" by selecting checkbox ---//
                    if (Locale.Equals("en"))
                    {
                        test.validateStringIsCorrect(driver, Elements.Shopping_Cart_PlaceOrder_tNC_ErrMsg, "Please review and accept \"Terms & Conditions\" by selecting checkbox");
                    }
                    else
                    {
                        test.validateElementIsPresent(driver, Elements.Shopping_Cart_PlaceOrder_tNC_ErrMsg);
                    }
                });

                //----- ECommerce TestPlan > Test Case Title: Verify the Previous button on the Enter Payment and Place Order page -----//

                //--- Action: Click on the Previous button ---//
                action.IClick(driver, Elements.Shopping_Cart_Prev_Button);

                //--- Expected Result: The page should be redirected to the About Your Project page ---//
                if (Locale.Equals("cn") || Locale.Equals("zh"))
                {
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + projectDetails_url + "?locale=zh");
                }
                else
                {
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + projectDetails_url + "?locale=" + Locale);
                }
            }
            else
            {
                //--- Validation if the Page is in the Enter Payment and Place Order Page ---//
                if (Locale.Equals("cn") || Locale.Equals("zh"))
                {
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + placeOrder_url + "?locale=zh");
                }
                else
                {
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + placeOrder_url + "?locale=" + Locale);
                }
            }
        }
    }
}