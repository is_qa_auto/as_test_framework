﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.ShoppingCart
{
    [TestFixture]
    public class ShoppingCart_ShoppingCartPage_VerifyViewShippingOptionsAndEstimatedShippingCostsLink : BaseSetUp
    {
        public ShoppingCart_ShoppingCartPage_VerifyViewShippingOptionsAndEstimatedShippingCostsLink() : base() { }

        //--- Login Credentials ---//
        string biz_username = "marvin.bebe@analog.com";
        string biz_password = "Test_1234";

        //--- URLs ---//
        string viewShippingRatesAndOptionsForBuyingOnline_page_url = "/support/customer-service-resources/customer-service/view-shipping-options-rates.html";

        [Test, Category("Shopping Cart"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that the View Shipping Options and Estimated Shipping Costs Link is Present and Working as Expected using a Business Email in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the View Shipping Options and Estimated Shipping Costs Link is Present and Working as Expected using a Business Email in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the View Shipping Options and Estimated Shipping Costs Link is Present and Working as Expected using a Business Email in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the View Shipping Options and Estimated Shipping Costs Link is Present and Working as Expected using a Business Email in RU Locale")]
        public void ShoppingCart_VerifyViewShippingOptionsAndEstimatedShippingCostsLinkUsingABusinessEmail(string Locale)
        {
            //--- Shopping Cart URL ---//
            string sc_page_url = Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx";
            if (Locale.Equals("jp") || Locale.Equals("ru"))
            {
                sc_page_url = sc_page_url + "?locale=" + Locale;
            }
            else if (Locale.Equals("cn") || Locale.Equals("zh"))
            {
                sc_page_url = sc_page_url + "?locale=zh";
            }

            action.Navigate(driver, sc_page_url);
            action.ILoginViaLogInLink(driver, biz_username, biz_password);

            util.RemoveAllModelsInYourCartTable(driver);

            //----- R2 > T11: Verify the View Shipping Options and Estimated Shipping Costs link -----//

            //--- Action: Get Test Data ---//
            string adi_model = util.GetAdiModel();

            action.IAddModelToPurchase(driver, adi_model);

            Assert.Multiple(() =>
            {
                //--- Expected Result: The View Shipping Options and Estimated Shipping Costs link should be displayed ---//
                test.validateElementIsPresent(driver, Elements.Shopping_Cart_VShippingOptsAndEstShippingC_Link);

                //--- Action: Click on the View Shipping Options and Estimated Shipping Costs link ---//
                action.IClick(driver, Elements.Shopping_Cart_VShippingOptsAndEstShippingC_Link);

                //--- Expected Result: The View Shipping Rates & Options for Buying Online page should be displayed in new tab/window ---//
                if (driver.Url.Contains(sc_page_url))
                {
                    driver.SwitchTo().Window(driver.WindowHandles.Last());
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + viewShippingRatesAndOptionsForBuyingOnline_page_url);
                    driver.Close();
                    driver.SwitchTo().Window(driver.WindowHandles.First());
                }
                else if (!driver.Url.Contains(sc_page_url))
                {
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + viewShippingRatesAndOptionsForBuyingOnline_page_url);
                    driver.Navigate().GoToUrl(sc_page_url);
                }

                action.IAddModelToSample(driver, adi_model);

                //--- Expected Result: The View Shipping Options and Estimated Shipping Costs link should be still displayed ---//
                test.validateElementIsPresent(driver, Elements.Shopping_Cart_VShippingOptsAndEstShippingC_Link);

                //--- Action: Click on the View Shipping Options and Estimated Shipping Costs link ---//
                action.IClick(driver, Elements.Shopping_Cart_VShippingOptsAndEstShippingC_Link);

                //--- Expected Result: The View Shipping Rates & Options for Buying Online page should be displayed in new tab/window ---//
                if (driver.Url.Contains(sc_page_url))
                {
                    driver.SwitchTo().Window(driver.WindowHandles.Last());
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + viewShippingRatesAndOptionsForBuyingOnline_page_url);
                    driver.Close();
                    driver.SwitchTo().Window(driver.WindowHandles.First());
                }
                else if (!driver.Url.Contains(sc_page_url))
                {
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + viewShippingRatesAndOptionsForBuyingOnline_page_url);
                    driver.Navigate().GoToUrl(sc_page_url);
                }

                //--- Action: Remove all purchased Items from the cart and keep only samples in the cart ---//
                util.RemoveAllModelsInYourCartTable(driver);

                //--- Expected Result: The 'View Shipping Options and Estimated Shipping Costs' link should not be displayed. ---//
                test.validateElementIsNotPresent(driver, Elements.Shopping_Cart_VShippingOptsAndEstShippingC_Link);
            });
        }
    }
}