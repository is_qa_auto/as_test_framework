﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Threading;

namespace AS_Test_Framework.ShoppingCart
{
    [TestFixture]
    public class ShoppingCart_ShippingAddressPage_VerifyFieldsWithIndiaCustomer : BaseSetUp
    {
        public ShoppingCart_ShippingAddressPage_VerifyFieldsWithIndiaCustomer() : base() { }

        //--- Login Credentials ---//
        string in_username = "test_account_india1@mailinator.com";
        string in_password = "Test_1234";

        [Test, Category("Shopping Cart"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that the Fields are Present and Working as Expected after Adding a Model for Purchase using an India User in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Fields are Present and Working as Expected after Adding a Model for Purchase using an India User in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Fields are Present and Working as Expected after Adding a Model for Purchase using an India User in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Fields are Present and Working as Expected after Adding a Model for Purchase using an India User in RU Locale")]
        public void ShoppingCart_VerifyFieldsWithIndiaCustomerAfterAddingAModelForPurchase(string Locale)
        {
            //--- Shopping Cart URL ---//
            string sc_page_url = Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx";
            if (Locale.Equals("jp") || Locale.Equals("ru"))
            {
                sc_page_url = sc_page_url + "?locale=" + Locale;
            }
            else if (Locale.Equals("cn") || Locale.Equals("zh"))
            {
                sc_page_url = sc_page_url + "?locale=zh";
            }

            action.Navigate(driver, sc_page_url);
            action.ILoginViaLogInLink(driver, in_username, in_password);

            util.RemoveAllModelsInYourCartTable(driver);

            //--- Action: Get Test Data ---//
            string adi_model = util.GetAdiModel();

            action.IAddModelToPurchase(driver, adi_model);

            action.IClick(driver, Elements.Shopping_Cart_Checkout_Button);
            Thread.Sleep(3000);

            //----- R5 > T2_4: Verify Purcahse Shipments to India customers (AL-8303) -----//
            Assert.Multiple(() =>
            {
                //--- Expected Result: The page go to ENTER SHIPPING ADDRESS page and the pop up "The Central Board of Excise & Customs (CBEC) of India…" should appear on the page ----//
                test.validateElementIsPresent(driver, Elements.Shopping_Cart_CBEC_PopUp);

                if (util.CheckElement(driver, Elements.Shopping_Cart_CBEC_PopUp, 2))
                {
                    //--- Expected Result: Verify the links inside the pop up should be working ----//
                    test.validateElementIsPresent(driver, Elements.Shopping_Cart_CBEC_Link1);
                    test.validateElementIsPresent(driver, Elements.Shopping_Cart_CBEC_Link2);
                    test.validateElementIsPresent(driver, Elements.Shopping_Cart_CBEC_Link3);

                    //--- Action: Click the Close Button ---//
                    action.IClick(driver, Elements.Shopping_Cart_CBEC_Close_Btn);
                }
            });

            //----- R5 > T2_2: Verify the Tax Exempt screen on the Shipping Address page for non-US users -----//

            //--- Expected Result: The "Are Shipments To This Address Tax Exempt?" option should not be available in non-US users ----//
            test.validateElementIsNotPresent(driver, Elements.Shopping_Cart_TaxExempt_Sec);
        }

        [Test, Category("Shopping Cart"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that the Fields are Present and Working as Expected after Adding a Model for Sample using an India User in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Fields are Present and Working as Expected after Adding a Model for Sample using an India User in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Fields are Present and Working as Expected after Adding a Model for Sample using an India User in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Fields are Present and Working as Expected after Adding a Model for Sample using an India User in RU Locale")]
        public void ShoppingCart_VerifyFieldsWithIndiaCustomerAfterAddingAModelForSample(string Locale)
        {
            if (!Util.Configuration.Environment.Equals("production"))
            {
                //--- Shopping Cart URL ---//
                string sc_page_url = Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx";
                if (Locale.Equals("jp") || Locale.Equals("ru"))
                {
                    sc_page_url = sc_page_url + "?locale=" + Locale;
                }
                else if (Locale.Equals("cn") || Locale.Equals("zh"))
                {
                    sc_page_url = sc_page_url + "?locale=zh";
                }

                action.Navigate(driver, sc_page_url);
                action.ILoginViaLogInLink(driver, in_username, in_password);

                util.RemoveAllModelsInYourCartTable(driver);

                //--- Action: Get Test Data ---//
                string adi_model = util.GetAdiModel();

                action.IAddModelToSample(driver, adi_model);

                action.IClick(driver, Elements.Shopping_Cart_Checkout_Button);
                Thread.Sleep(3000);

                //----- R5 > T2_3: Verify Sample Shipments to India customers (AL-8303) -----//
                Assert.Multiple(() =>
                {

                    //--- Expected Result: The page go to ENTER SHIPPING ADDRESS page and the pop up "The Central Board of Excise & Customs (CBEC) of India…" should appear on the page ----//
                    test.validateElementIsPresent(driver, Elements.Shopping_Cart_CBEC_PopUp);

                    if (util.CheckElement(driver, Elements.Shopping_Cart_CBEC_PopUp, 2))
                    {
                        //--- Expected Result: Verify the links inside the pop up should be working ----//
                        test.validateElementIsPresent(driver, Elements.Shopping_Cart_CBEC_Link1);
                        test.validateElementIsPresent(driver, Elements.Shopping_Cart_CBEC_Link2);
                        test.validateElementIsPresent(driver, Elements.Shopping_Cart_CBEC_Link3);

                        //--- Action: Click the Close Button ---//
                        action.IClick(driver, Elements.Shopping_Cart_CBEC_Close_Btn);
                    }
                });

                //----- R5 > T2_2: Verify the Tax Exempt screen on the Shipping Address page for non-US users -----//

                //--- Expected Result: The "Are Shipments To This Address Tax Exempt?" option should not be available in non-US users ----//
                test.validateElementIsNotPresent(driver, Elements.Shopping_Cart_TaxExempt_Sec);
            }
        }
    }
}