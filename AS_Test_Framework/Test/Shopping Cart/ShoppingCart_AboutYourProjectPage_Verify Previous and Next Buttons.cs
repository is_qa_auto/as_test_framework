﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Threading;

namespace AS_Test_Framework.ShoppingCart
{
    [TestFixture]
    public class ShoppingCart_AboutYourProjectPage_VerifyPreviousAndNextButtons : BaseSetUp
    {
        public ShoppingCart_AboutYourProjectPage_VerifyPreviousAndNextButtons() : base() { }

        //--- Login Credentials ---//
        string biz_username = "marvin.bebe@analog.com";
        string biz_password = "Test_1234";

        //--- URLs ---//        
        string shippingInformation_url = "ShippingAddress.aspx";
        string editShippingInformation_url = "EditShippingAddress.aspx";
        string billingInformation_url = "BillingAddress.aspx";
        string projectDetails_url = "AboutYourProject.aspx";
        string placeOrder_url = "PlaceOrder.aspx";

        //--- Labels ---//
        string placeSampleRequest_txt = "Place Sample Request";

        [Test, Category("Shopping Cart"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that the Previous and Next Buttons are Present and Working as Expected after Adding a Model for Purchase in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Previous and Next Buttons are Present and Working as Expected after Adding a Model for Purchase in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Previous and Next Buttons are Present and Working as Expected after Adding a Model for Purchase in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Previous and Next Buttons are Present and Working as Expected after Adding a Model for Purchase in RU Locale")]
        public void ShoppingCart_VerifyPreviousAndNextButtonsAfterAddingAModelForPurchase(string Locale)
        {
            //--- Shopping Cart URL ---//
            string sc_page_url = Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx";
            if (Locale.Equals("jp") || Locale.Equals("ru"))
            {
                sc_page_url = sc_page_url + "?locale=" + Locale;
            }
            else if (Locale.Equals("cn") || Locale.Equals("zh"))
            {
                sc_page_url = sc_page_url + "?locale=zh";
            }

            action.Navigate(driver, sc_page_url);
            action.ILoginViaLogInLink(driver, biz_username, biz_password);

            util.RemoveAllModelsInYourCartTable(driver);

            //--- Action: Get Test Data ---//
            string adi_model = util.GetAdiModel();

            action.IAddModelToPurchase(driver, adi_model);

            action.IClick(driver, Elements.Shopping_Cart_Checkout_Button);
            Thread.Sleep(3000);

            action.GoToAboutYourProjectPage(driver);

            if (driver.Url.Contains(projectDetails_url))
            {
                //----- R7 > T6: Verify the Previous button on the About Your Project page -----//

                //--- Action: Click on the Previous button ---//
                action.IClick(driver, Elements.Shopping_Cart_Prev_Button);

                //--- Expected Result: The page should be redirected to the Billing Address page ---//
                if (Locale.Equals("cn") || Locale.Equals("zh"))
                {
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + billingInformation_url + "?locale=zh");
                }
                else
                {
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + billingInformation_url + "?locale=" + Locale);
                }

                //--- Action: Click on the Next button ---//
                action.IClick(driver, Elements.Shopping_Cart_Footer_Next_Button);

                //----- R7 > T7: Verify the Next button on the About Your Project page -----//

                //--- Action: Complete all the mandatory fields on the About Your Project page ---//          
                //--- Action: Click on the Next button ---//
                action.IPopulateFieldsInAboutYourProjectPage(driver);

                //--- Expected Result: The Enter Payment and Place Order page should be displayed. ---//
                if (Locale.Equals("cn") || Locale.Equals("zh"))
                {
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + placeOrder_url + "?locale=zh");
                }
                else
                {
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + placeOrder_url + "?locale=" + Locale);
                }
            }
            else
            {
                //--- Validation if the Page is in the About Your Project Page ---//
                if (Locale.Equals("cn") || Locale.Equals("zh"))
                {
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + projectDetails_url + "?locale=zh");
                }
                else
                {
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + projectDetails_url + "?locale=" + Locale);
                }
            }
        }

        [Test, Category("Shopping Cart"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that the Previous and Next Buttons are Present and Working as Expected after Adding a Model for Sample in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Previous and Next Buttons are Present and Working as Expected after Adding a Model for Sample in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Previous and Next Buttons are Present and Working as Expected after Adding a Model for Sample in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Previous and Next Buttons are Present and Working as Expected after Adding a Model for Sample in RU Locale")]
        public void ShoppingCart_VerifyPreviousAndNextButtonsAfterAddingAModelForSample(string Locale)
        {
            //--- Shopping Cart URL ---//
            string sc_page_url = Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx";
            if (Locale.Equals("jp") || Locale.Equals("ru"))
            {
                sc_page_url = sc_page_url + "?locale=" + Locale;
            }
            else if (Locale.Equals("cn") || Locale.Equals("zh"))
            {
                sc_page_url = sc_page_url + "?locale=zh";
            }

            action.Navigate(driver, sc_page_url);
            action.ILoginViaLogInLink(driver, biz_username, biz_password);

            util.RemoveAllModelsInYourCartTable(driver);

            //--- Action: Get Test Data ---//
            string adi_model = util.GetAdiModel();

            action.IAddModelToSample(driver, adi_model);

            action.IClick(driver, Elements.Shopping_Cart_Checkout_Button);
            Thread.Sleep(3000);

            action.GoToAboutYourProjectPage(driver);

            if (driver.Url.Contains(projectDetails_url))
            {
                //----- R8 > T6: Verify the Previous button on the About Your Project page -----//

                //--- Action: Click on the Previous button ---//
                action.IClick(driver, Elements.Shopping_Cart_Prev_Button);

                //--- Expected Result: The page should be redirected to the Shipping Address page ---//
                if (driver.Url.Contains(editShippingInformation_url))
                {
                    if (Locale.Equals("cn") || Locale.Equals("zh"))
                    {
                        test.validateStringInstance(driver, driver.Url, "analog.com/" + editShippingInformation_url + "?locale=zh");
                    }
                    else
                    {
                        test.validateStringInstance(driver, driver.Url, "analog.com/" + editShippingInformation_url + "?locale=" + Locale);
                    }
                }
                else
                {
                    if (Locale.Equals("cn") || Locale.Equals("zh"))
                    {
                        test.validateStringInstance(driver, driver.Url, "analog.com/" + shippingInformation_url + "?locale=zh");
                    }
                    else
                    {
                        test.validateStringInstance(driver, driver.Url, "analog.com/" + shippingInformation_url + "?locale=" + Locale);
                    }
                }

                //--- Action: Click on the Next button ---//
                action.IClick(driver, Elements.Shopping_Cart_Footer_Next_Button);

                //----- R8 > T6: Verify the Next button on the About Your Project page -----//

                //--- Action: Complete all the mandatory fields on the About Your Project page ---//          
                //--- Action: Click on the Next button ---//
                action.IPopulateFieldsInAboutYourProjectPage(driver);

                //--- Expected Result: The Sample Request Review page should be displayed. ---//
                if (Locale.Equals("cn") || Locale.Equals("zh"))
                {
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + placeOrder_url + "?locale=zh");
                }
                else
                {
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + placeOrder_url + "?locale=" + Locale);
                }

                //--- Expected Result: The label on the step should be Place Sample Request ---//
                if (Locale.Equals("en"))
                {
                    test.validateStringIsCorrect(driver, Elements.Shopping_Cart_PlaceOrder_Step_Lbl, placeSampleRequest_txt);
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.Shopping_Cart_PlaceOrder_Step_Lbl);
                }
            }
            else
            {
                //--- Validation if the Page is in the About Your Project Page ---//
                if (Locale.Equals("cn") || Locale.Equals("zh"))
                {
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + projectDetails_url + "?locale=zh");
                }
                else
                {
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + projectDetails_url + "?locale=" + Locale);
                }
            }
        }
    }
}