﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Threading;

namespace AS_Test_Framework.ShoppingCart
{
    [TestFixture]
    public class ShoppingCart_ShippingAddressPage_VerifyBackToShoppingCartAndNextButtons : BaseSetUp
    {
        public ShoppingCart_ShippingAddressPage_VerifyBackToShoppingCartAndNextButtons() : base() { }

        //--- Login Credentials ---//
        string biz_username = "marvin.bebe@analog.com";
        string biz_password = "Test_1234";

        //--- URLs ---//
        string shippingInformation_url = "ShippingAddress.aspx";
        string billingInformation_url = "BillingAddress.aspx";
        string projectDetails_url = "AboutYourProject.aspx";

        [Test, Category("Shopping Cart"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that the Footer Back to Shopping Cart and Next Buttons are Present and Working as Expected after Adding a Model for Purchase in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Footer Back to Shopping Cart and Next Buttons are Present and Working as Expected after Adding a Model for Purchase in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Footer Back to Shopping Cart and Next Buttons are Present and Working as Expected after Adding a Model for Purchase in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Footer Back to Shopping Cart and Next Buttons are Present and Working as Expected after Adding a Model for Purchase in RU Locale")]
        public void ShoppingCart_VerifyFooterBackToShoppingCartAndNextButtonsAfterAddingAModelForPurchase(string Locale)
        {
            //--- Shopping Cart URL ---//
            string sc_page_url = Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx";
            if (Locale.Equals("jp") || Locale.Equals("ru"))
            {
                sc_page_url = sc_page_url + "?locale=" + Locale;
            }
            else if (Locale.Equals("cn") || Locale.Equals("zh"))
            {
                sc_page_url = sc_page_url + "?locale=zh";
            }

            action.Navigate(driver, sc_page_url);
            action.ILoginViaLogInLink(driver, biz_username, biz_password);

            util.RemoveAllModelsInYourCartTable(driver);

            //--- Action: Get Test Data ---//
            string adi_model = util.GetAdiModel();

            action.IAddModelToPurchase(driver, adi_model);

            action.IClick(driver, Elements.Shopping_Cart_Checkout_Button);
            Thread.Sleep(3000);

            if (driver.Url.Contains(shippingInformation_url))
            {
                //----- R5 > T4: Verify the Back to Shopping Cart button on the Shipping Address page -----//

                //--- Action: Click on the Back to Shopping Cart button (in the Footer) ----//
                action.IClick(driver, Elements.Shopping_Cart_ShippingAddress_Footer_BackToSC_Button);

                //--- Expected Result: The page should be redirected to the Shopping Cart page ----//
                test.validateStringInstance(driver, driver.Url, sc_page_url);

                //--- Action: Click the Checkout Button ----//
                action.IClick(driver, Elements.Shopping_Cart_Checkout_Button);
                Thread.Sleep(3000);

                //----- R5 > T5.2: Verify the Next button on the Shipping Address page (Purchase) (AL-10650) -----//

                //--- Action: Complete all the mandatory fields on the Shipping Address page ----//
                //--- Action: Click on the Next button ----//
                action.IPopulateFieldsInShippingAddressPage(driver);

                //--- Expected Result: The Billing Address page should be displayed. ----//
                if (Locale.Equals("cn") || Locale.Equals("zh"))
                {
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + billingInformation_url + "?locale=zh");
                }
                else
                {
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + billingInformation_url + "?locale=" + Locale);
                }
            }
            else
            {
                //--- Validation if the Page is in the Shipping Address Page ---//
                if (Locale.Equals("cn") || Locale.Equals("zh"))
                {
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + shippingInformation_url + "?locale=zh");
                }
                else
                {
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + shippingInformation_url + "?locale=" + Locale);
                }
            }
        }

        [Test, Category("Shopping Cart"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that the Heading Back to Shopping Cart Button is Present and Working as Expected after Adding a Model for Sample in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Heading Back to Shopping Cart Button is Present and Working as Expected after Adding a Model for Sample in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Heading Back to Shopping Cart Button is Present and Working as Expected after Adding a Model for Sample in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Heading Back to Shopping Cart Button is Present and Working as Expected after Adding a Model for Sample in RU Locale")]
        public void ShoppingCart_VerifyHeadingBackToShoppingCartButtonAfterAddingAModelForSample(string Locale)
        {
            //--- Shopping Cart URL ---//
            string sc_page_url = Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx";
            if (Locale.Equals("jp") || Locale.Equals("ru"))
            {
                sc_page_url = sc_page_url + "?locale=" + Locale;
            }
            else if (Locale.Equals("cn") || Locale.Equals("zh"))
            {
                sc_page_url = sc_page_url + "?locale=zh";
            }

            action.Navigate(driver, sc_page_url);
            action.ILoginViaLogInLink(driver, biz_username, biz_password);

            util.RemoveAllModelsInYourCartTable(driver);

            //--- Action: Get Test Data ---//
            string adi_model = util.GetAdiModel();

            action.IAddModelToSample(driver, adi_model);

            action.IClick(driver, Elements.Shopping_Cart_Checkout_Button);
            Thread.Sleep(3000);

            //----- R5 > T4: Verify the Back to Shopping Cart button on the Shipping Address page -----//

            //--- Action: Click on the Back to Shopping Cart button (in the ENTER SHIPPING ADDRESS Heading) ----//
            action.IClick(driver, Elements.Shopping_Cart_ShippingAddress_Heading_BackToSC_Button);

            //--- Expected Result: The page should be redirected to the Shopping Cart page ----//
            test.validateStringInstance(driver, driver.Url, sc_page_url);

            //----- ECommerce TestPlan > Test Case Title: Verify the Next button on the Shipping Address page (Sample) (AL-10650) -----//

            //--- Action: Click Checkout button ---//
            action.IClick(driver, Elements.Shopping_Cart_Checkout_Button);
            Thread.Sleep(3000);

            //--- Action: Complete all the mandatory fields on the Shipping Address page ---//
            //--- Action: Click on the Next button ---//
            action.GoToAboutYourProjectPage(driver);

            //--- Expected Result: The About Your Project page should be displayed. ---//
            if (Locale.Equals("cn") || Locale.Equals("zh"))
            {
                test.validateStringInstance(driver, driver.Url, "analog.com/" + projectDetails_url + "?locale=zh");
            }
            else
            {
                test.validateStringInstance(driver, driver.Url, "analog.com/" + projectDetails_url + "?locale=" + Locale);
            }
        }
    }
}