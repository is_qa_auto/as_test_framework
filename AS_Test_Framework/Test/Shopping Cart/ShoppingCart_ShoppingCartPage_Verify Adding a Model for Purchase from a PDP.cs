﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Threading;
using System;

namespace AS_Test_Framework.ShoppingCart
{
    [TestFixture]
    public class ShoppingCart_ShoppingCartPage_VerifyAddingAModelForPurchaseFromAPdp : BaseSetUp
    {
        public ShoppingCart_ShoppingCartPage_VerifyAddingAModelForPurchaseFromAPdp() : base() { }

        //--- Login Credentials ---//
        string nonBiz_username = "qa_automation_user2@mailinator.com";
        string nonBiz_password = "Test_1234";
        //string biz_username = "marvin.bebe@analog.com";
        //string biz_password = "Test_1234";

        //--- URLs ---//
        string exportControlled_pdp_url = "ad9208";
        //string exportControlled_pdp_url = "ad9162";
        string ltc_pdp_url = "ltc4415";
        //string ltc_pdp_url = "ltc4420";
        //string ltc_pdp_url = "ltc2360";

        //--- Labels ---//
        string exportControlledModel_errMsg = " is controlled for export. ADI policy does not permit export controlled products to be ordered or sampled on line. View sales & distributor map to contact local sales or distribution office for options. (Error : 021)";

        [Test, Category("Shopping Cart"), Category("NonCore")]
        [TestCase("en", TestName = "Verify Adding an Export Controlled Model for Purchase from a PDP using a Non-business Email in EN Locale")]
        public void ShoppingCart_VerifyAddingAnExportControlledModelForPurchaseFromAPdpUsingANonBusinessEmail(string Locale)
        {
            //----- R3 > T3_2: Verify the Message for Parts that are Export Controlled from a PDP -----//

            //----- Shopping Cart Page: Login via Log in/Register in -----//
            string sc_page_url = Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx";

            action.Navigate(driver, sc_page_url);
            action.ILoginViaLogInLink(driver, nonBiz_username, nonBiz_password);

            util.RemoveAllModelsInYourCartTable(driver);

            //----- PDP: Add a Model to Shopping Cart -----//
            Console.WriteLine("Go to " + Configuration.Env_Url + Locale + "/" + exportControlled_pdp_url);
            driver.Navigate().GoToUrl(Configuration.Env_Url + Locale + "/" + exportControlled_pdp_url);
            Thread.Sleep(1000);

            //--- Select "UNITED STATES" in Select a country Dropdown ---//
            action.IClick(driver, By.CssSelector("div[data-grid-template$='sb-table-1'] * a>span:nth-last-of-type(1)"));
            action.IClick(driver, By.CssSelector("div[data-grid-template$='sb-table-1'] * a[data-id='US']"));

            //--- Click the Check Inventory Button ---//
            action.IClick(driver, Elements.PDP_CheckInventory_Btn);

            //--- Click the Add to cart Button ---//
            action.IClick(driver, By.CssSelector("div[class$='view-inventory'] * td[data-id='AnalogDevices']>a[class='btn btn-primary']"));

            int quantity = Int32.Parse(util.ReturnAttribute(driver, Elements.SC_Quantity_Textfield, "value"));
            //---------------------------------------------------//
            if (quantity > 1) {
                action.IDeleteValueOnFields(driver, Elements.SC_Quantity_Textfield);
                action.IType(driver, Elements.SC_Quantity_Textfield, "1");
                action.IClick(driver, Elements.Shopping_Cart_Update_Btn);
            }
           
            test.validateStringIsCorrect(driver, Elements.SC_QtyCol_ErrorMessage_Line1_Label, util.GetText(driver, Elements.Shopping_Cart_AddedModel) + exportControlledModel_errMsg);
           
        }

        //[Test, Category("Shopping Cart"), Category("NonCore")]
        //[TestCase("en", TestName = "Verify Adding an LTC Model for Purchase from a PDP using a Business Email in EN Locale")]
        ////[TestCase("cn", TestName = "Verify Adding an LTC Model for Purchase from a PDP using a Business Email in CN Locale")]
        ////[TestCase("jp", TestName = "Verify Adding an LTC Model for Purchase from a PDP using a Business Email in JP Locale")]
        ////[TestCase("ru", TestName = "Verify Adding an LTC Model for Purchase from a PDP using a Business Email in RU Locale")]
        //public void ShoppingCart_VerifyAddingAnLtcModelForPurchaseFromAPdpUsingABusinessEmail(string Locale)
        //{
        //    //----- R20 > T2 > S3: Add LTC model from product page after login to the page (any on sample or purchase) -----//

        //    //--- Shopping Cart URL ---//
        //    string sc_page_url = Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx";
        //    if (Locale.Equals("jp") || Locale.Equals("ru"))
        //    {
        //        sc_page_url = sc_page_url + "?locale=" + Locale;
        //    }
        //    else if (Locale.Equals("cn") || Locale.Equals("zh"))
        //    {
        //        sc_page_url = sc_page_url + "?locale=zh";
        //    }

        //    //----- Shopping Cart Page -----//
        //    action.Navigate(driver, sc_page_url);
        //    action.ILoginViaLogInLink(driver, biz_username, biz_password);

        //    util.RemoveAllModelsInYourCartTable(driver);

        //    //----- LTC PDP -----//
        //    driver.Navigate().GoToUrl(Configuration.Env_Url + Locale + "/" + ltc_pdp_url);
        //    Thread.Sleep(1000);

        //    //--- Action: Select "UNITED STATES" in Select a country Dropdown ---//
        //    action.IClick(driver, By.CssSelector("div[data-grid-template$='sb-table-1'] * a>span:nth-last-of-type(1)"));
        //    action.IClick(driver, By.CssSelector("div[data-grid-template$='sb-table-1'] * a[data-id='US']"));

        //    //--- Action: Click the Check Inventory Button ---//
        //    action.IClick(driver, Elements.PDP_CheckInventory_Btn);

        //    //--- Action: Click the Add to cart Button ---//
        //    action.IMouseOverTo(driver, By.CssSelector("section[id='product-samplebuy'] * div[class='sectionHeader']"));
        //    Thread.Sleep(1000);
        //    action.IClick(driver, By.CssSelector("div[class$='view-inventory'] * td[data-id='AnalogDevices']>a[class='btn btn-primary']"));
        //    Thread.Sleep(4000);

        //    //----- Shopping Cart Page -----//
        //    if (!driver.Url.Contains("shoppingcart"))
        //    {
        //        driver.Navigate().GoToUrl(sc_page_url);
        //        driver.Navigate().Refresh();
        //    }

        //    //--- Expected Result: After clicking sample or purchase, the page will redirect to shopping cart page without any error ---//
        //    test.validateStringInstance(driver, util.GetText(driver, Elements.Shopping_Cart_AddedModel), ltc_pdp_url.ToUpper());
        //}

        [Test, Category("Shopping Cart"), Category("NonCore")]
        [TestCase("en", TestName = "Verify Adding an LTC Model for Purchase from a PDP as Anonymous User in EN Locale")]
        //[TestCase("cn", TestName = "Verify Adding an LTC Model for Purchase from a PDP as Anonymous User in CN Locale")]
        //[TestCase("jp", TestName = "Verify Adding an LTC Model for Purchase from a PDP as Anonymous User in JP Locale")]
        //[TestCase("ru", TestName = "Verify Adding an LTC Model for Purchase from a PDP as Anonymous User in RU Locale")]
        public void ShoppingCart_VerifyAddingAnLtcModelForPurchaseFromAPdpAsAnonymousUser(string Locale)
        {
            //----- R20 > T1 > S2: Add LTC model from product page without logging in (any on sample or purchase) -----//

            //--- Shopping Cart URL ---//
            string sc_page_url = Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx";
            if (Locale.Equals("jp") || Locale.Equals("ru"))
            {
                sc_page_url = sc_page_url + "?locale=" + Locale;
            }
            else if (Locale.Equals("cn") || Locale.Equals("zh"))
            {
                sc_page_url = sc_page_url + "?locale=zh";
            }

            //----- LTC PDP -----//
            action.Navigate(driver, Configuration.Env_Url + Locale + "/" + ltc_pdp_url);

            //--- Action: Select "UNITED STATES" in Select a country Dropdown ---//
            action.IClick(driver, By.CssSelector("div[data-grid-template$='sb-table-1'] * a>span:nth-last-of-type(1)"));
            action.IClick(driver, By.CssSelector("div[data-grid-template$='sb-table-1'] * a[data-id='US']"));

            //--- Action: Click the Check Inventory Button ---//
            action.IClick(driver, Elements.PDP_CheckInventory_Btn);

            //--- Action: Click the Add to cart Button ---//
            action.IMouseOverTo(driver, By.CssSelector("section[id='product-samplebuy'] * div[class='sectionHeader']"));
            Thread.Sleep(1000);
            action.IClick(driver, By.CssSelector("div[class$='view-inventory'] * td[data-id='AnalogDevices']>a[class='btn btn-primary']"));
            Thread.Sleep(4000);

            //----- Shopping Cart Page -----//
            if (!driver.Url.Contains("shoppingcart"))
            {
                driver.Navigate().GoToUrl(sc_page_url);
                driver.Navigate().Refresh();
            }

            //--- Expected Result: After clicking sample or purchase, the page will redirect to shopping cart page without any error ---//
            test.validateStringInstance(driver, util.GetText(driver, Elements.Shopping_Cart_AddedModel), ltc_pdp_url.ToUpper());
        }
    }
}