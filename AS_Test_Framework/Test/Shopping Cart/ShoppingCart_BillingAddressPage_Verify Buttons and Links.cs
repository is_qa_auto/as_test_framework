﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.ShoppingCart
{
    [TestFixture]
    public class ShoppingCart_BillingAddressPage_VerifyButtonsAndLinks : BaseSetUp
    {
        public ShoppingCart_BillingAddressPage_VerifyButtonsAndLinks() : base() { }

        //--- Login Credentials ---//
        string biz_username = "marvin.bebe@analog.com";
        string biz_password = "Test_1234";

        //--- URLs ---//
        string customerServiceSupport_form_url = "analog.com/Form_Pages/support/customerService.aspx";
        string customerServiceSupport_china_form_url = "analog.com/Form_Pages/support/customerService_china.aspx";

        [Test, Category("Shopping Cart"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that the Buttons and Links are Present and Working as Expected after Adding a Model for Purchase in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Buttons and Links are Present and Working as Expected after Adding a Model for Purchase in CN Locale")]
        //[TestCase("ru", TestName = "Verify that the Buttons and Links are Present and Working as Expected after Adding a Model for Purchase in RU Locale")]
        public void ShoppingCart_VerifyButtonsAndLinksAfterAddingAModelForPurchase(string Locale)
        {
            //--- Shopping Cart URL ---//
            string sc_page_url = Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx";
            if (Locale.Equals("jp") || Locale.Equals("ru"))
            {
                sc_page_url = sc_page_url + "?locale=" + Locale;
            }
            else if (Locale.Equals("cn") || Locale.Equals("zh"))
            {
                sc_page_url = sc_page_url + "?locale=zh";
            }

            action.Navigate(driver, sc_page_url);
            action.ILoginViaLogInLink(driver, biz_username, biz_password);

            util.RemoveAllModelsInYourCartTable(driver);

            //--- Action: Get Test Data ---//
            string adi_model = util.GetAdiModel();

            action.IAddModelToPurchase(driver, adi_model);

            action.IClick(driver, Elements.Shopping_Cart_Checkout_Button);
            Thread.Sleep(3000);

            action.GoToBillingAddressPage(driver);

            //----- R6 > T3: Verify the Customer Service link on the Billing Address page -----//
            if (!Locale.Equals("jp"))
            {
                //--- Action: Click on the Customer Service link ----//
                string sc_billingAdd_url = driver.Url;

                action.IClick(driver, Elements.Shopping_Cart_CustomerService_Link);
                Thread.Sleep(1000);

                if (driver.Url.Contains(sc_billingAdd_url))
                {
                    driver.SwitchTo().Window(driver.WindowHandles.Last());
                }

                //--- Expected Result: The Customer Service Support form should be displayed in a new tab/window. ----//
                if (Locale.Equals("cn") || Locale.Equals("zh"))
                {
                    test.validateStringInstance(driver, driver.Url, customerServiceSupport_china_form_url);
                }
                else
                {
                    test.validateStringInstance(driver, driver.Url, customerServiceSupport_form_url);
                }
            }
        }
    }
}