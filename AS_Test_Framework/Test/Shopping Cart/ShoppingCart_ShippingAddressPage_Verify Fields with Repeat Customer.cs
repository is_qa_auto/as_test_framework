﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Threading;

namespace AS_Test_Framework.ShoppingCart
{
    [TestFixture]
    public class ShoppingCart_ShippingAddressPage_VerifyFieldsWithRepeatCustomer : BaseSetUp
    {
        public ShoppingCart_ShippingAddressPage_VerifyFieldsWithRepeatCustomer() : base() { }

        //--- Login Credentials (Repeat Customer) ---//
        string repeatCust_username = "marvin.bebe@analog.com";
        string repeatCust_password = "Test_1234";

        //--- Login Credentials (Stored Shipping Addresses Limit) ---//
        string storedShippingAddressesLimit_username = "sc_t3st_addr3ss_l1m1t@mailinator.com";
        string storedShippingAddressesLimit_password = "Test_1234";

        [Test, Category("Shopping Cart"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that the Fields are Present and Working as Expected after Adding a Model for Purchase using a Repeat Customer in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Fields are Present and Working as Expected after Adding a Model for Purchase using a Repeat Customer in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Fields are Present and Working as Expected after Adding a Model for Purchase using a Repeat Customer in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Fields are Present and Working as Expected after Adding a Model for Purchase using a Repeat Customer in RU Locale")]
        public void ShoppingCart_VerifyFieldsWithRepeatCustomerAfterAddingAModelForForPurchase(string Locale)
        {
            //--- Shopping Cart URL ---//
            string sc_page_url = Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx";
            if (Locale.Equals("jp") || Locale.Equals("ru"))
            {
                sc_page_url = sc_page_url + "?locale=" + Locale;
            }
            else if (Locale.Equals("cn") || Locale.Equals("zh"))
            {
                sc_page_url = sc_page_url + "?locale=zh";
            }

            action.Navigate(driver, sc_page_url);
            action.ILoginViaLogInLink(driver, repeatCust_username, repeatCust_password);

            util.RemoveAllModelsInYourCartTable(driver);

            //--- Action: Get Test Data ---//
            string adi_model = util.GetAdiModel();

            action.IAddModelToPurchase(driver, adi_model);

            action.IClick(driver, Elements.Shopping_Cart_Checkout_Button);
            Thread.Sleep(3000);

            //----- R5 > T2_1: Verify the Tax Exempt screen on the Shipping Address page for US users -----//

            //--- Action: Select the Tax Exempt option on the 'Are Shipments To This Address Tax Exempt?' section ----//
            action.IClick(driver, Elements.Shopping_Cart_TaxExpt_Rb);

            //--- Expected Result: The Tax Exempt Notice lightbox should be displayed. ----//
            test.validateElementIsPresent(driver, Elements.Shopping_Cart_TaxExptNotice_LtBx);

            //--- Action: Click on the Close (X) link on the upper right corner of the lightbox ----//
            action.IClick(driver, Elements.Shopping_Cart_TaxExptNotice_LtBx_X_Btn);

            //--- Expected Result: The lightbox should be closed. ----//
            test.validateElementIsNotPresent(driver, Elements.Shopping_Cart_TaxExptNotice_LtBx);

            //--- Action: Select again the Tax Exempt option on the 'Are Shipments To This Address Tax Exempt?' section ----//
            action.IClick(driver, Elements.Shopping_Cart_TaxExpt_Rb);

            //--- Action: Click on the Change Tax Status button within the lightbox ----//
            action.IClick(driver, Elements.Shopping_Cart_TaxExptNotice_LtBx_ChangeTaxStat_Btn);

            //--- Expected Result: The lightbox should be closed. ----//
            test.validateElementIsNotPresent(driver, Elements.Shopping_Cart_TaxExptNotice_LtBx);

            //--- Expected Result: The value on the 'Are Shipments To This Address Tax Exempt?' section is changed to Taxable ----//
            test.validateSelectedRadioButtonIsCorrect(driver, Elements.Shopping_Cart_Taxable_Rb, true);

            //--- Action: Select again the Tax Exempt option on the 'Are Shipments To This Address Tax Exempt?' section ----//
            action.IClick(driver, Elements.Shopping_Cart_TaxExpt_Rb);

            //--- Action: Click on the Keep Tax Exempt Status and Continue button within the lightbox ----//
            action.IClick(driver, Elements.Shopping_Cart_TaxExptNotice_LtBx_KeepTaxExemptStatAndCont_Btn);

            //--- Expected Result: The lightbox should be closed. ----//
            test.validateElementIsNotPresent(driver, Elements.Shopping_Cart_TaxExptNotice_LtBx);

            //--- Expected Result: ----//
            test.validateSelectedRadioButtonIsCorrect(driver, Elements.Shopping_Cart_TaxExpt_Rb, true);

            if (util.CheckElement(driver, Elements.Shopping_Cart_ShippingAddress_PrevAdd_Sec, 2))
            {
                string defaultCityValue = util.GetText(driver, Elements.Shopping_Cart_ShippingAddress_PrevAdd_City_Value);

                //----- R5 > T1_2: Verify the Shipping Address page fields with repeat customer -----//

                //--- Expected Result: The buttons should be displayed on the Shipping Address box ---//
                test.validateElementIsPresent(driver, Elements.Shopping_Cart_ShippingAddress_Delete_Btn);
                test.validateElementIsPresent(driver, Elements.Shopping_Cart_ShippingAddress_Edit_Btn);

                //--- Action: Click on the Edit button ---//
                action.IClick(driver, Elements.Shopping_Cart_ShippingAddress_Edit_Btn);

                //--- Expected Result: The details on the Shipping Address box should be populated on the input fields ---//
                test.validateFieldIsNotEmpty(driver, Elements.Shopping_Cart_OrgName_Tbx);
                test.validateFieldIsNotEmpty(driver, Elements.Shopping_Cart_FirstName_Tbx);
                test.validateFieldIsNotEmpty(driver, Elements.Shopping_Cart_LastName_Tbx);
                test.validateFieldIsNotEmpty(driver, Elements.Shopping_Cart_AddressLine1_Tbx);
                test.validateFieldIsNotEmpty(driver, Elements.Shopping_Cart_City_Tbx);
                test.validateSelectedValueIsCorrect(driver, Elements.Shopping_Cart_StateProvince_Dd, "Alabama");
                test.validateFieldIsNotEmpty(driver, Elements.Shopping_Cart_ZipPostalCode_Tbx);
                test.validateSelectedValueIsCorrect(driver, Elements.Shopping_Cart_CountryRegion_Dd, "UNITED STATES");
                test.validateFieldIsNotEmpty(driver, Elements.Shopping_Cart_Telephone_Tbx);

                //--- Action: Update any field values (e.g. City) ---//
                string city_input = "City_" + util.RandomString();
                action.IDeleteValueOnFields(driver, Elements.Shopping_Cart_City_Tbx);
                action.IType(driver, Elements.Shopping_Cart_City_Tbx, city_input);

                //--- Action: Click on the Cancel button ---//
                action.IClick(driver, Elements.Shopping_Cart_EditShippingAddress_Cancel_Button);

                //--- Expected Result: The stored shipping address box should be displayed ---//
                test.validateElementIsPresent(driver, Elements.Shopping_Cart_ShippingAddress_PrevAdd_Sec);

                //--- Expected Result: The changes will be discarded ---//
                test.validateStringIsCorrect(driver, Elements.Shopping_Cart_ShippingAddress_PrevAdd_City_Value, defaultCityValue);

                //--- Action: Click on the Edit button ---//
                action.IClick(driver, Elements.Shopping_Cart_ShippingAddress_Edit_Btn);

                //--- Action: Update any field values (e.g. Organization Name) ---//
                string orgName_input = "Organization_" + util.RandomString();
                action.IDeleteValueOnFields(driver, Elements.Shopping_Cart_OrgName_Tbx);
                action.IType(driver, Elements.Shopping_Cart_OrgName_Tbx, orgName_input);

                //--- Click on the Save button ---//
                action.IClick(driver, Elements.Shopping_Cart_EditShippingAddress_Save_Button);

                //--- Expected Result: The Stored Shipping Address box should be updated ---//
                test.validateStringIsCorrect(driver, Elements.Shopping_Cart_ShippingAddress_PrevAdd_OrgName_Value, orgName_input);

                //--- Action: Click on the Delete button ---//
                action.IClick(driver, Elements.Shopping_Cart_ShippingAddress_Delete_Btn);

                //--- Expected Result: The Stored Shipping Address box should be removed ---//
                test.validateElementIsNotPresent(driver, Elements.Shopping_Cart_ShippingAddress_PrevAdd_Sec);
            }
        }

        [Test, Category("Shopping Cart"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that the Fields are Present and Working as Expected after Adding a Model for Sample using a Repeat Customer in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Fields are Present and Working as Expected after Adding a Model for Sample using a Repeat Customer in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Fields are Present and Working as Expected after Adding a Model for Sample using a Repeat Customer in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Fields are Present and Working as Expected after Adding a Model for Sample using a Repeat Customer in RU Locale")]
        public void ShoppingCart_VerifyFieldsWithRepeatCustomerAfterAddingAModelForSample(string Locale)
        {
            //--- Shopping Cart URL ---//
            string sc_page_url = Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx";
            if (Locale.Equals("jp") || Locale.Equals("ru"))
            {
                sc_page_url = sc_page_url + "?locale=" + Locale;
            }
            else if (Locale.Equals("cn") || Locale.Equals("zh"))
            {
                sc_page_url = sc_page_url + "?locale=zh";
            }

            action.Navigate(driver, sc_page_url);
            action.ILoginViaLogInLink(driver, repeatCust_username, repeatCust_password);

            util.RemoveAllModelsInYourCartTable(driver);

            //--- Action: Get Test Data ---//
            string adi_model = util.GetAdiModel();

            action.IAddModelToSample(driver, adi_model);

            action.IClick(driver, Elements.Shopping_Cart_Checkout_Button);
            Thread.Sleep(3000);

            if (util.CheckElement(driver, Elements.Shopping_Cart_ShippingAddress_PrevAdd_Sec, 2))
            {
                string defaultCityValue = util.GetText(driver, Elements.Shopping_Cart_ShippingAddress_PrevAdd_City_Value);

                //----- R5 > T1_2: Verify the Shipping Address page fields with repeat customer -----//

                //--- Expected Result: The buttons should be displayed on the Shipping Address box ---//
                test.validateElementIsPresent(driver, Elements.Shopping_Cart_ShippingAddress_Delete_Btn);
                test.validateElementIsPresent(driver, Elements.Shopping_Cart_ShippingAddress_Edit_Btn);

                //--- Action: Click on the Edit button ---//
                action.IClick(driver, Elements.Shopping_Cart_ShippingAddress_Edit_Btn);

                //--- Expected Result: The details on the Shipping Address box should be populated on the input fields ---//
                test.validateFieldIsNotEmpty(driver, Elements.Shopping_Cart_OrgName_Tbx);
                test.validateFieldIsNotEmpty(driver, Elements.Shopping_Cart_FirstName_Tbx);
                test.validateFieldIsNotEmpty(driver, Elements.Shopping_Cart_LastName_Tbx);
                test.validateFieldIsNotEmpty(driver, Elements.Shopping_Cart_AddressLine1_Tbx);
                test.validateFieldIsNotEmpty(driver, Elements.Shopping_Cart_City_Tbx);
                test.validateSelectedValueIsCorrect(driver, Elements.Shopping_Cart_StateProvince_Dd, "Alabama");
                test.validateFieldIsNotEmpty(driver, Elements.Shopping_Cart_ZipPostalCode_Tbx);
                test.validateSelectedValueIsCorrect(driver, Elements.Shopping_Cart_CountryRegion_Dd, "UNITED STATES");
                test.validateFieldIsNotEmpty(driver, Elements.Shopping_Cart_Telephone_Tbx);

                //--- Action: Update any field values (e.g. City) ---//
                string city_input = "City_" + util.RandomString();
                action.IDeleteValueOnFields(driver, Elements.Shopping_Cart_City_Tbx);
                action.IType(driver, Elements.Shopping_Cart_City_Tbx, city_input);

                //--- Action: Click on the Cancel button ---//
                action.IClick(driver, Elements.Shopping_Cart_EditShippingAddress_Cancel_Button);

                //--- Expected Result: The stored shipping address box should be displayed ---//
                test.validateElementIsPresent(driver, Elements.Shopping_Cart_ShippingAddress_PrevAdd_Sec);

                //--- Expected Result: The changes will be discarded ---//
                test.validateStringIsCorrect(driver, Elements.Shopping_Cart_ShippingAddress_PrevAdd_City_Value, defaultCityValue);

                //--- Action: Click on the Edit button ---//
                action.IClick(driver, Elements.Shopping_Cart_ShippingAddress_Edit_Btn);

                //--- Action: Update any field values (e.g. Organization Name) ---//
                string orgName_input = "Organization_" + util.RandomString();
                action.IDeleteValueOnFields(driver, Elements.Shopping_Cart_OrgName_Tbx);
                action.IType(driver, Elements.Shopping_Cart_OrgName_Tbx, orgName_input);

                //--- Click on the Save button ---//
                action.IClick(driver, Elements.Shopping_Cart_EditShippingAddress_Save_Button);

                //--- Expected Result: The Stored Shipping Address box should be updated ---//
                test.validateStringIsCorrect(driver, Elements.Shopping_Cart_ShippingAddress_PrevAdd_OrgName_Value, orgName_input);

                //--- Action: Click on the Delete button ---//
                action.IClick(driver, Elements.Shopping_Cart_ShippingAddress_Delete_Btn);

                //--- Expected Result: The Stored Shipping Address box should be removed ---//
                test.validateElementIsNotPresent(driver, Elements.Shopping_Cart_ShippingAddress_PrevAdd_Sec);
            }
        }

        [Test, Category("Shopping Cart"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that the Stored Shipping Addresses Limit Validation is Working as Expected after Adding a Model for Purchase in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Stored Shipping Addresses Limit Validation is Working as Expected after Adding a Model for Purchase in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Stored Shipping Addresses Limit Validation is Working as Expected after Adding a Model for Purchase in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Stored Shipping Addresses Limit Validation is Working as Expected after Adding a Model for Purchase in RU Locale")]
        public void ShoppingCart_VerifyStoredShippingAddressesLimitValidationAfterAddingAModelForForPurchase(string Locale)
        {
            if (!Util.Configuration.Environment.Equals("production"))
            {
                //--- Shopping Cart URL ---//
                string sc_page_url = Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx";
                if (Locale.Equals("jp") || Locale.Equals("ru"))
                {
                    sc_page_url = sc_page_url + "?locale=" + Locale;
                }
                else if (Locale.Equals("cn") || Locale.Equals("zh"))
                {
                    sc_page_url = sc_page_url + "?locale=zh";
                }

                action.Navigate(driver, sc_page_url);
                action.ILoginViaLogInLink(driver, storedShippingAddressesLimit_username, storedShippingAddressesLimit_password);

                util.RemoveAllModelsInYourCartTable(driver);

                //--- Action: Get Test Data ---//
                string adi_model = util.GetAdiModel();

                //--- Action: User add product for purchase on the Cart ---//
                action.IAddModelToPurchase(driver, adi_model);

                //--- Action: Click the Checkout button ---//
                action.IClick(driver, Elements.Shopping_Cart_Checkout_Button);
                Thread.Sleep(3000);

                //----- ECommerce TestPlan > Test Case Title: Place a purchase order using account with 8 existing shipping address -----//

                //--- Expected Result: The message should be displayed showing that the user have exceeded the number of addresses: You’ve reached the maximum number of eight stored shipping addresses. Please select, edit, or delete a shipping address. ---//
                if (Locale.Equals("en"))
                {
                    test.validateStringIsCorrect(driver, Elements.Shopping_Cart_ShippingAddress_Message, "You’ve reached the maximum number of eight stored shipping addresses. Please select, edit, or delete a shipping address.");
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.Shopping_Cart_ShippingAddress_Message);
                }
            }
        }
    }
}