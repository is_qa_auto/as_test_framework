﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Threading;

namespace AS_Test_Framework.ShoppingCart
{
    [TestFixture]
    public class ShoppingCart_ShoppingCartPage_VerifyQuickLoginSection : BaseSetUp
    {
        public ShoppingCart_ShoppingCartPage_VerifyQuickLoginSection() : base() { }

        //--- Login Credentials ---//
        string biz_username = "marvin.bebe@analog.com";
        string biz_password = "Test_1234";

        [Test, Category("Shopping Cart"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that the Quick Login Section is Present and Working as Expected in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Quick Login Section is Present and Working as Expected in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Quick Login Section is Present and Working as Expected in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Quick Login Section is Present and Working as Expected in RU Locale")]
        public void ShoppingCart_VerifyQuickLoginSection(string Locale)
        {
            //--- Shopping Cart URL ---//
            string sc_page_url = Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx";
            if (Locale.Equals("jp") || Locale.Equals("ru"))
            {
                sc_page_url = sc_page_url + "?locale=" + Locale;
            }
            else if (Locale.Equals("cn") || Locale.Equals("zh"))
            {
                sc_page_url = sc_page_url + "?locale=zh";
            }

            action.Navigate(driver, sc_page_url);
            Thread.Sleep(1000);

            Assert.Multiple(() =>
            {
                //----- R4 > T1: Verify Quick login section -----//
                test.validateElementIsPresent(driver, Elements.SC_LoginLink);

                action.IClick(driver, Elements.SC_LoginLink);
                test.validateStringInstance(driver, driver.Url, "b2clogin.com");
                //-----------------------------------------------//
            });

            //----- R1 > T4: Verify if the Login Register link is not redirect to error page. -----//
            action.ILogin(driver, biz_username, biz_password);
            Thread.Sleep(1000);

            test.validateStringInstance(driver, driver.Url, sc_page_url);
        }
    }
}