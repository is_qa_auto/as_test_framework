﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Threading;

namespace AS_Test_Framework.ShoppingCart
{
    [TestFixture]
    public class ShoppingCart_BillingAddressPage_VerifyPreviousAndNextButtons : BaseSetUp
    {
        public ShoppingCart_BillingAddressPage_VerifyPreviousAndNextButtons() : base() { }

        //--- Login Credentials ---//
        string biz_username = "marvin.bebe@analog.com";
        string biz_password = "Test_1234";

        //--- URLs ---//
        string shippingInformation_url = "ShippingAddress.aspx";
        string editShippingInformation_url = "EditShippingAddress.aspx";
        string billingInformation_url = "BillingAddress.aspx";
        string projectDetails_url = "AboutYourProject.aspx";

        [Test, Category("Shopping Cart"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that the Previous and Next Buttons are Present and Working as Expected after Adding a Model for Purchase in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Previous and Next Buttons are Present and Working as Expected after Adding a Model for Purchase in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Previous and Next Buttons are Present and Working as Expected after Adding a Model for Purchase in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Previous and Next Buttons are Present and Working as Expected after Adding a Model for Purchase in RU Locale")]
        public void ShoppingCart_VerifyPreviousAndNextButtonsAfterAddingAModelForPurchase(string Locale)
        {
            //--- Shopping Cart URL ---//
            string sc_page_url = Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx";
            if (Locale.Equals("jp") || Locale.Equals("ru"))
            {
                sc_page_url = sc_page_url + "?locale=" + Locale;
            }
            else if (Locale.Equals("cn") || Locale.Equals("zh"))
            {
                sc_page_url = sc_page_url + "?locale=zh";
            }

            action.Navigate(driver, sc_page_url);
            action.ILoginViaLogInLink(driver, biz_username, biz_password);

            util.RemoveAllModelsInYourCartTable(driver);

            //--- Action: Get Test Data ---//
            string adi_model = util.GetAdiModel();

            action.IAddModelToPurchase(driver, adi_model);

            action.IClick(driver, Elements.Shopping_Cart_Checkout_Button);
            Thread.Sleep(3000);

            action.GoToBillingAddressPage(driver);

            if (driver.Url.Contains(billingInformation_url))
            {
                //----- R6 > T4: Verify the Previous button on the Billing Address page -----//

                //--- Action: Click on the Previous button ----//
                action.IClick(driver, Elements.Shopping_Cart_Prev_Button);

                //--- Expected Result: The page should be redirected to the Shipping Address page ----//
                if (driver.Url.Contains(editShippingInformation_url))
                {
                    if (Locale.Equals("cn") || Locale.Equals("zh"))
                    {
                        test.validateStringInstance(driver, driver.Url, "analog.com/" + editShippingInformation_url + "?locale=zh");
                    }
                    else
                    {
                        test.validateStringInstance(driver, driver.Url, "analog.com/" + editShippingInformation_url + "?locale=" + Locale);
                    }

                    //--- Click on the SHIP TO THIS ADDRESS Button ---//
                    action.IClick(driver, Elements.Shopping_Cart_ShippingAddress_ShipToThisEditedAdd_Btn);
                }
                else
                {
                    if (Locale.Equals("cn") || Locale.Equals("zh"))
                    {
                        test.validateStringInstance(driver, driver.Url, "analog.com/" + shippingInformation_url + "?locale=zh");
                    }
                    else
                    {
                        test.validateStringInstance(driver, driver.Url, "analog.com/" + shippingInformation_url + "?locale=" + Locale);
                    }

                    //--- Action: Click on the Next button ---//
                    action.IClick(driver, Elements.Shopping_Cart_Footer_Next_Button);
                }

                //----- R6 > T5: Verify the Next button on the Billing Address page -----//

                //--- Action: Complete all the mandatory fields on the Billing Address page ---//          
                //--- Action: Click on the Next button ---//
                action.IPopulateFieldsInBillingAddressPage(driver);

                //--- Expected Result: The Billing Address page should be displayed. ---//
                if (Locale.Equals("cn") || Locale.Equals("zh"))
                {
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + projectDetails_url + "?locale=zh");
                }
                else
                {
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + projectDetails_url + "?locale=" + Locale);
                }
            }
            else
            {
                //--- Validation if the Page is in the Billing Address Page ---//
                if (Locale.Equals("cn") || Locale.Equals("zh"))
                {
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + billingInformation_url + "?locale=zh");
                }
                else
                {
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + billingInformation_url + "?locale=" + Locale);
                }
            }
        }
    }
}