﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Threading;

namespace AS_Test_Framework.ShoppingCart
{
    [TestFixture]
    public class ShoppingCart_ShippingAddressPage_VerifyFieldsWithEuropeanCustomer : BaseSetUp
    {
        public ShoppingCart_ShippingAddressPage_VerifyFieldsWithEuropeanCustomer() : base() { }

        //--- Login Credentials ---//
        string european_username = "sc_t3st_g3rm4ny@mailinator.com";
        string european_password = "Test_1234";

        [Test, Category("Shopping Cart"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that the Fields are Present and Working as Expected after Adding a Model for Purchase using a European User in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Fields are Present and Working as Expected after Adding a Model for Purchase using a European User in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Fields are Present and Working as Expected after Adding a Model for Purchase using a European User in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Fields are Present and Working as Expected after Adding a Model for Purchase using a European User in RU Locale")]
        public void ShoppingCart_VerifyFieldsWithEuropeanCustomerAfterAddingAModelForPurchase(string Locale)
        {
            //--- Shopping Cart URL ---//
            string sc_page_url = Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx";
            if (Locale.Equals("jp") || Locale.Equals("ru"))
            {
                sc_page_url = sc_page_url + "?locale=" + Locale;
            }
            else if (Locale.Equals("cn") || Locale.Equals("zh"))
            {
                sc_page_url = sc_page_url + "?locale=zh";
            }

            action.Navigate(driver, sc_page_url);
            action.ILoginViaLogInLink(driver, european_username, european_password);

            util.RemoveAllModelsInYourCartTable(driver);

            //--- Action: Get Test Data ---//
            string adi_model = util.GetAdiModel();

            //--- Action: Enter a Product in the Model # textbox ---//
            //--- Action: Select Purchase radio button ---//
            //--- Action: Click the Add to Cart button ---//
            action.IAddModelToPurchase(driver, adi_model);

            //--- Action: Click the Checkout button ---//
            action.IClick(driver, Elements.Shopping_Cart_Checkout_Button);
            Thread.Sleep(3000);

            //----- ECommerce TestPlan > Test Case Title: Verify the VAT Number field (AL-1224) -----//

            //--- Expected Result: VAT Number field should be displayed as a mandatory field ---//
            test.validateElementIsPresent(driver, Elements.Shopping_Cart_VatNumber_Required_Icon);            
        }      
    }
}