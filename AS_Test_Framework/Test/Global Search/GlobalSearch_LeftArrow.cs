﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;

namespace AS_Test_Framework.GlobalSearch
{
    [TestFixture]
    public class GlobalSearch_LeftArrow : BaseSetUp
    {
        public GlobalSearch_LeftArrow() : base() { }

        //--- URLs ---//
        string searchResults_page_url = "/search.html";

        [Test, Category("Global Search"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Left Arrow is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Left Arrow is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Left Arrow is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Left Arrow is Present and Working as Expected in RU Locale")]
        public void GlobalSearch_VerifyLeftArrow(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + searchResults_page_url);
            action.ISearchViaGlobalSearchResultsTextbox(driver, "*");

            Assert.Multiple(() =>
            {
                action.IClick(driver, By.CssSelector("a[class='facet-heading']>span"));
                test.validateElementIsPresent(driver, Elements.GlobalSearchResults_LeftArrow);

                action.IClick(driver, Elements.GlobalSearchResults_LeftArrow);
                test.validateElementIsPresent(driver, Elements.GlobalSearchResults_Collapsed_View);
            });
        }
    }
}