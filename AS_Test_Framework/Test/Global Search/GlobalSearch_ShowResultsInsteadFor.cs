﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;

namespace AS_Test_Framework.GlobalSearch
{
    [TestFixture]
    public class GlobalSearch_ShowResultsInsteadFor : BaseSetUp
    {
        public GlobalSearch_ShowResultsInsteadFor() : base() { }

        //--- URLs ---//
        string searchResults_page_url = "/search.html";

        //--- Labels ---//
        string showingResultsFor_Txt = "Showing results for";
        string showResultsInsteadFor_Txt = "Show results instead for";

        [Test, Category("Global Search"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Show results instead for is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Show results instead for is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Show results instead for is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Show results instead for is Present and Working as Expected in RU Locale")]
        public void GlobalSearch_VerifyShowResultsInsteadFor(string Locale)
        {
            //--- Inputs ---//
            string search_input = "adsp-ac573";
            //string search_input = "AD77o5";

            action.Navigate(driver, Configuration.Env_Url + Locale + searchResults_page_url);

            action.ISearchViaGlobalSearchResultsTextbox(driver, search_input);

            //--- REQUIREMENT 22: "Did you mean?" functionality enhancement ---//
            Assert.Multiple(() =>
            {
                if (Locale.Equals("en"))
                {
                    test.validateString(driver, showingResultsFor_Txt, util.GetText(driver, Elements.GlobalSearchResults_ShowingResultsFor_Txt).Substring(0, util.GetText(driver, Elements.GlobalSearchResults_ShowingResultsFor_Txt).LastIndexOf(" ") < 0 ? 0 : util.GetText(driver, Elements.GlobalSearchResults_ShowingResultsFor_Txt).LastIndexOf(" ")));
                    test.validateString(driver, showResultsInsteadFor_Txt + " " + search_input, util.GetText(driver, Elements.GlobalSearchResults_ShowResultsInsteadFor_Txt));
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.GlobalSearchResults_ShowingResultsFor_Txt);
                    test.validateElementIsPresent(driver, Elements.GlobalSearchResults_ShowResultsInsteadFor_Txt);
                }
            });
        }
    }
}