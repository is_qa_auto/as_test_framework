﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;

namespace AS_Test_Framework.GlobalSearch
{
    [TestFixture]
    public class GlobalSearch_BaseballCard : BaseSetUp
    {
        public GlobalSearch_BaseballCard() : base() { }

        //--- URLs ---//
        string searchResults_page_url = "/search.html";
        string productLifeCycleInformation_page_url = "/product-life-cycle-information.html";

        [Test, Category("Global Search"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the PDP Baseball Card is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the PDP Baseball Card is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the PDP Baseball Card is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the PDP Baseball Card is Present and Working as Expected in RU Locale")]
        public void GlobalSearch_VerifyPdpBaseballCard(string Locale)
        {
            //--- Inputs ---//
            string search_input = "AD7705";
            //string search_input = "AD7706";

            action.Navigate(driver, Configuration.Env_Url + Locale + searchResults_page_url);

            action.ISearchViaGlobalSearchResultsTextbox(driver, search_input);

            Assert.Multiple(() =>
            {
                //--- R2 > T30: Search for product with single matches ---//
                test.validateElementIsPresent(driver, Elements.GlobalSearchResults_BaseballCard_ProductCat_Link);

                //--- R19 > T1: Verify that the Product Status is displayed in the Baseball card below the Product Description (AL-9871) ---//
                test.validateElementIsPresent(driver, Elements.GlobalSearchResults_BaseballCard_ProductStat_Icon);
                test.validateElementIsPresent(driver, Elements.GlobalSearchResults_BaseballCard_ProductStat_Link);

                test.validateElementIsPresent(driver, Elements.GlobalSearchResults_BaseballCard_Title_Link);
                test.validateElementIsPresent(driver, Elements.GlobalSearchResults_BaseballCard_ShortDesc_Txt);

                if (util.CheckElement(driver, Elements.GlobalSearchResults_BaseballCard_ProductStat_Link, 1))
                {
                    //--- Action: Click the Product Status ---//
                    action.IOpenLinkInNewTab(driver, Elements.GlobalSearchResults_BaseballCard_ProductStat_Link);

                    //--- Expected Result: The page should redirect to the Product Life Cycle Information page ---//
                    test.validateStringInstance(driver, driver.Url, productLifeCycleInformation_page_url);
                }
            });
        }

        [Test, Category("Global Search"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Obsolete PDP Baseball Card is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Obsolete PDP Baseball Card is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Obsolete PDP Baseball Card is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Obsolete PDP Baseball Card is Present and Working as Expected in RU Locale")]
        public void GlobalSearch_VerifyObsoletePdpBaseballCard(string Locale)
        {
            //--- Inputs ---//
            string search_input = "AD2040";

            action.Navigate(driver, Configuration.Env_Url + Locale + searchResults_page_url);
            action.ISearchViaGlobalSearchResultsTextbox(driver, search_input);

            Assert.Multiple(() =>
            {
                //--- R5 > T1: The obsolete product entered and its associated resources, quick links and warning status should appear as top search result in search result page highlighted in blue(baseball card) ---//
                test.validateElementIsPresent(driver, Elements.GlobalSearchResults_BaseballCard_ProductCat_Link);

                //--- R19 > T1: Verify that the Product Status is displayed in the Baseball card below the Product Description ---//
                test.validateElementIsPresent(driver, Elements.GlobalSearchResults_BaseballCard_ProductStat_Icon);
                test.validateElementIsPresent(driver, Elements.GlobalSearchResults_BaseballCard_ProductStat_Link);

                //--- R5 > T1: Resources section should not be shown. ---//
                test.validateElementIsNotPresent(driver, Elements.GlobalSearchResults_BaseballCard_Resources_Link);
            });
        }
    }
}