﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;

namespace AS_Test_Framework.GlobalSearch
{
    [TestFixture]
    public class GlobalSearch_SearchResultsTextbox : BaseSetUp
    {
        public GlobalSearch_SearchResultsTextbox() : base() { }

        //--- URLs ---//
        string searchResults_page_url = "/search.html";

        //--- Labels ---//
        string ghost_text = "Unified Search";

        [Test, Category("Global Search"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Ghost Text is Present and Correct in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Ghost Text is Present in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Ghost Text is Present in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Ghost Text is Present in RU Locale")]
        public void GlobalSearch_VerifyGhostText(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + searchResults_page_url);

            //--- Verify Ghost text (read-only text) ---//
            if (Locale.Equals("en"))
            {
                test.validateString(driver, ghost_text, util.ReturnAttribute(driver, Elements.GlobalSearchResults_Search_Txtbox, "placeholder"));
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.GlobalSearchResults_Search_Txtbox);
            }
        }

        [Test, Category("Global Search"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the X Button is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the X Button is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the X Button is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the X Button is Present and Working as Expected in RU Locale")]
        public void GlobalSearch_VerifyXButton(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + searchResults_page_url);

            Assert.Multiple(() =>
            {
                //--- Type a keyword in search bar ---//
                action.IDeleteValueOnFields(driver, Elements.GlobalSearchResults_Search_Txtbox);
                string search_input = "AD7960";
                action.IType(driver, Elements.GlobalSearchResults_Search_Txtbox, search_input);

                test.validateElementIsPresent(driver, Elements.GlobalSearchResults_X_Btn);

                //--- Clear the search by clicking (X). ---//
                action.IClick(driver, Elements.GlobalSearchResults_X_Btn);
                test.validateString(driver, string.Empty, util.ReturnAttribute(driver, Elements.GlobalSearchResults_Search_Txtbox, "value"));
            });
        }

        [Test, Category("Global Search"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Search Results Textbox is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Search Results Textbox is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Search Results Textbox is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Search Results Textbox is Present and Working as Expected in RU Locale")]
        public void GlobalSearch_VerifySearchResultsTextbox(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + searchResults_page_url);

            Assert.Multiple(() =>
            {
                //--- R2 > T6: Verify Search function in search result page by clicking search icon. ---//
                string search_input = "Circuit I & II Lab Activities, Interface & Isolation, Monitor, Control & Protection";
                action.ISearchViaGlobalSearchResultsTextbox(driver, search_input);
                test.validateString(driver, search_input, util.ReturnAttribute(driver, Elements.GlobalSearchResults_Search_Txtbox, "value"));

                //--- R2 > T7: Verify Search function in search result page by clicking search icon. ---//
                action.Navigate(driver, Configuration.Env_Url + Locale + searchResults_page_url);
                action.ISearchViaGlobalSearchResultsTextboxUsingEnterKey(driver, search_input);
                test.validateString(driver, search_input, util.ReturnAttribute(driver, Elements.GlobalSearchResults_Search_Txtbox, "value"));
            });
        }
    }
}