﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System;

namespace AS_Test_Framework.GlobalSearch
{
    [TestFixture]
    public class GlobalSearch_Pagination : BaseSetUp
    {
        public GlobalSearch_Pagination() : base() { }

        //--- URLs ---//
        string searchResults_page_url = "/search.html";

        [Test, Category("Global Search"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Pagination is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Pagination is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Pagination is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Pagination is Present and Working as Expected in RU Locale")]
        public void GlobalSearch_VerifyPagination(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + searchResults_page_url);

            //----- R2 > T11: Verify Pagination. -----//
            Assert.Multiple(() =>
            {
                string search_input = "*";
                action.ISearchViaGlobalSearchResultsTextbox(driver, search_input);

                //--- Navigate at the bottom of search result. ---//
                test.validateElementIsPresent(driver, Elements.GlobalSearchResults_Next_Btn);

                //--- Verify Previous link ---//
                test.validateElementIsNotPresent(driver, Elements.GlobalSearchResults_Prev_Btn);

                //--- Click Next link. ---//
                string page_selected = util.GetText(driver, Elements.GlobalSearchResults_PageSelected_Txt);
                action.IClick(driver, Elements.GlobalSearchResults_Next_Btn);
                test.validateString(driver, (Int32.Parse(page_selected) + 1).ToString(), util.GetText(driver, Elements.GlobalSearchResults_PageSelected_Txt));

                //--- Click Previous link ---//
                page_selected = util.GetText(driver, Elements.GlobalSearchResults_PageSelected_Txt);
                action.IClick(driver, Elements.GlobalSearchResults_Prev_Btn);
                test.validateString(driver, (Int32.Parse(page_selected) - 1).ToString(), util.GetText(driver, Elements.GlobalSearchResults_PageSelected_Txt));

                //--- Click on page number links. ---//
                string pageLink_clicked = util.GetText(driver, By.CssSelector("div[class='pagination-control pagination-top pull-right visible-desktop']>span:nth-last-child(2)>a"));
                action.IClick(driver, By.CssSelector("div[class='pagination-control pagination-top pull-right visible-desktop']>span:nth-last-child(2)>a"));
                test.validateString(driver, pageLink_clicked, util.GetText(driver, Elements.GlobalSearchResults_PageSelected_Txt));
            });
        }
    }
}