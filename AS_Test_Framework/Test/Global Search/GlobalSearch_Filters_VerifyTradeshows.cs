﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System;
using System.Text.RegularExpressions;

namespace AS_Test_Framework.GlobalSearch
{
    [TestFixture]
    public class GlobalSearch_Filters_VerifyTradeshows : BaseSetUp
    {
        public GlobalSearch_Filters_VerifyTradeshows() : base() { }

        //--- URLs ---//
        string searchResults_page_url = "/search.html";

        [Test, Category("Global Search"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Tradeshows Filter is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Tradeshows Filter is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Tradeshows Filter is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Tradeshows Filter is Present and Working as Expected in RU Locale")]
        public void GlobalSearch_VerifyTradeshowsFilter(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + searchResults_page_url);
            action.ISearchViaGlobalSearchResultsTextbox(driver, "*");

            if (util.CheckElement(driver, Elements.GlobalSearchResults_Tradeshows_Filter, 1))
            {
                Assert.Multiple(() =>
                {
                    //--- R3 > T1: The Level 2 category filters should be in collapse state by default. ---//
                    test.validateElementIsPresent(driver, Elements.GlobalSearchResults_Default_Collapsed_Tradeshows_Filter);

                    //--- R3 > T1: The Level 2 category filter should expand and show the list of different subset of filters associated with them. ---//
                    action.IClick(driver, Elements.GlobalSearchResults_Tradeshows_Filter_Btn);
                    test.validateElementIsPresent(driver, Elements.GlobalSearchResults_Expanded_Tradeshows_Filter);

                    //--- R2 > T4: Verify maximum filter item on each category. ---//
                    test.validateCountIsLessOrEqual(driver, 4, util.GetCount(driver, Elements.GlobalSearchResults_Tradeshows_SeeLess_Subfilters));

                    //--- R3 > T2: Should show the different subset of filters associated with them. ---//
                    test.validateElementIsPresent(driver, Elements.GlobalSearchResults_Tradeshows_Subfilter1);

                    //--- R3 > T2: Verify that the tally of available result for each subset filter should display next to them. ---// 
                    test.validateElementIsPresent(driver, Elements.GlobalSearchResults_Tradeshows_Subfilter_SearchTot_Txt);

                    //--- R2 > T4: Navigate to  See All link ---//
                    if (util.CheckElement(driver, Elements.GlobalSearchResults_Tradeshows_SeeAll_Link, 1))
                    {
                        //--- R2 > T4: Click See All link. ---//
                        int seeAll_link_count = Int32.Parse(Regex.Match(util.GetText(driver, Elements.GlobalSearchResults_Tradeshows_SeeAll_Link), @"\d+").Value);

                        action.IClick(driver, Elements.GlobalSearchResults_Tradeshows_SeeAll_Link);

                        test.validateElementIsPresent(driver, Elements.GlobalSearchResults_Tradeshows_Expanded_Subfilters);

                        test.validateCountIsEqual(driver, seeAll_link_count, util.GetCount(driver, Elements.GlobalSearchResults_Tradeshows_SeeAll_Subfilters));

                        test.validateElementIsPresent(driver, Elements.GlobalSearchResults_Tradeshows_SeeLess_Link);

                        //--- R2 > T4: Click Show Less ---//
                        action.IClick(driver, Elements.GlobalSearchResults_Tradeshows_SeeLess_Link);
                        test.validateElementIsPresent(driver, Elements.GlobalSearchResults_Tradeshows_SeeAll_Link);
                    }

                    //--- R2 > T4: Check filter to select. ---//
                    action.IClick(driver, Elements.GlobalSearchResults_Tradeshows_Subfilter1);
                    test.validateElementIsPresent(driver, Elements.GlobalSearchResults_SelectedFilter_Txt);

                    //--- R2 > T4: Cicking the (x) icon on should remove the filter. ---//
                    action.IClick(driver, Elements.GlobalSearchResults_SelectedFilter_X_Btn);
                    test.validateElementIsNotPresent(driver, Elements.GlobalSearchResults_SelectedFilter_Txt);

                    //--- R2 > T4: Clicking the filter category while in expand mode should collapse. ---//
                    action.IClick(driver, Elements.GlobalSearchResults_Tradeshows_Filter_Btn);
                    test.validateElementIsPresent(driver, Elements.GlobalSearchResults_Collapsed_Tradeshows_Filter);
                });
            }
        }
    }
}