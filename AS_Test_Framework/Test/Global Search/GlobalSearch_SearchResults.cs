﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;

namespace AS_Test_Framework.GlobalSearch
{
    [TestFixture]
    public class GlobalSearch_SearchResults : BaseSetUp
    {
        public GlobalSearch_SearchResults() : base() { }

        //--- URLs ---//
        string searchResults_page_url = "/search.html";

        //--- Labels ---//
        string invalidSearchResults_Txt_Line1 = "No results found.";
        string invalidSearchResults_Txt_Line2 = "Please modify your query.";
        string noSearchResults_Txt = "Please initiate new search";

        [Test, Category("Global Search"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Invalid Search Results Text is Present and Correct in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Invalid Search Results Text is Present in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Invalid Search Results Text is Present in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Invalid Search Results Text is Present in RU Locale")]
        public void GlobalSearch_VerifyInvalidSearchResults(string Locale)
        {
            //--- Inputs ---//
            string search_input = "!@#$%^&*()";
            //string search_input = "AD76699";

            action.Navigate(driver, Configuration.Env_Url + Locale + searchResults_page_url);

            action.ISearchViaGlobalSearchResultsTextbox(driver, search_input);

            Assert.Multiple(() =>
            {
                if (Locale.Equals("en"))
                {
                    test.validateString(driver, invalidSearchResults_Txt_Line1, util.GetText(driver, Elements.GlobalSearchResults_InvalidSearchResults_Txt_Line1));
                    test.validateString(driver, invalidSearchResults_Txt_Line2, util.GetText(driver, Elements.GlobalSearchResults_InvalidSearchResults_Txt_Line2));
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.GlobalSearchResults_InvalidSearchResults_Txt_Line1);
                    test.validateElementIsPresent(driver, Elements.GlobalSearchResults_InvalidSearchResults_Txt_Line2);
                }
            });
        }

        [Test, Category("Global Search"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the No Search Results Text is Present and Correct in EN Locale")]
        [TestCase("cn", TestName = "Verify that the No Search Results Text is Present in CN Locale")]
        [TestCase("jp", TestName = "Verify that the No Search Results Text is Present in JP Locale")]
        [TestCase("ru", TestName = "Verify that the No Search Results Text is Present in RU Locale")]
        public void GlobalSearch_VerifyNoSearchResults(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + searchResults_page_url);

            //----- Global Search Test Plan > Test Case Title: Verify Search in search result page with no Results found. -----//

            //--- Action: Search empty value on search bar. ---//
            string search_input = "";
            action.ISearchViaGlobalSearchResultsTextbox(driver, search_input);

            //--- Expected Result: The page should redirect to Search Results page with the following message: "Please initiate new search" ---//
            if (Locale.Equals("en"))
            {
                test.validateString(driver, noSearchResults_Txt, util.GetText(driver, Elements.GlobalSearchResults_NoSearchResults_Txt));
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.GlobalSearchResults_NoSearchResults_Txt);
            }
        }
    }
}