﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;

namespace AS_Test_Framework.GlobalSearch
{
    [TestFixture]
    public class GlobalSearch_Filters_VerifyCompanyVideos : BaseSetUp
    {
        public GlobalSearch_Filters_VerifyCompanyVideos() : base() { }

        //--- URLs ---//
        string searchResults_page_url = "/search.html?q=*";

        [Test, Category("Global Search"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Company Videos Filter is Present and Working as Expected in EN Locale", Category = "Smoke_ADIWeb")]
        [TestCase("cn", TestName = "Verify that the Company Videos Filter is Present and Working as Expected in CN Locale", Category = "Smoke_ADIWeb")]
        [TestCase("jp", TestName = "Verify that the Company Videos Filter is Present and Working as Expected in JP Locale", Category = "Smoke_ADIWeb")]
        [TestCase("ru", TestName = "Verify that the Company Videos Filter is Present and Working as Expected in RU Locale")]
        public void GlobalSearch_VerifyCompanyVideosFilter(string Locale)
        {
            string initial_steps = "1. Go to " + Configuration.Env_Url + Locale + searchResults_page_url;                               
            string scenario = "";

            action.INavigate(driver, Configuration.Env_Url + Locale + searchResults_page_url);
            //action.ISearchViaGlobalSearchResultsTextbox(driver, "*");

            if (util.CheckElement(driver, Elements.GlobalSearchResults_CompanyVideos_Filter, 1))
            {
                Assert.Multiple(() =>
                {
                //--- R3 > T1: The Level 2 category filters should be in collapse state by default. ---//
                scenario = "Verify that company videos filter in collapsed state is present";
                test.validateElementIsPresentv2(driver, Elements.GlobalSearchResults_Default_Collapsed_CompanyVideos_Filter, scenario, initial_steps);

                //--- R3 > T1: The Level 2 category filter should expand and show the list of different subset of filters associated with them. ---//

                initial_steps = initial_steps + "<br>2. Click Company Videos filter button";
                action.IClick(driver, Elements.GlobalSearchResults_CompanyVideos_Filter_Btn);

                scenario = "Verify that expanded company videos filter is present";
                test.validateElementIsPresentv2(driver, Elements.GlobalSearchResults_Expanded_CompanyVideos_Filter, scenario, initial_steps);

                //--- R3 > T2: Should show the different subset of filters associated with them. ---//
                scenario = "Verify that company videos subset filter is present";
                test.validateElementIsPresentv2(driver, Elements.GlobalSearchResults_CompanyVideos_Subfilter1, scenario, initial_steps);

                //--- R3 > T2: Verify that the tally of available result for each subset filter should display next to them. ---//
                scenario = "Verify that company videos subset filter count is present";
                test.validateElementIsPresentv2(driver, Elements.GlobalSearchResults_CompanyVideos_Subfilter_SearchTot_Txt, scenario, initial_steps);

                //--- R2 > T4: Check filter to select. ---//
                initial_steps = initial_steps + "<br>3. Select the 1st company sub filter";
                action.IClick(driver, Elements.GlobalSearchResults_CompanyVideos_Subfilter1);

                scenario = "Verify that selected filter will be displayed under the search textbox";
                test.validateElementIsPresentv2(driver, Elements.GlobalSearchResults_SelectedFilter_Txt, scenario, initial_steps);

                //--- R2 > T4: Cicking the (x) icon on should remove the filter. ---//
                action.IClick(driver, Elements.GlobalSearchResults_SelectedFilter_X_Btn);
                initial_steps = initial_steps + "<br>4. Click (x) button of the selected filter";

                scenario = "Verify that user can remove selected filter by clicking the (x) button";
                test.validateElementIsNotPresentv2(driver, Elements.GlobalSearchResults_SelectedFilter_Txt, scenario, initial_steps);

                //--- R2 > T4: Clicking the filter category while in expand mode should collapse. ---//
                action.IClick(driver, Elements.GlobalSearchResults_CompanyVideos_Filter_Btn);
                scenario = "Verify that user can collapsed the company videos filter button";
                initial_steps = "1. Go to " + Configuration.Env_Url + Locale + searchResults_page_url
                        +   "<br>2. Click company videos filter to expand" 
                        +   "<br>3. Click company videos again to collapsed";
                test.validateElementIsPresentv2(driver, Elements.GlobalSearchResults_Collapsed_CompanyVideos_Filter,scenario,initial_steps);
                });
            }
        }
    }
}