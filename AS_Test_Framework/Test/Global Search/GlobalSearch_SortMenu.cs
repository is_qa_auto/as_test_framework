﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;

namespace AS_Test_Framework.GlobalSearch
{
    [TestFixture]
    public class GlobalSearch_SortMenu : BaseSetUp
    {
        public GlobalSearch_SortMenu() : base() { }

        //--- URLs ---//
        string searchResults_page_url = "/search.html";

        //--- Labels ---//
        string sortMenu_defaultVal = "relevancy";

        [Test, Category("Global Search"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Sort Menu is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Sort Menu is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Sort Menu is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Sort Menu is Present and Working as Expected in RU Locale")]
        public void GlobalSearch_VerifySortMenu(string Locale)
        {
            //--- Inputs ---//
            string search_input = "adsp-ac573";
            //string search_input = "AD77o5";

            action.Navigate(driver, Configuration.Env_Url + Locale + searchResults_page_url);
            action.ISearchViaGlobalSearchResultsTextbox(driver, search_input);

            //----- AL-6759/T14_1: Verify the Sort Menu -----//
            Assert.Multiple(() =>
            {
                //--- Verify that the Sort menu is present on the page ---//
                test.validateElementIsPresent(driver, Elements.GlobalSearchResults_Sort_Menu);
                test.validateSelectedValueIsCorrect(driver, Elements.GlobalSearchResults_Sort_Menu, sortMenu_defaultVal);

                //--- Click on the Sort menu ---//
                test.validateElementIsPresent(driver, Elements.GlobalSearchResults_Sort_Menu_Newest);
                test.validateElementIsPresent(driver, Elements.GlobalSearchResults_Sort_Menu_Oldest);
                test.validateElementIsPresent(driver, Elements.GlobalSearchResults_Sort_Menu_Relevancy);
            });
        }
    }
}