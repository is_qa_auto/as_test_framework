﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Threading;

namespace AS_Test_Framework.GlobalSearch
{
    [TestFixture]
    public class GlobalSearch_GlobalSearchBar : BaseSetUp
    {
        public GlobalSearch_GlobalSearchBar() : base() { }

        //--- URLs ---//
        string corePage_url = "/index.html";
        string searchResults_page_url = "/search.html";

        //--- Labels ---//
        string noSearchResults_Txt_Line1 = "No results found.";
        string noSearchResults_Txt_Line2 = "Please modify your query.";

        [Test, Category("Global Search"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Global Search Bar - Active State is Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Global Search Bar - Active State is Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Global Search Bar - Active State is Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Global Search Bar - Active State is Working as Expected in RU Locale")]
        public void GlobalSearch_GlobalSearchBar_VerifyActiveState(string Locale)
        {
            //----- Global Search Test Plan > Desktop Tab > R1 > T1: Verify Global Search bar is present on header. -----//

            //--- Action: Navigate through the header part of the Homepage. ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + corePage_url);
            Thread.Sleep(250);

            //--- Action: Click on the Global Search Text Box ---//
            action.IClick(driver, Elements.GlobalSearchTextField);

            //--- Expected Result: The X Button will be displayed. ---//
            test.validateElementIsPresent(driver, Elements.GlobalSearch_ClearButton);

            //----- 21.2.1 Projects > Desktop-Onsite Search Redesign Tab > R1 > T11: Verify if manigfying glass is display in search bar. -----//

            //--- Expected Result: The magnifying glass button appears ---//
            test.validateElementIsPresent(driver, Elements.GlobalSearch_SearchIcon);
        }

        [Test, Category("Global Search"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Global Search Bar - Predictive Search is Present and Working as Expected after Entering 1 Character in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Global Search Bar - Predictive Search is Present and Working as Expected after Entering 1 Character in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Global Search Bar - Predictive Search is Present and Working as Expected after Entering 1 Character in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Global Search Bar - Predictive Search is Present and Working as Expected after Entering 1 Character in RU Locale")]
        public void GlobalSearch_GlobalSearchBar_VerifyPredictiveSearchAfterEntering1Character(string Locale)
        {
            //----- 21.2.1 Projects > Desktop-Onsite Search Redesign Tab > R2 > T4: Verify if the quick search results display by typing one character in search bar. -----//

            //--- Action: Go to analog pages ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + corePage_url);

            //--- Action: Type any thing on search bar of Quick search (ex. "A")---//
            string search_input = "A";
            action.IDeleteValueOnFields(driver, Elements.GlobalSearchTextField);
            action.IType(driver, Elements.GlobalSearchTextField, search_input);
            Thread.Sleep(1000);

            //--- Expected Result: quick search results are not displayed. ---//
            test.validateElementIsNotPresent(driver, Elements.GlobalSearch_Auto_Complete);
        }

        [Test, Category("Global Search"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Global Search Bar - Predictive Search is Present and Working as Expected for Product Inputs in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Global Search Bar - Predictive Search is Present and Working as Expected for Product Inputs in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Global Search Bar - Predictive Search is Present and Working as Expected for Product Inputs in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Global Search Bar - Predictive Search is Present and Working as Expected for Product Inputs in RU Locale")]
        public void GlobalSearch_GlobalSearchBar_VerifyPredictiveSearchForProductInputs(string Locale)
        {
            //--- Action: Access Homepage ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + corePage_url);

            //--- Action: Enter 3 Characters on the Global Search Text Box ---//
            string search_input = "AD7";
            action.IDeleteValueOnFields(driver, Elements.GlobalSearchTextField);
            action.IType(driver, Elements.GlobalSearchTextField, search_input);

            //----- Global Search Test Plan > Desktop Tab > R1 > T3: Verify predictive search in global search bar -----//

            //--- Expected Result: The predictive search will be displayed. ---//
            test.validateElementIsPresent(driver, Elements.GlobalSearch_Auto_Complete);

            //--- Expected Result: The predictive search result should contain up to a maximum of 5 records. ---//
            test.validateCountIsEqual(driver, 5, util.GetCount(driver, Elements.GlobalSearch_PredictiveSearch_Items));

            if (util.CheckElement(driver, Elements.GlobalSearch_Auto_Complete, 1))
            {
                //----- Global Search Test Plan > Test Case Title: Verify the background of Typing State of quick search results. -----//

                //--- Expected Result: The  background color is Background color: #F4FBFC ---//
                test.validateColorIsCorrect(driver, util.GetCssValue(driver, Elements.GlobalSearch_Auto_Complete, "background-color"), "#F4FBFC");

                //----- 21.2.1 Projects > Desktop-Onsite Search Redesign Tab > R2 > T6: Verify if the content type label are display in each quick search results. -----//

                //--- Expected Result: Content type displayed ---//
                test.validateElementIsPresent(driver, Elements.GlobalSearch_PredictiveSearch_Items_ContentType_Label);

                //----- 21.2.1 Projects > Desktop-Onsite Search Redesign Tab > R2 > T14: Verify the text style of Typing State of quick search results. -----//

                //--- Expected Result: Content Type: color: #636363 ---//
                test.validateColorIsCorrect(driver, util.GetCssValue(driver, Elements.GlobalSearch_PredictiveSearch_Items_ContentType_Label, "text-decoration-color"), "#636363");

                //----- 21.2.1 Projects > Desktop-Onsite Search Redesign Tab > R2 > T7: Verify if the Product name/ title are display in each quick search results. -----//

                //--- Expected Result: title displayed ---//
                test.validateElementIsPresent(driver, Elements.GlobalSearch_PredictiveSearch_Items_Title_Link);

                //----- 21.2.1 Projects > Desktop-Onsite Search Redesign Tab > R2 > T14: Verify the text style of Typing State of quick search results. -----//

                //--- Expected Result: Title: color: #009FBD ---//
                test.validateColorIsCorrect(driver, util.GetCssValue(driver, Elements.GlobalSearch_PredictiveSearch_Items_Title_Link, "text-decoration-color"), "#009FBD");

                //----- 21.2.1 Projects > Desktop-Onsite Search Redesign Tab > R2 > T8: Verify if the search description are display in each quick search results. -----//

                //--- Expected Result: the search description displayed ---//
                test.validateElementIsPresent(driver, Elements.GlobalSearch_PredictiveSearch_Items_ShortDescription_Text);

                //----- 21.2.1 Projects > Desktop-Onsite Search Redesign Tab > R2 > T14: Verify the text style of Typing State of quick search results. -----//

                //--- Expected Result: Description: #636363---//
                test.validateColorIsCorrect(driver, util.GetCssValue(driver, Elements.GlobalSearch_PredictiveSearch_Items_ShortDescription_Text, "text-decoration-color"), "#636363");

                ////----- Global Search Test Plan > Desktop Tab > R21 (AL-5677) > T2: Using the Search link in the Auto-suggest -----// --> Search Link was removed as part of the changes for Onsite Search Redesign (IQ-10997/AL-17289).

                ////--- Action: Hover to the Auto-suggest ---//
                //action.IMouseOverTo(driver, Elements.GlobalSearch_PredictiveSearch_Items);

                ////--- Click the Search link at the right side of the Auto-suggested item ---//
                //action.IClick(driver, Elements.GlobalSearch_PredictiveSearch_Search_Link);

                ////---Expected Result: User should be directed to the Search Result page-- -//
                //test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + searchResults_page_url);
            }
        }

        [Test, Category("Global Search"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Global Search Bar - Predictive Search is Present and Working as Expected for Non-product Inputs in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Global Search Bar - Predictive Search is Present and Working as Expected for Non-product Inputs in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Global Search Bar - Predictive Search is Present and Working as Expected for Non-product Inputs in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Global Search Bar - Predictive Search is Present and Working as Expected for Non-product Inputs in RU Locale")]
        public void GlobalSearch_GlobalSearchBar_VerifyPredictiveSearchForNonProductInputs(string Locale)
        {
            //--- Action: Go to analog pages ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + corePage_url);

            //----- 21.2.1 Projects > Desktop-Onsite Search Redesign Tab > R2 > T21: Verify if there's a search descriptions will display for all other content types -----//

            //--- Action: Type any thing on search bar of Quick search (ex. "Goldstein") ---//
            string search_input = "Goldstein";
            action.IDeleteValueOnFields(driver, Elements.GlobalSearchTextField);
            action.IType(driver, Elements.GlobalSearchTextField, search_input);

            //----- 21.2.1 Projects > Desktop-Onsite Search Redesign Tab > R2 > T21: Verify if there's a search descriptions will display for all other content types -----//

            //--- Expected Result: No search descriptions will display for all other content types ---//
            test.validateElementIsNotPresent(driver, Elements.GlobalSearch_PredictiveSearch_Items_ShortDescription_Text);
        }

        [Test, Category("Global Search"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Search Using the Search Icon is Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Search Using the Search Icon is Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Search Using the Search Icon is Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Search Using the Search Icon is Working as Expected in RU Locale")]
        public void GlobalSearch_GlobalSearchBar_VerifySearchUsingSearchIcon(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + corePage_url);

            string search_input = "!@#$%^&*()";
            //string search_input = "AD76699";
            action.ISearchViaGlobalSearchTextbox(driver, search_input);

            Assert.Multiple(() =>
            {
                //----- R1 > T6: Verify global Search with no Results found. -----//                
                if (Locale.Equals("en"))
                {
                    test.validateString(driver, noSearchResults_Txt_Line1, util.GetText(driver, Elements.GlobalSearchResults_InvalidSearchResults_Txt_Line1));
                    test.validateString(driver, noSearchResults_Txt_Line2, util.GetText(driver, Elements.GlobalSearchResults_InvalidSearchResults_Txt_Line2));
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.GlobalSearchResults_InvalidSearchResults_Txt_Line1);
                    test.validateElementIsPresent(driver, Elements.GlobalSearchResults_InvalidSearchResults_Txt_Line2);
                }
                //----------------------------------------------------------------//

                //----- R21 (AL-5677) > T2: Using the Search link in the Auto-suggest -----//

                test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + searchResults_page_url);

                //-------------------------------------------------------------------------//

                //----- AL-11987/IQ-4377 -----// 

                test.validateElementIsPresent(driver, Elements.AnalogLog);
                test.validateElementIsPresent(driver, Elements.GlobalSearchTextField);

                //----------------------------//
            });
        }

        [Test, Category("Global Search"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Search Using the Enter Key is Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Search Using the Enter Key is Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Search Using the Enter Key is Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Search Using the Enter Key is Working as Expected in RU Locale")]
        public void GlobalSearch_GlobalSearchBar_VerifySearchUsingEnterKey(string Locale)
        {
            //----- 21.2.1 Projects > Desktop-Onsite Search Redesign Tab > R3 > T2: Verify if hitting the enter in search result redirected to Result page. -----//

            //--- Action: Go to analog pages ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + corePage_url);

            //--- Action: Type any thing on search bar of Quick search (ex. "ADXL" ) ---//
            string search_input = "ADXL";
            action.IDeleteValueOnFields(driver, Elements.GlobalSearchResults_Search_Txtbox);
            action.IType(driver, Elements.GlobalSearchTextField, search_input);

            //--- Action: Hit the enter ---//
            Console.WriteLine("Press Enter Key");
            driver.FindElement(Elements.GlobalSearch_SearchIcon).SendKeys(Keys.Enter);

            //--- Expected Result: Page redirected to result page. ---//
            test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + searchResults_page_url);
        }
    }
}