﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;

namespace AS_Test_Framework.PST
{
    [TestFixture]
    public class PST_HeaderAndFooter_PstTitle : BaseSetUp
    {
        public PST_HeaderAndFooter_PstTitle() : base() { }

        //--- URLs ---//
        string pst_page_url = "/parametricsearch/11470";

        [Test, Category("PST"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the PST Title is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the PST Title is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the PST Title is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the PST Title is Present and Working as Expected in RU Locale")]
        public void PST_HeaderAndFooter_VerifyPstTitle(string Locale)
        {
            //--- Action: Access any Dynamic PST page. ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + pst_page_url);

            //----- PST Front End TestPlan - Redesign > Test Case Tab > R2 > TC4: Verify PST Title -----//

            //--- Expected Result: The title is displayed ---//
            test.validateElementIsPresent(driver, Elements.PST_Title_Txt);

            if (util.CheckElement(driver, Elements.PST_Title_Txt, 1))
            {
                //--- Expected Result: The title is the same as the last link in the breadcrumb ---//
                test.validateStringIsCorrect(driver, Elements.PST_Breadcrumb_CurrentPage_Txt, util.GetText(driver, Elements.PST_Title_Txt));
            }
        }
    }
}