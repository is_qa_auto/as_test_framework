﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;

namespace AS_Test_Framework.PST
{
    [TestFixture]
    public class PST_TableNavigationMainButtons_QuickTipsButton : BaseSetUp
    {
        public PST_TableNavigationMainButtons_QuickTipsButton() : base() { }

        //--- URLs ---//
        string pst_page_url = "/parametricsearch/11470";

        //--- Labels ----//
        string welcomeToQuickTipsTour_txt = "Welcome to Quick Tips Tour";
        string selectAllParameters_txt = "Select All Parameters";
        string shareFilteredTable_txt = "Share Filtered Table";
        string sortColumns_txt = "Sort Columns";
        string reorderColumns_txt = "Reorder Columns";
        string hideColumns_txt = "Hide Columns";
        string oneClickView_txt = "One Click View";
        string compareParts_txt = "Compare Parts";
        string productDetailsHover_txt = "Product Details Hover";

        [Test, Category("PST"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the X Button is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the X Button is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the X Button is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the X Button is Present and Working as Expected in RU Locale")]
        public void PST_TableNavigationMainButtons_QuickTips_VerifyXButton(string Locale)
        {
            //--- Action: Access any Dynamic PST page. ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + pst_page_url);

            //----- PST Front End TestPlan - Redesign > Test Case Tab > R4 > TC8: Verify "Quick Tips" button -----//

            //--- Expected Result: The Quick Tips Button is present ---//
            test.validateElementIsPresent(driver, Elements.PST_QuickTips_Button);

            //--- Action: Click Quick Tips button ---//
            action.IClick(driver, Elements.PST_QuickTips_Button);

            //--- Action: Click the "x" icon in the dialog box ---//
            action.IClick(driver, Elements.PST_QuickTips_DialogBox_X_Button);

            //--- Expected Result: Quick tips dialog box should disappear ---//
            test.validateElementIsNotPresent(driver, Elements.PST_QuickTips_DialogBox);
        }

        [Test, Category("PST"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Next Button is Present and Working as Expected in EN Locale")]
        public void PST_TableNavigationMainButtons_QuickTips_VerifyNextButton(string Locale)
        {
            //--- Action: Access any Dynamic PST page. ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + pst_page_url);

            if (Locale.Equals("en"))
            {
                //----- PST Front End TestPlan - Redesign > Test Case Tab > R4 > TC8: Verify "Quick Tips" button -----//

                //--- Action: Click Quick Tips button ---//
                action.IClick(driver, Elements.PST_QuickTips_Button);

                //--- Expected Result: "Welcome to Quick Tips Tour" dialog box should be displayed ---//
                test.validateStringIsCorrect(driver, Elements.PST_QuickTips_DialogBox_Header_Label, welcomeToQuickTipsTour_txt);

                //--- Action: Click Next button ---//
                action.IClick(driver, Elements.PST_QuickTips_DialogBox_Next_Button);

                //--- Expected Result: "Select All Parameters" dialog box should be displayed ---//
                test.validateStringIsCorrect(driver, Elements.PST_QuickTips_DialogBox_Header_Label, selectAllParameters_txt);

                //--- Action: Click Next button ---//
                action.IClick(driver, Elements.PST_QuickTips_DialogBox_Next_Button);

                //--- Expected Result: "Shared Filtered Table" dialog box should be displayed ---//
                test.validateStringIsCorrect(driver, Elements.PST_QuickTips_DialogBox_Header_Label, shareFilteredTable_txt);

                //--- Action: Click Next button ---//
                action.IClick(driver, Elements.PST_QuickTips_DialogBox_Next_Button);

                //--- Expected Result: Sort Column dialog box should be displayed ---//
                test.validateStringIsCorrect(driver, Elements.PST_QuickTips_DialogBox_Header_Label, sortColumns_txt);

                //--- Action: Click Next button ---//
                action.IClick(driver, Elements.PST_QuickTips_DialogBox_Next_Button);

                //--- Expected Result: Reorder Columns dialog box should be displayed ---//
                test.validateStringIsCorrect(driver, Elements.PST_QuickTips_DialogBox_Header_Label, reorderColumns_txt);

                //--- Action: Click Next button ---//
                action.IClick(driver, Elements.PST_QuickTips_DialogBox_Next_Button);

                //--- Expected Result: Hide Columns dialog box should be displayed ---//
                test.validateStringIsCorrect(driver, Elements.PST_QuickTips_DialogBox_Header_Label, hideColumns_txt);

                //--- Action: Click Next button ---//
                action.IClick(driver, Elements.PST_QuickTips_DialogBox_Next_Button);

                //--- Expected Result: "One Click View" dialog box should be displayed ---//
                test.validateStringIsCorrect(driver, Elements.PST_QuickTips_DialogBox_Header_Label, oneClickView_txt);

                //--- Action: Click Next button ---//
                action.IClick(driver, Elements.PST_QuickTips_DialogBox_Next_Button);

                //--- Expected Result: Compare Parts dialog box should be displayed ---//
                test.validateStringIsCorrect(driver, Elements.PST_QuickTips_DialogBox_Header_Label, compareParts_txt);

                //--- Action: Click Next button ---//
                action.IClick(driver, Elements.PST_QuickTips_DialogBox_Next_Button);

                //--- Expected Result: Product Details Hover dialog box should be displayed ---//
                test.validateStringIsCorrect(driver, Elements.PST_QuickTips_DialogBox_Header_Label, productDetailsHover_txt);
            }
        }

        [Test, Category("PST"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Previous Button is Present and Working as Expected in EN Locale")]
        public void PST_TableNavigationMainButtons_QuickTips_VerifyPreviousButton(string Locale)
        {
            //--- Action: Access any Dynamic PST page. ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + pst_page_url);

            if (Locale.Equals("en"))
            {
                //----- PST Front End TestPlan - Redesign > Test Case Tab > R4 > TC8: Verify "Quick Tips" button -----//

                //--- Action: Click Quick Tips button ---//
                action.IClick(driver, Elements.PST_QuickTips_Button);

                //--- Expected Result: "Welcome to Quick Tips Tour" dialog box should be displayed ---//
                test.validateStringIsCorrect(driver, Elements.PST_QuickTips_DialogBox_Header_Label, welcomeToQuickTipsTour_txt);

                //--- Action: Click Next button ---//
                action.IClick(driver, Elements.PST_QuickTips_DialogBox_Next_Button);

                //--- Expected Result: "Select All Parameters" dialog box should be displayed ---//
                test.validateStringIsCorrect(driver, Elements.PST_QuickTips_DialogBox_Header_Label, selectAllParameters_txt);

                //--- Action: Click Next button ---//
                action.IClick(driver, Elements.PST_QuickTips_DialogBox_Next_Button);

                //--- Expected Result: "Shared Filtered Table" dialog box should be displayed ---//
                test.validateStringIsCorrect(driver, Elements.PST_QuickTips_DialogBox_Header_Label, shareFilteredTable_txt);

                //--- Action: Click Click the Previous button ---//
                action.IClick(driver, Elements.PST_QuickTips_DialogBox_Previous_Button);

                //--- Expected Result: "Select All Parameters" dialog box should be displayed ---//
                test.validateStringIsCorrect(driver, Elements.PST_QuickTips_DialogBox_Header_Label, selectAllParameters_txt);
            }
        }

        [Test, Category("PST"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Last Button is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Last Button is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Last Button is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Last Button is Present and Working as Expected in RU Locale")]
        public void PST_TableNavigationMainButtons_QuickTips_VerifyLastButton(string Locale)
        {
            //--- Action: Access any Dynamic PST page. ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + pst_page_url);

                //----- PST Front End TestPlan - Redesign > Test Case Tab > R4 > TC8: Verify "Quick Tips" button -----//

                //--- Action: Click Quick Tips button ---//
                action.IClick(driver, Elements.PST_QuickTips_Button);

                //--- Action: Click Next button ---//
                action.IClick(driver, Elements.PST_QuickTips_DialogBox_Next_Button);

                //--- Action: Click Next button ---//
                action.IClick(driver, Elements.PST_QuickTips_DialogBox_Next_Button);

                //--- Action: Click Next button ---//
                action.IClick(driver, Elements.PST_QuickTips_DialogBox_Next_Button);

                //--- Action: Click Next button ---//
                action.IClick(driver, Elements.PST_QuickTips_DialogBox_Next_Button);

                //--- Action: Click Next button ---//
                action.IClick(driver, Elements.PST_QuickTips_DialogBox_Next_Button);

                //--- Action: Click Next button ---//
                action.IClick(driver, Elements.PST_QuickTips_DialogBox_Next_Button);

                //--- Action: Click Next button ---//
                action.IClick(driver, Elements.PST_QuickTips_DialogBox_Next_Button);

                //--- Action: Click Next button ---//
                action.IClick(driver, Elements.PST_QuickTips_DialogBox_Next_Button);

                //--- Action: Click Last button ---//
                action.IClick(driver, Elements.PST_QuickTips_DialogBox_Last_Button);

                //--- Expected Result: Quick tips dialog box should disappear ---//
                test.validateElementIsNotPresent(driver, Elements.PST_QuickTips_DialogBox);
        }
    }
}