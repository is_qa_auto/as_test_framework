﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using OpenQA.Selenium;
using System.Threading;

namespace AS_Test_Framework.PST
{
    [TestFixture]
    public class PST_QuickSearch_QuickPowerSearch_Input_InputBoxFilters : BaseSetUp
    {
        public PST_QuickSearch_QuickPowerSearch_Input_InputBoxFilters() : base() { }

        //--- URLs ---//
        string pst_page_url = "/parametricsearch/11436";

        //--- Labels ---//
        string vinMin_errorMessage = "Error: Vin(Min) is required";
        string vinMax_errorMessage = "Error: Vin(Max) is required";

        [Test, Category("PST"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Vin Min Input Box is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Vin Min Input Box is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Vin Min Input Box is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Vin Min Input Box is Present and Working as Expected in RU Locale")]
        public void PST_QuickSearch_QuickPowerSearch_Input_InputBoxFilters_VerifyVinMinInputBox(string Locale)
        {
            //--- Action: Access Dynamic PST with a Quick Search Tool ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + pst_page_url);

            //----- PST Front End TestPlan - Redesign > Test Case Tab > R3 > TC1: Verify fields are present -----//

            //--- Expected Result: The Vin(Min) textbox is present ---//
            test.validateElementIsPresent(driver, Elements.PST_QuickPowerSearch_VinMin_InputBox);

            if (util.CheckElement(driver, Elements.PST_QuickPowerSearch_VinMin_InputBox, 1))
            {
                //----- PST Front End TestPlan - Redesign > Test Case Tab > R3 > TC2: Verify Inputs -----//

                //--- Action: Enter valid inputs in the Vin(Min) textbox ---//
                string vinMin_valid_input = "3.4";
                action.IDeleteValueOnFields(driver, Elements.PST_QuickPowerSearch_VinMin_InputBox);
                action.IType(driver, Elements.PST_QuickPowerSearch_VinMin_InputBox, vinMin_valid_input);
                action.IType(driver, Elements.PST_QuickPowerSearch_VinMin_InputBox, Keys.Tab);

                //--- Expected Result: Entered inputs should be appear in the Vin(Min) textbox ---//
                test.validateString(driver, vinMin_valid_input, util.GetInputBoxValue(driver, Elements.PST_QuickPowerSearch_VinMin_InputBox));

                //----- PST Front End TestPlan - Redesign > Test Case Tab > R3 > TC2: Verify Inputs -----//

                //--- Action: Enter invalid inputs in the Vin(Min) textbox ---//
                string vinMin_invalid_input = "lttr";
                action.IDeleteValueOnFields(driver, Elements.PST_QuickPowerSearch_VinMin_InputBox);
                action.IType(driver, Elements.PST_QuickPowerSearch_VinMin_InputBox, vinMin_invalid_input);
                action.IType(driver, Elements.PST_QuickPowerSearch_VinMin_InputBox, Keys.Tab);

                //--- Expected Result: Entered invalid inputs should be cleared out ---//
                test.validateString(driver, "", util.GetInputBoxValue(driver, Elements.PST_QuickPowerSearch_VinMin_InputBox));

                //----- PST Front End TestPlan - Redesign > Test Case Tab > R3 > TC4.2: Verify Search with blank inputs -----//

                //--- INPUT (textbox) ---//

                //--- Action: Enter valid input in the Vin(Max) textbox ---//
                string vinMax_input = "40";
                action.IDeleteValueOnFields(driver, Elements.PST_QuickPowerSearch_VinMax_InputBox);
                action.IType(driver, Elements.PST_QuickPowerSearch_VinMax_InputBox, vinMax_input);
                action.IType(driver, Elements.PST_QuickPowerSearch_VinMax_InputBox, Keys.Tab);

                //--- OUTPUT (textbox) ---//

                //--- Action: Enter valid input in the Vout textbox ---//
                string vOut_input = "970m";
                action.IDeleteValueOnFields(driver, Elements.PST_QuickPowerSearch_VOut_InputBox);
                action.IType(driver, Elements.PST_QuickPowerSearch_VOut_InputBox, vOut_input);
                action.IType(driver, Elements.PST_QuickPowerSearch_VOut_InputBox, Keys.Tab);

                //--- Action: Enter valid input in the Iout textbox ---//
                string iOut_input = "2.5";
                action.IDeleteValueOnFields(driver, Elements.PST_QuickPowerSearch_IOut_InputBox);
                action.IType(driver, Elements.PST_QuickPowerSearch_IOut_InputBox, iOut_input);
                action.IType(driver, Elements.PST_QuickPowerSearch_IOut_InputBox, Keys.Tab);

                action.IDeleteValueOnFields(driver, Elements.PST_QuickPowerSearch_VinMin_InputBox);
                action.IType(driver, Elements.PST_QuickPowerSearch_VinMin_InputBox, Keys.Tab);

                //--- Action: Click Search button ---//
                Thread.Sleep(2000);
                action.IClick(driver, Elements.PST_QuickPowerSearch_Search_Button);

                //--- Expected Result: Error message should be displayed ---//
                if (Locale.Equals("en"))
                {
                    test.validateStringIsCorrect(driver, Elements.PST_QuickPowerSearch_ErrorMessage, vinMin_errorMessage);
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.PST_QuickPowerSearch_ErrorMessage);
                }
            }
        }

        [Test, Category("PST"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Vin Max Input Box is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Vin Max Input Box is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Vin Max Input Box is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Vin Max Input Box is Present and Working as Expected in RU Locale")]
        public void PST_QuickSearch_QuickPowerSearch_Input_InputBoxFilters_VerifyVinMaxInputBox(string Locale)
        {
            //--- Action: Access Dynamic PST with a Quick Search Tool ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + pst_page_url);

            //----- PST Front End TestPlan - Redesign > Test Case Tab > R3 > TC1: Verify fields are present -----//

            //--- Expected Result: The Vin(Max) textbox is present ---//
            test.validateElementIsPresent(driver, Elements.PST_QuickPowerSearch_VinMax_InputBox);

            if (util.CheckElement(driver, Elements.PST_QuickPowerSearch_VinMax_InputBox, 1))
            {
                //----- PST Front End TestPlan - Redesign > Test Case Tab > R3 > TC2: Verify Inputs -----//

                //--- Action: Enter valid inputs in the Vin(Max) textbox ---//
                string vinMax_valid_input = "40";
                action.IDeleteValueOnFields(driver, Elements.PST_QuickPowerSearch_VinMax_InputBox);
                action.IType(driver, Elements.PST_QuickPowerSearch_VinMax_InputBox, vinMax_valid_input);
                action.IType(driver, Elements.PST_QuickPowerSearch_VinMax_InputBox, Keys.Tab);

                //--- Expected Result: Entered inputs should be appear in the Vin(Max) textbox ---//
                test.validateString(driver, vinMax_valid_input, util.GetInputBoxValue(driver, Elements.PST_QuickPowerSearch_VinMax_InputBox));

                //----- PST Front End TestPlan - Redesign > Test Case Tab > R3 > TC2: Verify Inputs -----//

                //--- Action: Enter invalid inputs in the Vin(Max) textbox ---//
                string vinMax_invalid_input = "1,11";
                action.IDeleteValueOnFields(driver, Elements.PST_QuickPowerSearch_VinMax_InputBox);
                action.IType(driver, Elements.PST_QuickPowerSearch_VinMax_InputBox, vinMax_invalid_input);
                action.IType(driver, Elements.PST_QuickPowerSearch_VinMax_InputBox, Keys.Tab);

                //--- Expected Result: Entered invalid inputs should be cleared out ---//
                test.validateString(driver, "", util.GetInputBoxValue(driver, Elements.PST_QuickPowerSearch_VinMax_InputBox));

                //----- PST Front End TestPlan - Redesign > Test Case Tab > R3 > TC4.2: Verify Search with blank inputs -----//

                //--- INPUT (textbox) ---//

                //--- Action: Enter valid input in the Vin(Min) textbox---//
                string vinMin_input = "3.4";
                action.IDeleteValueOnFields(driver, Elements.PST_QuickPowerSearch_VinMin_InputBox);
                action.IType(driver, Elements.PST_QuickPowerSearch_VinMin_InputBox, vinMin_input);
                action.IType(driver, Elements.PST_QuickPowerSearch_VinMin_InputBox, Keys.Tab);

                //--- OUTPUT (textbox) ---//

                //--- Action: Enter valid input in the Vout textbox ---//
                string vOut_input = "970m";
                action.IDeleteValueOnFields(driver, Elements.PST_QuickPowerSearch_VOut_InputBox);
                action.IType(driver, Elements.PST_QuickPowerSearch_VOut_InputBox, vOut_input);
                action.IType(driver, Elements.PST_QuickPowerSearch_VOut_InputBox, Keys.Tab);

                //--- Action: Enter valid input in the Iout textbox ---//
                string iOut_input = "2.5";
                action.IDeleteValueOnFields(driver, Elements.PST_QuickPowerSearch_IOut_InputBox);
                action.IType(driver, Elements.PST_QuickPowerSearch_IOut_InputBox, iOut_input);
                action.IType(driver, Elements.PST_QuickPowerSearch_IOut_InputBox, Keys.Tab);

                action.IDeleteValueOnFields(driver, Elements.PST_QuickPowerSearch_VinMax_InputBox);
                action.IType(driver, Elements.PST_QuickPowerSearch_VinMax_InputBox, Keys.Tab);

                //--- Action: Click Search button ---//
                Thread.Sleep(2000);
                action.IClick(driver, Elements.PST_QuickPowerSearch_Search_Button);

                //--- Expected Result: Error message should be displayed ---//
                if (Locale.Equals("en"))
                {
                    test.validateStringIsCorrect(driver, Elements.PST_QuickPowerSearch_ErrorMessage, vinMax_errorMessage);
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.PST_QuickPowerSearch_ErrorMessage);
                }
            }
        }
    }
}