﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using OpenQA.Selenium;
using System.Threading;

namespace AS_Test_Framework.PST
{
    [TestFixture]
    public class PST_QuickSearch_QuickPowerSearch : BaseSetUp
    {
        public PST_QuickSearch_QuickPowerSearch() : base() { }

        //--- URLs ---//
        string pst_page_url = "/parametricsearch/11436";

        [Test, Category("PST"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Search Button is Present and Working as Expected after Entering Valid Inputs in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Search Button is Present and Working as Expected after Entering Valid Inputs in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Search Button is Present and Working as Expected after Entering Valid Inputs in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Search Button is Present and Working as Expected after Entering Valid Inputs in RU Locale")]
        public void PST_QuickSearch_QuickPowerSearch_VerifySearchButtonAfterEnteringValidInputs(string Locale)
        {
            //--- Action: Access Dynamic PST with a Quick Search Tool ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + pst_page_url);

            //----- PST Front End TestPlan - Redesign > Test Case Tab > R3 > TC4.3: Verify Search with multiple Outputs -----//

            //--- INPUT (textbox) ---//

            //--- Action: Enter valid input in the Vin(Min) textbox---//
            string vinMin_valid_input = "3.4";
            action.IDeleteValueOnFields(driver, Elements.PST_QuickPowerSearch_VinMin_InputBox);
            action.IType(driver, Elements.PST_QuickPowerSearch_VinMin_InputBox, vinMin_valid_input);
            action.IType(driver, Elements.PST_QuickPowerSearch_VinMin_InputBox, Keys.Tab);

            //--- Action: Enter valid input in the Vin(Max) textbox ---//
            string vinMax_valid_input = "40";
            action.IDeleteValueOnFields(driver, Elements.PST_QuickPowerSearch_VinMax_InputBox);
            action.IType(driver, Elements.PST_QuickPowerSearch_VinMax_InputBox, vinMax_valid_input);
            action.IType(driver, Elements.PST_QuickPowerSearch_VinMax_InputBox, Keys.Tab);

            //--- OUTPUT (textbox) ---//

            //--- Action: Enter valid input in the Vout textbox ---//
            string vOut_valid_input = "970m";
            action.IDeleteValueOnFields(driver, Elements.PST_QuickPowerSearch_VOut_InputBox);
            action.IType(driver, Elements.PST_QuickPowerSearch_VOut_InputBox, vOut_valid_input);
            action.IType(driver, Elements.PST_QuickPowerSearch_VOut_InputBox, Keys.Tab);

            //--- Action: Enter valid input in the Iout textbox ---//
            string iOut_valid_input = "2.5";
            action.IDeleteValueOnFields(driver, Elements.PST_QuickPowerSearch_IOut_InputBox);
            action.IType(driver, Elements.PST_QuickPowerSearch_IOut_InputBox, iOut_valid_input);
            action.IType(driver, Elements.PST_QuickPowerSearch_IOut_InputBox, Keys.Tab);

            //--- Action: Click Search button ---//
            Thread.Sleep(2000);
            action.IClick(driver, Elements.PST_QuickPowerSearch_Search_Button);

            //--- Expected Result: Entered values in Vin(Min) textbox should reflect on the filter value in the Vin (min | V) Column - Max textbox (AUAT-1584) ---//
            test.validateString(driver, util.ConverterToMetric(vinMin_valid_input).ToString(), util.GetInputBoxValue(driver, Elements.PST_Vin_min_V_Column_Max_InputBox));

            //--- Expected Result: Entered values in Vin(Max) textbox should reflect on the filter value in the Vin (max | V) Column - Min textbox (AUAT-1584) ---//
            test.validateString(driver, util.ConverterToMetric(vinMax_valid_input).ToString(), util.GetInputBoxValue(driver, Elements.PST_Vin_max_V_Column_Min_InputBox));

            //--- Expected Result: Entered values in Vout textbox should reflect on the filter value in the Vout Min Column - Max textbox and Vout Max - Min textbox (AUAT-1584) ---//
            test.validateString(driver, util.ConverterToMetric(vOut_valid_input).ToString(), util.GetInputBoxValue(driver, Elements.PST_VOutMin_Column_Max_InputBox));
            test.validateString(driver, util.ConverterToMetric(vOut_valid_input).ToString(), util.GetInputBoxValue(driver, Elements.PST_VOutMax_Column_Min_InputBox));

            //--- Expected Result: Entered values in Iout textbox should reflect on the filter value in the Output Current Column - Min textbox (AUAT-1584) ---//
            test.validateString(driver, util.ConverterToMetric(iOut_valid_input).ToString(), util.GetInputBoxValue(driver, Elements.PST_OutputCurrent_Column_Min_InputBox));
        }

        [Test, Category("PST"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Search Button is Present and Working as Expected after Entering Blank Inputs in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Search Button is Present and Working as Expected after Entering Blank Inputs in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Search Button is Present and Working as Expected after Entering Blank Inputs in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Search Button is Present and Working as Expected after Entering Blank Inputs in RU Locale")]
        public void PST_QuickSearch_QuickPowerSearch_VerifySearchButtonAfterEnteringBlankInputs(string Locale)
        {
            //--- Action: Access Dynamic PST with a Quick Search Tool ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + pst_page_url);

            //----- PST Front End TestPlan - Redesign > Test Case Tab > R3 > TC4.2: Verify Search with blank inputs -----//

            //--- INPUT (textbox) ---//

            //--- Action: Leave the Vin(Min) textbox blank ---//
            action.IDeleteValueOnFields(driver, Elements.PST_QuickPowerSearch_VinMin_InputBox);
            action.IType(driver, Elements.PST_QuickPowerSearch_VinMin_InputBox, Keys.Tab);

            //--- Action: Leave the Vin(Max) textbox blank ---//
            action.IDeleteValueOnFields(driver, Elements.PST_QuickPowerSearch_VinMax_InputBox);
            action.IType(driver, Elements.PST_QuickPowerSearch_VinMax_InputBox, Keys.Tab);

            //--- OUTPUT (textbox) ---//

            //--- Action: Leave the Vout textbox blank ---//
            action.IDeleteValueOnFields(driver, Elements.PST_QuickPowerSearch_VOut_InputBox);
            action.IType(driver, Elements.PST_QuickPowerSearch_VOut_InputBox, Keys.Tab);

            //--- Action: Leave the Iout textbox blank ---//
            action.IDeleteValueOnFields(driver, Elements.PST_QuickPowerSearch_IOut_InputBox);
            action.IType(driver, Elements.PST_QuickPowerSearch_IOut_InputBox, Keys.Tab);

            //--- Action: Click Search button ---//
            Thread.Sleep(2000);
            action.IClick(driver, Elements.PST_QuickPowerSearch_Search_Button);

            //--- Expected Result: Error message should be displayed ---//
            test.validateCountIsEqual(driver, 4, util.GetCount(driver, By.CssSelector("div[id='qserrors']>ul>li")));
        }
    }
}