﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System.Threading;

namespace AS_Test_Framework.PST
{
    [TestFixture]
    public class PST_ColumnHeaderFilterAndSort_CompareButtonAndPartNumberCheckbox : BaseSetUp
    {
        public PST_ColumnHeaderFilterAndSort_CompareButtonAndPartNumberCheckbox() : base() { }

        //--- URLs ---//
        string pst_page_url = "/parametricsearch/11470";

        //--- Labels ---//
        string compare_txt = "Compare";
        string showAll_txt = "Show All";

        [Test, Category("PST"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Compare Button and Part Number Checkbox are Present and Working as Expected after Clicking the Show All Button in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Compare Button and Part Number Checkbox are Present and Working as Expected after Clicking the Show All Button in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Compare Button and Part Number Checkbox are Present and Working as Expected after Clicking the Show All Button in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Compare Button and Part Number Checkbox are Present and Working as Expected after Clicking the Show All Button in RU Locale")]
        public void PST_ColumnHeaderFilterAndSort_VerifyCompareButtonAndPartNumberCheckboxAfterClickingShowAllButton(string Locale)
        {
            //--- Action: Access any Dynamic PST page. ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + pst_page_url);
            Thread.Sleep(1000);

            //--- Get the Default Values ---//
            int default_row_count = util.GetCount(driver, Elements.PST_PartNumber_Column_Rows);

            //----- PST Front End TestPlan - Redesign > Test Case Tab > R5 > TC2: Verify Compare button and part number checkbox -----//

            //--- Expected Result: Compare button is disabled during on-load ---//
            test.validateElementIsNotEnabled(driver, Elements.PST_Compare_ShowAll_Button);

            //--- Action: Check the checkbox of any part number ---//
            action.ICheck(driver, Elements.PST_PartNumber_Column_1st_Row_Checkbox);
            string partNumber_1st_row_value = util.GetText(driver, Elements.PST_PartNumber_Column_1st_Row_Link);

            //--- Expected Result: Compare button should still be disabled ---//
            test.validateElementIsNotEnabled(driver, Elements.PST_Compare_ShowAll_Button);

            //--- Action: Check 2 or more checkboxes of any part number ---//
            action.ICheck(driver, Elements.PST_PartNumber_Column_2nd_Row_Checkbox);
            string partNumber_2nd_row_value = util.GetText(driver, Elements.PST_PartNumber_Column_2nd_Row_Link);

            //--- Expected Result: Compare button should be enabled ---//
            test.validateElementIsEnabled(driver, Elements.PST_Compare_ShowAll_Button);

            //--- Action: Click the Compare button ---//
            action.IClick(driver, Elements.PST_Compare_ShowAll_Button);

            //--- Expected Result: Only the checked Part number should be displayed ---//
            test.validateCountIsEqual(driver, 2, util.GetCount(driver, Elements.PST_PartNumber_Column_Rows));
            if (util.GetCount(driver, Elements.PST_PartNumber_Column_Rows) == 2)
            {
                test.validateStringIsCorrect(driver, Elements.PST_PartNumber_Column_1st_Row_Link, partNumber_1st_row_value);
                test.validateStringIsCorrect(driver, Elements.PST_PartNumber_Column_2nd_Row_Link, partNumber_2nd_row_value);
            }

            if (Locale.Equals("en"))
            {
                //--- Expected Result: Compare button label changes to "Show All" ---//
                test.validateStringIsCorrect(driver, Elements.PST_Compare_ShowAll_Button, showAll_txt);
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.PST_Compare_ShowAll_Button);
            }

            //--- Action: Click the Show All button ---//
            action.IClick(driver, Elements.PST_Compare_ShowAll_Button);
            Thread.Sleep(1000);

            //--- Expected Result: All of the parts should be displayed again ---//
            test.validateCountIsEqual(driver, default_row_count, util.GetCount(driver, Elements.PST_PartNumber_Column_Rows));

            //--- Expected Result: The previous selected part number still checked ---//
            test.validateElementIsSelected(driver, Elements.PST_PartNumber_Column_1st_Row_Checkbox);
            test.validateElementIsSelected(driver, Elements.PST_PartNumber_Column_2nd_Row_Checkbox);

            if (Locale.Equals("en"))
            {
                //--- Expected Result: Compare button label changes to "Compare" ---//
                test.validateStringIsCorrect(driver, Elements.PST_Compare_ShowAll_Button, compare_txt);
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.PST_Compare_ShowAll_Button);
            }
        }

        [Test, Category("PST"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Compare Button and Part Number Checkbox are Present and Working as Expected after Unchecking 1 Checkbox in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Compare Button and Part Number Checkbox are Present and Working as Expected after Unchecking 1 Checkbox in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Compare Button and Part Number Checkbox are Present and Working as Expected after Unchecking 1 Checkbox in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Compare Button and Part Number Checkbox are Present and Working as Expected after Unchecking 1 Checkbox in RU Locale")]
        public void PST_ColumnHeaderFilterAndSort_VerifyCompareButtonAndPartNumberCheckboxAfterUnchecking1Checkbox(string Locale)
        {
            //--- Action: Access any Dynamic PST page. ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + pst_page_url);
            Thread.Sleep(2000);

            //--- Get the Default Values ---//
            int default_row_count = util.GetCount(driver, Elements.PST_PartNumber_Column_Rows);

            //--- Action: Check the checkbox of any part number ---//
            action.ICheck(driver, Elements.PST_PartNumber_Column_1st_Row_Checkbox);

            //--- Action: Check 2 or more checkboxes of any part number ---//
            action.ICheck(driver, Elements.PST_PartNumber_Column_2nd_Row_Checkbox);

            //--- Action: Click the Compare button ---//
            action.IClick(driver, Elements.PST_Compare_ShowAll_Button);

            //--- Action: Click the Show All button ---//
            test.validateElementIsEnabled(driver, Elements.PST_Compare_ShowAll_Button);

            //----- PST Front End TestPlan - Redesign > Test Case Tab > R4 > TC3_1: Verify that table resets after unchecking Products getting compared -----//

            //--- Action: Uncheck the checkbox until until one part number is checked ---//
            action.IUncheck(driver, Elements.PST_PartNumber_Column_1st_Row_Checkbox);

            //--- Expected Result: Table should display all the parts ---//
            test.validateCountIsEqual(driver, default_row_count, util.GetCount(driver, Elements.PST_PartNumber_Column_Rows));
        }

        [Test, Category("PST"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Compare Button and Part Number Checkbox are Present and Working as Expected after Unchecking All Checkboxes in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Compare Button and Part Number Checkbox are Present and Working as Expected after Unchecking All Checkboxes in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Compare Button and Part Number Checkbox are Present and Working as Expected after Unchecking All Checkboxes in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Compare Button and Part Number Checkbox are Present and Working as Expected after Unchecking All Checkboxes in RU Locale")]
        public void PST_ColumnHeaderFilterAndSort_VerifyCompareButtonAndPartNumberCheckboxAfterUncheckingAllCheckboxes(string Locale)
        {
            //--- Action: Access any Dynamic PST page. ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + pst_page_url);

            //----- PST Front End TestPlan - Redesign > Test Case Tab > R5 > TC2: Verify Compare button and part number checkbox -----//

            //--- Action: Check the checkbox of any part number ---//
            action.ICheck(driver, Elements.PST_PartNumber_Column_1st_Row_Checkbox);

            //--- Action: Check 2 or more checkboxes of any part number ---//
            action.ICheck(driver, Elements.PST_PartNumber_Column_2nd_Row_Checkbox);

            //--- Action: Click the Compare button ---//
            action.IClick(driver, Elements.PST_Compare_ShowAll_Button);

            //--- Action: Click the Show All button ---//
            test.validateElementIsEnabled(driver, Elements.PST_Compare_ShowAll_Button);

            //----- PST Front End TestPlan - Redesign > Test Case Tab > R5 > TC2: Verify Compare button and part number checkbox -----//

            //--- Action: Uncheck all the checkboxes ---//
            action.IUncheck(driver, Elements.PST_PartNumber_Column_1st_Row_Checkbox);
            action.IUncheck(driver, Elements.PST_PartNumber_Column_2nd_Row_Checkbox);

            //--- Expected Result: Compare button should be disabled ---//
            test.validateElementIsNotEnabled(driver, Elements.PST_Compare_ShowAll_Button);
        }
    }
}