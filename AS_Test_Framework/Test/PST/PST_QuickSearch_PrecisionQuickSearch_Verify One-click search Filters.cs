﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System.Threading;

namespace AS_Test_Framework.PST
{
    [TestFixture]
    public class PST_QuickSearch_PrecisionQuickSearch_OneClickSearchFilters : BaseSetUp
    {
        public PST_QuickSearch_PrecisionQuickSearch_OneClickSearchFilters() : base() { }

        //--- URLs ---//
        string pst_page_url = "/parametricsearch/10825";

        //--- Labe;s ---//
        string yes_text = "Yes";

        [Test, Category("PST"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Automotive Link is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Automotive Link is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Automotive Link is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Automotive Link is Present and Working as Expected in RU Locale")]
        public void PST_QuickSearch_PrecisionQuickSearch_OneClickSearchFilters_VerifyAutomotiveLink(string Locale)
        {
            //--- Action: Access Dynamic PST with a Quick Search Tool ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + pst_page_url);

            if (util.CheckElement(driver, Elements.PST_PrecisionQuickSearch_Automotive_Link, 1))
            {
                //----- PST Front End TestPlan - Redesign > Test Case Tab > R3 > TC6.3: Verify one-click search filter -----//

                //--- Action: On the One-click search, click Automotive ---//
                action.IClick(driver, Elements.PST_PrecisionQuickSearch_Automotive_Link);
                Thread.Sleep(2000);

                //--- Action: Check "Automotive" in Choose Parameters Dialog ---//
                action.IClick(driver, Elements.PST_ChooseParameters_Button);
                action.IClick(driver, Elements.PST_ChooseParameters_DialogBox_DeselectAll_Button);
                action.IClick(driver, Elements.PST_ChooseParameters_DialogBox_Automotive_Checkbox);
                action.IClick(driver, Elements.PST_ChooseParameters_DialogBox_Ok_Button);
                Thread.Sleep(9000);

                //--- Expected Result: Record should be filtered where Automotive column is set to Yes ---//
                test.validateStringIsCorrect(driver, Elements.PST_Automotive_Column_Row_Values, yes_text);

                //--- Expected Result: Automotive column filter should be updated only checking "Yes" in its filter ---//
                test.validateElementIsSelected(driver, Elements.PST_Automotive_Column_Filter_Yes_Checkbox);
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.PST_PrecisionQuickSearch_Automotive_Link);
            }
        }

        //[Test, Category("PST"), Category("NonCore"), Retry(2)] --> NOTE: Pending. Manual Testing Team to confirm if this is still valid.
        //[TestCase("en", TestName = "Verify that the Small Size Link is Present and Working as Expected in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Small Size Link is Present and Working as Expected in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Small Size Link is Present and Working as Expected in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Small Size Link is Present and Working as Expected in RU Locale")]
        //public void PST_QuickSearch_PrecisionQuickSearch_OneClickSearchFilters_VerifySmallSizeLink(string Locale)
        //{
        //    //--- Action: Access Dynamic PST with a Quick Search Tool ---//
        //    action.Navigate(driver, Configuration.Env_Url + Locale + pst_page_url);

        //    if (util.CheckElement(driver, Elements.PST_PrecisionQuickSearch_SmallSize_Link, 1))
        //    {
        //        //----- PST Front End TestPlan - Redesign > Test Case Tab > R3 > TC6.3: Verify one-click search filter -----//

        //        //--- Action: On the One-click search, click Small Size ---//
        //        action.IClick(driver, Elements.PST_PrecisionQuickSearch_SmallSize_Link);
        //        Thread.Sleep(2000);

        //        //--- Action: Check "Packag eArea" in Choose Parameters Dialog ---//
        //        action.IClick(driver, Elements.PST_ChooseParameters_Button);
        //        action.IClick(driver, Elements.PST_ChooseParameters_DialogBox_DeselectAll_Button);
        //        action.IClick(driver, Elements.PST_ChooseParameters_DialogBox_PackageArea_Checkbox);
        //        action.IClick(driver, Elements.PST_ChooseParameters_DialogBox_Ok_Button);

        //        //--- Expected Result: Package Area filter column should be set to: Min textbox = 2 ---//
        //        test.validateStringInstance(driver, "2", util.GetInputBoxValue(driver, Elements.PST_PackageArea_Column_Min_Value));

        //        //--- Expected Result: Package Area filter column should be set to: Max textbox = 10 ---//
        //        test.validateStringInstance(driver, "10", util.GetInputBoxValue(driver, Elements.PST_PackageArea_Column_Max_Value));
        //    }
        //}

        [Test, Category("PST"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Low Power Link is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Low Power Link is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Low Power Link is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Low Power Link is Present and Working as Expected in RU Locale")]
        public void PST_QuickSearch_PrecisionQuickSearch_OneClickSearchFilters_VerifyLowPowerLink(string Locale)
        {
            //--- Action: Access Dynamic PST with a Quick Search Tool ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + pst_page_url);
            Thread.Sleep(1000);

            if (util.CheckElement(driver, Elements.PST_PrecisionQuickSearch_LowPower_Link, 1))
            {
                //----- PST Front End TestPlan - Redesign > Test Case Tab > R3 > TC6.3: Verify one-click search filter -----//

                //--- Action: On the One-click search, click Low Power ---//
                action.IClick(driver, Elements.PST_PrecisionQuickSearch_LowPower_Link);
                Thread.Sleep(2000);

                //--- Expected Result: Power filter column should be set to: Max textbox = 1000u ---//
                test.validateStringInstance(driver, "1000u", util.GetInputBoxValue(driver, Elements.PST_Power_Column_Max_Value));
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.PST_PrecisionQuickSearch_LowPower_Link);
            }
        }

        [Test, Category("PST"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Integrated Voltage Reference Link is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Integrated Voltage Reference Link is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Integrated Voltage Reference Link is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Integrated Voltage Reference Link is Present and Working as Expected in RU Locale")]
        public void PST_QuickSearch_PrecisionQuickSearch_OneClickSearchFilters_VerifyIntegratedVoltageReferenceLink(string Locale)
        {
            //--- Action: Access Dynamic PST with a Quick Search Tool ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + pst_page_url);
            Thread.Sleep(2000);

            if (util.CheckElement(driver, Elements.PST_PrecisionQuickSearch_IntegratedVoltageReference_Link, 1))
            {
                //----- PST Front End TestPlan - Redesign > Test Case Tab > R3 > TC6.3: Verify one-click search filter -----//

                //--- Action: On the One-click search, click Integrated Voltage Reference ---//
                action.IClick(driver, Elements.PST_PrecisionQuickSearch_IntegratedVoltageReference_Link);
                Thread.Sleep(2000);

                //--- Action: Check "Vref Source" in Choose Parameters Dialog ---//
                action.IClick(driver, Elements.PST_ChooseParameters_Button);
                action.IClick(driver, Elements.PST_ChooseParameters_DialogBox_DeselectAll_Button);
                action.IClick(driver, Elements.PST_ChooseParameters_DialogBox_VRefSource_Checkbox);
                action.IClick(driver, Elements.PST_ChooseParameters_DialogBox_Ok_Button);

                //--- Expected Result: Vref Source column filter should be updated only checking "Internal" in its filter ---//
                test.validateElementIsSelected(driver, Elements.PST_VRefSource_Column_Filter_Internal_Checkbox);
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.PST_PrecisionQuickSearch_IntegratedVoltageReference_Link);
            }
        }

        //--- Note: The CHIPS OR DIE Checkbox is not being Selected. AL-17691 has been logged for the Issue. ---//
        //[Test, Category("PST"), Category("NonCore"), Retry(2)]
        //[TestCase("en", TestName = "Verify that the Available As Die Link is Present and Working as Expected in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Available As Die Link is Present and Working as Expected in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Available As Die Link is Present and Working as Expected in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Available As Die Link is Present and Working as Expected in RU Locale")]
        //public void PST_QuickSearch_PrecisionQuickSearch_OneClickSearchFilters_VerifyAvailableAsDieLink(string Locale)
        //{
        //    //--- Action: Access Dynamic PST with a Quick Search Tool ---//
        //    action.Navigate(driver, Configuration.Env_Url + Locale + pst_page_url);

        //    if (util.CheckElement(driver, Elements.PST_PrecisionQuickSearch_AvailableAsDie_Link, 1))
        //    {
        //        //----- PST Front End TestPlan - Redesign > Test Case Tab > R3 > TC6.3: Verify one-click search filter -----//

        //        //--- Action: On the One-click search, click Available as Die ---//
        //        action.IClick(driver, Elements.PST_PrecisionQuickSearch_AvailableAsDie_Link);

        //        //--- Action: Check "Package" in Choose Parameters Dialog ---//
        //        action.IClick(driver, Elements.PST_ChooseParameters_Button);
        //        action.IClick(driver, Elements.PST_ChooseParameters_DialogBox_DeselectAll_Button);
        //        action.IClick(driver, Elements.PST_ChooseParameters_DialogBox_Package_Checkbox);
        //        action.IClick(driver, Elements.PST_ChooseParameters_DialogBox_Ok_Button);

        //        //--- Expected Result: Package column filter should be updated only checking "CHIPS OR DIE" in its filter ---//
        //        test.validateElementIsSelected(driver, Elements.PST_Package_Column_Filter_ChipsOrDie_Checkbox);
        //    }
        //    else
        //    {
        //        test.validateElementIsPresent(driver, Elements.PST_PrecisionQuickSearch_AvailableAsDie_Link);
        //    }
        //}
    }
}