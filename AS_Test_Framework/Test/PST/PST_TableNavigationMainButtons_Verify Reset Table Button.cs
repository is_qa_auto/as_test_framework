﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using OpenQA.Selenium;
using System.Threading;

namespace AS_Test_Framework.PST
{
    [TestFixture]
    public class PST_TableNavigationMainButtons_ResetTableButton : BaseSetUp
    {
        public PST_TableNavigationMainButtons_ResetTableButton() : base() { }

        //--- URLs ---//
        string pst_page_url = "/parametricsearch/11470";

        [Test, Category("PST"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Reset Table Button is Present and Working as Expected after Clicking the Select All Button in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Reset Table Button is Present and Working as Expected after Clicking the Select All Button in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Reset Table Button is Present and Working as Expected after Clicking the Select All Button in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Reset Table Button is Present and Working as Expected after Clicking the Select All Button in RU Locale")]
        public void PST_TableNavigationMainButtons_ResetTableButton_VerifyAfterClickingTheSelectAllButton(string Locale)
        {
            //--- Action: Access any Dynamic PST page. ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + pst_page_url);
            Thread.Sleep(1000);

            //--- Get the Default Values ---//
            string default_url = driver.Url;
            int default_column_count = util.GetCount(driver, Elements.PST_Columns);

            //--- Action: Click the Choose Parameters button ---//
            action.IClick(driver, Elements.PST_ChooseParameters_Button);

            //--- Action: Click the Select All button ---//
            action.IClick(driver, Elements.PST_ChooseParameters_DialogBox_SelectAll_Button);

            //--- Action: Click OK button ---//
            action.IClick(driver, Elements.PST_ChooseParameters_DialogBox_Ok_Button);

            if (util.CheckElement(driver, Elements.PST_ResetTable_Button, 1))
            {
                //----- PST Front End TestPlan - Redesign > Test Case Tab > R4 > TC3: Verify "Reset Table" button -----//

                //--- Action: Click the Reset Table button ---//
                action.IClick(driver, Elements.PST_ResetTable_Button);

                //--- Expected Result: Dynamic PST should reset to its default display ---//
                test.validateCountIsEqual(driver, default_column_count, util.GetCount(driver, Elements.PST_Columns));

                //--- Expected Result: Any string concatenated to the URL is removed ---//
                if (driver.Url.EndsWith("/") && !default_url.EndsWith("/"))
                {
                    default_url = default_url + "/";
                }
                test.validateStringInstance(driver, default_url, driver.Url);
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.PST_ResetTable_Button);
            }
        }

        [Test, Category("PST"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Reset Table Button is Present and Working as Expected after Clicking the Deselect All Button in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Reset Table Button is Present and Working as Expected after Clicking the Deselect All Button in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Reset Table Button is Present and Working as Expected after Clicking the Deselect All Button in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Reset Table Button is Present and Working as Expected after Clicking the Deselect All Button in RU Locale")]
        public void PST_TableNavigationMainButtons_ResetTableButton_VerifyAfterClickingTheDeselectAllButton(string Locale)
        {
            //--- Action: Access any Dynamic PST page. ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + pst_page_url);
            Thread.Sleep(1000);

            //--- Get the Default Values ---//
            string default_url = driver.Url;
            int default_column_count = util.GetCount(driver, Elements.PST_Columns);

            //--- Action: Click the Choose Parameters button ---//
            action.IClick(driver, Elements.PST_ChooseParameters_Button);

            //--- Action: Click the Deselect All button ---//
            action.IClick(driver, Elements.PST_ChooseParameters_DialogBox_DeselectAll_Button);

            //--- Action: Click X button ---//
            action.IClick(driver, Elements.PST_ChooseParameters_DialogBox_X_Button);

            if (util.CheckElement(driver, Elements.PST_ResetTable_Button, 1))
            {
                //----- PST Front End TestPlan - Redesign > Test Case Tab > R4 > TC3: Verify "Reset Table" button -----//

                //--- Action: Click the Reset Table button ---//
                action.IClick(driver, Elements.PST_ResetTable_Button);

                //--- Expected Result: Dynamic PST should reset to its default display ---//
                test.validateCountIsEqual(driver, default_column_count, util.GetCount(driver, Elements.PST_Columns));

                //--- Expected Result: Any string concatenated to the URL is removed ---//
                if (driver.Url.EndsWith("/") && !default_url.EndsWith("/"))
                {
                    default_url = default_url + "/";
                }
                test.validateStringInstance(driver, default_url, driver.Url);
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.PST_ResetTable_Button);
            }
        }

        [Test, Category("PST"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Reset Table Button is Present and Working as Expected after Selecting a Checkbox in Choose Parameters in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Reset Table Button is Present and Working as Expected after Selecting a Checkbox in Choose Parameters in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Reset Table Button is Present and Working as Expected after Selecting a Checkbox in Choose Parameters in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Reset Table Button is Present and Working as Expected after Selecting a Checkbox in Choose Parameters in RU Locale")]
        public void PST_TableNavigationMainButtons_ResetTableButton_VerifyAfterSelectingACheckboxInChooseParameters(string Locale)
        {
            //--- Action: Access any Dynamic PST page. ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + pst_page_url);

            //--- Get the Default Values ---//
            string default_url = driver.Url;
            int default_column_count = util.GetCount(driver, Elements.PST_Columns);

            //--- Action: Click the Choose Parameters button ---//
            action.IClick(driver, Elements.PST_ChooseParameters_Button);

            //--- Action: Tick any unchecked parameters in the Choose Parameters window ---//
            action.IClick(driver, Elements.PST_ChooseParameters_DialogBox_DeselectAll_Button);
            action.IClick(driver, Elements.PST_ChooseParameters_DialogBox_Checkboxes);
            string selected_parameter_value = util.ReturnAttribute(driver, Elements.PST_ChooseParameters_DialogBox_Checkboxes, "value");

            //--- Action: Click OK button ---//
            action.IClick(driver, Elements.PST_ChooseParameters_DialogBox_Ok_Button);
            Thread.Sleep(2000);

            if (util.CheckElement(driver, Elements.PST_ResetTable_Button, 1))
            {
                //----- PST Front End TestPlan - Redesign > Test Case Tab > R4 > TC3: Verify "Reset Table" button -----//

                //--- Action: Click the Reset Table button ---//
                action.IClick(driver, Elements.PST_ResetTable_Button);

                //--- Expected Result: Dynamic PST should reset to its default display ---//
                test.validateCountIsEqual(driver, default_column_count, util.GetCount(driver, Elements.PST_Columns));

                //--- Expected Result: Any string concatenated to the URL is removed ---//
                if (driver.Url.EndsWith("/") && !default_url.EndsWith("/"))
                {
                    default_url = default_url + "/";
                }
                test.validateStringInstance(driver, default_url, driver.Url);
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.PST_ResetTable_Button);
            }
        }

        [Test, Category("PST"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Reset Table Button is Present and Working as Expected after Filtering a Column in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Reset Table Button is Present and Working as Expected after Filtering a Column in CN Locale")]
        [TestCase("ru", TestName = "Verify that the Reset Table Button is Present and Working as Expected after Filtering a Column in RU Locale")]
        public void PST_TableNavigationMainButtons_ResetTableButton_VerifyFilteringAColumn(string Locale)
        {
            //--- Action: Access any Dynamic PST page. ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + pst_page_url);

            //--- Get the Default Values ---//
            string default_url = driver.Url;
            int default_column_count = util.GetCount(driver, Elements.PST_Columns);

            //--- Action: Enter two Part number in the Part number filter textbox with delimiter as separator (e.g. ADA4870|AD8003) ---//
            string partNumber_1st_row_value = util.GetText(driver, Elements.PST_PartNumber_Column_1st_Row_Link);
            string partNumber_2nd_row_value = util.GetText(driver, Elements.PST_PartNumber_Column_2nd_Row_Link);
            action.IDeleteValueOnFields(driver, Elements.PST_PartNumber_Column_InputBox);
            action.IType(driver, Elements.PST_PartNumber_Column_InputBox, partNumber_1st_row_value + "|" + partNumber_2nd_row_value);
            action.IType(driver, Elements.PST_PartNumber_Column_InputBox, Keys.Tab);

            if (!Locale.Equals("jp"))
            {
                //----- PST Front End TestPlan - Redesign > Test Case Tab > R4 > TC3: Verify "Reset Table" button -----//

                //--- Action: Click the Reset Table button ---//
                action.IClick(driver, Elements.PST_ResetTable_Button);

                //--- Expected Result: Dynamic PST should reset to its default display ---//
                test.validateCountIsEqual(driver, default_column_count, util.GetCount(driver, Elements.PST_Columns));

                //--- Expected Result: Any string concatenated to the URL is removed ---//
                if (driver.Url.EndsWith("/") && !default_url.EndsWith("/"))
                {
                    default_url = default_url + "/";
                }
                test.validateStringInstance(driver, default_url, driver.Url);
            }
        }

        [Test, Category("PST"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Reset Table Button is Present and Working as Expected after Checking the Checkbox of Any PartNumber in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Reset Table Button is Present and Working as Expected after Checking the Checkbox of Any PartNumber in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Reset Table Button is Present and Working as Expected after Checking the Checkbox of Any PartNumber in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Reset Table Button is Present and Working as Expected after Checking the Checkbox of Any PartNumber in RU Locale")]
        public void PST_TableNavigationMainButtons_ResetTableButton_VerifyAfterCheckingTheCheckboxOfAnyPartNumber(string Locale)
        {
            //--- Action: Access any Dynamic PST page. ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + pst_page_url);

            //--- Get the Default Values ---//
            string default_url = driver.Url;
            int default_column_count = util.GetCount(driver, Elements.PST_Columns);

            //----- PST Front End TestPlan - Redesign > Test Case Tab > R4 > TC3: Verify "Reset Table" button -----//

            //--- Action: Check the checkbox of any part number ---//
            action.ICheck(driver, Elements.PST_PartNumber_Column_1st_Row_Checkbox);

            //--- Action: Click the Reset Table button ---//
            action.IClick(driver, Elements.PST_ResetTable_Button);

            //--- Expected Result: Dynamic PST should reset to its default display ---//
            test.validateCountIsEqual(driver, default_column_count, util.GetCount(driver, Elements.PST_Columns));

            //--- Expected Result: Any string concatenated to the URL is removed ---//
            if (driver.Url.EndsWith("/") && !default_url.EndsWith("/"))
            {
                default_url = default_url + "/";
            }
            test.validateStringInstance(driver, default_url, driver.Url);
        }
    }
}