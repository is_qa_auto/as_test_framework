﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using OpenQA.Selenium;
using System.Threading;

namespace AS_Test_Framework.PST
{
    [TestFixture]
    public class PST_ColumnHeaderFilterAndSort_HideButton : BaseSetUp
    {
        public PST_ColumnHeaderFilterAndSort_HideButton() : base() { }

        //--- URLs ---//
        string pst_page_url = "/parametricsearch/11084";

        [Test, Category("PST"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Hide Button is Present and Working as Expected for Minimized Columns in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Hide Button is Present and Working as Expected for Minimized Columns in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Hide Button is Present and Working as Expected for Minimized Columns in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Hide Button is Present and Working as Expected for Minimized Columns in RU Locale")]
        public void PST_ColumnHeaderFilterAndSort_HideButton_VerifyForMinimizedColumns(string Locale)
        {
            //--- Action: Go to PST page ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + pst_page_url);
            Thread.Sleep(5000);

            if (!util.CheckElement(driver, Elements.PST_Minimized_Columns, 1))
            {
                //--- Action: Click Maximize Filters button ---//
                action.IClick(driver, Elements.PST_MaximizeFilters_MinimizeFilters_Button);
            }

            //----- PST Front End TestPlan - Redesign > Test Case Tab > R4 > TC6: Verify Hide button -----//

            //--- Action: Get the Data Parameter Value of the 2nd Column ---//
            string column_selected_locator = "1";
            string column_dataParameter_value = util.ReturnAttribute(driver, By.CssSelector("section[class='details']>section[class='parameters']>section:nth-of-type(" + column_selected_locator + ")"), "data-parameter");

            //--- Locators ---//
            string checkbox_locator = "section[class='choose parameters'] * input[id$='" + column_dataParameter_value + "']";
            string column_locator = "section[data-parameter='" + column_dataParameter_value + "']";
            string column_hide_button_locator = "section[data-parameter='" + column_dataParameter_value + "']>button[class='custom action']";

            action.IClick(driver, Elements.PST_ChooseParameters_Button);
            action.IClick(driver, Elements.PST_ChooseParameters_DialogBox_DeselectAll_Button);
            action.IClick(driver, By.CssSelector(checkbox_locator));
            action.IClick(driver, Elements.PST_ChooseParameters_DialogBox_Ok_Button);

            //--- Action: Click Hide button in the column header of any Column ---//
            action.IClick(driver, By.CssSelector(column_hide_button_locator));

            //--- Expected Result: Column should be hidden ---//
            //--- Expected Result: Filter values of hidden column must not appear on the remaining columns' filters (IQ-8996/AL-16051) ---//
            test.validateElementIsNotPresent(driver, By.CssSelector(column_locator));
        }

        [Test, Category("PST"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Hide Button is Present and Working as Expected for Maximized Columns in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Hide Button is Present and Working as Expected for Maximized Columns in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Hide Button is Present and Working as Expected for Maximized Columns in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Hide Button is Present and Working as Expected for Maximized Columns in RU Locale")]
        public void PST_ColumnHeaderFilterAndSort_HideButton_VerifyForMaximizedColumns(string Locale)
        {
            //--- Action: Go to PST page ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + pst_page_url);

            //----- PST Front End TestPlan - Redesign > Test Case Tab > R4 > TC6.1: Verify Hide button for maximized columns (IQ-8996/AL-16051) -----//

            if (util.CheckElement(driver, Elements.PST_Minimized_Columns, 1))
            {
                //--- Action: Click Maximize Filters button ---//
                action.IClick(driver, Elements.PST_MaximizeFilters_MinimizeFilters_Button);
                Thread.Sleep(2000);
            }

            //--- Action: Get the Data Parameter Value of the 2nd Column ---//
            string column_selected_locator = "1";
            string column_dataParameter_value = util.ReturnAttribute(driver, By.CssSelector("section[class='details']>section[class='parameters']>section:nth-of-type(" + column_selected_locator + ")"), "data-parameter");

            //--- Locators ---//
            string column_locator = "section[data-parameter='" + column_dataParameter_value + "']";
            string column_hide_button_locator = "section[data-parameter='" + column_dataParameter_value + "']>button[class='custom action']";

            //--- Action: Click Hide button in the column header of any Column ---//
            action.IClick(driver, By.CssSelector(column_hide_button_locator));

            //--- Expected Result: Column should be hidden ---//
            //--- Expected Result: Filter values of hidden column must not appear on the remaining columns' filters ---//
            test.validateElementIsNotPresent(driver, By.CssSelector(column_locator));
        }
    }
}