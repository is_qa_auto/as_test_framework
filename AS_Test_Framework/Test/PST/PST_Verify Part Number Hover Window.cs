﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System.Threading;

namespace AS_Test_Framework.PST
{
    [TestFixture]
    public class PST_PartNumberHoverWindow : BaseSetUp
    {
        public PST_PartNumberHoverWindow() : base() { }

        //--- URLs ---//
        string pst_page_url = "/parametricsearch/11470";
        string pdp_url_format = "/products/";

        [Test, Category("PST"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Part Number Hover Window is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Part Number Hover Window is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Part Number Hover Window is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Part Number Hover Window is Present and Working as Expected in RU Locale")]
        public void PST_VerifyPartNumberHoverWindow(string Locale)
        {
            //--- Action: Access any Dynamic PST page. ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + pst_page_url);
            Thread.Sleep(1000);

            //----- PST Front End TestPlan - Redesign > Test Case Tab > R6 > TC1: Verify Part Number Hover Window -----//

            //--- Action: Hover to the Part number link in the Part number column ---//
            action.IMouseOverTo(driver, Elements.PST_PartNumber_Column_1st_Row_Link);
            Thread.Sleep(4000);

            //--- Expected Result: Part Number Hover window should be displayed ---//
            test.validateElementIsPresent(driver, Elements.PST_PartNumber_HoverWindow);

            if (util.CheckElement(driver, Elements.PST_PartNumber_HoverWindow, 1))
            {
                //--- Expected Result: The Part Number should be present in the Part numner Hover window ---//
                test.validateElementIsPresent(driver, Elements.PST_PartNumber_HoverWindow_PartNumber_Link);

                //--- Expected Result: The Download Datasheet should be present in the Part numner Hover window ---//
                test.validateElementIsPresent(driver, Elements.PST_PartNumber_HoverWindow_DownloadDataSheet_Link);

                //--- Expected Result: The View Documetation should be present in the Part numner Hover window ---//
                test.validateElementIsPresent(driver, Elements.PST_PartNumber_HoverWindow_ViewDocumentation_Link);

                //--- Expected Result: The Sample and Buy should be present in the Part numner Hover window ---//
                test.validateElementIsPresent(driver, Elements.PST_PartNumber_HoverWindow_SampleAndBuy_Link);

                //--- Expected Result: The Diagram should be present in the Part numner Hover window ---//
                test.validateElementIsPresent(driver, Elements.PST_PartNumber_HoverWindow_Diagram_Img);
            }
        }

        [Test, Category("PST"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Part Number Link is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Part Number Link is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Part Number Link is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Part Number Link is Present and Working as Expected in RU Locale")]
        public void PST_PartNumberHoverWindow_VerifyPartNumberLink(string Locale)
        {
            //--- Action: Access any Dynamic PST page. ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + pst_page_url);

            //----- PST Front End TestPlan - Redesign > Test Case Tab > R6 > TC1: Verify Part Number Hover Window -----//

            //--- Action: Click any Part number in the Part number column ---//
            action.IOpenLinkInNewTab(driver, Elements.PST_PartNumber_Column_1st_Row_Link);
            Thread.Sleep(10000);

            //--- Expected Result: Product page should open ---//
            test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + pdp_url_format);
        }
    }
}