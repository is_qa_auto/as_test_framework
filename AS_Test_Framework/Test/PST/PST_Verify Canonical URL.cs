﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using OpenQA.Selenium;

namespace AS_Test_Framework.PST
{
    [TestFixture]
    public class PST_CanonicalUrl : BaseSetUp
    {
        public PST_CanonicalUrl() : base() { }

        //--- URLs ---//
        string pst_page_url = "/parametricsearch/10825";

        [Test, Category("PST"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Canonical URL is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Canonical URL is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Canonical URL is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Canonical URL is Present and Working as Expected in RU Locale")]
        public void PST_VerifyCanonicalUrl(string Locale)
        {
            //--- Action: Access any PST page ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + pst_page_url);

            //--- Expected Result: The canonical URL has https as the protocol ---//
            test.validateCanonicalUrl(driver, Configuration.Env_Url + Locale + pst_page_url);
        }
    }
}