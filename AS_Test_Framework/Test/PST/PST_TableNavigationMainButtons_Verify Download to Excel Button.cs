﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System.Threading;

namespace AS_Test_Framework.PST
{
    [TestFixture]
    public class PST_TableNavigationMainButtons_DownloadToExcelButton : BaseSetUp
    {
        public PST_TableNavigationMainButtons_DownloadToExcelButton() : base() { }

        //--- URLs ---//
        string pst_page_url = "/parametricsearch/11470";

        [Test, Category("PST"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Download to Excel Button is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Download to Excel Button is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Download to Excel Button is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Download to Excel Button is Present and Working as Expected in RU Locale")]
        public void PST_TableNavigationMainButtons_VerifyDownloadToExcelButton(string Locale)
        {
            //--- Action: Access any Dynamic PST page. ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + pst_page_url);
            Thread.Sleep(1000);

            //----- PST Front End TestPlan - Redesign > Test Case Tab > R4 > TC6: Verify "Download to Excel" button-----//

            //--- Expected Result: The Download to Excel Button is present ---//
            test.validateElementIsPresent(driver, Elements.PST_DownloadToExcel_Button);
        }
    }
}