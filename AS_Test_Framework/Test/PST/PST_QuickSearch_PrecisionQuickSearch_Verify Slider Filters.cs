﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System.Threading;

namespace AS_Test_Framework.PST
{
    [TestFixture]
    public class PST_QuickSearch_PrecisionQuickSearch_SliderFilters : BaseSetUp
    {
        public PST_QuickSearch_PrecisionQuickSearch_SliderFilters() : base() { }

        //--- URLs ---//
        string pst_page_url = "/parametricsearch/10825";

        [Test, Category("PST"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Resolution Slider is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Resolution Slider is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Resolution Slider is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Resolution Slider is Present and Working as Expected in RU Locale")]
        public void PST_QuickSearch_PrecisionQuickSearch_DropdownFilters_VerifyResolutionSlider(string Locale)
        {
            //--- Action: Access Dynamic PST with a Quick Search Tool ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + pst_page_url);
            Thread.Sleep(5000);

            if (util.CheckElement(driver, Elements.PST_PrecisionQuickSearch_Resolution_Min_Slider, 1) && util.CheckElement(driver, Elements.PST_PrecisionQuickSearch_Resolution_Max_Slider, 1))
            {
                //----- PST Front End TestPlan - Redesign > Test Case Tab > R3 > TC6.1: Verify Slider filter -----//

                //--- Get the Default Values ---//
                string resolution_min_default_value = util.GetInputBoxValue(driver, Elements.PST_PrecisionQuickSearch_Resolution_Min_InputBox);
                string resolution_max_default_value = util.GetInputBoxValue(driver, Elements.PST_PrecisionQuickSearch_Resolution_Max_InputBox);

                //--- Action: Click the Submit button ---//
                action.IClick(driver, Elements.PST_PrecisionQuickSearch_Submit_Button);

                //--- Action: Check "Resolution" in Choose Parameters Dialog ---//
                action.IClick(driver, Elements.PST_ChooseParameters_Button);
                action.IClick(driver, Elements.PST_ChooseParameters_DialogBox_DeselectAll_Button);
                action.IClick(driver, Elements.PST_ChooseParameters_DialogBox_Resolution_Checkbox);
                action.IClick(driver, Elements.PST_ChooseParameters_DialogBox_Ok_Button);

                //--- Expected Result: Set values on the Resolution quick filter should reflect on the Resolution column filter ---//
                test.validateStringInstance(driver, util.GetInputBoxValue(driver, Elements.PST_PrecisionQuickSearch_Resolution_Min_InputBox), util.GetInputBoxValue(driver, Elements.PST_Resolution_Column_Min_Value));
                test.validateStringInstance(driver, util.GetInputBoxValue(driver, Elements.PST_PrecisionQuickSearch_Resolution_Max_InputBox), util.GetInputBoxValue(driver, Elements.PST_Resolution_Column_Max_Value));

                //--- Action: In the resolution quick filter, drag the min slider bar to the right ---//
                action.GivenIDragAndDrop(driver, Elements.PST_PrecisionQuickSearch_Resolution_Min_Slider, 10, 0);

                //--- Expected Result: Min textbox should change value depending on the set position of the Min slider bar ---//
                test.validateStringChanged(driver, util.GetInputBoxValue(driver, Elements.PST_PrecisionQuickSearch_Resolution_Min_InputBox), resolution_min_default_value);

                //--- Action: In the resolution quick filter, drag the max slider bar to the left ---//
                action.GivenIDragAndDrop(driver, Elements.PST_PrecisionQuickSearch_Resolution_Max_Slider, -10, 0);

                //--- Expected Result: Max textbox should change value depending on the set position of the max slider bar ---//
                test.validateStringChanged(driver, util.GetInputBoxValue(driver, Elements.PST_PrecisionQuickSearch_Resolution_Max_InputBox), resolution_max_default_value);
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.PST_PrecisionQuickSearch_Resolution_Min_Slider);
                test.validateElementIsPresent(driver, Elements.PST_PrecisionQuickSearch_Resolution_Max_Slider);
            }
        }

        [Test, Category("PST"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Sample Rate Slider is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Sample Rate Slider is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Sample Rate Slider is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Sample Rate Slider is Present and Working as Expected in RU Locale")]
        public void PST_QuickSearch_PrecisionQuickSearch_DropdownFilters_VerifySampleRateSlider(string Locale)
        {
            //--- Action: Access Dynamic PST with a Quick Search Tool ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + pst_page_url);
            Thread.Sleep(1000);

            if (util.CheckElement(driver, Elements.PST_PrecisionQuickSearch_SampleRate_Min_Slider, 1) && util.CheckElement(driver, Elements.PST_PrecisionQuickSearch_SampleRate_Max_Slider, 1))
            {
                //----- PST Front End TestPlan - Redesign > Test Case Tab > R3 > TC6.1: Verify Slider filter -----//

                //--- Get the Default Values ---//
                string sampleRate_min_default_value = util.GetInputBoxValue(driver, Elements.PST_PrecisionQuickSearch_SampleRate_Min_InputBox);
                string sampleRate_max_default_value = util.GetInputBoxValue(driver, Elements.PST_PrecisionQuickSearch_SampleRate_Max_InputBox);

                //--- Action: Click the Submit button ---//
                action.IClick(driver, Elements.PST_PrecisionQuickSearch_Submit_Button);

                //--- Action: Check "Sample Rate" in Choose Parameters Dialog ---//
                action.IClick(driver, Elements.PST_ChooseParameters_Button);
                action.IClick(driver, Elements.PST_ChooseParameters_DialogBox_DeselectAll_Button);
                action.IClick(driver, Elements.PST_ChooseParameters_DialogBox_SampleRate_Checkbox);
                action.IClick(driver, Elements.PST_ChooseParameters_DialogBox_Ok_Button);

                //--- Expected Result: Set values on the Sample rate quick filter should reflect on the Sample rate column filter ---//
                test.validateStringInstance(driver, util.GetInputBoxValue(driver, Elements.PST_PrecisionQuickSearch_SampleRate_Min_InputBox), util.GetInputBoxValue(driver, Elements.PST_SampleRate_Column_Min_Value));
                //test.validateStringInstance(driver, util.GetInputBoxValue(driver, Elements.PST_PrecisionQuickSearch_SampleRate_Max_InputBox), util.GetInputBoxValue(driver, Elements.PST_SampleRate_Column_Max_Value)); --> NOTE: Pending. Manual Testing Team to confirm if this is still valid.

                //--- Action: In the Sample rate quick filter, drag the min slider bar to the right ---//
                action.GivenIDragAndDrop(driver, Elements.PST_PrecisionQuickSearch_SampleRate_Min_Slider, 10, 0);

                //--- Expected Result: Min textbox should change value depending on the set position of the Min slider bar ---//
                test.validateStringChanged(driver, util.GetInputBoxValue(driver, Elements.PST_PrecisionQuickSearch_SampleRate_Min_InputBox), sampleRate_min_default_value);

                //--- Action: In the Sample rate quick filter, drag the max slider bar to the left ---//
                action.GivenIDragAndDrop(driver, Elements.PST_PrecisionQuickSearch_SampleRate_Max_Slider, -10, 0);

                //--- Expected Result: Max textbox should change value depending on the set position of the max slider bar ---//
                test.validateStringChanged(driver, util.GetInputBoxValue(driver, Elements.PST_PrecisionQuickSearch_SampleRate_Max_InputBox), sampleRate_max_default_value);
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.PST_PrecisionQuickSearch_SampleRate_Min_Slider);
                test.validateElementIsPresent(driver, Elements.PST_PrecisionQuickSearch_SampleRate_Max_Slider);
            }
        }

        [Test, Category("PST"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Number of Input Channels Slider is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Number of Input Channels Slider is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Number of Input Channels Slider is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Number of Input Channels Slider is Present and Working as Expected in RU Locale")]
        public void PST_QuickSearch_PrecisionQuickSearch_DropdownFilters_VerifyNumberOfInputChannelsSlider(string Locale)
        {
            //--- Action: Access Dynamic PST with a Quick Search Tool ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + pst_page_url);
            Thread.Sleep(4000);

            if (util.CheckElement(driver, Elements.PST_PrecisionQuickSearch_NumberOfInputChannels_Min_Slider, 1) && util.CheckElement(driver, Elements.PST_PrecisionQuickSearch_NumberOfInputChannels_Max_Slider, 1))
            {
                //----- PST Front End TestPlan - Redesign > Test Case Tab > R3 > TC6.1: Verify Slider filter -----//

                //--- Get the Default Values ---//
                string numberOfInputChannels_min_default_value = util.GetInputBoxValue(driver, Elements.PST_PrecisionQuickSearch_NumberOfInputChannels_Min_InputBox);
                string numberOfInputChannels_max_default_value = util.GetInputBoxValue(driver, Elements.PST_PrecisionQuickSearch_NumberOfInputChannels_Max_InputBox);

                //--- Action: Click the Submit button ---//
                action.IClick(driver, Elements.PST_PrecisionQuickSearch_Submit_Button);

                //--- Action: Check "Channels" in Choose Parameters Dialog ---//
                action.IClick(driver, Elements.PST_ChooseParameters_Button);
                action.IClick(driver, Elements.PST_ChooseParameters_DialogBox_DeselectAll_Button);
                action.IClick(driver, Elements.PST_ChooseParameters_DialogBox_Channels_Checkbox);
                action.IClick(driver, Elements.PST_ChooseParameters_DialogBox_Ok_Button);

                //--- Expected Result: Set values on the Resolution quick filter should reflect on the Channels column filter ---//
                test.validateStringInstance(driver, util.GetInputBoxValue(driver, Elements.PST_PrecisionQuickSearch_NumberOfInputChannels_Min_InputBox), util.GetInputBoxValue(driver, Elements.PST_Channels_Column_Min_Value));
                test.validateStringInstance(driver, util.GetInputBoxValue(driver, Elements.PST_PrecisionQuickSearch_NumberOfInputChannels_Max_InputBox), util.GetInputBoxValue(driver, Elements.PST_Channels_Column_Max_Value));

                //--- Action: In the Number of Input Channels quick filter, drag the min slider bar to the right ---//
                action.GivenIDragAndDrop(driver, Elements.PST_PrecisionQuickSearch_NumberOfInputChannels_Min_Slider, 10, 0);

                //--- Expected Result: Min textbox should change value depending on the set position of the Min slider bar ---//
                test.validateStringChanged(driver, util.GetInputBoxValue(driver, Elements.PST_PrecisionQuickSearch_NumberOfInputChannels_Min_InputBox), numberOfInputChannels_min_default_value);

                //--- Action: In the resolution quick filter, drag the max slider bar to the left ---//
                action.GivenIDragAndDrop(driver, Elements.PST_PrecisionQuickSearch_NumberOfInputChannels_Max_Slider, -10, 0);

                //--- Expected Result: Max textbox should change value depending on the set position of the max slider bar ---//
                test.validateStringChanged(driver, util.GetInputBoxValue(driver, Elements.PST_PrecisionQuickSearch_NumberOfInputChannels_Max_InputBox), numberOfInputChannels_max_default_value);
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.PST_PrecisionQuickSearch_NumberOfInputChannels_Min_Slider);
                test.validateElementIsPresent(driver, Elements.PST_PrecisionQuickSearch_NumberOfInputChannels_Max_Slider);
            }
        }
    }
}