﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using OpenQA.Selenium;
using System;
using System.Threading;

namespace AS_Test_Framework.PST
{
    [TestFixture]
    public class PST_ColumnHeaderFilterAndSort_CheckboxFilters : BaseSetUp
    {
        public PST_ColumnHeaderFilterAndSort_CheckboxFilters() : base() { }

        //--- URLs ---//
        string pst_page_url = "/parametricsearch/11470";

        //--- Labels ---//
        string valuesSelected_text = "Values Selected";
        string noResults_text = "The search yielded no results";

        [Test, Category("PST"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Checkbox Filters are Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Checkbox Filters are Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Checkbox Filters are Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Checkbox Filters are Present and Working as Expected in RU Locale")]
        public void PST_ColumnHeaderFilterAndSort_VerifyCheckboxFilters(string Locale)
        {
            //--- Action: Access any Dynamic PST page. ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + pst_page_url);

            //--- Get the Default Values ---//
            int default_column_count = util.GetCount(driver, Elements.PST_Columns);
            int default_row_count = util.GetCount(driver, Elements.PST_PartNumber_Column_Rows);

            //----- PST Front End TestPlan - Redesign > Test Case Tab > R5 > TC4_3: Verify that count of how many checkboxes are checked are updated correctly (AL-15510) -----//

            //--- Action: Choose any column with checkbox filter ---//
            string checkbox_column_dataParameter_value = null;
            for (int column_counter = 1; column_counter <= default_column_count; column_counter++)
            {
                if (util.CheckElement(driver, By.CssSelector("section[class='details']>section[class='parameters']>section:nth-of-type(" + column_counter + ") * div[class='toggle']"), 1))
                {
                    checkbox_column_dataParameter_value = util.ReturnAttribute(driver, By.CssSelector("section[class='details']>section[class='parameters']>section:nth-of-type(" + column_counter + ")"), "data-parameter");
                    break;
                }
            }
            Console.WriteLine("\tdata-parameter Value = " + checkbox_column_dataParameter_value);

            //--- Locators ---//
            string column_header_locator = "section[data-parameter='" + checkbox_column_dataParameter_value + "']";
            string active_column_header_locator = "section[class*='filter active'][data-parameter='" + checkbox_column_dataParameter_value + "']";
            string column_header_toggle_locator = "section[data-parameter='" + checkbox_column_dataParameter_value + "'] * div[class='toggle']";
            string checkboxes_locator = "section[data-parameter='" + checkbox_column_dataParameter_value + "'] * div[class^='checkbox']";
            string disabled_checkboxes_locator = "section[data-parameter='" + checkbox_column_dataParameter_value + "'] * div[class='checkbox disabled']";
            string selectAll_checkbox_locator = "section[data-parameter='" + checkbox_column_dataParameter_value + "'] * div[class='checkbox']>input[id$='select-all']";

            action.IClick(driver, Elements.PST_ChooseParameters_Button);
            action.IClick(driver, Elements.PST_ChooseParameters_DialogBox_DeselectAll_Button);
            action.IClick(driver, By.CssSelector("section[class='choose parameters'] * input[id$='" + checkbox_column_dataParameter_value + "']"));
            action.IClick(driver, Elements.PST_ChooseParameters_DialogBox_Ok_Button);
            Thread.Sleep(3000);

            //--- Action: Take note of count in the checkbox dropdown label displayed as: <number of checked checkboxes> Values Selected ---//
            string valuesSelected_count = util.ExtractNumber(driver, By.CssSelector(column_header_toggle_locator));
            //Console.WriteLine("Values Selected - Count = " + valuesSelected_count);

            //--- Action: Mouser-hover on the Values Selected Label ---//
            action.IMouseOverTo(driver, By.CssSelector(column_header_toggle_locator));

            //--- Action: Get the Count the Selected Checkboxes ---//
            int default_selected_checkboxes_count = 0;
            int checkboxes_count = util.GetCount(driver, By.CssSelector(checkboxes_locator));
            for (int checkboxes_counter = 2; checkboxes_counter <= checkboxes_count; checkboxes_counter++)
            {
                if (driver.FindElement(By.CssSelector(checkboxes_locator + ":nth-of-type(" + checkboxes_counter + ")>input[type='checkbox']")).Selected)
                {
                    default_selected_checkboxes_count++;
                }
            }
            //Console.WriteLine("Default Selected Checkboxes - Count = " + default_selected_checkboxes_count);

            //--- Action: Uncheck any checkbox in the Checkbox filter other than the "Select All" checkbox ---//
            for (int checkboxes_counter = 2; checkboxes_counter <= checkboxes_count; checkboxes_counter++)
            {
                if (!util.CheckElement(driver, By.CssSelector(disabled_checkboxes_locator + ":nth-of-type(" + checkboxes_counter + ")"), 1))
                {
                    action.IMouseOverTo(driver, By.CssSelector(column_header_toggle_locator));
                    action.IUncheck(driver, By.CssSelector(checkboxes_locator + ":nth-of-type(" + checkboxes_counter + ")>input[type='checkbox']"));

                    //----- PST Front End TestPlan - Redesign > Test Case Tab > R5 > TC4: Verify Checkbox filter -----//

                    //--- Expected Result: The unchecked checkbox should NOT be disabled ---//
                    test.validateElementIsEnabled(driver, By.CssSelector(checkboxes_locator + ":nth-of-type(" + checkboxes_counter + ")>input[type='checkbox']"));
                }
            }

            //----- PST Front End TestPlan - Redesign > Test Case Tab > R5 > TC4_3: Verify that count of how many checkboxes are checked are updated correctly (AL-15510) -----//

            //--- Action: Get the Count the Selected Checkboxes ---//
            int selected_checkboxes_count = 0;
            for (int checkboxes_counter = 1; checkboxes_counter <= checkboxes_count; checkboxes_counter++)
            {
                if (driver.FindElement(By.CssSelector(checkboxes_locator + ":nth-of-type(" + checkboxes_counter + ")>input[type='checkbox']")).Selected)
                {
                    selected_checkboxes_count++;
                }
            }
            //Console.WriteLine("Selected Checkboxes - Count = " + selected_checkboxes_count);

            //--- Expected Result: Checkbox dropdown count label should be updated ---//
            if (Locale.Equals("en"))
            {
                test.validateStringIsCorrect(driver, By.CssSelector(column_header_toggle_locator), selected_checkboxes_count + " " + valuesSelected_text);
            }
            else
            {
                test.validateStringInstance(driver, util.GetText(driver, By.CssSelector(column_header_toggle_locator)), selected_checkboxes_count.ToString());
            }
        }

        [Test, Category("PST"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Select All Checkbox is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Select All Checkbox is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Select All Checkbox is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Select All Checkbox is Present and Working as Expected in RU Locale")]
        public void PST_ColumnHeaderFilterAndSort_CheckboxFilters_VerifySelectAllCheckbox(string Locale)
        {
            //--- Action: Access any Dynamic PST page. ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + pst_page_url);

            //--- Get the Default Values ---//
            int default_column_count = util.GetCount(driver, Elements.PST_Columns);
            int default_row_count = util.GetCount(driver, Elements.PST_PartNumber_Column_Rows);

            //----- PST Front End TestPlan - Redesign > Test Case Tab > R5 > TC4_3: Verify that count of how many checkboxes are checked are updated correctly (AL-15510) -----//

            //--- Action: Choose any column with checkbox filter ---//
            string checkbox_column_dataParameter_value = null;
            for (int column_counter = 1; column_counter <= default_column_count; column_counter++)
            {
                if (util.CheckElement(driver, By.CssSelector("section[class='details']>section[class='parameters']>section:nth-of-type(" + column_counter + ") * div[class='toggle']"), 1))
                {
                    checkbox_column_dataParameter_value = util.ReturnAttribute(driver, By.CssSelector("section[class='details']>section[class='parameters']>section:nth-of-type(" + column_counter + ")"), "data-parameter");
                    break;
                }
            }
            Console.WriteLine("\tdata-parameter Value = " + checkbox_column_dataParameter_value);

            //--- Locators ---//
            string column_header_locator = "section[data-parameter='" + checkbox_column_dataParameter_value + "']";
            string active_column_header_locator = "section[class*='filter active'][data-parameter='" + checkbox_column_dataParameter_value + "']";
            string column_header_toggle_locator = "section[data-parameter='" + checkbox_column_dataParameter_value + "'] * div[class='toggle']";
            string checkboxes_locator = "section[data-parameter='" + checkbox_column_dataParameter_value + "'] * div[class^='checkbox']";
            string disabled_checkboxes_locator = "section[data-parameter='" + checkbox_column_dataParameter_value + "'] * div[class='checkbox disabled']";
            string selectAll_checkbox_locator = "section[data-parameter='" + checkbox_column_dataParameter_value + "'] * div[class='checkbox']>input[id$='select-all']";

            action.IClick(driver, Elements.PST_ChooseParameters_Button);
            action.IClick(driver, Elements.PST_ChooseParameters_DialogBox_DeselectAll_Button);
            action.IClick(driver, By.CssSelector("section[class='choose parameters'] * input[id$='" + checkbox_column_dataParameter_value + "']"));
            action.IClick(driver, Elements.PST_ChooseParameters_DialogBox_Ok_Button);
            Thread.Sleep(6000);

            //--- Action: Take note of count in the checkbox dropdown label displayed as: <number of checked checkboxes> Values Selected ---//
            string valuesSelected_count = util.ExtractNumber(driver, By.CssSelector(column_header_toggle_locator));
            //Console.WriteLine("Values Selected - Count = " + valuesSelected_count);

            //--- Action: Mouser-hover on the Values Selected Label ---//
            action.IMouseOverTo(driver, By.CssSelector(column_header_toggle_locator));

            //--- Action: Get the Count the Selected Checkboxes ---//
            int default_selected_checkboxes_count = 0;
            int checkboxes_count = util.GetCount(driver, By.CssSelector(checkboxes_locator));
            for (int checkboxes_counter = 2; checkboxes_counter <= checkboxes_count; checkboxes_counter++)
            {
                if (driver.FindElement(By.CssSelector(checkboxes_locator + ":nth-of-type(" + checkboxes_counter + ")>input[type='checkbox']")).Selected)
                {
                    default_selected_checkboxes_count++;
                }
            }
            //Console.WriteLine("Default Selected Checkboxes - Count = " + default_selected_checkboxes_count);

            //--- Expected Result: Number of checked checkboxes should be the same as the checkbox dropdown count label ---//
            test.validateCountIsEqual(driver, Int32.Parse(valuesSelected_count), default_selected_checkboxes_count);

            //--- Action: Uncheck Select All checkbox ---//
            action.IUncheck(driver, By.CssSelector(selectAll_checkbox_locator));
            Thread.Sleep(1000);

            //----- PST Front End TestPlan - Redesign > Test Case Tab > R5 > TC4: Verify Checkbox filter -----//

            //--- Expected Result: Parametric Search table should not display result with displayed message "The search yielded no results" ---//
            if (Locale.Equals("en"))
            {
                test.validateStringIsCorrect(driver, Elements.PST_Rows_NoResults_Label, noResults_text);
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.PST_Rows_NoResults_Label);
            }

            //----- PST Front End TestPlan - Redesign > Test Case Tab > R5 > TC4_3: Verify that count of how many checkboxes are checked are updated correctly (AL-15510) -----//

            //--- Expected Result: Checkbox dropdown label should be displayed as "0 Values Selected" ---//
            if (Locale.Equals("en"))
            {
                test.validateStringIsCorrect(driver, By.CssSelector(column_header_toggle_locator), "0 " + valuesSelected_text);
            }
            else
            {
                test.validateStringInstance(driver, util.GetText(driver, By.CssSelector(column_header_toggle_locator)), "0");
            }

            //--- Action: Check the Select All Checkbox ---//
            action.IMouseOverTo(driver, By.CssSelector(column_header_toggle_locator));
            action.ICheck(driver, By.CssSelector(selectAll_checkbox_locator));

            //----- PST Front End TestPlan - Redesign > Test Case Tab > R5 > TC4: Verify Checkbox filter -----//

            //--- Expected Result: The Blue Highlight on the Column will be Removed (AL-11222/IQ-4354) ---//
            test.validateElementIsNotPresent(driver, By.CssSelector(active_column_header_locator));

            //--- Expected Result: Results should be displayed ---//
            test.validateCountIsEqual(driver, default_row_count, util.GetCount(driver, Elements.PST_Rows));

            //----- PST Front End TestPlan - Redesign > Test Case Tab > R5 > TC4_3: Verify that count of how many checkboxes are checked are updated correctly (AL-15510) -----//

            //--- Expected Result: Checkbox dropdown label should revert to default count ---//
            if (Locale.Equals("en"))
            {
                test.validateStringIsCorrect(driver, By.CssSelector(column_header_toggle_locator), default_selected_checkboxes_count + " " + valuesSelected_text);
            }
            else
            {
                test.validateStringInstance(driver, util.GetText(driver, By.CssSelector(column_header_toggle_locator)), default_selected_checkboxes_count.ToString());
            }
        }
    }
}