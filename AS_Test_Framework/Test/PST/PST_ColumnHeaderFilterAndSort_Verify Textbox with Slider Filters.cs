﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using OpenQA.Selenium;
using System;
using System.Threading;

namespace AS_Test_Framework.PST
{
    [TestFixture]
    public class PST_ColumnHeaderFilterAndSort_TextboxWithSliderFilters : BaseSetUp
    {
        public PST_ColumnHeaderFilterAndSort_TextboxWithSliderFilters() : base() { }

        //--- URLs ---//
        string pst_page_url = "/parametricsearch/11470";

        public string GetColumn_WithMinAndMaxColumnInputLimits()
        {
            string textboxWithSlider_column_dataParameter_value = null;

            int default_column_count = util.GetCount(driver, Elements.PST_Columns);
            for (int column_counter = 1; column_counter <= default_column_count; column_counter++)
            {
                if (util.CheckElement(driver, By.CssSelector("section[class='details']>section[class='parameters']>section:nth-of-type(" + column_counter + ") * input[type='text']"), 1))
                {
                    textboxWithSlider_column_dataParameter_value = util.ReturnAttribute(driver, By.CssSelector("section[class='details']>section[class='parameters']>section:nth-of-type(" + column_counter + ")"), "data-parameter");
                    break;
                }
            }

            return textboxWithSlider_column_dataParameter_value;
        }

        [Test, Category("PST"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Min Limit Textbox with Slider Filters are Present and Working for Minimized Columns as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Min Limit Textbox with Slider Filters are Present and Working for Minimized Columns as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Min Limit Textbox with Slider Filters are Present and Working for Minimized Columns as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Min Limit Textbox with Slider Filters are Present and Working for Minimized Columns as Expected in RU Locale")]
        public void PST_ColumnHeaderFilterAndSort_TextboxWithSliderFilters_VerifyMinLimitTextboxForMinimizedColumns(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- Action: Access any Dynamic PST page. ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + pst_page_url);

            //--- Action: Find a column with min/max column input limits ---//
            string textboxWithSlider_column_dataParameter_value = GetColumn_WithMinAndMaxColumnInputLimits();
            Console.WriteLine("\tdata-parameter Value = " + textboxWithSlider_column_dataParameter_value);

            //--- Locators ---//
            string minLimit_inputBox_locator = "section[data-parameter='" + textboxWithSlider_column_dataParameter_value + "'] * input[data-type='min']";
            string textboxWithSlider_1st_row_value = "div[data-parameter='" + textboxWithSlider_column_dataParameter_value + "']";

            action.IClick(driver, Elements.PST_ChooseParameters_Button);
            action.IClick(driver, Elements.PST_ChooseParameters_DialogBox_DeselectAll_Button);
            action.IClick(driver, By.CssSelector("section[class='choose parameters'] * input[id$='" + textboxWithSlider_column_dataParameter_value + "']"));
            action.IClick(driver, Elements.PST_ChooseParameters_DialogBox_Ok_Button);

            //----- PST Front End TestPlan - Redesign > Test Case Tab > R5 > TC5_1: Verify Numeric Filter -----//

            ////--- Get the Default Values ---//
            //string minLimit_default_value = util.GetInputBoxValue(driver, By.CssSelector(minLimit_inputBox_locator));

            if (!util.CheckElement(driver, Elements.PST_Minimized_Columns, 1))
            {
                //--- Action: Click "Minimize Filters" button ---//
                action.IClick(driver, Elements.PST_MaximizeFilters_MinimizeFilters_Button);
            }

            //--- Action: Change the value of the Min limit textbox (e.g. "2.55") ---//
            string minLimit_input = (util.ConverterToMetric(util.GetInputBoxValue(driver, By.CssSelector(minLimit_inputBox_locator))) + 1).ToString();
            action.IDeleteValueOnFields(driver, By.CssSelector(minLimit_inputBox_locator));
            action.IType(driver, By.CssSelector(minLimit_inputBox_locator), minLimit_input);
            action.IType(driver, By.CssSelector(minLimit_inputBox_locator), Keys.Tab);

            //--- Expected Result: Table should be filtered based on the entered min limit ---//
            //Console.WriteLine("1st Row - Value = " + util.ConverterToMetric(util.GetText(driver, By.CssSelector(textboxWithSlider_1st_row_value))));
            //Console.WriteLine("Min Limit - Input = " + util.ConverterToMetric(minLimit_input));
            test.validateCountIsGreaterOrEqual(driver, util.ConverterToMetric(util.GetText(driver, By.CssSelector(textboxWithSlider_1st_row_value))), util.ConverterToMetric(minLimit_input));

            ////----- PST Front End TestPlan - Redesign > Test Case Tab > R5 > TC5_6: Verify that clearing the value of filter textbox does not change its value to "0" -----// --> PENDING. Manual Testing Team to confirm if this is still valid. In the ADC Drivers PST, the Min Limit doesn't return to its Default Value. "2" is displayed instead of 2.5M

            ////--- Action: Clear the value of the Min limit textbox ---//
            //action.IDeleteValueOnFields(driver, By.CssSelector(minLimit_inputBox_locator));

            ////--- Action: Change the cursor focus out of the Min limit textbox ---//
            //action.IType(driver, By.CssSelector(minLimit_inputBox_locator), Keys.Tab);

            ////--- Expected Result: Min limit textbox upon out of cursor focus should reset its value to default ---//
            //Console.WriteLine("Min Limit - Default Value = " + minLimit_default_value);
            //Console.WriteLine("Min Limit - Value = " + util.GetInputBoxValue(driver, By.CssSelector(minLimit_default_value)));
            //test.validateStringInstance(driver, minLimit_default_value, util.GetInputBoxValue(driver, By.CssSelector(minLimit_inputBox_locator)));
        }

        [Test, Category("PST"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Max Limit Textbox with Slider Filters are Present and Working for Minimized Columns as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Max Limit Textbox with Slider Filters are Present and Working for Minimized Columns as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Max Limit Textbox with Slider Filters are Present and Working for Minimized Columns as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Max Limit Textbox with Slider Filters are Present and Working for Minimized Columns as Expected in RU Locale")]
        public void PST_ColumnHeaderFilterAndSort_TextboxWithSliderFilters_VerifyMaxLimitTextboxForMinimizedColumns(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- PST URL ---//
            string pst_url = Configuration.Env_Url + Locale + pst_page_url;

            //--- Action: Access any Dynamic PST page. ---//
            action.Navigate(driver, pst_url);

            //--- Action: Find a column with min/max column input limits ---//
            string textboxWithSlider_column_dataParameter_value = GetColumn_WithMinAndMaxColumnInputLimits();
            Console.WriteLine("\tdata-parameter Value = " + textboxWithSlider_column_dataParameter_value);

            string column_header = util.GetText(driver, By.CssSelector("section[data-parameter='" + textboxWithSlider_column_dataParameter_value + "'] * div[class='parameter name']"));

            //--- Locators ---//
            string maxLimit_inputBox_locator = "section[data-parameter='" + textboxWithSlider_column_dataParameter_value + "'] * input[data-type='max']";
            string textboxWithSlider_1st_row_value = "div[data-parameter='" + textboxWithSlider_column_dataParameter_value + "']";

            action.IClick(driver, Elements.PST_ChooseParameters_Button);
            action.IClick(driver, Elements.PST_ChooseParameters_DialogBox_DeselectAll_Button);
            action.IClick(driver, By.CssSelector("section[class='choose parameters'] * input[id$='" + textboxWithSlider_column_dataParameter_value + "']"));
            action.IClick(driver, Elements.PST_ChooseParameters_DialogBox_Ok_Button);
            Thread.Sleep(1000);

            //----- PST Front End TestPlan - Redesign > Test Case Tab > R5 > TC5_1: Verify Numeric Filter -----//

            //--- Get the Default Values ---//
            string maxLimit_default_value = util.GetInputBoxValue(driver, By.CssSelector(maxLimit_inputBox_locator));

            if (!util.CheckElement(driver, Elements.PST_Minimized_Columns, 1))
            {
                //--- Action: Click "Minimize Filters" button ---//
                action.IClick(driver, Elements.PST_MaximizeFilters_MinimizeFilters_Button);
            }

            //--- Action: Change the value of the Max limit textbox (e.g. "11") ---//
            string maxLimit_input = (util.ConverterToMetric(util.GetInputBoxValue(driver, By.CssSelector(maxLimit_inputBox_locator))) - 1).ToString();
            action.IDeleteValueOnFields(driver, By.CssSelector(maxLimit_inputBox_locator));
            action.IType(driver, By.CssSelector(maxLimit_inputBox_locator), maxLimit_input);
            action.IType(driver, By.CssSelector(maxLimit_inputBox_locator), Keys.Tab);

            //--- Expected Result: Table should be filtered based on the entered max limit ---//
            //Console.WriteLine("1st Row - Value = " + util.ConverterToMetric(util.GetText(driver, By.CssSelector(textboxWithSlider_1st_row_value))));
            //Console.WriteLine("Max Limit - Value = " + util.GetInputBoxValue(driver, By.CssSelector(maxLimit_inputBox_locator)));
            test.validateCountIsLessOrEqual(driver, util.ConverterToMetric(util.GetText(driver, By.CssSelector(textboxWithSlider_1st_row_value))), util.ConverterToMetric(util.GetInputBoxValue(driver, By.CssSelector(maxLimit_inputBox_locator))));

            //----- PST Front End TestPlan - Redesign > Test Case Tab > R5 > TC5_6: Verify that clearing the value of filter textbox does not change its value to "0" -----//

            //--- Action: Clear the value of the Max limit textbox ---//
            action.IDeleteValueOnFields(driver, By.CssSelector(maxLimit_inputBox_locator));

            //--- Action: Change the cursor focus out of the Max limit textbox ---//
            action.IType(driver, By.CssSelector(maxLimit_inputBox_locator), Keys.Tab);

            //--- Expected Result: Max limit textbox should reset to default value ---//
            //Console.WriteLine("Max Limit - Default Value = " + maxLimit_default_value);
            //Console.WriteLine("Max Limit - Value = " + util.GetInputBoxValue(driver, By.CssSelector(maxLimit_inputBox_locator)));
            test.validateStringInstance(driver, maxLimit_default_value, util.GetInputBoxValue(driver, By.CssSelector(maxLimit_inputBox_locator)));
        }

        [Test, Category("PST"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Min Limit Textbox with Slider Filters are Present and Working as Expected for Maximized Columns after Changing the Value in the Textbox in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Min Limit Textbox with Slider Filters are Present and Working as Expected for Maximized Columns after Changing the Value in the Textbox in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Min Limit Textbox with Slider Filters are Present and Working as Expected for Maximized Columns after Changing the Value in the Textbox in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Min Limit Textbox with Slider Filters are Present and Working as Expected for Maximized Columns after Changing the Value in the Textbox in RU Locale")]
        public void PST_ColumnHeaderFilterAndSort_TextboxWithSliderFilters_VerifyMinLimitTextboxForMaximizedColumnsAfterChangingTheValueInTheTextbox(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- Action: Access any Dynamic PST page. ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + pst_page_url);

            //--- Action: Find a column with min/max column input limits ---//
            string textboxWithSlider_column_dataParameter_value = GetColumn_WithMinAndMaxColumnInputLimits();
            Console.WriteLine("\tdata-parameter Value = " + textboxWithSlider_column_dataParameter_value);

            //--- Locators ---//
            string minLimit_inputBox_locator = "section[data-parameter='" + textboxWithSlider_column_dataParameter_value + "'] * input[data-type='min']";
            string textboxWithSlider_1st_row_value = "div[data-parameter='" + textboxWithSlider_column_dataParameter_value + "']";

            action.IClick(driver, Elements.PST_ChooseParameters_Button);
            action.IClick(driver, Elements.PST_ChooseParameters_DialogBox_DeselectAll_Button);
            action.IClick(driver, By.CssSelector("section[class='choose parameters'] * input[id$='" + textboxWithSlider_column_dataParameter_value + "']"));
            action.IClick(driver, Elements.PST_ChooseParameters_DialogBox_Ok_Button);
            Thread.Sleep(1000);

            //----- PST Front End TestPlan - Redesign > Test Case Tab > R5 > TC5_1: Verify Numeric Filter -----//

            //--- Get the Default Values ---//
            string minLimit_default_value = util.GetInputBoxValue(driver, By.CssSelector(minLimit_inputBox_locator));

            if (util.CheckElement(driver, Elements.PST_Minimized_Columns, 1))
            {
                //--- Action: Click Maximize Filters" button ---//
                action.IClick(driver, Elements.PST_MaximizeFilters_MinimizeFilters_Button);
            }

            //--- Expected Result: Min limit values should be retained ---//
            test.validateStringInstance(driver, minLimit_default_value, util.GetInputBoxValue(driver, By.CssSelector(minLimit_inputBox_locator)));

            //--- Expected Result: Table should retain its filter ---//
            test.validateCountIsGreaterOrEqual(driver, util.ConverterToMetric(util.GetText(driver, By.CssSelector(textboxWithSlider_1st_row_value))), util.ConverterToMetric(util.GetInputBoxValue(driver, By.CssSelector(minLimit_inputBox_locator))));

            //--- Action: Change the value of Min limit textbox ---//
            string minLimit_input = (util.ConverterToMetric(util.GetInputBoxValue(driver, By.CssSelector(minLimit_inputBox_locator))) + 1).ToString();
            action.IDeleteValueOnFields(driver, By.CssSelector(minLimit_inputBox_locator));
            action.IType(driver, By.CssSelector(minLimit_inputBox_locator), minLimit_input);
            action.IType(driver, By.CssSelector(minLimit_inputBox_locator), Keys.Tab);

            //--- Expected Result: Table should be filtered based on the set min limit ---//
            test.validateCountIsGreaterOrEqual(driver, util.ConverterToMetric(util.GetText(driver, By.CssSelector(textboxWithSlider_1st_row_value))), util.ConverterToMetric(minLimit_input));

            //--- Get the Values ---//
            string minLimit_value = util.GetInputBoxValue(driver, By.CssSelector(minLimit_inputBox_locator));

            if (!util.CheckElement(driver, Elements.PST_Minimized_Columns, 1))
            {
                //--- Action: Click "Minimize Filters" button ---//
                action.IClick(driver, Elements.PST_MaximizeFilters_MinimizeFilters_Button);
            }

            //--- Expected Result: Min limit values should be retained ---//
            test.validateStringInstance(driver, minLimit_value, util.GetInputBoxValue(driver, By.CssSelector(minLimit_inputBox_locator)));

            //--- Expected Result: Table filter should be retained ---//
            test.validateCountIsGreaterOrEqual(driver, util.ConverterToMetric(util.GetText(driver, By.CssSelector(textboxWithSlider_1st_row_value))), util.ConverterToMetric(util.GetInputBoxValue(driver, By.CssSelector(minLimit_inputBox_locator))));
        }

        [Test, Category("PST"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Min Limit Textbox with Slider Filters are Present and Working as Expected for Maximized Columns after Moving the Slider in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Min Limit Textbox with Slider Filters are Present and Working as Expected for Maximized Columns after Moving the Slider in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Min Limit Textbox with Slider Filters are Present and Working as Expected for Maximized Columns after Moving the Slider in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Min Limit Textbox with Slider Filters are Present and Working as Expected for Maximized Columns after Moving the Slider in RU Locale")]
        public void PST_ColumnHeaderFilterAndSort_TextboxWithSliderFilters_VerifyMinLimitTextboxForMaximizedColumnsAfterMovingTheSlider(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- Action: Access any Dynamic PST page. ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + pst_page_url);

            //--- Action: Find a column with min/max column input limits ---//
            string textboxWithSlider_column_dataParameter_value = GetColumn_WithMinAndMaxColumnInputLimits();
            Console.WriteLine("\tdata-parameter Value = " + textboxWithSlider_column_dataParameter_value);

            //--- Locators ---//
            string active_column_header_locator = "section[class*='filter active'][data-parameter='" + textboxWithSlider_column_dataParameter_value + "']";
            string minLimit_slider_locator = "section[data-parameter='" + textboxWithSlider_column_dataParameter_value + "'] * div[class$='rc-slider-handle-1']";
            string minLimit_inputBox_locator = "section[data-parameter='" + textboxWithSlider_column_dataParameter_value + "'] * input[data-type='min']";
            string textboxWithSlider_1st_row_value = "div[data-parameter='" + textboxWithSlider_column_dataParameter_value + "']";

            action.IClick(driver, Elements.PST_ChooseParameters_Button);
            action.IClick(driver, Elements.PST_ChooseParameters_DialogBox_DeselectAll_Button);
            action.IClick(driver, By.CssSelector("section[class='choose parameters'] * input[id$='" + textboxWithSlider_column_dataParameter_value + "']"));
            action.IClick(driver, Elements.PST_ChooseParameters_DialogBox_Ok_Button);

            //----- PST Front End TestPlan - Redesign > Test Case Tab > R5 > TC5_1: Verify Numeric Filter -----//

            //--- Get the Default Values ---//
            string minLimit_default_value = util.GetInputBoxValue(driver, By.CssSelector(minLimit_inputBox_locator));

            if (util.CheckElement(driver, Elements.PST_Minimized_Columns, 1))
            {
                //--- Action: Click Maximize Filters" button ---//
                action.IClick(driver, Elements.PST_MaximizeFilters_MinimizeFilters_Button);
            }

            //--- Get the Values ---//
            string minLimit_value = util.GetInputBoxValue(driver, By.CssSelector(minLimit_inputBox_locator));

            //--- Action: Move the Min slider bar ---//
            action.GivenIDragAndDrop(driver, By.CssSelector(minLimit_slider_locator), 0, -10);

            //--- Expected Result: Value in the Min limit textbox should reflect the change in the position of the Min slider bar ---//
            test.validateStringChanged(driver, util.GetInputBoxValue(driver, By.CssSelector(minLimit_inputBox_locator)), minLimit_value);

            //--- Expected Result: Table should be filtered based on the set max limit ---//
            test.validateCountIsGreaterOrEqual(driver, util.ConverterToMetric(util.GetText(driver, By.CssSelector(textboxWithSlider_1st_row_value))), util.ConverterToMetric(util.GetInputBoxValue(driver, By.CssSelector(minLimit_inputBox_locator))));

            //--- Get the Values ---//
            minLimit_value = util.GetInputBoxValue(driver, By.CssSelector(minLimit_inputBox_locator));

            if (!util.CheckElement(driver, Elements.PST_Minimized_Columns, 1))
            {
                //--- Action: Click "Minimize Filters" button ---//
                action.IClick(driver, Elements.PST_MaximizeFilters_MinimizeFilters_Button);
            }

            //--- Expected Result: Min limit values should be retained ---//
            test.validateStringInstance(driver, minLimit_value, util.GetInputBoxValue(driver, By.CssSelector(minLimit_inputBox_locator)));

            //--- Expected Result: Table filter should be retained ---//
            test.validateCountIsGreaterOrEqual(driver, util.ConverterToMetric(util.GetText(driver, By.CssSelector(textboxWithSlider_1st_row_value))), util.ConverterToMetric(util.GetInputBoxValue(driver, By.CssSelector(minLimit_inputBox_locator))));

            //----- PST Front End TestPlan - Redesign > Test Case Tab > R5 > TC5: Verify Textbox with Slider filter -----//

            if (util.CheckElement(driver, Elements.PST_Minimized_Columns, 1))
            {
                //--- Action: Click "Maximize Filters" button ---//
                action.IClick(driver, Elements.PST_MaximizeFilters_MinimizeFilters_Button);
            }

            //--- Action: Return the textbox value to default values ---//
            action.IDeleteValueOnFields(driver, By.CssSelector(minLimit_inputBox_locator));
            action.IType(driver, By.CssSelector(minLimit_inputBox_locator), minLimit_default_value);
            action.IType(driver, By.CssSelector(minLimit_inputBox_locator), Keys.Tab);

            //--- Expected Result: The Blue Highlight on the Column will be Removed. ---//
            test.validateElementIsNotPresent(driver, By.CssSelector(active_column_header_locator));
        }

        [Test, Category("PST"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Max Limit Textbox with Slider Filters are Present and Working as Expected for Maximized Columns after Changing the Value in the Textbox in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Max Limit Textbox with Slider Filters are Present and Working as Expected for Maximized Columns after Changing the Value in the Textbox in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Max Limit Textbox with Slider Filters are Present and Working as Expected for Maximized Columns after Changing the Value in the Textbox in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Max Limit Textbox with Slider Filters are Present and Working as Expected for Maximized Columns after Changing the Value in the Textbox in RU Locale")]
        public void PST_ColumnHeaderFilterAndSort_TextboxWithSliderFilters_VerifyMaxLimitTextboxForMaximizedColumnsAfterChangingTheValueInTheTextbox(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- PST URL ---//
            string pst_url = Configuration.Env_Url + Locale + pst_page_url;

            //--- Action: Access any Dynamic PST page. ---//
            action.Navigate(driver, pst_url);

            //--- Action: Find a column with min/max column input limits ---//
            string textboxWithSlider_column_dataParameter_value = GetColumn_WithMinAndMaxColumnInputLimits();
            Console.WriteLine("\tdata-parameter Value = " + textboxWithSlider_column_dataParameter_value);

            string column_header = util.GetText(driver, By.CssSelector("section[data-parameter='" + textboxWithSlider_column_dataParameter_value + "'] * div[class='parameter name']"));

            //--- Locators ---//
            string maxLimit_inputBox_locator = "section[data-parameter='" + textboxWithSlider_column_dataParameter_value + "'] * input[data-type='max']";
            string textboxWithSlider_1st_row_value = "div[data-parameter='" + textboxWithSlider_column_dataParameter_value + "']";

            action.IClick(driver, Elements.PST_ChooseParameters_Button);
            action.IClick(driver, Elements.PST_ChooseParameters_DialogBox_DeselectAll_Button);
            action.IClick(driver, By.CssSelector("section[class='choose parameters'] * input[id$='" + textboxWithSlider_column_dataParameter_value + "']"));
            action.IClick(driver, Elements.PST_ChooseParameters_DialogBox_Ok_Button);
            Thread.Sleep(1000);

            //----- PST Front End TestPlan - Redesign > Test Case Tab > R5 > TC5_1: Verify Numeric Filter -----//

            //--- Get the Default Values ---//
            string maxLimit_default_value = util.GetInputBoxValue(driver, By.CssSelector(maxLimit_inputBox_locator));

            if (util.CheckElement(driver, Elements.PST_Minimized_Columns, 1))
            {
                //--- Action: Click Maximize Filters" button ---//
                action.IClick(driver, Elements.PST_MaximizeFilters_MinimizeFilters_Button);
                Thread.Sleep(3000);
            }

            //--- Expected Result: Max limit values should be retained ---//
            test.validateStringInstance(driver, maxLimit_default_value, util.GetInputBoxValue(driver, By.CssSelector(maxLimit_inputBox_locator)));

            //--- Expected Result: Table should retain its filter ---//
            test.validateCountIsLessOrEqual(driver, util.ConverterToMetric(util.GetText(driver, By.CssSelector(textboxWithSlider_1st_row_value))), util.ConverterToMetric(util.GetInputBoxValue(driver, By.CssSelector(maxLimit_inputBox_locator))));

            //--- Action: Change the value of Max limit textbox ---//
            string maxLimit_input = (util.ConverterToMetric(util.GetInputBoxValue(driver, By.CssSelector(maxLimit_inputBox_locator))) - 1).ToString();
            action.IDeleteValueOnFields(driver, By.CssSelector(maxLimit_inputBox_locator));
            action.IType(driver, By.CssSelector(maxLimit_inputBox_locator), maxLimit_input);
            action.IType(driver, By.CssSelector(maxLimit_inputBox_locator), Keys.Tab);

            //--- Expected Result: Table should be filtered based on the set max limit ---//
            //Console.WriteLine("Actual Result: 1st Row - Value = " + util.ConverterToMetric(util.GetText(driver, By.CssSelector(textboxWithSlider_1st_row_value))));
            //Console.WriteLine("Expected Result: Max Limit - Value = " + util.GetInputBoxValue(driver, By.CssSelector(maxLimit_inputBox_locator)));
            test.validateCountIsLessOrEqual(driver, util.ConverterToMetric(util.GetText(driver, By.CssSelector(textboxWithSlider_1st_row_value))), util.ConverterToMetric(util.GetInputBoxValue(driver, By.CssSelector(maxLimit_inputBox_locator))));

            //--- Get the Values ---//
            string maxLimit_value = util.GetInputBoxValue(driver, By.CssSelector(maxLimit_inputBox_locator));

            if (!util.CheckElement(driver, Elements.PST_Minimized_Columns, 1))
            {
                //--- Action: Click "Minimize Filters" button ---//
                action.IClick(driver, Elements.PST_MaximizeFilters_MinimizeFilters_Button);
            }

            //--- Expected Result: Max limit values should be retained ---//
            //Console.WriteLine("Actual Result: Current Max Limit - Value = " + util.GetInputBoxValue(driver, By.CssSelector(maxLimit_inputBox_locator)));
            //Console.WriteLine("Expected Result: Previous Max Limit - Value = " + maxLimit_value);
            test.validateStringInstance(driver, maxLimit_value, util.GetInputBoxValue(driver, By.CssSelector(maxLimit_inputBox_locator)));

            //--- Expected Result: Table filter should be retained ---//
            test.validateCountIsLessOrEqual(driver, util.ConverterToMetric(util.GetText(driver, By.CssSelector(textboxWithSlider_1st_row_value))), util.ConverterToMetric(util.GetInputBoxValue(driver, By.CssSelector(maxLimit_inputBox_locator))));

        }

        [Test, Category("PST"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Max Limit Textbox with Slider Filters are Present and Working as Expected for Maximized Columns after Moving the Slider in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Max Limit Textbox with Slider Filters are Present and Working as Expected for Maximized Columns after Moving the Slider in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Max Limit Textbox with Slider Filters are Present and Working as Expected for Maximized Columns after Moving the Slider in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Max Limit Textbox with Slider Filters are Present and Working as Expected for Maximized Columns after Moving the Slider in RU Locale")]
        public void PST_ColumnHeaderFilterAndSort_TextboxWithSliderFilters_VerifyMaxLimitTextboxForMaximizedColumnsAfterMovingTheSlider(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- Action: Access any Dynamic PST page. ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + pst_page_url);

            //--- Action: Find a column with min/max column input limits ---//
            string textboxWithSlider_column_dataParameter_value = GetColumn_WithMinAndMaxColumnInputLimits();
            Console.WriteLine("\tdata-parameter Value = " + textboxWithSlider_column_dataParameter_value);

            //--- Locators ---//
            string active_column_header_locator = "section[class*='filter active'][data-parameter='" + textboxWithSlider_column_dataParameter_value + "']";
            string maxLimit_slider_locator = "section[data-parameter='" + textboxWithSlider_column_dataParameter_value + "'] * div[class$='rc-slider-handle-2']";
            string maxLimit_inputBox_locator = "section[data-parameter='" + textboxWithSlider_column_dataParameter_value + "'] * input[data-type='max']";
            string textboxWithSlider_1st_row_value = "div[data-parameter='" + textboxWithSlider_column_dataParameter_value + "']";

            action.IClick(driver, Elements.PST_ChooseParameters_Button);
            action.IClick(driver, Elements.PST_ChooseParameters_DialogBox_DeselectAll_Button);
            action.IClick(driver, By.CssSelector("section[class='choose parameters'] * input[id$='" + textboxWithSlider_column_dataParameter_value + "']"));
            action.IClick(driver, Elements.PST_ChooseParameters_DialogBox_Ok_Button);
            Thread.Sleep(2000);

            //----- PST Front End TestPlan - Redesign > Test Case Tab > R5 > TC5_1: Verify Numeric Filter -----//

            //--- Get the Default Values ---//
            string maxLimit_default_value = util.GetInputBoxValue(driver, By.CssSelector(maxLimit_inputBox_locator));

            if (util.CheckElement(driver, Elements.PST_Minimized_Columns, 1))
            {
                //--- Action: Click Maximize Filters" button ---//
                action.IClick(driver, Elements.PST_MaximizeFilters_MinimizeFilters_Button);
            }

            //--- Get the Values ---//
            string maxLimit_value = util.GetInputBoxValue(driver, By.CssSelector(maxLimit_inputBox_locator));

            //--- Action: Move the Max slider bar ---//
            action.GivenIDragAndDrop(driver, By.CssSelector(maxLimit_slider_locator), 0, 10);

            //--- Expected Result: Value in the Max limit textbox should reflect the change in the position of the Max slider bar ---//
            test.validateStringChanged(driver, util.GetInputBoxValue(driver, By.CssSelector(maxLimit_inputBox_locator)), maxLimit_value);

            //--- Expected Result: Table should be filtered based in the set max limit ---//
            test.validateCountIsLessOrEqual(driver, util.ConverterToMetric(util.GetText(driver, By.CssSelector(textboxWithSlider_1st_row_value))), util.ConverterToMetric(util.GetInputBoxValue(driver, By.CssSelector(maxLimit_inputBox_locator))));

            if (!util.CheckElement(driver, Elements.PST_Minimized_Columns, 1))
            {
                //--- Action: Click "Minimize Filters" button ---//
                action.IClick(driver, Elements.PST_MaximizeFilters_MinimizeFilters_Button);
            }

            //----- PST Front End TestPlan - Redesign > Test Case Tab > R5 > TC5: Verify Textbox with Slider filter -----//

            if (util.CheckElement(driver, Elements.PST_Minimized_Columns, 1))
            {
                //--- Action: Click "Maximize Filters" button ---//
                action.IClick(driver, Elements.PST_MaximizeFilters_MinimizeFilters_Button);
            }

            //--- Action: Return the textbox value to default values ---//
            action.IDeleteValueOnFields(driver, By.CssSelector(maxLimit_inputBox_locator));
            action.IType(driver, By.CssSelector(maxLimit_inputBox_locator), maxLimit_default_value);
            action.IType(driver, By.CssSelector(maxLimit_inputBox_locator), Keys.Tab);

            //--- Expected Result: The Blue Highlight on the Column will be Removed. ---//
            test.validateElementIsNotPresent(driver, By.CssSelector(active_column_header_locator));
        }
    }
}