﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;

namespace AS_Test_Framework.PST
{
    [TestFixture]
    public class PST_ColumnHeaderFilterAndSort_ToggleToolsColumnButton : BaseSetUp
    {
        public PST_ColumnHeaderFilterAndSort_ToggleToolsColumnButton() : base() { }

        //--- URLs ---//
        string pst_page_url = "/parametricsearch/11470";

        //--- Labels ---//
        string toggleToolsColumn_text = "Toggle Tools Column";

        [Test, Category("PST"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Toggle Tools Column Button is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Toggle Tools Column Button is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Toggle Tools Column Button is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Toggle Tools Column Button is Present and Working as Expected in RU Locale")]
        public void PST_ColumnHeaderFilterAndSort_VerifyToggleToolsColumnButton(string Locale)
        {
            //--- Action: Access any Dynamic PST page. ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + pst_page_url);

            //--- Expected Result: The Toggle Tools Column Button is present ---//
            test.validateElementIsPresent(driver, Elements.PST_ToggleToolsColumn_Button);

            if (util.CheckElement(driver, Elements.PST_ToggleToolsColumn_Button, 1))
            {
                //----- PST Front End TestPlan - Redesign > Test Case Tab > R5 > TC1: Verify Toggle buttons -----//

                //--- Action: Hover to Toggle Tools column button ---//
                action.IMouseOverTo(driver, Elements.PST_ToggleToolsColumn_Button);

                //--- Expected Result: Tooltips of the Toggle Tools column should be displaying (AL-15725) ---//
                if (Locale.Equals("en"))
                {
                    test.validateStringIsCorrect(driver, Elements.PST_ToggleToolsColumn_Button_ToolTip, toggleToolsColumn_text);
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.PST_ToggleToolsColumn_Button_ToolTip);
                }
            }
        }
    }
}