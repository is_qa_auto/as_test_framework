﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;

namespace AS_Test_Framework.PST
{
    [TestFixture]
    public class PST_TableNavigationMainButtons_ShareButton : BaseSetUp
    {
        public PST_TableNavigationMainButtons_ShareButton() : base() { }

        //--- URLs ---//
        string pst_page_url = "/parametricsearch/11470";

        [Test, Category("PST"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Share Button is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Share Button is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Share Button is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Share Button is Present and Working as Expected in RU Locale")]
        public void PST_TableNavigationMainButtons_VerifyShareButton(string Locale)
        {
            //--- Action: Access any Dynamic PST page. ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + pst_page_url);

            //----- PST Front End TestPlan - Redesign > Test Case Tab > R4 > TC7: Verify "Share" button -----//

            //--- Expected Result: The Share Button is present ---//
            test.validateElementIsPresent(driver, Elements.PST_Share_Button);
        }
    }
}