﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using OpenQA.Selenium;
using System.Threading;

namespace AS_Test_Framework.PST
{
    [TestFixture]
    public class PST_QuickSearch_QuickPowerSearch_MultipleOutputs : BaseSetUp
    {
        public PST_QuickSearch_QuickPowerSearch_MultipleOutputs() : base() { }

        //--- URLs ---//
        string pst_page_url = "/parametricsearch/11436";

        [Test, Category("PST"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Fields are Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Fields are Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Fields are Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Fields are Present and Working as Expected in RU Locale")]
        public void PST_QuickSearch_QuickPowerSearch_MultipleOutputs_VerifyFields(string Locale)
        {
            //--- Action: Access Dynamic PST with a Quick Search Tool ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + pst_page_url);

            //----- PST Front End TestPlan - Redesign > Test Case Tab > R3 > TC4.3: Verify Search with multiple Outputs -----//

            //--- INPUT ---//

            //--- Action: Enter valid input in the Vin(Min) textbox---//
            string vinMin_input = "3.4";
            action.IDeleteValueOnFields(driver, Elements.PST_QuickPowerSearch_VinMin_InputBox);
            action.IType(driver, Elements.PST_QuickPowerSearch_VinMin_InputBox, vinMin_input);
            action.IType(driver, Elements.PST_QuickPowerSearch_VinMin_InputBox, Keys.Tab);

            //--- Action: Enter valid input in the Vin(Max) textbox ---//
            string vinMax_input = "40";
            action.IDeleteValueOnFields(driver, Elements.PST_QuickPowerSearch_VinMax_InputBox);
            action.IType(driver, Elements.PST_QuickPowerSearch_VinMax_InputBox, vinMax_input);
            action.IType(driver, Elements.PST_QuickPowerSearch_VinMax_InputBox, Keys.Tab);

            //--- OUTPUT ---//

            //--- Action: Enter valid input in the Vout textbox ---//
            string vOut_input = "970m";
            action.IDeleteValueOnFields(driver, Elements.PST_QuickPowerSearch_VOut_InputBox);
            action.IType(driver, Elements.PST_QuickPowerSearch_VOut_InputBox, vOut_input);
            action.IType(driver, Elements.PST_QuickPowerSearch_VOut_InputBox, Keys.Tab);

            //--- Action: Enter valid input in the Iout textbox ---//
            string iOut_input = "2.5";
            action.IDeleteValueOnFields(driver, Elements.PST_QuickPowerSearch_IOut_InputBox);
            action.IType(driver, Elements.PST_QuickPowerSearch_IOut_InputBox, iOut_input);
            action.IType(driver, Elements.PST_QuickPowerSearch_IOut_InputBox, Keys.Tab);

            //--- Action: Click Multiple Outputs button ---//
            Thread.Sleep(4000);
            action.IClick(driver, Elements.PST_QuickPowerSearch_MultipleOutputs_Button);

            //--- Action: Click Search button ---//
            action.IClick(driver, Elements.PST_QuickPowerSearch_Search_Button);

            //--- Expected Result: There should be no error leaving the Multple Outputs blank ---//
            test.validateString(driver, "", util.GetText(driver, Elements.PST_QuickPowerSearch_ErrorMessage));
        }

        [Test, Category("PST"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Vout2 Input Box is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Vout2 Input Box is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Vout2 Input Box is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Vout2 Input Box is Present and Working as Expected in RU Locale")]
        public void PST_QuickSearch_QuickPowerSearch_MultipleOutputs_VerifyVOut2InputBox(string Locale)
        {
            //--- Action: Access Dynamic PST with a Quick Search Tool ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + pst_page_url);

            //----- PST Front End TestPlan - Redesign > Test Case Tab > R3 > TC3: Verify Multiple Outputs -----//

            //--- Action: Click the Multiple Outputs button ---//
            action.IClick(driver, Elements.PST_QuickPowerSearch_MultipleOutputs_Button);
            Thread.Sleep(1000);

            //--- Expected Result: Vout2 textbox should display ---//
            test.validateElementIsPresent(driver, Elements.PST_QuickPowerSearch_VOut2_InputBox);

            if (util.CheckElement(driver, Elements.PST_QuickPowerSearch_VOut2_InputBox, 1))
            {
                //--- Action: Click the Multiple Outputs button again ---//
                action.IClick(driver, Elements.PST_QuickPowerSearch_MultipleOutputs_Button);

                //--- Expected Result: Vout2 textbox should disappear ---//
                test.validateElementIsNotPresent(driver, Elements.PST_QuickPowerSearch_VOut2_InputBox);
            }
        }

        [Test, Category("PST"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Iout2 Input Box is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Iout2 Input Box is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Iout2 Input Box is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Iout2 Input Box is Present and Working as Expected in RU Locale")]
        public void PST_QuickSearch_QuickPowerSearch_MultipleOutputs_VerifyIOut2InputBox(string Locale)
        {
            //--- Action: Access Dynamic PST with a Quick Search Tool ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + pst_page_url);

            //----- PST Front End TestPlan - Redesign > Test Case Tab > R3 > TC3: Verify Multiple Outputs -----//

            //--- Action: Click the Multiple Outputs button ---//
            action.IClick(driver, Elements.PST_QuickPowerSearch_MultipleOutputs_Button);
            Thread.Sleep(9000);

            //--- Expected Result: Iout2 textbox should display ---//
            test.validateElementIsPresent(driver, Elements.PST_QuickPowerSearch_IOut2_InputBox);

            if (util.CheckElement(driver, Elements.PST_QuickPowerSearch_IOut2_InputBox, 1))
            {
                //--- Action: Click the Multiple Outputs button again ---//
                action.IClick(driver, Elements.PST_QuickPowerSearch_MultipleOutputs_Button);

                //--- Expected Result: Iout2 textbox should disappear ---//
                test.validateElementIsNotPresent(driver, Elements.PST_QuickPowerSearch_IOut2_InputBox);
            }
        }

        [Test, Category("PST"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Vout3 Input Box is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Vout3 Input Box is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Vout3 Input Box is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Vout3 Input Box is Present and Working as Expected in RU Locale")]
        public void PST_QuickSearch_QuickPowerSearch_MultipleOutputs_VerifyVOut3InputBox(string Locale)
        {
            //--- Action: Access Dynamic PST with a Quick Search Tool ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + pst_page_url);

            //----- PST Front End TestPlan - Redesign > Test Case Tab > R3 > TC3: Verify Multiple Outputs -----//

            //--- Action: Click the Multiple Outputs button ---//
            action.IClick(driver, Elements.PST_QuickPowerSearch_MultipleOutputs_Button);

            //--- Expected Result: Vout3 textbox should display ---//
            test.validateElementIsPresent(driver, Elements.PST_QuickPowerSearch_VOut3_InputBox);

            if (util.CheckElement(driver, Elements.PST_QuickPowerSearch_VOut3_InputBox, 1))
            {
                //--- Action: Click the Multiple Outputs button again ---//
                action.IClick(driver, Elements.PST_QuickPowerSearch_MultipleOutputs_Button);

                //--- Expected Result: Vout3 textbox should disappear ---//
                test.validateElementIsNotPresent(driver, Elements.PST_QuickPowerSearch_VOut3_InputBox);
            }
        }

        [Test, Category("PST"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Iout3 Input Box is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Iout3 Input Box is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Iout3 Input Box is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Iout3 Input Box is Present and Working as Expected in RU Locale")]
        public void PST_QuickSearch_QuickPowerSearch_MultipleOutputs_VerifyIOut3InputBox(string Locale)
        {
            //--- Action: Access Dynamic PST with a Quick Search Tool ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + pst_page_url);

            //----- PST Front End TestPlan - Redesign > Test Case Tab > R3 > TC3: Verify Multiple Outputs -----//

            //--- Action: Click the Multiple Outputs button ---//
            action.IClick(driver, Elements.PST_QuickPowerSearch_MultipleOutputs_Button);
            Thread.Sleep(3000);

            //--- Expected Result: Iout3 textbox should display ---//
            test.validateElementIsPresent(driver, Elements.PST_QuickPowerSearch_IOut3_InputBox);

            if (util.CheckElement(driver, Elements.PST_QuickPowerSearch_IOut3_InputBox, 1))
            {
                //--- Action: Click the Multiple Outputs button again ---//
                action.IClick(driver, Elements.PST_QuickPowerSearch_MultipleOutputs_Button);

                //--- Expected Result: Iout3 textbox should disappear ---//
                test.validateElementIsNotPresent(driver, Elements.PST_QuickPowerSearch_IOut3_InputBox);
            }
        }
    }
}