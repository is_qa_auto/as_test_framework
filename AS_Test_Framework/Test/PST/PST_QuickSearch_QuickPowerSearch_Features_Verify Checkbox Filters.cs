﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using OpenQA.Selenium;
using System.Threading;

namespace AS_Test_Framework.PST
{
    [TestFixture]
    public class PST_QuickSearch_QuickPowerSearch_Features_CheckboxFilters : BaseSetUp
    {
        public PST_QuickSearch_QuickPowerSearch_Features_CheckboxFilters() : base() { }

        //--- URLs ---//
        string pst_page_url = "/parametricsearch/11436";

        [Test, Category("PST"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Checkboxes are Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Checkboxes are Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Checkboxes are Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Checkboxes are Present and Working as Expected in RU Locale")]
        public void PST_QuickSearch_QuickPowerSearch_Features_VerifyCheckboxFilters(string Locale)
        {
            //--- Action: Access Dynamic PST with a Quick Search Tool ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + pst_page_url);
            Thread.Sleep(1000);

            //----- PST Front End TestPlan - Redesign > Test Case Tab > R3 > TC1: Verify fields are present -----//

            //--- FEATURES (checkbox) ---//

            //--- Expected Result: The Low EMI checkbox is present ---//
            test.validateElementIsPresent(driver, Elements.PST_QuickPowerSearch_LowEmi_CheckBox);

            //--- Expected Result: The Ultrathin checkbox is present ---//
            test.validateElementIsPresent(driver, Elements.PST_QuickPowerSearch_Ultrathin_CheckBox);

            //--- Expected Result: The Internal Heat Sink checkbox is present ---//
            test.validateElementIsPresent(driver, Elements.PST_QuickPowerSearch_InternalHeatSink_CheckBox);
        }
    }
}