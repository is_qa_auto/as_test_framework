﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using OpenQA.Selenium;
using System;
using System.Threading;

namespace AS_Test_Framework.PST
{
    [TestFixture]
    public class PST_TableNavigationMainButtons_MaximizeFiltersButtonAndMinimizeFiltersButton : BaseSetUp
    {
        public PST_TableNavigationMainButtons_MaximizeFiltersButtonAndMinimizeFiltersButton() : base() { }

        //--- URLs ---//
        string pst_page_url = "/parametricsearch/11470";

        //--- Labels ----//
        string minimizeFilters_txt = "Minimize Filters";
        string maximizeFilters_txt = "Maximize Filters";

        [Test, Category("PST"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Maximize Filters Button and Minimize Filters Button are Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Maximize Filters Button and Minimize Filters Button are Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Maximize Filters Button and Minimize Filters Button are Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Maximize Filters Button and Minimize Filters Button are Present and Working as Expected in RU Locale")]
        public void PST_TableNavigationMainButtons_VerifyMaximizeFiltersButtonAndMinimizeFiltersButton(string Locale)
        {
            //--- Action: Access any Dynamic PST page. ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + pst_page_url);
            Thread.Sleep(1000);

            //----- PST Front End TestPlan - Redesign > Test Case Tab > R4 > TC4: Verify Minimize/Maximize Filters button -----//

            //--- Expected Result: Column header is set to Minimize Filters during on-load ---//
            test.validateElementIsPresent(driver, Elements.PST_Minimized_Columns);

            if (Locale.Equals("en"))
            {
                //--- Expected Result: Maximize Filters button is displayed during on-load ---//
                test.validateStringIsCorrect(driver, Elements.PST_MaximizeFilters_MinimizeFilters_Button, maximizeFilters_txt);

                if (util.GetText(driver, Elements.PST_MaximizeFilters_MinimizeFilters_Button).Equals(maximizeFilters_txt))
                {
                    //--- Action: Click the Maximize Filters button ---//
                    action.IClick(driver, Elements.PST_MaximizeFilters_MinimizeFilters_Button);

                    //--- Expected Result: Column header is set to Maximize Filters ---//
                    test.validateElementIsNotPresent(driver, Elements.PST_Minimized_Columns);

                    //--- Expected Result: Button namw changes from Maximize Filters to Minimize Filters ---//
                    test.validateStringIsCorrect(driver, Elements.PST_MaximizeFilters_MinimizeFilters_Button, minimizeFilters_txt);

                    if (util.CheckElement(driver, Elements.PST_ChooseParameters_Button, 1))
                    {
                        //----- PST Front End TestPlan - Redesign > Test Case Tab > R4 > TC1.5: Verify display of maximized columns based on chosen parameters (IQ-8996/AL-16051) -----//

                        //--- Get the Default Values ---//
                        string default_url = driver.Url;

                        if (!default_url.EndsWith("/"))
                        {
                            default_url = default_url + "/";
                        }

                        //--- Action: Click Choose Parameters button ---//
                        action.IClick(driver, Elements.PST_ChooseParameters_Button);

                        //--- Action: Tick any unchecked parameters in the Choose Parameters window ---//
                        action.IClick(driver, Elements.PST_ChooseParameters_DialogBox_DeselectAll_Button);
                        action.IClick(driver, Elements.PST_ChooseParameters_DialogBox_Checkboxes);
                        string selected_parameter_value = util.ReturnAttribute(driver, Elements.PST_ChooseParameters_DialogBox_Checkboxes, "value");

                        //--- Action: Click OK button ---//
                        action.IClick(driver, Elements.PST_ChooseParameters_DialogBox_Ok_Button);

                        //--- Expected Result: Chosen parameters will be added next to the right of Part Number colum ---//
                        test.validateElementIsPresent(driver, By.CssSelector("section[data-parameter='" + selected_parameter_value + "']"));

                        //--- Expected Result: Parameter IDs are concatenated to the URL ---//
                        //Console.WriteLine("default_url = " + default_url + "d=" + selected_parameter_value);
                        //Console.WriteLine("driver.Url = " + driver.Url);
                        test.validateStringInstance(driver, default_url + "d=" + selected_parameter_value, driver.Url);
                    }
                }
            }
        }
    }
}