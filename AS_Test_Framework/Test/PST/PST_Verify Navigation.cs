﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;

namespace AS_Test_Framework.PST
{
    [TestFixture]
    public class PST_Navigation : BaseSetUp
    {
        public PST_Navigation() : base() { }

        //--- URLs ---//
        string pst_page_url = "/parametricsearch/10636";
        string pstLanding_page_url = "/parametric-search.html";
        string productsLanding_page_url = "/products.html";

        [Test, Category("PST"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Navigation to PST Landing Page is Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Navigation to PST Landing Page is Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Navigation to PST Landing Page is Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Navigation to PST Landing Page is Working as Expected in RU Locale")]
        public void PST_Navigation_VerifyNavigationToPstLandingPage(string Locale)
        {
            //----- PST Front End TestPlan - Redesign > Test Case Tab > R1 > TC6: (AL-8165) -----//

            //--- Action: Access PST landing page ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + pstLanding_page_url);

            //--- Expected Result: Page should redirect to Product page ---//
            test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + productsLanding_page_url);
        }

        [Test, Category("PST"), Category("NonCore"), Retry(2)]
        [TestCase("cn", "zh", TestName = "Verify that the Language Redirect Rule is Working as Expected in CN Locale")]
        [TestCase("jp", "ja", TestName = "Verify that the Language Redirect Rule is Working as Expected in JP Locale")]
        public void PST_Navigation_VerifyLanguageRedirectRule(string Locale, string Url_Locale)
        {
            //----- PST Front End TestPlan - Redesign > Test Case Tab > R14 > TC1: Verify language redirect rule for parametric search (IQ-8921/AL-15750) -----//

            //--- Action: Access URLs ---//
            action.Navigate(driver, Configuration.Env_Url + Url_Locale + pst_page_url);

            //--- Expected Result: The System redirects to URLs ---//
            test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + pst_page_url);
        }
    }
}