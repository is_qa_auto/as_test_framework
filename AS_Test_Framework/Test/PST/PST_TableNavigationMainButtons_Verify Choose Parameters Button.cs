﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using OpenQA.Selenium;
using System;
using System.Threading;

namespace AS_Test_Framework.PST
{
    [TestFixture]
    public class PST_TableNavigationMainButtons_ChooseParametersButton : BaseSetUp
    {
        public PST_TableNavigationMainButtons_ChooseParametersButton() : base() { }

        //--- URLs ---//
        string pst_page_url = "/parametricsearch/11470";

        [Test, Category("PST"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Choose Parameters Button is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Choose Parameters Button is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Choose Parameters Button is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Choose Parameters Button is Present and Working as Expected in RU Locale")]
        public void PST_TableNavigationMainButtons_VerifyChooseParametersButton(string Locale)
        {
            //--- Action: Access any Dynamic PST page. ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + pst_page_url);

            if (util.CheckElement(driver, Elements.PST_ChooseParameters_Button, 1))
            {
                //--- Get the Default Values ---//
                string default_url = driver.Url;
                int default_column_count = util.GetCount(driver, Elements.PST_Columns);

                //----- PST Front End TestPlan - Redesign > Test Case Tab > R4 > TC1: Verify Choose Parameters button -----//

                //--- Action: Click the Choose Parameters button ---//
                action.IClick(driver, Elements.PST_ChooseParameters_Button);

                //--- Expected Result: Choose Parameters window should be displayed ---//
                test.validateElementIsPresent(driver, Elements.PST_ChooseParameters_DialogBox);

                if (util.CheckElement(driver, Elements.PST_ChooseParameters_DialogBox, 1))
                {
                    //----- PST Front End TestPlan - Redesign > Test Case Tab > R4 > TC1.4: Verify displayed columns based on chosen parameters -----//

                    if (!default_url.EndsWith("/"))
                    {
                        default_url = default_url + "/";
                    }

                    //--- Action: Tick any unchecked parameters in the Choose Parameters window ---//
                    action.IClick(driver, Elements.PST_ChooseParameters_DialogBox_DeselectAll_Button);
                    action.IClick(driver, Elements.PST_ChooseParameters_DialogBox_Checkboxes);
                    string selected_parameter_value = util.ReturnAttribute(driver, Elements.PST_ChooseParameters_DialogBox_Checkboxes, "value");

                    //--- Action: Click OK button ---//
                    action.IClick(driver, Elements.PST_ChooseParameters_DialogBox_Ok_Button);

                    //--- Expected Result: Chosen parameters will be added next to the right of Part Number colum ---//
                    test.validateElementIsPresent(driver, By.CssSelector("section[data-parameter='" + selected_parameter_value + "']"));

                    //--- Expected Result: Parameter IDs are concatenated to the URL ---//
                    //Console.WriteLine("default_url = " + default_url + "d=" + selected_parameter_value);
                    //Console.WriteLine("driver.Url = " + driver.Url);
                    test.validateStringInstance(driver, default_url + "d=" + selected_parameter_value, driver.Url);
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.PST_ChooseParameters_Button);
            }
        }

        [Test, Category("PST"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Select All Button is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Select All Button is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Select All Button is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Select All Button is Present and Working as Expected in RU Locale")]
        public void PST_TableNavigationMainButtons_ChooseParameters_VerifySelectAllButton(string Locale)
        {
            //--- Action: Access any Dynamic PST page. ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + pst_page_url);

            //--- Get the Default Values ---//
            string default_url = driver.Url;
            int default_column_count = util.GetCount(driver, Elements.PST_Columns);

            //----- PST Front End TestPlan - Redesign > Test Case Tab > R4 > TC1: Verify Choose Parameters button -----//

            //--- Action: Click the Choose Parameters button ---//
            action.IClick(driver, Elements.PST_ChooseParameters_Button);

            //--- Expected Result: Select All button should be displayed ---//
            test.validateElementIsPresent(driver, Elements.PST_ChooseParameters_DialogBox_SelectAll_Button);

            if (util.CheckElement(driver, Elements.PST_ChooseParameters_DialogBox_SelectAll_Button, 1))
            {
                //----- PST Front End TestPlan - Redesign > Test Case Tab > R4 > TC1.1: Verify Select All button -----//

                //--- Action: Click the Select All button ---//
                action.IClick(driver, Elements.PST_ChooseParameters_DialogBox_SelectAll_Button);

                //--- Expected Result: All of the parameters are checked ---//
                test.validateElementIsSelected(driver, Elements.PST_ChooseParameters_DialogBox_Checkboxes);

                //--- Action: Click OK button ---//
                action.IClick(driver, Elements.PST_ChooseParameters_DialogBox_Ok_Button);

                //--- Expected Result: All of the parameters are added to the Dynamic PST ---//
                test.validateCountIsGreater(driver, util.GetCount(driver, Elements.PST_Columns), default_column_count);

                //--- Expected Result: Parameter IDs are concatenated to the URL ---//
                test.validateStringChanged(driver, driver.Url, default_url);
            }
        }

        [Test, Category("PST"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Deselect All Button is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Deselect All Button is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Deselect All Button is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Deselect All Button is Present and Working as Expected in RU Locale")]
        public void PST_TableNavigationMainButtons_ChooseParameters_VerifyDeselectAllButton(string Locale)
        {
            //--- Action: Access any Dynamic PST page. ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + pst_page_url);

            //--- Get the Default Values ---//
            string default_url = driver.Url;
            int default_column_count = util.GetCount(driver, Elements.PST_Columns);

            //----- PST Front End TestPlan - Redesign > Test Case Tab > R4 > TC1: Verify Choose Parameters button -----//

            //--- Action: Click the Choose Parameters button ---//
            action.IClick(driver, Elements.PST_ChooseParameters_Button);

            //--- Expected Result: Deselect All button should be displayed ---//
            test.validateElementIsPresent(driver, Elements.PST_ChooseParameters_DialogBox_DeselectAll_Button);

            if (util.CheckElement(driver, Elements.PST_ChooseParameters_DialogBox_DeselectAll_Button, 1))
            {
                //----- PST Front End TestPlan - Redesign > Test Case Tab > R4 > TC1.2: Verify Deselect All button -----//

                //--- Action: Click the Deselect All button ---//
                action.IClick(driver, Elements.PST_ChooseParameters_DialogBox_DeselectAll_Button);

                //--- Expected Result: All of the parameters are unchecked ---//
                test.validateElementIsNotSelected(driver, Elements.PST_ChooseParameters_DialogBox_Checkboxes);
            }
        }

        [Test, Category("PST"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Default Button is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Default Button is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Default Button is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Default Button is Present and Working as Expected in RU Locale")]
        public void PST_TableNavigationMainButtons_ChooseParameters_VerifyDefaultButton(string Locale)
        {
            //--- Action: Access any Dynamic PST page. ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + pst_page_url);
            Thread.Sleep(1000);

            //--- Get the Default Values ---//
            int default_column_count = util.GetCount(driver, Elements.PST_Columns);

            //----- PST Front End TestPlan - Redesign > Test Case Tab > R4 > TC1: Verify Choose Parameters button -----//

            //--- Action: Click the Choose Parameters button ---//
            action.IClick(driver, Elements.PST_ChooseParameters_Button);
            Thread.Sleep(1000);

            //--- Expected Result: Default button should be displayed ---//
            test.validateElementIsPresent(driver, Elements.PST_ChooseParameters_DialogBox_Default_Button);

            if (util.CheckElement(driver, Elements.PST_ChooseParameters_DialogBox_Default_Button, 1))
            {
                //----- PST Front End TestPlan - Redesign > Test Case Tab > R4 > TC1.3: Verify Default button -----//

                //--- Action: Click the Default button ---//
                action.IClick(driver, Elements.PST_ChooseParameters_DialogBox_Default_Button);

                //--- Action: Click OK button ---//
                action.IClick(driver, Elements.PST_ChooseParameters_DialogBox_Ok_Button);

                //--- Expected Result: Default paramaters are displayed in the Dynamic PST ---//
                test.validateCountIsEqual(driver, default_column_count, util.GetCount(driver, Elements.PST_Columns));
            }
        }
    }
}