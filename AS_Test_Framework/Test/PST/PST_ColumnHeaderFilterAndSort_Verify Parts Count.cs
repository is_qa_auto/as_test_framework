﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using OpenQA.Selenium;
using System;
using System.Threading;

namespace AS_Test_Framework.PST
{
    [TestFixture]
    public class PST_ColumnHeaderFilterAndSort_PartsCount : BaseSetUp
    {
        public PST_ColumnHeaderFilterAndSort_PartsCount() : base() { }

        //--- URLs ---//
        string pst_page_url = "/parametricsearch/11470";

        [Test, Category("PST"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Parts Count is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Parts Count is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Parts Count is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Parts Count is Present and Working as Expected in RU Locale")]
        public void PST_ColumnHeaderFilterAndSort_VerifyPartsCount(string Locale)
        {
            //--- Action: Access any Dynamic PST page. ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + pst_page_url);
            Thread.Sleep(1000);

            //----- PST Front End TestPlan - Redesign > Test Case Tab > R5 > TC9: Verify Part Number count -----//

            //--- Expected Result: Part number count should be present  ---//
            test.validateElementIsPresent(driver, Elements.PST_PartNumber_Column_PartsCount_Label);
        }

        [Test, Category("PST"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Parts Count is Present and Working as Expected after Filtering by Part Number in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Parts Count is Present and Working as Expected after Filtering by Part Number in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Parts Count is Present and Working as Expected after Filtering by Part Number in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Parts Count is Present and Working as Expected after Filtering by Part Number in RU Locale")]
        public void PST_ColumnHeaderFilterAndSort_PartsCount_VerifyAfterFilteringByPartNumber(string Locale)
        {
            //--- Action: Access any Dynamic PST page. ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + pst_page_url);
            Thread.Sleep(1000);

            //----- PST Front End TestPlan - Redesign > Test Case Tab > R5 > TC9: Verify Part Number count -----//

            //--- Get the Values ---//
            string partNumber_1st_row_value = util.GetText(driver, Elements.PST_PartNumber_Column_1st_Row_Link);

            //--- Action: Do filter by Part number ---//
            char partNumber_letter_input = partNumber_1st_row_value[0];
            action.IDeleteValueOnFields(driver, Elements.PST_PartNumber_Column_InputBox);
            action.IType(driver, Elements.PST_PartNumber_Column_InputBox, partNumber_letter_input.ToString());
            action.IType(driver, Elements.PST_PartNumber_Column_InputBox, Keys.Tab);

            //--- Expected Result: The number of Parts displayed should be the same as the Part number count ---//
            test.validateStringInstance(driver, util.GetText(driver, Elements.PST_PartNumber_Column_PartsCount_Label), util.GetCount(driver, Elements.PST_PartNumber_Column_Rows).ToString());
        }

        [Test, Category("PST"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Parts Count is Present and Working as Expected after doing Checkbox Filter in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Parts Count is Present and Working as Expected after doing Checkbox Filter in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Parts Count is Present and Working as Expected after doing Checkbox Filter in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Parts Count is Present and Working as Expected after doing Checkbox Filter in RU Locale")]
        public void PST_ColumnHeaderFilterAndSort_PartsCount_VerifyAfterDoingCheckboxFilter(string Locale)
        {
            //--- Action: Access any Dynamic PST page. ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + pst_page_url);

            //----- PST Front End TestPlan - Redesign > Test Case Tab > R5 > TC9: Verify Part Number count -----//

            //--- Get the Default Values ---//
            int default_column_count = util.GetCount(driver, Elements.PST_Columns);
            int default_row_count = util.GetCount(driver, Elements.PST_PartNumber_Column_Rows);

            //--- Action: Do checkbox filter of any column with checkbox filter ---//
            string checkbox_column_dataParameter_value = null;
            for (int column_counter = 1; column_counter <= default_column_count; column_counter++)
            {
                if (util.CheckElement(driver, By.CssSelector("section[class='details']>section[class='parameters']>section:nth-of-type(" + column_counter + ") * div[class='toggle']"), 1))
                {
                    checkbox_column_dataParameter_value = util.ReturnAttribute(driver, By.CssSelector("section[class='details']>section[class='parameters']>section:nth-of-type(" + column_counter + ")"), "data-parameter");
                    break;
                }
            }
            Console.WriteLine("\tdata-parameter Value = " + checkbox_column_dataParameter_value);

            //--- Locators ---//
            string column_header_locator = "section[data-parameter='" + checkbox_column_dataParameter_value + "']";
            string active_column_header_locator = "section[class*='filter active'][data-parameter='" + checkbox_column_dataParameter_value + "']";
            string column_header_toggle_locator = "section[data-parameter='" + checkbox_column_dataParameter_value + "'] * div[class='toggle']";
            string checkboxes_locator = "section[data-parameter='" + checkbox_column_dataParameter_value + "'] * div[class^='checkbox']";
            string disabled_checkboxes_locator = "section[data-parameter='" + checkbox_column_dataParameter_value + "'] * div[class='checkbox disabled']";
            string selectAll_checkbox_locator = "section[data-parameter='" + checkbox_column_dataParameter_value + "'] * div[class='checkbox']>input[id$='select-all']";

            action.IClick(driver, Elements.PST_ChooseParameters_Button);
            action.IClick(driver, Elements.PST_ChooseParameters_DialogBox_DeselectAll_Button);
            action.IClick(driver, By.CssSelector("section[class='choose parameters'] * input[id$='" + checkbox_column_dataParameter_value + "']"));
            action.IClick(driver, Elements.PST_ChooseParameters_DialogBox_Ok_Button);
            Thread.Sleep(7000);

            //--- Action: Take note of count in the checkbox dropdown label displayed as: <number of checked checkboxes> Values Selected ---//
            string valuesSelected_count = util.ExtractNumber(driver, By.CssSelector(column_header_toggle_locator));
            //Console.WriteLine("Values Selected - Count = " + valuesSelected_count);

            //--- Action: Mouser-hover on the Values Selected Label ---//
            action.IMouseOverTo(driver, By.CssSelector(column_header_toggle_locator));

            //--- Action: Get the Count the Selected Checkboxes ---//
            int default_selected_checkboxes_count = 0;
            int checkboxes_count = util.GetCount(driver, By.CssSelector(checkboxes_locator));
            for (int checkboxes_counter = 2; checkboxes_counter <= checkboxes_count; checkboxes_counter++)
            {
                if (driver.FindElement(By.CssSelector(checkboxes_locator + ":nth-of-type(" + checkboxes_counter + ")>input[type='checkbox']")).Selected)
                {
                    default_selected_checkboxes_count++;
                }
            }
            //Console.WriteLine("Default Selected Checkboxes - Count = " + default_selected_checkboxes_count);

            //--- Action: Do checkbox filter of any column with checkbox filter ---//
            for (int checkboxes_counter = 2; checkboxes_counter < checkboxes_count; checkboxes_counter++)
            {
                if (!util.CheckElement(driver, By.CssSelector(disabled_checkboxes_locator + ":nth-of-type(" + checkboxes_counter + ")"), 1))
                {
                    action.IMouseOverTo(driver, By.CssSelector(column_header_toggle_locator));
                    action.IUncheck(driver, By.CssSelector(checkboxes_locator + ":nth-of-type(" + checkboxes_counter + ")>input[type='checkbox']"));
                }
            }

            //--- Expected Result: The number of Parts displayed should be the same as the Part number count  ---//
            test.validateStringInstance(driver, util.GetText(driver, Elements.PST_PartNumber_Column_PartsCount_Label), util.GetCount(driver, Elements.PST_PartNumber_Column_Rows).ToString());
        }
    }
}