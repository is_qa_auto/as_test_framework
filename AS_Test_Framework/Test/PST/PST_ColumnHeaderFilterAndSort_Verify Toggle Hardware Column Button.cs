﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System.Threading;

namespace AS_Test_Framework.PST
{
    [TestFixture]
    public class PST_ColumnHeaderFilterAndSort_ToggleHardwareColumnButton : BaseSetUp
    {
        public PST_ColumnHeaderFilterAndSort_ToggleHardwareColumnButton() : base() { }

        //--- URLs ---//
        string pst_page_url = "/parametricsearch/11470";

        //--- Labels ---//
        string toggleHardwareColumn_text = "Toggle Hardware Column";

        [Test, Category("PST"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Toggle Hardware Column Button is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Toggle Hardware Column Button is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Toggle Hardware Column Button is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Toggle Hardware Column Button is Present and Working as Expected in RU Locale")]
        public void PST_ColumnHeaderFilterAndSort_VerifyToggleHardwareColumnButton(string Locale)
        {
            //--- Action: Access any Dynamic PST page. ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + pst_page_url);
            Thread.Sleep(1000);

            //--- Expected Result: The Toggle Hardware Column Button is present ---//
            test.validateElementIsPresent(driver, Elements.PST_ToggleHardwareColumn_Button);

            if (util.CheckElement(driver, Elements.PST_ToggleHardwareColumn_Button, 1))
            {
                //----- PST Front End TestPlan - Redesign > Test Case Tab > R5 > TC1: Verify Toggle buttons -----//

                //--- Action: Hover to Toggle Hardware column button ---//
                action.IMouseOverTo(driver, Elements.PST_ToggleHardwareColumn_Button);

                //--- Expected Result: Tooltips of the Toggle Hardware column should be displaying (AL-15725) ---//
                if (Locale.Equals("en"))
                {
                    test.validateStringIsCorrect(driver, Elements.PST_ToggleHardwareColumn_Button_ToolTip, toggleHardwareColumn_text);
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.PST_ToggleHardwareColumn_Button_ToolTip);
                }
            }
        }
    }
}