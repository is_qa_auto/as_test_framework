﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;

namespace AS_Test_Framework.PST
{
    [TestFixture]
    public class PST_TableNavigationMainButtons_SortByNewestButton : BaseSetUp
    {
        public PST_TableNavigationMainButtons_SortByNewestButton() : base() { }

        //--- URLs ---//
        string pst_page_url = "/parametricsearch/11470";

        [Test, Category("PST"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Sort by Newest Button is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Sort by Newest Button is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Sort by Newest Button is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Sort by Newest Button is Present and Working as Expected in RU Locale")]
        public void PST_TableNavigationMainButtons_VerifySortByNewestButton(string Locale)
        {
            //--- Action: Access any Dynamic PST page. ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + pst_page_url);

            if (util.CheckElement(driver, Elements.PST_SortByNewest_Button, 1))
            {
                //----- PST Front End TestPlan - Redesign > Test Case Tab > R4 > TC5: Verify "Sort by Newest" button -----//

                //--- Action: Click any Column Header to do a sort ---//
                action.IClick(driver, Elements.PST_Column_Sort_Buttons);

                //--- Action: Click the Sort by Newest ---//
                action.IClick(driver, Elements.PST_SortByNewest_Button);

                //--- Expected Result: Sort from the previous column should be remove ---//
                test.validateElementIsNotPresent(driver, Elements.PST_Sorted_Columns);
            }
            else
            {
                //--- Expected Result: The Sort by Newest Button is present ---//
                test.validateElementIsPresent(driver, Elements.PST_SortByNewest_Button);
            }
        }
    }
}