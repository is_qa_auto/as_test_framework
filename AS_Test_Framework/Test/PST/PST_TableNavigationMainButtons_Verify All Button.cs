﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;

namespace AS_Test_Framework.PST
{
    [TestFixture]
    public class PST_TableNavigationMainButtons_AllButton : BaseSetUp
    {
        public PST_TableNavigationMainButtons_AllButton() : base() { }

        //--- URLs ---//
        string pst_page_url = "/parametricsearch/11470";

        [Test, Category("PST"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the All Button is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the All Button is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the All Button is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the All Button is Present and Working as Expected in RU Locale")]
        public void PST_TableNavigationMainButtons_VerifyAllButton(string Locale)
        {
            //--- Action: Access any Dynamic PST page. ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + pst_page_url);

            if (util.CheckElement(driver, Elements.PST_All_Button, 1))
            {
                //--- Get the Default Values ---//
                string default_url = driver.Url;
                int default_column_count = util.GetCount(driver, Elements.PST_Columns);

                //----- PST Front End TestPlan - Redesign > Test Case Tab > R4 > TC2: Verify "All" Buttons -----//

                //--- Action: Click the All button ---//
                action.IClick(driver, Elements.PST_All_Button);

                //--- Expected Result: All parameters should be added to the Dynamic PST ---//
                test.validateCountIsGreater(driver, util.GetCount(driver, Elements.PST_Columns), default_column_count);

                //--- Action: Click the Choose Parameters button ---//
                action.IClick(driver, Elements.PST_ChooseParameters_Button);

                //--- Expected Result: All the paramaters are checked in the Choose Parameters window ---//
                test.validateElementIsSelected(driver, Elements.PST_ChooseParameters_DialogBox_Checkboxes);

                //--- Action: Click OK button ---//
                action.IClick(driver, Elements.PST_ChooseParameters_DialogBox_Ok_Button);

                //--- Expected Result: Parameter IDs are concatenated to the URL ---//
                test.validateStringChanged(driver, driver.Url, default_url);
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.PST_All_Button);
            }
        }
    }
}