﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;

namespace AS_Test_Framework.PST
{
    [TestFixture]
    public class PST_ColumnHeaderFilterAndSort_ToggleStatusColumnButton : BaseSetUp
    {
        public PST_ColumnHeaderFilterAndSort_ToggleStatusColumnButton() : base() { }

        //--- URLs ---//
        string pst_page_url = "/parametricsearch/11470";

        //--- Labels ---//
        string toggleStatusColumn_text = "Toggle Status Column";

        [Test, Category("PST"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Toggle Status Column Button is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Toggle Status Column Button is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Toggle Status Column Button is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Toggle Status Column Button is Present and Working as Expected in RU Locale")]
        public void PST_ColumnHeaderFilterAndSort_VerifyToggleStatusColumnButton(string Locale)
        {
            //--- Action: Access any Dynamic PST page. ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + pst_page_url);

            //--- Expected Result: The Toggle Status Column Button is present ---//
            test.validateElementIsPresent(driver, Elements.PST_ToggleStatusColumn_Button);

            if (util.CheckElement(driver, Elements.PST_ToggleStatusColumn_Button, 1))
            {
                //----- PST Front End TestPlan - Redesign > Test Case Tab > R5 > TC1: Verify Toggle buttons -----//

                //--- Action: Hover to Toggle Status column button ---//
                action.IMouseOverTo(driver, Elements.PST_ToggleStatusColumn_Button);

                //--- Expected Result: Tooltips of the Toggle Status column should be displaying (AL-15725) ---//
                if (Locale.Equals("en"))
                {
                    test.validateStringIsCorrect(driver, Elements.PST_ToggleStatusColumn_Button_ToolTip, toggleStatusColumn_text);
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.PST_ToggleStatusColumn_Button_ToolTip);
                }
            }
        }
    }
}