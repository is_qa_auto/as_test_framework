﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using System;
using System.Threading;

namespace AS_Test_Framework.PST
{
    [TestFixture]
    public class PST_ColumnHeaderFilterAndSort_ColumnSort : BaseSetUp
    {
        public PST_ColumnHeaderFilterAndSort_ColumnSort() : base() { }

        //--- URLs ---//
        string pst_page_url = "/parametricsearch/11470";

        [Test, Category("PST"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Column Sort is Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Column Sort is Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Column Sort is Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Column Sort is Working as Expected in RU Locale")]
        public void PST_ColumnHeaderFilterAndSort_VerifyColumnSort(string Locale)
        {
            //--- Action: Access any Dynamic PST page. ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + pst_page_url);
            Thread.Sleep(8000);

            //--- Action: Choose any column ---//
            string checkbox_column_dataParameter_value = null;

            int default_column_count = util.GetCount(driver, Elements.PST_Columns);
            for (int column_counter = 1; column_counter <= default_column_count; column_counter++)
            {
                if (util.CheckElement(driver, By.CssSelector("section[class='details']>section[class='parameters']>section:nth-of-type(" + column_counter + ") * div[class='toggle']"), 1))
                {
                    checkbox_column_dataParameter_value = util.ReturnAttribute(driver, By.CssSelector("section[class='details']>section[class='parameters']>section:nth-of-type(" + column_counter + ")"), "data-parameter");
                    break;
                }
            }
            Console.WriteLine("\tdata-parameter Value = " + checkbox_column_dataParameter_value);

            //--- Locators ---//
            string checkbox_locator = "section[class='choose parameters'] * input[id$='" + checkbox_column_dataParameter_value + "']";
            string column_header_locator = "section[data-parameter='" + checkbox_column_dataParameter_value + "']";
            string column_header_sort_counter_locator = "section[data-parameter='" + checkbox_column_dataParameter_value + "'] * i>span";

            action.IClick(driver, Elements.PST_ChooseParameters_Button);
            action.IClick(driver, Elements.PST_ChooseParameters_DialogBox_DeselectAll_Button);
            action.IClick(driver, By.CssSelector(checkbox_locator));
            action.IClick(driver, Elements.PST_ChooseParameters_DialogBox_Ok_Button);

            //----- PST Front End TestPlan - Redesign > Test Case Tab > R5 > TC8: Verify Column Sort -----//

            //--- Action: Click the column header of any column ---//
            action.IClick(driver, By.CssSelector(column_header_locator));
            Thread.Sleep(5000);

            //--- Expected Result: Selected column should be sorted in ascending order ---//
            string row_value = null;
            object[] row_values = new object[10];
            for (int locator_counter = 1, object_counter = 0; locator_counter <= 10; locator_counter++, object_counter++)
            {
                row_value = util.GetText(driver, By.CssSelector("section[class='details']>article:nth-of-type(" + locator_counter + ")>div[data-parameter='" + checkbox_column_dataParameter_value + "']")).Trim();

                if (int.TryParse(row_value, out int n))
                {
                    row_values[object_counter] = util.ConverterToMetric(row_value);
                }
                else
                {
                    row_values[object_counter] = row_value;
                }
                //Console.WriteLine("Ascenading - Row #" + locator_counter + " = " + row_values[object_counter]);
            }
            test.validateStringIfInAscenading(row_values);

            //--- Expected Result: "1" written in the column header ---//
            test.validateStringIsCorrect(driver, By.CssSelector(column_header_sort_counter_locator), "1");

            //--- Action: Click the column header again ---//
            action.IClick(driver, By.CssSelector(column_header_locator));

            //--- Expected Result: Selected column should be sorted in descending order ---//
            row_value = null;
            row_values = new object[10];
            for (int locator_counter = 1, object_counter = 0; locator_counter <= 10; locator_counter++, object_counter++)
            {
                row_value = util.GetText(driver, By.CssSelector("section[class='details']>article:nth-of-type(" + locator_counter + ")>div[data-parameter='" + checkbox_column_dataParameter_value + "']")).Trim();

                if (int.TryParse(row_value, out int n))
                {
                    row_values[object_counter] = util.ConverterToMetric(row_value);
                }
                else
                {
                    row_values[object_counter] = row_value;
                }
                //Console.WriteLine("Descending - Row #" + locator_counter + " = " + row_values[object_counter]);
            }
            test.validateStringIfInDescending(row_values);
        }

        [Test, Category("PST"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Multi-sort Column is Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Multi-sort Column is Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Multi-sort Column is Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Multi-sort Column is Working as Expected in RU Locale")]
        public void PST_ColumnHeaderFilterAndSort_VerifyMultiSortColumn(string Locale)
        {
            //--- Action: Access any Dynamic PST page. ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + pst_page_url);
            Thread.Sleep(1000);

            //--- Action: Choose any column ---//
            string[] checkbox_column_dataParameter_value = new string[2];

            int default_column_count = util.GetCount(driver, Elements.PST_Columns);
            for (int column_counter = 1, array_counter = 0; column_counter <= default_column_count; column_counter++)
            {
                if (util.CheckElement(driver, By.CssSelector("section[class='details']>section[class='parameters']>section:nth-of-type(" + column_counter + ") * div[class='toggle']"), 1))
                {
                    checkbox_column_dataParameter_value[array_counter] = util.ReturnAttribute(driver, By.CssSelector("section[class='details']>section[class='parameters']>section:nth-of-type(" + column_counter + ")"), "data-parameter");
                    array_counter++;

                    if (array_counter == 2)
                    {
                        break;
                    }
                }
            }
            Console.WriteLine("\tdata-parameter 1 - Value = " + checkbox_column_dataParameter_value[0]);
            Console.WriteLine("\tdata-parameter 2 - Value = " + checkbox_column_dataParameter_value[1]);

            //--- Locators ---//
            string checkbox_1_locator = "section[class='choose parameters'] * input[id$='" + checkbox_column_dataParameter_value[0] + "']";
            string column_1_header_locator = "section[data-parameter='" + checkbox_column_dataParameter_value[0] + "']";
            string column_1_header_sort_counter_locator = "section[data-parameter='" + checkbox_column_dataParameter_value[0] + "'] * i>span";
            string checkbox_2_locator = "section[class='choose parameters'] * input[id$='" + checkbox_column_dataParameter_value[1] + "']";
            string column_2_header_locator = "section[data-parameter='" + checkbox_column_dataParameter_value[1] + "']";
            string column_2_header_sort_counter_locator = "section[data-parameter='" + checkbox_column_dataParameter_value[1] + "'] * i>span";

            action.IClick(driver, Elements.PST_ChooseParameters_Button);
            action.IClick(driver, Elements.PST_ChooseParameters_DialogBox_DeselectAll_Button);
            action.IClick(driver, By.CssSelector(checkbox_1_locator));
            action.IClick(driver, By.CssSelector(checkbox_2_locator));
            action.IClick(driver, Elements.PST_ChooseParameters_DialogBox_Ok_Button);

            //----- PST Front End TestPlan - Redesign > Test Case Tab > R5 > TC8.1: Verify multi-sort column -----//

            //--- Action: Click the column header of any column ---//
            action.IClick(driver, By.CssSelector(column_1_header_locator));
            Thread.Sleep(2000);

            //--- Action: Hold press "Ctrl" key in the keyboard and click another column header ---//
            IWebElement body = driver.FindElement(By.CssSelector(column_2_header_locator));
            Actions OpenLink = new Actions(driver);
            OpenLink.KeyDown(Keys.Control).MoveToElement(body).Click().KeyUp(Keys.Control).Perform();

            //--- Expected Result: Selected column should be sorted in ascending order ---//
            string row_value = null;
            object[] row_values = new object[10];
            for (int locator_counter = 1, object_counter = 0; locator_counter <= 10; locator_counter++, object_counter++)
            {
                row_value = util.GetText(driver, By.CssSelector("section[class='details']>article:nth-of-type(" + locator_counter + ")>div[data-parameter='" + checkbox_column_dataParameter_value[1] + "']")).Trim();

                if (int.TryParse(row_value, out int n))
                {
                    row_values[object_counter] = util.ConverterToMetric(row_value);
                }
                else
                {
                    row_values[object_counter] = row_value;
                }
                //Console.WriteLine("Ascenading - Row #" + locator_counter + " = " + row_values[object_counter]);
            }
            test.validateStringIfInAscenading(row_values);

            //--- Expected Result: "2" written in the column header ---//
            test.validateStringIsCorrect(driver, By.CssSelector(column_2_header_sort_counter_locator), "2");

            //--- Action: Hold press "Ctrl" key in the keyboard and click the same column header ---//
            body = driver.FindElement(By.CssSelector(column_2_header_locator));
            OpenLink = new Actions(driver);
            OpenLink.KeyDown(Keys.Control).MoveToElement(body).Click().KeyUp(Keys.Control).Perform();

            //--- Expected Result: Selected column should be sorted in descending order ---//
            row_value = null;
            row_values = new object[10];
            for (int locator_counter = 1, object_counter = 0; locator_counter <= 10; locator_counter++, object_counter++)
            {
                row_value = util.GetText(driver, By.CssSelector("section[class='details']>article:nth-of-type(" + locator_counter + ")>div[data-parameter='" + checkbox_column_dataParameter_value[1] + "']")).Trim();

                if (int.TryParse(row_value, out int n))
                {
                    row_values[object_counter] = util.ConverterToMetric(row_value);
                }
                else
                {
                    row_values[object_counter] = row_value;
                }
                //Console.WriteLine("Descending - Row #" + locator_counter + " = " + row_values[object_counter]);
            }
        }
    }
}