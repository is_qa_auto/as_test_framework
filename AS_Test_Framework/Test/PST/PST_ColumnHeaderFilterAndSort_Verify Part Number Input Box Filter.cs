﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using OpenQA.Selenium;
using System.Threading;

namespace AS_Test_Framework.PST
{
    [TestFixture]
    public class PST_ColumnHeaderFilterAndSort_PartNumberInputBoxFilter : BaseSetUp
    {
        public PST_ColumnHeaderFilterAndSort_PartNumberInputBoxFilter() : base() { }

        //--- URLs ---//
        string pst_page_url = "/parametricsearch/11470";

        //--- Labels ---//
        string filterParts_text = "Filter Parts";

        [Test, Category("PST"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Parts Filter in Part Number Input Box is Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Parts Filter in Part Number Input Box is Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Parts Filter in Part Number Input Box is Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Parts Filter in Part Number Input Box is Working as Expected in RU Locale")]
        public void PST_ColumnHeaderFilterAndSort_VerifyPartsFilterInPartNumberInputBox(string Locale)
        {
            //--- Action: Access any Dynamic PST page. ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + pst_page_url);
            Thread.Sleep(1000);

            //----- PST Front End TestPlan - Redesign > Test Case Tab > R5 > TC3: Verify "Part Number" filter -----//

            //--- Expected Result: Part number filter is present ---//
            test.validateElementIsPresent(driver, Elements.PST_PartNumber_Column_InputBox);

            if (util.CheckElement(driver, Elements.PST_PartNumber_Column_InputBox, 1))
            {
                //--- Get the Values ---//
                string partNumber_1st_row_value = util.GetText(driver, Elements.PST_PartNumber_Column_1st_Row_Link);

                if (Locale.Equals("en"))
                {
                    //--- Expected Result: "Filter parts" is displayed as ghost text for the Part number filter ---//
                    test.validateString(driver, filterParts_text, util.ReturnAttribute(driver, Elements.PST_PartNumber_Column_InputBox, "placeholder"));
                }

                //--- Action: Enter any letter in the Part number filter textbox (e.g. A) ---//
                char partNumber_letter_input = partNumber_1st_row_value[0];
                action.IDeleteValueOnFields(driver, Elements.PST_PartNumber_Column_InputBox);
                action.IType(driver, Elements.PST_PartNumber_Column_InputBox, partNumber_letter_input.ToString());
                action.IType(driver, Elements.PST_PartNumber_Column_InputBox, Keys.Tab);

                //--- Expected Result: Parametric Table should be filtered by Part number containing the entered letter. ---//
                test.validateStringInstance(driver, util.GetText(driver, Elements.PST_PartNumber_Column_1st_Row_Link), partNumber_letter_input.ToString());

                //--- Action: Enter a letter that no part number in the table have (e.g. 1111111) ---//
                string partNumber_number_input = "1111111";
                action.IDeleteValueOnFields(driver, Elements.PST_PartNumber_Column_InputBox);
                action.IType(driver, Elements.PST_PartNumber_Column_InputBox, partNumber_number_input);
                action.IType(driver, Elements.PST_PartNumber_Column_InputBox, Keys.Tab);

                //--- Expected Result: Parametric Table should not display result ---//
                test.validateElementIsPresent(driver, Elements.PST_Rows_NoResults_Label);

                //--- Action: Removed any Input in the Part Number Text Box  ---//
                action.IDeleteValueOnFields(driver, Elements.PST_PartNumber_Column_InputBox);

                //--- Expected Result: The Blue Highlight on the Column will be Removed. (AL-11222 / IQ-4354) ---//
                test.validateElementIsPresent(driver, Elements.PST_Active_PartNumber_Column);
            }
        }

        [Test, Category("PST"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Multiple Parts Filter in Part Number Input Box is Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Multiple Parts Filter in Part Number Input Box is Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Multiple Parts Filter in Part Number Input Box is Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Multiple Parts Filter in Part Number Input Box is Working as Expected in RU Locale")]
        public void PST_ColumnHeaderFilterAndSort_VerifyMultiplePartsFilterInPartNumberInputBox(string Locale)
        {
            //--- Action: Access any Dynamic PST page. ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + pst_page_url);

            //--- Get the Default Values ---//
            string default_url = driver.Url;
            int default_column_count = util.GetCount(driver, Elements.PST_Columns);

            //----- PST Front End TestPlan - Redesign > Test Case Tab > R5 > TC3.1: Allow "|" delimiter to use in the part search to filter multiple parts -----//

            //--- Action: Enter two Part number in the Part number filter textbox with delimiter as separator (e.g. ADA4870|AD8003) ---//
            string partNumber_1st_row_value = util.GetText(driver, Elements.PST_PartNumber_Column_1st_Row_Link);
            string partNumber_2nd_row_value = util.GetText(driver, Elements.PST_PartNumber_Column_2nd_Row_Link);
            action.IDeleteValueOnFields(driver, Elements.PST_PartNumber_Column_InputBox);
            action.IType(driver, Elements.PST_PartNumber_Column_InputBox, partNumber_1st_row_value + "|" + partNumber_2nd_row_value);
            action.IType(driver, Elements.PST_PartNumber_Column_InputBox, Keys.Tab);

            //--- Expected Result: Parametric search table should be filtered by the two part number ---//
            test.validateStringIsCorrect(driver, Elements.PST_PartNumber_Column_1st_Row_Link, partNumber_1st_row_value);
            test.validateStringIsCorrect(driver, Elements.PST_PartNumber_Column_2nd_Row_Link, partNumber_2nd_row_value);
        }
    }
}