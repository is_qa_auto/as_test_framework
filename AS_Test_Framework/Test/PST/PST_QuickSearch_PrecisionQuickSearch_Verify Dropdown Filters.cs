﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using OpenQA.Selenium;

namespace AS_Test_Framework.PST
{
    [TestFixture]
    public class PST_QuickSearch_PrecisionQuickSearch_DropdownFilters : BaseSetUp
    {
        public PST_QuickSearch_PrecisionQuickSearch_DropdownFilters() : base() { }

        //--- URLs ---//
        string pst_page_url = "/parametricsearch/10825";

        [Test, Category("PST"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Interface Dropdown is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Interface Dropdown is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Interface Dropdown is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Interface Dropdown is Present and Working as Expected in RU Locale")]
        public void PST_QuickSearch_PrecisionQuickSearch_DropdownFilters_VerifyInterfaceDropdown(string Locale)
        {
            //--- Action: Access Dynamic PST with a Quick Search Tool ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + pst_page_url);

            if (util.CheckElement(driver, Elements.PST_PrecisionQuickSearch_Interface_Dropdown, 1))
            {
                //----- PST Front End TestPlan - Redesign > Test Case Tab > R3 > TC6.2: Verify dropdown filter -----//

                //--- Action: Select a value in the Interface quick filter ---//
                action.IClick(driver, Elements.PST_PrecisionQuickSearch_Interface_Dropdown);
                action.IClick(driver, By.CssSelector("select[id='interface']>option:nth-of-type(2)"));

                //--- Action: Click the Submit button ---//
                action.IClick(driver, Elements.PST_PrecisionQuickSearch_Submit_Button);

                //--- Action: Check "Data Output Interface" in Choose Parameters Dialog ---//
                action.IClick(driver, Elements.PST_ChooseParameters_Button);
                action.IClick(driver, Elements.PST_ChooseParameters_DialogBox_DeselectAll_Button);
                action.IClick(driver, Elements.PST_ChooseParameters_DialogBox_DataOutputInterface_Checkbox);
                action.IClick(driver, Elements.PST_ChooseParameters_DialogBox_Ok_Button);

                //--- Expected Result: Set value on the Interface quick filter should reflect on Interface column filter ---//
                test.validateElementIsSelected(driver, By.CssSelector("section[data-parameter='4365'] * input[id*='" + util.GetSelectedDropdownValue(driver, Elements.PST_PrecisionQuickSearch_Interface_Dropdown) + "']"));
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.PST_PrecisionQuickSearch_Interface_Dropdown);
            }
        }

        [Test, Category("PST"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Architecture Dropdown is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Architecture Dropdown is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Architecture Dropdown is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Architecture Dropdown is Present and Working as Expected in RU Locale")]
        public void PST_QuickSearch_PrecisionQuickSearch_DropdownFilters_VerifyArchitectureDropdown(string Locale)
        {
            //--- Action: Access Dynamic PST with a Quick Search Tool ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + pst_page_url);

            if (util.CheckElement(driver, Elements.PST_PrecisionQuickSearch_Architecture_Dropdown, 1))
            {
                //----- PST Front End TestPlan - Redesign > Test Case Tab > R3 > TC6.2: Verify dropdown filter -----//

                //--- Action: Select a value in the Architecture quick filter ---//
                action.IClick(driver, Elements.PST_PrecisionQuickSearch_Architecture_Dropdown);
                action.IClick(driver, By.CssSelector("select[id='architecture']>option:nth-of-type(2)"));

                //--- Action: Click the Submit button ---//
                action.IClick(driver, Elements.PST_PrecisionQuickSearch_Submit_Button);

                //--- Action: Check "Device Architecture" in Choose Parameters Dialog ---//
                action.IClick(driver, Elements.PST_ChooseParameters_Button);
                action.IClick(driver, Elements.PST_ChooseParameters_DialogBox_DeselectAll_Button);
                action.IClick(driver, Elements.PST_ChooseParameters_DialogBox_DeviceArchitecture_Checkbox);
                action.IClick(driver, Elements.PST_ChooseParameters_DialogBox_Ok_Button);

                //--- Expected Result: Set value on the Architecture quick filter should reflect on Device Architecture column filter ---//
                test.validateElementIsSelected(driver, By.CssSelector("section[data-parameter='4364'] * input[id*='" + util.GetSelectedDropdownValue(driver, Elements.PST_PrecisionQuickSearch_Architecture_Dropdown) + "']"));
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.PST_PrecisionQuickSearch_Architecture_Dropdown);
            }
        }

        [Test, Category("PST"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Input Type Dropdown is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Input Type Dropdown is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Input Type Dropdown is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Input Type Dropdown is Present and Working as Expected in RU Locale")]
        public void PST_QuickSearch_PrecisionQuickSearch_DropdownFilters_VerifyInputTypeDropdown(string Locale)
        {
            //--- Action: Access Dynamic PST with a Quick Search Tool ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + pst_page_url);

            if (util.CheckElement(driver, Elements.PST_PrecisionQuickSearch_InputType_Dropdown, 1))
            {
                //----- PST Front End TestPlan - Redesign > Test Case Tab > R3 > TC6.2: Verify dropdown filter -----//

                //--- Action: Select a value in the Input type quick filter ---//
                action.IClick(driver, Elements.PST_PrecisionQuickSearch_InputType_Dropdown);
                action.IClick(driver, By.CssSelector("select[id='inputType']>option:nth-of-type(2)"));

                //--- Action: Click the Submit button ---//
                action.IClick(driver, Elements.PST_PrecisionQuickSearch_Submit_Button);

                //--- Action: Check "Input Type" in Choose Parameters Dialog ---//
                action.IClick(driver, Elements.PST_ChooseParameters_Button);
                action.IClick(driver, Elements.PST_ChooseParameters_DialogBox_DeselectAll_Button);
                action.IClick(driver, Elements.PST_ChooseParameters_DialogBox_InputType_Checkbox);
                action.IClick(driver, Elements.PST_ChooseParameters_DialogBox_Ok_Button);

                //--- Expected Result: Set value on the Input type quick filter should reflect on Input type column filter ---//
                test.validateElementIsSelected(driver, By.CssSelector("section[data-parameter='4363'] * input[id*='" + util.GetSelectedDropdownValue(driver, Elements.PST_PrecisionQuickSearch_InputType_Dropdown) + "']"));
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.PST_PrecisionQuickSearch_InputType_Dropdown);
            }
        }
    }
}