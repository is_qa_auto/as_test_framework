﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Collections.Generic;

namespace AS_Test_Framework.Adobe_Analytics
{

    [TestFixture]
    public class AA_Requirement_2 : BaseSetUp
    {
        public AA_Requirement_2() : base() { }

        //[Test, Category("AA"), Category("Core")]
        //[TestCase(TestName = "Verify that the personalized page call is firing on the application page")]
        //public void AA_Personalized_App()
        //{
        //    action.Navigate(driver, Configuration.Env_Url + "en/index.html");
        //    action.IClick(driver, Elements.MainNavigation_ApplicationsTab);
        //    action.IClick(driver, By.CssSelector("a[data-category='Aerospace and Defense']"));
        //    var currentWindow = driver.CurrentWindowHandle;

        //    Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

        //    Assert.Multiple(() =>
        //    {
        //        Assert.True(GetTags.ContainsKey("events"));
        //        test.validateString(driver, "event38", GetTags["events"]);
        //    });

        //    Assert.True(GetTags.ContainsKey("list1"));
        //}
    }
}