﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Collections.Generic;
using OpenQA.Selenium.Interactions;
using System.Threading;

namespace AS_Test_Framework.Adobe_Analytics
{

    [TestFixture]
    public class AA_Requirement_20: BaseSetUp
    {
        public AA_Requirement_20() : base() { }

        [Test, Category("AA"), Category("Core")]
        [TestCase(TestName = "Verify that the analytics call is firing when a Product is removed from the Saved List")]
        public void AA_MyAnalogAddProduct()
        {
            action.Navigate(driver, Configuration.Env_Url + "en/ad7960");
            action.IClick(driver, Elements.PDP_MyAnalogWidget_Btn);
            //action.IClick(driver, Elements.MyAnalog_Widget_LogInToMyAnalog_Btn);
            action.ILogin(driver, "aries.sorosoro@analog.com", "Test_1234");
            //action.IClick(driver, Elements.PDP_MyAnalogWidget_Btn);
            action.IClick(driver, By.CssSelector("div[class='saveTo createProject'] button[id='saveProductToMyanalog']"));
            driver.Navigate().GoToUrl(Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + "en/app/products");
            action.IClick(driver, By.CssSelector("section[class='product']>div>div[class='controls']>button"));
            action.IClick(driver, By.CssSelector("section[class='product']>div>div[class='controls']>menu>button:nth-child(2)"));
            action.IClick(driver, By.CssSelector("section[class='product'] * div[class='modal-dialog'] * button[class='ma btn btn-primary']"));

            var currentWindow = driver.CurrentWindowHandle;

            Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("events"));
                test.validateString(driver, "event122", GetTags["events"]);
            });


            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar97"));
                test.validateString(driver, "ad7960", GetTags["eVar97"]);
            });
        }
    }
}