﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Collections.Generic;
using OpenQA.Selenium.Interactions;
using System.Threading;

namespace AS_Test_Framework.Adobe_Analytics
{

    [TestFixture]
    public class AA_Requirement_13 : BaseSetUp
    {
        public AA_Requirement_13() : base() { }

        [Test, Category("AA"), Category("Core")]
        [TestCase(TestName = "Verify that the analytics call is firing when clicking First name field and submit button for Customer and Technical forms")]
        public void AA_Forms_2()
        {
            action.Navigate(driver, Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + "Form_Pages/support/customerService.aspx ");
            action.IClick(driver, Elements.Forms_FirstName_InputBox);
            var currentWindow = driver.CurrentWindowHandle;
            Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("events"));
                test.validateString(driver, "event103", GetTags["events"]);
            });

            driver.Close();
            driver.SwitchTo().Window(currentWindow);
            driver.Navigate().Refresh();
            action.IClick(driver, Elements.Forms_Submit_Button);

            GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("events"));
                test.validateString(driver, "event104", GetTags["events"]);
            });
        }

        [Test, Category("AA"), Category("Core")]
        [TestCase(TestName = "Verify that email are captured on forms")]
        public void AA_Forms_3()
        {
            action.Navigate(driver, Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + "Form_Pages/support/customerService.aspx ");
            action.IType(driver, Elements.Forms_CustomerServiceSupport_EmailId_InputBox, "aries.sorosoro@analog.com");
            action.IClick(driver, By.CssSelector("input[id='_content_submit']"));
            var currentWindow = driver.CurrentWindowHandle;
            Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar59"));
                test.validateString(driver, "aries.sorosoro@analog.com", GetTags["eVar59"]);
            });
            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("Request Type"));
                test.validateString(driver, "lnk_o", GetTags["Request Type"]);
            });
            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("Friendly Name (non-page)"));
                test.validateString(driver, "Customer Form: Submit Clicked", GetTags["Friendly Name (non-page)"]);
            });
        }
    }
}