﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Collections.Generic;
using OpenQA.Selenium.Interactions;
using System.Threading;

namespace AS_Test_Framework.Adobe_Analytics
{

    [TestFixture]
    public class AA_Requirement_17 : BaseSetUp
    {
        public AA_Requirement_17() : base() { }

        [Test, Category("AA"), Category("Core")]
        [TestCase(TestName = "Verify and track the analytic call in network Where a visitor clicks on \"Skip All\"" )]
        public void AA_Reg1()
        {
            action.Navigate(driver, Configuration.Env_Url.Replace("www","my").Replace("cldnet","corpnt") + "en/app/registration");
            action.IClick(driver,Elements.Registration_Skip_Btn);
            var currentWindow = driver.CurrentWindowHandle;

            Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("events"));
                test.validateString(driver, "event116", GetTags["events"]);
            });


            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar62"));
                test.validateString(driver, "Skipped | categories", GetTags["eVar62"]);
            });
        }

        [Test, Category("AA"), Category("Core")]
        [TestCase(TestName = "Verify and track the analytic call in network Where a visitor clicks on \"Skip All\" on market section")]
        public void AA_Reg2()
        {
            action.Navigate(driver, Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + "en/app/registration");
            action.IClick(driver, Elements.Registration_Next_Btn);
            action.IClick(driver, Elements.Registration_Skip_Btn);
            var currentWindow = driver.CurrentWindowHandle;

            Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("events"));
                test.validateString(driver, "event116", GetTags["events"]);
            });


            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar62"));
                test.validateString(driver, "Skipped | markets", GetTags["eVar62"]);
            });
        }

        [Test, Category("AA"), Category("Core")]
        [TestCase(TestName = "Verify and track the analytic call in network Where a visitor clicks on \"Skip All\" on occupation section")]
        public void AA_Reg3()
        {
            action.Navigate(driver, Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + "en/app/registration");
            action.IClick(driver, Elements.Registration_Next_Btn);
            action.IClick(driver, Elements.Registration_Next_Btn);
            action.IClick(driver, Elements.Registration_Skip_Btn);
            var currentWindow = driver.CurrentWindowHandle;

            Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("events"));
                test.validateString(driver, "event116", GetTags["events"]);
            });


            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar62"));
                test.validateString(driver, "Skipped | occupations", GetTags["eVar62"]);
            });
        }


        [Test, Category("AA"), Category("Core")]
        [TestCase(TestName = "Verify and track the analytic call in network where Visitor selects categories, markets, occupation and fills out the form")]
        public void AA_Reg4()
        {
            action.Navigate(driver, Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + "en/app/registration");
            string ProdCat = util.GetText(driver, By.CssSelector("section[class='categories']>div[class='options grid']>div>label"));
            action.IClick(driver, By.CssSelector("section[class='categories']>div[class='options grid']>div>label"));
            action.IClick(driver, Elements.Registration_Next_Btn);

            var currentWindow = driver.CurrentWindowHandle;

            Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("events"));
                test.validateString(driver, "event112", GetTags["events"]);
            });


            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar62"));
                test.validateString(driver, ProdCat, GetTags["eVar62"].ToUpper());
            });

            driver.Close();
            driver.SwitchTo().Window(currentWindow);
            string Market = util.GetText(driver, By.CssSelector("section[class='markets']>div[class='options grid']>div>label"));
            action.IClick(driver, By.CssSelector("section[class='markets']>div[class='options grid']>div>label"));
            action.IClick(driver, Elements.Registration_Next_Btn);

            GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("events"));
                test.validateString(driver, "event113", GetTags["events"]);
            });


            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar97"));
                test.validateString(driver, Market.ToLower(), GetTags["eVar97"]);
            });

            driver.Close();
            driver.SwitchTo().Window(currentWindow);

            string Occupation = util.GetText(driver, By.CssSelector("section[class='occupations']>div[class='large options grid']>div>label"));
            action.IClick(driver, By.CssSelector("section[class='occupations']>div[class='large options grid']>div>label"));
            GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("events"));
                test.validateString(driver, "event114", GetTags["events"]);
            });


            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar97"));
                test.validateString(driver, Occupation.ToLower(), GetTags["eVar97"]);
            });

        }
    }
}