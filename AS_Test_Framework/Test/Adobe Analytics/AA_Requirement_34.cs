﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Collections.Generic;
using OpenQA.Selenium.Interactions;
using System.Threading;
using System;

namespace AS_Test_Framework.Adobe_Analytics
{

    [TestFixture]
    public class AA_Requirement_34: BaseSetUp
    {
        public AA_Requirement_34() : base() { }

        [Test, Category("AA"), Category("Core")]
        [TestCase(TestName = "Verify Prop17 on PDP")]
        public void AA_MW1()
        {
            action.Navigate(driver, Configuration.Env_Url + "en/index.html");

            driver.Navigate().GoToUrl(Configuration.Env_Url + "en/lt8636");
            Console.WriteLine("Go to: " + Configuration.Env_Url + "en/lt8636");

            var currentWindow = driver.CurrentWindowHandle;

            Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("prop17"));
                test.validateString(driver, "/products/lt8636.html", GetTags["prop17"]);
            });
        }

        [Test, Category("AA"), Category("Core")]
        [TestCase(TestName = "Verify analytics call for Cross Reference Search - Search by Manufacturer")]
        public void AA_XRef_SearchByMfg()
        {
            action.Navigate(driver, Configuration.Env_Url + "cpsearch/CrossReferenceSearch.aspx");
            action.ISelectFromDropdown(driver, Elements.XRef_XRefSearch_Mfg_Dropdown, "133");
            action.IClick(driver, Elements.XRef_XRefSearch_Mfg_Search_Button);

            var currentWindow = driver.CurrentWindowHandle;

            Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("events"));
                test.validateString(driver, "event6", GetTags["events"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar15"));
                test.validateString(driver, "133", GetTags["eVar15"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar16"));
                test.validateString(driver, "crsearch", GetTags["eVar16"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar19"));
                test.validateString(driver, "D=v16", GetTags["eVar19"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar27"));
                test.validateString(driver, "+1", GetTags["eVar27"]);
            });
        }
        
    }
}