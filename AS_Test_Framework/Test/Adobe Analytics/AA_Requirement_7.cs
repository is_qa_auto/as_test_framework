﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Collections.Generic;
using OpenQA.Selenium.Interactions;

namespace AS_Test_Framework.Adobe_Analytics
{

    [TestFixture]
    public class AA_Requirement_7 : BaseSetUp
    {
        public AA_Requirement_7() : base() { }

        [Test, Category("AA"), Category("Core")]
        [TestCase(TestName = "Verify that the analytics call is firing when subscribing on Analog Dialogue")]
        public void AA_AD_Subscribe()
        {
            action.Navigate(driver, Configuration.Env_Url + "en/analog-dialogue.html");
            action.IClick(driver, Elements.AD_Subscribe_Btn);
            action.IType(driver, Elements.AD_Subscribe_EmailAddress_TextField, "aries.sorosoro@analog.com");
            action.IClick(driver, Elements.Registration_TermsOfUse_ChxBox);
            action.IClick(driver, Elements.Registration_Consent_Comm);
            action.IClick(driver, Elements.AD_Subscribe_Submit_Btn);

            var currentWindow = driver.CurrentWindowHandle;

            Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("events"));
                test.validateString(driver,  "event74", GetTags["events"]);
            });
            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar80"));
                test.validateString(driver, "aries.sorosoro@analog.com", GetTags["eVar80"]);
            });
        }

        [Test, Category("AA"), Category("Core")]
        [TestCase(TestName = "Verify that the analytics call is firing when emailing an Author on Analog Dialogue")]
        public void AA_AD_Email()
        {
            action.Navigate(driver, Configuration.Env_Url + "en/analog-dialogue.html");
            action.IClick(driver, By.CssSelector("div[class='col-lg-8 col-md-8 banner-left'] * a"));
            action.IClick(driver, By.CssSelector("div[class='author-details']>p[class='social']>a"));
           
            var currentWindow = driver.CurrentWindowHandle;

            Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("events"));
                test.validateString(driver, "event75", GetTags["events"]);
            });

        }

    }
}