﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Collections.Generic;
using OpenQA.Selenium.Interactions;

namespace AS_Test_Framework.Adobe_Analytics
{

    [TestFixture]
    public class AA_Requirement_42 : BaseSetUp
    {
        public AA_Requirement_42() : base() { }

        [Test, Category("AA"), Category("Core")]
        [TestCase(TestName = "Verify analytics call for myAnalog Products page - Register an evaluation board")]
        public void AA_RegisterEvalBoard()
        {
            action.Navigate(driver, Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + "en/app/products");
            action.ILogin(driver, "aries.sorosoro@analog.com", "Test_1234");
            action.IClick(driver, By.CssSelector("div[class='tabs']>button:nth-child(2)"));
            action.IClick(driver, By.CssSelector("section[class='boardRegistration'] button"));

            var currentWindow = driver.CurrentWindowHandle;

            Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("events"));
                test.validateString(driver, "event210", GetTags["events"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("Friendly Name (non-page)"));
                test.validateString(driver, "Hardware/Software Product registration: myAnalog", GetTags["Friendly Name (non-page)"]);
            });

        }
    }
}