﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Collections.Generic;
using OpenQA.Selenium.Interactions;
using System;
using System.Threading;

namespace AS_Test_Framework.Adobe_Analytics
{

    [TestFixture]
    public class AA_Requirement_6 : BaseSetUp
    {
        public AA_Requirement_6() : base() { }

        [Test, Category("AA"), Category("Core")]
        [TestCase(TestName = "Verify General Analytisc call for on-site search")]
        public void AA_Pst_Events12()
        {
            // Console.WriteLine("year=" + DateTime.Now.Year.ToString() + " | month=" + DateTime.Today.ToString("MMMM") + " | date=" + DateTime.Now.Day.ToString() + " | day=" + DateTime.Now.DayOfWeek.ToString() + " | time=" + TimeZoneInfo.ConvertTime(, TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time")).ToString("h:mm tt"));
            action.Navigate(driver, Configuration.Env_Url + "en/index.html");
            action.ISearchViaGlobalSearchTextbox(driver, "AD706");
            Thread.Sleep(3000);
            DateTime TimeToday = DateTime.Now;
            string WindowTitle = driver.Title.ToLower();
            Thread.Sleep(3000);
            string Search_Url = driver.Url.Replace("https://", "").Replace("#/", "");
            var currentWindow = driver.CurrentWindowHandle;

            Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar1"));
                test.validateString(driver, Search_Url.Split('?')[0], GetTags["eVar1"]);
            });
            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("hier1"));
                test.validateString(driver, Configuration.Env_Url.Replace("https://","").Replace("/","") + "|||", GetTags["hier1"]);
            });
            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("prop2"));
                test.validateString(driver, "new", GetTags["prop2"]);
            });
            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar2"));
                test.validateString(driver, "D=c2", GetTags["eVar2"]);
            });
            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar3"));
                test.validateString(driver, "D=c3", GetTags["eVar3"]);
            });
            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("prop4"));
                test.validateString(driver, "1", GetTags["prop4"]);
            });
            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar4"));
                test.validateString(driver, "D=c4", GetTags["eVar4"]);
            });
            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar5"));
                test.validateString(driver, "D=c5", GetTags["eVar5"]);
            });
            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar6"));
                test.validateString(driver, "D=c6", GetTags["eVar6"]);
            });
            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("prop7"));
                test.validateString(driver, "year=" + DateTime.Now.Year.ToString() + " | month=" + DateTime.Today.ToString("MMMM") + " | date=" + DateTime.Now.Day.ToString() + " | day=" + DateTime.Now.DayOfWeek.ToString() + " | time=" + TimeZoneInfo.ConvertTime(TimeToday, TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time")).ToString("h:mm tt"), GetTags["prop7"]);
            });
            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar7"));
                test.validateString(driver, "D=c7", GetTags["eVar7"]);
            });
           
            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar9"));
                test.validateString(driver, "non-member", GetTags["eVar9"]);
            });
            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("prop10"));
                test.validateString(driver, Configuration.Env_Url.Replace("https://", "") + "en/index.html", GetTags["prop10"]);
            });
            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar10"));
                test.validateString(driver, "logged out", GetTags["eVar10"]);
            });
            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("prop13"));
                test.validateString(driver, "D=ch+'|'+v51", GetTags["prop13"]);
            });
            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar13"));
                test.validateString(driver, "D=c10", GetTags["eVar13"]);
            });
            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("prop18"));
                test.validateString(driver, "D=ch", GetTags["prop18"]);
            });
            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("prop19"));
                test.validateString(driver, "D=c1", GetTags["prop19"]);
            });
            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("prop22"));
                test.validateString(driver, "en", GetTags["prop22"]);
            });
            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar22"));
                test.validateString(driver, "D=c25", GetTags["eVar22"]);
            });
            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar24"));
                test.validateString(driver, "D=c22", GetTags["eVar24"]);
            });
            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("prop25"));
                test.validateString(driver, WindowTitle, GetTags["prop25"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar39"));
                test.validateString(driver, "D=s_vi", GetTags["eVar39"]);
            });
            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar51"));
                test.validateString(driver, "en", GetTags["eVar51"]);
            });
        }

        [Test, Category("AA"), Category("Core")]
        [TestCase(TestName = "Verify that the click event call is firing when the auto suggest result is clicked")]
        public void AA_Search_Auto()
        {
            action.Navigate(driver, Configuration.Env_Url + "en/index.html");
            action.IType(driver, Elements.GlobalSearchTextField, "AD79");

            string Search_Auto = util.GetText(driver, By.CssSelector("ul[class^='auto-complete']>li:nth-child(1) a>span[class='search-content-title']"));

            var currentWindow = driver.CurrentWindowHandle;

            IWebElement body = driver.FindElement(By.CssSelector("ul[class^='auto-complete']>li:nth-child(1) a"));
            Actions OpenLink = new Actions(driver);
            OpenLink.KeyDown(Keys.Control).MoveToElement(body).Click().Perform();

            Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("events"));
                test.validateString(driver, "event85", GetTags["events"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("prop42"));
                test.validateString(driver, "AD79", GetTags["prop42"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar89"));
                test.validateString(driver, Configuration.Env_Url + "en/products/" + Search_Auto.ToLower() + ".html", GetTags["eVar89"]);
            });
        }

        [Test, Category("AA"), Category("Core")]
        [TestCase(TestName = "Verify that the analytics call is firing when a search result is clicked")]
        public void AA_Search_Result_Click() {
            action.Navigate(driver, Configuration.Env_Url + "en/index.html");
            action.ISearchViaGlobalSearchTextbox(driver, "AD9361");
            var currentWindow = driver.CurrentWindowHandle;

            string Url = util.ReturnAttribute(driver, By.CssSelector("div[class='search-results-items']>div:nth-child(3) * a"), "href");
            IWebElement body = driver.FindElement(By.CssSelector("div[class='search-results-items']>div:nth-child(3) * a"));
            Actions OpenLink = new Actions(driver);
            OpenLink.KeyDown(Keys.Control).MoveToElement(body).Click().Perform();
            
            Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("events"));
                test.validateStringInstance(driver, GetTags["events"], "event24");
                test.validateStringInstance(driver, GetTags["events"], "event67:3");
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar29"));
                test.validateString(driver, "3", GetTags["eVar29"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar89"));
                test.validateString(driver, Url, GetTags["eVar89"]);
            });
        }

        [Test, Category("AA"), Category("Core")]
        [TestCase(TestName = "Verify that the analytics call is firing when search correction is triggered")]
        public void AA_ShowInstead_Result()
        {
            action.Navigate(driver, Configuration.Env_Url + "en/index.html");
            action.ISearchViaGlobalSearchTextbox(driver, "AD9325");

            string prop41_value = util.GetText(driver, By.CssSelector("a[class='no-spell-check']")) + "|" + util.GetText(driver, By.CssSelector("div[class='suggestion-container'] * strong"));

            var currentWindow = driver.CurrentWindowHandle;

            Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("events"));
                test.validateString(driver, "event84", GetTags["events"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("prop41"));
                test.validateString(driver, prop41_value, GetTags["prop41"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("Request Type"));
                test.validateString(driver, "lnk_o", GetTags["Request Type"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("Friendly Name (non-page)"));
                test.validateString(driver, "Onsite Search Correction", GetTags["Friendly Name (non-page)"]);
            });
        }

        [Test, Category("AA"), Category("Core")]
        [TestCase(TestName = "Verify that the click event call is firing when Sample and Buy in the baseball card is clicked")]
        public void AA_SampleAndBuy_Click()
        {
            action.Navigate(driver, Configuration.Env_Url + "en/index.html");
            action.ISearchViaGlobalSearchTextbox(driver, "AD7960");

            IWebElement body = driver.FindElement(By.CssSelector("div[class='search-results-item top-result']>ul>li:nth-child(3)>a"));
            Actions OpenLink = new Actions(driver);
            OpenLink.KeyDown(Keys.Control).MoveToElement(body).Click().Perform();

            var currentWindow = driver.CurrentWindowHandle;

            Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("events"));
                test.validateStringInstance(driver, GetTags["events"], "event101");
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("prop46"));
                test.validateString(driver, "Sample & Buy", GetTags["prop46"]);
            });
            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("Request Type"));
                test.validateString(driver, "lnk_o", GetTags["Request Type"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("Friendly Name (non-page)"));
                test.validateString(driver, "link clicked", GetTags["Friendly Name (non-page)"]);
            });
        }

        [Test, Category("AA"), Category("Core")]
        [TestCase(TestName = "Verify that the analytics call is firing when an Alternative Part is clicked")]
        public void AA_AlternativeParts_Click()
        {
            action.Navigate(driver, Configuration.Env_Url + "en/index.html");
            action.ISearchViaGlobalSearchTextbox(driver, "AD5325");

            IWebElement body = driver.FindElement(By.CssSelector("div[class='link-list'] * ul>li>a"));
            Actions OpenLink = new Actions(driver);
            OpenLink.KeyDown(Keys.Control).MoveToElement(body).Click().Perform();

            var currentWindow = driver.CurrentWindowHandle;

            Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("events"));
                test.validateStringInstance(driver, GetTags["events"], "event24");
            });
            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("Request Type"));
                test.validateString(driver, "lnk_o", GetTags["Request Type"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("Friendly Name (non-page)"));
                test.validateString(driver, "Onsite Search: Alternative parts Clicked", GetTags["Friendly Name (non-page)"]);
            });
        }

        [Test, Category("AA"), Category("Core")]
        [TestCase(TestName = "Verify that the analytics call is firing when an File in baseball card is clicked")]
        public void AA_BBFile_Click()
        {
            action.Navigate(driver, Configuration.Env_Url + "en/index.html");
            action.ISearchViaGlobalSearchTextbox(driver, "AD7960");
            string Search_Url = driver.Url.Replace("https://", "").Replace("#/", "");
            string DataSheeturl = util.ReturnAttribute(driver, By.CssSelector("ul[class='resources-links']>li:nth-child(1)>a"), "href");
            //Console.WriteLine(DataSheeturl);

            IWebElement body = driver.FindElement(By.CssSelector("ul[class='resources-links']>li:nth-child(1)>a"));
            Actions OpenLink = new Actions(driver);
            OpenLink.KeyDown(Keys.Control).MoveToElement(body).Click().Perform();

            var currentWindow = driver.CurrentWindowHandle;

            Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("events"));
                test.validateString(driver, "event1", GetTags["events"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar1"));
                test.validateString(driver, Search_Url.Split('?')[0], GetTags["eVar1"]);
            });
            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar8"));
                test.validateString(driver, DataSheeturl, GetTags["eVar8"]);
            });
            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("prop11"));
                test.validateString(driver, DataSheeturl, GetTags["prop11"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("Request Type"));
                test.validateString(driver, "lnk_o", GetTags["Request Type"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("Friendly Name (non-page)"));
                test.validateString(driver, "Onsite Search: Data-Sheet & User-Guide Download", GetTags["Friendly Name (non-page)"]);
            });
        }

        [Test, Category("AA"), Category("Core")]
        [TestCase(TestName = "Verify that the analytics call is firing when product categiry is clicked")]
        public void AA_ProdCatLink_Click()
        {
            action.Navigate(driver, Configuration.Env_Url + "en/index.html");
            action.ISearchViaGlobalSearchTextbox(driver, "AD9361");

            string ProdCat_Url = util.ReturnAttribute(driver, By.CssSelector("div[class='search-results'] span[class='title-category']>a"), "href");

            IWebElement body = driver.FindElement(By.CssSelector("div[class='search-results'] span[class='title-category']>a"));
            Actions OpenLink = new Actions(driver);
            OpenLink.KeyDown(Keys.Control).MoveToElement(body).Click().Perform();

            var currentWindow = driver.CurrentWindowHandle;

            Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("events"));
                test.validateStringInstance(driver, GetTags["events"], "event67:1");
            });
            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("events"));
                test.validateStringInstance(driver, GetTags["events"], "event24");
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar29"));
                test.validateString(driver, "1", GetTags["eVar29"]);
            });
            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar89"));
                test.validateString(driver, ProdCat_Url, GetTags["eVar89"]);
            });
        }
    }
}