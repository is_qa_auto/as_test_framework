﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using OpenQA.Selenium.Interactions;

namespace AS_Test_Framework.Adobe_Analytics
{

    [TestFixture]
    public class AA_Requirement_40 : BaseSetUp
    {
        public AA_Requirement_40() : base() { }

        [Test, Category("AA"), Category("Core")]
        [TestCase(TestName = "Verify the sub_menu analytics tracking")]
        public void AA_ADISignalPlus1()
        {
            action.Navigate(driver, Configuration.Env_Url + "en/signals.html");
            action.IClick(driver, By.CssSelector("header[class='ch-masthead'] menu[class='ch-masthead__navigation__menu container']>li:nth-child(2)>a"));

            var currentWindow = driver.CurrentWindowHandle;

            Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("events"));
                test.validateString(driver, "event217", GetTags["events"]);
            });
        }

        [Test, Category("AA"), Category("Core")]
        [TestCase(TestName = "Verify the Related Content: Market and technology analytics tracking")]
        public void AA_ADISignalPlus3()
        {
            action.Navigate(driver, Configuration.Env_Url + "en/signals/articles/the-next-step-in-manufacturing.html");
            string Market_Title = util.GetText(driver, Elements.SignalPlus_RelatedSection_MarketAndTechnology_Category).Split('(')[0].Trim();

            IWebElement body = driver.FindElement(By.CssSelector("div[class='related__markets-and-technologies']>ul>li a"));
            Actions OpenLink = new Actions(driver);
            OpenLink.KeyDown(Keys.Control).MoveToElement(body).Click().Perform();

            var currentWindow = driver.CurrentWindowHandle;

            Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("events"));
                test.validateString(driver, "event212", GetTags["events"]);
            });
        }

        [Test, Category("AA"), Category("Core")]
        [TestCase(TestName = "Verify the Related Content: Resources analytics tracking")]
        public void AA_ADISignalPlus5()
        {
            action.Navigate(driver, Configuration.Env_Url + "en/signals/articles/the-next-step-in-manufacturing.html");

            IWebElement body = driver.FindElement(By.CssSelector("div[class='related__resources']>ul>li a"));
            Actions OpenLink = new Actions(driver);
            OpenLink.KeyDown(Keys.Control).MoveToElement(body).Click().Perform();

            var currentWindow = driver.CurrentWindowHandle;

            Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("events"));
                test.validateStringInstance(driver, GetTags["events"], "event213");
            });
        }

    }
}