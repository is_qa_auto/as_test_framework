﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Collections.Generic;

namespace AS_Test_Framework.Adobe_Analytics
{

    [TestFixture]
    public class AA_Requirement_1 : BaseSetUp
    {
        public AA_Requirement_1() : base() { }

        [Test, Category("AA"), Category("Core")]
        [TestCase(TestName = "Verify that the Demandbase call is firing on the first page load - New Visitor")]
        public void AA_FirstLoad()
        {
            action.Navigate(driver, Configuration.Env_Url + "en/index.html");
            var currentWindow = driver.CurrentWindowHandle;

            Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("events"));
                test.validateString(driver, "event100", GetTags["events"]);
            });

        }

        [Test, Category("AA"), Category("Core")]
        [TestCase(TestName = "Verify that the Demandbase call is firing on the first page load for PDP - New Visitor")]
        public void AA_FirstLoadPDP()
        {
            action.Navigate(driver, Configuration.Env_Url + "en/ad706.html");
            var currentWindow = driver.CurrentWindowHandle;

            Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("events"));
                test.validateString(driver, "event100", GetTags["events"]);
            });

        }

        [Test, Category("AA"), Category("Core")]
        [TestCase(TestName = "Verify that the Demandbase call is firing on the first page load for Category Page - New Visitor")]
        public void AA_FirstLoadCategory()
        {
            action.Navigate(driver, Configuration.Env_Url + "en/amplifiers.html");
            var currentWindow = driver.CurrentWindowHandle;

            Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("events"));
                test.validateString(driver, "event100", GetTags["events"]);
            });

        }

    }
}