﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Collections.Generic;
using OpenQA.Selenium.Interactions;

namespace AS_Test_Framework.Adobe_Analytics
{

    [TestFixture]
    public class AA_Requirement_9 : BaseSetUp
    {
        public AA_Requirement_9() : base() { }

        [Test, Category("AA"), Category("Core")]
        [TestCase(TestName = "Verify that the analytics call is firing when downloading EXE files on Design Center")]
        public void AA_DownloadFile_Exe()
        {
            
            action.Navigate(driver, Configuration.Env_Url + "en/design-center/ltpower-play.html");
            action.IClick(driver, By.CssSelector("span[class='analog-rounded-button']>a"));


            var currentWindow = driver.CurrentWindowHandle;

            Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("events"));
                test.validateString(driver, "event1", GetTags["events"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar1"));
                test.validateString(driver, Configuration.Env_Url.Replace("https://", "") + "en/design-center/ltpower-play.html", GetTags["eVar1"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar8"));
                test.validateString(driver, "https://ltpowerplay.analog.com/downloads/first_time_installer/setupltpowerplay.exe", GetTags["eVar8"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("prop11"));
                test.validateString(driver, "https://ltpowerplay.analog.com/downloads/first_time_installer/setupltpowerplay.exe", GetTags["prop11"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar32"));
                test.validateString(driver, "exe", GetTags["eVar32"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("Request Type"));
                test.validateString(driver, "lnk_d", GetTags["Request Type"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("Friendly Name (non-page)"));
                test.validateString(driver, "https://ltpowerplay.analog.com/downloads/first_time_installer/setupltpowerplay.exe", GetTags["Friendly Name (non-page)"]);
            });

        }

        //[Test, Category("AA"), Category("Core")]
        //[TestCase(TestName = "Verify that the analytics call is firing when downloading DMG files on Design Center")]
        //public void AA_DownloadFile_Dmg()
        //{
        //    action.Navigate(driver, Configuration.Env_Url + "en/design-center/design-tools-and-calculators/ltspice-simulator.html");
        //    action.IClick(driver, By.CssSelector("div[class='container']>div>p:nth-of-type(3) span[class='analog-rounded-button']>a"));


        //    var currentWindow = driver.CurrentWindowHandle;

        //    Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

        //    Assert.Multiple(() =>
        //    {
        //        Assert.True(GetTags.ContainsKey("events"));
        //        test.validateString(driver, "event1", GetTags["events"]);
        //    });

        //    Assert.Multiple(() =>
        //    {
        //        Assert.True(GetTags.ContainsKey("eVar1"));
        //        test.validateString(driver, Configuration.Env_Url.Replace("https://", "") + "en/design-center/design-tools-and-calculators/ltspice-simulator.html", GetTags["eVar1"]);
        //    });

        //    Assert.Multiple(() =>
        //    {
        //        Assert.True(GetTags.ContainsKey("eVar8"));
        //        test.validateString(driver, "http://ltspice.analog.com/software/LTspice.dmg", GetTags["eVar8"]);
        //    });

        //    Assert.Multiple(() =>
        //    {
        //        Assert.True(GetTags.ContainsKey("prop11"));
        //        test.validateString(driver, "http://ltspice.analog.com/software/LTspice.dmg", GetTags["prop11"]);
        //    });

        //    Assert.Multiple(() =>
        //    {
        //        Assert.True(GetTags.ContainsKey("eVar32"));
        //        test.validateString(driver, "dmg", GetTags["eVar32"]);
        //    });


        //}

        [Test, Category("AA"), Category("Core")]
        [TestCase(TestName = "Verify that the analytics call is firing when downloading PDF files on Design Center")]
        public void AA_DownloadFile_Pdf()
        {
            action.Navigate(driver, Configuration.Env_Url + "en/design-center/design-tools-and-calculators/ltspice-simulator.html");
            action.IClick(driver, By.CssSelector("div[class='row fca-row one-columnspot top-spacing-32 bottom-spacing-32 '] * div[class='clearfix rte']>p:nth-child(2)>a"));

            var currentWindow = driver.CurrentWindowHandle;

            Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("events"));
                test.validateString(driver, "event1", GetTags["events"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar1"));
                test.validateString(driver, Configuration.Env_Url.Replace("https://","") + "en/design-center/design-tools-and-calculators/ltspice-simulator.html", GetTags["eVar1"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar8"));
                test.validateString(driver, Configuration.Env_Url + "media/en/simulation-models/spice-models/LTspice_ShortcutFlyer.pdf", GetTags["eVar8"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("prop11"));
                test.validateString(driver, Configuration.Env_Url + "media/en/simulation-models/spice-models/LTspice_ShortcutFlyer.pdf", GetTags["prop11"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar32"));
                test.validateString(driver, "pdf", GetTags["eVar32"]);
            });

        }

        [Test, Category("AA"), Category("Core")]
        [TestCase(TestName = "Verify that the analytics call is firing when downloading ZIP files on Design Center")]
        public void AA_DownloadFile_Zip()
        {
            action.Navigate(driver, Configuration.Env_Url + "en/design-center/landing-pages/001/transceiver-evaluation-software.html");
            action.IClick(driver, By.CssSelector("div[id='rte-body'] * tbody>tr:nth-child(2)>td:nth-child(3)>a"));

            var currentWindow = driver.CurrentWindowHandle;

            Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("events"));
                test.validateString(driver, "event1", GetTags["events"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar1"));
                test.validateString(driver, Configuration.Env_Url.Replace("https://", "") + "en/design-center/landing-pages/001/transceiver-evaluation-software.html", GetTags["eVar1"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar8"));
                test.validateString(driver, Configuration.Env_Url + "media/en/evaluation-boards-kits/evaluation-software/MYK_v2068.zip", GetTags["eVar8"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("prop11"));
                test.validateString(driver, Configuration.Env_Url + "media/en/evaluation-boards-kits/evaluation-software/MYK_v2068.zip", GetTags["prop11"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar32"));
                test.validateString(driver, "zip", GetTags["eVar32"]);
            });

        }

    }
}