﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Collections.Generic;
using OpenQA.Selenium.Interactions;

namespace AS_Test_Framework.Adobe_Analytics
{

    [TestFixture]
    public class AA_Requirement_3 : BaseSetUp
    {
        public AA_Requirement_3() : base() { }

        [Test, Category("AA"), Category("Core")]
        [TestCase(TestName = "Verify that the click event call is firing when the Company menu tab is clicked")]
        public void AA_Click_Event_Company()
        {
            action.Navigate(driver, Configuration.Env_Url + "en/index.html");
            action.IClick(driver, Elements.MainNavigation_Company);

            var currentWindow = driver.CurrentWindowHandle;

            Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("events"));
                test.validateString(driver, "event66", GetTags["events"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("prop27"));
                test.validateString(driver, "Company", GetTags["prop27"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar38"));
                test.validateString(driver, "Company", GetTags["eVar38"]);
            });

            driver.Close();
            driver.SwitchTo().Window(currentWindow);
        }

        /******remove due to changes in AL-18380****/
        //[Test, Category("AA"), Category("Core")]
        //[TestCase(TestName = "Verify that the click event call is firing when the My Analog menu tab is clicked")]
        //public void AA_Click_Event_MyAnalog()
        //{
        //    action.Navigate(driver, Configuration.Env_Url + "en/index.html");
        //    action.IClick(driver, Elements.MainNavigation_MyAnalogTab);

        //    var currentWindow = driver.CurrentWindowHandle;

        //    Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

        //    Assert.Multiple(() =>
        //    {
        //        Assert.True(GetTags.ContainsKey("events"));
        //        test.validateString(driver, "event66", GetTags["events"]);
        //    });

        //    Assert.Multiple(() =>
        //    {
        //        Assert.True(GetTags.ContainsKey("prop27"));
        //        test.validateString(driver, "MyAnalog", GetTags["prop27"]);
        //    });

        //    Assert.Multiple(() =>
        //    {
        //        Assert.True(GetTags.ContainsKey("eVar38"));
        //        test.validateString(driver, "MyAnalog", GetTags["eVar38"]);
        //    });

        //    driver.Close();
        //    driver.SwitchTo().Window(currentWindow);
        //}

        [Test, Category("AA"), Category("Core")]
        [TestCase(TestName = "Verify that the click event call is firing when the Products menu tab is clicked")]
        public void AA_Click_Event_Products()
        {
            action.Navigate(driver, Configuration.Env_Url + "en/index.html");
            action.IClick(driver, Elements.MainNavigation_ProductsTab);

            var currentWindow = driver.CurrentWindowHandle;

            Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("events"));
                test.validateString(driver, "event66", GetTags["events"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("prop27"));
                test.validateString(driver, "Products", GetTags["prop27"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar38"));
                test.validateString(driver, "Products", GetTags["eVar38"]);
            });

        }

        [Test, Category("AA"), Category("Core")]
        [TestCase(TestName = "Verify that the click event call is firing when the Applications menu tab is clicked")]
        public void AA_Click_Event_Application()
        {
            action.Navigate(driver, Configuration.Env_Url + "en/index.html");
            action.IClick(driver, Elements.MainNavigation_ApplicationsTab);

            var currentWindow = driver.CurrentWindowHandle;

            Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("events"));
                test.validateString(driver, "event66", GetTags["events"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("prop27"));
                test.validateString(driver, "Applications", GetTags["prop27"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar38"));
                test.validateString(driver, "Applications", GetTags["eVar38"]);
            });

        }

        [Test, Category("AA"), Category("Core")]
        [TestCase(TestName = "Verify that the click event call is firing when the Design Center menu tab is clicked")]
        public void AA_Click_Event_DC()
        {
            action.Navigate(driver, Configuration.Env_Url + "en/index.html");
            action.IClick(driver, Elements.MainNavigation_DesignCenterTab);

            var currentWindow = driver.CurrentWindowHandle;

            Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("events"));
                test.validateString(driver, "event66", GetTags["events"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("prop27"));
                test.validateString(driver, "Design Center", GetTags["prop27"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar38"));
                test.validateString(driver, "Design Center", GetTags["eVar38"]);
            });
        }

        [Test, Category("AA"), Category("Core")]
        [TestCase(TestName = "Verify that the click event call is firing when the Education menu tab is clicked")]
        public void AA_Click_Event_Education()
        {
            action.Navigate(driver, Configuration.Env_Url + "en/index.html");
            action.IClick(driver, Elements.MainNavigation_EducationTab);

            var currentWindow = driver.CurrentWindowHandle;

            Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("events"));
                test.validateString(driver, "event66", GetTags["events"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("prop27"));
                test.validateString(driver, "Education", GetTags["prop27"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar38"));
                test.validateString(driver, "Education", GetTags["eVar38"]);
            });
        }

        [Test, Category("AA"), Category("Core")]
        [TestCase(TestName = "Verify that the click event call is firing when the Support menu tab is clicked")]
        public void AA_Click_Event_Support()
        {
            action.Navigate(driver, Configuration.Env_Url + "en/index.html");
            action.IClick(driver, Elements.MainNavigation_SupportTab);

            var currentWindow = driver.CurrentWindowHandle;

            Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("events"));
                test.validateString(driver, "event66", GetTags["events"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("prop27"));
                test.validateString(driver, "Support", GetTags["prop27"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar38"));
                test.validateString(driver, "Support", GetTags["eVar38"]);
            });
        }

        [Test, Category("AA"), Category("Core")]
        [TestCase(TestName = "Verify that the click event call is firing when a First-level Category on left nav in the PRODUCTS panel is clicked")]
        public void AA_Click_Event_FirstLevelCat()
        {
            action.Navigate(driver, Configuration.Env_Url + "en/index.html");
            action.IClick(driver, Elements.MainNavigation_ProductsTab);
            action.IClick(driver, By.CssSelector("ul[class='categories']>li[class='category']>a"));

            var currentWindow = driver.CurrentWindowHandle;

            Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("events"));
                test.validateString(driver, "event188", GetTags["events"]);
            });

        }

        [Test, Category("AA"), Category("Core")]
        [TestCase(TestName = "Verify that the click event call is firing when the Type Category search textbox is clicked")]
        public void AA_Click_Event_SearchProducts()
        {
            action.Navigate(driver, Configuration.Env_Url + "en/index.html");
            action.IClick(driver, Elements.MainNavigation_ProductsTab);
            action.IClick(driver, Elements.Products_Tab_Search);

            var currentWindow = driver.CurrentWindowHandle;

            Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("events"));
                test.validateString(driver, "event87", GetTags["events"]);
            });

        }

        [Test, Category("AA"), Category("Core")]
        [TestCase(TestName = "Verify that the click event call is firing when cancelling search entry in the PRODUCTS panel")]
        public void AA_Click_Event_CancelSearch()
        {
            action.Navigate(driver, Configuration.Env_Url + "en/index.html");
            action.IClick(driver, Elements.MainNavigation_ProductsTab);
            action.IType(driver, Elements.Products_Tab_Search, "Amplifiers");
            action.IClick(driver, Elements.Products_Tab_Search_Cancel);

            var currentWindow = driver.CurrentWindowHandle;

            Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("events"));
                test.validateString(driver, "event191", GetTags["events"]);
            });

        }

        [Test, Category("AA"), Category("Core")]
        [TestCase(TestName = "Verify that the click event call is firing when the Category search textbox is clicked")]
        public void AA_Click_Event_SearchProducts3()
        {
            action.Navigate(driver, Configuration.Env_Url + "en/index.html");
            action.IClick(driver, Elements.MainNavigation_ProductsTab);
            action.IClick(driver, By.CssSelector("ul[class='categories']>li[class='category']>a"));
            action.IClick(driver, Elements.Products_Tab_SmallSearch);

            var currentWindow = driver.CurrentWindowHandle;

            Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("events"));
                test.validateString(driver, "event92", GetTags["events"]);
            });

        }

    }
}