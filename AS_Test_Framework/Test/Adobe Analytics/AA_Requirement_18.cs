﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Collections.Generic;
using OpenQA.Selenium.Interactions;
using System.Threading;

namespace AS_Test_Framework.Adobe_Analytics
{

    [TestFixture]
    public class AA_Requirement_18: BaseSetUp
    {
        public AA_Requirement_18() : base() { }

        /****Remove due to changes for AL-18380****/
        //[Test, Category("AA"), Category("Core")]
        //[TestCase(TestName = "Verify and track the analytic call in network Where a visitor clicks on \"Register\" Link in myAnalog Tab" )]
        //public void AA_MyAnalogReg1()
        //{
        //    action.Navigate(driver, Configuration.Env_Url + "en/index.html");
        //    action.IClick(driver, Elements.MainNavigation_MyAnalogTab);
        //    IWebElement body = driver.FindElement(Elements.MyAnalogTab_RegisterNow_Link);
        //    Actions OpenLink = new Actions(driver);
        //    OpenLink.KeyDown(Keys.Control).MoveToElement(body).Click().Perform();
            
        //    var currentWindow = driver.CurrentWindowHandle;

        //    Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

        //    Assert.Multiple(() =>
        //    {
        //        Assert.True(GetTags.ContainsKey("events"));
        //        test.validateString(driver, "event107", GetTags["events"]);
        //    });


        //    Assert.Multiple(() =>
        //    {
        //        Assert.True(GetTags.ContainsKey("eVar97"));
        //        test.validateString(driver, "Masthead|Register", GetTags["eVar97"]);
        //    });
        //}

        [Test, Category("AA"), Category("Core")]
        [TestCase(TestName = "Veirfy that the analytics call is firing for the creation of project (via left navigation)")]
        public void AA_MyAnalogReg2()
        {
            action.Navigate(driver, Configuration.Env_Url + "en/index.html");
            action.ILoginViaMyAnalogMegaMenu(driver, "aries.sorosoro@analog.com", "Test_1234");
            action.IClick(driver, Elements.MainNavigation_MyAnalogTab);
            action.IClick(driver, By.CssSelector("a[class='link dashboard']"));
            action.IClick(driver, By.CssSelector("ul[class='nav nav-pills nav-stacked']>li:nth-child(5)>a"));
            action.IClick(driver, By.CssSelector("div[class='create new project']>button"));

            var currentWindow = driver.CurrentWindowHandle;

            Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("events"));
                test.validateString(driver, "event123", GetTags["events"]);
            });
        }

       
    }
}