﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Collections.Generic;
using OpenQA.Selenium.Interactions;

namespace AS_Test_Framework.Adobe_Analytics
{

    [TestFixture]
    public class AA_Requirement_4 : BaseSetUp
    {
        public AA_Requirement_4() : base() { }

        [Test, Category("AA"), Category("Core")]
        [TestCase(TestName = "Verify that the click event call is firing when clicking Product link")]
        public void AA_PDP_Events()
        {
            action.Navigate(driver, Configuration.Env_Url + "en/index.html");
            action.ISearchViaGlobalSearchTextbox(driver, "ADL5569");
            action.IClick(driver, Elements.GlobalSearchResults_BaseballCard_Title_Link);

            var currentWindow = driver.CurrentWindowHandle;

            Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("events"));
                test.validateStringInstance(driver,  GetTags["events"], "event10");
                test.validateStringInstance(driver,  GetTags["events"], "prodView");
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("prop24"));
                test.validateString(driver, "adl5569", GetTags["prop24"]);
            });

        }

        //[Test, Category("AA"), Category("Core")]
        //[TestCase(TestName = "Verify that the click event call is firing when a Market Category is clicked")]
        //public void AA_PDP_MarketCat()
        //{
        //    action.Navigate(driver, Configuration.Env_Url + "en/AD7960");
        //    var currentWindow = driver.CurrentWindowHandle;

        //    string MarketCatName = util.GetText(driver, By.CssSelector("div[class='radioverse col-md-3 col-sm-12']>ul>li[class='accordionGroup']:nth-child(1)>div>a"));

        //    IWebElement body = driver.FindElement(By.CssSelector("div[class='radioverse col-md-3 col-sm-12']>ul>li[class='accordionGroup']:nth-child(1)>div>a"));
        //    Actions OpenLink = new Actions(driver);
        //    OpenLink.KeyDown(Keys.Control).MoveToElement(body).Click().Perform();

        //    Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

        //    Assert.Multiple(() =>
        //    {
        //        Assert.True(GetTags.ContainsKey("events"));
        //        test.validateString(driver, "event78", GetTags["events"]);
        //    });

        //    Assert.Multiple(() =>
        //    {
        //        Assert.True(GetTags.ContainsKey("eVar81"));
        //        test.validateString(driver, MarketCatName, GetTags["eVar81"]);
        //    });

        //    Assert.Multiple(() =>
        //    {
        //        Assert.True(GetTags.ContainsKey("prop38"));
        //        test.validateString(driver, MarketCatName, GetTags["prop38"]);
        //    });

        //}

        [Test, Category("AA"), Category("Core")]
        [TestCase(TestName = "Verify that the click event call is firing when a Sub Market Category is clicked")]
        public void AA_PDP_SubMarketCat()
        {
            action.Navigate(driver, Configuration.Env_Url + "en/AD7960");
            var currentWindow = driver.CurrentWindowHandle;

            action.IClick(driver, By.CssSelector("div[class='radioverse col-md-3 col-sm-12']>ul>li[class='accordionGroup']:nth-child(1)>div>span[class='accordionGroup__toggle__icon']"));

            string SubMarketCatName = util.GetText(driver, By.CssSelector("div[class='radioverse col-md-3 col-sm-12']>ul>li[class='accordionGroup']:nth-child(1) div[class='accordionGroup__body__inner']>ul>li>a"));

            IWebElement body = driver.FindElement(By.CssSelector("div[class='radioverse col-md-3 col-sm-12']>ul>li[class='accordionGroup']:nth-child(1) div[class='accordionGroup__body__inner']>ul>li>a"));
            Actions OpenLink = new Actions(driver);
            OpenLink.KeyDown(Keys.Control).MoveToElement(body).Click().Perform();

            Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("events"));
                test.validateString(driver, "event79", GetTags["events"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar82"));
                test.validateString(driver, SubMarketCatName, GetTags["eVar82"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("prop39"));
                test.validateString(driver, SubMarketCatName, GetTags["prop39"]);
            });

        }

        [Test, Category("AA"), Category("Core")]
        [TestCase(TestName = "Verify that the click event call is firing when a menu in the sticky nav is clicked")]
        public void AA_PDP_StickyNav()
        {
            action.Navigate(driver, Configuration.Env_Url + "en/AD7960");
            string prop45value = util.ReturnAttribute(driver, By.CssSelector("a[href='#product-samplebuy']"), "href").Split('#')[1];
            action.IClick(driver, By.CssSelector("a[href='#product-samplebuy']"));

            var currentWindow = driver.CurrentWindowHandle;

            Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("events"));
                test.validateString(driver, "event88", GetTags["events"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("prop45"));
                test.validateString(driver, prop45value, GetTags["prop45"]);
            });
        }
        
        //[Test, Category("AA"), Category("Core")]
        //[TestCase(TestName = "Verify that the click event call is firing when the Buy button uder Distributors is clicked")]
        //public void AA_PDP_Distributor()
        //{
        //    action.Navigate(driver, Configuration.Env_Url + "en/ADL5569");
        //    var currentWindow = driver.CurrentWindowHandle;

        //    action.IClick(driver, By.CssSelector("a[href='#product-samplebuy']"));
        //    action.IClick(driver, Elements.PDP_SampleAndBuy_Tbl_SelectACountry_Dd_Btn);
        //    action.IClick(driver, By.CssSelector("div[class='search-with-filter pcnpdn-country-filter'] ul>li>a[data-id='US']"));
        //    action.IClick(driver, Elements.PDP_SampleAndBuy_Tbl_CheckInventory_Btn);

        //    //for (int x = 11; util.CheckElement(driver, By.CssSelector("div[class='products-packaging-grid sb-grid-model table-responsive grid-clone view-inventory'] tr>td[class='distributor hidden-panel']:nth-of-type(" + x + ")"),1); x++)
        //    //{
        //    //    if(util.ReturnAttribute(driver, By.CssSelector("div[class='products-packaging-grid sb-grid-model table-responsive grid-clone view-inventory'] tr>td[class='distributor hidden-panel']:nth-of-type(" + x + ") a"), "href").Contains("digikey"))
        //    //    {
        //    //        action.IClick(driver, By.CssSelector("div[class='products-packaging-grid sb-grid-model table-responsive grid-clone view-inventory'] tr>td[class='distributor hidden-panel']:nth-of-type(" + x + ") a"));
        //    //        break;
        //    //    }
        //    //}
        //    action.IClick(driver, By.CssSelector("div[class='products-packaging-grid sb-grid-model table-responsive grid-clone view-inventory'] tr>td[class='distributor hidden-panel'] a"));

        //    Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

        //    Assert.Multiple(() =>
        //    {
        //        Assert.True(GetTags.ContainsKey("events"));
        //        test.validateString(driver, "event11", GetTags["events"]);
        //    });

        //    Assert.True(GetTags.ContainsKey("eVar60"));
        //}

        [Test, Category("AA"), Category("Core")]
        [TestCase(TestName = "Verify analytics is firing upon clicking purchase button in Sample and buy")]
        public void AA_PDP_PurchaseBtn()
        {
            action.Navigate(driver, Configuration.Env_Url + "en/ADL5569");
            action.IClick(driver, By.CssSelector("a[href='#product-samplebuy']"));
            action.IClick(driver, By.CssSelector("a[class='btn purchase']"));

            var currentWindow = driver.CurrentWindowHandle;

            Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("events"));
                test.validateString(driver, "event88", GetTags["events"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("prop45"));
                test.validateString(driver, "purchase", GetTags["prop45"]);
            });


            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("Request Type"));
                test.validateString(driver, "lnk_o", GetTags["Request Type"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("Friendly Name (non-page)"));
                test.validateString(driver, "product purchase click", GetTags["Friendly Name (non-page)"]);
            });
        }
    }
}