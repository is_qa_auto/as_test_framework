﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Collections.Generic;
using OpenQA.Selenium.Interactions;
using System.Threading;
using System;
using System.Globalization;

namespace AS_Test_Framework.Adobe_Analytics
{

    [TestFixture]
    public class AA_Requirement_5 : BaseSetUp
    {
        public AA_Requirement_5() : base() { }

        [Test, Category("AA"), Category("Core")]
        [TestCase(TestName = "Verify that the click event call is firing when the Maximize Filters button is clicked")]
        public void AA_Pst_Events()
        {
            action.Navigate(driver, Configuration.Env_Url + "en/parametricsearch/11470");
            string PST_State = util.GetText(driver, By.CssSelector("menu[class='tools']>div:nth-child(2)>button:nth-child(2)")).Split(' ')[0];
            action.IClick(driver, By.CssSelector("menu[class='tools']>div:nth-child(2)>button:nth-child(2)"));

            var currentWindow = driver.CurrentWindowHandle;

            Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("prop8"));
                test.validateString(driver, "pst", GetTags["prop8"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("prop26"));
                test.validateString(driver, "PST Filter Display", GetTags["prop26"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar20"));
                test.validateString(driver, "pst", GetTags["eVar20"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar71"));
                test.validateString(driver, "11470", GetTags["eVar71"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar73"));
                test.validateString(driver, PST_State.ToLower(), GetTags["eVar73"]);
            });

            /*Test For Minimize
            driver.Close();
            driver.SwitchTo().Window(currentWindow);
            
            PST_State = util.GetText(driver, By.CssSelector("menu[class='tools']>div:nth-child(2)>button:nth-child(2)")).Split(' ')[0];
            action.IClick(driver, By.CssSelector("menu[class='tools']>div:nth-child(2)>button:nth-child(2)"));

            GetTags = util.GetAdobeTags(driver);
            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar73"));
                test.validateString(driver, PST_State.ToLower(), GetTags["eVar73"]);
            });*/
        }

        [Test, Category("AA"), Category("Core")]
        [TestCase(TestName = "Verify that the click event call is firing when Sort by Newest button is clicked")]
        public void AA_Pst_Events2()
        {
            action.Navigate(driver, Configuration.Env_Url + "en/parametricsearch/11470");
            action.IClick(driver, By.CssSelector("menu[class='tools']>div:nth-child(2)>button:nth-child(3)"));
            action.IClick(driver, By.CssSelector("menu[class='tools']>div:nth-child(2)>button:nth-child(3)"));

            var currentWindow = driver.CurrentWindowHandle;

            Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("prop8"));
                test.validateString(driver, "pst", GetTags["prop8"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("prop26"));
                test.validateString(driver, "Sort by Newest", GetTags["prop26"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar20"));
                test.validateString(driver, "pst", GetTags["eVar20"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar71"));
                test.validateString(driver, "11470", GetTags["eVar71"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar73"));
                test.validateString(driver, "Previously Chronological", GetTags["eVar73"]);
            });

            driver.Close();
            driver.SwitchTo().Window(currentWindow);

            action.IClick(driver, By.CssSelector("section[class='parts'] div[class='sortable title']>div[class='parameter name']"));
            action.IClick(driver, By.CssSelector("menu[class='tools']>div:nth-child(2)>button:nth-child(3)"));

            GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("prop8"));
                test.validateString(driver, "pst", GetTags["prop8"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("prop26"));
                test.validateString(driver, "Sort by Newest", GetTags["prop26"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar20"));
                test.validateString(driver, "pst", GetTags["eVar20"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar71"));
                test.validateString(driver, "11470", GetTags["eVar71"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar73"));
                test.validateString(driver, "Previously Alphabetical", GetTags["eVar73"]);
            });


        }

        [Test, Category("AA"), Category("Core")]
        [TestCase(TestName = "Verify that the click event call is firing when cancel button in Choose Parameters Dialog is clicked")]
        public void AA_Pst_Events3()
        {
            action.Navigate(driver, Configuration.Env_Url + "en/parametricsearch/11470");
            action.IClick(driver, By.CssSelector("menu[class='tools']>div:nth-child(1)>button:nth-child(1)"));
            action.IClick(driver, By.CssSelector("section[class='choose parameters']>div>div[class='footer']>button:nth-child(2)"));

            var currentWindow = driver.CurrentWindowHandle;

            Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("prop8"));
                test.validateString(driver, "pst", GetTags["prop8"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("prop26"));
                test.validateString(driver, "Choose Parameters", GetTags["prop26"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar20"));
                test.validateString(driver, "pst", GetTags["eVar20"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar71"));
                test.validateString(driver, "11470", GetTags["eVar71"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar73"));
                test.validateString(driver, "No Change", GetTags["eVar73"]);
            });
        }

        [Test, Category("AA"), Category("Core")]
        [TestCase(TestName = "Verify that the click event call is firing when Select All button in Choose Parameters Dialog is clicked")]
        public void AA_Pst_Events4()
        {
            action.Navigate(driver, Configuration.Env_Url + "en/parametricsearch/11470");
            action.IClick(driver, By.CssSelector("menu[class='tools']>div:nth-child(1)>button:nth-child(1)"));
            action.IClick(driver, By.CssSelector("section[class='choose parameters']>div>div[class='content options']>button:nth-child(1)"));
            action.IClick(driver, By.CssSelector("section[class='choose parameters']>div>div[class='footer']>button:nth-child(1)"));

            var currentWindow = driver.CurrentWindowHandle;

            Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("prop8"));
                test.validateString(driver, "pst", GetTags["prop8"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("prop26"));
                test.validateString(driver, "Choose Parameters", GetTags["prop26"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar20"));
                test.validateString(driver, "pst", GetTags["eVar20"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar73"));
                test.validateString(driver, "All", GetTags["eVar73"]);
            });
        }

        [Test, Category("AA"), Category("Core")]
        [TestCase(TestName = "Verify that the click event call is firing when Default button in Choose Parameters Dialog is clicked")]
        public void AA_Pst_Events5()
        {
            action.Navigate(driver, Configuration.Env_Url + "en/parametricsearch/11470");
            action.IClick(driver, By.CssSelector("menu[class='tools']>div:nth-child(1)>button:nth-child(1)"));
            action.IClick(driver, By.CssSelector("section[class='choose parameters']>div>div[class='content options']>button:nth-child(3)"));
            action.IClick(driver, By.CssSelector("section[class='choose parameters']>div>div[class='footer']>button:nth-child(1)"));

            var currentWindow = driver.CurrentWindowHandle;

            Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("prop8"));
                test.validateString(driver, "pst", GetTags["prop8"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("prop26"));
                test.validateString(driver, "Choose Parameters", GetTags["prop26"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar20"));
                test.validateString(driver, "pst", GetTags["eVar20"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar71"));
                test.validateString(driver, "11470", GetTags["eVar71"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar73"));
                test.validateString(driver, "Default", GetTags["eVar73"]);
            });
        }

        [Test, Category("AA"), Category("Core")]
        [TestCase(TestName = "Verify that the click event call is firing when selected filter parameters in Choose Parameters Dialog is clicked")]
        public void AA_Pst_Events6()
        {
            action.Navigate(driver, Configuration.Env_Url + "en/parametricsearch/11470");
            action.IClick(driver, By.CssSelector("menu[class='tools']>div:nth-child(1)>button:nth-child(1)"));

            action.IClick(driver, By.CssSelector("section[class='choose parameters']>div>div[class='content parameters']>div:nth-of-type(1)>input"));
            action.IClick(driver, By.CssSelector("section[class='choose parameters']>div>div[class='footer']>button:nth-child(1)"));


            var currentWindow = driver.CurrentWindowHandle;

            Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("prop8"));
                test.validateString(driver, "pst", GetTags["prop8"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("prop26"));
                test.validateString(driver, "PST Filters", GetTags["prop26"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar20"));
                test.validateString(driver, "pst", GetTags["eVar20"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar71"));
                test.validateString(driver, "11470", GetTags["eVar71"]);
            });
        }

        [Test, Category("AA"), Category("Core")]
        [TestCase(TestName = "Verify that the click event call is firing when the Reset button is clicked")]
        public void AA_Pst_Events7()
        {
            action.Navigate(driver, Configuration.Env_Url + "en/parametricsearch/11470");
            action.IClick(driver, By.CssSelector("menu[class='tools']>div:nth-child(2)>button:nth-child(1)"));

            var currentWindow = driver.CurrentWindowHandle;

            Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("prop8"));
                test.validateString(driver, "pst", GetTags["prop8"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("prop26"));
                test.validateString(driver, "Reset Table", GetTags["prop26"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar20"));
                test.validateString(driver, "pst", GetTags["eVar20"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar71"));
                test.validateString(driver, "11470", GetTags["eVar71"]);
            });
        }

        [Test, Category("AA"), Category("Core")]
        [TestCase(TestName = "Verify that the click event call is firing when the Download to Excel button is clicked")]
        public void AA_Pst_Events8()
        {
            action.Navigate(driver, Configuration.Env_Url + "en/parametricsearch/11470");
            action.IClick(driver, By.CssSelector("menu[class='tools']>div:nth-child(3)>button:nth-child(3)"));

            var currentWindow = driver.CurrentWindowHandle;

            Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("events"));
                test.validateStringInstance(driver, GetTags["events"], "event49");
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("prop8"));
                test.validateString(driver, "pst", GetTags["prop8"]);
            });


            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("prop11"));
                test.validateString(driver, "pst: download to excel", GetTags["prop11"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("prop26"));
                test.validateString(driver, "Download to Excel", GetTags["prop26"]);
            });
            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar8"));
                test.validateString(driver, "D=c11", GetTags["eVar8"]);
            });
            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar20"));
                test.validateString(driver, "D=c8", GetTags["eVar20"]);
            });
            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar71"));
                test.validateString(driver, "11470", GetTags["eVar71"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar73"));
                test.validateString(driver, "OK", GetTags["eVar73"]);
            });
        }

        [Test, Category("AA"), Category("Core")]
        [TestCase(TestName = "Verify that the click event call is firing when sorting a column")]
        public void AA_Pst_Events9()
        {
            action.Navigate(driver, Configuration.Env_Url + "en/parametricsearch/11470");
            action.IClick(driver, By.CssSelector("section[class='parts'] * div[class='parameter name']"));

            var currentWindow = driver.CurrentWindowHandle;

            Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("prop8"));
                test.validateString(driver, "pst", GetTags["prop8"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("prop26"));
                test.validateString(driver, "Sort", GetTags["prop26"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar20"));
                test.validateString(driver, "pst", GetTags["eVar20"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar71"));
                test.validateString(driver, "11470", GetTags["eVar71"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar73"));
                test.validateString(driver, "0: ascending", GetTags["eVar73"]);
            });

            driver.Close();
            driver.SwitchTo().Window(currentWindow);

            action.IClick(driver, By.CssSelector("section[class='parts'] * div[class='parameter name']"));

            GetTags = util.GetAdobeTags(driver);
            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar73"));
                test.validateString(driver, "0: descending", GetTags["eVar73"]);
            });
        }

        [Test, Category("AA"), Category("Core")]
        [TestCase(TestName = "Verify that the click event call is firing when the Share button is clicked")]
        public void AA_Pst_Events10()
        {
            action.Navigate(driver, Configuration.Env_Url + "en/parametricsearch/11470");
            string Product_No = util.GetText(driver, By.CssSelector("section[class='parts']>article>div[class='product']>a"));
            action.IType(driver, By.CssSelector("section[class='parameter part filter minimized'] * input"), Product_No);
            action.IClick(driver, By.CssSelector("menu[class='tools']>div:nth-child(3)>button:nth-child(4)"));

            var currentWindow = driver.CurrentWindowHandle;

            Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("events"));
                test.validateString(driver, "event47", GetTags["events"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("prop8"));
                test.validateString(driver, "ADC Drivers", GetTags["prop8"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("prop11"));
                test.validateString(driver, "pst: share", GetTags["prop11"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar8"));
                test.validateString(driver, "D=c11", GetTags["eVar8"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar20"));
                test.validateString(driver, "D=c8", GetTags["eVar20"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar71"));
                test.validateString(driver, "11470", GetTags["eVar71"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar67"));
                test.validateString(driver, "#/p0=" + Product_No, GetTags["eVar67"]);
            });
        }

        [Test, Category("AA"), Category("Core")]
        [TestCase(TestName = "Verify that the click event call is firing when a Part Number is clicked under the Part Number column")]
        public void AA_Pst_Events11()
        {
            action.Navigate(driver, Configuration.Env_Url + "en/parametricsearch/11470");
            string Product_No = util.GetText(driver, By.CssSelector("section[class='parts']>article>div[class='product']>a"));
            action.IType(driver, By.CssSelector("section[class='parameter part filter minimized'] * input"), Product_No);
            action.IClick(driver, By.CssSelector("section[class='parts']>article>div[class='product']>a"));

            var currentWindow = driver.CurrentWindowHandle;

            Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("events"));
                test.validateString(driver, "event50", GetTags["events"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("prop8"));
                test.validateString(driver, "ADC Drivers", GetTags["prop8"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("prop11"));
                test.validateString(driver, "pst: product view", GetTags["prop11"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar8"));
                test.validateString(driver, "D=c11", GetTags["eVar8"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar20"));
                test.validateString(driver, "D=c8", GetTags["eVar20"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar71"));
                test.validateString(driver, "11470", GetTags["eVar71"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar67"));
                test.validateString(driver, "#/p0=" + Product_No, GetTags["eVar67"]);
            });
        }

        [Test, Category("AA"), Category("Core")]
        [TestCase(TestName = "Verify General Analytics call when accessing PST")]
        public void AA_Pst_Events12()
        {
           // Console.WriteLine("year=" + DateTime.Now.Year.ToString() + " | month=" + DateTime.Today.ToString("MMMM") + " | date=" + DateTime.Now.Day.ToString() + " | day=" + DateTime.Now.DayOfWeek.ToString() + " | time=" + TimeZoneInfo.ConvertTime(, TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time")).ToString("h:mm tt"));
            action.Navigate(driver, Configuration.Env_Url + "en/index.html");
            action.IClick(driver, Elements.MainNavigation_ProductsTab);
            action.IClick(driver, By.CssSelector("ul[class='categories']>li[class='category']>a"));
            Thread.Sleep(3000);
            action.IClick(driver, By.CssSelector("div[class='main'] div[class*='results'] a[class='pst']"));
            DateTime TimeToday = DateTime.Now;
            string WindowTitle = driver.Title.ToLower();
            Thread.Sleep(3000);
            string PST_Url = driver.Url.Replace("https://", "").Replace("#/", "");
            var currentWindow = driver.CurrentWindowHandle;

            Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("prop1"));
                test.validateString(driver, "parametricsearch", GetTags["prop1"]);
            });
            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar1"));
                test.validateString(driver, PST_Url, GetTags["eVar1"]);
            });
            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("hier1"));
                test.validateString(driver, PST_Url.Replace("en/", "").Replace("/", "|") + "|", GetTags["hier1"]);
            });
            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("prop2"));
                test.validateString(driver, "new", GetTags["prop2"]);
            });
            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar2"));
                test.validateString(driver, "D=c2", GetTags["eVar2"]);
            });
            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar3"));
                test.validateString(driver, "D=c3", GetTags["eVar3"]);
            });
            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("prop4"));
                test.validateString(driver, "1", GetTags["prop4"]);
            });
            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar4"));
                test.validateString(driver, "D=c4", GetTags["eVar4"]);
            });
            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar5"));
                test.validateString(driver, "D=c5", GetTags["eVar5"]);
            });
            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar6"));
                test.validateString(driver, "D=c6", GetTags["eVar6"]);
            });
            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("prop7"));
                test.validateString(driver, "year=" + DateTime.Now.Year.ToString() + " | month=" + DateTime.Today.ToString("MMMM") + " | date=" + DateTime.Now.Day.ToString() + " | day=" + DateTime.Now.DayOfWeek.ToString() + " | time=" + TimeZoneInfo.ConvertTime(TimeToday, TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time")).ToString("h:mm tt"), GetTags["prop7"]);
            });
            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar7"));
                test.validateString(driver, "D=c7", GetTags["eVar7"]);
            });
            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("prop8"));
                test.validateString(driver, "pst", GetTags["prop8"]);
            });
            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar9"));
                test.validateString(driver, "non-member", GetTags["eVar9"]);
            });
            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("prop10"));
                test.validateString(driver, Configuration.Env_Url.Replace("https://","") + "en/index.html", GetTags["prop10"]);
            });
            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar10"));
                test.validateString(driver, "logged out", GetTags["eVar10"]);
            });
            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("prop13"));
                test.validateString(driver, "D=ch+'|'+v51", GetTags["prop13"]);
            });
            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar13"));
                test.validateString(driver, "D=c10", GetTags["eVar13"]);
            });
            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("prop18"));
                test.validateString(driver, "D=ch", GetTags["prop18"]);
            });
            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("prop19"));
                test.validateString(driver, "D=c1", GetTags["prop19"]);
            });
            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("prop20"));
                test.validateString(driver, PST_Url.Substring(PST_Url.Length - 5), GetTags["prop20"]);
            });
            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar20"));
                test.validateString(driver, "pst", GetTags["eVar20"]);
            });
            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("prop22"));
                test.validateString(driver, "en", GetTags["prop22"]);
            });
            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar22"));
                test.validateString(driver, "D=c25", GetTags["eVar22"]);
            });
            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar24"));
                test.validateString(driver, "D=c22", GetTags["eVar24"]);
            });
            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("prop25"));
                test.validateString(driver, WindowTitle, GetTags["prop25"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("prop26"));
                test.validateString(driver, "Enter", GetTags["prop26"]);
            });
            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar39"));
                test.validateString(driver, "D=s_vi", GetTags["eVar39"]);
            });
            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar51"));
                test.validateString(driver, "en", GetTags["eVar51"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar71"));
                test.validateString(driver, PST_Url.Substring(PST_Url.Length - 5), GetTags["eVar71"]);
            });
            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar73"));
                test.validateString(driver, PST_Url.Substring(PST_Url.Length - 5), GetTags["eVar73"]);
            });
        }
    }
}