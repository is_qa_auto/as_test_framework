﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Collections.Generic;
using OpenQA.Selenium.Interactions;
using System.Threading;
using System.Linq;

namespace AS_Test_Framework.Adobe_Analytics
{

    [TestFixture]
    public class AA_Requirement_48: BaseSetUp
    {
        public AA_Requirement_48() : base() { }

        [Test, Category("AA"), Category("Core")]
        [TestCase(TestName = "Track email verification on myAnalog registration")]
        public void AA_MW1()
        {
            string Email_Add = util.Generate_EmailAddress();
            action.Navigate(driver, Configuration.Env_Url.Replace("www","my").Replace("cldnet", "corpnt") + "en/app/registration");
            action.IClick(driver, Elements.Registration_Skip_Btn);
            action.IType(driver, Elements.Registration_VerifyEmail_TxtField, Email_Add);
            action.IClick(driver, Elements.Registration_SendVerificationCode_Btn);

            var currentWindow = driver.CurrentWindowHandle;

            Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("events"));
                test.validateStringInstance(driver, GetTags["events"], "event173");
            });

            driver.Close();
            driver.SwitchTo().Window(currentWindow);

            action.IOpenNewTab(driver);
            action.ICheckMailinatorEmail(driver, Email_Add);
            action.IClick(driver, By.CssSelector("table[class='table-striped jambo_table'] * tr>td:nth-child(2)"));
            driver.SwitchTo().Frame(driver.FindElement(By.CssSelector("iframe[id='html_msg_body']")));
            string VerificationCode = util.GetText(driver, By.CssSelector("td>strong")).Substring(util.GetText(driver, By.CssSelector("td>strong")).Length - 6).Trim();
            driver.SwitchTo().Window(driver.WindowHandles.First());
            action.IType(driver, Elements.Registration_EnterCode_TextField, VerificationCode);
            action.IClick(driver, Elements.Registration_SendVerificationCode_Btn);
            Thread.Sleep(3000);

            GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("events"));
                test.validateStringInstance(driver, GetTags["events"], "event174");
            });
        }
    }
}