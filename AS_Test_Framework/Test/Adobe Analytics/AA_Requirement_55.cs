﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Collections.Generic;
using OpenQA.Selenium.Interactions;
using System.Threading;

namespace AS_Test_Framework.Adobe_Analytics
{

    [TestFixture]
    public class AA_Requirement_55 : BaseSetUp
    {
        public AA_Requirement_55() : base() { }

        [Test, Category("AA"), Category("Core")]
        [TestCase(TestName = "Verify that event158 is firing on the myAnalog Parametric Search  page")]
        public void AA_Pst_Events()
        {
            action.Navigate(driver, Configuration.Env_Url + "en/index.html");
            action.ILoginViaMyAnalogMegaMenu(driver, "aries.sorosoro@analog.com", "Test_1234");
            driver.Navigate().GoToUrl(Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + "en/app/parametric-searches");
            action.IClick(driver, By.CssSelector("section[class='parametric searches'] table tr>td>span>a"));

            var currentWindow = driver.CurrentWindowHandle;

            Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("events"));
                test.validateStringInstance(driver, GetTags["events"], "event158");
            });
        }
        [Test, Category("AA"), Category("Core")]
        [TestCase(TestName = "Verify that event158 is firing on the myAnalog Parametric Search page and clicking remove button")]
        public void AA_Pst_Events2()
        {
            action.Navigate(driver, Configuration.Env_Url + "en/index.html");
            action.ILoginViaMyAnalogMegaMenu(driver, "aries.sorosoro@analog.com", "Test_1234");
            driver.Navigate().GoToUrl(Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + "en/app/parametric-searches");
            action.IClick(driver, By.CssSelector("section[class='parametric searches'] table tr>td[class='controls no data'] * button"));
            action.IClick(driver, By.CssSelector("section[class='parametric searches'] table tr>td[class='controls no data']>menu>button:nth-child(4)>span"));
            var currentWindow = driver.CurrentWindowHandle;

            Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("events"));
                test.validateStringInstance(driver, GetTags["events"], "event158");
            });
        }
    }
}