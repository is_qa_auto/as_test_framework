﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using OpenQA.Selenium.Interactions;

namespace AS_Test_Framework.Adobe_Analytics
{

    [TestFixture]
    public class AA_Requirement_39 : BaseSetUp
    {
        public AA_Requirement_39() : base() { }

        [Test, Category("AA"), Category("Core")]
        [TestCase(TestName = "Verify that the click event call is firing when the card in the New Designs section is clicked")]
        public void AA_ReferenceDesign()
        {
            action.Navigate(driver, Configuration.Env_Url + "en/design-center/reference-designs.html");

            string NewDesign_ItemName = util.GetText(driver, By.CssSelector("div[class='reference-description']>h2"));
            action.IClick(driver, Elements.DesignCenter_ReferenceDesigns_NewDesigns_Sec_MostRecentlyLaunched_Item);
            
            var currentWindow = driver.CurrentWindowHandle;

            Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("events"));
                test.validateString(driver, "event192", GetTags["events"]);
            });

            //Assert.Multiple(() =>
            //{
            //    Assert.True(GetTags.ContainsKey("eVar112"));
            //    test.validateString(driver, "design|" + NewDesign_ItemName, GetTags["eVar112"]);
            //});
        }

        [Test, Category("AA"), Category("Core")]
        [TestCase(TestName = "Verify that the click event call is firing when the Reference Design name beside the card in the New Designs section is clicked")]
        public void AA_ReferenceDesign2()
        {
            action.Navigate(driver, Configuration.Env_Url + "en/design-center/reference-designs.html");

            string NewDesign_ItemName = util.GetText(driver, Elements.DesignCenter_ReferenceDesigns_NewDesigns_Sec_Item_ReferenceDesign_Name_Link);
            action.IClick(driver, Elements.DesignCenter_ReferenceDesigns_NewDesigns_Sec_Item_ReferenceDesign_Name_Link);

            var currentWindow = driver.CurrentWindowHandle;

            Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("events"));
                test.validateString(driver, "event192", GetTags["events"]);
            });

            //Assert.Multiple(() =>
            //{
            //    Assert.True(GetTags.ContainsKey("eVar112"));
            //    test.validateString(driver, "part|" + NewDesign_ItemName, GetTags["eVar112"]);
            //});
        }

        [Test, Category("AA"), Category("Core")]
        [TestCase(TestName = "Verify that the click event call is firing when the VIEW ALL NEW REFERENCE DESIGNS button in the New Designs section is clicked")]
        public void AA_ReferenceDesign3()
        {
            action.Navigate(driver, Configuration.Env_Url + "en/design-center/reference-designs.html");

            IWebElement body = driver.FindElement(Elements.DesignCenter_ReferenceDesigns_NewDesigns_ViewAllNewReferenceDesigns_Btn);
            Actions OpenLink = new Actions(driver);
            OpenLink.KeyDown(Keys.Control).MoveToElement(body).Click().Perform();

            var currentWindow = driver.CurrentWindowHandle;

            Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("events"));
                test.validateString(driver, "event192", GetTags["events"]);
            });

            //Assert.Multiple(() =>
            //{
            //    Assert.True(GetTags.ContainsKey("eVar112"));
            //    test.validateString(driver, "view all new reference designs", GetTags["eVar112"]);
            //});
        }

        [Test, Category("AA"), Category("Core")]
        [TestCase(TestName = "Verify that the click event call is firing when the Circuit Note hyperlink in the search results is clicked")]
        public void AA_ReferenceDesign4()
        {
            action.Navigate(driver, Configuration.Env_Url + "en/design-center/reference-designs.html");

            action.IClick(driver, Elements.DesignCenter_ReferenceDesigns_NewDesigns_ViewAllNewReferenceDesigns_Btn);
            string CN_Name = util.GetText(driver, By.CssSelector("div[class='search-results-item search-results-item-evaluation search-results-item-product reference-designs-results']>div[class='search-results-item panel panel-default']:nth-of-type(2) div[class='content-title clearfix']>div[class='header3']>a"));
            action.IClick(driver, By.CssSelector("div[class='search-results-item search-results-item-evaluation search-results-item-product reference-designs-results']>div[class='search-results-item panel panel-default']:nth-of-type(2) div[class='content-title clearfix']>div[class='pull-right']>a"));

            var currentWindow = driver.CurrentWindowHandle;

            Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("events"));
                test.validateString(driver, "event1", GetTags["events"]);
            });
            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar1"));
                test.validateString(driver, Configuration.Env_Url.Replace("https://","") + "en/design-center/search.html", GetTags["eVar1"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar8"));
                test.validateString(driver, "D=c11", GetTags["eVar8"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar32"));
                test.validateString(driver, "pdf", GetTags["eVar32"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("prop11"));
                test.validateString(driver, Configuration.Env_Url + "media/en/reference-design-documentation/reference-designs/" + CN_Name + ".pdf", GetTags["prop11"]);
            });
        }
        [Test, Category("AA"), Category("Core")]
        [TestCase(TestName = "Verify that the click event call is firing when a search result card to expand or collapse design details is clicked")]
        public void AA_ReferenceDesign5()
        {
            action.Navigate(driver, Configuration.Env_Url + "en/design-center/reference-designs.html");
            action.IClick(driver, Elements.DesignCenter_ReferenceDesigns_NewDesigns_ViewAllNewReferenceDesigns_Btn);
            string Cn_title = util.GetText(driver, Elements.DesignCenter_Search_SearchResults_Items_ReferenceDesign_Name_Link);
            action.IClick(driver, By.CssSelector("div[class='description']"));

            var currentWindow = driver.CurrentWindowHandle;

            Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("events"));
                test.validateString(driver, "event195", GetTags["events"]);
            });

            //Assert.Multiple(() =>
            //{
            //    Assert.True(GetTags.ContainsKey("eVar112"));
            //    test.validateString(driver, "search results expanded|" + Cn_title, GetTags["eVar112"]);
            //});

            driver.Close();
            driver.SwitchTo().Window(currentWindow);
            action.IClick(driver, By.CssSelector("div[class='description']"));
            GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("events"));
                test.validateString(driver, "event198", GetTags["events"]);
            });

            //Assert.Multiple(() =>
            //{
            //    Assert.True(GetTags.ContainsKey("eVar112"));
            //    //test.validateString(driver, "search results collapsed|" + Cn_title, GetTags["eVar112"]);
            //});
        }

        [Test, Category("AA"), Category("Core")]
        [TestCase(TestName = "Verify that the click event call is firing when an image on expanded card in the search results is clicked")]
        public void AA_ReferenceDesign6()
        {
            action.Navigate(driver, Configuration.Env_Url + "en/design-center/reference-designs.html");
            action.IClick(driver, Elements.DesignCenter_ReferenceDesigns_NewDesigns_ViewAllNewReferenceDesigns_Btn);
            action.IClick(driver, By.CssSelector("div[class='description']"));
            string ImageName = util.ReturnAttribute(driver, By.CssSelector("div[class='full-description row panel-body']>div[class='col-md-4']>ul[class='list-inline tabs clearfix']>li:nth-child(1)"), "alt");
            action.IClick(driver, By.CssSelector("div[class='full-description row panel-body']>div[class='col-md-4']>ul[class='list-inline tabs clearfix']>li:nth-child(1)"));

            var currentWindow = driver.CurrentWindowHandle;

            Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("events"));
                test.validateString(driver, "event196", GetTags["events"]);
            });

            //Assert.Multiple(() =>
            //{
            //    Assert.True(GetTags.ContainsKey("eVar112"));
            //    test.validateString(driver, "expanded image clicked|" + ImageName, GetTags["eVar112"]);
            //});

        }
        [Test, Category("AA"), Category("Core")]
        [TestCase(TestName = "Verify that the click event call is firing when a Parts Used in the search results is clicked")]
        public void AA_ReferenceDesign7()
        {
            action.Navigate(driver, Configuration.Env_Url + "en/design-center/reference-designs.html");
            action.IClick(driver, Elements.DesignCenter_ReferenceDesigns_NewDesigns_ViewAllNewReferenceDesigns_Btn);
            action.IClick(driver, By.CssSelector("div[class='description']"));
            string PartUsed_Link = util.GetText(driver, Elements.DesignCenter_Search_SearchResults_Items_PartsUsed_Links);
            action.IClick(driver, Elements.DesignCenter_Search_SearchResults_Items_PartsUsed_Links);

            var currentWindow = driver.CurrentWindowHandle;

            Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("events"));
                test.validateString(driver, "event197", GetTags["events"]);
            });

            //Assert.Multiple(() =>
            //{
            //    Assert.True(GetTags.ContainsKey("eVar112"));
            //    test.validateString(driver, "parts used|" + PartUsed_Link.ToLower(), GetTags["eVar112"]);
            //});
        }

        [Test, Category("AA"), Category("Core")]
        [TestCase(TestName = "Verify that the click event call is firing when a Markets and Technology in the Reference Designs page is clicked")]
        public void AA_ReferenceDesign8()
        {
            action.Navigate(driver, Configuration.Env_Url + "en/design-center/reference-designs/circuits-from-the-lab/CN0234.html");
            string ProdCat = util.GetText(driver, Elements.CFTL_Overview_MarketsAndTechnology_Links);
            IWebElement body = driver.FindElement(Elements.CFTL_Overview_MarketsAndTechnology_Links);
            Actions OpenLink = new Actions(driver);
            OpenLink.KeyDown(Keys.Control).MoveToElement(body).Click().Perform();
            var currentWindow = driver.CurrentWindowHandle;

            string Url = driver.Url.Replace("#", "").Replace("/", "");
            Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("events"));
                test.validateString(driver, "event204", GetTags["events"]);
            });

            //Assert.Multiple(() =>
            //{
            //    Assert.True(GetTags.ContainsKey("eVar114"));
            //    test.validateString(driver, "Markets and Technologies|" + ProdCat, GetTags["eVar114"]);
            //});
        }
        [Test, Category("AA"), Category("Core")]
        [TestCase(TestName = "Verify that the click event call is firing when a Parts Used in the Reference Designs page is clicked")]
        public void AA_ReferenceDesign9()
        {
            action.Navigate(driver, Configuration.Env_Url + "en/design-center/reference-designs/circuits-from-the-lab/CN0234.html");
            string Parts_Used = util.GetText(driver, Elements.CFTL_Overview_Displayed_PartsUsed_Links);
            IWebElement body = driver.FindElement(Elements.CFTL_Overview_Displayed_PartsUsed_Links);
            Actions OpenLink = new Actions(driver);
            OpenLink.KeyDown(Keys.Control).MoveToElement(body).Click().Perform();
            var currentWindow = driver.CurrentWindowHandle;

            string Url = driver.Url.Replace("#", "").Replace("/", "");
            Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("events"));
                test.validateString(driver, "event204", GetTags["events"]);
            });
            //Assert.Multiple(() =>
            //{
            //    Assert.True(GetTags.ContainsKey("eVar114"));
            //    test.validateString(driver, "parts used|" + Parts_Used.ToLower(), GetTags["eVar114"]);
            //});
        }
    }
}