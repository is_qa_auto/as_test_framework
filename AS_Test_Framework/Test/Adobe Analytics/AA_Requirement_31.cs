﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Collections.Generic;
using OpenQA.Selenium.Interactions;
using System.Threading;

namespace AS_Test_Framework.Adobe_Analytics
{

    [TestFixture]
    public class AA_Requirement_31: BaseSetUp
    {
        public AA_Requirement_31() : base() { }

        [Test, Category("AA"), Category("Core")]
        [TestCase(TestName = "Verify that the analytics call is firing upon Saving Products thru Save to myAnalog widget")]
        public void AA_SaveProduct1()
        {
            string ProjName = "Test_Proj" + util.RandomString();
            action.Navigate(driver, Configuration.Env_Url + "en/ad7960");
            action.IClick(driver, Elements.PDP_MyAnalogWidget_Btn);
            //action.IClick(driver, Elements.MyAnalog_Widget_LogInToMyAnalog_Btn);
            action.ILogin(driver, "aries.sorosoro@analog.com", "Test_1234");
            //action.IClick(driver, Elements.PDP_MyAnalogWidget_Btn);
            action.IClick(driver, By.CssSelector("div[class='saveTo createProject'] * ul>li[data-tab='createProject']"));
            action.IType(driver, By.CssSelector("input[id='newProjectTitle']"), ProjName);
            action.IClick(driver, By.CssSelector("div[class='saveTo createProject'] button[id='saveProductToMyanalog']"));
            var currentWindow = driver.CurrentWindowHandle;

            Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("events"));
                test.validateString(driver, "event119", GetTags["events"]);
            });


            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("prop57"));
                test.validateString(driver, "save Product to New Project", GetTags["prop57"]);
            });
        }
    }
}