﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.Adobe_Analytics
{

    [TestFixture]
    public class AA_Requirement_41 : BaseSetUp
    {
        public AA_Requirement_41() : base() { }

        [Test, Category("AA"), Category("Core")]
        [TestCase(TestName = "Verify analytics call for Gated Tools and Content Download - Registration without Categories")]
        public void AA_GatedTools()
        {
            action.Navigate(driver, Configuration.Env_Url + "en/design-center/adisimpll.html");
            action.IClick(driver, By.CssSelector("a[class='btn btn-primary gated-content__cta']"));
            action.IClick(driver, By.CssSelector("div[class='gated login or register'] div[class='tabs'] button:nth-child(2)"));
            action.IClick(driver, By.CssSelector("section[class='registration short'] div[class='controls fade in']>button[class='ma btn btn-primary']"));
            var currentWindow = driver.CurrentWindowHandle;

            Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("events"));
                test.validateString(driver, "event206", GetTags["events"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar62"));
                test.validateString(driver, "Categories and Market not selected", GetTags["eVar62"]);
            });
        }

        [Test, Category("AA"), Category("Core")]
        [TestCase(TestName = "Verify analytics call for Gated Tools and Content Download - Registration with Categories")]
        public void AA_GatedTools2()
        {
            action.Navigate(driver, Configuration.Env_Url + "en/design-center/adisimpll.html");
            action.IClick(driver, By.CssSelector("a[class='btn btn-primary gated-content__cta']"));
           
            action.IClick(driver, By.CssSelector("div[class='gated login or register'] div[class='tabs'] button:nth-child(2)"));
            action.IClick(driver, By.CssSelector("section[class='categories dropdown'] div[class='multiselect required']>button"));
            string Categories = util.GetText(driver, By.CssSelector("div[class='options open'] div[class='checkbox']:nth-child(1)>label"));
            action.IClick(driver, By.CssSelector("div[class='options open'] div[class='checkbox']:nth-child(1)>input"));
            action.IClick(driver, By.CssSelector("section[class='registration short'] div[class='controls fade in']>button[class='ma btn btn-primary']"));

            var currentWindow = driver.CurrentWindowHandle;

            Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("events"));
                test.validateString(driver, "event206", GetTags["events"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar62"));
                test.validateString(driver, Categories, GetTags["eVar62"]);
            });
        }
        [Test, Category("AA"), Category("Core")]
        [TestCase(TestName = "Verify analytics call for Gated Tools and Content Download - Registration")]
        public void AA_GatedTools3()
        {
            string Email_Address = util.Generate_EmailAddress();

            action.Navigate(driver, Configuration.Env_Url + "en/design-center/adisimpll.html");
            action.IClick(driver, By.CssSelector("a[class='btn btn-primary gated-content__cta']"));
            action.IClick(driver, By.CssSelector("div[class='gated login or register'] div[class='tabs'] button:nth-child(2)"));
            action.IClick(driver, By.CssSelector("section[class='registration short'] div[class='controls fade in']>button[class='ma btn btn-primary']"));
            action.IType(driver, Elements.Registration_VerifyEmail_TxtField, Email_Address);
            action.IClick(driver, Elements.Registration_SendVerificationCode_Btn);

            var currentWindow = driver.CurrentWindowHandle;

            Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("events"));
                test.validateString(driver, "event207", GetTags["events"]);
            });

            driver.Close();
            driver.SwitchTo().Window(currentWindow);
            //action.IClick(driver, By.CssSelector("div[class='verify email']>button[class='ma btn btn-link']"));
            action.IOpenNewTab(driver);
            action.ICheckMailinatorEmail(driver, Email_Address);
            action.IClick(driver, By.CssSelector("table[class='table-striped jambo_table'] * tr>td:nth-child(2)"));
            driver.SwitchTo().Frame(driver.FindElement(By.CssSelector("iframe[id='html_msg_body']")));
            string VerificationCode = util.GetText(driver, By.CssSelector("td>strong")).Substring(util.GetText(driver, By.CssSelector("td>strong")).Length - 6).Trim();
            driver.SwitchTo().Window(driver.WindowHandles.First());
            action.IType(driver, Elements.Registration_EnterCode_TextField, VerificationCode);
            action.IClick(driver, Elements.Registration_SendVerificationCode_Btn);

            GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("events"));
                test.validateString(driver, "event208", GetTags["events"]);
            });
            driver.Close();
            driver.SwitchTo().Window(currentWindow);

            action.IType(driver, Elements.Registration_FirstName_TxtField, "Test");
            action.IType(driver, Elements.Registration_LastName_TxtField, "Auto");
            action.IType(driver, Elements.Registration_Password_TxtField, "Test_1234");
            action.IType(driver, Elements.Registration_ConfirmPassword_TxtField, "Test_1234");
            action.IType(driver, Elements.Registration_ZipCode_TxtField, "12345");
            action.IType(driver, Elements.Registration_CompName_TxtField, "Test");
            action.IClick(driver, By.CssSelector("div[class='short registration fields'] button[class='ma btn btn-link']"));
            action.IClick(driver, By.CssSelector("div[class='short registration fields'] div[class='options open']>div[data-label='UNITED STATES']"));
            action.IClick(driver, Elements.Registration_TermsOfUse_ChxBox);
            Thread.Sleep(3000);
            action.IClick(driver, By.CssSelector("section[class='register short'] * button[class='ma btn btn-primary']"));
            GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("events"));
                test.validateString(driver, "event209", GetTags["events"]);
            });
        }
    }
}