﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Collections.Generic;
using OpenQA.Selenium.Interactions;
using System.Threading;

namespace AS_Test_Framework.Adobe_Analytics
{

    [TestFixture]
    public class AA_Requirement_11 : BaseSetUp
    {
        public AA_Requirement_11() : base() { }

        [Test, Category("AA"), Category("Core")]
        [TestCase(TestName = "Verify that the analytics call is firing when the View Pricing button of a Reference Design is clicked")]
        public void AA_SignalsChains_1()
        {
            action.Navigate(driver, Configuration.Env_Url + "en/applications/markets/aerospace-and-defense-pavilion-home/radar-solution.html");
            action.IClick(driver, By.CssSelector("div[class='accordion-title pavillion-heading'] * a[class='expand']"));
            string CN_no = util.GetText(driver, By.CssSelector("div[class='adi-accordion alternate-state col-md-offset-2 container-offset  expand-all']>div:nth-child(2)>div[class='product-content'] * h3"));
            var currentWindow = driver.CurrentWindowHandle;
            IWebElement body = driver.FindElement(By.CssSelector("div[class='adi-accordion alternate-state col-md-offset-2 container-offset  expand-all']>div:nth-child(2)>div[class='product-content'] * div[class='col-md-8 clearfix']>a:nth-child(3)"));
            Actions OpenLink = new Actions(driver);
            OpenLink.KeyDown(Keys.Control).MoveToElement(body).Click().Perform();
            
            Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("events"));
                test.validateString(driver, "event110", GetTags["events"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar95"));
                test.validateString(driver, "next generation radar|" + CN_no.ToLower() + "|View Pricing", GetTags["eVar95"]);
            });
        }

        [Test, Category("AA"), Category("Core")]
        [TestCase(TestName = "Verify that the analytics call is firing when a Product Number of a Reference Design is clicked")]
        public void AA_SignalsChains_2()
        {
            action.Navigate(driver, Configuration.Env_Url + "en/applications/markets/aerospace-and-defense-pavilion-home/radar-solution.html");
            action.IClick(driver, By.CssSelector("div[class='accordion-title pavillion-heading'] * a[class='expand']"));
            string CN_no = util.GetText(driver, By.CssSelector("div[class='adi-accordion alternate-state col-md-offset-2 container-offset  expand-all']>div:nth-child(2)>div[class='product-content'] * h3"));
            string Prod_No = util.GetText(driver, By.CssSelector("div[class='adi-accordion alternate-state col-md-offset-2 container-offset  expand-all']>div:nth-child(2)>div[class='product-content'] * div[class='col-md-4']:nth-of-type(2) * a"));
            var currentWindow = driver.CurrentWindowHandle;
            IWebElement body = driver.FindElement(By.CssSelector("div[class='adi-accordion alternate-state col-md-offset-2 container-offset  expand-all']>div:nth-child(2)>div[class='product-content'] * div[class='col-md-4']:nth-of-type(2) * a"));
            Actions OpenLink = new Actions(driver);
            OpenLink.KeyDown(Keys.Control).MoveToElement(body).Click().Perform();

            Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("events"));
                test.validateString(driver, "event111", GetTags["events"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar95"));
                test.validateString(driver, "next generation radar|Products Used|" + CN_no.ToLower() + "|" + Prod_No.ToLower(), GetTags["eVar95"]);
            });
        }
    }
}