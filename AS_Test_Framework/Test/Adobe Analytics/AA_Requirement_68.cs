﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Collections.Generic;
using OpenQA.Selenium.Interactions;
using System.Threading;
using System.Linq;

namespace AS_Test_Framework.Adobe_Analytics
{

    [TestFixture]
    public class AA_Requirement_68: BaseSetUp
    {
        public AA_Requirement_68() : base() { }

        [Test, Category("AA"), Category("Core")]
        [TestCase(TestName = "Verify that the analytics call is firing on the Vehicle Electrification Interactive content")]
        public void AA_MW1()
        {
            action.Navigate(driver, Configuration.Env_Url + "en/applications/markets/automotive-pavilion-home/vehicle-electrification.html");
            action.IClick(driver, By.CssSelector("section[class='diagram']>a"));
            string evar58 = util.GetText(driver, By.CssSelector("h3[class='title']")) + "," + util.GetText(driver, By.CssSelector("section[class='information']>span"));

            var currentWindow = driver.CurrentWindowHandle;

            Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("events"));
                test.validateString(driver, "event176", GetTags["events"]);
            });

            Assert.Multiple(() =>
            {
                Assert.True(GetTags.ContainsKey("eVar58"));
                test.validateString(driver, evar58, GetTags["eVar58"]);
            });

        }
    }
}