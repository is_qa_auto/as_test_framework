﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Collections.Generic;
using OpenQA.Selenium.Interactions;
using System.Threading;
using System;

namespace AS_Test_Framework.Adobe_Analytics
{

    [TestFixture]
    public class AA_Requirement_26: BaseSetUp
    {
        public AA_Requirement_26() : base() { }

        [Test, Category("AA"), Category("NonCore")]
        [TestCase(TestName = "Verify that the analytics call is firing when user completes purchasing a product")]
        public void AA_OrderPurchaseItem()
        {
            if (!Configuration.Environment.Equals("production")) {
                action.Navigate(driver, Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx");
                action.IClick(driver, Elements.SC_LoginLink);
                action.ILogin(driver, "aries.sorosoro@analog.com", "Test_1234");
                util.RemoveAllModelsInYourCartTable(driver);

                action.IAddModelToPurchase(driver, "AD7960BCPZ");

                action.IClick(driver, Elements.Shopping_Cart_Checkout_Button);
                Thread.Sleep(3000);

                action.IPopulateFieldsInShippingAddressPage(driver);

                action.IPopulateFieldsInBillingAddressPage(driver);

                action.IPopulateFieldsInAboutYourProjectPage(driver);

                action.IPopulateFieldsInEnterPaymentAndPlaceOrderPage(driver);

                DateTime TimeToday = DateTime.Now;
                string WindowTitle = driver.Title.ToLower();
                string CurrentUrl = driver.Url.Replace("https://", "");
                string OrderConfirmationNo = util.GetText(driver, Elements.Shopping_Cart_OrderConfirmation_OrderRefNos);

                var currentWindow = driver.CurrentWindowHandle;

                Dictionary<string, string> GetTags = util.GetAdobeTags(driver);

                Assert.Multiple(() =>
                {
                    Assert.True(GetTags.ContainsKey("eVar64"));
                    test.validateString(driver, OrderConfirmationNo, GetTags["eVar64"]);
                });
                Assert.Multiple(() =>
                {
                    Assert.True(GetTags.ContainsKey("events"));
                    test.validateStringInstance(driver, GetTags["events"], "purchase");
                });
                Assert.Multiple(() =>
                {
                    Assert.True(GetTags.ContainsKey("purchase ID"));
                    test.validateString(driver, OrderConfirmationNo, GetTags["eVar64"]);
                });
                Assert.Multiple(() =>
                {
                    Assert.True(GetTags.ContainsKey("channel"));
                    test.validateString(driver, Configuration.Env_Url.Replace("https://","").Replace("www","shoppingcart").Replace("cldnet", "corpnt").Replace("com/","com"), GetTags["channel"]);
                });

                Assert.Multiple(() =>
                {
                    Assert.True(GetTags.ContainsKey("eVar1"));
                    test.validateString(driver, CurrentUrl.Split('?')[0].ToLower(), GetTags["eVar1"]);
                });
                Assert.Multiple(() =>
                {
                    Assert.True(GetTags.ContainsKey("hier1"));
                    test.validateString(driver, Configuration.Env_Url.Replace("https://", "").Replace("www", "shoppingcart").Replace("cldnet", "corpnt").Replace("com/", "com") + "|||", GetTags["hier1"]);
                });
                Assert.Multiple(() =>
                {
                    Assert.True(GetTags.ContainsKey("prop2"));
                    test.validateString(driver, "new", GetTags["prop2"]);
                });
                Assert.Multiple(() =>
                {
                    Assert.True(GetTags.ContainsKey("eVar2"));
                    test.validateString(driver, "D=c2", GetTags["eVar2"]);
                });
                Assert.Multiple(() =>
                {
                    Assert.True(GetTags.ContainsKey("eVar3"));
                    test.validateString(driver, "D=c3", GetTags["eVar3"]);
                });
                Assert.Multiple(() =>
                {
                    Assert.True(GetTags.ContainsKey("prop4"));
                    test.validateString(driver, "1", GetTags["prop4"]);
                });
                Assert.Multiple(() =>
                {
                    Assert.True(GetTags.ContainsKey("eVar4"));
                    test.validateString(driver, "D=c4", GetTags["eVar4"]);
                });
                Assert.Multiple(() =>
                {
                    Assert.True(GetTags.ContainsKey("eVar5"));
                    test.validateString(driver, "D=c5", GetTags["eVar5"]);
                });
                Assert.Multiple(() =>
                {
                    Assert.True(GetTags.ContainsKey("eVar6"));
                    test.validateString(driver, "D=c6", GetTags["eVar6"]);
                });
                Assert.Multiple(() =>
                {
                    Assert.True(GetTags.ContainsKey("prop7"));
                    test.validateString(driver, "year=" + DateTime.Now.Year.ToString() + " | month=" + DateTime.Today.ToString("MMMM") + " | date=" + DateTime.Now.Day.ToString() + " | day=" + DateTime.Now.DayOfWeek.ToString() + " | time=" + TimeZoneInfo.ConvertTime(TimeToday, TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time")).ToString("h:mm tt"), GetTags["prop7"]);
                });
                Assert.Multiple(() =>
                {
                    Assert.True(GetTags.ContainsKey("eVar7"));
                    test.validateString(driver, "D=c7", GetTags["eVar7"]);
                });

                Assert.Multiple(() =>
                {
                    Assert.True(GetTags.ContainsKey("eVar9"));
                    test.validateString(driver, "non-member", GetTags["eVar9"]);
                });
                Assert.Multiple(() =>
                {
                    Assert.True(GetTags.ContainsKey("prop10"));
                    test.validateString(driver, Configuration.Env_Url.Replace("https://", "").Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "placeorder.aspx", GetTags["prop10"]);
                });

                Assert.Multiple(() =>
                {
                    Assert.True(GetTags.ContainsKey("eVar10"));
                    test.validateString(driver, "logged out", GetTags["eVar10"]);
                });
                Assert.Multiple(() =>
                {
                    Assert.True(GetTags.ContainsKey("prop13"));
                    test.validateString(driver, "D=ch+'|'+v51", GetTags["prop13"]);
                });
                Assert.Multiple(() =>
                {
                    Assert.True(GetTags.ContainsKey("eVar13"));
                    test.validateString(driver, "D=c10", GetTags["eVar13"]);
                });
                Assert.Multiple(() =>
                {
                    Assert.True(GetTags.ContainsKey("prop18"));
                    test.validateString(driver, "D=ch", GetTags["prop18"]);
                });
                Assert.Multiple(() =>
                {
                    Assert.True(GetTags.ContainsKey("prop19"));
                    test.validateString(driver, "D=c1", GetTags["prop19"]);
                });
                Assert.Multiple(() =>
                {
                    Assert.True(GetTags.ContainsKey("prop22"));
                    test.validateString(driver, "en", GetTags["prop22"]);
                });
                Assert.Multiple(() =>
                {
                    Assert.True(GetTags.ContainsKey("eVar22"));
                    test.validateString(driver, "D=c25", GetTags["eVar22"]);
                });
                Assert.Multiple(() =>
                {
                    Assert.True(GetTags.ContainsKey("eVar24"));
                    test.validateString(driver, "D=c22", GetTags["eVar24"]);
                });
                Assert.Multiple(() =>
                {
                    Assert.True(GetTags.ContainsKey("prop25"));
                    test.validateString(driver, WindowTitle, GetTags["prop25"]);
                });

                Assert.Multiple(() =>
                {
                    Assert.True(GetTags.ContainsKey("eVar39"));
                    test.validateString(driver, "D=s_vi", GetTags["eVar39"]);
                });
                Assert.Multiple(() =>
                {
                    Assert.True(GetTags.ContainsKey("eVar51"));
                    test.validateString(driver, "en", GetTags["eVar51"]);
                });

                driver.Close();
                driver.SwitchTo().Window(currentWindow);
                action.ICancelOrdersInMyAnalog(driver, "aries.sorosor@analog.com", "Test_1234");
            }
        }
    }
}