﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Threading;

namespace AS_Test_Framework.Footer
{
    [TestFixture]
    public class Footer_SocialLinks_VerifyTwitter : BaseSetUp
    {
        public Footer_SocialLinks_VerifyTwitter() : base() { }

        //--- URLs ---//
        string core_page_url = "/index.html";

        [Test, Category("Footer"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Core Page - Verify that the Twitter Social Link is Present in EN Locale")]
        [TestCase("cn", TestName = "Core Page - Verify that the Youku Social Link is Present in CN Locale")]
        [TestCase("jp", TestName = "Core Page - Verify that the Twitter Social Link is Present in JP Locale")]
        [TestCase("ru", TestName = "Core Page - Verify that the Twitter Social Link is Present in RU Locale")]
        public void Footer_SocialLinks_VerifyTwitterInCore(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + core_page_url);

            if (Locale.Equals("cn") || Locale.Equals("zh"))
            {

                test.validateElementIsPresent(driver, Elements.Footer_Youku_SocialLink);
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.Footer_Twitter_SocialLink);
            }
        }

        [Test, Category("Footer"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Non Core Page - Verify that the Twitter Social Link is Present in EN Locale")]
        [TestCase("zh", TestName = "Non Core Page - Verify that the Youku Social Link is Present in CN Locale")]
        [TestCase("jp", TestName = "Non Core Page - Verify that the Twitter Social Link is Present in JP Locale")]
        [TestCase("ru", TestName = "Non Core Page - Verify that the Twitter Social Link is Present in RU Locale")]
        public void Footer_SocialLinks_VerifyTwitterInNonCore(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx?locale=" + Locale);
            Thread.Sleep(250);

            if (Locale.Equals("cn") || Locale.Equals("zh"))
            {
                test.validateElementIsPresent(driver, Elements.Footer_Youku_SocialLink);
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.Footer_Twitter_SocialLink);
            }
        }
    }
}