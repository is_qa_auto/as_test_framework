﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;

namespace AS_Test_Framework.Footer
{
    [TestFixture]
    public class Footer_QuickLinks_VerifyInvestorRelations : BaseSetUp
    {
        public Footer_QuickLinks_VerifyInvestorRelations() : base() { }

        //--- URLs ---//
        string corePage_url = "/index.html";
        string investorRelations_page_url = "investor.analog.com";

        [Test, Category("Footer"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Investor Relations Quick Link is Present and Redirects to the Investor Relations Page in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Investor Relations Quick Link is Present and Redirects to the Investor Relations Page in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Investor Relations Quick Link is Present and Redirects to the Investor Relations Page in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Investor Relations Quick Link is Present and Redirects to the Investor Relations Page in RU Locale")]
        public void Footer_VerifyInvestorRelationsQuickLink(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + corePage_url);

            Assert.Multiple(() =>
            {
                test.validateElementIsPresent(driver, Elements.Footer_InvestorRelations_QuickLink);
                if (util.CheckElement(driver, Elements.Footer_InvestorRelations_QuickLink, 1))
                {
                    action.IClick(driver, Elements.Footer_InvestorRelations_QuickLink);
                    test.validateStringInstance(driver, driver.Url, investorRelations_page_url);
                }
            });
        }
    }
}