﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;

namespace AS_Test_Framework.Footer
{
    [TestFixture]
    public class Footer_QuickLinks_VerifyAnalogDialogue : BaseSetUp
    {
        public Footer_QuickLinks_VerifyAnalogDialogue() : base() { }

        //--- URLs ---//
        string corePage_url = "/index.html";
        string analogDialogue_page_url = "/analog-dialogue.html";

        [Test, Category("Footer"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Analog Dialogue Quick Link is Present and Redirects to the Analog Dialogue Page in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Analog Dialogue Quick Link is Present and Redirects to the Analog Dialogue Page in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Analog Dialogue Quick Link is Present and Redirects to the Analog Dialogue Page in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Analog Dialogue Quick Link is Present and Redirects to the Analog Dialogue Page in RU Locale")]
        public void Footer_VerifyAnalogDialogueQuickLink(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + corePage_url);

            Assert.Multiple(() =>
            {
                test.validateElementIsPresent(driver, Elements.Footer_AnalogDialogue_QuickLink);
                if (util.CheckElement(driver, Elements.Footer_AnalogDialogue_QuickLink, 1))
                {
                    action.IClick(driver, Elements.Footer_AnalogDialogue_QuickLink);
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + analogDialogue_page_url);
                }
            });
        }
    }
}