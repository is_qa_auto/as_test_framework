﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;

namespace AS_Test_Framework.Footer
{
    [TestFixture]
    public class Footer_SocialLinks_VerifyFacebook : BaseSetUp
    {
        public Footer_SocialLinks_VerifyFacebook() : base() { }

        //--- URLs ---//
        string core_page_url = "/index.html";

        [Test, Category("Footer"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Core Page - Verify that the Facebook Social Link is Present in EN Locale")]
        [TestCase("cn", TestName = "Core Page - Verify that the Weibo Social Link is Present in CN Locale")]
        [TestCase("jp", TestName = "Core Page - Verify that the Facebook Social Link is Present in JP Locale")]
        [TestCase("ru", TestName = "Core Page - Verify that the Facebook Social Link is Present in RU Locale")]
        public void Footer_SocialLinks_VerifyFacebookInCore(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + core_page_url);

            if (Locale.Equals("cn") || Locale.Equals("zh"))
            {
                test.validateElementIsPresent(driver, Elements.Footer_Weibo_SocialLink);
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.Footer_Fb_SocialLink);
            }
        }

        [Test, Category("Footer"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Non Core Page - Verify that the Facebook Social Link is Present in EN Locale")]
        [TestCase("zh", TestName = "Non Core Page - Verify that the Weibo Social Link is Present in CN Locale")]
        [TestCase("jp", TestName = "Non Core Page - Verify that the Facebook Social Link is Present in JP Locale")]
        [TestCase("ru", TestName = "Non Core Page - Verify that the Facebook Social Link is Present in RU Locale")]
        public void Footer_SocialLinks_VerifyFacebookInNonCore(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx?locale=" + Locale);

            if (Locale.Equals("cn") || Locale.Equals("zh"))
            {
                test.validateElementIsPresent(driver, Elements.Footer_Weibo_SocialLink);
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.Footer_Fb_SocialLink);
            }
        }
    }
}