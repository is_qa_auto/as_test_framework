﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;

namespace AS_Test_Framework.Footer
{

    [TestFixture]
    public class Footer_Languages : BaseSetUp
    {
        public Footer_Languages() : base() { }

        //--- URLs ---//
        string corePage_url = "/index.html";

        [Test, Category("Footer"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the English Language is Present and Redirects to the EN Locale - Analog Home Page in EN Locale")]
        [TestCase("cn", TestName = "Verify that the English Language is Present and Redirects to the EN Locale - Analog Home Page in CN Locale")]
        [TestCase("jp", TestName = "Verify that the English Language is Present and Redirects to the EN Locale - Analog Home Page in JP Locale")]
        [TestCase("ru", TestName = "Verify that the English Language is Present and Redirects to the EN Locale - Analog Home Page in RU Locale")]
        public void Footer_Languages_VerifyEnglish(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + corePage_url);

            Assert.Multiple(() =>
            {
                test.validateElementIsPresent(driver, Elements.Footer_En_Language);
                if (util.CheckElement(driver, Elements.Footer_En_Language, 1))
                {
                    action.IClick(driver, Elements.Footer_En_Language);
                    test.validateStringInstance(driver, driver.Url, "analog.com/en/index.html");
                }
            });
        }

        [Test, Category("Footer"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the 简体中文 Language is Present and Redirects to the CN Locale - Analog Home Page in EN Locale")]
        [TestCase("cn", TestName = "Verify that the 简体中文 Language is Present and Redirects to the CN Locale - Analog Home Page in CN Locale")]
        [TestCase("jp", TestName = "Verify that the 简体中文 Language is Present and Redirects to the CN Locale - Analog Home Page in JP Locale")]
        [TestCase("ru", TestName = "Verify that the 简体中文 Language is Present and Redirects to the CN Locale - Analog Home Page in RU Locale")]
        public void Footer_Languages_VerifyChinese(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + corePage_url);

            Assert.Multiple(() =>
            {
                test.validateElementIsPresent(driver, Elements.Footer_Cn_Language);
                if (util.CheckElement(driver, Elements.Footer_Cn_Language, 1))
                {
                    action.IClick(driver, Elements.Footer_Cn_Language);
                    test.validateStringInstance(driver, driver.Url, "analog.com/cn/index.html");
                }
            });
        }

        [Test, Category("Footer"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the 日本語 Language is Present and Redirects to the JP Locale - Analog Home Page in EN Locale")]
        [TestCase("cn", TestName = "Verify that the 日本語 Language is Present and Redirects to the JP Locale - Analog Home Page in CN Locale")]
        [TestCase("jp", TestName = "Verify that the 日本語 Language is Present and Redirects to the JP Locale - Analog Home Page in JP Locale")]
        [TestCase("ru", TestName = "Verify that the 日本語 Language is Present and Redirects to the JP Locale - Analog Home Page in RU Locale")]
        public void Footer_Languages_VerifyJapanese(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + corePage_url);

            Assert.Multiple(() =>
            {
                test.validateElementIsPresent(driver, Elements.Footer_Jp_Language);
                if (util.CheckElement(driver, Elements.Footer_Jp_Language, 1))
                {
                    action.IClick(driver, Elements.Footer_Jp_Language);
                    test.validateStringInstance(driver, driver.Url, "analog.com/jp/index.html");
                }
            });
        }

        [Test, Category("Footer"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Руccкий Language is Present and Redirects to the RU Locale - Analog Home Page in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Руccкий Language is Present and Redirects to the RU Locale - Analog Home Page in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Руccкий Language is Present and Redirects to the RU Locale - Analog Home Page in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Руccкий Language is Present and Redirects to the RU Locale - Analog Home Page in RU Locale")]
        public void Footer_Languages_VerifyRussian(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + corePage_url);

            Assert.Multiple(() =>
            {
                test.validateElementIsPresent(driver, Elements.Footer_Ru_Language);
                if (util.CheckElement(driver, Elements.Footer_Ru_Language, 1))
                {
                    action.IClick(driver, Elements.Footer_Ru_Language);
                    test.validateStringInstance(driver, driver.Url, "analog.com/ru/index.html");
                }
            });
        }
    }
}