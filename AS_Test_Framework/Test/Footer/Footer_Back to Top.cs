﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;

namespace AS_Test_Framework.Footer
{

    [TestFixture]
    public class Footer_BackToTop : BaseSetUp
    {
        public Footer_BackToTop() : base() { }

        //--- URLs ---//
        string corePage_url = "/index.html";

        [Test, Category("Footer"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Back To Top Button is Present in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Back To Top Button is Present in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Back To Top Button is Present in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Back To Top Button is Present in RU Locale")]
        public void Footer_VerifyBackToTopButton(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + corePage_url);
            action.IMouseOverTo(driver, Elements.Footer);

            test.validateElementIsPresent(driver, Elements.BackToTop_Btn);
        }
    }
}