﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;

namespace AS_Test_Framework.Footer
{
    [TestFixture]
    public class Footer_QuickLinks_VerifySalesAndDistribution : BaseSetUp
    {
        public Footer_QuickLinks_VerifySalesAndDistribution() : base() { }

        //--- URLs ---//
        string corePage_url = "/index.html";
        string salesAndDistribution_page_url = "/about-adi/corporate-information/sales-distribution.html";

        [Test, Category("Footer"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Sales and Distribution Quick Link is Present and Redirects to the Sales and Distribution Page in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Sales and Distribution Quick Link is Present and Redirects to the Sales and Distribution Page in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Sales and Distribution Quick Link is Present and Redirects to the Sales and Distribution Page in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Sales and Distribution Quick Link is Present and Redirects to the Sales and Distribution Page in RU Locale")]
        public void Footer_VerifySalesAndDistributionQuickLink(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + corePage_url);

            Assert.Multiple(() =>
            {
                test.validateElementIsPresent(driver, Elements.Footer_Sd_QuickLink);
                if (util.CheckElement(driver, Elements.Footer_Sd_QuickLink, 1))
                {
                    action.IClick(driver, Elements.Footer_Sd_QuickLink);

                    if (Locale.Equals("cn") || Locale.Equals("zh"))
                    {
                        test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + "/about-adi/landing-pages/002/sales-and-distributors.html");
                    }
                    else if (Locale.Equals("jp"))
                    {
                        test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + "/about-adi/landing-pages/003/jp-sales-and-disti.html");
                    }
                    else
                    {
                        test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + salesAndDistribution_page_url);
                    }
                }
            });
        }
    }
}