﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;

namespace AS_Test_Framework.Footer
{
    [TestFixture]
    public class Footer_QuickLinks_VerifyAboutADI : BaseSetUp
    {
        public Footer_QuickLinks_VerifyAboutADI() : base() { }

        //--- URLs ---//
        string corePage_url = "/index.html";
        string aboutAdi_page_url = "/about-adi.html";

        [Test, Category("Footer"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the About ADI Quick Link is Present and Redirects to the About ADI Page in EN Locale")]
        [TestCase("cn", TestName = "Verify that the About ADI Quick Link is Present and Redirects to the About ADI Page in CN Locale")]
        [TestCase("jp", TestName = "Verify that the About ADI Quick Link is Present and Redirects to the About ADI Page in JP Locale")]
        [TestCase("ru", TestName = "Verify that the About ADI Quick Link is Present and Redirects to the About ADI Page in RU Locale")]
        public void Footer_VerifyAboutADIQuickLink(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + corePage_url);

            Assert.Multiple(() =>
            {
                test.validateElementIsPresent(driver, Elements.Footer_AboutAdi_QuickLink);
                if (util.CheckElement(driver, Elements.Footer_AboutAdi_QuickLink, 1))
                {
                    action.IClick(driver, Elements.Footer_AboutAdi_QuickLink);
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + aboutAdi_page_url);
                }
            });
        }
    }
}