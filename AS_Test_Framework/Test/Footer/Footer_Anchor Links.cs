﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Threading;

namespace AS_Test_Framework.Footer
{

    [TestFixture]
    public class Footer_AnchorLinks : BaseSetUp
    {
        public Footer_AnchorLinks() : base() { }

        //--- URLs ---//
        string corePage_url = "/index.html";
        string dynamicPst_url = "/parametricsearch/11062";
        string siteMap_page_url = "/sitemap.html";
        string privacyAndecurityStatement_page_url = "/about-adi/landing-pages/001/privacy_security_statement.html";
        string privacySettings_page_url = "/landing-pages/001/privacy-settings.html";
        string termsOfUse_page_url = "/about-adi/landing-pages/001/terms_of_use.html";

        [Test, Category("Footer"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Core Page - Verify that the Sitemap Anchor Link is Present in the Footer and Redirects to the Site Map Page in EN Locale")]
        [TestCase("cn", TestName = "Core Page - Verify that the Sitemap Anchor Link is Present in the Footer and Redirects to the Site Map Page in CN Locale")]
        [TestCase("jp", TestName = "Core Page - Verify that the Sitemap Anchor Link is Present in the Footer and Redirects to the Site Map Page in JP Locale")]
        [TestCase("ru", TestName = "Core Page - Verify that the Sitemap Anchor Link is Present in the Footer and Redirects to the Site Map Page in RU Locale")]
        public void Footer_AnchorLinks_VerifySitemapInFooter(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + corePage_url);

            Assert.Multiple(() =>
            {
                test.validateElementIsPresent(driver, Elements.Footer_Sitemap_Link);
                if (util.CheckElement(driver, Elements.Footer_Sitemap_Link, 1))
                {
                    action.IClick(driver, Elements.Footer_Sitemap_Link);
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + siteMap_page_url);
                }
            });
        }

        [Test, Category("Footer"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Non Core Page - Verify that the Sitemap Anchor Link is Present in the Thinner Footer and Redirects to the Site Map Page in EN Locale")]
        [TestCase("cn", TestName = "Non Core Page - Verify that the Sitemap Anchor Link is Present in the Thinner Footer and Redirects to the Site Map Page in CN Locale")]
        [TestCase("jp", TestName = "Non Core Page - Verify that the Sitemap Anchor Link is Present in the Thinner Footer and Redirects to the Site Map Page in JP Locale")]
        [TestCase("ru", TestName = "Non Core Page - Verify that the Sitemap Anchor Link is Present in the Thinner Footer and Redirects to the Site Map Page in RU Locale")]
        public void Footer_AnchorLinks_VerifySitemapInThinnerFooter(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + dynamicPst_url);
            Thread.Sleep(250);

            Assert.Multiple(() =>
            {
                test.validateElementIsPresent(driver, Elements.Footer_Sitemap_Link);
                if (util.CheckElement(driver, Elements.Footer_Sitemap_Link, 1))
                {
                    action.IClick(driver, Elements.Footer_Sitemap_Link);
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + siteMap_page_url);
                }
            });
        }

        [Test, Category("Footer"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Core Page - Verify that the Privacy and Security Anchor Link is Present in the Footer and Redirects to the Privacy & Security Statement Page in EN Locale")]
        [TestCase("cn", TestName = "Core Page - Verify that the Privacy and Security Anchor Link is Present in the Footer and Redirects to the Privacy & Security Statement Page in CN Locale")]
        [TestCase("jp", TestName = "Core Page - Verify that the Privacy and Security Anchor Link is Present in the Footer and Redirects to the Privacy & Security Statement Page in JP Locale")]
        [TestCase("ru", TestName = "Core Page - Verify that the Privacy and Security Anchor Link is Present in the Footer and Redirects to the Privacy & Security Statement Page in RU Locale")]
        public void Footer_AnchorLinks_VerifyPrivacyAndSecurityInFooter(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + corePage_url);

            Assert.Multiple(() =>
            {
                test.validateElementIsPresent(driver, Elements.Footer_PrivacyAndSecurity_Link);
                if (util.CheckElement(driver, Elements.Footer_PrivacyAndSecurity_Link, 1))
                {
                    action.IClick(driver, Elements.Footer_PrivacyAndSecurity_Link);
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + privacyAndecurityStatement_page_url);
                }
            });
        }

        [Test, Category("Footer"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Non Core Page - Verify that the Privacy and Security Anchor Link is Present in the Thinner Footer and Redirects to the Privacy & Security Statement Page in EN Locale")]
        [TestCase("cn", TestName = "Non Core Page - Verify that the Privacy and Security Anchor Link is Present in the Thinner Footer and Redirects to the Privacy & Security Statement Page in CN Locale")]
        [TestCase("jp", TestName = "Non Core Page - Verify that the Privacy and Security Anchor Link is Present in the Thinner Footer and Redirects to the Privacy & Security Statement Page in JP Locale")]
        [TestCase("ru", TestName = "Non Core Page - Verify that the Privacy and Security Anchor Link is Present in the Thinner Footer and Redirects to the Privacy & Security Statement Page in RU Locale")]
        public void Footer_AnchorLinks_VerifyPrivacyAndSecurityInThinnerFooter(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + dynamicPst_url);
            Thread.Sleep(250);

            Assert.Multiple(() =>
            {
                test.validateElementIsPresent(driver, Elements.Footer_PrivacyAndSecurity_Link);
                if (util.CheckElement(driver, Elements.Footer_PrivacyAndSecurity_Link, 1))
                {
                    action.IClick(driver, Elements.Footer_PrivacyAndSecurity_Link);
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + privacyAndecurityStatement_page_url);
                }
            });
        }

        [Test, Category("Footer"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Core Page - Verify that the Privacy Settings Anchor Link is Present in the Footer and Redirects to the Privacy Settings Page in EN Locale")]
        [TestCase("cn", TestName = "Core Page - Verify that the Privacy Settings Anchor Link is Present in the Footer and Redirects to the Privacy Settings Page in CN Locale")]
        [TestCase("jp", TestName = "Core Page - Verify that the Privacy Settings Anchor Link is Present in the Footer and Redirects to the Privacy Settings Page in JP Locale")]
        [TestCase("ru", TestName = "Core Page - Verify that the Privacy Settings Anchor Link is Present in the Footer and Redirects to the Privacy Settings Page in RU Locale")]
        public void Footer_AnchorLinks_VerifyPrivacySettingsInFooter(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + corePage_url);

            Assert.Multiple(() =>
            {
                test.validateElementIsPresent(driver, Elements.Footer_PrivacySettings_Link);
                if (util.CheckElement(driver, Elements.Footer_PrivacySettings_Link, 1))
                {
                    action.IClick(driver, Elements.Footer_PrivacySettings_Link);
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + privacySettings_page_url);
                }
            });
        }

        [Test, Category("Footer"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Core Page - Verify that the Terms of use Anchor Link is Present in the Footer and Redirects to the Terms of Use Page in EN Locale")]
        [TestCase("cn", TestName = "Core Page - Verify that the Terms of use Anchor Link is Present in the Footer and Redirects to the Terms of Use Page in CN Locale")]
        [TestCase("jp", TestName = "Core Page - Verify that the Terms of use Anchor Link is Present in the Footer and Redirects to the Terms of Use Page in JP Locale")]
        [TestCase("ru", TestName = "Core Page - Verify that the Terms of use Anchor Link is Present in the Footer and Redirects to the Terms of Use Page in RU Locale")]
        public void Footer_AnchorLinks_VerifyTermsOfUseInFooter(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + corePage_url);

            Assert.Multiple(() =>
            {
                test.validateElementIsPresent(driver, Elements.Footer_TermsOfUse_Link);
                if (util.CheckElement(driver, Elements.Footer_TermsOfUse_Link, 1))
                {
                    action.IClick(driver, Elements.Footer_TermsOfUse_Link);

                    if (Locale.Equals("cn") || Locale.Equals("zh") || Locale.Equals("jp"))
                    {
                        test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + termsOfUse_page_url.Replace("/about-adi", ""));
                    }
                    else
                    {
                        test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + termsOfUse_page_url);
                    }
                }
            });
        }

        [Test, Category("Footer"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Non Core Page - Verify that the Terms of use Anchor Link is Present in the Thinner Footer and Redirects to the Terms of Use Page in EN Locale")]
        [TestCase("cn", TestName = "Non Core Page - Verify that the Terms of use Anchor Link is Present in the Thinner Footer and Redirects to the Terms of Use Page in CN Locale")]
        [TestCase("jp", TestName = "Non Core Page - Verify that the Terms of use Anchor Link is Present in the Thinner Footer and Redirects to the Terms of Use Page in JP Locale")]
        [TestCase("ru", TestName = "Non Core Page - Verify that the Terms of use Anchor Link is Present in the Thinner Footer and Redirects to the Terms of Use Page in RU Locale")]
        public void Footer_AnchorLinks_VerifyTermsOfUseInThinnerFooter(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + dynamicPst_url);
            Thread.Sleep(1750);

            Assert.Multiple(() =>
            {
                test.validateElementIsPresent(driver, Elements.Footer_TermsOfUse_Link);
                if (util.CheckElement(driver, Elements.Footer_TermsOfUse_Link, 1))
                {
                    action.IClick(driver, Elements.Footer_TermsOfUse_Link);

                    if (Locale.Equals("cn") || Locale.Equals("zh") || Locale.Equals("jp"))
                    {
                        test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + termsOfUse_page_url.Replace("/about-adi", ""));
                    }
                    else
                    {
                        test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + termsOfUse_page_url);
                    }
                }
            });
        }
    }
}