﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.Footer
{

    [TestFixture]
    public class Footer_Feedback : BaseSetUp
    {
        public Footer_Feedback() : base() { }

        //--- URLs ---//
        string corePage_url = "/index.html";
        string dynamicPst_url = "/parametricsearch/11062";

        [Test, Category("Footer"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Core Page - Verify that the Feedback Button is Present in the Footer and Opens the Analog Study Window in EN Locale")]
        [TestCase("cn", TestName = "Core Page - Verify that the Feedback Button is Present in the Footer and Opens the Analog Study Window in CN Locale")]
        [TestCase("jp", TestName = "Core Page - Verify that the Feedback Button is Present in the Footer and Opens the Analog Study Window in JP Locale")]
        [TestCase("ru", TestName = "Core Page - Verify that the Feedback Button is Present in the Footer and Opens the Analog Study Window in RU Locale")]
        public void Footer_Feedback_VerifyInFooter(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + corePage_url);
            Thread.Sleep(250);
            driver.Navigate().Refresh();
            action.IMouseOverTo(driver, Elements.Footer);

            Assert.Multiple(() =>
            {
                test.validateElementIsPresent(driver, Elements.Footer_Fdbk_Btn);
                if (util.CheckElement(driver, Elements.Footer_Fdbk_Btn, 5))
                {
                    action.IClick(driver, Elements.Footer_Fdbk_Btn);
                    Thread.Sleep(1250);

                    driver.SwitchTo().Window(driver.WindowHandles.Last());
                    test.validateStringInstance(driver, driver.Url, "collect.iperceptions.com");

                    action.IClick(driver, Elements.Footer_FdbkForm_X_Btn);
                    Thread.Sleep(500);

                    test.validateElementIsNotPresent(driver, Elements.Footer_FdbkForm);                    
                }
            });
        }

        //[Test, Category("Footer"), Category("NonCore"), Retry(2)]
        //[TestCase("en", TestName = "Non Core Page - Verify that the Feedback Button is Present in the Thinner Footer and Opens the Analog Study Window in EN Locale")]
        //[TestCase("cn", TestName = "Non Core Page - Verify that the Feedback Button is Present in the Thinner Footer and Opens the Analog Study Window in CN Locale")]
        //[TestCase("jp", TestName = "Non Core Page - Verify that the Feedback Button is Present in the Thinner Footer and Opens the Analog Study Window in JP Locale")]
        //[TestCase("ru", TestName = "Non Core Page - Verify that the Feedback Button is Present in the Thinner Footer and Opens the Analog Study Window in RU Locale")]
        //public void Footer_Feedback_VerifyInThinnerFooter(string Locale)
        //{
        //    action.Navigate(driver, Configuration.Env_Url + Locale + dynamicPst_url);
        //    Thread.Sleep(250);
        //    action.IMouseOverTo(driver, Elements.Footer);
         
        //    Assert.Multiple(() =>
        //    {
        //        test.validateElementIsPresent(driver, Elements.Footer_Fdbk_Btn);
        //        if (util.CheckElement(driver, Elements.Footer_Fdbk_Btn, 1))
        //        {
        //            action.IClick(driver, Elements.Footer_Fdbk_Btn);
        //            Thread.Sleep(1250);

        //            driver.SwitchTo().Window(driver.WindowHandles.Last());
        //            test.validateStringInstance(driver, driver.Url, "collect.iperceptions.com");

        //            action.IClick(driver, Elements.Footer_FdbkForm_X_Btn);
        //            Thread.Sleep(500);

        //            test.validateElementIsNotPresent(driver, Elements.Footer_FdbkForm);
        //        }
        //    });
        //}
    }
}