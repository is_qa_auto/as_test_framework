﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;

namespace AS_Test_Framework.Footer
{

    [TestFixture]
    public class Footer_BigBlocks : BaseSetUp
    {
        public Footer_BigBlocks() : base() { }

        //--- URLs ---//
        string corePage_url = "/index.html";

        [Test, Category("Footer"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Non Core Page - Verify that the Big Blocks are Not Present in EN Locale")]
        [TestCase("zh", TestName = "Non Core Page - Verify that the Big Blocks are Not Present in CN Locale")]
        [TestCase("jp", TestName = "Non Core Page - Verify that the Big Blocks are Not Present in JP Locale")]
        [TestCase("ru", TestName = "Non Core Page - Verify that the Big Blocks are Not Present in RU Locale")]
        public void Footer_BigBlocks_VerifyInNonCore(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx?locale=" + Locale);
            test.validateElementIsNotPresent(driver, Elements.Footer_BigBlock);
        }

        [Test, Category("Footer"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Core Page - Verify that the Big Blocks are Present in the Analog Home Page in EN Locale")]
        [TestCase("cn", TestName = "Core Page - Verify that the Big Blocks are Present in the Analog Home Page in CN Locale")]
        [TestCase("jp", TestName = "Core Page - Verify that the Big Blocks are Present in the Analog Home Page in JP Locale")]
        [TestCase("ru", TestName = "Core Page - Verify that the Big Blocks are Present in the Analog Home Page in RU Locale")]
        public void Footer_BigBlocks_VerifyInCore(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + corePage_url);
            test.validateElementIsPresent(driver, Elements.Footer_BigBlock);
        }

        [Test, Category("Footer"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Core Page - Verify that the Problem Solvers Big Block is Present and Redirects to the About ADI Page in EN Locale")]
        [TestCase("cn", TestName = "Core Page - Verify that the Problem Solvers Big Block is Present and Redirects to the About ADI Page in CN Locale")]
        [TestCase("jp", TestName = "Core Page - Verify that the Problem Solvers Big Block is Present and Redirects to the About ADI Page in JP Locale")]
        [TestCase("ru", TestName = "Core Page - Verify that the Problem Solvers Big Block is Present and Redirects to the About ADI Page in RU Locale")]
        public void Footer_BigBlocks_VerifyProblemSolvers(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + corePage_url);

            Assert.Multiple(() =>
            {
                test.validateElementIsPresent(driver, Elements.Footer_ProblemSolvers_BigBlock);
                if (util.CheckElement(driver, Elements.Footer_ProblemSolvers_BigBlock, 1))
                {
                    action.IClick(driver, Elements.Footer_ProblemSolvers_BigBlock);
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + "/about-adi.html");
                }
            });
        }

        [Test, Category("Footer"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Core Page - Verify that the Patents Worldwide Block is Present and Redirects to the About ADI Page in EN Locale")]
        [TestCase("cn", TestName = "Core Page - Verify that the Patents Worldwide Block is Present and Redirects to the About ADI Page in CN Locale")]
        [TestCase("jp", TestName = "Core Page - Verify that the Patents Worldwide Block is Present and Redirects to the About ADI Page in JP Locale")]
        [TestCase("ru", TestName = "Core Page - Verify that the Patents Worldwide Block is Present and Redirects to the About ADI Page in RU Locale")]
        public void Footer_BigBlocks_VerifyPatentsWorldwide(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + corePage_url);

            Assert.Multiple(() =>
            {
                test.validateElementIsPresent(driver, Elements.Footer_PatentsWorldwide_BigBlock);
                if (util.CheckElement(driver, Elements.Footer_PatentsWorldwide_BigBlock, 1))
                {
                    action.IClick(driver, Elements.Footer_PatentsWorldwide_BigBlock);
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + "/about-adi.html");
                }
            });
        }

        [Test, Category("Footer"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Core Page - Verify that the Customers Big Block is Present and Redirects to the About ADI Page in EN Locale")]
        [TestCase("cn", TestName = "Core Page - Verify that the Customers Big Block is Present and Redirects to the About ADI Page in CN Locale")]
        [TestCase("jp", TestName = "Core Page - Verify that the Customers Big Block is Present and Redirects to the About ADI Page in JP Locale")]
        [TestCase("ru", TestName = "Core Page - Verify that the Customers Big Block is Present and Redirects to the About ADI Page in RU Locale")]
        public void Footer_BigBlocks_VerifyCustomers(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + corePage_url);

            Assert.Multiple(() =>
            {
                test.validateElementIsPresent(driver, Elements.Footer_Customers_BigBlock);
                if (util.CheckElement(driver, Elements.Footer_Customers_BigBlock, 1))
                {
                    action.IClick(driver, Elements.Footer_Customers_BigBlock);
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + "/about-adi.html");
                }
            });
        }

        [Test, Category("Footer"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Core Page - Verify that the Years Big Block and Redirects to The Future Turns Fifty Page in EN Locale")]
        [TestCase("cn", TestName = "Core Page - Verify that the Years Big Block and Redirects to The Future Turns Fifty Page inn CN Locale")]
        [TestCase("jp", TestName = "Core Page - Verify that the Years Big Block and Redirects to The Future Turns Fifty Page in JP Locale")]
        [TestCase("ru", TestName = "Core Page - Verify that the Years Big Block and Redirects to The Future Turns Fifty Page in RU Locale")]
        public void Footer_BigBlocks_VerifyYears(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + corePage_url);

            Assert.Multiple(() =>
            {
                test.validateElementIsPresent(driver, Elements.Footer_Years_BigBlock);
                if (util.CheckElement(driver, Elements.Footer_Years_BigBlock, 1))
                {
                    action.IClick(driver, Elements.Footer_Years_BigBlock);
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + "/landing-pages/001/future-turns-fifty.html");
                }
            });
        }
    }
}