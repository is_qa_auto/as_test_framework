﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;

namespace AS_Test_Framework.Footer
{
    [TestFixture]
    public class Footer_QuickLinks_VerifyContactUs : BaseSetUp
    {
        public Footer_QuickLinks_VerifyContactUs() : base() { }

        //--- URLs ---//
        string corePage_url = "/index.html";
        string contactUs_page_url = "/about-adi/contact-us.html";

        [Test, Category("Footer"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Contact us Quick Link is Present and Redirects to the Contact Us Page in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Contact us Quick Link is Present and Redirects to the Contact Us Page in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Contact us Quick Link is Present and Redirects to the Contact Us Page in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Contact us Quick Link is Present and Redirects to the Contact Us Page in RU Locale")]
        public void Footer_VerifyContactUsQuickLink(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + corePage_url);

            Assert.Multiple(() =>
            {
                test.validateElementIsPresent(driver, Elements.Footer_ContactUs_QuickLink);
                if (util.CheckElement(driver, Elements.Footer_ContactUs_QuickLink, 1))
                {
                    action.IClick(driver, Elements.Footer_ContactUs_QuickLink);
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + contactUs_page_url);
                }
            });
        }
    }
}