﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;

namespace AS_Test_Framework.Footer
{

    [TestFixture]
    public class Footer_AheadOfWhatsPossible : BaseSetUp
    {
        public Footer_AheadOfWhatsPossible() : base() { }

        //--- URLs ---//
        string corePage_url = "/index.html";
        string signals_page_url = "/signals.html";

        [Test, Category("Footer"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Core Page - Verify that the Ahead of Whats Possible is Present in the Analog Home Page in EN Locale", Category = "Smoke_ADIWeb")]
        [TestCase("cn", TestName = "Core Page - Verify that the Ahead of Whats Possible is Present in the Analog Home Page in CN Locale", Category = "Smoke_ADIWeb")]
        [TestCase("jp", TestName = "Core Page - Verify that the Ahead of Whats Possible is Present in the Analog Home Page in JP Locale", Category = "Smoke_ADIWeb")]
        [TestCase("ru", TestName = "Core Page - Verify that the Ahead of Whats Possible is Present in the Analog Home Page in RU Locale")]
        public void Footer_BigBlocks_VerifyInCore(string Locale)
        {
            string initial_steps = "1. Go to " + Configuration.Env_Url + Locale + corePage_url;
            string scenario = "";

            action.INavigate(driver, Configuration.Env_Url + Locale + corePage_url);

            Assert.Multiple(() =>
            {
                //----- Header and Footer TestPlan > Test Case Title: Verify Ahead of What's Possible section -----//

                //--- Expected Result: The Ahead of What's Possible section should display ---//
                scenario = " Verify that Ahead of what's possible section is present";
                test.validateElementIsPresentv2(driver, Elements.Footer_AheadOfWhatsPossible_Header, scenario, initial_steps);

                scenario = " Verify that Ahead of what's possible call out is present";
                if (Locale.Equals("en") || Locale.Equals("jp"))
                {
                    test.validateElementIsPresentv2(driver, By.CssSelector("div[class='callout']>h4"), scenario, initial_steps);
                }
                else
                {
                    test.validateElementIsPresentv2(driver, Elements.Footer_AheadOfWhatsPossible_Desc, scenario , initial_steps);
                }

                //--- Expected Result: "See the Innovations" link is displayed ---//
                scenario = "Verify that \"See the innovation\" link is present in ahead of whats possible section";
                test.validateElementIsPresentv2(driver, Elements.Footer_SeeTheInnovations_Link, scenario, initial_steps);

                if (util.CheckElement(driver, Elements.Footer_SeeTheInnovations_Link, 1))
                {
                    //--- Action: Click "See the Innovations" link ---//
                    action.IClick(driver, Elements.Footer_SeeTheInnovations_Link);

                    //--- Expected Result: Ahead of What's Possible page is displayed ---//
                    scenario = "Verify that user will be redirected to signal+ page when clicking see the innovation link";
                    test.validateScreenByUrlv2(driver, Configuration.Env_Url + Locale + signals_page_url, scenario, initial_steps + "<br>2. Click see the innovations link in ahead of whats possible section");
                }
            });
        }
    }
}