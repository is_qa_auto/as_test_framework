﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Threading;

namespace AS_Test_Framework.Footer
{
    [TestFixture]
    public class Footer_SocialLinks_VerifyEngineerZone : BaseSetUp
    {
        public Footer_SocialLinks_VerifyEngineerZone() : base() { }

        //--- URLs ---//
        string core_page_url = "/index.html";
        string engineerZone_page_url = "ez.analog.com";

        [Test, Category("Footer"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Core Page - Verify that the EngineerZone Social Link is Present and Redirects to the EngineerZone Page in EN Locale")]
        [TestCase("cn", TestName = "Core Page - Verify that the EngineerZone Social Link is Present and Redirects to the EngineerZone Page in CN Locale")]
        [TestCase("jp", TestName = "Core Page - Verify that the EngineerZone Social Link is Present and Redirects to the EngineerZone Page in JP Locale")]
        [TestCase("ru", TestName = "Core Page - Verify that the EngineerZone Social Link is Present and Redirects to the EngineerZone Page in RU Locale")]
        public void Footer_SocialLinks_VerifyEngineerZoneInCore(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + core_page_url);

            Assert.Multiple(() =>
            {
                test.validateElementIsPresent(driver, Elements.Footer_Ez_SocialLink);
                if (util.CheckElement(driver, Elements.Footer_Ez_SocialLink, 1))
                {
                    action.IClick(driver, Elements.Footer_Ez_SocialLink);
                    test.validateStringInstance(driver, driver.Url, engineerZone_page_url);
                }
            });
        }

        [Test, Category("Footer"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Non Core Page - Verify that the EngineerZone Social Link is Present and Redirects to the EngineerZone Page in EN Locale")]
        [TestCase("zh", TestName = "Non Core Page - Verify that the EngineerZone Social Link is Present and Redirects to the EngineerZone Page in CN Locale")]
        [TestCase("jp", TestName = "Non Core Page - Verify that the EngineerZone Social Link is Present and Redirects to the EngineerZone Page in JP Locale")]
        [TestCase("ru", TestName = "Non Core Page - Verify that the EngineerZone Social Link is Present and Redirects to the EngineerZone Page in RU Locale")]
        public void Footer_SocialLinks_VerifyEngineerZoneInNonCore(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx?locale=" + Locale);
            Thread.Sleep(250);

            Assert.Multiple(() =>
            {
                test.validateElementIsPresent(driver, Elements.Footer_Ez_SocialLink);
                if (util.CheckElement(driver, Elements.Footer_Ez_SocialLink, 1))
                {
                    action.IClick(driver, Elements.Footer_Ez_SocialLink);
                    test.validateStringInstance(driver, driver.Url, engineerZone_page_url);
                }
            });
        }
    }
}