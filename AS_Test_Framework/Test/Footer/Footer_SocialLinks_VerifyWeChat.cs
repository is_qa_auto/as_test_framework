﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;

namespace AS_Test_Framework.Footer
{
    [TestFixture]
    public class Footer_SocialLinks_VerifyWeChat : BaseSetUp
    {
        public Footer_SocialLinks_VerifyWeChat() : base() { }

        //--- URLs ---//
        string core_page_url = "/index.html";

        [Test, Category("Footer"), Category("Core"), Retry(2)]
        [TestCase("cn", TestName = "Core Page - Verify that the WeChat Social Link is Present in CN Locale")]
        public void Footer_SocialLinks_VerifyWeChatInCore(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + core_page_url);

            //----- Header and Footer TestPlan > Test Case Title: Verify the "wechat" social icon (IQ-9228/AL-16026) -----//

            //--- Expected Result: wechat icon displayed ---//
            test.validateElementIsPresent(driver, Elements.Footer_WeChat_SocialLink);

            //--- Action: Click on the "wechat" social icon ---//
            action.IClick(driver, Elements.Footer_WeChat_SocialLink);

            //--- Expected Result: The System displays a modal with QR code ---//
            test.validateElementIsPresent(driver, Elements.Footer_WeChat_Modal);
            test.validateElementIsPresent(driver, Elements.Footer_WeChat_Modal_QrCode_Image);
        }

        [Test, Category("Footer"), Category("NonCore"), Retry(2)]
        [TestCase("zh", TestName = "Non Core Page - Verify that the WeChat Social Link is Present in CN Locale")]
        public void Footer_SocialLinks_VerifyWeChatInNonCore(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx?locale=" + Locale);

            //----- Header and Footer TestPlan > Test Case Title: Verify the "wechat" social icon (IQ-9228/AL-16026) -----//

            //--- Expected Result: wechat icon displayed ---//
            test.validateElementIsPresent(driver, Elements.Footer_WeChat_SocialLink);

            //--- Action: Click on the "wechat" social icon ---//
            action.IClick(driver, Elements.Footer_WeChat_SocialLink);

            //--- Expected Result: The System displays a modal with QR code ---//
            test.validateElementIsPresent(driver, Elements.Footer_WeChat_Modal);
            test.validateElementIsPresent(driver, Elements.Footer_WeChat_Modal_QrCode_Image);
        }
    }
}