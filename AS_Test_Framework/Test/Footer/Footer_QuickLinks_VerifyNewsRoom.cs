﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;

namespace AS_Test_Framework.Footer
{
    [TestFixture]
    public class Footer_QuickLinks_VerifyNewsRoom : BaseSetUp
    {
        public Footer_QuickLinks_VerifyNewsRoom() : base() { }

        //--- URLs ---//
        string corePage_url = "/index.html";
        string newsRoom_page_url = "/about-adi/news-room.html";

        [Test, Category("Footer"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the News Room Quick Link is Present and Redirects to the News Room Page in EN Locale")]
        [TestCase("cn", TestName = "Verify that the News Room Quick Link is Present and Redirects to the News Room Page in CN Locale")]
        [TestCase("jp", TestName = "Verify that the News Room Quick Link is Present and Redirects to the News Room Page in JP Locale")]
        [TestCase("ru", TestName = "Verify that the News Room Quick Link is Present and Redirects to the News Room Page in RU Locale")]
        public void Footer_VerifyNewsRoomQuickLink(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + corePage_url);

            Assert.Multiple(() =>
            {
                test.validateElementIsPresent(driver, Elements.Footer_NewsRoom_QuickLink);
                if (util.CheckElement(driver, Elements.Footer_NewsRoom_QuickLink, 1))
                {
                    action.IClick(driver, Elements.Footer_NewsRoom_QuickLink);

                    test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + newsRoom_page_url);
                }
            });
        }
    }
}