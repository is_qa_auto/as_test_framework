﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;

namespace AS_Test_Framework.Footer
{
    [TestFixture]
    public class Footer_QuickLinks_VerifyQualityAndReliability : BaseSetUp
    {
        public Footer_QuickLinks_VerifyQualityAndReliability() : base() { }

        //--- URLs ---//
        string corePage_url = "/index.html";
        string qualityAndReliability_page_url = "/about-adi/quality-reliability.html";

        [Test, Category("Footer"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Quality and Reliability Quick Link is Present and Redirects to the Quality & Reliability Page in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Quality and Reliability Quick Link is Present and Redirects to the Quality & Reliability Page in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Quality and Reliability Quick Link is Present and Redirects to the Quality & Reliability Page in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Quality and Reliability Quick Link is Present and Redirects to the Quality & Reliability Page in RU Locale")]
        public void Footer_VerifyQualityAndReliabilityQuickLink(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + corePage_url);

            Assert.Multiple(() =>
            {
                test.validateElementIsPresent(driver, Elements.Footer_Qr_QuickLink);
                if (util.CheckElement(driver, Elements.Footer_Qr_QuickLink, 1))
                {
                    action.IClick(driver, Elements.Footer_Qr_QuickLink);
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + qualityAndReliability_page_url);
                }
            });
        }
    }
}