﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Threading;

namespace AS_Test_Framework.Footer
{

    [TestFixture]
    public class Footer_CopyrightInfo : BaseSetUp
    {
        public Footer_CopyrightInfo() : base() { }

        //--- URLs ---//
        string corePage_url = "/index.html";
        string dynamicPst_url = "/parametricsearch/11062";

        //--- Labels ---//
        string copyright_text = "© 1995 - " + System.DateTime.Now.ToString("yyyy") + " Analog Devices, Inc. All Rights Reserved";

        [Test, Category("Footer"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Core Page - Verify that the Copyright Info is Correct in the Footer in EN Locale")]
        [TestCase("cn", TestName = "Core Page - Verify that the Copyright Info is Correct in the Footer in CN Locale")]
        [TestCase("jp", TestName = "Core Page - Verify that the Copyright Info is Correct in the Footer in JP Locale")]
        [TestCase("ru", TestName = "Core Page - Verify that the Copyright Info is Correct in the Footer in RU Locale")]
        public void Footer_CopyrightInfo_VerifyInFooter(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + corePage_url);

            test.validateString(driver, util.GetText(driver, Elements.Footer_Copyright_Symb) + " " + util.GetText(driver, Elements.Footer_Copyright_Txt), copyright_text);
        }

        [Test, Category("Footer"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Non Core Page - Verify that the Copyright Info is Correct in the Thinner Footer in EN Locale")]
        [TestCase("cn", TestName = "Non Core Page - Verify that the Copyright Info is Correct in the Thinner Footer in CN Locale")]
        [TestCase("jp", TestName = "Non Core Page - Verify that the Copyright Info is Correct in the Thinner Footer in JP Locale")]
        [TestCase("ru", TestName = "Non Core Page - Verify that the Copyright Info is Correct in the Thinner Footer in RU Locale")]
        public void Footer_CopyrightInfo_VerifyInThinnerFooter(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + dynamicPst_url);
            Thread.Sleep(250);

            test.validateString(driver, util.GetText(driver, Elements.Footer_Copyright_Symb) + " " + util.GetText(driver, Elements.Footer_Copyright_Txt), copyright_text);
        }
    }
}