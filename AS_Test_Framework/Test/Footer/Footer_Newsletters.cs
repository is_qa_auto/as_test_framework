﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Threading;

namespace AS_Test_Framework.Footer
{

    [TestFixture]
    public class Footer_Newsletters : BaseSetUp
    {
        public Footer_Newsletters() : base() { }

        //--- Login Credentials ---//
        string username = "marvin.bebe@analog.com";
        string password = "Test_1234";

        //--- URLs ---//
        string corePage_url = "/index.html";

        [Test, Category("Footer"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Newsletters is Present in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Newsletters is Present in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Newsletters is Present in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Newsletters is Present in RU Locale")]
        public void Footer_VerifyNewsletters(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + corePage_url);

            Assert.Multiple(() =>
            {
                test.validateElementIsPresent(driver, Elements.Footer_Newsletters_Header);
                test.validateElementIsPresent(driver, Elements.Footer_Newsletters_Desc);
                test.validateElementIsPresent(driver, Elements.Footer_Newsletters_SignUp_Btn);
            });
        }

        [Test, Category("Footer"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Sign Up Button Redirects to the Login Page when Logged Out in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Sign Up Button Redirects to the Login Page when Logged Out in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Sign Up Button Redirects to the Login Page when Logged Out in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Sign Up Button Redirects to the Login Page when Logged Out in RU Locale")]
        public void Footer_Newsletters_VerifySignUpButtonWhenLoggedOut(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + corePage_url);

            Assert.Multiple(() =>
            {
                action.IClick(driver, Elements.Footer_Newsletters_SignUp_Btn);

                test.validateStringInstance(driver, driver.Url, "b2clogin.com");
            });
        }

        [Test, Category("Footer"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Sign Up Button Redirects to the myAnalog Subscriptions Page when Logged In in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Sign Up Button Redirects to the myAnalog Subscriptions Page when Logged In in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Sign Up Button Redirects to the myAnalog Subscriptions Page when Logged In in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Sign Up Button Redirects to the myAnalog Subscriptions Page when Logged In in RU Locale")]
        public void Footer_Newsletters_VerifySignUpButtonWhenLoggedIn(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + corePage_url);

            action.ILoginViaMyAnalogMegaMenu(driver, username, password);
            Thread.Sleep(1500);

            Assert.Multiple(() =>
            {
                action.IClick(driver, Elements.Footer_Newsletters_SignUp_Btn);

                test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + "/app/subscriptions");
            });
        }
    }
}