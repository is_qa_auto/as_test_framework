﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;

namespace AS_Test_Framework.Footer
{
    [TestFixture]
    public class Footer_QuickLinks_VerifyAnalogGarage : BaseSetUp
    {
        public Footer_QuickLinks_VerifyAnalogGarage() : base() { }

        //--- URLs ---//
        string corePage_url = "/index.html";
        string analogGarage_page_url = "/landing-pages/001/analog-garage.html";
        string jpSeminarInfo_page_url = "/about-adi/jp_seminar_info.html";

        [Test, Category("Footer"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Analog Garage Quick Link is Present and Redirects to the Analog Garage Page in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Analog Garage Quick Link is Present and Redirects to the Analog Garage Page in CN Locale")]
        [TestCase("jp", TestName = "Verify that the セミナー  Quick Link is Present and Redirects to the Analog Garage Page in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Analog Garage Quick Link is Present and Redirects to the Analog Garage Page in RU Locale")]
        public void Footer_VerifyAnalogGarageQuickLink(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + corePage_url);

            Assert.Multiple(() =>
            {
                //----- Header and Footer TestPlan > Test Case Title: Verify QUICK LINKS -----//

                if (Locale.Equals("jp"))
                {
                    //--- Expected Result: The Seminar hyperlink is displayed ---//
                    test.validateElementIsPresent(driver, Elements.Footer_JpSeminar_QuickLink);
                    if (util.CheckElement(driver, Elements.Footer_JpSeminar_QuickLink, 1))
                    {
                        //--- Action: Click the Seminar hyperlink in JP locale ---//
                        action.IOpenLinkInNewTab(driver, Elements.Footer_JpSeminar_QuickLink);

                        //--- Expected Result: The System displays the セミナー (Seminar) page in JP locale (IQ-7679/AL-14977) ---//
                        test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + jpSeminarInfo_page_url);
                    }
                }
                else
                {
                    //--- Expected Result: The Analog Garage hyperlink is displayed ---//
                    test.validateElementIsPresent(driver, Elements.Footer_AnalogGarage_QuickLink);
                    if (util.CheckElement(driver, Elements.Footer_AnalogGarage_QuickLink, 1))
                    {
                        //--- Action: Click the Analog Garage hyperlink in EN, CN and RU locales ---//
                        action.IOpenLinkInNewTab(driver, Elements.Footer_AnalogGarage_QuickLink);

                        //--- Expected Result: The System displays the Analog Garage page in EN, CN and RU locales (IQ-7679/AL-14977) ---//
                        test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + analogGarage_page_url);
                    }
                }
            });
        }
    }
}