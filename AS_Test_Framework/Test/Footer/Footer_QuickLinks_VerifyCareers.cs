﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;

namespace AS_Test_Framework.Footer
{
    [TestFixture]
    public class Footer_QuickLinks_VerifyCareers : BaseSetUp
    {
        public Footer_QuickLinks_VerifyCareers() : base() { }

        //--- URLs ---//
        string corePage_url = "/index.html";
        string careers_page_url = "/about-adi/careers.html";

        [Test, Category("Footer"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Careers Quick Link is Present and Redirects to the Careers Page in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Careers Quick Link is Present and Redirects to the Careers Page in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Careers Quick Link is Present and Redirects to the Careers Page in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Careers Quick Link is Present and Redirects to the Careers Page in RU Locale")]
        public void Footer_VerifyCareersQuickLink(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + corePage_url);

            Assert.Multiple(() =>
            {
                test.validateElementIsPresent(driver, Elements.Footer_Careers_QuickLink);
                if (util.CheckElement(driver, Elements.Footer_Careers_QuickLink, 1))
                {
                    action.IClick(driver, Elements.Footer_Careers_QuickLink);

                    if (Locale.Equals("cn") || Locale.Equals("zh") || Locale.Equals("jp"))
                    {
                        test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + careers_page_url);
                    }
                    else
                    {
                        test.validateStringInstance(driver, driver.Url, "careers.analog.com");
                    }
                }
            });
        }
    }
}