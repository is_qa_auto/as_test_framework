﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Threading;

namespace AS_Test_Framework.Footer
{

    [TestFixture]
    public class Footer_IcpCode : BaseSetUp
    {
        public Footer_IcpCode() : base() { }

        //--- URLs ---//
        string corePage_url = "/index.html";
        string dynamicPst_url = "/parametricsearch/11062";

        //--- Labels ---//
        string icp_code = "沪ICP备09046653号-1";

        [Test, Category("Footer"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Core Page - Verify that the ICP Code is Correct in the Footer in EN Locale")]
        [TestCase("cn", TestName = "Core Page - Verify that the ICP Code is Correct in the Footer in CN Locale")]
        [TestCase("jp", TestName = "Core Page - Verify that the ICP Code is Correct in the Footer in JP Locale")]
        [TestCase("ru", TestName = "Core Page - Verify that the ICP Code is Correct in the Footer in RU Locale")]
        public void Footer_IcpCode_VerifyInFooter(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + corePage_url);

            test.validateString(driver, util.GetText(driver, Elements.Footer_IcpCode), icp_code);
            if (Locale.Equals("cn")) {
                action.IClick(driver, Elements.Footer_IcpCode);
                test.validateScreenByUrl(driver, "https://beian.miit.gov.cn/");
            }
        }

        [Test, Category("Footer"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Non Core Page - Verify that the ICP Code is Correct in the Thinner Footer in EN Locale")]
        [TestCase("cn", TestName = "Non Core Page - Verify that the ICP Code is Correct in the Thinner Footer in CN Locale")]
        [TestCase("jp", TestName = "Non Core Page - Verify that the ICP Code is Correct in the Thinner Footer in JP Locale")]
        [TestCase("ru", TestName = "Non Core Page - Verify that the ICP Code is Correct in the Thinner Footer in RU Locale")]
        public void Footer_IcpCode_VerifyInThinnerFooter(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + dynamicPst_url);
            Thread.Sleep(250);

            test.validateString(driver, util.GetText(driver, Elements.Footer_IcpCode), icp_code);
            if (Locale.Equals("cn"))
            {
                action.IClick(driver, Elements.Footer_IcpCode);
                test.validateScreenByUrl(driver, "https://beian.miit.gov.cn/");
            }
        }
    }
}