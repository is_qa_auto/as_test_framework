﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;

namespace AS_Test_Framework.PDP
{
    [TestFixture]
    public class PDP_Overview_ProductImages : BaseSetUp
    {
        public PDP_Overview_ProductImages() : base() { }

        //--- URLs ---//
        string pdp_url = "/ad7705";
        //string pdp_url = "/ad7706";

        [Test, Category("PDP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Product Images are Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Product Images are Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Product Images are Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Product Images are Present and Working as Expected in RU Locale")]
        public void PDP_Overview_VerifyProductImages(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + pdp_url);

            //----- R6 > T1: Verify product images and schematic diagrams -----//

            //--- Expected Result: Product images and schematic diagrams under navigational menus should be displayed. ---//
            test.validateElementIsPresent(driver, Elements.PDP_ProductImage);
            test.validateElementIsPresent(driver, Elements.PDP_ProductImage_Thumbnails);

            //----- R6 > T8: Verify Zoom In icon on selected thumbnail image/schematic diagram -----//

            //--- Expected Result: The zoom in icon should be displayed for the selected image/diagram at the upper right corner of the image. ---//
            test.validateElementIsPresent(driver, Elements.PDP_Zoom_Icon);

            //--- Action: Click Zoom In icon ---//
            action.IClick(driver, Elements.PDP_Zoom_Icon);

            //--- Expected Result: A pop up screen should be shown displaying the selected image/diagram in much larger size. ---//
            test.validateElementIsPresent(driver, Elements.PDP_Product_PopUp);

            if (util.CheckElement(driver, Elements.PDP_Product_PopUp, 2))
            {
                //----- R6 > T9: Verify Download Icon on the selected image/diagram -----//

                //--- Expected Result: Download Icon must be displayed in the pop up image/diagram ---//
                test.validateElementIsPresent(driver, Elements.PDP_Product_PopUp_Download_Icon);

                //----- R6 > T10: Verify Print Icon on the selected image/diagram -----//

                //--- Expected Result: Print Icon must be displayed in the pop up image/diagram ---//
                test.validateElementIsPresent(driver, Elements.PDP_Product_PopUp_Print_Icon);

                //----- R6 > T11: Verify Close button on the selected image/diagram -----//

                //--- Expected Result: Close button must be displayed in the pop up image/diagram ---//
                test.validateElementIsPresent(driver, Elements.PDP_Product_PopUp_X_Icon);

                //--- Action: Click on Close button (X) ---//
                action.IClick(driver, Elements.PDP_Product_PopUp_X_Icon);

                //--- Expected Result: Pop up image must be closed ---//
                test.validateElementIsNotPresent(driver, Elements.PDP_Product_PopUp);
            }
        }
    }
}