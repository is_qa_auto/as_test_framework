﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.PDP
{
    [TestFixture]
    public class PDP_Overview_ReplacementPartsSection : BaseSetUp
    {
        public PDP_Overview_ReplacementPartsSection() : base() { }

        //--- URLs ---//
        string obsolete_pdp_withSuggestedReplacementParts_url = "/ad7240";
        string obsolete_pdp_url = "/ad803";
        string pdp_url = "/products/";
        string productsLanding_page_url = "/products.html";
        string crossReferenceSearch_page_url = "/cpsearch/CrossReferenceSearch.aspx";

        [Test, Category("PDP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Suggested Replacement Parts Section is Present and Working as Expected in an Obsolete PDP in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Suggested Replacement Parts Section is Present and Working as Expected in an Obsolete PDP in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Suggested Replacement Parts Section is Present and Working as Expected in an Obsolete PDP in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Suggested Replacement Parts Section is Present and Working as Expected in an Obsolete PDP in RU Locale")]
        public void PDP_Overview_VerifySuggestedReplacementPartsSectionInAnObsoletePdp(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + obsolete_pdp_withSuggestedReplacementParts_url);
            Thread.Sleep(1000);

            if (util.CheckElement(driver, Elements.ObsoletePDP_SuggestedReplacementParts_Sec, 2))
            {
                //----- PDP - Obosolete Test Plan > R4 > T1: Verify Suggested Replacement Parts -----//

                //--- Expected Result: Product image for the available Suggested Replacement Parts should be displayed. ---//
                test.validateElementIsPresent(driver, Elements.ObsoletePDP_SuggestedReplacementPart_Img);

                //--- Expected Result: Part number  for the available Suggested Replacement Parts should be displayed. ---//
                test.validateElementIsPresent(driver, Elements.ObsoletePDP_SuggestedReplacementPart_PartNo_Link);

                //--- Expected Result: Descriptions for the available Suggested Replacement Parts should be displayed. ---//
                test.validateElementIsPresent(driver, Elements.ObsoletePDP_SuggestedReplacementPart_PartNo_Desc_Txt);

                if (util.CheckElement(driver, Elements.ObsoletePDP_SuggestedReplacementPart_PartNo_Link, 2))
                {
                    //--- Action: Click Product link ---//
                    action.IOpenLinkInNewTab(driver, Elements.ObsoletePDP_SuggestedReplacementPart_PartNo_Link);
                    Thread.Sleep(3000);

                    //--- Expected Result: Redirect to Product detail page ---//
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + pdp_url);
                }
            }
        }

        [Test, Category("PDP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Replacement Parts Section is Present and Working as Expected in an Obsolete PDP in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Replacement Parts Section is Present and Working as Expected in an Obsolete PDP in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Replacement Parts Section is Present and Working as Expected in an Obsolete PDP in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Replacement Parts Section is Present and Working as Expected in an Obsolete PDP in RU Locale")]
        public void PDP_Overview_VerifyReplacementPartsSectionInAnObsoletePdp(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + obsolete_pdp_url);
            Thread.Sleep(1000);

            //----- PDP - Obosolete Test Plan > R5 > T3: Verify "To find replacement products for this part" -----//

            //--- Expected Result: "Use our Parametric Search and Selection Tables" link must be displayed below "To find replacement products for this part" ---//
            test.validateElementIsPresent(driver, Elements.ObsoletePDP_Overview_UseOurParametricSearchAndSelectionTbls_Link);

            if (util.CheckElement(driver, Elements.ObsoletePDP_Overview_UseOurParametricSearchAndSelectionTbls_Link, 2))
            {
                //--- Action: Click "Use our Parametric Search and Selection Tables" link ---//
                action.IOpenLinkInNewTab(driver, Elements.ObsoletePDP_Overview_UseOurParametricSearchAndSelectionTbls_Link);
                Thread.Sleep(4000);

                //--- Expected Result: It should redirect to Products landing page ---//
                test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + productsLanding_page_url);

                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles.First());
            }

            //--- Expected Result: "Use our Cross Reference Search" link must be displayed below  "Use our Parametric Search and Selection Tables" link ---//
            test.validateElementIsPresent(driver, Elements.ObsoletePDP_Overview_UseOurCrossReferenceSearch_Link);

            if (util.CheckElement(driver, Elements.ObsoletePDP_Overview_UseOurCrossReferenceSearch_Link, 2))
            {
                //--- Action: Click "Use our Cross Reference Search" link ---//
                action.IOpenLinkInNewTab(driver, Elements.ObsoletePDP_Overview_UseOurCrossReferenceSearch_Link);
                Thread.Sleep(3000);

                //--- Expected Result: Link redirect to ADI CROSS-REFERENCE AND OBSOLETE PART SEARCH. ---//
                test.validateStringInstance(driver, driver.Url, "analog.com" + crossReferenceSearch_page_url);
            }
        }
    }
}