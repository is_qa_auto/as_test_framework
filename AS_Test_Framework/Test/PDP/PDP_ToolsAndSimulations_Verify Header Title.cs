﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;

namespace AS_Test_Framework.PDP
{
    [TestFixture]
    public class PDP_ToolsAndSimulations_HeaderTitle : BaseSetUp
    {
        public PDP_ToolsAndSimulations_HeaderTitle() : base() { }

        //--- URLs ---//
        //string pdp_url = "/ad7705";
        string pdp_url = "/ad7706";
        string pdp_toolsAndSimulations_url = "#product-tools";

        //--- Labels ---//
        string toolsAndSimulations_txt = "Tools & Simulations";

        [Test, Category("PDP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Tools and Simulations Header Title is Present and Correct in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Tools and Simulations Header Title is Present in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Tools and Simulations Header Title is Present in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Tools and Simulations Header Title is Present in RU Locale")]
        public void PDP_ToolsAndSimulations_VerifyHeaderTitle(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + pdp_url);

            //----- R4 > T16: Verify STICKY HEADER (Navigational Menus) are clickable -----//
            if (util.CheckElement(driver, Elements.PDP_StickyHeader_ToolsAndSimulations, 2))
            {
                //--- Action: Click Tools & Simulations on sticky header (navigational menus) ---//
                action.IClick(driver, Elements.PDP_StickyHeader_ToolsAndSimulations);

                //--- Expected Result: It should anchor to Tools & Simulations section within the product detail page. ---//
                test.validateStringInstance(driver, driver.Url, pdp_toolsAndSimulations_url);
            }

            //----- R13 > T1: Verify Verify Tools & Simulations header title -----//

            //--- Expected Result: The header title "Tools & Simulations" must be displayed at the top left side of the page. ---//
            if (Locale.Equals("en"))
            {
                test.validateStringInstance(driver, toolsAndSimulations_txt, util.GetText(driver, Elements.PDP_ToolsAndSimulations_Section_Lbl));
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.PDP_ToolsAndSimulations_Section_Lbl);
            }
        }
    }
}