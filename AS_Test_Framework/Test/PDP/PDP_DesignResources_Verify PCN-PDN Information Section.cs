﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Threading;
using System.Linq;

namespace AS_Test_Framework.PDP
{
    [TestFixture]
    public class PDP_DesignResources_PcnPdnInformationSection : BaseSetUp
    {
        public PDP_DesignResources_PcnPdnInformationSection() : base() { }

        //--- Login Credentials ---//
        string username = "myanalog_t3st_111@mailinator.com";
        string password = "Test_1234";

        //--- URLs ---//
        string pdp_url = "/ad7705";
        //string pdp_url = "/ad7706";

        //--- Labels ---//
        string pcnNo_txt = "PCN #";
        string title_txt = "Title";
        string publicationDate_txt = "Publication Date";

        [Test, Category("PDP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the PCN-PDN Information Section is Present and Working as Expected when the User is Logged Out in EN Locale")]
        [TestCase("cn", TestName = "Verify that the PCN-PDN Information Section is Present and Working as Expected when the User is Logged Out in CN Locale")]
        [TestCase("jp", TestName = "Verify that the PCN-PDN Information Section is Present and Working as Expected when the User is Logged Out in JP Locale")]
        [TestCase("ru", TestName = "Verify that the PCN-PDN Information Section is Present and Working as Expected when the User is Logged Out in RU Locale")]
        public void PDP_DesignResources_VerifyPcnPdnInformationSectionWhenUserIsLoggedOut(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + pdp_url);
            Thread.Sleep(1000);

            //----- R10 > T9_1: Verify PCN-PDN Information Section in ANALOG Product that has Models that shows "PCN" in the Sample & Buy Table -----//

            //--- Expected Result: "PCN-PDN Information" section must be shown. ---//
            test.validateElementIsPresent(driver, Elements.PDP_PcnPdnInfo_Sec);

            //--- Expected Result: "Select Model" drop down menu should be available/displayed. ---//
            test.validateElementIsPresent(driver, Elements.PDP_SelectModel_Dd);

            if (util.CheckElement(driver, Elements.PDP_SelectModel_Dd, 2))
            {
                //--- Action: Click "Select Model" drop down menu  ---//
                action.IClick(driver, Elements.PDP_SelectModel_Dd);

                //--- Expected Result: List of available related ANALOG product models should be displayed. ---//
                test.validateElementIsPresent(driver, Elements.PDP_SelectModel_Dd_Menu);

                //--- Action: Select and click any ANALOG product model from the drop down menu. ---//
                action.IClick(driver, By.CssSelector("div[class*='model'] * ul[class^='dropdown-menu']>li:nth-of-type(1)>span:nth-of-type(1)"));

                //--- Expected Result: The PCN Table will be displayed. ---//
                test.validateElementIsPresent(driver, Elements.PDP_Pcn_Tbl);

                if (util.CheckElement(driver, Elements.PDP_Pcn_Tbl, 2))
                {
                    //--- Expected Result: PCN # column should be displayed as the first column of PCN table ---//
                    if (Locale.Equals("en"))
                    {
                        test.validateStringInstance(driver, pcnNo_txt, util.GetText(driver, Elements.PDP_Pcn_Tbl_PcnNo_Col_Header_Txt));
                    }
                    else
                    {
                        test.validateElementIsPresent(driver, Elements.PDP_Pcn_Tbl_PcnNo_Col_Header_Txt);
                    }

                    //--- Expected Result: Different available PCN # should be displayed in rows under PCN# column (Ex. 13_0321). ---//
                    test.validateElementIsPresent(driver, Elements.PDP_Pcn_Tbl_PcnNo_Col_PcnNos);

                    //--- Expected Result: Title column should be displayed as the second column of PCN-PDN table ---//
                    if (Locale.Equals("en"))
                    {
                        test.validateStringInstance(driver, title_txt, util.GetText(driver, Elements.PDP_Pcn_Tbl_Title_Col_Header_Txt));
                    }
                    else
                    {
                        test.validateElementIsPresent(driver, Elements.PDP_Pcn_Tbl_Title_Col_Header_Txt);
                    }

                    //--- Expected Result: A short description must be displayed for each row of PCN # under "Title" column. ---//
                    test.validateElementIsPresent(driver, Elements.PDP_Pcn_Tbl_Title_Col_Titles);

                    //--- Expected Result: Publication Date column should be displayed as the third column of PCN-PDN table ---//
                    if (Locale.Equals("en"))
                    {
                        test.validateStringInstance(driver, publicationDate_txt, util.GetText(driver, Elements.PDP_Pcn_Tbl_PublicationDate_Col_Header_Txt));
                    }
                    else
                    {
                        test.validateElementIsPresent(driver, Elements.PDP_Pcn_Tbl_PublicationDate_Col_Header_Txt);
                    }

                    //--- Expected Result: Date of publication must be displayed  for each of PCN # in the following format Month-Day-Year (Ex. Dec 6 2013) ---//
                    test.validateElementIsPresent(driver, Elements.PDP_Pcn_Tbl_PublicationDate_Col_PublicationDates);
                }

                //----- R10 > T11: Verify "Request Product/Process Change Notifications" link on PCN-PDN table for ANALOG product when user is LOGGED OUT -----//

                //--- Expected Result: "Request Product/Process Change Notifications" link at the top left side of the table should be displayed. ---//
                test.validateElementIsPresent(driver, Elements.PDP_RequestProductProcessChangeNotifs_Link);

                if (util.CheckElement(driver, Elements.PDP_RequestProductProcessChangeNotifs_Link, 2))
                {
                    //--- Action: Click on "Request Product/Process Change Notifications" link. ---//
                    action.IClick(driver, Elements.PDP_RequestProductProcessChangeNotifs_Link);

                    action.ILogin(driver, username, password);
                    action.IClick(driver, Elements.PDP_SelectModel_Dd);
                    action.IClick(driver, By.CssSelector("div[class*='model'] * ul[class^='dropdown-menu']>li:nth-of-type(1)>span:nth-of-type(1)"));
                    action.IClick(driver, Elements.PDP_RequestProductProcessChangeNotifs_Link);
                    Thread.Sleep(3000);
                    //--- Expected Result: "Sign in to myAnalog" fly out box should be shown. ---//
                    test.validateElementIsPresent(driver, Elements.PDP_PCNPDN_SaveToMyAnalog_Widget);
                }

                //----- R10 > T15: Verify Close (X) button on PCN-PDN Table for ANALOG product -----//

                //--- Expected Result: Close button (X) at the top right side of the PCN-PDN table must be shown. ---//
                test.validateElementIsPresent(driver, Elements.PDP_Pcn_Tbl_X_Btn);

                if (util.CheckElement(driver, Elements.PDP_Pcn_Tbl_X_Btn, 2))
                {
                    //--- Action: Click on Close button (X). ---//
                    action.IClick(driver, Elements.PDP_Pcn_Tbl_X_Btn);

                    //--- Expected Result: The table related to the selected product model should be closed. ---//
                    test.validateElementIsNotPresent(driver, Elements.PDP_Pcn_Tbl);
                }
            }
        }

        [Test, Category("PDP"), Category("Core"), Retry(2)]
        [TestCase("en", "Product Saved to \"myAnalog\"", TestName = "Verify that the PCN-PDN Information Section is Present and Working as Expected when the User is Logged In in EN Locale")]
        [TestCase("cn", "保存产品 \"myAnalog\"", TestName = "Verify that the PCN-PDN Information Section is Present and Working as Expected when the User is Logged In in CN Locale")]
        [TestCase("jp", "製品を保存しました： \"myAnalog\"", TestName = "Verify that the PCN-PDN Information Section is Present and Working as Expected when the User is Logged In in JP Locale")]
        [TestCase("ru", "", TestName = "Verify that the PCN-PDN Information Section is Present and Working as Expected when the User is Logged In in RU Locale")]
        public void PDP_DesignResources_VerifyPcnPdnInformationSectionWhenUserIsLoggedIn(string Locale, string SuccessMessage)
        {
            //----- R10 > T13: Verify "Request Product/Process Change Notifications" link on PCN-PDN table for ANALOG product when user is LOGGED IN -----//

            //--- myAnalog Products URL ---//
            string myAnalogProducts_page_url = null;
            if (Locale.Equals("cn") || Locale.Equals("zh"))
            {
                myAnalogProducts_page_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + "zh/app/products";
            }
            else
            {
                myAnalogProducts_page_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/app/products";
            }

            action.Navigate(driver, myAnalogProducts_page_url);

            action.ILogin(driver, username, password);

            action.IRemoveAllSavedProductsInMyAnalog(driver, username, password);

            //--- Action: Open any ANALOG product with Design Resources section ---//
            driver.Navigate().GoToUrl(Configuration.Env_Url + Locale + pdp_url);
            action.GivenIAcceptCookies(driver);
            Thread.Sleep(1000);

            if (util.CheckElement(driver, Elements.PDP_SelectModel_Dd, 2))
            {
                string productNo = util.GetText(driver, Elements.PDP_ProductNo_Txt);

                //--- Action: Click "Select Model" drop down menu  ---//
                action.IClick(driver, Elements.PDP_SelectModel_Dd);
                Thread.Sleep(1000);

                //--- Action: Select and click any ANALOG product model from the drop down menu. ---//
                action.IClick(driver, By.CssSelector("div[class*='model'] * ul[class^='dropdown-menu']>li:nth-of-type(1)>span:nth-of-type(1)"));

                if (util.CheckElement(driver, Elements.PDP_RequestProductProcessChangeNotifs_Link, 2))
                {
                    //--- Action: Click on "Request Product/Process Change Notifications" link. ---//
                    action.IClick(driver, Elements.PDP_RequestProductProcessChangeNotifs_Link);

                    //--- Expected Result: "Sign in to myAnalog" fly out box should be shown. ---//
                    test.validateElementIsPresent(driver, Elements.PDP_SaveToMyAnalog_Widget);

                    if (!Locale.Equals("ru"))
                    {
                        //--- Action: Click on "Save to myAnalog" link ---//
                        action.IClick(driver, Elements.PDP_SaveToMyAnalog_Widget_SaveProduct_Btn);

                        test.validateStringIsCorrect(driver, Elements.MyAnalog_Widget_SaveToAnalog_Success, SuccessMessage);

                        //--- Expected Result: The selected product model should be saved to user's My Analog account. ---//
                        action.IClick(driver, Elements.MyAnalog_Widget_ViewSavedLink);
                        driver.SwitchTo().Window(driver.WindowHandles.Last());

                        test.validateStringIsCorrect(driver, By.CssSelector("section[class='product']:nth-of-type(1) * a"), productNo.ToUpper());
                    }
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.PDP_RequestProductProcessChangeNotifs_Link);
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.PDP_SelectModel_Dd);
            }
        }
    }
}