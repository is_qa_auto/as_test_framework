﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;

namespace AS_Test_Framework.PDP
{
    [TestFixture]
    public class PDP_DesignResources_HeaderTitle : BaseSetUp
    {
        public PDP_DesignResources_HeaderTitle() : base() { }

        //--- URLs ---//
        string pdp_url = "/ad7705";
        //string pdp_url = "/ad7706";
        string pdp_designResources_url = "#product-quality";

        //--- Labels ---//
        string designResources_txt = "Design Resources";

        [Test, Category("PDP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Design Resources Header Title is Present and Correct in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Design Resources Header Title is Present in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Design Resources Header Title is Present in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Design Resources Header Title is Present in RU Locale")]
        public void PDP_DesignResources_VerifyHeaderTitle(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + pdp_url);

            //----- R4 > T16: Verify STICKY HEADER (Navigational Menus) are clickable -----//
            if (util.CheckElement(driver, Elements.PDP_StickyHeader_DesignResources, 2))
            {
                //--- Action: Click Design Resources on sticky header (navigational menus) ---//
                action.IClick(driver, Elements.PDP_StickyHeader_DesignResources);

                //--- Expected Result: It should anchor to Design Resources section within the product detail page. ---//
                test.validateStringInstance(driver, driver.Url, pdp_designResources_url);
            }

            //----- R10 > T1: Verify Design Resources header title -----//

            //--- Expected Result: The header title "Design Resources" must be displayed at the top left portion of the page. ---//
            if (Locale.Equals("en"))
            {
                test.validateStringInstance(driver, designResources_txt, util.GetText(driver, Elements.PDP_DesignResources_Section_Lbl));
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.PDP_DesignResources_Section_Lbl);
            }

            //--- Expected Result: Description under "Design Resources" should be in paragraph format. ---//
            test.validateElementIsPresent(driver, Elements.PDP_DesignResources_Section_Desc_Txt);
        }
    }
}