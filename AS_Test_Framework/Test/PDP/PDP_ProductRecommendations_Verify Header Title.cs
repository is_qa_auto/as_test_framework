﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;

namespace AS_Test_Framework.PDP
{
    [TestFixture]
    public class PDP_ProductRecommendations_HeaderTitle : BaseSetUp
    {
        public PDP_ProductRecommendations_HeaderTitle() : base() { }

        //--- URLs ---//
        string pdp_url = "/ad7705";
        //string pdp_url = "/ad7706";
        string pdp_productRecommendations_url = "#product-recommendations";

        [Test, Category("PDP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Product Recommendations Header Title is Present and Correct in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Product Recommendations Header Title is Present in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Product Recommendations Header Title is Present in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Product Recommendations Header Title is Present in RU Locale")]
        public void PDP_ProductRecommendations_VerifyHeaderTitle(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + pdp_url);

            //----- R4 > T16: Verify STICKY HEADER (Navigational Menus) are clickable -----//
            if (util.CheckElement(driver, Elements.PDP_StickyHeader_ProductRecommendations, 2))
            {
                //--- Action: Click Product Recommendation on sticky header (navigational menus) ---//
                action.IClick(driver, Elements.PDP_StickyHeader_ProductRecommendations);

                //--- Expected Result: It should anchor to Product Recommendation section within the product detail page. ---//
                test.validateStringInstance(driver, driver.Url, pdp_productRecommendations_url);
            }
        }
    }
}