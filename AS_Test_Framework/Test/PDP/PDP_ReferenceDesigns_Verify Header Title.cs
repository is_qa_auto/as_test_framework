﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Threading;

namespace AS_Test_Framework.PDP
{
    [TestFixture]
    public class PDP_ReferenceDesigns_HeaderTitle : BaseSetUp
    {
        public PDP_ReferenceDesigns_HeaderTitle() : base() { }

        //--- URLs ---//
        string pdp_url = "/ad7960";
        string pdp_referenceDesigns_url = "#product-designs";

        //--- Labels ---//
        string referenceDesigns_txt = "Reference Designs";

        [Test, Category("PDP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Reference Designs Header Title is Present and Correct in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Reference Designs Header Title is Present in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Reference Designs Header Title is Present in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Reference Designs Header Title is Present in RU Locale")]
        public void PDP_ReferenceDesigns_VerifyHeaderTitle(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + pdp_url);
            Thread.Sleep(2000);

            //----- R4 > T16: Verify STICKY HEADER (Navigational Menus) are clickable -----//
            if (util.CheckElement(driver, Elements.PDP_StickyHeader_ReferenceDesigns, 2))
            {
                //--- Action: Click Reference Designs on sticky header (navigational menus) ---//
                action.IClick(driver, Elements.PDP_StickyHeader_ReferenceDesigns);

                //--- Expected Result: It should anchor to Reference Designs section within the product detail page. ---//
                test.validateStringInstance(driver, driver.Url, pdp_referenceDesigns_url);
            }

            //----- R16 > T1: Verify Reference Designs header title -----//

            int referenceDesigns_count = util.GetCount(driver, Elements.PDP_ReferenceDesigns);

            //--- Expected Result: There should be a counter displayed beside "Reference Designs" title enclosed by parenthesis showing the number of available related reference design model(s). ---//
            if (Locale.Equals("en"))
            {
                test.validateStringInstance(driver, referenceDesigns_txt + " (" + referenceDesigns_count + ")", util.GetText(driver, Elements.PDP_ReferenceDesigns_Section_Lbl));
            }
            else
            {
                test.validateStringInstance(driver, util.GetText(driver, Elements.PDP_ReferenceDesigns_Section_Lbl), "(" + referenceDesigns_count + ")");
            }
        }
    }
}