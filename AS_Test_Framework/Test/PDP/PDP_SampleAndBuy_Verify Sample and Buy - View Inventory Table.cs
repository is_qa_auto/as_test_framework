﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.PDP
{
    [TestFixture]
    public class PDP_SampleAndBuy_SampleAndBuyViewInventoryTable : BaseSetUp
    {
        public PDP_SampleAndBuy_SampleAndBuyViewInventoryTable() : base() { }

        //--- URLs ---//
        string pdp_url1 = "/ad7705";
        string pdp_url2 = "/ad7706";
        string pdp_withAutomotiveModels_url = "/ad1933";

        //--- Labels ---//
        string macnicaMouser_txt = "Macnica-Mouser";
        string excelpoint_txt = "EXCELPOINT";

        [Test, Category("PDP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Sample and Buy - View Inventory Table is Present and Working as Expected when the Selected Country is United States in EN Locale", Category = "Smoke_ADIWeb")]
        [TestCase("cn", TestName = "Verify that the Sample and Buy - View Inventory Table is Present and Working as Expected when the Selected Country is United States in CN Locale", Category = "Smoke_ADIWeb")]
        [TestCase("jp", TestName = "Verify that the Sample and Buy - View Inventory Table is Present and Working as Expected when the Selected Country is United States in JP Locale", Category = "Smoke_ADIWeb")]
        [TestCase("ru", TestName = "Verify that the Sample and Buy - View Inventory Table is Present and Working as Expected when the Selected Country is United States in RU Locale")]
        public void PDP_SampleAndBuy_VerifySampleAndBuyViewInventoryTableWhenSelectedCountryisUnitedStates(string Locale)
        {
            string initial_steps = "1. Go to " + Configuration.Env_Url + Locale + pdp_withAutomotiveModels_url
                            +  "<br>2. Click Sample & Buy tab in sticky header";

            string scenario = "";

            action.INavigate(driver, Configuration.Env_Url + Locale + pdp_withAutomotiveModels_url);

            //----- R17 > T5_1: Verify "Check Inventory" button under Sample & Buy section in ANALOG product (AL-11355) -----//

            //--- Expected Result: Check Inventory button should be displayed beside Select a country dropdown menu in ANALOG product. ---//
            scenario = "Verify that check inventory button should be displayed in sample & buy table";
            test.validateElementIsPresentv2(driver, Elements.PDP_SampleAndBuy_Tbl_CheckInventory_Btn, scenario, initial_steps);

            if (util.CheckElement(driver, Elements.PDP_SampleAndBuy_Tbl_CheckInventory_Btn, 2))
            {
                bool automotive_symbol = false;
                if (util.CheckElement(driver, Elements.PDP_SampleAndBuy_Tbl_Automotive_Symbol, 2))
                {
                    automotive_symbol = true;
                }

                //--- Action: Select  United States in the "Select Country" dropdown ---//
                initial_steps = initial_steps + "<br>3. Click Country Dropdown";
                action.IClick(driver, Elements.PDP_SampleAndBuy_Tbl_SelectACountry_Dd_Btn);

                initial_steps = initial_steps + "<br>4. Select US as a Country";
                action.IClick(driver, By.CssSelector("div[id='product-samplebuy']>div>div:nth-of-type(1) * div[class$='country-filter'] * [data-id='US']"));

                //--- Action: Click Check Inventory button ---//
                initial_steps = initial_steps + "<br>5. Click Check Inventory button";
                action.IClick(driver, Elements.PDP_SampleAndBuy_Tbl_CheckInventory_Btn);

                //--- Expected Result: New table column should slide-in ---//
                scenario = "Verify that View Inventory table will slide-in";
                test.validateElementIsPresentv2(driver, Elements.PDP_SampleAndBuy_ViewInventory_Tbl, scenario, initial_steps);

                if (util.CheckElement(driver, Elements.PDP_SampleAndBuy_ViewInventory_Tbl, 2))
                {
                    if (automotive_symbol == true)
                    {
                        //----- R17 > T22: Verify Sample and Buy for Automotive Models (AL-10468) -----//

                        //--- Expected Result: The Automotive Symbol should remain displayed. ---//
                        scenario = "Verify that product with automatotive symbol will still be displayed in view inventory table";
                        test.validateElementIsPresentv2(driver, Elements.PDP_SampleAndBuy_ViewInventory_Tbl_Automotive_Symbol, scenario, initial_steps);
                    }

                    //----- R17 > T29: Verify Sample and Buy for LTC product with Purchase button (AL-12088) -----//

                    int viewInventory_tbl_row_count = util.GetCount(driver, By.CssSelector("div[id='product-samplebuy'] * div[class$='model table-responsive grid-clone view-inventory']>table>tbody>tr"));

                    for (int locator_ctr = 1; locator_ctr <= viewInventory_tbl_row_count; locator_ctr++)
                    {
                        if (util.CheckElement(driver, By.CssSelector("div[id='product-samplebuy'] * div[class$='model table-responsive grid-clone view-inventory']>table>tbody>tr:nth-of-type(" + locator_ctr + ")>td[class^='distributor']>a[class^='btn']"), 2))
                        {
                            //--- Action: Get the Model and Package ---//
                            string model_name = util.ReturnAttribute(driver, By.CssSelector("div[id='product-samplebuy'] * div[class$='model table-responsive grid-clone view-inventory']>table>tbody>tr:nth-of-type(" + locator_ctr + ")"), "data-id");
                            string package_description = util.ReturnAttribute(driver, By.CssSelector("div[id='product-samplebuy'] * div[class$='model table-responsive']>table>tbody>tr:nth-of-type(" + locator_ctr + ")>td[data-id='Package']"), "data-value");

                            //--- Expected Result: "Add to cart" button should be displayed in models with purchase button ---//
                            scenario = "Verify that \"add to cart\" button will be displayed in models with purchase button";
                            test.validateElementIsPresentv2(driver, By.CssSelector("div[id='product-samplebuy'] * div[class$='model table-responsive grid-clone view-inventory']>table>tbody>tr:nth-of-type(" + locator_ctr + ")>td[data-id='AnalogDevices']>a[class^='btn']"), scenario, initial_steps);

                            //--- Action: Click the "Add to cart" button ---//
                            initial_steps = initial_steps + "<br>6. Click add to cart button for model no. \"" + model_name + "\"";
                            action.IOpenLinkInNewTab(driver, By.CssSelector("div[id='product-samplebuy'] * div[class$='model table-responsive grid-clone view-inventory']>table>tbody>tr:nth-of-type(" + locator_ctr + ")>td[data-id='AnalogDevices']>a[class^='btn']"));

                            if (util.CheckElement(driver, Elements.Shopping_Cart_SelectShippingCountry_ErrorMsg, 2))
                            {
                                action.IClick(driver, Elements.Shopping_Cart_Country_Dropdown);
                                action.IClick(driver, By.CssSelector("li[value='US']>a"));
                                initial_steps = initial_steps + "<br>7. Once user is redirect to shopping cart and no country was selected, Select US as a country";
                            }

                            Thread.Sleep(1000);

                            //--- Expected Result: Shopping Cart  page should be displayed with the selected model added in the Your cart section---//
                            scenario = "Verify that selected model in PDP will be added in the shopping once user clicks on add to cart button";
                            test.validateStringIsCorrectv2(driver, Elements.Shopping_Cart_AddedModel, model_name, scenario , initial_steps);

                            //----- ECommerce TestPlan > Test Case Title: Verify Package Description on Shopping Cart Page (IQ-9070/AL-16054) -----//

                            //--- Expected Result: The Package Description of the Model is the same as the one shown in the Shopping Cart page ---//
                            scenario = "Verify that Package description of the model listed in the PDP is the same as the one shown in the Shopping Cart page";
                            test.validateStringIsCorrectv2(driver, Elements.Shopping_Cart_Package_Value, package_description,scenario, initial_steps);

                            driver.Close();
                            driver.SwitchTo().Window(driver.WindowHandles.First());
                            break;
                        }
                        
                        if (locator_ctr == viewInventory_tbl_row_count)
                        {
                            test.validateElementIsPresent(driver, By.CssSelector("div[id='product-samplebuy'] * div[class$='model table-responsive grid-clone view-inventory']>table>tbody>tr:nth-of-type(" + (locator_ctr + 1) + ")>td[data-id='AnalogDevices']>a[class^='btn']"));
                        }
                    }

                    //--- Expected Result: Back button should be enabled. ---//
                    initial_steps = "1. Go to " + Configuration.Env_Url + Locale + pdp_withAutomotiveModels_url
                              + "<br>2. Click Sample & Buy tab in sticky header"
                              + "<br>3. Click Country Dropdown"
                              + "<br>4. Select US as a country"
                              + "<br>5. Click Check Inventory";

                    scenario = "Verify that back button should be enabled";
                    test.validateElementIsEnabledv2(driver, Elements.PDP_SampleAndBuy_Tbl_Back_Btn,scenario, initial_steps);

                    if (driver.FindElement(Elements.PDP_SampleAndBuy_Tbl_Back_Btn).Enabled)
                    {
                        //--- Action: Click Back button ---//

                        action.IClick(driver, Elements.PDP_SampleAndBuy_Tbl_Back_Btn);

                        //--- Expected Result: Original table column should slide back. ---//
                        scenario = "Verify that original table will slide back when user clicks on back button";
                        test.validateElementIsPresentv2(driver, Elements.PDP_SampleAndBuy_Tbl, scenario, initial_steps + "<br>6. Click back button");
                    }
                }
            }
        }

        [Test, Category("PDP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Sample and Buy - View Inventory Table is Present and Working as Expected when the Selected Country is Japan in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Sample & Buy - View Inventory Table is Present and Working as Expected when the Selected Country is Japan in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Sample & Buy - View Inventory Table is Present and Working as Expected when the Selected Country is Japan in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Sample & Buy - View Inventory Table is Present and Working as Expected when the Selected Country is Japan in RU Locale")]
        public void PDP_SampleAndBuy_VerifySampleAndBuyViewInventoryTableWhenSelectedCountryisJapan(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + pdp_url2);

            if (util.CheckElement(driver, Elements.PDP_SampleAndBuy_Tbl_CheckInventory_Btn, 2))
            {
                //----- R17 > T28: Verify Inventory change in Japanese (AL-10703) -----//

                //--- Action: Select Japan as country in the "Select Country" dropdown ---//
                action.IClick(driver, Elements.PDP_SampleAndBuy_Tbl_SelectACountry_Dd_Btn);
                action.IClick(driver, By.CssSelector("div[id='product-samplebuy']>div>div:nth-of-type(1) * div[class$='country-filter'] * [data-id='JP']"));

                //--- Action: Click "Check Inventory" button ---//
                action.IClick(driver, Elements.PDP_SampleAndBuy_Tbl_CheckInventory_Btn);

                if (util.CheckElement(driver, Elements.PDP_SampleAndBuy_ViewInventory_Tbl, 2))
                {
                    int viewInventory_tbl_col_count = util.GetCount(driver, By.CssSelector("div[id='product-samplebuy'] * div[class$='model table-responsive grid-clone view-inventory']>table>thead>tr>th"));

                    for (int locator_ctr = 1; locator_ctr <= viewInventory_tbl_col_count; locator_ctr++)
                    {
                        if (util.GetText(driver, By.CssSelector("div[id='product-samplebuy'] * div[class$='model table-responsive grid-clone view-inventory']>table>thead>tr>th:nth-last-of-type(" + locator_ctr + ")")).Equals(macnicaMouser_txt))
                        {
                            //--- Expected Result: "Macnica-Mouser" column should be displayed ---//
                            test.validateStringIsCorrect(driver, By.CssSelector("div[id='product-samplebuy'] * div[class$='model table-responsive grid-clone view-inventory']>table>thead>tr>th:nth-last-of-type(" + locator_ctr + ")"), macnicaMouser_txt);

                            break;
                        }

                        if (locator_ctr == viewInventory_tbl_col_count)
                        {
                            test.validateElementIsPresent(driver, By.CssSelector("div[id='product-samplebuy'] * div[class$='model table-responsive grid-clone view-inventory']>table>thead>tr>th:nth-last-of-type(" + (locator_ctr + 1) + ")"));
                        }
                    }
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.PDP_SampleAndBuy_ViewInventory_Tbl);
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.PDP_SampleAndBuy_Tbl_CheckInventory_Btn);
            }
        }

        [Test, Category("PDP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Sample and Buy - View Inventory Table is Present and Working as Expected when the Selected Country is China in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Sample & Buy - View Inventory Table is Present and Working as Expected when the Selected Country is China in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Sample & Buy - View Inventory Table is Present and Working as Expected when the Selected Country is China in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Sample & Buy - View Inventory Table is Present and Working as Expected when the Selected Country is China in RU Locale")]
        public void PDP_SampleAndBuy_VerifySampleAndBuyViewInventoryTableWhenSelectedCountryisChina(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + pdp_url1);

            if (util.CheckElement(driver, Elements.PDP_SampleAndBuy_Tbl_CheckInventory_Btn, 2))
            {
                //----- R17 > T30: Verify that the ExcelPoint column is available for China country (IQ-4951/AL-8785) -----//

                //--- Action: Select China in country dropdown box ---//
                action.IClick(driver, Elements.PDP_SampleAndBuy_Tbl_SelectACountry_Dd_Btn);
                action.IClick(driver, By.CssSelector("div[id='product-samplebuy']>div>div:nth-of-type(1) * div[class$='country-filter'] * [data-id='CN']"));

                //--- Action: Click Check Inventory button ---//
                action.IClick(driver, Elements.PDP_SampleAndBuy_Tbl_CheckInventory_Btn);

                if (util.CheckElement(driver, Elements.PDP_SampleAndBuy_ViewInventory_Tbl, 2))
                {
                    int viewInventory_tbl_col_count = util.GetCount(driver, By.CssSelector("div[id='product-samplebuy'] * div[class$='model table-responsive grid-clone view-inventory']>table>thead>tr>th"));

                    for (int locator_ctr = 1; locator_ctr <= viewInventory_tbl_col_count; locator_ctr++)
                    {
                        if (util.GetText(driver, By.CssSelector("div[id='product-samplebuy'] * div[class$='model table-responsive grid-clone view-inventory']>table>thead>tr>th:nth-last-of-type(" + locator_ctr + ")")).Equals(excelpoint_txt))
                        {
                            //--- Expected Result: ExcelPoint column is present ---//
                            test.validateStringIsCorrect(driver, By.CssSelector("div[id='product-samplebuy'] * div[class$='model table-responsive grid-clone view-inventory']>table>thead>tr>th:nth-last-of-type(" + locator_ctr + ")"), excelpoint_txt);

                            break;
                        }

                        if (locator_ctr == viewInventory_tbl_col_count)
                        {
                            test.validateElementIsPresent(driver, By.CssSelector("div[id='product-samplebuy'] * div[class$='model table-responsive grid-clone view-inventory']>table>thead>tr>th:nth-last-of-type(" + (locator_ctr + 1) + ")"));
                        }
                    }
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.PDP_SampleAndBuy_ViewInventory_Tbl);
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.PDP_SampleAndBuy_Tbl_CheckInventory_Btn);
            }
        }

        [Test, Category("PDP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Sample and Buy - View Inventory Table is Present and Working as Expected when the Selected Country is Hong Kong in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Sample & Buy - View Inventory Table is Present and Working as Expected when the Selected Country is Hong Kong in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Sample & Buy - View Inventory Table is Present and Working as Expected when the Selected Country is Hong Kong in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Sample & Buy - View Inventory Table is Present and Working as Expected when the Selected Country is Hong Kong in RU Locale")]
        public void PDP_SampleAndBuy_VerifySampleAndBuyViewInventoryTableWhenSelectedCountryisHongKong(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + pdp_url2);

            if (util.CheckElement(driver, Elements.PDP_SampleAndBuy_Tbl_CheckInventory_Btn, 2))
            {
                //----- R17 > T30: Verify that the ExcelPoint column is available for Hongkong country (IQ-4951/AL-8785) -----//

                //--- Action: Select Hongkong in country dropdown box ---//
                action.IClick(driver, Elements.PDP_SampleAndBuy_Tbl_SelectACountry_Dd_Btn);
                action.IClick(driver, By.CssSelector("div[id='product-samplebuy']>div>div:nth-of-type(1) * div[class$='country-filter'] * [data-id='HK']"));

                //--- Action: Click Check Inventory button ---//
                action.IClick(driver, Elements.PDP_SampleAndBuy_Tbl_CheckInventory_Btn);

                if (util.CheckElement(driver, Elements.PDP_SampleAndBuy_ViewInventory_Tbl, 2))
                {
                    int viewInventory_tbl_col_count = util.GetCount(driver, By.CssSelector("div[id='product-samplebuy'] * div[class$='model table-responsive grid-clone view-inventory']>table>thead>tr>th"));

                    for (int locator_ctr = 1; locator_ctr <= viewInventory_tbl_col_count; locator_ctr++)
                    {
                        if (util.GetText(driver, By.CssSelector("div[id='product-samplebuy'] * div[class$='model table-responsive grid-clone view-inventory']>table>thead>tr>th:nth-last-of-type(" + locator_ctr + ")")).Equals(excelpoint_txt))
                        {
                            //--- Expected Result: ExcelPoint column is present ---//
                            test.validateStringIsCorrect(driver, By.CssSelector("div[id='product-samplebuy'] * div[class$='model table-responsive grid-clone view-inventory']>table>thead>tr>th:nth-last-of-type(" + locator_ctr + ")"), excelpoint_txt);

                            break;
                        }

                        if (locator_ctr == viewInventory_tbl_col_count)
                        {
                            test.validateElementIsPresent(driver, By.CssSelector("div[id='product-samplebuy'] * div[class$='model table-responsive grid-clone view-inventory']>table>thead>tr>th:nth-last-of-type(" + (locator_ctr + 1) + ")"));
                        }
                    }
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.PDP_SampleAndBuy_ViewInventory_Tbl);
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.PDP_SampleAndBuy_Tbl_CheckInventory_Btn);
            }
        }

        [Test, Category("PDP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Sample and Buy - View Inventory Table is Present and Working as Expected when the Selected Country is Benin in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Sample and Buy - View Inventory Table is Present and Working as Expected when the Selected Country is Benin in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Sample and Buy - View Inventory Table is Present and Working as Expected when the Selected Country is Benin in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Sample and Buy - View Inventory Table is Present and Working as Expected when the Selected Country is Benin in RU Locale")]
        public void PDP_SampleAndBuy_VerifySampleAndBuyViewInventoryTableWhenSelectedCountryisBenin(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + pdp_url1);

            if (util.CheckElement(driver, Elements.PDP_SampleAndBuy_Tbl_CheckInventory_Btn, 2))
            {
                //----- R17 > T31: Verify Country where product is not available for Sample/Purcahse (IQ-5503/AL-8078) -----//

                //--- Action: Select country (e.g. Benin) ---//
                action.IClick(driver, Elements.PDP_SampleAndBuy_Tbl_SelectACountry_Dd_Btn);
                action.IClick(driver, By.CssSelector("div[id='product-samplebuy']>div>div:nth-of-type(1) * div[class$='country-filter'] * [data-id='BJ']"));

                //--- Action: CClick Check inventory---//
                action.IClick(driver, Elements.PDP_SampleAndBuy_Tbl_CheckInventory_Btn);

                if (util.CheckElement(driver, Elements.PDP_SampleAndBuy_ViewInventory_Tbl, 2))
                {
                    //--- Expected Result: Add to cart button should not display upon click check inventory. ---//
                    test.validateElementIsNotPresent(driver, Elements.PDP_SampleAndBuy_ViewInventory_Tbl_AddToCart_Btn);

                    //--- Expected Result: "Contact ADI" should display. ---//
                    test.validateElementIsPresent(driver, Elements.PDP_SampleAndBuy_ViewInventory_Tbl_ContactAdi_Link);
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.PDP_SampleAndBuy_ViewInventory_Tbl);
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.PDP_SampleAndBuy_Tbl_CheckInventory_Btn);
            }
        }
    }
}