﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.PDP
{
    [TestFixture]
    public class PDP_EvaluationKits_EvalboardProductFlyOutBox : BaseSetUp
    {
        public PDP_EvaluationKits_EvalboardProductFlyOutBox() : base() { }

        //--- URLs ---//
        string pdp_url = "/ad7705";
        //string pdp_url = "/ad7706";
        string evalBoard_page_url = "/design-center/evaluation-hardware-and-software/evaluation-boards-kits/";

        [Test, Category("PDP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Evalboard Product Fly Out Box is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Evalboard Product Fly Out Box is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Evalboard Product Fly Out Box is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Evalboard Product Fly Out Box is Present and Working as Expected in RU Locale")]
        public void PDP_EvaluationKits_VerifyEvalboardProductFlyOutBox(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + pdp_url);
            Thread.Sleep(1000);

            //----- R9 > T8: Verify Evalboard product fly out box -----//

            //--- Action: Select and click any evalboard item. ---//
            if (!util.CheckElement(driver, Elements.PDP_EvaluationKits_FlyOut, 2))
            {
                action.IClick(driver, Elements.PDP_EvaluationKits);
            }

            //--- Expected Result: A fly out box should be shown displaying the selected evalboard item with more detail information. ---//
            test.validateElementIsPresent(driver, Elements.PDP_EvaluationKits_FlyOut);

            if (util.CheckElement(driver, Elements.PDP_EvaluationKits_FlyOut, 2))
            {
                //--- Expected Result: The following details/information must be displayed in fly out box: Evalboard images, Evalboard product model/part number, "View Detailed Evaluation Kit Information" button, Evalboard product description ---//
                test.validateElementIsPresent(driver, Elements.PDP_EvaluationKits_FlyOut_Thumbnail_Imgs);
                test.validateElementIsPresent(driver, Elements.PDP_EvaluationKits_FlyOut_Img);
                test.validateElementIsPresent(driver, Elements.PDP_EvaluationKits_FlyOut_PartNo_Txt);
                test.validateElementIsPresent(driver, Elements.PDP_EvaluationKits_FlyOut_ViewDetailedEvalKitInfo_Btn);
                test.validateElementIsPresent(driver, Elements.PDP_EvaluationKits_FlyOut_ProductDesc_Txt);

                //----- R9 > T9: Verify Evalboard 3 product images in fly out box -----//
                if (util.CheckElement(driver, Elements.PDP_EvaluationKits_FlyOut_Thumbnail_Imgs, 2))
                {
                    //--- Expected Result: There should be maximum of 3 thumbnail images displayed at the top left side part of the fly out box. ---//
                    test.validateCountIsLessOrEqual(driver, util.GetCount(driver, Elements.PDP_EvaluationKits_FlyOut_Thumbnail_Imgs), 3);

                    //--- Expected Result: User can select any of the 3 thumbnail images. ---//
                    action.IClick(driver, By.CssSelector("div[style=''] * div[class='evaluation-kits-section'] * div[class='thumb-item']:nth-last-of-type(1) * img"));
                    Thread.Sleep(5000);

                    //--- Expected Result: The selected thumbnail image should be displayed in larger size beside the thumbnail images. ---//
                    test.validateElementIsPresent(driver, Elements.PDP_EvaluationKits_FlyOut_Img);
                }

                ////----- R9 > T15: Verify Resources section in fly out box -----//
                //if (util.CheckElement(driver, Elements.PDP_EvaluationKits_FlyOut_Resources_Link, 2))
                //{
                //    //--- Action: Click "Production" Analog product status link ---//
                //    action.IOpenLinkInNewTab(driver, Elements.PDP_EvaluationKits_FlyOut_Resources_Link);

                //    //--- Expected Result: Page should redirect to related PDF file. ---// --> The Script is timing out after clicking the Link.
                //    test.validateStringInstance(driver, driver.Url, ".pdf");

                //    driver.Close();
                //    driver.SwitchTo().Window(driver.WindowHandles.First());
                //}

                //----- R18 > T1: Verify Companion Eval Kits and Demo Boards is Displayed for AD Products (AL-10512) -----//
                if (util.CheckElement(driver, Elements.PDP_EvaluationKits_FlyOut_ViewDetailedEvalKitInfo_Btn, 2))
                {
                    //--- Action: Click the button "View Detailed Evaluation Kit Information" ---//
                    action.IOpenLinkInNewTab(driver, Elements.PDP_EvaluationKits_FlyOut_ViewDetailedEvalKitInfo_Btn);
                    Thread.Sleep(2000);

                    //--- Expected Result: The page will redirect to an Eval Board Page ---//
                    test.validateStringInstance(driver, driver.Url, Locale + evalBoard_page_url);

                    driver.Close();
                    driver.SwitchTo().Window(driver.WindowHandles.First());
                }

                //----- R9 > T18: Verify Close (X) button in fly out box -----//

                //--- Expected Result: There should be close button (X) at the top right corner of the fly out box. ---//
                test.validateElementIsPresent(driver, Elements.PDP_EvaluationKits_FlyOut_X_Btn);

                if (util.CheckElement(driver, Elements.PDP_EvaluationKits_FlyOut_X_Btn, 2))
                {
                    //--- Action: Click the close button (X). ---//
                    action.IClick(driver, Elements.PDP_EvaluationKits_FlyOut_X_Btn);

                    //--- Expected Result: The pop up box should be closed. ---//
                    test.validateElementIsNotPresent(driver, Elements.PDP_EvaluationKits_FlyOut);
                }
            }
        }
    }
}