﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Linq;
using System.Threading;
using OpenQA.Selenium;
using System;
using System.Text.RegularExpressions;

namespace AS_Test_Framework.PDP
{
    [TestFixture]
    public class PDP_Overview_RadioVerseSection : BaseSetUp
    {
        public PDP_Overview_RadioVerseSection() : base() { }

        //--- URLs ---//
        string pdp_withProductCategories_url = "/ad7705";
        //string pdp_withProductCategories_url = "/ad7706";
        string pdp_withMarketsAndTechnology_url = "/ad7960";

        //--- Labels ---//
        string productCategories_txt = "Product Categories";
        string marketsAndTechnology_txt = "Markets and Technologies";

        [Test, Category("PDP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Product Categories is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Product Categories is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Product Categories is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Product Categories is Present and Working as Expected in RU Locale")]
        public void PDP_Overview_VerifyProductCategories(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + pdp_withProductCategories_url);
            Thread.Sleep(1000);

            //----- R8 > T7: Verify Product Categories in ANALOG product -----//

            //--- Expected Result: Product Categories must be displayed under RadioVerse logo in ANALOG product ---//
            if (Locale.Equals("en"))
            {
                test.validateStringInstance(driver, productCategories_txt, util.GetText(driver, Elements.PDP_ProductCategories_Txt));
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.PDP_ProductCategories_Txt);
            }

            //--- Expected Result: Product category links must be displayed showing the list of category/ies where the product part number belongs. ---//
            test.validateElementIsPresent(driver, Elements.PDP_ProductCategory_Link);

            if (util.CheckElement(driver, Elements.PDP_ProductCategory_Link, 2))
            {
                //--- Action: Click on any product category links ---//
                action.IClick(driver, Elements.PDP_ProductCategory_Link);

                if (driver.Url.Contains(Locale + pdp_withProductCategories_url))
                {
                    driver.SwitchTo().Window(driver.WindowHandles.Last());

                    //--- Expected Result: Link should redirect to its respective product category landing pages ---//
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + "/products/");

                    driver.Close();
                    driver.SwitchTo().Window(driver.WindowHandles.First());
                }
                else
                {
                    //--- Expected Result: Link should redirect to its respective product category landing pages ---//
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + "/products/");

                    driver.Navigate().Back();
                    Thread.Sleep(1000);
                }
            }

            if (util.CheckElement(driver, Elements.PDP_ProductSubcategory_Link, 2))
            {
                //--- Action: Click on any product subcategory links ---//
                action.IClick(driver, Elements.PDP_ProductSubcategory_Link);

                if (driver.Url.Contains(Locale + pdp_withProductCategories_url))
                {
                    driver.SwitchTo().Window(driver.WindowHandles.Last());

                    //--- Expected Result: Link should redirect to its respective product subcategory landing pages ---//
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + "/products/");

                    driver.Close();
                    driver.SwitchTo().Window(driver.WindowHandles.First());
                }
                else
                {
                    //--- Expected Result: Link should redirect to its respective product subcategory landing pages ---//
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + "/products/");

                    driver.Navigate().Back();
                }
            }

            //----- R8 > T27: Verify that the PST icons are added to the Product Page/Product Categories section (IQ-4963/AL-12017) -----//

            //--- Expected Result: The PST icon is displayed beside the sub category ---//
            test.validateElementIsPresent(driver, Elements.PDP_Pst_Icon);

            if (util.CheckElement(driver, Elements.PDP_Pst_Icon, 2))
            {
                //--- Action: Click on the PST icon ---//
                action.IClick(driver, Elements.PDP_Pst_Icon);

                if (driver.Url.Contains(Locale + pdp_withProductCategories_url))
                {
                    driver.SwitchTo().Window(driver.WindowHandles.Last());
                }

                //--- Expected Result: System will direct user to the PST pages ---//
                test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + "/parametricsearch/");
            }
        }

        [Test, Category("PDP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Markets and Technology is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Markets and Technology is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Markets and Technology is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Markets and Technology is Present and Working as Expected in RU Locale")]
        public void PDP_Overview_VerifyMarketsAndTechnology(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + pdp_withMarketsAndTechnology_url);
            Thread.Sleep(1000);

            //----- R8 > T8: Verify Markets & Technology in ANALOG product-----//
            if (util.CheckElement(driver, Elements.PDP_MarketsAndTechnology_Txt, 2))
            {
                //--- Expected Result: Markets & Technology must be displayed under Product Categories links in ANALOG product ---//
                if (Locale.Equals("en"))
                {
                    test.validateStringInstance(driver, marketsAndTechnology_txt, util.GetText(driver, Elements.PDP_MarketsAndTechnology_Txt));
                }
                /*****this is for AL-16901*****/
                test.validateElementIsPresent(driver, By.CssSelector("div[class^='radioverse'] ul>li[class='accordionGroup']"));
                test.validateElementIsPresent(driver, By.CssSelector("div[class^='radioverse'] ul>li[class='accordionGroup']:nth-child(1)>div[class='accordionGroup__toggle collapsed']"));
                string SubItemCount = Regex.Match(util.GetText(driver, By.CssSelector("div[class^='radioverse'] ul>li[class='accordionGroup']>div>a>span")), @"\d+").Value;
                /***expand the accordion***/
                action.IClick(driver, By.CssSelector("div[class^='radioverse'] ul>li[class='accordionGroup']:nth-child(1)>div[class='accordionGroup__toggle collapsed'] span[class='accordionGroup__toggle__icon']"));
                test.validateElementIsPresent(driver, By.CssSelector("div[class^='radioverse'] ul>li[class='accordionGroup']:nth-child(1)>div[class='accordionGroup__toggle']"));
                test.validateElementIsPresent(driver, By.CssSelector("div[class^='radioverse'] ul>li[class='accordionGroup']:nth-child(1) div[class='accordionGroup__body__inner']"));
                test.validateCountIsEqual(driver, Int32.Parse(SubItemCount), util.GetCount(driver, By.CssSelector("div[class^='radioverse'] ul>li[class='accordionGroup']:nth-child(1) div[class='accordionGroup__body__inner']>ul>li")));
                //--- Expected Result: Markets & Technology links must be displayed showing the list of markets & technology applications where the product part number belongs. ---//
                test.validateElementIsPresent(driver, Elements.PDP_MarketsAndTechnology_Link);

                if (util.CheckElement(driver, Elements.PDP_MarketsAndTechnology_Link, 2))
                {
                    //--- Action: Click on any markets & technology links ---//
                    action.IOpenLinkInNewTab(driver, Elements.PDP_MarketsAndTechnology_Link);
                    Thread.Sleep(2000);

                    //--- Expected Result: Link should redirect to its respective markets & technology application landing pages ---//
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + "/applications/");
                }
            }
        }
    }
}