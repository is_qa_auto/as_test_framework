﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Threading;

namespace AS_Test_Framework.PDP
{
    [TestFixture]
    public class PDP_SampleAndBuy_SampleAndBuyTable : BaseSetUp
    {
        public PDP_SampleAndBuy_SampleAndBuyTable() : base() { }

        //--- Login Credentials ---//
        string username = "myanalog_t3st_111@mailinator.com";
        string password = "Test_1234";

        //--- URLs ---//
        string pdp_url = "/ad7705";
        //string pdp_url = "/ad7706";

        //--- Labels ---//
        string[] sampleAndBuy_col_headers = { "Model", "Package", "Pins", "Temp Range", "Packing Qty", "Price (100-499)", "Price (1000+)", "RoHS", "Order from Analog Devices" };
        string[] priceTableHelp_popUpBox_link_labels = { "Model", "Package", "Pins", "Temp. range", "Packing Qty.", "Price", "Rohs", "Orders from Analog Devices" };
        string[] priceTableHelp_popUpBox_link_anchors = { "#model", "#pkgDesc", "#pinCount", "#tempRange", "#pkgQty", "#priceCnt", "#leadFree", "#CheckInventory_Purchase_Sample" };
        string country_selected = "United States";

        [Test, Category("PDP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Sample and Buy Table is Present and Working as Expected when the User is Logged Out in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Sample and Buy Table is Present and Working as Expected when the User is Logged Out in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Sample and Buy Table is Present and Working as Expected when the User is Logged Out in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Sample and Buy Table is Present and Working as Expected when the User is Logged Out in RU Locale")]
        public void PDP_SampleAndBuy_VerifySampleAndBuyTableWhenUserIsLoggedOut(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + pdp_url);

            if (util.CheckElement(driver, Elements.PDP_SampleAndBuy_Tbl, 2))
            {
                for (int locator_ctr = 1, label_ctr = 0; locator_ctr <= sampleAndBuy_col_headers.Length; locator_ctr++, label_ctr++)
                {
                    //----- R17 > T2: Verify Sample & Buy table in ANALOG product -----//

                    //--- Expected Result: Sample & Buy table must be displayed with the following columns in ANALOG product: Model | Package | Pins | Temp Range | Packing Qty | Price(100 - 499) | Price(1000 +) | RoHS | Order from Analog Devices ---//
                    if (Locale.Equals("en"))
                    {
                        test.validateStringIsCorrect(driver, By.CssSelector("div[id='product-samplebuy'] * div[class$='model table-responsive']>table>thead>tr>th:nth-of-type(" + locator_ctr + ")"), sampleAndBuy_col_headers[label_ctr]);
                    }
                    else
                    {
                        test.validateElementIsPresent(driver, By.CssSelector("div[id='product-samplebuy'] * div[class$='model table-responsive']>table>thead>tr>th:nth-of-type(" + locator_ctr + ")"));
                    }

                    //--- Expected Result: All the Models under the Packing Qty Column has Values ---//
                    test.validateStringChanged(driver, util.GetText(driver, Elements.PDP_SampleAndBuy_Tbl_PackingQty_Col_Val), "-");
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.PDP_SampleAndBuy_Tbl);

            }

            //----- R17 > T5: Verify "Select a country" dropdown menu under Sample & Buy section in LINEAR product -----//

            //--- Expected Result: "Select a country" dropdown menu should be displayed under Sample & Buy table in LINEAR product. ---//
            test.validateElementIsPresent(driver, Elements.PDP_SampleAndBuy_Tbl_SelectACountry_Dd);

            if (util.CheckElement(driver, Elements.PDP_SampleAndBuy_Tbl_SelectACountry_Dd, 2))
            {
                //--- Action: Click the dropdown menu. ---//
                action.IClick(driver, Elements.PDP_SampleAndBuy_Tbl_SelectACountry_Dd_Btn);

                //--- Expected Result: Countries will be shown. ---//
                test.validateElementIsPresent(driver, Elements.PDP_SampleAndBuy_Tbl_SelectACountry_Dd_Open);

                //--- Action: Select a country (Ex. VIETNAM) ---//
                action.IClick(driver, By.CssSelector("div[id='product-samplebuy']>div>div:nth-of-type(1) * div[class$='country-filter'] * [data-id='VN']"));

                //--- Expected Result: The Country will be selected on the dropdown. (Ex. VIETNAM is shown as "VIETNAM".) ---//
                test.validateStringIsCorrect(driver, Elements.PDP_SampleAndBuy_Tbl_SelectACountry_Dd_Selected_Val, "Vietnam");
            }

            //----- R17 > T8: Verify the note under Sample & Buy table in ANALOG product -----//

            //--- Expected Result: The note must be seen under Sample & Buy table in ANALOG product ---//
            test.validateElementIsPresent(driver, Elements.PDP_SampleAndBuy_Tbl_Note);

            //----- R17 > T10: Verify Price Table Help link under Sample & Buy table in ANALOG product -----//

            //--- Expected Result: The "Price Table Help" link must be displayed under Sample & Buy table in ANALOG product ---//
            test.validateElementIsPresent(driver, Elements.PDP_PriceTableHelp_Link);

            if (util.CheckElement(driver, Elements.PDP_PriceTableHelp_Link, 2))
            {
                //--- Action: Click Price Table Help link. ---//
                action.IClick(driver, Elements.PDP_PriceTableHelp_Link);

                if (util.CheckElement(driver, Elements.PDP_PriceTableHelp_PopUpBox, 2))
                {
                    //--- Expected Result: "Print" link at the top right corner of the pop up box must be shown. ---//
                    test.validateElementIsPresent(driver, Elements.PDP_PriceTableHelp_Print_Link);

                    //--- Expected Result: Close button (X) at the top right corner of the pop up box should be disaplayed just above the Print link. ---//
                    test.validateElementIsPresent(driver, Elements.PDP_PriceTableHelp_X_Btn);

                    for (int locator_ctr = 1, label_ctr = 0; locator_ctr <= priceTableHelp_popUpBox_link_labels.Length; locator_ctr++, label_ctr++)
                    {
                        //--- Expected Result: A pop up box must be opened with the following Sample & Buy table column links and descriptions: Model | Package | Pins | Temp Range | Packing Qty | Price(100 - 499) | Price(1000 +) | RoHS | Order from Analog Devices ---//
                        if (Locale.Equals("en"))
                        {
                            test.validateStringIsCorrect(driver, By.CssSelector("div[class*='glossary'] * div[class='glossary']>div>a:nth-of-type(" + locator_ctr + ")"), priceTableHelp_popUpBox_link_labels[label_ctr]);
                        }
                        else
                        {
                            test.validateElementIsPresent(driver, By.CssSelector("div[class*='glossary'] * div[class='glossary']>div>a:nth-of-type(" + locator_ctr + ")"));
                        }

                        if (util.CheckElement(driver, By.CssSelector("div[class*='glossary'] * div[class='glossary']>div>a:nth-of-type(" + locator_ctr + ")"), 2))
                        {
                            //--- Action: Click each links. ---//
                            action.IClick(driver, By.CssSelector("div[class*='glossary'] * div[class='glossary']>div>a:nth-of-type(" + locator_ctr + ")"));

                            //--- Expected Result: The selected/clicked link(s) will anchor to its description within the pop up box. ---//
                            test.validateStringInstance(driver, driver.Url, priceTableHelp_popUpBox_link_anchors[label_ctr]);
                        }
                    }

                    if (util.CheckElement(driver, Elements.PDP_PriceTableHelp_X_Btn, 2))
                    {
                        //--- Action: Click close button (X). ---//
                        action.IClick(driver, Elements.PDP_PriceTableHelp_X_Btn);

                        //--- Expected Result: Price Table Help pop up box must be closed. ---//
                        test.validateElementIsNotPresent(driver, Elements.PDP_PriceTableHelp_PopUpBox);
                    }
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.PDP_PriceTableHelp_PopUpBox);
                }
            }
        }

        [Test, Category("PDP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Sample and Buy Table is Present and Working as Expected when the User is Logged In in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Sample and Buy Table is Present and Working as Expected when the User is Logged In in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Sample and Buy Table is Present and Working as Expected when the User is Logged In in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Sample and Buy Table is Present and Working as Expected when the User is Logged In in RU Locale")]
        public void PDP_SampleAndBuy_VerifySampleAndBuyTableWhenUserIsLoggedIn(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + pdp_url);

            if (util.CheckElement(driver, Elements.MyAnalog_Widget_Btn, 2))
            {
                action.ILogInViaMyAnalogWidget(driver, username, password);

                //----- R17 > T4.2: Verify the country selected if user is Logged-in -----//

                //--- Expected Result: The country selected should be the same as the country registered in the user's account ---//
                test.validateStringIsCorrect(driver, Elements.PDP_SampleAndBuy_Tbl_SelectACountry_Dd_Selected_Val, country_selected);
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.MyAnalog_Widget_Btn);
            }

            if (util.CheckElement(driver, Elements.PDP_SampleAndBuy_Tbl_Sample_Btn, 2))
            {
                //----- R17 > T4: Verify "Sample" and "Purchase" buttons in ANALOG product (IQ-4000/AL-11205) -----//

                int sampleAndBuy_tbl_row_count = util.GetCount(driver, By.CssSelector("div[id='product-samplebuy'] * div[class$='model table-responsive']>table>tbody>tr"));

                for (int locator_ctr = 1; locator_ctr <= sampleAndBuy_tbl_row_count; locator_ctr++)
                {
                    if (util.CheckElement(driver, By.CssSelector("div[id='product-samplebuy'] * div[class$='model table-responsive']>table>tbody>tr:nth-of-type(" + locator_ctr + ") * a[class$='sample']"), 2))
                    {
                        string model_name = util.ReturnAttribute(driver, By.CssSelector("div[id='product-samplebuy'] * div[class$='model table-responsive']>table>tbody>tr:nth-of-type(" + locator_ctr + ")"), "data-id");

                        //--- Action: Click on Sample button from Order from Analog Devices column in ANALOG product. ---//
                        action.IOpenLinkInNewTab(driver, By.CssSelector("div[id='product-samplebuy'] * div[class$='model table-responsive']>table>tbody>tr:nth-of-type(" + locator_ctr + ") * a[class$='sample']"));

                        if (util.CheckElement(driver, Elements.Shopping_Cart_SelectShippingCountry_ErrorMsg, 2))
                        {
                            action.IClick(driver, Elements.Shopping_Cart_Country_Dropdown);
                            action.IClick(driver, By.CssSelector("li[value='US']>a"));
                        }

                        Thread.Sleep(1000);

                        //--- Expected Result: Shopping cart page with selected model should be displayed in new tab. ---//
                        test.validateStringIsCorrect(driver, Elements.Shopping_Cart_AddedModel, model_name);

                        break;
                    }

                    if (locator_ctr == sampleAndBuy_tbl_row_count)
                    {
                        test.validateElementIsPresent(driver, By.CssSelector("div[id='product-samplebuy'] * div[class$='model table-responsive']>table>tbody>tr:nth-of-type(" + (locator_ctr + 1) + ") * a[class$='sample']"));
                    }
                }
            }
        }
    }
}