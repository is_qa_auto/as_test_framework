﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;

namespace AS_Test_Framework.PDP
{
    [TestFixture]
    public class PDP_SampleAndBuy_EvaluationBoardsTable : BaseSetUp
    {
        public PDP_SampleAndBuy_EvaluationBoardsTable() : base() { }

        //--- URLs ---//
        //string pdp_url = "/ad7705";
        string pdp_url = "/ad7706";

        //--- Labels ---//
        string[] evaluationBoards_col_headers = { "Model", "Description", "Price", "RoHS" };

        [Test, Category("PDP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Evaluation Boards Table is Present and Working as Expected when in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Evaluation Boards Table is Present and Working as Expected when in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Evaluation Boards Table is Present and Working as Expected when in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Evaluation Boards Table is Present and Working as Expected when in RU Locale")]
        public void PDP_SampleAndBuy_VerifyEvaluationBoardsTable(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + pdp_url);

            if (util.CheckElement(driver, Elements.PDP_EvaluationBoards_Tbl, 2))
            {
                for (int locator_ctr = 1, label_ctr = 0; locator_ctr <= evaluationBoards_col_headers.Length; locator_ctr++, label_ctr++)
                {
                    //----- R17 > T12: Verify Evaluation Boards table under Sample & Buy table in ANALOG product -----//

                    //--- Expected Result: The message "Pricing displayed is based on 1-piece." beside Evaluation Boards must be shown. ---//
                    test.validateElementIsPresent(driver, Elements.PDP_EvaluationBoards_Tbl_Desc_Txt);

                    //--- Expected Result: Evaluation Boards table should be displayed with the following 4 columns: Model | Description | Price | RoHS-- -//
                    if (Locale.Equals("en"))
                    {
                        test.validateStringIsCorrect(driver, By.CssSelector("div[id='product-samplebuy'] * div[class$='evaluation table-responsive']>table>thead>tr>th:nth-of-type(" + locator_ctr + ")"), evaluationBoards_col_headers[label_ctr]);
                    }
                    else
                    {
                        test.validateElementIsPresent(driver, By.CssSelector("div[id='product-samplebuy'] * div[class$='evaluation table-responsive']>table>thead>tr>th:nth-of-type(" + locator_ctr + ")"));
                    }
                }

                //----- R17 > T18: Verify the note under Evaluation Boards table in ANALOG product -----//

                //--- Expected Result: The note must be seen under Evaluation Boards table in ANALOG product: ---//
                test.validateElementIsPresent(driver, Elements.PDP_EvaluationBoards_Tbl_Note);

                //----- R17 > T14: Verify "Select a country" dropdown menu under Evaluation Boards table in ANALOG product -----//

                //--- Expected Result: "Select a country" dropdown menu should be displayed under Evaluation Boards table in ANALOG product. ---//
                test.validateElementIsPresent(driver, Elements.PDP_EvaluationBoards_Tbl_SelectACountry_Dd);

                if (util.CheckElement(driver, Elements.PDP_EvaluationBoards_Tbl_SelectACountry_Dd, 2))
                {
                    //--- Action: Click the dropdown menu. ---//
                    action.IClick(driver, Elements.PDP_EvaluationBoards_Tbl_SelectACountry_Dd_Btn);

                    //--- Expected Result: Countries will be shown. ---//
                    test.validateElementIsPresent(driver, Elements.PDP_EvaluationBoards_Tbl_SelectACountry_Dd_Open);

                    //--- Action: Select a country (Ex. DENMARK) ---//
                    action.IClick(driver, By.CssSelector("div[id='product-samplebuy']>div>div:nth-of-type(2) * div[class$='country-filter'] * [data-id='DK']"));

                    //--- Expected Result: The Country will be selected on the dropdown. (Ex. DENMARK is shown as "DENMARK".) ---//
                    test.validateStringIsCorrect(driver, Elements.PDP_EvaluationBoards_Tbl_SelectACountry_Dd_Selected_Val, "Denmark");
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.PDP_EvaluationBoards_Tbl);
            }
        }
    }
}