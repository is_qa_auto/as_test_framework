﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;

namespace AS_Test_Framework.PDP
{
    [TestFixture]
    public class PDP_ReferenceMaterials_HeaderTitle : BaseSetUp
    {
        public PDP_ReferenceMaterials_HeaderTitle() : base() { }

        //--- URLs ---//
        //string pdp_url = "/ad7705";
        string pdp_url = "/ad7706";
        string pdp_referenceMaterials_url = "#product-reference";

        //--- Labels ---//
        string referenceMaterials_txt = "Reference Materials";

        [Test, Category("PDP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Reference Materials Header Title is Present and Correct in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Reference Materials Header Title is Present in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Reference Materials Header Title is Present in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Reference Materials Header Title is Present in RU Locale")]
        public void PDP_ReferenceMaterials_VerifyHeaderTitle(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + pdp_url);

            //----- R4 > T16: Verify STICKY HEADER (Navigational Menus) are clickable -----//
            if (util.CheckElement(driver, Elements.PDP_StickyHeader_ReferenceMaterials, 2))
            {
                //--- Action: Click Reference Designs on sticky header (navigational menus) ---//
                action.IClick(driver, Elements.PDP_StickyHeader_ReferenceMaterials);

                //--- Expected Result: It should anchor to Reference Designs section within the product detail page. ---//
                test.validateStringInstance(driver, driver.Url, pdp_referenceMaterials_url);
            }

            //----- R15 > T1: Verify Reference Materials header title -----//

            //--- Expected Result: The header title "Reference Materials" must be displayed at the top left side of the page. ---//
            if (Locale.Equals("en"))
            {
                test.validateStringInstance(driver, referenceMaterials_txt, util.GetText(driver, Elements.PDP_ReferenceMaterials_Sec_Lbl));
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.PDP_ReferenceMaterials_Sec_Lbl);
            }
        }
    }
}