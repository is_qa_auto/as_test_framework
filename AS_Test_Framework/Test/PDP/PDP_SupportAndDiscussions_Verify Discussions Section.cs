﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Threading;

namespace AS_Test_Framework.PDP
{
    [TestFixture]
    public class PDP_SupportAndDiscussions_DiscussionsSection : BaseSetUp
    {
        public PDP_SupportAndDiscussions_DiscussionsSection() : base() { }

        //--- URLs ---//
        string pdp_url = "/ad7705";
        //string pdp_url = "/ad7706";

        //--- Labels ---//
        string discussions_txt = "Discussions";
        string all_txt = "All";

        [Test, Category("PDP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Discussions Section is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Discussions Section is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Discussions Section is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Discussions Section is Present and Working as Expected in RU Locale")]
        public void PDP_SupportAndDiscussions_VerifyDiscussionsSection(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + pdp_url);

            string productNo = util.GetText(driver, Elements.PDP_ProductNo_Txt);

            //----- R16 > T2: Verify "<Part#> Discussions" title and content (IQ-6266/ AL-13624) -----//

            //--- Expected Result: The title "<Part#> Discussions" must be displayed at the top left side of the page under header title Discussions. ---//
            if (util.CheckElement(driver, Elements.PDP_SupportAndDiscussions_Discussions_Section_Lbl, 2) && Locale.Equals("en"))
            {
                test.validateStringInstance(driver, productNo.ToUpper() + " " + discussions_txt, util.GetText(driver, Elements.PDP_SupportAndDiscussions_Discussions_Section_Lbl));
            }

            //--- Expected Result: EngineerZone logo must be displayed under discussions link(s) and is aligned at the center. ---//
            if (Locale.Equals("cn") || Locale.Equals("zh"))
            {
                test.validateElementIsPresent(driver, Elements.PDP_SupportAndDiscussions_Cn_Ez_Logo);
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.PDP_SupportAndDiscussions_Ez_Logo);
            }

            //--- Expected Result: The "All <Part#> Discussions" button must be displayed under the message "Didn't find what you were looking for?" and should be aligned at the center. ---//
            if (util.CheckElement(driver, Elements.PDP_SupportAndDiscussions_AllDiscussions_Btn, 2) && Locale.Equals("en"))
            {
                test.validateStringInstance(driver, all_txt + " " + productNo.ToUpper() + " " + discussions_txt, util.GetText(driver, Elements.PDP_SupportAndDiscussions_AllDiscussions_Btn));
            }
        }
    }
}