﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;

namespace AS_Test_Framework.PDP
{
    [TestFixture]
    public class PDP_SampleAndBuy_HeaderTitle : BaseSetUp
    {
        public PDP_SampleAndBuy_HeaderTitle() : base() { }

        //--- URLs ---//
        //string pdp_url = "/ad7705";
        string pdp_url = "/ad7706";
        string pdp_sampleAndBuy_url = "#product-samplebuy";

        //--- Labels ---//
        string sampleAndBuy_txt = "Sample & Buy";

        [Test, Category("PDP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Sample and Buy Header Title is Present and Correct in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Sample and Buy Header Title is Present in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Sample and Buy Header Title is Present in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Sample and Buy Header Title is Present in RU Locale")]
        public void PDP_SampleAndBuy_VerifyHeaderTitle(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + pdp_url);

            //----- R4 > T16: Verify STICKY HEADER (Navigational Menus) are clickable -----//
            if (util.CheckElement(driver, Elements.PDP_StickyHeader_SampleAndBuy, 2))
            {
                //--- Action: Click Sample & Buy on sticky header (navigational menus) ---//
                action.IClick(driver, Elements.PDP_StickyHeader_SampleAndBuy);

                //--- Expected Result: It should anchor to Sample & Buy section within the product detail page. ---//
                test.validateStringInstance(driver, driver.Url, pdp_sampleAndBuy_url);
            }

            //----- R17 > T1: Verify Sample & Buy header title -----//

            //--- Expected Result: The header title "Sample & Buy" must be displayed at the top left side of the page. ---//
            if (Locale.Equals("en"))
            {
                test.validateStringInstance(driver, sampleAndBuy_txt, util.GetText(driver, Elements.PDP_SampleAndBuy_Sec_Lbl));
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.PDP_SampleAndBuy_Sec_Lbl);
            }
        }
    }
}