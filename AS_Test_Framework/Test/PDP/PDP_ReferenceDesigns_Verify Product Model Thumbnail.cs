﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;

namespace AS_Test_Framework.PDP
{
    [TestFixture]
    public class PDP_ReferenceDesigns_ProductModelThumbnail : BaseSetUp
    {
        public PDP_ReferenceDesigns_ProductModelThumbnail() : base() { }

        //--- URLs ---//
        string pdp_url = "/ad7960";

        [Test, Category("PDP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Product Model Thumbnail is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Product Model Thumbnail is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Product Model Thumbnail is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Product Model Thumbnail is Present and Working as Expected in RU Locale")]
        public void PDP_ReferenceDesigns_VerifyProductModelThumbnail(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + pdp_url);

            //----- R16 > T2: Verify Reference Design items for ANALOG product having 4 Reference Design Models -----//

            if (util.CheckElement(driver, Elements.PDP_ReferenceDesigns_ProductModel_FlyOut_Box, 2))
            {
                action.IClick(driver, Elements.PDP_ReferenceDesigns_ProductModel_FlyOut_Box_X_Btn);
            }

            //--- Expected Result: Reference design models should be displayed with thumbnail image, model number and description. ---//
            test.validateElementIsPresent(driver, Elements.PDP_ReferenceDesigns_ProductModel_Thumbnail_Img);
            test.validateElementIsPresent(driver, Elements.PDP_ReferenceDesigns_ProductModel_Thumbnail_ModelNo);

            if (!Locale.Equals("jp"))
            {
                test.validateElementIsPresent(driver, Elements.PDP_ReferenceDesigns_ProductModel_Thumbnail_Desc_Txt);
            }
        }
    }
}