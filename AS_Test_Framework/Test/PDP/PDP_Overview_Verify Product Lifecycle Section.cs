﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Threading;

namespace AS_Test_Framework.PDP
{
    [TestFixture]
    public class PDP_Overview_ProductLifecycleSection : BaseSetUp
    {
        public PDP_Overview_ProductLifecycleSection() : base() { }

        //--- URLs ---//
        string pdp_url = "/ad7705";
        //string pdp_url = "/ad7706";
        string pdp_withProductionStatus_url = "/ad7706";
        string pdp_withRecommendedForNewDesigns_url = "/ad7616";
        //string productLifeCycleInfo_page_url = "/about-adi/sustainability/product-life-cycle-information.html";
        string productLifeCycleInfo_page_url = "/product-life-cycle-information.html";

        [Test, Category("PDP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Product Lifecycle Section is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Product Lifecycle Section is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Product Lifecycle Section is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Product Lifecycle Section is Present and Working as Expected in RU Locale")]
        public void PDP_Overview_VerifyProductLifecycleSection(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + pdp_url);

            //----- R8 > T10: Verify Product Lifecycle for ANALOG product -----//

            //--- Expected Result: Product Lifecycle section under comparable parts must be available for ANALOG product. ---//
            test.validateElementIsPresent(driver, Elements.PDP_ProductLifecycle_Txt);

            //--- Expected Result: Details/information under "Product Lifecycle" should be in paragraph format. ---//
            test.validateElementIsPresent(driver, Elements.PDP_ProductLifecycle_Desc_Txt);
        }

        [Test, Category("PDP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Production Link is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Production Link is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Production Link is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Production Link is Present and Working as Expected in RU Locale")]
        public void PDP_Overview_VerifyProductionProductLink(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + pdp_withProductionStatus_url);

            //----- R8 > T13: Verify ANALOG product status "Production" beside Product Lifecycle -----//

            //--- Expected Result: Production status Icon should be displayed ---//
            test.validateElementIsPresent(driver, Elements.PDP_ProductLifecycle_ProductStatus_Icon);

            //--- Expected Result: "Production" Analog product status link should be displayed beside Product Lifecycle ---//
            test.validateElementIsPresent(driver, Elements.PDP_ProductLifecycle_ProductStatus_Link);

            if (util.CheckElement(driver, Elements.PDP_ProductLifecycle_ProductStatus_Link, 2))
            {
                //--- Action: Click "Production" Analog product status link ---//
                action.IOpenLinkInNewTab(driver, Elements.PDP_ProductLifecycle_ProductStatus_Link);
                Thread.Sleep(3000);

                //--- Expected Result: "Production" Analog product status link should redirect to Product Life Cycle Information landing page. ---//
                //test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + productLifeCycleInfo_page_url);
                test.validateStringInstance(driver, driver.Url, productLifeCycleInfo_page_url);
            }
        }

        [Test, Category("PDP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Recommended for New Designs Link is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Recommended for New Designs Link is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Recommended for New Designs Link is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Recommended for New Designs Link is Present and Working as Expected in RU Locale")]
        public void PDP_Overview_VerifyRecommendedForNewDesignsLink(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + pdp_withRecommendedForNewDesigns_url);

            //----- R8 > T11: Verify ANALOG product status "Recommended for New Designs" beside Product Lifecycle -----//

            //--- Expected Result: "Recommended for New Designs" Analog product status link should be displayed beside Product Lifecycle ---//
            test.validateElementIsPresent(driver, Elements.PDP_ProductLifecycle_ProductStatus_Link);

            if (util.CheckElement(driver, Elements.PDP_ProductLifecycle_ProductStatus_Link, 2))
            {
                //--- Action: Click "Recommended for New Designs" Analog product status link ---//
                action.IOpenLinkInNewTab(driver, Elements.PDP_ProductLifecycle_ProductStatus_Link);
                Thread.Sleep(3000);

                //--- Expected Result: "Recommended for New Designs" Analog product status link should redirect to Product Life Cycle Information landing page. ---//
                //test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + productLifeCycleInfo_page_url);
                test.validateStringInstance(driver, driver.Url, productLifeCycleInfo_page_url);
            }
        }
    }
}