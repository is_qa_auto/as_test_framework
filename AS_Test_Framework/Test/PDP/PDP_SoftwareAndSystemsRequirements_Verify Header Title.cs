﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Threading;

namespace AS_Test_Framework.PDP
{
    [TestFixture]
    public class PDP_SoftwareAndSystemsRequirements_HeaderTitle : BaseSetUp
    {
        public PDP_SoftwareAndSystemsRequirements_HeaderTitle() : base() { }

        //--- URLs ---//
        string pdp_url = "/ad7960";
        string pdp_softwareAndSystemsRequirements_url = "#product-requirement";

        //--- Labels ---//
        string softwareAndSystemsRequirements_txt = "Software & Systems Requirements";

        [Test, Category("PDP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Software and Systems Requirements Header Title is Present and Correct in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Software and Systems Requirements Header Title is Present in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Software and Systems Requirements Header Title is Present in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Software and Systems Requirements Header Title is Present in RU Locale")]
        public void PDP_SoftwareAndSystemsRequirements_VerifyHeaderTitle(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + pdp_url);

            //----- R4 > T16: Verify STICKY HEADER (Navigational Menus) are clickable -----//
            if (util.CheckElement(driver, Elements.PDP_StickyHeader_SoftwareAndSystemsRequirements, 2))
            {
                //--- Action: Click Software & Systems Requirements on sticky header (navigational menus) ---//
                action.IClick(driver, Elements.PDP_StickyHeader_SoftwareAndSystemsRequirements);
                Thread.Sleep(1000);

                //--- Expected Result: It should anchor to Software & Systems Requirements section within the product detail page. ---//
                test.validateStringInstance(driver, driver.Url, pdp_softwareAndSystemsRequirements_url);
            }

            //----- R12 > T1: Verify Software & Systems Requirements header title -----//

            //--- Expected Result: The header title "Software & Systems Requirements" must be displayed at the top left side of the page.. ---//
            if (Locale.Equals("en"))
            {
                test.validateStringInstance(driver, softwareAndSystemsRequirements_txt, util.GetText(driver, Elements.PDP_SoftwareAndSystemsRequirements_Section_Lbl));
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.PDP_SoftwareAndSystemsRequirements_Section_Lbl);
            }
        }
    }
}