﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;

namespace AS_Test_Framework.PDP
{
    [TestFixture]
    public class PDP_SupportAndDiscussions_HeaderTitle : BaseSetUp
    {
        public PDP_SupportAndDiscussions_HeaderTitle() : base() { }

        //--- URLs ---//
        string pdp_url = "/ad7705";
        //string pdp_url = "/ad7706";
        string pdp_supportAndDiscussions_url = "#product-discussions";

        //--- Labels ---//
        string supportAndDiscussions_txt = "Support & Discussions";

        [Test, Category("PDP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Support and Discussions Header Title is Present and Correct in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Support and Discussions Header Title is Present in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Support and Discussions Header Title is Present in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Support and Discussions Header Title is Present in RU Locale")]
        public void PDP_SupportAndDiscussions_VerifyHeaderTitle(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + pdp_url);

            //----- R4 > T16: Verify STICKY HEADER (Navigational Menus) are clickable -----//
            if (util.CheckElement(driver, Elements.PDP_StickyHeader_SupportAndDiscussions, 2))
            {
                //--- Action: Click Documentation on sticky header (navigational menus) ---//
                action.IClick(driver, Elements.PDP_StickyHeader_SupportAndDiscussions);

                //--- Expected Result: It should anchor to Documentation section within the product detail page. ---//
                test.validateStringInstance(driver, driver.Url, pdp_supportAndDiscussions_url);
            }

            //----- R16 > T1: Verify Discussions header title -----//

            //--- Expected Result: The header title "Discussions" must be displayed at the top left side of the page. ---//
            if (Locale.Equals("en"))
            {
                test.validateStringInstance(driver, supportAndDiscussions_txt, util.GetText(driver, Elements.PDP_SupportAndDiscussions_Sec_Lbl));
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.PDP_SupportAndDiscussions_Sec_Lbl);
            }
        }
    }
}