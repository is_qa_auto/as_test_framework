﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Text.RegularExpressions;
using System.Threading;

namespace AS_Test_Framework.PDP
{
    [TestFixture]
    public class PDP_Documentation_Tabs : BaseSetUp
    {
        public PDP_Documentation_Tabs() : base() { }

        //--- URLs ---//
        //string pdp_url = "/ad7705";
        string pdp_url = "/ad7706";
        string pdp_withUserGuides_url = "/ad7960";
        string obsolete_pdp_url = "/ad7240";

        //--- Labels ---//
        string viewAll_txt = "View All";
        string dataSheets_txt = "Data Sheets";
        string applicationNotes_txt = "Application Notes";
        string userGuides_txt = "User Guides";
        string obsoleteDataSheets_txt = "Obsolete Data Sheets";

        [Test, Category("PDP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the View All Tab is Present and Working as Expected in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the View All Tab is Present and Working as Expected in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the View All Tab is Present and Working as Expected in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the View All Tab is Present and Working as Expected in RU Locale")]
        public void PDP_Documentation_VerifyViewAllTab(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + pdp_url);

            if (util.CheckElement(driver, Elements.PDP_Documentation_Tabs, 2))
            {
                int documentationTabs_count = util.GetCount(driver, Elements.PDP_Documentation_Tabs);

                for (int ctr = 1; ctr <= documentationTabs_count; ctr++)
                {
                    if (util.GetText(driver, By.CssSelector("section[id$='documentation'] * div[class$='tabs']>ul>li:nth-of-type(" + ctr + ")>a")).Contains(viewAll_txt))
                    {
                        //----- R11 > T3: Verify "View All" tab in ANALOG product -----//

                        //--- Expected Result: "View All" tab is present at the top left side of the page under Documentation section in ANALOG product. ---//
                        test.validateElementIsPresent(driver, By.CssSelector("section[id$='documentation'] * div[class$='tabs']>ul>li:nth-of-type(" + ctr + ")>a"));

                        action.IClick(driver, By.CssSelector("section[id$='documentation'] * div[class$='tabs']>ul>li:nth-of-type(" + ctr + ")>a"));

                        //--- Expected Result: Number of Documents shown beside to "View All" should be same as the total number of documents. ---//
                        test.validateStringInstance(driver, util.GetCount(driver, Elements.PDP_Documentation_Tab_Links).ToString(), Regex.Match(util.GetText(driver, By.CssSelector("section[id$='documentation'] * div[class$='tabs']>ul>li:nth-of-type(" + ctr + ")>a")), @"\d+").Value);

                        //--- Expected Result: There should be available pdf file links for each document categories. ---//
                        test.validateElementIsPresent(driver, Elements.PDP_Documentation_Tab_Links);

                        break;
                    }

                    if (ctr == documentationTabs_count)
                    {
                        test.validateElementIsPresent(driver, By.CssSelector("section[id$='documentation'] * div[class$='tabs']>ul>li:nth-of-type(" + (ctr + 1) + ")>a"));
                    }
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.PDP_Documentation_Tabs);
            }
        }

        [Test, Category("PDP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Data Sheets Tab is Present and Working as Expected in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Data Sheets Tab is Present and Working as Expected in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Data Sheets Tab is Present and Working as Expected in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Data Sheets Tab is Present and Working as Expected in RU Locale")]
        public void PDP_Documentation_VerifyDataSheetsTab(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + pdp_url);

            if (util.CheckElement(driver, Elements.PDP_Documentation_Tabs, 2))
            {
                int documentationTabs_count = util.GetCount(driver, Elements.PDP_Documentation_Tabs);

                for (int ctr = 1; ctr <= documentationTabs_count; ctr++)
                {
                    if (util.GetText(driver, By.CssSelector("section[id$='documentation'] * div[class$='tabs']>ul>li:nth-of-type(" + ctr + ")>a")).Contains(dataSheets_txt))
                    {
                        action.IClick(driver, By.CssSelector("section[id$='documentation'] * div[class$='tabs']>ul>li:nth-of-type(" + ctr + ")>a"));

                        //----- R11 > T5: Verify Data Sheets tab in ANALOG product -----//

                        //--- Expected Result: There should be a counter displayed beside Data Sheets tab enclosed by parenthesis showing the number of available pdf data sheet documents. ---//
                        test.validateStringInstance(driver, util.GetCount(driver, Elements.PDP_Documentation_Tab_Links).ToString(), Regex.Match(util.GetText(driver, By.CssSelector("section[id$='documentation'] * div[class$='tabs']>ul>li:nth-of-type(" + ctr + ")>a")), @"\d+").Value);

                        //--- Expected Result: There should be "Data Sheets" title displayed under Data Sheets tab. ---//
                        test.validateStringIsCorrect(driver, Elements.PDP_Documentation_Tab_Title, dataSheets_txt);

                        //--- Expected Result: There should be available pdf file links for each document categories. ---//
                        test.validateElementIsPresent(driver, Elements.PDP_Documentation_Tab_Links);

                        break;
                    }

                    if (ctr == documentationTabs_count)
                    {
                        test.validateElementIsPresent(driver, By.CssSelector("section[id$='documentation'] * div[class$='tabs']>ul>li:nth-of-type(" + (ctr + 1) + ")>a"));
                    }
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.PDP_Documentation_Tabs);
            }
        }

        [Test, Category("PDP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Application Notes Tab is Present and Working as Expected in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Application Notes Tab is Present and Working as Expected in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Application Notes Tab is Present and Working as Expected in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Application Notes Tab is Present and Working as Expected in RU Locale")]
        public void PDP_Documentation_VerifyApplicationNotesTab(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + pdp_url);
            Thread.Sleep(1000);

            if (util.CheckElement(driver, Elements.PDP_Documentation_Tabs, 2))
            {
                int documentationTabs_count = util.GetCount(driver, Elements.PDP_Documentation_Tabs);

                for (int ctr = 1; ctr <= documentationTabs_count; ctr++)
                {
                    if (util.GetText(driver, By.CssSelector("section[id$='documentation'] * div[class$='tabs']>ul>li:nth-of-type(" + ctr + ")>a")).Contains(applicationNotes_txt))
                    {
                        action.IClick(driver, By.CssSelector("section[id$='documentation'] * div[class$='tabs']>ul>li:nth-of-type(" + ctr + ")>a"));

                        //----- R11 > T9: Verify Application Notes tab in ANALOG product -----//

                        //--- Expected Result: There should be a counter displayed beside Application Notes tab enclosed by parenthesis showing the number of available pdf data sheet documents. ---//
                        test.validateStringInstance(driver, util.GetCount(driver, Elements.PDP_Documentation_Tab_Links).ToString(), Regex.Match(util.GetText(driver, By.CssSelector("section[id$='documentation'] * div[class$='tabs']>ul>li:nth-of-type(" + ctr + ")>a")), @"\d+").Value);

                        //--- Expected Result: There should be "Application Notes" title displayed under Application Notes tab. ---//
                        test.validateStringIsCorrect(driver, Elements.PDP_Documentation_Tab_Title, applicationNotes_txt);

                        //--- Expected Result: There should be pdf file link(s) displayed under gray horizontal line. ---//
                        test.validateElementIsPresent(driver, Elements.PDP_Documentation_Tab_Links);

                        break;
                    }

                    if (ctr == documentationTabs_count)
                    {
                        test.validateElementIsPresent(driver, By.CssSelector("section[id$='documentation'] * div[class$='tabs']>ul>li:nth-of-type(" + (ctr + 1) + ")>a"));
                    }
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.PDP_Documentation_Tabs);
            }
        }

        [Test, Category("PDP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the User Guides Tab is Present and Working as Expected in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the User Guides Tab is Present and Working as Expected in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the User Guides Tab is Present and Working as Expected in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the User Guides Tab is Present and Working as Expected in RU Locale")]
        public void PDP_Documentation_VerifyUserGuidesTab(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + pdp_withUserGuides_url);

            if (util.CheckElement(driver, Elements.PDP_Documentation_Tabs, 2))
            {
                int documentationTabs_count = util.GetCount(driver, Elements.PDP_Documentation_Tabs);

                for (int ctr = 1; ctr <= documentationTabs_count; ctr++)
                {
                    if (util.GetText(driver, By.CssSelector("section[id$='documentation'] * div[class$='tabs']>ul>li:nth-of-type(" + ctr + ")>a")).Contains(userGuides_txt))
                    {
                        action.IClick(driver, By.CssSelector("section[id$='documentation'] * div[class$='tabs']>ul>li:nth-of-type(" + ctr + ")>a"));

                        //----- R11 > T3: Verify User Guides tab in ANALOG product -----//

                        //--- Expected Result: There should be a counter displayed beside User Guides tab enclosed by parenthesis showing the number of available pdf data sheet documents. ---//
                        test.validateStringInstance(driver, util.GetCount(driver, Elements.PDP_Documentation_Tab_Links).ToString(), Regex.Match(util.GetText(driver, By.CssSelector("section[id$='documentation'] * div[class$='tabs']>ul>li:nth-of-type(" + ctr + ")>a")), @"\d+").Value);

                        //--- Expected Result: There should be "User Guides" title displayed under User Guides tab. ---//
                        test.validateStringIsCorrect(driver, Elements.PDP_Documentation_Tab_Title, userGuides_txt);

                        //--- Expected Result: There should be pdf file link(s) displayed under gray horizontal line. ---//
                        test.validateElementIsPresent(driver, Elements.PDP_Documentation_Tab_Links);

                        break;
                    }

                    if (ctr == documentationTabs_count)
                    {
                        test.validateElementIsPresent(driver, By.CssSelector("section[id$='documentation'] * div[class$='tabs']>ul>li:nth-of-type(" + (ctr + 1) + ")>a"));
                    }
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.PDP_Documentation_Tabs);
            }
        }

        [Test, Category("PDP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the View All Tab is Present and Working as Expected in an Obsolete PDP in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the View All Tab is Present and Working as Expected in an Obsolete PDP in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the View All Tab is Present and Working as Expected in an Obsolete PDP in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the View All Tab is Present and Working as Expected in an Obsolete PDP in RU Locale")]
        public void PDP_Documentation_VerifyViewAllTabInAnObsoletePdp(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + obsolete_pdp_url);

            if (util.CheckElement(driver, Elements.PDP_Documentation_Tabs, 2))
            {
                int documentationTabs_count = util.GetCount(driver, Elements.PDP_Documentation_Tabs);

                for (int ctr = 1; ctr <= documentationTabs_count; ctr++)
                {
                    if (util.GetText(driver, By.CssSelector("section[id$='documentation'] * div[class$='tabs']>ul>li:nth-of-type(" + ctr + ")>a")).Contains(viewAll_txt))
                    {
                        //----- PDP - Obosolete Test Plan > R6 > T2: Verify navigational menus should dock to the header upon mouse scroll -----//

                        //--- Expected Result: "View All" tab is present at the top left side of the page under Documentation section in ANALOG product. ---//
                        test.validateElementIsPresent(driver, By.CssSelector("section[id$='documentation'] * div[class$='tabs']>ul>li:nth-of-type(" + ctr + ")>a"));

                        action.IClick(driver, By.CssSelector("section[id$='documentation'] * div[class$='tabs']>ul>li:nth-of-type(" + ctr + ")>a"));

                        //--- Expected Result: There should be a counter displayed beside View All tab enclosed by parenthesis showing the number of available pdf documents. ---//
                        test.validateStringInstance(driver, util.GetCount(driver, Elements.PDP_Documentation_Tab_Links).ToString(), Regex.Match(util.GetText(driver, By.CssSelector("section[id$='documentation'] * div[class$='tabs']>ul>li:nth-of-type(" + ctr + ")>a")), @"\d+").Value);

                        //--- Expected Result: There should be available pdf file links for each document categories. ---//
                        test.validateElementIsPresent(driver, Elements.PDP_Documentation_Tab_Links);

                        break;
                    }

                    if (ctr == documentationTabs_count)
                    {
                        test.validateElementIsPresent(driver, By.CssSelector("section[id$='documentation'] * div[class$='tabs']>ul>li:nth-of-type(" + (ctr + 1) + ")>a"));
                    }
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.PDP_Documentation_Tabs);
            }
        }

        [Test, Category("PDP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Obsolete Data Sheets Tab is Present and Working as Expected in an Obsolete PDP in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Obsolete Data Sheets Tab is Present and Working as Expected in an Obsolete PDP in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Obsolete Data Sheets Tab is Present and Working as Expected in an Obsolete PDP in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Obsolete Data Sheets Tab is Present and Working as Expected in an Obsolete PDP in RU Locale")]
        public void PDP_Documentation_VerifyObsoleteDataSheetsTabInAnObsoletePdp(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + obsolete_pdp_url);

            if (util.CheckElement(driver, Elements.PDP_Documentation_Tabs, 2))
            {
                int documentationTabs_count = util.GetCount(driver, Elements.PDP_Documentation_Tabs);

                for (int ctr = 1; ctr <= documentationTabs_count; ctr++)
                {
                    if (util.GetText(driver, By.CssSelector("section[id$='documentation'] * div[class$='tabs']>ul>li:nth-of-type(" + ctr + ")>a")).Contains(obsoleteDataSheets_txt))
                    {
                        action.IClick(driver, By.CssSelector("section[id$='documentation'] * div[class$='tabs']>ul>li:nth-of-type(" + ctr + ")>a"));

                        //----- PDP - Obosolete Test Plan > R2 > T4: Verify Obsolete Data Sheets tab -----//

                        //--- Expected Result: There should be a counter displayed beside Obsolete Data Sheets tab enclosed by parenthesis showing the number of available pdf data sheet documents. ---//
                        test.validateStringInstance(driver, util.GetCount(driver, Elements.PDP_Documentation_Tab_Links).ToString(), Regex.Match(util.GetText(driver, By.CssSelector("section[id$='documentation'] * div[class$='tabs']>ul>li:nth-of-type(" + ctr + ")>a")), @"\d+").Value);

                        //--- Expected Result: There should be "Obsolete Data Sheets" title displayed under Data Sheets tab. ---//
                        test.validateStringIsCorrect(driver, Elements.PDP_Documentation_Tab_Title, obsoleteDataSheets_txt);

                        //--- Expected Result: There should be pdf file link(s) displayed under gray horizontal line. ---//
                        test.validateElementIsPresent(driver, Elements.PDP_Documentation_Tab_Links);

                        //--- Expected Result: PDF icon is displayed with file size information ---//
                        test.validateElementIsPresent(driver, Elements.PDP_Documentation_Tab_Pdf_Icons);

                        break;
                    }

                    if (ctr == documentationTabs_count)
                    {
                        test.validateElementIsPresent(driver, By.CssSelector("section[id$='documentation'] * div[class$='tabs']>ul>li:nth-of-type(" + (ctr + 1) + ")>a"));
                    }
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.PDP_Documentation_Tabs);
            }
        }
    }
}