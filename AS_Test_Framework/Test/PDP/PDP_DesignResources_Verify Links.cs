﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.PDP
{
    [TestFixture]
    public class PDP_DesignResources_Links : BaseSetUp
    {
        public PDP_DesignResources_Links() : base() { }

        //--- URLs ---//
        //string pdp_url = "/ad7705";
        string pdp_url = "/ad7706";
        string materialDeclarationSearchResults_page_url = "/searchresults.aspx";
        string qualityAndReliability_page_url = "/about-adi/quality-reliability.html";
        string symbolsAndFootprints_page_url = "/design-center/packaging-quality-symbols-footprints/symbols-and-footprints/";

        [Test, Category("PDP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Links are Present and Correct in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Links are Present and Correct in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Links are Present and Correct in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Links are Present and Correct in RU Locale")]
        public void PDP_DesignResources_VerifyLinks(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + pdp_url);
            Thread.Sleep(1000);

            string productNo = util.GetText(driver, Elements.PDP_ProductNo_Txt);

            //----- R10 > T2: Verify Material Declaration link in ANALOG product -----//

            //--- Expected Result: Material Declaration link must be displayed ---//
            test.validateElementIsPresent(driver, Elements.PDP_MaterialDeclaration_Link);

            if (util.CheckElement(driver, Elements.PDP_MaterialDeclaration_Link, 2))
            {
                //--- Action: Click Material Declaration link ---//
                action.IClick(driver, Elements.PDP_MaterialDeclaration_Link);

                if (driver.Url.Contains(Locale + pdp_url))
                {
                    driver.SwitchTo().Window(driver.WindowHandles.Last());

                    //--- Expected Result: It should redirect to Material Declaration Search Results page ---//
                    if (Locale.Equals("en"))
                    {
                        test.validateStringInstance(driver, driver.Url, "analog.com" + materialDeclarationSearchResults_page_url);
                    }
                    else if (Locale.Equals("cn") || Locale.Equals("zh"))
                    {
                        test.validateStringInstance(driver, driver.Url, "analog.com" + materialDeclarationSearchResults_page_url + "?locale=zh");
                    }
                    else
                    {
                        test.validateStringInstance(driver, driver.Url, "analog.com" + materialDeclarationSearchResults_page_url + "?locale=" + Locale);
                    }

                    driver.Close();
                    driver.SwitchTo().Window(driver.WindowHandles.First());
                }
                else
                {
                    //--- Expected Result: It should redirect to Material Declaration Search Results page ---//
                    if (Locale.Equals("en"))
                    {
                        test.validateStringInstance(driver, driver.Url, "analog.com" + materialDeclarationSearchResults_page_url);
                    }
                    else if (Locale.Equals("cn") || Locale.Equals("zh"))
                    {
                        test.validateStringInstance(driver, driver.Url, "analog.com" + materialDeclarationSearchResults_page_url + "?locale=zh");
                    }
                    else
                    {
                        test.validateStringInstance(driver, driver.Url, "analog.com" + materialDeclarationSearchResults_page_url + "?locale=" + Locale);
                    }

                    driver.Navigate().Back();
                    Thread.Sleep(1000);
                }
            }

            //----- R10 > T4: Verify Quality And Reliability link in ANALOG product -----//

            //--- Expected Result: "Quality And Reliability" link must be displayed ---//
            test.validateElementIsPresent(driver, Elements.PDP_QualityAndReliability_Link);

            if (util.CheckElement(driver, Elements.PDP_QualityAndReliability_Link, 2))
            {
                //--- Action: Click "Quality And Reliability" link ---//
                action.IOpenLinkInNewTab(driver, Elements.PDP_QualityAndReliability_Link);
                Thread.Sleep(2000);

                //--- Expected Result: It should redirect to Quality And Reliability page. ---//
                test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + qualityAndReliability_page_url);

                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles.First());
            }

            //----- R10 > T6: Verify Symbols and Footprints link in ANALOG product -----//

            //--- Expected Result: "Symbols and Footprints" link must be displayed ---//
            test.validateElementIsPresent(driver, Elements.PDP_SymbolsAndFootprints_Link);

            if (util.CheckElement(driver, Elements.PDP_SymbolsAndFootprints_Link, 2))
            {
                //--- Action: Click "Symbols and Footprints" link ---//
                action.IOpenLinkInNewTab(driver, Elements.PDP_SymbolsAndFootprints_Link);
                Thread.Sleep(1000);

                //--- Expected Result: It should redirect to Symbols and Footprints page ---//
                test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + symbolsAndFootprints_page_url + productNo.ToUpper() + ".html");
            }
        }
    }
}