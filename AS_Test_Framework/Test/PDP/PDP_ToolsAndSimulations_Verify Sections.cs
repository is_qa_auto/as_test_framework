﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;

namespace AS_Test_Framework.PDP
{
    [TestFixture]
    public class PDP_ToolsAndSimulations_Sections : BaseSetUp
    {
        public PDP_ToolsAndSimulations_Sections() : base() { }

        //--- URLs ---//
        string pdp_withVirtualEvalBeta_url = "/ad6674";
        string pdp_withIbisModels_url = "/ad6674";
        string pdp_withDesignTools_url = "/ad6674";
        string pdp_withSParameters_url = "/adgm1304";
        string pdp_withSoftwareDevelopmentKit_url = "/ltc2937";
        string pdp_withLtSpice_url = "/ltc3880";

        //--- Labels ---//
        string virtualEvalBeta_txt = "Virtual Eval - BETA";
        string ibisModels_txt = "IBIS Models";
        string designTools_txt = "Design Tools";
        string sParameters_txt = "S-Parameters";
        string softwareDevelopmentKit_txt = "Software Development Kit";
        string ltSpice_txt = "LTspice";

        [Test, Category("PDP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Virtual Eval - BETA Section is Present and Working as Expected in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Virtual Eval - BETA Section is Present and Working as Expected in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Virtual Eval - BETA Section is Present and Working as Expected in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Virtual Eval - BETA Section is Present and Working as Expected in RU Locale")]
        public void PDP_ToolsAndSimulations_VerifyVirtualEvalBetaSection(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + pdp_withVirtualEvalBeta_url);

            if (util.CheckElement(driver, Elements.PDP_ToolsAndSimulations_VirtualEval_Section_Lbl, 2))
            {
                //----- R13 > T3: Verify the Virtual Eval - BETA Section -----//

                //--- Expected Result: The title "Virtual Eval - BETA" must be displayed ---//
                if (Locale.Equals("en"))
                {
                    test.validateStringIsCorrect(driver, Elements.PDP_ToolsAndSimulations_VirtualEval_Section_Lbl, virtualEvalBeta_txt);
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.PDP_ToolsAndSimulations_VirtualEval_Section_Lbl);
                }

                //--- Expected Result: The description under Virtual Eval - BETA should be displayed. ---//
                test.validateElementIsPresent(driver, Elements.PDP_ToolsAndSimulations_VirtualEval_Section_Desc_Txt);

                //--- Expected Result: There should be a Virtual Eval - BETA thumbnail Image ---//
                test.validateElementIsPresent(driver, Elements.PDP_ToolsAndSimulations_VirtualEval_Section_Img);
            }
        }

        [Test, Category("PDP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the IBIS Models Section is Present and Working as Expected in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the IBIS Models Section is Present and Working as Expected in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the IBIS Models Section is Present and Working as Expected in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the IBIS Models Section is Present and Working as Expected in RU Locale")]
        public void PDP_ToolsAndSimulations_VerifyIbisModelsSection(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + pdp_withIbisModels_url);

            if (util.CheckElement(driver, Elements.PDP_ToolsAndSimulations_Sections, 2))
            {
                int toolsAndSimulationsSections_count = util.GetCount(driver, Elements.PDP_ToolsAndSimulations_Sections);

                for (int ctr = 1; ctr <= toolsAndSimulationsSections_count; ctr++)
                {
                    if (util.GetText(driver, By.CssSelector("section[id$='tools']>div>div[class='data-container']:nth-last-of-type(1)>div:nth-last-of-type(1)>div:nth-of-type(" + ctr + ")>h3")).Contains(ibisModels_txt))
                    {
                        //----- R13 > T3: Verify the IBIS Models Section -----//

                        //--- Expected Result: The title "IBIS Models" must be displayed ---//
                        if (Locale.Equals("en"))
                        {
                            test.validateStringIsCorrect(driver, By.CssSelector("section[id$='tools']>div>div[class='data-container']:nth-last-of-type(1)>div:nth-last-of-type(1)>div:nth-of-type(" + ctr + ")>h3"), ibisModels_txt);
                        }
                        else
                        {
                            test.validateElementIsPresent(driver, By.CssSelector("section[id$='tools']>div>div[class='data-container']:nth-last-of-type(1)>div:nth-last-of-type(1)>div:nth-of-type(" + ctr + ")>h3"));
                        }

                        //--- Expected Result: There should be available IBS link(s) displayed. ---//
                        test.validateElementIsPresent(driver, By.CssSelector("section[id$='tools']>div>div[class='data-container']:nth-last-of-type(1)>div:nth-last-of-type(1)>div:nth-of-type(" + ctr + ") * a[class$='item-link']"));

                        break;
                    }
                }
            }
        }

        [Test, Category("PDP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Design Tools Section is Present and Working as Expected in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Design Tools Section is Present and Working as Expected in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Design Tools Section is Present and Working as Expected in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Design Tools Section is Present and Working as Expected in RU Locale")]
        public void PDP_ToolsAndSimulations_VerifyDesignToolsSection(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + pdp_withDesignTools_url);

            if (util.CheckElement(driver, Elements.PDP_ToolsAndSimulations_Sections, 2))
            {
                int toolsAndSimulationsSections_count = util.GetCount(driver, Elements.PDP_ToolsAndSimulations_Sections);

                for (int ctr = 1; ctr <= toolsAndSimulationsSections_count; ctr++)
                {
                    if (util.GetText(driver, By.CssSelector("section[id$='tools']>div>div[class='data-container']:nth-last-of-type(1)>div:nth-last-of-type(1)>div:nth-of-type(" + ctr + ")>h3")).Contains(designTools_txt))
                    {
                        //----- R13 > T5.1: Verify the Design Tools Section -----//

                        //--- Expected Result: The title "Design Tools" must be displayed ---//
                        if (Locale.Equals("en"))
                        {
                            test.validateStringIsCorrect(driver, By.CssSelector("section[id$='tools']>div>div[class='data-container']:nth-last-of-type(1)>div:nth-last-of-type(1)>div:nth-of-type(" + ctr + ")>h3"), designTools_txt);
                        }
                        else
                        {
                            test.validateElementIsPresent(driver, By.CssSelector("section[id$='tools']>div>div[class='data-container']:nth-last-of-type(1)>div:nth-last-of-type(1)>div:nth-of-type(" + ctr + ")>h3"));
                        }

                        //--- Expected Result: There should be available link(s) displayed for software package download. ---//
                        test.validateElementIsPresent(driver, By.CssSelector("section[id$='tools']>div>div[class='data-container']:nth-last-of-type(1)>div:nth-last-of-type(1)>div:nth-of-type(" + ctr + ") * a[class$='item-link']"));

                        break;
                    }
                }
            }
        }

        [Test, Category("PDP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the S-Parameters Section is Present and Working as Expected in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the S-Parameters Section is Present and Working as Expected in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the S-Parameters Section is Present and Working as Expected in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the S-Parameters Section is Present and Working as Expected in RU Locale")]
        public void PDP_ToolsAndSimulations_VerifSParametersSection(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + pdp_withSParameters_url);

            if (util.CheckElement(driver, Elements.PDP_ToolsAndSimulations_Sections, 2))
            {
                int toolsAndSimulationsSections_count = util.GetCount(driver, Elements.PDP_ToolsAndSimulations_Sections);

                for (int ctr = 1; ctr <= toolsAndSimulationsSections_count; ctr++)
                {
                    if (util.GetText(driver, By.CssSelector("section[id$='tools']>div>div[class='data-container']:nth-last-of-type(1)>div:nth-last-of-type(1)>div:nth-of-type(" + ctr + ")>h3")).Contains(sParameters_txt))
                    {
                        //----- R13 > T6: Verify the S-Parameters Section -----//

                        //--- Expected Result: The title "S-Parameters" must be displayed ---//
                        if (Locale.Equals("en"))
                        {
                            test.validateStringIsCorrect(driver, By.CssSelector("section[id$='tools']>div>div[class='data-container']:nth-last-of-type(1)>div:nth-last-of-type(1)>div:nth-of-type(" + ctr + ")>h3"), sParameters_txt);
                        }
                        else
                        {
                            test.validateElementIsPresent(driver, By.CssSelector("section[id$='tools']>div>div[class='data-container']:nth-last-of-type(1)>div:nth-last-of-type(1)>div:nth-of-type(" + ctr + ")>h3"));
                        }

                        //--- Expected Result: There should be available S-Parameters link(s) displayed. ---//
                        test.validateElementIsPresent(driver, By.CssSelector("section[id$='tools']>div>div[class='data-container']:nth-last-of-type(1)>div:nth-last-of-type(1)>div:nth-of-type(" + ctr + ") * a[class$='item-link']"));

                        break;
                    }
                }
            }
        }

        [Test, Category("PDP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Software Development Kit Section is Present and Working as Expected in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Software Development Kit Section is Present and Working as Expected in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Software Development Kit Section is Present and Working as Expected in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Software Development Kit Section is Present and Working as Expected in RU Locale")]
        public void PDP_ToolsAndSimulations_VerifySoftwareDevelopmentKitSection(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + pdp_withSoftwareDevelopmentKit_url);

            if (util.CheckElement(driver, Elements.PDP_ToolsAndSimulations_Sections, 2))
            {
                int toolsAndSimulationsSections_count = util.GetCount(driver, Elements.PDP_ToolsAndSimulations_Sections);

                for (int ctr = 1; ctr <= toolsAndSimulationsSections_count; ctr++)
                {
                    if (util.GetText(driver, By.CssSelector("section[id$='tools']>div>div[class='data-container']:nth-last-of-type(1)>div:nth-last-of-type(1)>div:nth-of-type(" + ctr + ")>h3")).Contains(softwareDevelopmentKit_txt))
                    {
                        //----- R13 > T3: Verify the Software Development Kit Section -----//

                        //--- Expected Result: Software Development kit Title is available ---//
                        test.validateElementIsPresent(driver, By.CssSelector("section[id$='tools']>div>div[class='data-container']:nth-last-of-type(1)>div:nth-last-of-type(1)>div:nth-of-type(" + ctr + ")>h3"));

                        //--- Expected Result: Downloadable zip file is available  ---//
                        test.validateElementIsPresent(driver, By.CssSelector("section[id$='tools']>div>div[class='data-container']:nth-last-of-type(1)>div:nth-last-of-type(1)>div:nth-of-type(" + ctr + ") * a[class$='item-link']"));

                        break;
                    }
                }
            }
        }

        [Test, Category("PDP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the LTspice Section is Present and Working as Expected in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the LTspice Section is Present and Working as Expected in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the LTspice Section is Present and Working as Expected in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the LTspice Section is Present and Working as Expected in RU Locale")]
        public void PDP_ToolsAndSimulations_VerifyLtSpice_Section(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + pdp_withLtSpice_url);

            if (util.CheckElement(driver, Elements.PDP_ToolsAndSimulations_Sections, 2))
            {
                int toolsAndSimulationsSections_count = util.GetCount(driver, Elements.PDP_ToolsAndSimulations_Sections);

                for (int ctr = 2; ctr <= toolsAndSimulationsSections_count; ctr++)
                {
                    if (util.GetText(driver, By.CssSelector("section[id$='tools']>div>div:nth-of-type(" + ctr + "))>div[class$='product-row'] * h2")).Contains(ltSpice_txt))
                    {
                        //----- R13 > T13: Verify the missing LTspice models in LTC products -----//

                        //--- Expected Result: LTspice models under "Models for the following parts are available in LTspice:" should be displayed. ---//
                        test.validateElementIsPresent(driver, By.CssSelector("section[id$='tools']>div>div:nth-of-type(" + ctr + ")>div[class$='models'] * h4"));

                        break;
                    }
                }
            }
        }
    }
}