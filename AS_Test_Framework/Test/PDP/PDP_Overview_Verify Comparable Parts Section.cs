﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Threading;

namespace AS_Test_Framework.PDP
{
    [TestFixture]
    public class PDP_Overview_ComparablePartsSection : BaseSetUp
    {
        public PDP_Overview_ComparablePartsSection() : base() { }

        //--- URLs ---//
        //string pdp_url = "/ad7705";
        string pdp_url = "/ad7706";

        //--- Labels ---//
        string comparableParts_txt = "Comparable Parts";

        [Test, Category("PDP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Comparable Parts Section is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Comparable Parts Section is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Comparable Parts Section is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Comparable Parts Section is Present and Working as Expected in RU Locale")]
        public void PDP_Overview_VerifyComparablePartsSection(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + pdp_url);
            Thread.Sleep(1000);

            //--- Expected Result: "Comparable Parts" under Features & Benefits/Product Details must be shown for ANALOG product. ---//
            if (Locale.Equals("en"))
            {
                test.validateStringInstance(driver, comparableParts_txt, util.GetText(driver, Elements.PDP_ComparableParts_Txt));
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.PDP_ComparableParts_Txt);
            }

            //--- Expected Result: "Click to see all in Parametric Search" link beside Comparable Parts title should be displayed. ---//
            test.validateElementIsPresent(driver, Elements.PDP_ClickToSeeAllInParametricSearch_Link);

            if (util.CheckElement(driver, Elements.PDP_ClickToSeeAllInParametricSearch_Link, 2))
            {
                //--- Action: Click the link "Click to see all in Parametric Search" ---//
                action.IOpenLinkInNewTab(driver, Elements.PDP_ClickToSeeAllInParametricSearch_Link);
                Thread.Sleep(1000);

                //--- Expected Result: Page should redirect to related parametric search table. ---//
                test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + "/parametricsearch/");
            }
        }
    }
}