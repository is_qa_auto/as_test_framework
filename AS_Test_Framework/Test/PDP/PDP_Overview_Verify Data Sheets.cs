﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Threading;

namespace AS_Test_Framework.PDP
{
    [TestFixture]
    public class PDP_Overview_DataSheets : BaseSetUp
    {
        public PDP_Overview_DataSheets() : base() { }

        //--- URLs ---//
        string pdp_url_withDataSheetCard = "/ad2s1210";
        string pdp_url_withUserGuidesCard = "/ad7960";
        string documentation_sec_url = "#product-documentation";
        string pdp_url_withCircuitNoteCard = "/ad7960";
        string referenceDesigns_sec_url = "#product-designs";
        string pdp_url_withIbisModelsCard = "/ad7960";
        string toolsAndSimulations_sec_url = "#product-tools";
        string pdp_url_withFpdgaHdlCard = "/ad7960";
        string softwareAndSystemsRequirements_sec_url = "#product-requirement";

        [Test, Category("PDP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Data Sheet Card is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Data Sheet Card is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Data Sheet Card is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Data Sheet Card is Present and Working as Expected in RU Locale")]
        public void PDP_Overview_VerifyDataSheetCard(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + pdp_url_withDataSheetCard);
            Thread.Sleep(1000);

            //----- R5 > T1: Verify Data Sheet card -----//

            //--- Expected Result: "Data Sheet" card/box under navigational menus at the top left portion of the page should be displayed. ---//
            test.validateElementIsPresent(driver, Elements.PDP_DataSheet_Card);

            if (util.CheckElement(driver, Elements.PDP_DataSheet_Card, 2))
            {
                //----- R5 > T2/T3/T4/T5: Verify Data Sheet Revision -----//

                //--- Expected Result: Data Sheet link should be present. ---//
                test.validateElementIsPresent(driver, Elements.PDP_DataSheet_Card_DataSheet_Link);

                //--- Expected Result: Revision Number (e.g. Rev. C) label beside Data Sheet link should be present. ---//
                test.validateElementIsPresent(driver, Elements.PDP_DataSheet_Card_RevNo_Txt);

                //--- Expected Result: Revision Number (e.g. Rev. C) Label should be present beside each Regional Version Link of Data Sheet. ---//
                if (util.CheckElement(driver, By.CssSelector("section[id$='overview'] * div[class='locales']"), 2))
                {
                    test.validateElementIsPresent(driver, Elements.PDP_RegionalVersion_RevNo_Txt);
                }

                if (!Locale.Equals("en"))
                {
                    //--- Expected Result: (产品技术资料帮助) / データシートご利用上の注意 / Помощь по техническим описаниям (Product Technical Information Help) should be present. ---//
                    test.validateElementIsPresent(driver, Elements.PDP_ProductInfoHelp_Link);

                    if (util.CheckElement(driver, Elements.PDP_ProductInfoHelp_Link, 2))
                    {
                        //--- Action: Click on (产品技术资料帮助) / データシートご利用上の注意 / Помощь по техническим описаниям (Product Technical Information Help) link ---//
                        action.IClick(driver, Elements.PDP_ProductInfoHelp_Link);

                        //--- Expected Result: 产品技术资料帮助 / 利用上の注意 / ПОМОЩЬ ПО ТЕХНИЧЕСКИМ ОПИСАНИЯМ modal box with help information should be displayed properly in selected locale ---//
                        action.IClick(driver, Elements.PDP_ProductInfoHelp_Modal);
                    }
                }
            }
        }

        [Test, Category("PDP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Subtype Data Sheet is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Subtype Data Sheet is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Subtype Data Sheet is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Subtype Data Sheet is Present and Working as Expected in RU Locale")]
        public void PDP_Overview_VerifySubtypeDataSheet(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + pdp_url_withDataSheetCard);

            //----- R5 > T13: Verify Sub type Data sheets (IQ-3853/AL-10407) -----//
            test.validateElementIsPresent(driver, Elements.PDP_SubtypeDataSheet);

            if (util.CheckElement(driver, Elements.PDP_SubtypeDataSheet, 2))
            {
                //--- Expected Result: Subtype Data sheet should have a drop down arrow ---//
                test.validateElementIsPresent(driver, Elements.PDP_SubtypeDataSheet_DropdownArrow_Up_Icon);

                //--- Expected Result: A circle should be displayed above the arrow dropdown indicating the number of data sheet available in the documentation section ---//
                test.validateElementIsPresent(driver, Elements.PDP_SubtypeDataSheet_Counter_Txt);

                //--- Action: Click the arrow drowp down ---//
                action.IClick(driver, Elements.PDP_SubtypeDataSheet_DropdownArrow_Up_Icon);

                //--- Expected Result: Subtype data sheet should be displayed ---//
                test.validateElementIsPresent(driver, Elements.PDP_SubtypeDataSheet_Menu);

                //--- Action: Click again the arrow drop down ---//
                action.IClick(driver, Elements.PDP_SubtypeDataSheet_DropdownArrow_Down_Icon);

                //--- Expected Result: The subtype data sheet should be collapsed ---//
                test.validateElementIsNotPresent(driver, Elements.PDP_SubtypeDataSheet_Menu);
            }
        }

        [Test, Category("PDP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the User Guides Card is Present and Working as Expected in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the User Guides Card is Present and Working as Expected in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the User Guides Card is Present and Working as Expected in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the User Guides Card is Present and Working as Expected in RU Locale")]
        public void PDP_Overview_VerifyUserGuidesCard(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + pdp_url_withUserGuidesCard);
            Thread.Sleep(1000);

            if (util.CheckElement(driver, Elements.PDP_UserGuides_Link, 2))
            {
                int dataSheets_count = util.GetCount(driver, By.CssSelector("section[id$='overview'] * div[class$='datasheets']>div"));

                for (int ctr = 2; ctr <= dataSheets_count; ctr++)
                {
                    if (util.GetText(driver, By.CssSelector("section[id$='overview'] * div[class$='datasheets']>div:nth-of-type(" + ctr + ") * div[class='title'] * span")).Equals("User Guides"))
                    {
                        //----- R5 > T7: Verify Users Guide card/box (IQ-3855/AL-10332) -----//

                        //--- Expected Result: Counter icon is displayed at the bottom right side of the Users Guide card displaying the number of available documents. ---//
                        test.validateElementIsPresent(driver, By.CssSelector("section[id$='overview'] * div[class$='datasheets']>div:nth-of-type(" + ctr + ") * span[class='count']"));

                        //--- Expected Result: View All under counter icon must be displayed. ---//
                        test.validateElementIsPresent(driver, By.CssSelector("section[id$='overview'] * div[class$='datasheets']>div:nth-of-type(" + ctr + ") * span[class='viewAll']"));

                        //--- Action: Click on View All ---//
                        action.IClick(driver, By.CssSelector("section[id$='overview'] * div[class$='datasheets']>div:nth-of-type(" + ctr + ") * span[class='viewAll']"));

                        //--- Expected Result: The page should anchor down to "Documentation" section. ---//
                        test.validateStringInstance(driver, driver.Url, documentation_sec_url);

                        break;
                    }

                    if (ctr == dataSheets_count)
                    {
                        test.validateElementIsPresent(driver, By.CssSelector("section[id$='overview'] * div[class$='datasheets']>div:nth-of-type(" + (ctr + 1) + ")"));
                    }
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.PDP_UserGuides_Link);
            }
        }

        [Test, Category("PDP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Circuit Note Card is Present and Working as Expected in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Circuit Note Card is Present and Working as Expected in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Circuit Note Card is Present and Working as Expected in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Circuit Note Card is Present and Working as Expected in RU Locale")]
        public void PDP_Overview_VerifyCircuitNoteCard(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + pdp_url_withCircuitNoteCard);

            if (util.CheckElement(driver, Elements.PDP_CircuitNote_Link, 2))
            {
                int dataSheets_count = util.GetCount(driver, By.CssSelector("section[id$='overview'] * div[class$='datasheets']>div"));

                for (int ctr = 2; ctr <= dataSheets_count; ctr++)
                {
                    if (util.GetText(driver, By.CssSelector("section[id$='overview'] * div[class$='datasheets']>div:nth-of-type(" + ctr + ") * div[class='title'] * span")).Equals("Circuit Note"))
                    {
                        //----- R5 > T8: Verify Circuit Note card/box (IQ-3855/AL-10332) -----//

                        //--- Expected Result: Counter icon is displayed at the bottom right side of the Circuit ---//
                        test.validateElementIsPresent(driver, By.CssSelector("section[id$='overview'] * div[class$='datasheets']>div:nth-of-type(" + ctr + ") * span[class='count']"));

                        //--- Expected Result: View All under counter icon must be displayed. ---//
                        test.validateElementIsPresent(driver, By.CssSelector("section[id$='overview'] * div[class$='datasheets']>div:nth-of-type(" + ctr + ") * span[class='viewAll']"));

                        //--- Action: Click on View All ---//
                        action.IClick(driver, By.CssSelector("section[id$='overview'] * div[class$='datasheets']>div:nth-of-type(" + ctr + ") * span[class='viewAll']"));

                        //--- Expected Result: The page should anchor down to "Reference Designs" section. ---//
                        test.validateStringInstance(driver, driver.Url, referenceDesigns_sec_url);

                        break;
                    }

                    if (ctr == dataSheets_count)
                    {
                        test.validateElementIsPresent(driver, By.CssSelector("section[id$='overview'] * div[class$='datasheets']>div:nth-of-type(" + (ctr + 1) + ")"));
                    }
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.PDP_CircuitNote_Link);
            }
        }

        [Test, Category("PDP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the IBIS Models Card is Present and Working as Expected in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the IBIS Models Card is Present and Working as Expected in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the IBIS Models Card is Present and Working as Expected in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the IBIS Models Card is Present and Working as Expected in RU Locale")]
        public void PDP_Overview_VerifyIbisModelsCard(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + pdp_url_withIbisModelsCard);

            if (util.CheckElement(driver, Elements.PDP_IbisModels_Link, 2))
            {
                int dataSheets_count = util.GetCount(driver, By.CssSelector("section[id$='overview'] * div[class$='datasheets']>div"));

                for (int ctr = 2; ctr <= dataSheets_count; ctr++)
                {
                    if (util.GetText(driver, By.CssSelector("section[id$='overview'] * div[class$='datasheets']>div:nth-of-type(" + ctr + ") * div[class='title'] * span")).Equals("IBIS Models"))
                    {
                        //----- R5 > T10: Verify IBS Models card/box (IQ-3855/AL-10332) -----//

                        //--- Action: Click on View All ---//
                        action.IClick(driver, By.CssSelector("section[id$='overview'] * div[class$='datasheets']>div:nth-of-type(" + ctr + ") * span[class='viewAll']"));

                        //--- Expected Result: The page should anchor down to "Tools & Simuations" section. ---//
                        test.validateStringInstance(driver, driver.Url, toolsAndSimulations_sec_url);

                        break;
                    }

                    if (ctr == dataSheets_count)
                    {
                        test.validateElementIsPresent(driver, By.CssSelector("section[id$='overview'] * div[class$='datasheets']>div:nth-of-type(" + (ctr + 1) + ")"));
                    }
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.PDP_IbisModels_Link);
            }
        }

        [Test, Category("PDP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the FPGA/HDL Card is Present and Working as Expected in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the FPGA/HDL Card is Present and Working as Expected in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the FPGA/HDL Card is Present and Working as Expected in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the FPGA/HDL Card is Present and Working as Expected in RU Locale")]
        public void PDP_Overview_VerifyFpgaHdlCard(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + pdp_url_withFpdgaHdlCard);

            if (util.CheckElement(driver, Elements.PDP_FpgaHdl_Link, 2))
            {
                int dataSheets_count = util.GetCount(driver, By.CssSelector("section[id$='overview'] * div[class$='datasheets']>div"));

                for (int ctr = 2; ctr <= dataSheets_count; ctr++)
                {
                    if (util.GetText(driver, By.CssSelector("section[id$='overview'] * div[class$='datasheets']>div:nth-of-type(" + ctr + ") * div[class='title'] * span")).Equals("FPGA/HDL"))
                    {
                        //----- R5 > T10_1: Verify FPGA/HDL card/box (IQ-3855/AL-10332) -----//

                        //--- Action: Click on View All ---//
                        action.IClick(driver, By.CssSelector("section[id$='overview'] * div[class$='datasheets']>div:nth-of-type(" + ctr + ") * span[class='viewAll']"));

                        //--- Expected Result: The page should anchor to Software & Systems Requirements ---//
                        test.validateStringInstance(driver, driver.Url, softwareAndSystemsRequirements_sec_url);

                        break;
                    }

                    if (ctr == dataSheets_count)
                    {
                        test.validateElementIsPresent(driver, By.CssSelector("section[id$='overview'] * div[class$='datasheets']>div:nth-of-type(" + (ctr + 1) + ")"));
                    }
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.PDP_FpgaHdl_Link);
            }
        }
    }
}