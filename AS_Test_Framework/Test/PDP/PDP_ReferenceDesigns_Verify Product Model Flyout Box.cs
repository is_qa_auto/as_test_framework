﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.PDP
{
    [TestFixture]
    public class PDP_ReferenceDesigns_ProductModelFlyoutBox : BaseSetUp
    {
        public PDP_ReferenceDesigns_ProductModelFlyoutBox() : base() { }

        //--- URLs ---//
        string pdp_url = "/ad7960";

        //--- Labels ---//
        string featuresAndBenefits_txt = "Features & Benefits";
        string partsUsed_txt = "Parts Used";
        string resources_txt = "Resources";

        [Test, Category("PDP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Product Model Flyout Box is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Product Model Flyout Box is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Product Model Flyout Box is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Product Model Flyout Box is Present and Working as Expected in RU Locale")]
        public void PDP_ReferenceDesigns_VerifyProductModelFlyoutBox(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + pdp_url);

            //----- R16 > T8: Verify Reference Design product model fly out box -----//

            if (!util.CheckElement(driver, Elements.PDP_ReferenceDesigns_ProductModel_FlyOut_Box, 2))
            {
                //--- Action: Select and click any reference design model(s). ---//
                action.IClick(driver, Elements.PDP_ReferenceDesigns);
            }

            //--- Expected Result: A fly out box should be shown displaying the selected reference design model with more detail information. ---//
            test.validateElementIsPresent(driver, Elements.PDP_ReferenceDesigns_ProductModel_FlyOut_Box);

            if (util.CheckElement(driver, Elements.PDP_ReferenceDesigns_ProductModel_FlyOut_Box, 2))
            {
                //----- R16 > T9: Verify Reference Design product images in fly out box -----//

                //--- Expected Result: There should be available thumbnail image(s) at the top left side of the reference design model fly out box. ---//
                test.validateElementIsPresent(driver, Elements.PDP_ReferenceDesigns_ProductModel_FlyOut_Box_Thumbnail_Imgs);

                if (util.CheckElement(driver, Elements.PDP_ReferenceDesigns_ProductModel_FlyOut_Box_Thumbnail_Imgs, 2))
                {
                    //--- Expected Result: A maximum of 3 thumbnail images must be displayed. ---//
                    test.validateCountIsLessOrEqual(driver, util.GetCount(driver, Elements.PDP_ReferenceDesigns_ProductModel_FlyOut_Box_Thumbnail_Imgs), 3);

                    //--- Action: Click any of the thumbnail image(s). ---//
                    action.IClick(driver, By.CssSelector("section[id$='designs'] * div[style=''] * div[class*='thumbnail-img']>div:nth-last-of-type(1)"));
                    Thread.Sleep(3000);

                    //--- Expected Result: User can select any of the thumbnail image(s). ---//
                    test.validateElementIsPresent(driver, By.CssSelector("section[id$='designs'] * div[style=''] * div[class*='thumbnail-img']>div:nth-last-of-type(1)>a[class='active']"));

                    //--- Expected Result: The selected/clicked image should be displayed in larger size beside the thumbnail image(s). ---//
                    test.validateElementIsPresent(driver, Elements.PDP_ReferenceDesigns_ProductModel_FlyOut_Box_Product_Img);
                }

                //----- R16 > T10: Verify Reference Design Model Number in fly out box -----//

                //--- Expected Result: The reference design model should be displayed at the top right side of the reference design fly out box. ---//
                test.validateElementIsPresent(driver, Elements.PDP_ReferenceDesigns_ProductModel_FlyOut_Box_ModelNo_Txt);

                //----- R16 > T11: Verify Reference Design Model Name in fly out box -----//

                //--- Expected Result: The reference design model name should be displayed beside reference design model number. ---//
                test.validateElementIsPresent(driver, Elements.PDP_ReferenceDesigns_ProductModel_FlyOut_Box_ModelName_Txt);

                //----- R16 > T12: Verify "View Detailed Reference Design Information" button in fly out box -----//

                //--- Expected Result: The button "View Detailed Reference Design Information" should be displayed below reference design model number and model name. ---//
                test.validateElementIsPresent(driver, Elements.PDP_ReferenceDesigns_ProductModel_FlyOut_Box_ViewDetailedReferenceDesignInfo_Btn);

                //----- R16 > T15: Verify Features & Benefits section in fly out box -----//

                //--- Expected Result: "Features & Benefits" must be shown below thumbnail images at the lower left corner of the reference design product model fly out box. ---//
                if (Locale.Equals("en"))
                {
                    test.validateStringInstance(driver, featuresAndBenefits_txt, util.GetText(driver, Elements.PDP_ReferenceDesigns_ProductModel_FlyOut_Box_FeaturesAndBenefits_Sec_Lbl));
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.PDP_ReferenceDesigns_ProductModel_FlyOut_Box_FeaturesAndBenefits_Sec_Lbl);
                }

                //--- Expected Result: Details/information under "Features & Benefits" should be in bullet format. ---//
                test.validateElementIsPresent(driver, Elements.PDP_ReferenceDesigns_ProductModel_FlyOut_Box_FeaturesAndBenefits_Sec_Info);

                //----- R16 > T16: Verify Parts Used section in fly out box -----//

                //--- Expected Result: "Parts Used" must be shown below reference design description at the lower right corner of the reference design product model fly out box. ---//
                if (Locale.Equals("en"))
                {
                    test.validateStringInstance(driver, partsUsed_txt, util.GetText(driver, Elements.PDP_ReferenceDesigns_ProductModel_FlyOut_Box_PartsUsed_Sec_Lbl));
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.PDP_ReferenceDesigns_ProductModel_FlyOut_Box_PartsUsed_Sec_Lbl);
                }

                //--- Expected Result: Available link(s) under "Parts Used" should be in bullet format. ---//
                test.validateElementIsPresent(driver, Elements.PDP_ReferenceDesigns_ProductModel_FlyOut_Box_PartsUsed_Links);

                //--- Expected Result: Bullets should be a badge/icon. ---//
                test.validateElementIsPresent(driver, Elements.PDP_ReferenceDesigns_ProductModel_FlyOut_Box_PartsUsed_Icons);

                if (util.CheckElement(driver, Elements.PDP_ReferenceDesigns_ProductModel_FlyOut_Box_PartsUsed_Links, 2))
                {
                    //--- Action: Click any of the available link(s). ---//
                    action.IOpenLinkInNewTab(driver, Elements.PDP_ReferenceDesigns_ProductModel_FlyOut_Box_PartsUsed_Links);
                    Thread.Sleep(2000);

                    //--- Expected Result: Page should redirect to related product landing page. ---//
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + "/products/");

                    driver.Close();
                    driver.SwitchTo().Window(driver.WindowHandles.First());
                }

                //----- R16 > T17: Verify Resources section in fly out box -----//

                //--- Expected Result: "Resources" must be shown below reference design model description at the lower right corner of the reference design product model fly out box. ---//
                if (Locale.Equals("en"))
                {
                    test.validateStringInstance(driver, resources_txt, util.GetText(driver, Elements.PDP_ReferenceDesigns_ProductModel_FlyOut_Box_Resources_Sec_Lbl));
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.PDP_ReferenceDesigns_ProductModel_FlyOut_Box_Resources_Sec_Lbl);
                }

                //--- Expected Result: Available link(s) under "Resources" should be in bullet format. ---//
                test.validateElementIsPresent(driver, Elements.PDP_ReferenceDesigns_ProductModel_FlyOut_Box_Resources_Links);

                //--- Expected Result: Bullet should be a badge/icon. ---//
                test.validateElementIsPresent(driver, Elements.PDP_ReferenceDesigns_ProductModel_FlyOut_Box_Resources_Download_Icons);

                test.validateElementIsPresent(driver, Elements.PDP_ReferenceDesigns_ProductModel_FlyOut_Box_Resources_File_Icons);
            }
        }
    }
}