﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Threading;

namespace AS_Test_Framework.PDP
{
    [TestFixture]
    public class PDP_Overview_HeaderTitle : BaseSetUp
    {
        public PDP_Overview_HeaderTitle() : base() { }

        //--- URLs ---//
        string pdp_url = "/ad7705";
        //string pdp_url = "/ad7706";
        string obsolete_pdp_url = "/ad803";
        string pdp_overview_url = "#product-overview";

        //--- Labels ---//
        string overview_txt = "Overview";

        [Test, Category("PDP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Overview Header Title is Present and Correct in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Overview Header Title is Present in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Overview Header Title is Present in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Overview Header Title is Present in RU Locale")]
        public void PDP_Overview_VerifyHeaderTitle(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + pdp_url);
            Thread.Sleep(1000);

            //----- R4 > T16: Verify STICKY HEADER (Navigational Menus) are clickable -----//
            if (util.CheckElement(driver, Elements.PDP_StickyHeader_Overview, 2))
            {
                //--- Action: Click Overview on sticky header (navigational menus) ---//
                action.IClick(driver, Elements.PDP_StickyHeader_Overview);
                Thread.Sleep(3000);

                //--- Expected Result: It should anchor to Data Sheets and Product Images section within the product detail page. ---//
                test.validateStringInstance(driver, driver.Url, pdp_overview_url);
            }

            //----- R8 > T1: Verify Overview header title -----//

            //--- Expected Result: The header title "Overview" must be displayed at the top left portion of the page. ---//
            if (Locale.Equals("en"))
            {
                test.validateStringInstance(driver, overview_txt, util.GetText(driver, Elements.PDP_Overview_Section_Lbl));
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.PDP_Overview_Section_Lbl);
            }
        }

        [Test, Category("PDP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Overview Header Title is Present and Correct in an Obsolete PDP in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Overview Header Title is Present in an Obsolete PDP in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Overview Header Title is Present in an Obsolete PDP in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Overview Header Title is Present in an Obsolete PDP in RU Locale")]
        public void PDP_Overview_VerifyHeaderTitleInAnObsoletePdp(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + obsolete_pdp_url);
            Thread.Sleep(1000);

            //----- PDP - Obosolete Test Plan > R2 > T3: Verify navigational menus are clickable -----//
            if (util.CheckElement(driver, Elements.PDP_StickyHeader_Overview, 2))
            {
                //--- Action: Click "Overview" on navigational menus ---//
                action.IClick(driver, Elements.PDP_StickyHeader_Overview);
                Thread.Sleep(3000);

                //--- Expected Result: Each navigational buttons must anchor on its respective sections within the page upon clicking. ---//
                test.validateStringInstance(driver, driver.Url, pdp_overview_url);
            }

            //----- PDP - Obosolete Test Plan > R5 > T1: Verify Overview header title -----//

            //--- Expected Result: The header title "Overview" must be displayed at the top left portion of the page. ---//
            if (Locale.Equals("en"))
            {
                test.validateStringInstance(driver, overview_txt, util.GetText(driver, Elements.PDP_Overview_Section_Lbl));
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.PDP_Overview_Section_Lbl);
            }
        }
    }
}