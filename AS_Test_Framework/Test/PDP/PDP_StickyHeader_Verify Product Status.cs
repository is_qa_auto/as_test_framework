﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Threading;

namespace AS_Test_Framework.PDP
{
    [TestFixture]
    public class PDP_StickyHeader_VerifyProductStatus : BaseSetUp
    {
        public PDP_StickyHeader_VerifyProductStatus() : base() { }

        //--- URLs ---//
        //string pdp_productionStatus_url = "/ad7705";
        string pdp_productionStatus_url = "/ad7706";
        string pdp_recommendedForNewDesigns_url = "/ad7616";
        string obsolete_pdp_url = "/ad803";
        //string productLifeCycleInfo_page_url = "/about-adi/sustainability/product-life-cycle-information.html";
        string productLifeCycleInfo_page_url = "/product-life-cycle-information.html";

        [Test, Category("PDP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Production Link is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Production Link is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Production Link is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Production Link is Present and Working as Expected in RU Locale")]
        public void PDP_StickyHeader_VerifyProductLink(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + pdp_productionStatus_url);

            if (util.CheckElement(driver, Elements.PDP_StickyHeader_Sec, 2))
            {
                //----- R4 > T6: Verify ANALOG product status "Production" (IQ-3262/AL-10386) -----//

                //--- Expected Result: Production status Icon should be displayed ---//
                test.validateElementIsPresent(driver, Elements.PDP_StickyHeader_ProductStatus_Icon);

                //--- Expected Result: "Production" Analog product status link should be displayed beside product description. ---//
                test.validateElementIsPresent(driver, Elements.PDP_StickyHeader_ProductStatus_Link);

                if (util.CheckElement(driver, Elements.PDP_StickyHeader_ProductStatus_Link, 2))
                {
                    //--- Action: Click "Production" Analog product status link ---//
                    action.IOpenLinkInNewTab(driver, Elements.PDP_StickyHeader_ProductStatus_Link);
                    Thread.Sleep(3000);

                    //--- Expected Result: "Production" Analog product status link should redirect to Product Life Cycle Information landing page. ---//
                    //test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + productLifeCycleInfo_page_url);
                    test.validateStringInstance(driver, driver.Url, productLifeCycleInfo_page_url);
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.PDP_StickyHeader_Sec);
            }
        }

        [Test, Category("PDP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Recommended for New Designs Link is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Recommended for New Designs Link is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Recommended for New Designs Link is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Recommended for New Designs Link is Present and Working as Expected in RU Locale")]
        public void PDP_StickyHeader_VerifyRecommendedForNewDesignsLink(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + pdp_recommendedForNewDesigns_url);

            if (util.CheckElement(driver, Elements.PDP_StickyHeader_Sec, 2))
            {
                //----- R4 > T6: Verify ANALOG product status "Production" -----//

                //--- Expected Result: "Recommended for New Designs" Analog product status link should be displayed beside product description. ---//
                test.validateElementIsPresent(driver, Elements.PDP_StickyHeader_ProductStatus_Link);

                if (util.CheckElement(driver, Elements.PDP_StickyHeader_ProductStatus_Link, 2))
                {
                    //--- Action: Click "Recommended for New Designs" Analog product status link ---//
                    action.IOpenLinkInNewTab(driver, Elements.PDP_StickyHeader_ProductStatus_Link);
                    Thread.Sleep(3000);

                    //--- Expected Result: "Recommended for New Designs" Analog product status link should redirect to Product Life Cycle Information landing page. ---//
                    //test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + productLifeCycleInfo_page_url);
                    test.validateStringInstance(driver, driver.Url, productLifeCycleInfo_page_url);
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.PDP_StickyHeader_Sec);
            }
        }

        [Test, Category("PDP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Obsolete Link is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Obsolete Link is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Obsolete Link is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Obsolete Link is Present and Working as Expected in RU Locale")]
        public void PDP_StickyHeader_VerifyObsoleteLink(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + obsolete_pdp_url);

            if (util.CheckElement(driver, Elements.PDP_StickyHeader_Sec, 2))
            {
                //----- PDP - Obosolete Test Plan > R1 > T2: Verify ANALOG product status "Obsolete" -----//

                //--- Expected Result: Obsolete status Icon should be displayed ---//
                test.validateElementIsPresent(driver, Elements.ObsoletePDP_StickyHeader_ProductStatus_Link);

                //--- Expected Result: "Obsolete" product status link should be displayed beside product description. ---//
                test.validateElementIsPresent(driver, Elements.ObsoletePDP_StickyHeader_ProductStatus_Link);

                if (util.CheckElement(driver, Elements.ObsoletePDP_StickyHeader_ProductStatus_Link, 2))
                {
                    //--- Action: Click "Obsolete" product status link ---//
                    action.IOpenLinkInNewTab(driver, Elements.ObsoletePDP_StickyHeader_ProductStatus_Link);
                    Thread.Sleep(3000);

                    //--- Expected Result: "Obsolete" product status link should redirect to Product Life Cycle Information landing page. ---//
                    //test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + productLifeCycleInfo_page_url);
                    test.validateStringInstance(driver, driver.Url, productLifeCycleInfo_page_url);
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.PDP_StickyHeader_Sec);
            }
        }

        [Test, Category("PDP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that PDP section is present in EN Locale", Category = "Smoke_ADIWeb")]
        public void PDP_VerifySection(string Locale)
        {
            string initial_steps = "1. Go to " + Configuration.Env_Url + Locale + "/AD7960";
            string scenario = "";

            action.INavigate(driver, Configuration.Env_Url + Locale + "/AD7960");

            scenario = "Verify that overview section is present";
            test.validateElementIsPresentv2(driver, Elements.PDP_Overview_Section_Lbl, scenario, initial_steps);

            scenario = "Verify that Evaluation Kits section is present";
            test.validateElementIsPresentv2(driver, Elements.PDP_EvaluationKits_Section_Lbl, scenario, initial_steps);

            scenario = "Verify that Documentation section is present";
            test.validateElementIsPresentv2(driver, Elements.PDP_Documentation_Section_Lbl, scenario, initial_steps);

            scenario = "Verify that Software and System Requirements section is present";
            test.validateElementIsPresentv2(driver, Elements.PDP_SoftwareAndSystemsRequirements_Section_Lbl, scenario, initial_steps);

            scenario = "Verify that Tools And Simulation section is present";
            test.validateElementIsPresentv2(driver, Elements.PDP_ToolsAndSimulations_Section_Lbl, scenario, initial_steps);

            scenario = "Verify that Reference Designs section is present";
            test.validateElementIsPresentv2(driver, Elements.PDP_ReferenceDesigns_Section_Lbl, scenario, initial_steps);

            scenario = "Verify that Product Recommendation section is present";
            test.validateElementIsPresentv2(driver, Elements.PDP_ProductRecommendation_Secton, scenario, initial_steps);

            scenario = "Verify that Reference Materials section is present";
            test.validateElementIsPresentv2(driver, Elements.PDP_ReferenceMaterials_Sec_Lbl, scenario, initial_steps);

            scenario = "Verify that Design Resources section is present";
            test.validateElementIsPresentv2(driver, Elements.PDP_DesignResources_Section_Lbl, scenario, initial_steps);

            scenario = "Verify that Support and Discussions section is present";
            test.validateElementIsPresentv2(driver, Elements.PDP_SupportAndDiscussions_Sec_Lbl, scenario, initial_steps);

            scenario = "Verify that Sample & Buy section is present";
            test.validateElementIsPresentv2(driver, Elements.PDP_SampleAndBuy_Sec_Lbl, scenario, initial_steps);
        }
    }
}