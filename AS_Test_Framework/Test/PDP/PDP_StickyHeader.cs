﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Threading;

namespace AS_Test_Framework.PDP
{
    [TestFixture]
    public class PDP_StickyHeader : BaseSetUp
    {
        public PDP_StickyHeader() : base() { }

        //--- URLs ---//
        string pdp_url = "/ad7705";
        //string pdp_url = "/ad7706";
        string obsolete_pdp_url = "/ad7240";
        //string dataSheet_url = "/data-sheets/";
        string productOverview_sec_url = "#product-overview";

        [Test, Category("PDP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Sticky Header is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Sticky Header is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Sticky Header is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Sticky Header is Present and Working as Expected in RU Locale")]
        public void PDP_VerifyStickyHeader(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + pdp_url);

            if (util.CheckElement(driver, Elements.PDP_StickyHeader_Sec, 2))
            {
                //----- R4 > T1: Verify ANALOG product number and description -----//

                //--- Expected Result: Analog product part number must be shown. ---//
                test.validateElementIsPresent(driver, Elements.PDP_ProductNo_Txt);

                //----- R4 > T2: Verify ANALOG product short description -----//

                //--- Expected Result: Analog product SHORT description and/or specs beside product number/part number should be displayed. ---//
                test.validateElementIsPresent(driver, Elements.PDP_Desc_Txt);

                //----- R4 > T14: Verify STICKY HEADER (Navigational Menus) -----//

                //--- Expected Result: The sticky header (navigational menus) at the top of the page under product number/part number should be available. ---//
                test.validateElementIsPresent(driver, Elements.PDP_StickyHeader);

                //----- R4 > T17: Verify STICKY HEADER (Navigational Menus) should dock to top page upon mouse scroll (IQ-4207/AL-11579) -----//

                //--- Action: Scroll down to the last section of product detail page. ---//
                action.IMouseOverTo(driver, Elements.Footer);

                //--- Expected Result: Sticky Header (Navigational Menus) should dock to top page upon mouse scroll. ---//
                test.validateElementIsPresent(driver, Elements.PDP_StickyHeader);

                //--- Expected Result: Back to top button labelled as "TOP" must be shown above the sticky header (Discussions). ---//
                test.validateElementIsPresent(driver, Elements.PDP_Top_Btn);

                //----- R4 > T21: Verify Data Sheet link on sticky header upon mouse scroll -----//

                //--- Expected Result: Data Sheet link must be displayed on sticky header (navigational menus) beside product status. ---//
                test.validateElementIsPresent(driver, Elements.PDP_DataSheet_Link);

                ////--- Action: Click on Data Sheet link ---//
                //action.IOpenLinkInNewTab(driver, Elements.PDP_DataSheet_Link);

                ////--- Expected Result: A new tab should be opened showing the product related data sheet in pdf file. ---//
                //test.validateStringInstance(driver, driver.Url, dataSheet_url);

                //----- R4 > T23: Verify TOP button upon clicking navigational menus -----//

                if (util.CheckElement(driver, Elements.PDP_Top_Btn, 2))
                {
                    //--- Action: Click "TOP" button. ---//
                    action.IClick(driver, Elements.PDP_Top_Btn);
                    Thread.Sleep(4000);

                    //--- Expected Result: Sticky Header (Navigational Menus) should dock to top page upon mouse scroll. ---//
                    test.validateStringInstance(driver, driver.Url, productOverview_sec_url);
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.PDP_StickyHeader_Sec);
            }
        }

        [Test, Category("PDP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Sticky Header is Present and Working as Expected in an Obsolete PDP in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Sticky Header is Present and Working as Expected in an Obsolete PDP in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Sticky Header is Present and Working as Expected in an Obsolete PDP in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Sticky Header is Present and Working as Expected in an Obsolete PDP in RU Locale")]
        public void PDP_VerifyStickyHeaderInAnObsoletePdp(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + obsolete_pdp_url);

            //----- PDP - Obosolete Test Plan > R2 > T1: Verify navigational menus -----//

            //--- Expected Result: The navigational menus at the top of the page under product/part number should be available. ---//
            test.validateElementIsPresent(driver, Elements.PDP_StickyHeader);

            if (util.CheckElement(driver, Elements.PDP_StickyHeader, 2))
            {
                //----- PDP - Obosolete Test Plan > R2 > T4: Verify navigational menus should dock to the header upon mouse scroll -----//

                //--- Action: Scroll down to the last section of obsolete product detail page. ---//
                action.IMouseOverTo(driver, Elements.Footer);

                //--- Expected Result: Navigational menus should dock to the header upon mouse scroll. ---//
                test.validateElementIsPresent(driver, Elements.PDP_StickyHeader);

                ////----- PDP - Obosolete Test Plan > R2 > T5: Verify "Top" button functionality -----//

                ////--- Expected Result: Back to top button labelled as "TOP" must be shown above the navigational menu (at the top right portion of the page). ---// ---> Note: The Top is not displaying because the page is too small.
                //test.validateElementIsPresent(driver, Elements.PDP_Top_Btn);

                //if (util.CheckElement(driver, Elements.PDP_Top_Btn, 2))
                //{
                //    //--- Action: Click "TOP" button ---//
                //    action.IClick(driver, Elements.PDP_Top_Btn);

                //    //--- Expected Result: The page should anchor back to the top page. ---//
                //    test.validateStringInstance(driver, driver.Url, productOverview_sec_url);
                //}
            }
        }
    }
}