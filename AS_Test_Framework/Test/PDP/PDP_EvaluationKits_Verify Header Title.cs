﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Threading;

namespace AS_Test_Framework.PDP
{
    [TestFixture]
    public class PDP_EvaluationKits_HeaderTitle : BaseSetUp
    {
        public PDP_EvaluationKits_HeaderTitle() : base() { }

        //--- URLs ---//
        //string pdp_url = "/ad7705";
        string pdp_url = "/ad7706";
        //string pdp_url = "/ad9213";
        string pdp_evaluationKits_url = "#product-evaluationkit";

        //--- Labels ---//
        string evaluationKits_txt = "Evaluation Kits";

        [Test, Category("PDP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Evaluation Kits Header Title is Present and Correct in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Evaluation Kits Header Title is Present in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Evaluation Kits Header Title is Present in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Evaluation Kits Header Title is Present in RU Locale")]
        public void PDP_EvaluationKits_VerifyHeaderTitle(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + pdp_url);
            Thread.Sleep(2000);

            //----- R4 > T16: Verify STICKY HEADER (Navigational Menus) are clickable -----//
            if (util.CheckElement(driver, Elements.PDP_StickyHeader_EvaluationKits, 2))
            {
                //--- Action: Click Evaluation Kits on sticky header (navigational menus) ---//
                action.IClick(driver, Elements.PDP_StickyHeader_EvaluationKits);

                //--- Expected Result: It should anchor to Evaluation Kits section within the product detail page. ---//
                test.validateStringInstance(driver, driver.Url, pdp_evaluationKits_url);
            }

            //----- R9 > T1: Check the "Evaluation Kits" header title -----//

            if (util.CheckElement(driver, Elements.PDP_EvaluationKits_FlyOut, 2))
            {
                action.IClick(driver, Elements.PDP_EvaluationKits_FlyOut_X_Btn);
            }

            int evaluationKits_count = util.GetCount(driver, Elements.PDP_EvaluationKits);

            //--- Expected Result: The header title "Evaluation Kits" must be displayed at the top left portion of the page. ---//
            if (Locale.Equals("en"))
            {
                test.validateStringInstance(driver, evaluationKits_txt + " (" + evaluationKits_count + ")", util.GetText(driver, Elements.PDP_EvaluationKits_Section_Lbl));
            }
            else
            {
                test.validateStringInstance(driver, util.GetText(driver, Elements.PDP_EvaluationKits_Section_Lbl), "(" + evaluationKits_count + ")");
            }
        }
    }
}