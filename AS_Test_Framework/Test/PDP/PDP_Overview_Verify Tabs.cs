﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Threading;

namespace AS_Test_Framework.PDP
{
    [TestFixture]
    public class PDP_Overview_Tabs : BaseSetUp
    {
        public PDP_Overview_Tabs() : base() { }

        //--- URLs ---//
        //string pdp_url = "/ad7705";
        string pdp_url = "/ad7706";

        //--- Labels ---//
        string featuresAndBenefits_txt = "Features and Benefits";
        string productDetails_txt = "Product Details";

        [Test, Category("PDP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Features and Benefits is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Features and Benefits is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Features and Benefits is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Features and Benefits is Present and Working as Expected in RU Locale")]
        public void PDP_Overview_VerifyFeaturesAndBenefits(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + pdp_url);
            Thread.Sleep(1000);

            //----- R8 > T2: Verify Features and Benefits for ANALOG product -----//

            //--- Expected Result: "Features and Benefits" below Overview title must be shown for ANALOG product. ---//
            if (Locale.Equals("en"))
            {
                test.validateStringInstance(driver, featuresAndBenefits_txt, util.GetText(driver, Elements.PDP_FeaturesAndBenefits_Link));
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.PDP_FeaturesAndBenefits_Link);
            }

            //--- Expected Result: Content under "Features and Benefits" must be displayed. ---//
            test.validateElementIsPresent(driver, Elements.PDP_FeaturesAndBenefits_Content);

            //--- Expected Result: "Features and Benefits" should be clickable (when it is not selected). ---//
            action.IClick(driver, Elements.PDP_ProductDetails_Link);
            test.validateElementIsEnabled(driver, Elements.PDP_FeaturesAndBenefits_Link);
        }

        [Test, Category("PDP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Product Details is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Product Details is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Product Details is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Product Details is Present and Working as Expected in RU Locale")]
        public void PDP_Overview_VerifyProductDetails(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + pdp_url);

            //----- R8 > T3: Verify Features and Benefits font color upon mouse hover for ANALOG product -----//

            //--- Expected Result: "Product Details" should be clickable (when it is not selected). ---//
            test.validateElementIsEnabled(driver, Elements.PDP_ProductDetails_Link);

            //--- Action: Click on "Product Details" beside Features and Benefits. ---//
            action.IClick(driver, Elements.PDP_ProductDetails_Link);

            //--- Expected Result: Product Details content must be displayed. ---//
            test.validateElementIsPresent(driver, Elements.PDP_ProductDetails_Content);

            //----- R8 > T4: Verify Product Details for ANALOG product -----//

            //--- Expected Result: "Product Details" beside Features & Benefits should be displayed for ANALOG product. ---//
            if (Locale.Equals("en"))
            {
                test.validateStringInstance(driver, productDetails_txt, util.GetText(driver, Elements.PDP_ProductDetails_Link));
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.PDP_ProductDetails_Link);
            }
        }
    }
}