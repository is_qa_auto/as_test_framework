﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Threading;

namespace AS_Test_Framework.PDP
{
    [TestFixture]
    public class PDP_Documentation_HeaderTitle : BaseSetUp
    {
        public PDP_Documentation_HeaderTitle() : base() { }

        //--- URLs ---//
        //string pdp_url = "/ad7705";
        string pdp_url = "/ad7706";
        string obsolete_pdp_url = "/ad7240";
        string pdp_documentation_url = "#product-documentation";

        //--- Labels ---//
        string documentation_txt = "Documentation";

        [Test, Category("PDP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Documentation Header Title is Present and Correct in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Documentation Header Title is Present in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Documentation Header Title is Present in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Documentation Header Title is Present in RU Locale")]
        public void PDP_Documentation_VerifyHeaderTitle(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + pdp_url);

            //----- R4 > T16: Verify STICKY HEADER (Navigational Menus) are clickable -----//
            if (util.CheckElement(driver, Elements.PDP_StickyHeader_Documentation, 2))
            {
                //--- Action: Click Documentation on sticky header (navigational menus) ---//
                action.IClick(driver, Elements.PDP_StickyHeader_Documentation);

                //--- Expected Result: It should anchor to Documentation section within the product detail page. ---//
                test.validateStringInstance(driver, driver.Url, pdp_documentation_url);
            }

            //----- R11 > T1: Verify Documentation header title -----//

            //--- Expected Result: The header title "Documentation" must be displayed at the top left side of the page. ---//
            if (Locale.Equals("en"))
            {
                test.validateStringInstance(driver, documentation_txt, util.GetText(driver, Elements.PDP_Documentation_Section_Lbl));
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.PDP_Documentation_Section_Lbl);
            }
        }

        [Test, Category("PDP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Documentation Header Title is Present and Correct in an Obsolete PDP in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Documentation Header Title is Present in an Obsolete PDP in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Documentation Header Title is Present in an Obsolete PDP in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Documentation Header Title is Present in an Obsolete PDP in RU Locale")]
        public void PDP_Documentation_VerifyHeaderTitleInAnObsoletePdp(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + obsolete_pdp_url);
            Thread.Sleep(3000);

            //----- PDP - Obosolete Test Plan > R2 > T3: Verify navigational menus are clickable -----//
            if (util.CheckElement(driver, Elements.PDP_StickyHeader_Documentation, 2))
            {
                //--- Action: Click "Documentation" on navigational menus ---//
                action.IClick(driver, Elements.PDP_StickyHeader_Documentation);
                Thread.Sleep(2000);

                //--- Expected Result: Each navigational buttons must anchor on its respective sections within the page upon clicking. ---//
                test.validateStringInstance(driver, driver.Url, pdp_documentation_url);
            }

            //----- PDP - Obosolete Test Plan > R6 > T1: Verify Documentation header title -----//

            //--- Expected Result: The header title "Documentation" must be displayed at the top left side of the page. ---//
            if (Locale.Equals("en"))
            {
                test.validateStringInstance(driver, documentation_txt, util.GetText(driver, Elements.PDP_Documentation_Section_Lbl));
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.PDP_Documentation_Section_Lbl);
            }
        }
    }
}