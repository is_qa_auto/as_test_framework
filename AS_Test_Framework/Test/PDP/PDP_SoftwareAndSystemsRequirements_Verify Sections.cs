﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;

namespace AS_Test_Framework.PDP
{
    [TestFixture]
    public class PDP_SoftwareAndSystemsRequirements_Sections : BaseSetUp
    {
        public PDP_SoftwareAndSystemsRequirements_Sections() : base() { }

        //--- URLs ---//
        string pdp_url = "/ad7960";

        //--- Labels ---//
        string FpgaHdl_txt = "FPGA/HDL";

        [Test, Category("PDP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the FPGA/HDL Section is Present and Working as Expected in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the FPGA/HDL Section is Present and Working as Expected in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the FPGA/HDL Section is Present and Working as Expected in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the FPGA/HDL Section is Present and Working as Expected in RU Locale")]
        public void PDP_SoftwareAndSystemsRequirements_VerifyFpgaHdlSection(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + pdp_url);

            if (util.CheckElement(driver, Elements.PDP_SoftwareAndSystemsRequirements_Section_Headers, 2))
            {
                int softwareAndSystemsRequirementsSections_count = util.GetCount(driver, Elements.PDP_SoftwareAndSystemsRequirements_Section_Headers);

                for (int ctr = 1; ctr <= softwareAndSystemsRequirementsSections_count; ctr++)
                {
                    if (util.GetText(driver, By.CssSelector("section[id$='requirement'] * div[class='row']>div:nth-of-type(" + ctr + ")>h3")).Contains(FpgaHdl_txt))
                    {
                        //----- R12 > T2: Verify FPGA/HDL section -----//

                        //--- Expected Result: There should be available wiki link(s) displayed. ---//
                        test.validateElementIsPresent(driver, By.CssSelector("section[id$='requirement'] * div[class='row']>div:nth-of-type(" + ctr + ") * a"));

                        break;
                    }

                    if (ctr == softwareAndSystemsRequirementsSections_count)
                    {
                        test.validateElementIsPresent(driver, By.CssSelector("section[id$='requirement'] * div[class='row']>div:nth-of-type(" + (ctr + 1) + ")>h3"));
                    }
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.PDP_SoftwareAndSystemsRequirements_Section_Headers);
            }
        }
    }
}