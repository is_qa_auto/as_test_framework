﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Text.RegularExpressions;
using System.Threading;

namespace AS_Test_Framework.PDP
{
    [TestFixture]
    public class PDP_ReferenceMaterials_Tabs : BaseSetUp
    {
        public PDP_ReferenceMaterials_Tabs() : base() { }

        //--- URLs ---//
        string pdp_url = "/ad7705";
        //string pdp_url = "/ad7706";

        //--- Labels ---//
        string viewAll_txt = "View All";

        [Test, Category("PDP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the View All Tab is Present and Working as Expected in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the View All Tab is Present and Working as Expected in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the View All Tab is Present and Working as Expected in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the View All Tab is Present and Working as Expected in RU Locale")]
        public void PDP_ReferenceMaterials_VerifyViewAllTab(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + pdp_url);
            Thread.Sleep(1000);

            if (util.CheckElement(driver, Elements.PDP_ReferenceMaterials_Tabs, 2))
            {
                int referenceMaterialsTabs_count = util.GetCount(driver, Elements.PDP_ReferenceMaterials_Tabs);

                for (int ctr = 1; ctr <= referenceMaterialsTabs_count; ctr++)
                {
                    if (util.GetText(driver, By.CssSelector("section[id$='reference'] * div[class$='tabs']>ul>li:nth-of-type(" + ctr + ")>a")).Contains(viewAll_txt))
                    {
                        //----- R15 > T2: Verify "View All" tab in ANALOG product -----//

                        //--- Expected Result: "View All" tab is present at the top left side of the page under Reference Materials section in ANALOG product. ---//
                        test.validateElementIsPresent(driver, By.CssSelector("section[id$='reference'] * div[class$='tabs']>ul>li:nth-of-type(" + ctr + ")>a"));

                        action.IClick(driver, By.CssSelector("section[id$='reference'] * div[class$='tabs']>ul>li:nth-of-type(" + ctr + ")>a"));

                        //--- Expected Result: There should be a counter displayed beside View All tab enclosed by parenthesis showing the number of available pdf reference links. ---//
                        test.validateStringInstance(driver, util.GetCount(driver, Elements.PDP_ReferenceMaterials_Tab_Links).ToString(), Regex.Match(util.GetText(driver, By.CssSelector("section[id$='reference'] * div[class$='tabs']>ul>li:nth-of-type(" + ctr + ")>a")), @"\d+").Value);

                        //--- Expected Result: There should be available reference links for each reference material categories. ---//
                        test.validateElementIsPresent(driver, Elements.PDP_ReferenceMaterials_Tab_Links);

                        break;
                    }

                    if (ctr == referenceMaterialsTabs_count)
                    {
                        test.validateElementIsPresent(driver, By.CssSelector("section[id$='reference'] * div[class$='tabs']>ul>li:nth-of-type(" + (ctr + 1) + ")>a"));
                    }
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.PDP_ReferenceMaterials_Tabs);
            }
        }       
    }
}