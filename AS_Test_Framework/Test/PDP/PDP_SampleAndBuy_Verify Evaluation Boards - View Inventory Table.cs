﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.PDP
{
    [TestFixture]
    public class PDP_SampleAndBuy_EvaluationBoardsViewInventoryTable : BaseSetUp
    {
        public PDP_SampleAndBuy_EvaluationBoardsViewInventoryTable() : base() { }

        //--- URLs ---//
        string pdp_url = "/ad7705";
        //string pdp_url = "/ad7706";

        [Test, Category("PDP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Evaluation Boards - View Inventory Table is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Evaluation Boards - View Inventory Table is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Evaluation Boards - View Inventory Table is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Evaluation Boards - View Inventory Table is Present and Working as Expected in RU Locale")]
        public void PDP_SampleAndBuy_VerifyEvaluationBoardsViewInventoryTableWhen(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + pdp_url);

            if (util.CheckElement(driver, Elements.PDP_EvaluationBoards_Tbl, 2))
            {
                //----- R17 > T16: Verify "Check Inventory" button under Evaluation Boards table in ANALOG product -----//

                //--- Expected Result: Check Inventory button should be displayed beside Select a country dropdown menu in ANALOG product. ---//
                test.validateElementIsPresent(driver, Elements.PDP_EvaluationBoards_Tbl_CheckInventory_Btn);

                if (util.CheckElement(driver, Elements.PDP_EvaluationBoards_Tbl_CheckInventory_Btn, 2))
                {
                    action.IClick(driver, Elements.PDP_EvaluationBoards_Tbl_SelectACountry_Dd_Btn);
                    action.IClick(driver, By.CssSelector("div[id='product-samplebuy']>div>div:nth-of-type(2) * div[class$='country-filter'] * [data-id='US']"));

                    //--- Action: Click Check Inventory button ---//
                    action.IClick(driver, Elements.PDP_EvaluationBoards_Tbl_CheckInventory_Btn);

                    //--- Expected Result: New table column should slide-in ---//
                    test.validateElementIsPresent(driver, Elements.PDP_EvaluationBoards_ViewInventory_Tbl);

                    if (util.CheckElement(driver, Elements.PDP_EvaluationBoards_ViewInventory_Tbl, 2))
                    {
                        //--- Expected Result: Back button should be enabled. ---//
                        test.validateElementIsEnabled(driver, Elements.PDP_EvaluationBoards_Tbl_Back_Btn);

                        if (driver.FindElement(Elements.PDP_EvaluationBoards_Tbl_Back_Btn).Enabled)
                        {
                            //--- Action: Click Back button ---//
                            action.IClick(driver, Elements.PDP_EvaluationBoards_Tbl_Back_Btn);

                            //--- Expected Result: Original table column should slide back. ---//
                            test.validateElementIsPresent(driver, Elements.PDP_EvaluationBoards_Tbl);

                            action.IClick(driver, Elements.PDP_EvaluationBoards_Tbl_SelectACountry_Dd_Btn);
                            action.IClick(driver, By.CssSelector("div[id='product-samplebuy']>div>div:nth-of-type(2) * div[class$='country-filter'] * [data-id='US']"));

                            action.IClick(driver, Elements.PDP_EvaluationBoards_Tbl_CheckInventory_Btn);
                        }

                        int viewInventory_tbl_row_count = util.GetCount(driver, By.CssSelector("div[id='product-samplebuy'] * div[class$='evaluation table-responsive grid-clone view-inventory']>table>tbody>tr"));

                        for (int locator_ctr = 1; locator_ctr <= viewInventory_tbl_row_count; locator_ctr++)
                        {
                            if (util.CheckElement(driver, By.CssSelector("div[id='product-samplebuy'] * div[class$='evaluation table-responsive grid-clone view-inventory']>table>tbody>tr:nth-of-type(" + locator_ctr + ") * input[class='check-add-to-cart']"), 2))
                            {
                                string model_name = util.ReturnAttribute(driver, By.CssSelector("div[id='product-samplebuy'] * div[class$='evaluation table-responsive grid-clone view-inventory']>table>tbody>tr:nth-of-type(" + locator_ctr + ")"), "data-id");

                                //--- Action: Under "Order from Analog Devices" column, select any model and click the corresponding check box for purchase. ---//
                                action.ICheck(driver, By.CssSelector("div[id='product-samplebuy'] * div[class$='evaluation table-responsive grid-clone view-inventory']>table>tbody>tr:nth-of-type(" + locator_ctr + ") * input[class='check-add-to-cart']"));

                                //--- Expected Result: Add to cart button should be enabled. ---//
                                test.validateElementIsEnabled(driver, Elements.PDP_EvaluationBoards_ViewInventory_Tbl_AddToCart_Btn);

                                if (driver.FindElement(Elements.PDP_EvaluationBoards_ViewInventory_Tbl_AddToCart_Btn).Enabled)
                                {
                                    string current_url = driver.Url;

                                    //--- Action: Click Add to cart button. ---//
                                    action.IClick(driver, Elements.PDP_EvaluationBoards_ViewInventory_Tbl_AddToCart_Btn);

                                    if (driver.Url.Contains(current_url))
                                    {
                                        driver.SwitchTo().Window(driver.WindowHandles.Last());
                                    }

                                    if (util.CheckElement(driver, Elements.Shopping_Cart_SelectShippingCountry_ErrorMsg, 2))
                                    {
                                        action.IClick(driver, Elements.Shopping_Cart_Country_Dropdown);
                                        action.IClick(driver, By.CssSelector("li[value='US']>a"));
                                    }

                                    Thread.Sleep(1000);

                                    //--- Expected Result: Page should redirect to shopping cart page with the selected evaluation board model to purchase. ---//
                                    test.validateStringIsCorrect(driver, Elements.Shopping_Cart_AddedModel, model_name);
                                }

                                break;
                            }

                            if (locator_ctr == viewInventory_tbl_row_count)
                            {
                                test.validateElementIsPresent(driver, By.CssSelector("div[id='product-samplebuy'] * div[class$='evaluation table-responsive grid-clone view-inventory']>table>tbody>tr:nth-of-type(" + (locator_ctr + 1) + ") * input[class='check-add-to-cart']"));
                            }
                        }
                    }
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.PDP_EvaluationBoards_Tbl);
            }
        }
    }
}