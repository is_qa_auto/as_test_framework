﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Collections.Generic;

namespace AS_Test_Framework.CrossRefenceSearch
{

    [TestFixture]
    public class Xref_ObsoletePart_ContactUs : BaseSetUp
    {
        public Xref_ObsoletePart_ContactUs() : base() { }

        [Test, Category("Cross Reference Search"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that experienced application engineers and Contact Technical Support link in Cross reference search landing page is present and working in EN Locale")]
        [TestCase("cn", TestName = "Verify that experienced application engineers and Contact Technical Support link in Cross reference search landing page is present and working in CN Locale")]
        [TestCase("jp", TestName = "Verify that experienced application engineers and Contact Technical Support link in Cross reference search landing page is present and working in JP Locale")]
        [TestCase("ru", TestName = "Verify that experienced application engineers and Contact Technical Support link in Cross reference search landing page is present and working in RU Locale")]
        public void Xref_VerifyXRefSearch_ContactUs(string Locale)
        {
            if (Locale.Equals("cn"))
            {
                action.Navigate(driver, Configuration.Env_Url + "cpsearch/CrossReferenceSearch.aspx?locale=zh");
            }
            else
            {
                action.Navigate(driver, Configuration.Env_Url + "cpsearch/CrossReferenceSearch.aspx?locale=" + Locale);
            }
            action.IClick(driver, Elements.XRef_ObsoletePartSearch_Toggle);
            action.IClick(driver, By.CssSelector("div[class='col-md-9']>div>p>a"));
            if (Locale.Equals("jp"))
            {
                test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/support/jp_tech-support.html");
            }
            else
            {
                test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/support/technical-support.html");
            }

            driver.Navigate().Back();
            action.IClick(driver, Elements.XRef_ObsoletePartSearch_Toggle);
            action.IClick(driver, Elements.XRef_ObsoletePart_TechnicalSupport_Link);
            if (Locale.Equals("jp"))
            {
                test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/support/jp_tech-support.html");
            }
            else
            {
                test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/support/technical-support.html");
            }
        }
    }
}