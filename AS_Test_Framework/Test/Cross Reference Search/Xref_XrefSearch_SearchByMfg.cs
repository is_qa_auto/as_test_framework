﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Collections.Generic;
using System.Threading;
using System.Linq;

namespace AS_Test_Framework.CrossRefenceSearch
{

    [TestFixture]
    public class Xref_XrefSearch_SearchByMfg : BaseSetUp
    {
        public Xref_XrefSearch_SearchByMfg() : base() { }

        [Test, Category("Cross Reference Search"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that search by mfg components are all present in EN Locale")]
        [TestCase("cn", TestName = "Verify that search by mfg components are all present in CN Locale")]
        [TestCase("jp", TestName = "Verify that search by mfg components are all present in JP Locale")]
        [TestCase("ru", TestName = "Verify that search by mfg components are all present in RU Locale")]
        public void Xref_VerifyXRefSearch_SearchByMfg(string Locale)
        {
            if (Locale.Equals("cn"))
            {
                action.Navigate(driver, Configuration.Env_Url + "cpsearch/CrossReferenceSearch.aspx?locale=zh");
            }
            else
            {
                action.Navigate(driver, Configuration.Env_Url + "cpsearch/CrossReferenceSearch.aspx?locale=" + Locale);
            }
            test.validateElementIsPresent(driver, Elements.XRef_XRefSearch_Mfg_Dropdown);
            test.validateElementIsPresent(driver, Elements.XRef_XRefSearch_Mfg_Search_Button);
            test.validateSelectedValueIsCorrect(driver, Elements.XRef_XRefSearch_Mfg_Dropdown, "Select a Manufacturer");
            test.validateStringIsCorrect(driver, By.CssSelector("table[class='search-options'] td:nth-child(3) p[class='help-block']"), "Select manufacturer and click search");
        }

        [Test, Category("Cross Reference Search"), Category("NonCore")]
        [TestCase("en", TestName = "Verify Product Link in \"ADI Equivalent Product Part #\" redirects to PDP - EN Locale")]
        [TestCase("cn", TestName = "Verify Product Link in \"ADI Equivalent Product Part #\" redirects to PDP - CN Locale")]
        [TestCase("jp", TestName = "Verify Product Link in \"ADI Equivalent Product Part #\" redirects to PDP - JP Locale")]
        [TestCase("ru", TestName = "Verify Product Link in \"ADI Equivalent Product Part #\" redirects to PDP - RU Locale")]
        public void Xref_VerifyXRefSearch_SearchByMfg2(string Locale)
        {
            if (Locale.Equals("cn"))
            {
                action.Navigate(driver, Configuration.Env_Url + "cpsearch/CrossReferenceSearch.aspx?locale=zh");
            }
            else
            {
                action.Navigate(driver, Configuration.Env_Url + "cpsearch/CrossReferenceSearch.aspx?locale=" + Locale);
            }
            action.ISelectFromDropdown(driver, Elements.XRef_XRefSearch_Mfg_Dropdown, "133");
            action.IClick(driver, Elements.XRef_XRefSearch_Mfg_Search_Button);
            Thread.Sleep(5000);
            action.IClick(driver, By.CssSelector("div[id='divSearchResult'] * ul>li:nth-child(1)>a"));
            string ProdNo_Url = util.ReturnAttribute(driver, By.CssSelector("div[id='divequivalent']>p:nth-child(1)>a"), "href");
            action.IClick(driver, By.CssSelector("div[id='divequivalent']>p:nth-child(1)>a"));
            driver.SwitchTo().Window(driver.WindowHandles.Last());
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/products/" + ProdNo_Url.Split('/').Last().ToLower());
        }

        [Test, Category("Cross Reference Search"), Category("NonCore")]
        [TestCase("en", TestName = "Verify search by mfg is working as expected in EN Locale")]
        [TestCase("cn", TestName = "Verify search by mfg is working as expected in CN Locale")]
        [TestCase("jp", TestName = "Verify search by mfg is working as expected in JP Locale")]
        [TestCase("ru", TestName = "Verify search by mfg is working as expected in RU Locale")]
        public void Xref_VerifyXRefSearch_SearchByMfg3(string Locale)
        {
            if (Locale.Equals("cn"))
            {
                action.Navigate(driver, Configuration.Env_Url + "cpsearch/CrossReferenceSearch.aspx?locale=zh");
            }
            else
            {
                action.Navigate(driver, Configuration.Env_Url + "cpsearch/CrossReferenceSearch.aspx?locale=" + Locale);
            }
            action.ISelectFromDropdown(driver, Elements.XRef_XRefSearch_Mfg_Dropdown, "133");
            action.IClick(driver, Elements.XRef_XRefSearch_Mfg_Search_Button);
            Thread.Sleep(5000);
            string SelectedValue = util.GetSelectedDropdownValue(driver, Elements.XRef_XRefSearch_Mfg_Dropdown);
            test.validateElementIsPresent(driver, By.CssSelector("div[id='divSearchResult']"));
            test.validateStringIsCorrect(driver, Elements.XRef_ResultPanel1_Header,"Select Product Part #");
            test.validateStringIsCorrect(driver, Elements.XRef_ResultPanel2_Header, "ADI Equivalent Product Part #");
            test.validateElementIsPresent(driver, Elements.XRef_PartNo_List);
            string PartNo = util.GetText(driver, By.CssSelector("div[id='divSearchResult'] * ul>li:nth-child(1)>a"));
            action.IClick(driver, By.CssSelector("div[id='divSearchResult'] * ul>li:nth-child(1)>a"));
            test.validateElementIsPresent(driver, Elements.XRef_EquivalentPart_List);
            test.validateElementIsPresent(driver, Elements.XRef_EquivalentPart_ToolTip_Button);
            test.validateStringIsCorrect(driver, By.CssSelector("div[id='divequivalent'] p[class='lead']"), "Analog Devices part " + util.GetText(driver, By.CssSelector("div[id='divequivalent'] p[class='lead'] a")) + " matches " + SelectedValue + " part " + PartNo);
        }

        [Test, Category("Cross Reference Search"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that error message will be displayed when user click search button without inputting a value or using invalid keyword for Search By Manufacturer - EN Locale")]
        [TestCase("cn", TestName = "Verify that error message will be displayed when user click search button without inputting a value or using invalid keyword for Search By Manufacturer - CN Locale")]
        [TestCase("jp", TestName = "Verify that error message will be displayed when user click search button without inputting a value or using invalid keyword for Search By Manufacturer - JP Locale")]
        [TestCase("ru", TestName = "Verify that error message will be displayed when user click search button without inputting a value or using invalid keyword for Search By Manufacturer - RU Locale")]
        public void Xref_VerifyXRefSearch_SearchByPart3(string Locale)
        {
            if (Locale.Equals("cn"))
            {
                action.Navigate(driver, Configuration.Env_Url + "cpsearch/CrossReferenceSearch.aspx?locale=zh");
            }
            else
            {
                action.Navigate(driver, Configuration.Env_Url + "cpsearch/CrossReferenceSearch.aspx?locale=" + Locale);
            }
            action.IClick(driver, Elements.XRef_XRefSearch_Mfg_Search_Button);
            Thread.Sleep(5000);
            test.validateStringIsCorrect(driver, Elements.XRef_XRefSearch_ErrorByMfg, "Please select a manufacturer");
          
        }

    }
}