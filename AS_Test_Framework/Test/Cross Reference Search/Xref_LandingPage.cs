﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Collections.Generic;

namespace AS_Test_Framework.CrossRefenceSearch
{

    [TestFixture]
    public class Xref_LandingPage : BaseSetUp
    {
        public Xref_LandingPage() : base() { }

        [Test, Category("Cross Reference Search"), Category("NonCore")]
        [TestCase("en", TestName = "Verify Cross reference search landing page component in EN Locale")]
        [TestCase("cn", TestName = "Verify Cross reference search landing page component in CN Locale")]
        [TestCase("jp", TestName = "Verify Cross reference search landing page component in JP Locale")]
        [TestCase("ru", TestName = "Verify Cross reference search landing page component in RU Locale")]
        public void Xref_VerifyLandingPage(string Locale)
        {
            if (Locale.Equals("cn"))
            {
                action.Navigate(driver, Configuration.Env_Url + "cpsearch/CrossReferenceSearch.aspx?locale=zh");
            }
            else {
                action.Navigate(driver, Configuration.Env_Url + "cpsearch/CrossReferenceSearch.aspx?locale=" + Locale);
            }
            test.validateStringIsCorrect(driver, By.CssSelector("div[class='container page-title '] h1"), "ADI CROSS-REFERENCE AND OBSOLETE PART SEARCH.");
            test.validateStringIsCorrect(driver, By.CssSelector("ul[class='list-inline tabs clearfix']>li[class*='active'] h3"), "CROSS-REFERENCE SEARCH");
            test.validateElementIsPresent(driver, Elements.XRef_ObsoletePartSearch_Toggle);
            test.validateElementIsPresent(driver, Elements.XRef_XRefSearch_Toggle);
            test.validateElementIsPresent(driver, By.CssSelector("div[class='col-md-9']>a"));
            test.validateElementIsPresent(driver, Elements.XRef_XRefSearch_TechnicalSupport_Link);
            test.validateStringIsCorrect(driver, By.CssSelector("table[class='search-options'] td:nth-child(1) div[class='panel-heading'] h3"), "Search by Part Number");
            test.validateStringIsCorrect(driver, By.CssSelector("table[class='search-options'] td:nth-child(3) div[class='panel-heading'] h3"), "Search by Manufacturer");

        }
    }
}