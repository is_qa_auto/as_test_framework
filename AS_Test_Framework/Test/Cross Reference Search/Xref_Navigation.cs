﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Collections.Generic;

namespace AS_Test_Framework.CrossRefenceSearch
{

    [TestFixture]
    public class Xref_Navigation : BaseSetUp
    {
        public Xref_Navigation() : base() { }

        [Test, Category("Cross Reference Search"), Category("NonCore")]
        [TestCase("en", "Cross Reference Search", TestName = "Verify cross reference link is present in Core Applications for EN Locale")]
        [TestCase("cn", "交互搜索和停产产品搜索", TestName = "Verify cross reference link is present in Core Applications for CN Locale")]
        [TestCase("jp", "クロスリファレンス検索", TestName = "Verify cross reference link is present in Core Applications for JP Locale")]
        [TestCase("ru", "Поиск аналогов", TestName = "Verify cross reference link is present in Core Applications for RU Locale")]
        public void XREf_VerifyXrefNavigation1(string Locale, string XRefLink)
        {
            action.Navigate(driver, Configuration.Env_Url +  Locale + "/index.html");
            action.IClick(driver, Elements.SearchTextField);
            test.validateStringIsCorrect(driver, Elements.XRefLink, XRefLink);
            action.IClick(driver, Elements.XRefLink);
            if (!Configuration.Environment.Equals("production"))
            {
                if (Locale.Equals("cn"))
                {
                    test.validateStringInstance(driver, driver.Url, "analog.com/cpsearch/CrossReferenceSearch.aspx?locale=zh");
                }
                else if (Locale.Equals("en"))
                {
                    test.validateStringInstance(driver, driver.Url, "analog.com/cpsearch/CrossReferenceSearch.aspx");
                }
                else
                {
                    test.validateStringInstance(driver, driver.Url, "analog.com/cpsearch/CrossReferenceSearch.aspx?locale=" + Locale);
                }
            }
            else
            {
                if (Locale.Equals("cn"))
                {
                    test.validateScreenByUrl(driver, Configuration.Env_Url + "cpsearch/CrossReferenceSearch.aspx?locale=zh");
                }
                else if (Locale.Equals("en"))
                {
                    test.validateScreenByUrl(driver, Configuration.Env_Url + "cpsearch/CrossReferenceSearch.aspx");
                }
                else
                { 
                    test.validateScreenByUrl(driver, Configuration.Env_Url + "cpsearch/CrossReferenceSearch.aspx?locale=" + Locale);
                }
            }
        }


        [Test, Category("Cross Reference Search"), Category("NonCore")]
        [TestCase("en", "Cross Reference Search", TestName = "Verify cross reference link is present in Non-Core Applications for EN Locale")]
        [TestCase("zh", "交互搜索和停产产品搜索", TestName = "Verify cross reference link is present in Non-Core Applications for CN Locale")]
        [TestCase("jp", "クロスリファレンス検索", TestName = "Verify cross reference link is present in Non-Core Applications for JP Locale")]
        [TestCase("ru", "Поиск аналогов", TestName = "Verify cross reference link is present in Non-Core Applications for RU Locale")]
        public void XREf_VerifyXrefNavigation2(string Locale, string XRefLink)
        {
            action.Navigate(driver, Configuration.Env_Url.Replace("www","shoppingcart").Replace("cldnet", "corpnt") + "/ShoppingCartPage.aspx?locale=" + Locale);
            action.IClick(driver, Elements.SearchTextField);
            test.validateStringIsCorrect(driver, Elements.XRefLink, XRefLink);
            action.IClick(driver, Elements.XRefLink);
            if (!Configuration.Environment.Equals("production"))
            {
                if (!Locale.Equals("en"))
                {
                    test.validateStringInstance(driver, driver.Url, "analog.com/cpsearch/CrossReferenceSearch.aspx?locale=" + Locale);
                }
                else {
                    test.validateStringInstance(driver, driver.Url, "analog.com/cpsearch/CrossReferenceSearch.aspx");
                }
            }
            else
            {
                if (!Locale.Equals("en"))
                {
                    test.validateScreenByUrl(driver, Configuration.Env_Url + "cpsearch/CrossReferenceSearch.aspx?locale=" + Locale);
                }
                else {
                    test.validateScreenByUrl(driver, Configuration.Env_Url + "cpsearch/CrossReferenceSearch.aspx");
                }
            }
        }
    }
}