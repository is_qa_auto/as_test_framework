﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Collections.Generic;

namespace AS_Test_Framework.CrossRefenceSearch
{

    [TestFixture]
    public class Xref_BreadCrumbs : BaseSetUp
    {
        public Xref_BreadCrumbs() : base() { }

        [Test, Category("Cross Reference Search"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that Breadcrumbs is present in Cross Reference Search for EN Locale")]
        [TestCase("cn", TestName = "Verify that Breadcrumbs is present in Cross Reference Search for CN Locale")]
        [TestCase("jp", TestName = "Verify that Breadcrumbs is present in Cross Reference Search for JP Locale")]
        [TestCase("ru", TestName = "Verify that Breadcrumbs is present in Cross Reference Search for RU Locale")]
        public void Xref_VerifyBreadCrumbs(string Locale)
        {
            if (Locale.Equals("cn"))
            {
                action.Navigate(driver, Configuration.Env_Url + "cpsearch/CrossReferenceSearch.aspx?locale=zh");
            }
            else {
                action.Navigate(driver, Configuration.Env_Url + "cpsearch/CrossReferenceSearch.aspx?locale=" + Locale);
            }
            test.validateElementIsPresent(driver, Elements.Applications_BreadCrumbs);
            test.validateElementIsPresent(driver, Elements.Applications_BreadCrumbs_Home);
            action.IClick(driver, Elements.Applications_BreadCrumbs_Home);
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/index.html");
        }
    }
}