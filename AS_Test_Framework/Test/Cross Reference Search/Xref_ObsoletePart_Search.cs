﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Collections.Generic;
using System.Threading;

namespace AS_Test_Framework.CrossRefenceSearch
{

    [TestFixture]
    public class Xref_ObsoletePart_Search : BaseSetUp
    {
        public Xref_ObsoletePart_Search() : base() { }

        [Test, Category("Cross Reference Search"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that Obsolete Part Search component are all present in EN Locale")]
        [TestCase("cn", TestName = "Verify that Obsolete Part Search component are all present in CN Locale")]
        [TestCase("jp", TestName = "Verify that Obsolete Part Search component are all present in JP Locale")]
        [TestCase("ru", TestName = "Verify that Obsolete Part Search component are all present in RU Locale")]
        public void Xref_VerifyObsoletePart(string Locale)
        {
            if (Locale.Equals("cn"))
            {
                action.Navigate(driver, Configuration.Env_Url + "cpsearch/CrossReferenceSearch.aspx?locale=zh");
            }
            else
            {
                action.Navigate(driver, Configuration.Env_Url + "cpsearch/CrossReferenceSearch.aspx?locale=" + Locale);
            }
            action.IClick(driver, Elements.XRef_ObsoletePartSearch_Toggle);
            test.validateStringIsCorrect(driver, By.CssSelector("ul[class='list-inline tabs clearfix']>li[class*='active'] h3"), "OBSOLETE PART");
            test.validateElementIsPresent(driver, By.CssSelector("div[class='col-md-9']>div>p>a"));
            test.validateElementIsPresent(driver, Elements.XRef_ObsoletePart_TechnicalSupport_Link);
            test.validateElementIsPresent(driver, Elements.XRef_ObsoletePart_TextField);
            test.validateElementIsPresent(driver, Elements.XRef_ObsoletePart_Search_Button);
            test.validateString(driver, "Enter Product number", util.ReturnAttribute(driver, Elements.XRef_ObsoletePart_TextField, "placeholder"));
            action.IClick(driver, Elements.XRef_XRefSearch_Toggle);
            test.validateStringIsCorrect(driver, By.CssSelector("ul[class='list-inline tabs clearfix']>li[class*='active'] h3"), "CROSS-REFERENCE SEARCH");

        }

        [Test, Category("Cross Reference Search"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that Obsolete Part Search is working as expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that Obsolete Part Search is working as expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that Obsolete Part Search is working as expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that Obsolete Part Search is working as expected in RU Locale")]
        public void Xref_VerifyObsoletePart_SearchFunctionality(string Locale)
        {
            if (Locale.Equals("cn"))
            {
                action.Navigate(driver, Configuration.Env_Url + "cpsearch/CrossReferenceSearch.aspx?locale=zh");
            }
            else
            {
                action.Navigate(driver, Configuration.Env_Url + "cpsearch/CrossReferenceSearch.aspx?locale=" + Locale);
            }
            action.IClick(driver, Elements.XRef_ObsoletePartSearch_Toggle);
            action.IType(driver, Elements.XRef_ObsoletePart_TextField, "AD1321");
            action.IClick(driver, Elements.XRef_ObsoletePart_Search_Button);
            Thread.Sleep(5000);
          
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/search.html?q=AD1321");
            if (Locale.Equals("cn"))
            {
                test.validateStringIsCorrect(driver, Elements.GlobalSearchResults_BaseballCard_Title_Link, "ad1321");
            }
            else
            { 
                test.validateStringIsCorrect(driver, Elements.GlobalSearchResults_BaseballCard_Title_Link, "AD1321");
            }
        }
    }
}