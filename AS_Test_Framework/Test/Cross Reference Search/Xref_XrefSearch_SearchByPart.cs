﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Collections.Generic;
using System.Threading;
using System.Linq;

namespace AS_Test_Framework.CrossRefenceSearch
{

    [TestFixture]
    public class Xref_XrefSearch_SearchByPart : BaseSetUp
    {
        public Xref_XrefSearch_SearchByPart() : base() { }

        [Test, Category("Cross Reference Search"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that search by part number components are all present in EN Locale")]
        [TestCase("cn", TestName = "Verify that search by part number components are all present in CN Locale")]
        [TestCase("jp", TestName = "Verify that search by part number components are all present in JP Locale")]
        [TestCase("ru", TestName = "Verify that search by part number components are all present in RU Locale")]
        public void Xref_VerifyXRefSearch_SearchByPart(string Locale)
        {
            if (Locale.Equals("cn"))
            {
                action.Navigate(driver, Configuration.Env_Url + "cpsearch/CrossReferenceSearch.aspx?locale=zh");
            }
            else
            {
                action.Navigate(driver, Configuration.Env_Url + "cpsearch/CrossReferenceSearch.aspx?locale=" + Locale);
            }
            test.validateElementIsPresent(driver, Elements.XRef_XRefSearch_PartNumber_Textfield);
            test.validateElementIsPresent(driver, Elements.XRef_XRefSearch_PartNumber_Search_Button);
            test.validateString(driver, "Enter Part number", util.ReturnAttribute(driver, Elements.XRef_XRefSearch_PartNumber_Textfield, "placeholder"));
            test.validateStringIsCorrect(driver, By.CssSelector("table[class='search-options'] td:nth-child(1) p[class='help-block']"), "Part Number should be at least 3 characters");
        }

        [Test, Category("Cross Reference Search"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that Search By Part is working as expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that Search By Part is working as expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that Search By Part is working as expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that Search By Part is working as expected in RU Locale")]
        public void Xref_VerifyXRefSearch_SearchByPart2(string Locale)
        {
            if (Locale.Equals("cn"))
            {
                action.Navigate(driver, Configuration.Env_Url + "cpsearch/CrossReferenceSearch.aspx?locale=zh");
            }
            else
            {
                action.Navigate(driver, Configuration.Env_Url + "cpsearch/CrossReferenceSearch.aspx?locale=" + Locale);
            }
            action.IType(driver, Elements.XRef_XRefSearch_PartNumber_Textfield, "AD7706");
            action.IClick(driver, Elements.XRef_XRefSearch_PartNumber_Search_Button);
            Thread.Sleep(5000);
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/search.html?q=AD7706");
            test.validateStringIsCorrect(driver, Elements.GlobalSearchResults_BaseballCard_Title_Link, "AD7706");
        }


        [Test, Category("Cross Reference Search"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that error message will be displayed when user click search button without inputting a value or using invalid keyword for Search By Part Number - EN Locale")]
        [TestCase("cn", TestName = "Verify that error message will be displayed when user click search button without inputting a value or using invalid keyword for Search By Part Number - CN Locale")]
        [TestCase("jp", TestName = "Verify that error message will be displayed when user click search button without inputting a value or using invalid keyword for Search By Part Number - JP Locale")]
        [TestCase("ru", TestName = "Verify that error message will be displayed when user click search button without inputting a value or using invalid keyword for Search By Part Number - RU Locale")]
        public void Xref_VerifyXRefSearch_SearchByPart3(string Locale)
        {
            if (Locale.Equals("cn"))
            {
                action.Navigate(driver, Configuration.Env_Url + "cpsearch/CrossReferenceSearch.aspx?locale=zh");
            }
            else
            {
                action.Navigate(driver, Configuration.Env_Url + "cpsearch/CrossReferenceSearch.aspx?locale=" + Locale);
            }
            action.IClick(driver, Elements.XRef_XRefSearch_PartNumber_Search_Button);
            Thread.Sleep(5000);
            test.validateStringIsCorrect(driver, Elements.XRef_XRefSearch_ErrorByPart, "Part Number does not meet requirements");
            driver.Navigate().Refresh();
            action.IType(driver, Elements.XRef_XRefSearch_PartNumber_Textfield, "AD");
            action.IClick(driver, Elements.XRef_XRefSearch_PartNumber_Search_Button);
            Thread.Sleep(5000);
            test.validateStringIsCorrect(driver, Elements.XRef_XRefSearch_ErrorByPart, "Part Number does not meet requirements");

        }

        [Test, Category("Cross Reference Search"), Category("NonCore")]
        [TestCase("en", TestName = "Verify search by using a value that returns no results - EN Locale")]
        [TestCase("cn", TestName = "Verify search by using a value that returns no results - CN Locale")]
        [TestCase("jp", TestName = "Verify search by using a value that returns no results - JP Locale")]
        [TestCase("ru", TestName = "Verify search by using a value that returns no results - RU Locale")]
        public void Xref_VerifyXRefSearch_SearchByPart4(string Locale)
        {
            if (Locale.Equals("cn"))
            {
                action.Navigate(driver, Configuration.Env_Url + "cpsearch/CrossReferenceSearch.aspx?locale=zh");
            }
            else
            {
                action.Navigate(driver, Configuration.Env_Url + "cpsearch/CrossReferenceSearch.aspx?locale=" + Locale);
            }
            action.IType(driver, Elements.XRef_XRefSearch_PartNumber_Textfield, "BOZO123");
            action.IClick(driver, Elements.XRef_XRefSearch_PartNumber_Search_Button);
            Thread.Sleep(5000);
            test.validateStringIsCorrect(driver, Elements.XRef_XRefSearch_NoMatchFound_Label, "The part number you entered is not recognized. Please re-enter the part number or:");

            string[] NoMatchFoundLinks = { "View Parametric Data for researching parts", "View the entire ADI product portfolio", "Contact our experienced Application Engineers for assistance." };

            for (int x = 1, y = 0; util.CheckElement(driver, By.CssSelector("ul[class='listImg']>li:nth-child(" + x + ")"), 5); x++, y++) {
                test.validateStringIsCorrect(driver, By.CssSelector("ul[class='listImg']>li:nth-child(" + x + ")>a"), NoMatchFoundLinks[y]);
            }
            action.IClick(driver, By.CssSelector("ul[class='listImg']>li:nth-child(3)>a"));
            if (Locale.Equals("jp"))
            {
                test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/support/jp_tech-support.html");
            }
            else
            {
                test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/support/technical-support.html");
            }

        }

        [Test, Category("Cross Reference Search"), Category("NonCore")]
        [TestCase("en", TestName = "Verify Product Link in \"ADI Equivalent Product Part #\" redirects to PDP - EN Locale")]
        [TestCase("cn", TestName = "Verify Product Link in \"ADI Equivalent Product Part #\" redirects to PDP - CN Locale")]
        [TestCase("jp", TestName = "Verify Product Link in \"ADI Equivalent Product Part #\" redirects to PDP - JP Locale")]
        [TestCase("ru", TestName = "Verify Product Link in \"ADI Equivalent Product Part #\" redirects to PDP - RU Locale")]
        public void Xref_VerifyXRefSearch_SearchByPart5(string Locale)
        {
            if (Locale.Equals("cn"))
            {
                action.Navigate(driver, Configuration.Env_Url + "cpsearch/CrossReferenceSearch.aspx?locale=zh");
            }
            else
            {
                action.Navigate(driver, Configuration.Env_Url + "cpsearch/CrossReferenceSearch.aspx?locale=" + Locale);
            }
            action.IType(driver, Elements.XRef_XRefSearch_PartNumber_Textfield, "REF01CJ8");
            action.IClick(driver, Elements.XRef_XRefSearch_PartNumber_Search_Button);
            Thread.Sleep(5000);
            action.IClick(driver, By.CssSelector("div[id='divSearchResult'] * ul>li:nth-child(1)>a"));
            string ProdNo_Url = util.ReturnAttribute(driver, By.CssSelector("div[id='divequivalent']>p:nth-child(1)>a"), "href");
            action.IClick(driver, By.CssSelector("div[id='divequivalent']>p:nth-child(1)>a"));
            driver.SwitchTo().Window(driver.WindowHandles.Last());
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/products/" + ProdNo_Url.Split('/').Last().ToLower());
        }

        [Test, Category("Cross Reference Search"), Category("NonCore")]
        [TestCase("en", TestName = "Verify user can search for an ADI Part that isnt in competetive data")]
        public void Xref_VerifyXRefSearch_SearchByPart6(string Locale)
        {
            if (Locale.Equals("cn"))
            {
                action.Navigate(driver, Configuration.Env_Url + "cpsearch/CrossReferenceSearch.aspx?locale=zh");
            }
            else
            {
                action.Navigate(driver, Configuration.Env_Url + "cpsearch/CrossReferenceSearch.aspx?locale=" + Locale);
            }
            action.IType(driver, Elements.XRef_XRefSearch_PartNumber_Textfield, "SSM2212");
            action.IClick(driver, Elements.XRef_XRefSearch_PartNumber_Search_Button);
            Thread.Sleep(5000);
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/search.html?q=SSM2212");
        }

        [Test, Category("Cross Reference Search"), Category("NonCore")]
        [TestCase("en", TestName = "Verify user can search for using partial name/keyword")]
        public void Xref_VerifyXRefSearch_SearchByPart7(string Locale)
        {
            if (Locale.Equals("cn"))
            {
                action.Navigate(driver, Configuration.Env_Url + "cpsearch/CrossReferenceSearch.aspx?locale=zh");
            }
            else
            {
                action.Navigate(driver, Configuration.Env_Url + "cpsearch/CrossReferenceSearch.aspx?locale=" + Locale);
            }
            action.IType(driver, Elements.XRef_XRefSearch_PartNumber_Textfield, "AD7");
            action.IClick(driver, Elements.XRef_XRefSearch_PartNumber_Search_Button);
            Thread.Sleep(5000);
            test.validateElementIsPresent(driver, Elements.XRef_PartNo_List);
        }
    }
}