﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using OpenQA.Selenium;

namespace AS_Test_Framework.ExportClassification
{
    [TestFixture]
    public class ExportClassification_ExportClassificationInformation_MultipleProductModels_WildcardSearch : BaseSetUp
    {
        public ExportClassification_ExportClassificationInformation_MultipleProductModels_WildcardSearch() : base() { }

        //--- URLs ---//
        string viewExportClassifications_page_url = "/support/customer-service-resources/customer-service/view-export-classification.html";
        string exportClassificationInformation_page_url = "exportclassification/Search/Result";

        [Test, Category("Export Classification"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Page is Working as Expected in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Page is Working as Expected in CN Locale")] --> NOTE: AL-17596 has been logged for the Issue where Product Model Search always redirects to Export Classification Information in EN Locale.
        //[TestCase("jp", TestName = "Verify that the Page is Working as Expected in JP Locale")] --> NOTE: AL-17596 has been logged for the Issue where Product Model Search always redirects to Export Classification Information in EN Locale.
        //[TestCase("ru", TestName = "Verify that the Page is Working as Expected in RU Locale")] --> NOTE: AL-17596 has been logged for the Issue where Product Model Search always redirects to Export Classification Information in EN Locale.
        public void ExportClassification_ExportClassificationInformation_MultipleProductModels_VerifyWildcardSearch(string Locale)
        {
            //--- Action: Access Export Classification Information URL ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + viewExportClassifications_page_url);

            driver.SwitchTo().Frame(driver.FindElement(By.CssSelector("div[id='rte-body'] iframe")));

            //--- Action: Click on Multiple Product Models radio button ---//
            action.IClick(driver, Elements.ExportClassification_ViewExportClassifications_MultipleProductModels_RadioButton);

            //----- Export Classification Test Plan > English Tab > R4 > T4: Verify search when no results returned (AL-8962) -----//

            //--- Action: Enter a search with minimum 4 characters ---//
            string wildcard_multipleProductModels_input = "AD78";
            action.IDeleteValueOnFields(driver, Elements.ExportClassification_ViewExportClassifications_MultipleProductModels_InputBox);
            action.IType(driver, Elements.ExportClassification_ViewExportClassifications_MultipleProductModels_InputBox, wildcard_multipleProductModels_input);

            //--- Action: Click on the Search Now button ---//
            action.IClick(driver, Elements.ExportClassification_ViewExportClassifications_ProductModelSearch_SearchNow_Button);

            //--- Expected Result: The page will redirect to the EXPORT CLASSIFICATION INFORMATION - Search Result page. ---//
            test.validateStringInstance(driver, driver.Url, Configuration.Env_Url + exportClassificationInformation_page_url);

            bool url_locale = false;
            if (Locale.Equals("cn") || Locale.Equals("zh"))
            {
                url_locale = true;
                test.validateStringInstance(driver, driver.Url, "locale=zh");
            }
            else
            {
                url_locale = true;
                test.validateStringInstance(driver, driver.Url, "locale=" + Locale);
            }

            if (driver.Url.Contains(Configuration.Env_Url + exportClassificationInformation_page_url) && url_locale == true)
            {
                //--- Expected Result: No Results will be displayed. ---//
                test.validateElementIsNotPresent(driver, Elements.ExportClassification_ExportClassificationInformation_Product_Table);

                //--- Expected Result: ADI Export Classification Information link will be displayed ---//
                test.validateElementIsPresent(driver, Elements.ExportClassification_ExportClassificationInformation_AdiExportClassificationInformation_Link);

                //--- Expected Result: ExportComplianceDepartment@analog.com will be displayed ---//
                test.validateElementIsPresent(driver, Elements.ExportClassification_ExportClassificationInformation_ExportComplianceDepartment_AnalogCom_Link);
            }
        }
    }
}