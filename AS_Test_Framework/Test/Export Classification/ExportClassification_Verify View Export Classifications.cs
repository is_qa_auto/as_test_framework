﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;

namespace AS_Test_Framework.ExportClassification
{
    [TestFixture]
    public class ExportClassification_ViewExportClassifications : BaseSetUp
    {
        public ExportClassification_ViewExportClassifications() : base() { }

        //--- URLs ---//
        string viewExportClassifications_page_url = "/support/customer-service-resources/customer-service/view-export-classification.html";

        [Test, Category("Export Classification"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the View Export Classifications Page is Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the View Export Classifications Page is Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the View Export Classifications Page is Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the View Export Classifications Page is Working as Expected in RU Locale")]
        public void ExportClassification_VerifyViewExportClassifications(string Locale)
        {
            //--- Action: Open Export Classification Information URL ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + viewExportClassifications_page_url);

            //----- Export Classification Test Plan > English Tab > R2 > T1: Verify the breadcrumb -----//

            //--- Expected Result: The breadcrumb of the current  page should just a label name of the form ---//
            test.validateStringIsCorrect(driver, Elements.Breadcrumb_CurrentPage_Txt, util.GetText(driver, Elements.ExportClassification_ViewExportClassifications_Page_Title_Txt));

            //----- Export Classification Test Plan > English Tab > R2 > T2: Verify the ADI Export Classification Information link -----//

            //--- Expected Result: ADI Export Classification Information link is displayed ---//
            test.validateElementIsPresent(driver, Elements.ExportClassification_ViewExportClassifications_AdiExportClassificationInformation_Link);

            //----- Export Classification Test Plan > English Tab > R2 > T3: Verify the ExportComplianceDepartment@analog.com link -----//

            //--- Expected Result: ExportComplianceDepartment@analog.com link is displayed ---//
            test.validateElementIsPresent(driver, Elements.ExportClassification_ViewExportClassifications_ExportComplianceDepartment_AnalogCom);
        }     
    }
}