﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using OpenQA.Selenium;
using System.Threading;

namespace AS_Test_Framework.ExportClassification
{
    [TestFixture]
    public class ExportClassification_ExportClassificationInformation_SingleProductModel_SearchResults : BaseSetUp
    {
        public ExportClassification_ExportClassificationInformation_SingleProductModel_SearchResults() : base() { }

        //--- URLs ---//
        string viewExportClassifications_page_url = "/support/customer-service-resources/customer-service/view-export-classification.html";
        string exportClassificationInformation_page_url = "exportclassification/Search/Result";

        //--- Labels ---//
        string noMatchesRemaining_label = "No matches remaining.";

        [Test, Category("Export Classification"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Links are Present and Working as Expected in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Links are Present and Working as Expected in CN Locale")] --> NOTE: AL-17596 has been logged for the Issue where Product Model Search always redirects to Export Classification Information in EN Locale.
        //[TestCase("jp", TestName = "Verify that the Links are Present and Working as Expected in JP Locale")] --> NOTE: AL-17596 has been logged for the Issue where Product Model Search always redirects to Export Classification Information in EN Locale.
        //[TestCase("ru", TestName = "Verify that the Links are Present and Working as Expected in RU Locale")] --> NOTE: AL-17596 has been logged for the Issue where Product Model Search always redirects to Export Classification Information in EN Locale.
        public void ExportClassification_ExportClassificationInformation_SingleProductModel_SearchResults_VerifyLinks(string Locale)
        {
            //--- Action: Access Export Classification Information URL ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + viewExportClassifications_page_url);

            driver.SwitchTo().Frame(driver.FindElement(By.CssSelector("div[id='rte-body'] iframe")));

            //--- Action: Click on Single Product Models radio button ---//
            action.IClick(driver, Elements.ExportClassification_ViewExportClassifications_SingleProductModel_RadioButton);

            //----- Export Classification Test Plan > English Tab > R3 > T4: Verify wild card search (AL-8962) -----//

            //--- Action: Enter a search with minimum 4 characters ---//
            string valid_singleProductModel_input = "AD7705";
            action.IDeleteValueOnFields(driver, Elements.ExportClassification_ViewExportClassifications_SingleProductModel_InputBox);
            action.IType(driver, Elements.ExportClassification_ViewExportClassifications_SingleProductModel_InputBox, valid_singleProductModel_input);

            //--- Action: Click on the Search Now button ---//
            action.IClick(driver, Elements.ExportClassification_ViewExportClassifications_ProductModelSearch_SearchNow_Button);

            //--- Expected Result: The page will redirect to the EXPORT CLASSIFICATION INFORMATION - Search Result page (AL-6306) ---//
            test.validateStringInstance(driver, driver.Url, Configuration.Env_Url + exportClassificationInformation_page_url);

            bool url_locale = false;
            if (Locale.Equals("cn") || Locale.Equals("zh"))
            {
                url_locale = true;
                test.validateStringInstance(driver, driver.Url, "locale=zh");
            }
            else
            {
                url_locale = true;
                test.validateStringInstance(driver, driver.Url, "locale=" + Locale);
            }

            if (driver.Url.Contains(Configuration.Env_Url + exportClassificationInformation_page_url) && url_locale == true)
            {
                //--- Expected Result: Export to: Excel link should be displayed ---//
                test.validateElementIsPresent(driver, Elements.ExportClassification_ExportClassificationInformation_ExportToExcel_Link);

                //--- Expected Result: ADI Export Classification Information link should be displayed ---//
                test.validateElementIsPresent(driver, Elements.ExportClassification_ExportClassificationInformation_AdiExportClassificationInformation_Link);

                //--- Expected Result: ExportComplianceDepartment@analog.com should be displayed ---//
                test.validateElementIsPresent(driver, Elements.ExportClassification_ExportClassificationInformation_ExportComplianceDepartment_AnalogCom_Link);

                if (util.CheckElement(driver, Elements.ExportClassification_ExportClassificationInformation_RunANewSearch_Link, 1))
                {
                    //--- Action: Click on the the Run a new search link ---//
                    action.IOpenLinkInNewTab(driver, Elements.ExportClassification_ExportClassificationInformation_RunANewSearch_Link);
                    Thread.Sleep(1000);

                    //--- Expected Result: The page will go back to the Export Classification Landing page in the current locale. ---//
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + viewExportClassifications_page_url);
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.ExportClassification_ExportClassificationInformation_RunANewSearch_Link);
                }
            }
        }

        [Test, Category("Export Classification"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Product Table is Present and Working as Expected in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Product Table is Present and Working as Expected in CN Locale")] --> NOTE: AL-17596 has been logged for the Issue where Product Model Search always redirects to Export Classification Information in EN Locale.
        //[TestCase("jp", TestName = "Verify that the Product Table is Present and Working as Expected in JP Locale")] --> NOTE: AL-17596 has been logged for the Issue where Product Model Search always redirects to Export Classification Information in EN Locale.
        //[TestCase("ru", TestName = "Verify that the Product Table is Present and Working as Expected in RU Locale")] --> NOTE: AL-17596 has been logged for the Issue where Product Model Search always redirects to Export Classification Information in EN Locale.
        public void ExportClassification_ExportClassificationInformation_SingleProductModel_SearchResults_VerifyProductTable(string Locale)
        {
            //--- Action: Access Export Classification Information URL ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + viewExportClassifications_page_url);

            driver.SwitchTo().Frame(driver.FindElement(By.CssSelector("div[id='rte-body'] iframe")));

            //--- Action: Click on Single Product Models radio button ---//
            action.IClick(driver, Elements.ExportClassification_ViewExportClassifications_SingleProductModel_RadioButton);

            //----- Export Classification Test Plan > English Tab > R3 > T4: Verify wild card search (AL-8962) -----//

            //--- Action: Enter a search with minimum 4 characters ---//
            string valid_singleProductModel_input = "AD7705";
            action.IDeleteValueOnFields(driver, Elements.ExportClassification_ViewExportClassifications_SingleProductModel_InputBox);
            action.IType(driver, Elements.ExportClassification_ViewExportClassifications_SingleProductModel_InputBox, valid_singleProductModel_input);

            //--- Action: Click on the Search Now button ---//
            action.IClick(driver, Elements.ExportClassification_ViewExportClassifications_ProductModelSearch_SearchNow_Button);

            //--- Expected Result: The page will redirect to the EXPORT CLASSIFICATION INFORMATION - Search Result page (AL-6306) ---//
            test.validateStringInstance(driver, driver.Url, Configuration.Env_Url + exportClassificationInformation_page_url);

            bool url_locale = false;
            if (Locale.Equals("cn") || Locale.Equals("zh"))
            {
                url_locale = true;
                test.validateStringInstance(driver, driver.Url, "locale=zh");
            }
            else
            {
                url_locale = true;
                test.validateStringInstance(driver, driver.Url, "locale=" + Locale);
            }

            if (driver.Url.Contains(Configuration.Env_Url + exportClassificationInformation_page_url) && url_locale == true)
            {
                //--- Expected Result: The Model Number column should be displayed ---//
                test.validateElementIsPresent(driver, Elements.ExportClassification_ExportClassificationInformation_Product_Table_ModelNumber_Column_Header);

                //--- Expected Result: The Description column should be displayed ---//
                test.validateElementIsPresent(driver, Elements.ExportClassification_ExportClassificationInformation_Product_Table_Description_Column_Header);

                //--- Expected Result: The US ECCN column should be displayed ---//
                test.validateElementIsPresent(driver, Elements.ExportClassification_ExportClassificationInformation_Product_Table_UsEccn_Column_Header);

                //--- Expected Result: The SG ECCN column should be displayed ---//
                test.validateElementIsPresent(driver, Elements.ExportClassification_ExportClassificationInformation_Product_Table_SgEccn_Column_Header);

                //--- Expected Result: The IE ECCN column should be displayed ---//
                test.validateElementIsPresent(driver, Elements.ExportClassification_ExportClassificationInformation_Product_Table_IeEccn_Column_Header);

                //--- Expected Result: The Euro Commodity Code column should be displayed ---//
                test.validateElementIsPresent(driver, Elements.ExportClassification_ExportClassificationInformation_Product_Table_EuroCommodityCode_Column_Header);

                //--- Expected Result: The US Commodity Code column should be displayed ---//
                test.validateElementIsPresent(driver, Elements.ExportClassification_ExportClassificationInformation_Product_Table_UsCommodityCode_Column_Header);

                //--- Expected Result: The Country of Origin (Assembly) column should be displayed ---//
                test.validateElementIsPresent(driver, Elements.ExportClassification_ExportClassificationInformation_Product_Table_CountryOfOrigin_Column_Header);

                //--- Expected Result: The Country of Diffusion column should be displayed ---//
                test.validateElementIsPresent(driver, Elements.ExportClassification_ExportClassificationInformation_Product_Table_CountryOfDiffusion_Column_Header);

                //--- Expected Result: The Remove column should be displayed ---//
                test.validateElementIsPresent(driver, Elements.ExportClassification_ExportClassificationInformation_Product_Table_Remove_Column_Header);

                //--- Action: Navigate to the bottom of the page ---//
                action.IMouseOverTo(driver, Elements.Footer);

                //--- Expected Result: The Table Headers will dock on top of the Window ---//
                test.validateElementIsPresent(driver, Elements.ExportClassification_ExportClassificationInformation_Product_Table_Column_Headers);
            }
        }

        [Test, Category("Export Classification"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Remove Button is Present and Working as Expected in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Remove Button is Present and Working as Expected in CN Locale")] --> NOTE: AL-17596 has been logged for the Issue where Product Model Search always redirects to Export Classification Information in EN Locale.
        //[TestCase("jp", TestName = "Verify that the Remove Button is Present and Working as Expected in JP Locale")] --> NOTE: AL-17596 has been logged for the Issue where Product Model Search always redirects to Export Classification Information in EN Locale.
        //[TestCase("ru", TestName = "Verify that the Remove Button is Present and Working as Expected in RU Locale")] --> NOTE: AL-17596 has been logged for the Issue where Product Model Search always redirects to Export Classification Information in EN Locale.
        public void ExportClassification_ExportClassificationInformation_SingleProductModel_SearchResults_VerifyRemoveButton(string Locale)
        {
            //--- Action: Access Export Classification Information URL ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + viewExportClassifications_page_url);

            driver.SwitchTo().Frame(driver.FindElement(By.CssSelector("div[id='rte-body'] iframe")));

            //--- Action: Click on Single Product Models radio button ---//
            action.IClick(driver, Elements.ExportClassification_ViewExportClassifications_SingleProductModel_RadioButton);

            //----- Export Classification Test Plan > English Tab > R3 > T4: Verify wild card search (AL-8962) -----//

            //--- Action: Enter a search with minimum 4 characters ---//
            string valid_singleProductModel_input = "AD7705";
            action.IDeleteValueOnFields(driver, Elements.ExportClassification_ViewExportClassifications_SingleProductModel_InputBox);
            action.IType(driver, Elements.ExportClassification_ViewExportClassifications_SingleProductModel_InputBox, valid_singleProductModel_input);

            //--- Action: Click on the Search Now button ---//
            action.IClick(driver, Elements.ExportClassification_ViewExportClassifications_ProductModelSearch_SearchNow_Button);

            //--- Expected Result: The page will redirect to the EXPORT CLASSIFICATION INFORMATION - Search Result page (AL-6306) ---//
            test.validateStringInstance(driver, driver.Url, Configuration.Env_Url + exportClassificationInformation_page_url);

            bool url_locale = false;
            if (Locale.Equals("cn") || Locale.Equals("zh"))
            {
                url_locale = true;
                test.validateStringInstance(driver, driver.Url, "locale=zh");
            }
            else
            {
                url_locale = true;
                test.validateStringInstance(driver, driver.Url, "locale=" + Locale);
            }

            if (driver.Url.Contains(Configuration.Env_Url + exportClassificationInformation_page_url) && url_locale == true)
            {
                //--- Expected Result: "Showing x Product Matches" will be displayed above the table. ---//
                int default_productTable_row_count = util.GetCount(driver, Elements.ExportClassification_ExportClassificationInformation_Product_Table_Rows) - 1;
                if (Locale.Equals("en"))
                {
                    test.validateStringIsCorrect(driver, Elements.ExportClassification_ExportClassificationInformation_ProductMatches_Label, "Showing " + default_productTable_row_count + " Product Matches");
                }
                else
                {
                    test.validateStringIsCorrect(driver, Elements.ExportClassification_ExportClassificationInformation_ProductMatches_Label, default_productTable_row_count.ToString());
                }

                if (util.CheckElement(driver, Elements.ExportClassification_ExportClassificationInformation_Product_Table_Remove_Column_Remove_Buttons, 1))
                {
                    //--- Action: Click on the Remove button under the Remove column ---//
                    do
                    {
                        action.IClick(driver, Elements.ExportClassification_ExportClassificationInformation_Product_Table_Remove_Column_Remove_Buttons);
                    }
                    while (util.CheckElement(driver, Elements.ExportClassification_ExportClassificationInformation_Product_Table_Remove_Column_Remove_Buttons, 2));

                    //--- Expected Result: The row of the clicked Remove button will be deleted. ---//
                    int productTable_row_count = util.GetCount(driver, Elements.ExportClassification_ExportClassificationInformation_Product_Table_Rows) - 1;
                    test.validateCountIsLessOrEqual(driver, productTable_row_count, 0);

                    //--- Expected Result: "No matches remaining." will be displayed on above the table if all the rows were removed ---//
                    if (Locale.Equals("en"))
                    {
                        test.validateStringIsCorrect(driver, Elements.ExportClassification_ExportClassificationInformation_ProductMatches_Label, noMatchesRemaining_label);
                    }
                    else
                    {
                        test.validateElementIsPresent(driver, Elements.ExportClassification_ExportClassificationInformation_ProductMatches_Label);
                    }

                    //--- Expected Result: "Restore x Hidden Products" will be displayed above the table. ---//
                    test.validateElementIsPresent(driver, Elements.ExportClassification_ExportClassificationInformation_RestoreHiddenProducts_Link);

                    if (util.CheckElement(driver, Elements.ExportClassification_ExportClassificationInformation_RestoreHiddenProducts_Link, 1))
                    {
                        //--- Action: Click on the Restore x Hidden Products link ---//
                        action.IClick(driver, Elements.ExportClassification_ExportClassificationInformation_RestoreHiddenProducts_Link);

                        //--- Expected Result: All the removed items will be displayed in the table. ---//
                        productTable_row_count = util.GetCount(driver, Elements.ExportClassification_ExportClassificationInformation_Product_Table_Rows) - 1;
                        test.validateCountIsEqual(driver, default_productTable_row_count, productTable_row_count);
                    }
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.ExportClassification_ExportClassificationInformation_Product_Table_Remove_Column_Remove_Buttons);
                }
            }
        }
    }
}