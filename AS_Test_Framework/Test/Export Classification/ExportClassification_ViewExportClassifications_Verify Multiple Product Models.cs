﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using OpenQA.Selenium;

namespace AS_Test_Framework.ExportClassification
{
    [TestFixture]
    public class ExportClassification_ViewExportClassifications_MultipleProductModelsSearch : BaseSetUp
    {
        public ExportClassification_ViewExportClassifications_MultipleProductModelsSearch() : base() { }

        //--- URLs ---//
        string viewExportClassifications_page_url = "/support/customer-service-resources/customer-service/view-export-classification.html";

        //--- Labels ---//
        string multipleProductModels_searchTextGuide = "Enter full model part number. Enter multiple model part numbers on separate lines.";
        string multipleProductModels_errorMessage = "A minimum of four characters is required.";

        [Test, Category("Export Classification"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Multiple Product Models Search is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Multiple Product Models Search is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Multiple Product Models Search is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Multiple Product Models Search is Present and Working as Expected in RU Locale")]
        public void ExportClassification_ViewExportClassifications_VerifyMultipleProductModelsSearch(string Locale)
        {
            //--- Action: Open Export Classification Information URL ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + viewExportClassifications_page_url);

            //----- Export Classification Test Plan > English Tab > R2 > T5: Verify Product Model Search section (AL-10009) -----//
            driver.SwitchTo().Frame(driver.FindElement(By.CssSelector("div[id='rte-body'] iframe")));

            //--- Expected Result: Multiple Product Models search should be displayed ---//
            test.validateElementIsPresent(driver, Elements.ExportClassification_ViewExportClassifications_MultipleProductModels);

            if (util.CheckElement(driver, Elements.ExportClassification_ViewExportClassifications_MultipleProductModels, 1))
            {
                //--- Action: Click the Multiple Product Models radio button ---//
                action.IClick(driver, Elements.ExportClassification_ViewExportClassifications_MultipleProductModels_RadioButton);

                if (Locale.Equals("en"))
                {
                    //--- Expected Result: The Search Text Guide should be displayed (AL-10722) ---//
                    test.validateStringInstance(driver, multipleProductModels_searchTextGuide, util.GetCssValue(driver, Elements.ExportClassification_ViewExportClassifications_MultipleProductModels_InputBox, "value"));
                }

                //----- Export Classification Test Plan > English Tab > R2 > T4: Verify help icon -----//

                //--- Action: Hover over on help icon beside Multiple Product Models label ---//
                action.IMouseOverTo(driver, Elements.ExportClassification_ViewExportClassifications_MultipleProductModels_Help_Icon);

                //--- Expected Result: Multi Product Search help should be displayed ---//
                test.validateElementIsPresent(driver, Elements.ExportClassification_ViewExportClassifications_MultipleProductModels_Help_ToolTip);
            }
        }

        [Test, Category("Export Classification"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Multiple Product Models Search is Present and Working as Expected when Invalid Input is Entered in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Multiple Product Models Search is Present and Working as Expected when Invalid Input is Entered in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Multiple Product Models Search is Present and Working as Expected when Invalid Input is Entered in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Multiple Product Models Search is Present and Working as Expected when Invalid Input is Entered in RU Locale")]
        public void ExportClassification_ViewExportClassifications_MultipleProductModelsSearch_VerifyWhenInvalidInputIsEntered(string Locale)
        {
            //--- Action: Access Export Classification Information URL ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + viewExportClassifications_page_url);

            driver.SwitchTo().Frame(driver.FindElement(By.CssSelector("div[id='rte-body'] iframe")));

            //--- Action: Click on Multiple Product Models radio button ---//
            action.IClick(driver, Elements.ExportClassification_ViewExportClassifications_MultipleProductModels_RadioButton);

            //----- Export Classification Test Plan > English Tab > R4 > T1: Verify partial number search less than minimum requirement (AL-8962) -----//

            //--- Action: Enter a search with 3 characters ---//
            string invalid_multipleProductModels_input = "HMC";
            action.IDeleteValueOnFields(driver, Elements.ExportClassification_ViewExportClassifications_MultipleProductModels_InputBox);
            action.IType(driver, Elements.ExportClassification_ViewExportClassifications_MultipleProductModels_InputBox, invalid_multipleProductModels_input);

            //--- Action: Click on the Search Now button ---//
            action.IClick(driver, Elements.ExportClassification_ViewExportClassifications_ProductModelSearch_SearchNow_Button);

            //--- Expected Result: "A minimum of four characters is required." message should be displayed below the text field ---//
            if (Locale.Equals("en"))
            {
                test.validateStringIsCorrect(driver, Elements.ExportClassification_ViewExportClassifications_MultipleProductModels_ErrorMessage, multipleProductModels_errorMessage);
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.ExportClassification_ViewExportClassifications_MultipleProductModels_ErrorMessage);
            }
        }
    }
}