﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using OpenQA.Selenium;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.ExportClassification
{
    [TestFixture]
    public class ExportClassification_ExportClassificationInformation_SingleProductModel_NoSearchResult : BaseSetUp
    {
        public ExportClassification_ExportClassificationInformation_SingleProductModel_NoSearchResult() : base() { }

        //--- URLs ---//
        string viewExportClassifications_page_url = "/support/customer-service-resources/customer-service/view-export-classification.html";
        string exportClassificationInformation_page_url = "exportclassification/Search/Result";
        string technicalSupport_page_url = "/support/technical-support.html";

        //--- Labels ---//
        string noResults_message = "No results were found for your search term";

        [Test, Category("Export Classification"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Page is Working as Expected in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Page is Working as Expected in CN Locale")] --> NOTE: AL-17596 has been logged for the Issue where Product Model Search always redirects to Export Classification Information in EN Locale.
        //[TestCase("jp", TestName = "Verify that the Page is Working as Expected in JP Locale")] --> NOTE: AL-17596 has been logged for the Issue where Product Model Search always redirects to Export Classification Information in EN Locale.
        //[TestCase("ru", TestName = "Verify that the Page is Working as Expected in RU Locale")] --> NOTE: AL-17596 has been logged for the Issue where Product Model Search always redirects to Export Classification Information in EN Locale.
        public void ExportClassification_ExportClassificationInformation_SingleProductModel_VerifyNoSearchResult(string Locale)
        {
            //--- Action: Access Export Classification Information URL ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + viewExportClassifications_page_url);

            driver.SwitchTo().Frame(driver.FindElement(By.CssSelector("div[id='rte-body'] iframe")));

            //--- Action: Click on Single Product Models radio button ---//
            action.IClick(driver, Elements.ExportClassification_ViewExportClassifications_SingleProductModel_RadioButton);

            //----- Export Classification Test Plan > English Tab > R3 > T2: Verify search when no results returned (AL-8962) -----//

            //--- Action: Enter an invalid model number ---//
            string invalid_singleProductModel_input = "ADDDD";
            action.IDeleteValueOnFields(driver, Elements.ExportClassification_ViewExportClassifications_SingleProductModel_InputBox);
            action.IType(driver, Elements.ExportClassification_ViewExportClassifications_SingleProductModel_InputBox, invalid_singleProductModel_input);

            //--- Action: Click on Search Now button ---//
            action.IClick(driver, Elements.ExportClassification_ViewExportClassifications_ProductModelSearch_SearchNow_Button);

            //--- Expected Result: The page will redirect to the EXPORT CLASSIFICATION INFORMATION - Search Result page in the current locale ---//
            test.validateStringInstance(driver, driver.Url, Configuration.Env_Url + exportClassificationInformation_page_url);

            bool url_locale = false;
            if (Locale.Equals("cn") || Locale.Equals("zh"))
            {
                url_locale = true;
                test.validateStringInstance(driver, driver.Url, "locale=zh");
            }
            else
            {
                url_locale = true;
                test.validateStringInstance(driver, driver.Url, "locale=" + Locale);
            }

            if (driver.Url.Contains(Configuration.Env_Url + exportClassificationInformation_page_url) && url_locale == true)
            {
                //----- Export Classification Test Plan > English Tab > R3 > T10: Verify nav menus are present in Export Classification Information under “No results were found….” page. (AL-10711) -----//

                //--- Expected Result: Navigational menus should be displayed/present ---//
                test.validateElementIsPresent(driver, Elements.MainNavigation);

                //----- Export Classification Test Plan > English Tab > R3 > T2: Verify search when no results returned (AL-8962) -----//

                //--- Expected Result: "No results were found for your search term "xxx"" will be displayed. ---//
                if (Locale.Equals("en"))
                {
                    test.validateStringIsCorrect(driver, Elements.ExportClassification_ExportClassificationInformation_NoResults_Label, noResults_message + " \"" + invalid_singleProductModel_input + "\"");
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.ExportClassification_ExportClassificationInformation_NoResults_Label);
                }

                if (util.CheckElement(driver, Elements.ExportClassification_ExportClassificationInformation_TechnicalSupport_Link, 1))
                {
                    //--- Action: Click on the Technical Support link ---//
                    action.IOpenLinkInNewTab(driver, Elements.ExportClassification_ExportClassificationInformation_TechnicalSupport_Link);
                    Thread.Sleep(1000);

                    //--- Expected Result: The page will redirect to the Technical Support page in the current locale. ---//
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + technicalSupport_page_url);

                    driver.Close();
                    driver.SwitchTo().Window(driver.WindowHandles.First());
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.ExportClassification_ExportClassificationInformation_TechnicalSupport_Link);
                }

                //--- Expected Result: ADI Export Classification Information link is displayed ---//
                test.validateElementIsPresent(driver, Elements.ExportClassification_ExportClassificationInformation_AdiExportClassificationInformation_Link);

                //--- Expected Result: ExportComplianceDepartment@analog.com is displayed ---//
                test.validateElementIsPresent(driver, Elements.ExportClassification_ExportClassificationInformation_ExportComplianceDepartment_AnalogCom_Link);

                if (util.CheckElement(driver, Elements.ExportClassification_ExportClassificationInformation_RunANewSearch_Link, 1))
                {
                    //--- Action: Click on the Run a new search link ---//
                    action.IOpenLinkInNewTab(driver, Elements.ExportClassification_ExportClassificationInformation_RunANewSearch_Link);

                    //--- Expected Result: The page will go back to the Export Classification Landing page in the current locale ---//
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + viewExportClassifications_page_url);
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.ExportClassification_ExportClassificationInformation_RunANewSearch_Link);
                }
            }
        }
    }
}