﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using OpenQA.Selenium;

namespace AS_Test_Framework.ExportClassification
{
    [TestFixture]
    public class ExportClassification_ViewExportClassifications_SingleProductModelSearch : BaseSetUp
    {
        public ExportClassification_ViewExportClassifications_SingleProductModelSearch() : base() { }

        //--- URLs ---//
        string viewExportClassifications_page_url = "/support/customer-service-resources/customer-service/view-export-classification.html";

        //--- Labels ---//
        string singleProductModel_errorMessage = "A minimum of four characters is required.";

        [Test, Category("Export Classification"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Single Product Model Search is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Single Product Model Search is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Single Product Model Search is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Single Product Model Search is Present and Working as Expected in RU Locale")]
        public void ExportClassification_ViewExportClassifications_VerifySingleProductModelSearch(string Locale)
        {
            //--- Action: Open Export Classification Information URL ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + viewExportClassifications_page_url);

            //----- Export Classification Test Plan > English Tab > R2 > T5: Verify Product Model Search section (AL-10009) -----//
            driver.SwitchTo().Frame(driver.FindElement(By.CssSelector("div[id='rte-body'] iframe")));

            //--- Expected Result: Single Product Model search should be displayed ---//
            test.validateElementIsPresent(driver, Elements.ExportClassification_ViewExportClassifications_SingleProductModel);

            if (util.CheckElement(driver, Elements.ExportClassification_ViewExportClassifications_SingleProductModel, 1))
            {
                //--- Action: Click the Multiple Product Models radio button ---//
                action.IClick(driver, Elements.ExportClassification_ViewExportClassifications_SingleProductModel_RadioButton);

                //----- Export Classification Test Plan > English Tab > R2 > T3: Verify nav menus are present in Export Classification Information under “No results were found….” page. (AL-10711) -----//

                //--- Expected Result: Textbox must be displayed under “Single Product Model”. ---//
                test.validateElementIsPresent(driver, Elements.ExportClassification_ViewExportClassifications_SingleProductModel_InputBox);

                //----- Export Classification Test Plan > English Tab > R2 > T4: Verify help icon -----//

                //--- Action: Hover over on help icon beside Single Product Model label ---//
                action.IMouseOverTo(driver, Elements.ExportClassification_ViewExportClassifications_SingleProductModel_Help_Icon);

                //--- Expected Result: Single Product Search help should be displayed ---//
                test.validateElementIsPresent(driver, Elements.ExportClassification_ViewExportClassifications_SingleProductModel_Help_ToolTip);
            }
        }

        [Test, Category("Export Classification"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Single Product Model Search is Present and Working as Expected when Invalid Input is Entered in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Single Product Model Search is Present and Working as Expected when Invalid Input is Entered in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Single Product Model Search is Present and Working as Expected when Invalid Input is Entered in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Single Product Model Search is Present and Working as Expected when Invalid Input is Entered in RU Locale")]
        public void ExportClassification_ViewExportClassifications_SingleProductModelSearch_VerifyWhenInvalidInputIsEntered(string Locale)
        {
            //--- Action: Access Export Classification Information URL ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + viewExportClassifications_page_url);

            driver.SwitchTo().Frame(driver.FindElement(By.CssSelector("div[id='rte-body'] iframe")));

            //--- Action: Click on Single Product Models radio button ---//
            action.IClick(driver, Elements.ExportClassification_ViewExportClassifications_SingleProductModel_RadioButton);

            //----- Export Classification Test Plan > English Tab > R3 > T1: Verify partial number search less than minimum requirement -----//

            //--- Action: Without any input, click on Search Now button ---//
            action.IDeleteValueOnFields(driver, Elements.ExportClassification_ViewExportClassifications_SingleProductModel_InputBox);
            action.IClick(driver, Elements.ExportClassification_ViewExportClassifications_ProductModelSearch_SearchNow_Button);

            //--- Expected Result: message should be displayed below the text field. ---//
            if (Locale.Equals("en"))
            {
                test.validateStringIsCorrect(driver, Elements.ExportClassification_ViewExportClassifications_SingleProductModel_ErrorMessage, singleProductModel_errorMessage);
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.ExportClassification_ViewExportClassifications_SingleProductModel_ErrorMessage);
            }

            //--- Action: Enter a search with 3 characters ---//
            string invalid_singleProductModel_input = "AD7";
            action.IDeleteValueOnFields(driver, Elements.ExportClassification_ViewExportClassifications_SingleProductModel_InputBox);
            action.IType(driver, Elements.ExportClassification_ViewExportClassifications_SingleProductModel_InputBox, invalid_singleProductModel_input);

            //--- Action: Click on the Search Now button ---//
            action.IClick(driver, Elements.ExportClassification_ViewExportClassifications_ProductModelSearch_SearchNow_Button);

            //--- Expected Result: "A minimum of four characters is required." message should be displayed below the text field ---//
            if (Locale.Equals("en"))
            {
                test.validateStringIsCorrect(driver, Elements.ExportClassification_ViewExportClassifications_SingleProductModel_ErrorMessage, singleProductModel_errorMessage);
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.ExportClassification_ViewExportClassifications_SingleProductModel_ErrorMessage);
            }
        }
    }
}