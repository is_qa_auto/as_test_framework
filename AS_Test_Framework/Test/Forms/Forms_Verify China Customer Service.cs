﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using OpenQA.Selenium;
using System;
using System.Threading;

namespace AS_Test_Framework.Forms
{
    [TestFixture]
    public class Forms_ChinaCustomerService : BaseSetUp
    {
        //--- China Customer Service Form URL: ---//
        //--- DEV: https://form-dev.corpnt.analog.com/Form_Pages/support/CustomerService_China.aspx ---//
        //--- QA: https://form-qa.corpnt.analog.com/Form_Pages/support/CustomerService_China.aspx ---//
        //--- PROD: https://form.analog.com/Form_Pages/support/CustomerService_China.aspx---//

        public Forms_ChinaCustomerService() : base() { }

        //--- Login Credentials ---//
        string username = "f0rms_t3st_555@mailinator.com";
        string password = "Test_1234";

        //--- URLs ---//
        string chinaCustomerService_form_url = "Form_Pages/support/CustomerService_China.aspx";
        string chinaCustomerService_thankYou_page_url = "/customer-service-support.html";

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("zh", TestName = "Verify that the Elements are Present and Working as Expected when the User is Logged Out in CN Locale")]
        public void Forms_ChinaCustomerService_VerifyElementsWhenUserIsLoggedOut(string Locale)
        {
            //--- China Customer Service Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + chinaCustomerService_form_url;

            //--- Action: Navigate to given URL ---//
            action.Navigate(driver, form_url);

            //----- Form TestPlan > ZH Tab > R1 > T2_1: Login using Save time to login section with no data -----//

            //----- Save time by logging in or registering for myAnalog -----//

            if (!util.CheckElement(driver, Elements.Forms_Expanded_SaveTimeByLoggingInOrRegisteringForMyAnalog_Panel, 1))
            {
                //--- Action: Click on  arrow down to view  the save time to login section ---//
                action.IClick(driver, Elements.Forms_SaveTimeByLoggingInOrRegisteringForMyAnalog_Link);
            }

            //--- Expected Result: Save time to login section should be displayed ---//
            test.validateElementIsPresent(driver, Elements.Forms_Expanded_SaveTimeByLoggingInOrRegisteringForMyAnalog_Panel);
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("zh", TestName = "Verify that the Elements are Present and Working as Expected when the User is Logged In in CN Locale")]
        public void Forms_ChinaCustomerService_VerifyElementsWhenUserIsLoggedIn(string Locale)
        {
            //--- China Customer Service Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + chinaCustomerService_form_url;

            //--- Action: Navigate to given URL ---//
            action.Navigate(driver, form_url);

            //----- Form TestPlan > ZH Tab > R1 > T2_3: Login using Save time to login section with valid data -----//

            //----- Save time by logging in or registering for myAnalog -----//

            //--- Action: Login using valid data ---//
            action.ILogInViaSaveTimeByLoggingInPanel(driver, username, password);
            Thread.Sleep(1000);

            //----- 您的基本资料 (Customer Information) -----//

            //--- Expected Result: The メールアドレス(Email Address) field should be  automatically populated ---//
            test.validateFieldIsNotEmpty(driver, Elements.Forms_CustomerServiceSupport_EmailId_InputBox);

            //--- Expected Result: The 名(Name) field should be  automatically populated ---//
            test.validateFieldIsNotEmpty(driver, Elements.Forms_FirstName_InputBox);

            //--- Expected Result: The 姓(Surname ) field should be  automatically populated ---//
            test.validateFieldIsNotEmpty(driver, Elements.Forms_LastName_InputBox);

            //----- 您的详细信息 (Your Details) -----//

            //--- Expected Result: The 社名＆団体名(Company Name and Organization ) field should be  automatically populated ---//
            test.validateFieldIsNotEmpty(driver, Elements.Forms_CompanyName_InputBox);
        }

        [TestCase("zh", TestName = "Verify that the Input Validations are Working as Expected for Invalid Inputs in CN Locale")]
        public void Forms_ChinaCustomerService_VerifyInputValidationsForInvalidInputs(string Locale)
        {
            //--- China Customer Service Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + chinaCustomerService_form_url;

            //--- Action: Navigate to given URL ---//
            action.Navigate(driver, form_url);

            //----- Form TestPlan > ZH Tab > R1 > T3_2: Submit the China Technical Support Form with invalid data -----//

            //----- 您的基本资料 (Customer Information) -----//

            //--- Action: Enter invalid data on the Email Address field. (e.g. ad.123) ---//
            string email_input = "ad.123";
            action.IDeleteValueOnFields(driver, Elements.Forms_CustomerServiceSupport_EmailId_InputBox);
            action.IType(driver, Elements.Forms_CustomerServiceSupport_EmailId_InputBox, email_input);
            action.IType(driver, Elements.Forms_CustomerServiceSupport_EmailId_InputBox, Keys.Tab);

            //--- Expected Result: The error message should be displayed: 请输入有效的邮件地址(Please enter a valid email address) ---//
            test.validateElementIsPresent(driver, Elements.Forms_ChinaCustomerService_Email_InvalidInput_ErrorMessage);
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("zh", TestName = "Verify that the Input Validations are Working as Expected for Blank Inputs in CN Locale")]
        public void Forms_ChinaCustomerService_VerifyInputValidationsForBlankInputs(string Locale)
        {
            //--- China Customer Service Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + chinaCustomerService_form_url;

            //--- Action: Navigate to given URL ---//
            action.Navigate(driver, form_url);

            //----- Form TestPlan > ZH Tab > R1 > T3_1: Submit China Customer ServiceForm  with no data on mandatory fields -----//

            //--- Action: Click on the Submit button without entering data on the mandatory fields. ---//
            action.IClick(driver, Elements.Forms_Submit_Button);

            //----- 支持类型 (The Type of Support) -----//

            //--- Expected Result: 您需要哪一类支持? (What kind of support you need?)bullet: 请选择至少一类支持Please select at least one class support Error Message should displayed ---//
            test.validateElementIsPresent(driver, Elements.Forms_ChinaCustomerService_WhatTypeOfSupportDoYouNeed_InvalidInput_ErrorMessage);

            //----- 您的基本资料 (Your Basic Information) -----//

            //--- Expected Result: 电子邮件(Email): 电子邮件”为必填项 E-mail "is required Error Message should displayed ---//
            test.validateElementIsPresent(driver, Elements.Forms_ChinaCustomerService_Email_BlankInput_ErrorMessage);

            //--- Expected Result: 名(First Name)： “名”为必填项 "Name" is required Error Message should displayed ---//
            test.validateElementIsPresent(driver, Elements.Forms_ChinaCustomerService_FirstName_BlankInput_ErrorMessage);

            //--- Expected Result: 姓(Last Name )：“名”为必填项 "Name" is required Error Message should displayed ---//
            test.validateElementIsPresent(driver, Elements.Forms_ChinaCustomerService_LastName_BlankInput_ErrorMessage);

            //----- 您的详细信息 (Your Details) -----//

            //--- Expected Result: 机构名称(Organization Name ) :“机构名称”为必填项"Organization Name" is required. Error Message should displayed ---//
            test.validateElementIsPresent(driver, Elements.Forms_ChinaCustomerService_CompanyName_BlankInput_ErrorMessage);

            //--- Expected Result: 电话(Phone)： 请选择至少一类支持Please select at least one class support Error Message should displayed ---//
            test.validateElementIsPresent(driver, Elements.Forms_ChinaCustomerService_Telephone_BlankInput_ErrorMessage);

            //----- 您的问题或意见（请限制在1500个字以内)Your question or comment (please limit within 1500 words) -----//

            //--- Expected Result: 请在此写下您的详细问题或意见(Please write your questions or comments in detail in this): “问题/意见”为必填项 "Questions / Comments" is required. Error Message should displayed ---//
            test.validateElementIsPresent(driver, Elements.Forms_PleaseWriteYourQuestionsOrCommentsHere_BlankInput_ErrorMessage);
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("zh", TestName = "Verify that the Form Submission is Working as Expected in CN Locale")]
        public void Forms_ChinaCustomerService_VerifyFormSubmission(string Locale)
        {
            if (Util.Configuration.Environment.Equals("dev") || Util.Configuration.Environment.Equals("qa"))
            {
                //--- China Customer Service Form URL ---//
                string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + chinaCustomerService_form_url;

                //--- Action: Navigate to given URL ---//
                action.Navigate(driver, form_url);

                //----- Form TestPlan > ZH Tab > R1 > T3_3: Submit China Customer ServiceForm with valid data (AL-2496) -----//

                //--- Action: Enter valid data on the mandatory fields. ---//

                //----- 支持类型 (The Type of Support) -----//
                action.IClick(driver, Elements.Forms_WhatTypeOfSupportDoYouNeed_RadioButtons);

                //----- 您的基本资料 (Your Basic Information) -----//
                string email_input = "t3st_" + Util.Configuration.Environment.ToLower() + "_chinaCustomerService_f0rm_" + util.GenerateRandomNumber() + "@mailinator.com";
                //Console.WriteLine("Email Id Input = " + email_input);
                action.IDeleteValueOnFields(driver, Elements.Forms_CustomerServiceSupport_EmailId_InputBox);
                action.IType(driver, Elements.Forms_CustomerServiceSupport_EmailId_InputBox, email_input);

                string firstName_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_FirstName_InputBox);
                action.IType(driver, Elements.Forms_FirstName_InputBox, firstName_input);

                string lastName_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_LastName_InputBox);
                action.IType(driver, Elements.Forms_LastName_InputBox, lastName_input);

                //----- 您的详细信息 (Your Details) -----//
                string companyName_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_CompanyName_InputBox);
                action.IType(driver, Elements.Forms_CompanyName_InputBox, companyName_input);

                string telephone_input = "123456789";
                action.IDeleteValueOnFields(driver, Elements.Forms_Telephone_InputBox);
                action.IType(driver, Elements.Forms_Telephone_InputBox, telephone_input);

                //----- 您的问题或意见（请限制在1500个字以内)Your question or comment (please limit within 1500 words) -----//
                string pleaseWriteYourQuestionsOrCommentsHere_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_Comments_TextArea);
                action.IType(driver, Elements.Forms_Comments_TextArea, pleaseWriteYourQuestionsOrCommentsHere_input);

                //----- 隐私设置 (Privacy Settings) -----//
                action.ICheck(driver, Elements.Forms_Communication_Checkbox);

                //--- Action: Click on submit button ---//
                action.IClick(driver, Elements.Forms_Submit_Button);

                //--- Expected Result: China Customer Service Thank You page should be displayed ---//
                test.validateStringInstance(driver, driver.Url, chinaCustomerService_thankYou_page_url);
            }
        }
    }
}