﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using OpenQA.Selenium;
using System;
using System.Threading;

namespace AS_Test_Framework.Forms
{
    [TestFixture]
    public class Forms_PlcDemoSchematicsAndLayout : BaseSetUp
    {
        //--- PLC Demo Schematics and Layout Form URL: ---//
        //--- DEV: https://form-dev.corpnt.analog.com/Form_Pages/Subscription/PLCDemo.aspx ---//
        //--- QA: https://form-qa.corpnt.analog.com/Form_Pages/Subscription/PLCDemo.aspx ---//
        //--- PROD: https://form.analog.com/Form_Pages/Subscription/PLCDemo.aspx ---//

        public Forms_PlcDemoSchematicsAndLayout() : base() { }

        //--- Login Credentials ---//
        string username = "f0rms_t3st_555@mailinator.com";
        string password = "Test_1234";

        //--- URLs ---//
        string plcDemoSchematicsAndLayout_form_url = "Form_Pages/Subscription/PLCDemo.aspx";
        string plcDemoSchematicsAndLayout_thankYou_page_url = "/PLC_Demo_Schematics_and_Layout_ThankYou.html";

        //--- Labels ---//
        string countryRegion_text = "Country/Region";
        string email_blankInput_errorMessage = "Email address is required";
        string firstName_blankInput_errorMessage = "First Name is required";
        string lastName_blankInput_errorMessage = "Last Name is required";
        string jobTitle_blankInput_errorMessage = "Job Title is required";
        string organizationName_blankInput_errorMessage = "Organization name is required";
        string address_1_blankInput_errorMessage = "Address Line 1 is required";
        string city_blankInput_errorMessage = "City is required";
        string stateProvince_blankInput_errorMessage = "State/Province is required";
        string zipPostalCode_blankInput_errorMessage = "Zip/Postal Code is required";
        string countryRegion_blankInput_errorMessage = "Country/Region is required";
        string whyAreYouPrimarilyInterestedInThePlcDemoBoardSchematics_blankInput_errorMessage = "Comments are required.";
        string emailId_invalidInput_errorMessage = "Please enter a valid email address";
        string ifOutsideTheUsOrCanadaPleaseEnterTheRegion_blankInput_errorMessage = "Please provide other State/Province";

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Elements are Present and Working as Expected when the User is Logged Out in EN Locale")]
        public void Forms_PlcDemoSchematicsAndLayout_VerifyElementsWhenUserIsLoggedOut(string Locale)
        {
            //--- PLC Demo Schematics and Layout Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + plcDemoSchematicsAndLayout_form_url;

            //--- Action: Navigate to the given URL ---//
            action.Navigate(driver, form_url);

            //----- Form TestPlan > EN Tab > R40 > T1: Verify all labels for Country is changed to "Country/Region" (IQ-4970/AL-12466) -----//

            //----- Your detailed information -----//

            //--- Expected Result: The Country label has been changed to Country/Region ---//
            test.validateStringInstance(driver, util.GetText(driver, Elements.Forms_Country_Label), countryRegion_text);

            //----- Form TestPlan > EN Tab > R38 > T1: Verify Privacy Settings in All Forms [account is not logged in] (AL-11345) -----//

            //----- Privacy Settings -----//

            //--- Expected Result: Privacy Settings section is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_Section);

            //--- Expected Result: Prompt "You can change your privacy settings at any time by clicking on the unsubscribe link in emails sent from Analog Devices or in Analog’s Privacy Settings." is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_LegalDisclaimer_Text);

            //--- Expected Result: Privacy Settings is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_Link);

            //--- Expected Result: Privacy & Security Statement ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacyAndSecurityStatement_Link);

            //--- Expected Result: Authorized Partners ---//
            test.validateElementIsPresent(driver, Elements.Forms_AuthorizedPartners_Link);

            //----- Form TestPlan > EN Tab > R13 > T3: Login using Save time to login section with valid data -----//

            //----- Save time by logging in or registering for myAnalog -----//

            if (!util.CheckElement(driver, Elements.Forms_Expanded_SaveTimeByLoggingInOrRegisteringForMyAnalog_Panel, 1))
            {
                //--- Action: Click on Save time by logging in or registering for myAnalog   link ---//
                action.IClick(driver, Elements.Forms_SaveTimeByLoggingInOrRegisteringForMyAnalog_Link);
            }

            //--- Expected Result: Save time to login section should be displayed ---//
            test.validateElementIsPresent(driver, Elements.Forms_Expanded_SaveTimeByLoggingInOrRegisteringForMyAnalog_Panel);           
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Elements are Present and Working as Expected when the User is Logged In in EN Locale")]
        public void Forms_PlcDemoSchematicsAndLayout_VerifyElementsWhenUserIsLoggedIn(string Locale)
        {
            //--- PLC Demo Schematics and Layout Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + plcDemoSchematicsAndLayout_form_url;

            //--- Action: Navigate to the given URL ---//
            action.Navigate(driver, form_url);

            //----- Form TestPlan > EN Tab > R13 > T3: Login using Save time to login section with valid data -----//

            //----- Save time by logging in or registering for myAnalog -----//

            //--- Action: Login using valid data ---//
            action.ILogInViaSaveTimeByLoggingInPanel(driver, username, password);
            Thread.Sleep(1000);

            driver.Navigate().Refresh();
            Thread.Sleep(1000);

            //----- About you -----//

            //--- Expected Result: The Emai field should be  automatically populated ---//
            test.validateFieldIsNotEmpty(driver, Elements.Forms_Email_InputBox);

            //--- Expected Result: The First Name field should be  automatically populated ---//
            test.validateFieldIsNotEmpty(driver, Elements.Forms_FirstName_InputBox);

            //--- Expected Result: The Last Name field should be  automatically populated ---//
            test.validateFieldIsNotEmpty(driver, Elements.Forms_LastName_InputBox);

            //----- Your detailed information -----//

            //--- Expected Result: The Organization Name field should be  automatically populated ---//
            test.validateFieldIsNotEmpty(driver, Elements.Forms_OrganizationName_InputBox);

            //--- Expected Result: The Country(dropdown) field should be  automatically populated ---//
            test.validateStringChanged(driver, util.GetSelectedDropdownValue(driver, Elements.Forms_CountryRegion_Dropdown), "Select Country/Region");

            //--- Expected Result: The State/Province field should be  automatically populated ---//
            test.validateStringChanged(driver, util.GetSelectedDropdownValue(driver, Elements.Forms_StateProvince_Dropdown), "Select State/Province");

            //----- Form TestPlan > EN Tab > R38 > T1: Verify Privacy Settings in All Forms [account is logged in] (AL-11345) -----//

            //----- Privacy Settings -----//

            //--- Expected Result: Prompt "You can change your privacy settings at any time by clicking on the unsubscribe link in emails sent from Analog Devices or in Analog’s Privacy Settings." is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_LegalDisclaimer_Text);

            //--- Expected Result: Privacy Settings is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_Link);

            //--- Expected Result: Privacy & Security Statement ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacyAndSecurityStatement_Link);

            //--- Expected Result: Authorized Partners ---//
            test.validateElementIsPresent(driver, Elements.Forms_AuthorizedPartners_Link);
        }

        [TestCase("en", TestName = "Verify that the Input Validations are Working as Expected for Invalid Inputs in EN Locale")]
        public void Forms_PlcDemoSchematicsAndLayout_VerifyInputValidationsForInvalidInputs(string Locale)
        {
            //--- PLC Demo Schematics and Layout Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + plcDemoSchematicsAndLayout_form_url;

            //--- Action: Navigate to the given URL ---//
            action.Navigate(driver, form_url);

            //----- Form TestPlan > EN Tab > R13 > T4_2: Submit the PLC Demo Form    with invalid data -----//

            //--- Action: Enter invalid data on the Email Address field. (e.g. 123!@#$%@yahoo.com) ---//
            string email_input = "123!@#$%@yahoo.com";
            action.IDeleteValueOnFields(driver, Elements.Forms_Email_InputBox);
            action.IType(driver, Elements.Forms_Email_InputBox, email_input);
            action.IType(driver, Elements.Forms_Email_InputBox, Keys.Tab);

            //--- Expected Result: The error message below should be displayed: "Please enter a valid email address" ---//
            test.validateStringIsCorrect(driver, Elements.Forms_PlcDemoSchematicsAndLayout_Email_InvalidInput_ErrorMessage, emailId_invalidInput_errorMessage);

            //----- Form TestPlan > EN Tab > R13 > T4_3: Verify PLC Demo Form error messages (AL-2631) -----//

            //--- Action: On State/Province dropdown select value "Outside the US, Mexico, or Canada"  ---//
            action.IClick(driver, Elements.Forms_StateProvince_Dropdown);
            action.IClick(driver, By.CssSelector("select[id$='tate']>option:nth-of-type(2)"));

            //--- Action: Leave the "If Outside the US or Canada, please enter the region" empty ---//
            action.IDeleteValueOnFields(driver, Elements.Forms_IfOutsideTheUsOrCanadaPleaseEnterTheRegion_InputBox);
            action.IType(driver, Elements.Forms_IfOutsideTheUsOrCanadaPleaseEnterTheRegion_InputBox, Keys.Tab);

            //--- Action: Submit the form ---//
            action.IClick(driver, Elements.Forms_Submit_Button);

            //--- Expected Result: The error message should be displayed: "Please provide other state/Province". ---//
            test.validateStringIsCorrect(driver, Elements.Forms_IfOutsideTheUsOrCanadaPleaseEnterTheRegion_BlankInput_ErrorMessage, ifOutsideTheUsOrCanadaPleaseEnterTheRegion_blankInput_errorMessage);
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Input Validations are Working as Expected for Blank Inputs in EN Locale")]
        public void Forms_PlcDemoSchematicsAndLayout_VerifyInputValidationsForBlankInputs(string Locale)
        {
            //--- PLC Demo Schematics and Layout Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + plcDemoSchematicsAndLayout_form_url;

            //--- Action: Navigate to the given URL ---//
            action.Navigate(driver, form_url);

            //----- Form TestPlan > EN Tab > R13 > T4_1: Submit PLC Demo Form   with no data on mandatory fields -----//

            //--- Action: Click on the Submit button without entering data on the mandatory fields. ---//
            action.IClick(driver, Elements.Forms_Submit_Button);

            //----- About you -----//

            //--- Expected Result: The Email: "Email address is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_PlcDemoSchematicsAndLayout_Email_BlankInput_ErrorMessage, email_blankInput_errorMessage);

            //--- Expected Result: The FirstName : "First Name is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_FirstName_BlankInput_ErrorMessage, firstName_blankInput_errorMessage);

            //--- Expected Result: The LastName : "Last Name  is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_LastName_BlankInput_ErrorMessage, lastName_blankInput_errorMessage);

            //--- Expected Result: The Role: "Job Title  is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_PlcDemoSchematicsAndLayout_Role_BlankInput_ErrorMessage, jobTitle_blankInput_errorMessage);

            //----- Your detailed information -----//

            //--- Expected Result: The Organization Name : "Organization name  is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_OrganizationName_BlankInput_ErrorMessage, organizationName_blankInput_errorMessage);

            //--- Expected Result: The Address 1 : "Address Line 1  is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_Address_1_BlankInput_ErrorMessage, address_1_blankInput_errorMessage);

            //--- Expected Result: The City : "City is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_City_BlankInput_ErrorMessage, city_blankInput_errorMessage);

            //--- Expected Result: The State/Province (dropdown): "State / Province  is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_StateProvince_BlankInput_ErrorMessage, stateProvince_blankInput_errorMessage);

            //--- Expected Result: The Zip/Postal Code: "Zip/Postal Code is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_ZipPostalCode_BlankInput_ErrorMessage, zipPostalCode_blankInput_errorMessage);

            //--- Expected Result: The "Country(dropdown): Country  is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_CountryRegion_BlankInput_ErrorMessage, countryRegion_blankInput_errorMessage);

            //--- Expected Result: The "Comments is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_WhyAreYouPrimarilyInterestedInThePlcDemoBoardSchematics_BlankInput_ErrorMessage, whyAreYouPrimarilyInterestedInThePlcDemoBoardSchematics_blankInput_errorMessage);
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Form Submission is Working as Expected in EN Locale")]
        public void Forms_PlcDemoSchematicsAndLayout_VerifyFormSubmission(string Locale)
        {
            if (Util.Configuration.Environment.Equals("dev") || Util.Configuration.Environment.Equals("qa"))
            {
                //--- PLC Demo Schematics and Layout Form URL ---//
                string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + plcDemoSchematicsAndLayout_form_url;

                //--- Action: Navigate to the given URL ---//
                action.Navigate(driver, form_url);

                //----- Form TestPlan > EN Tab > R13 > T4_3: Submit PLC Demo  form with valid data (AL-2496) -----//

                //--- Action: Enter valid data on the mandatory fields. ---//

                //----- About you -----//
                string email_input = "t3st_" + Util.Configuration.Environment.ToLower() + "_plcDemoSchematicsAndLayout_f0rm_" + util.GenerateRandomNumber() + "@mailinator.com";
                //Console.WriteLine("Email Input = " + email_input);
                action.IDeleteValueOnFields(driver, Elements.Forms_Email_InputBox);
                action.IType(driver, Elements.Forms_Email_InputBox, email_input);

                string firstName_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_FirstName_InputBox);
                action.IType(driver, Elements.Forms_FirstName_InputBox, firstName_input);

                string lastName_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_LastName_InputBox);
                action.IType(driver, Elements.Forms_LastName_InputBox, lastName_input);

                string role_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_Role_InputBox);
                action.IType(driver, Elements.Forms_Role_InputBox, role_input);

                //----- Your detailed information -----//
                string organizationName_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_OrganizationName_InputBox);
                action.IType(driver, Elements.Forms_OrganizationName_InputBox, organizationName_input);

                string address_1_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_Address_1_InputBox);
                action.IType(driver, Elements.Forms_Address_1_InputBox, address_1_input);

                string city_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_City_InputBox);
                action.IType(driver, Elements.Forms_City_InputBox, city_input);

                action.IClick(driver, Elements.Forms_StateProvince_Dropdown);
                action.IClick(driver, By.CssSelector("select[id$='tate']>option:nth-of-type(3)"));

                string zipPostalCode_input = "12345";
                action.IDeleteValueOnFields(driver, Elements.Forms_ZipPostalCode_InputBox);
                action.IType(driver, Elements.Forms_ZipPostalCode_InputBox, zipPostalCode_input);

                string countryRegion_input = "US";
                action.ISelectFromDropdown(driver, Elements.Forms_CountryRegion_Dropdown, countryRegion_input);

                string whyAreYouPrimarilyInterestedInThePlcDemoBoardSchematics_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_WhyAreYouPrimarilyInterestedInThePlcDemoBoardSchematics_TextArea);
                action.IType(driver, Elements.Forms_WhyAreYouPrimarilyInterestedInThePlcDemoBoardSchematics_TextArea, whyAreYouPrimarilyInterestedInThePlcDemoBoardSchematics_input);

                //----- Privacy Settings -----//
                action.ICheck(driver, Elements.Forms_Communication_Checkbox);

                //--- Action: Click on submit button  ---//
                action.IClick(driver, Elements.Forms_Submit_Button);

                //--- Expected Result: PLC Demo Schematics and Layout Thank You Page should be displayed ---//
                test.validateStringInstance(driver, driver.Url, plcDemoSchematicsAndLayout_thankYou_page_url);
            }
        }
    }
}