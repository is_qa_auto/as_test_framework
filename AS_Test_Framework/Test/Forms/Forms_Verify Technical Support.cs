﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using OpenQA.Selenium;
using System.Threading;

namespace AS_Test_Framework.Forms
{
    [TestFixture]
    public class Forms_TechnicalSupport : BaseSetUp
    {
        //--- Technical Support Form URL: ---//
        //--- DEV: https://form-dev.corpnt.analog.com/form_pages/support/integrated/techsupport.aspx ---//
        //--- QA: https://form-qa.corpnt.analog.com/form_pages/support/integrated/techsupport.aspx ---//
        //--- PROD: https://form.analog.com/form_pages/support/integrated/techsupport.aspx ---//

        public Forms_TechnicalSupport() : base() { }

        //--- Login Credentials ---//
        string username = "f0rms_t3st_555@mailinator.com";
        string password = "Test_1234";

        //--- URLs ---//
        string technicalSupport_form_url = "form_pages/support/integrated/techsupport.aspx";

        //--- Labels ---//
        string countryRegion_text = "Country/Region";
        string email_blankInput_errorMessage = "Email address is required";
        string firstName_blankInput_errorMessage = "First Name is Required";
        string lastName_blankInput_errorMessage = "Last Name is Required";
        string organizationName_blankInput_errorMessage = "Organization Name is Required";
        string address_blankInput_errorMessage = "Address is Required";
        string telephone_blankInput_errorMessage = "Telephone is Required";
        string city_blankInput_errorMessage = "City is Required";
        string state_blankInput_errorMessage = "State is required";
        string zipCode_blankInput_errorMessage = "Zip/Postal Code is Required";
        string countryRegion_blankInput_errorMessage = "Country/Region is required";
        string whatIsTheEndUseApplicationThesePartsWillBeUsedFor_blankInput_errorMessage = "End Application is Required";
        string whatDesignCycleStageAreYouIn_blankInput_errorMessage = "Design Stage is required";
        string whatIsYourEstimatedAnnualVolume_blankInput_errorMessage = "Estimated Annual Volume is required";
        string selectRequestType_blankInput_errorMessage = "Request Type is required";
        string subject_blankInput_errorMessage = "Please enter the Subject";
        string partNumber_linearProductCategory_blankInput_errorMessage = "Please enter a Part Number or Linear Product Category";
        string typeOfIssue_blankInput_errorMessage = "Please select a Type of Issue";
        string processor_blankInput_errorMessage = "Please select a Processor";
        string hardwarePlatform_blankInput_errorMessage = "Please select a Hardware Platform";
        string emulator_blankInput_errorMessage = "Please select Emulator";
        string developmentSoftware_blankInput_errorMessage = "Please select Software Development Tool";
        string targetSoftware_blankInput_errorMessage = "Please select Target Software";
        string hostOperatingSystem_blankInput_errorMessage = "Please select an Operating System";
        string requestDescription_blankInput_errorMessage = "Please enter the inquiry description";
        string emailId_invalidInput_errorMessage = "Please enter a valid email address";

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Elements are Present and Working as Expected when the User is Logged Out in EN Locale")]
        public void Forms_TechnicalSupport_VerifyElementsWhenUserIsLoggedOut(string Locale)
        {
            //--- Technical Support Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + technicalSupport_form_url;

            //--- Action: Go to Technical Support Page ---//
            action.Navigate(driver, form_url);

            //----- Form TestPlan > EN Tab > R40 > T1: Verify all labels for Country is changed to "Country/Region" (IQ-4970/AL-12466) -----//

            //----- Your Detailed Information -----//

            //--- Expected Result: The Country label has been changed to Country/Region ---//
            test.validateStringInstance(driver, util.GetText(driver, Elements.Forms_TechnicalSupport_Country_Label), countryRegion_text);

            //----- Form TestPlan > EN Tab > R38 > T1: Verify Privacy Settings in All Forms [account is not logged in] (AL-11345) -----//

            //----- Privacy Settings -----//

            //--- Expected Result: Privacy Settings section is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_Section);

            //--- Expected Result: Prompt "You can change your privacy settings at any time by clicking on the unsubscribe link in emails sent from Analog Devices or in Analog’s Privacy Settings." is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_LegalDisclaimer_Text);

            //--- Expected Result: Privacy Settings is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_Link);

            //--- Expected Result: Privacy & Security Statement ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacyAndSecurityStatement_Link);

            //--- Expected Result: Authorized Partners ---//
            test.validateElementIsPresent(driver, Elements.Forms_AuthorizedPartners_Link);

            //----- Form TestPlan > EN Tab > R23 > T4: Login using Save time to login section with valid data -----//

            //----- Save time by logging in or registering for myAnalog -----//

            if (!util.CheckElement(driver, Elements.Forms_Expanded_SaveTimeByLoggingInOrRegisteringForMyAnalog_Panel, 1))
            {
                //--- Action: Click on Save time by logging in or registering for myAnalog   link ---//
                action.IClick(driver, Elements.Forms_SaveTimeByLoggingInOrRegisteringForMyAnalog_Link);
            }

            //--- Expected Result: Save time to login section should be displayed ---//
            test.validateElementIsPresent(driver, Elements.Forms_Expanded_SaveTimeByLoggingInOrRegisteringForMyAnalog_Panel);

            //----- Form TestPlan > EN Tab > R23 > T9: Verify Captcha on Integrated Support form -----//

            //----- Request Details -----//

            //--- Expected Result: Capcha must appear of the form. ---//
            test.validateElementIsPresent(driver, Elements.Forms_Captcha);
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Elements are Present and Working as Expected when the User is Logged In in EN Locale")]
        public void Forms_TechnicalSupport_VerifyElementsWhenUserIsLoggedIn(string Locale)
        {
            //--- Technical Support Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + technicalSupport_form_url;

            //--- Action: Go to Technical Support Page ---//
            action.Navigate(driver, form_url);

            //----- Form TestPlan > EN Tab > R23 > T4: Login using Save time to login section with valid data -----//

            //----- Save time by logging in or registering for myAnalog -----//

            //--- Action: Login using valid data ---//
            action.ILogInViaSaveTimeByLoggingInPanel(driver, username, password);
            Thread.Sleep(3000);

            //----- About you -----//

            //--- Expected Result: The Email field should be  automatically populated ---//
            test.validateFieldIsNotEmpty(driver, Elements.Forms_TechnicalSupport_Email_InputBox);

            //--- Expected Result: The First Name field should be  automatically populated ---//
            test.validateFieldIsNotEmpty(driver, Elements.Forms_FirstName_InputBox);

            //--- Expected Result: The Last Name field should be  automatically populated ---//
            test.validateFieldIsNotEmpty(driver, Elements.Forms_LastName_InputBox);

            //----- Your Detailed Information -----//

            //--- Expected Result: The Organization Name field should be  automatically populated ---//
            test.validateFieldIsNotEmpty(driver, Elements.Forms_OrganizationName_InputBox);

            //--- Expected Result: The State/Province(dropdown) field should be  automatically populated ---//
            test.validateStringChanged(driver, util.GetSelectedDropdownValue(driver, Elements.Forms_StateProvince_Dropdown), "Select State/Province");

            //--- Expected Result: The Zip/Postal Code field should be  automatically populated ---//
            test.validateFieldIsNotEmpty(driver, Elements.Forms_ZipPostalCode_InputBox);

            //--- Expected Result: The Country/Region(dropdown) field should be  automatically populated ---//
            test.validateStringChanged(driver, util.GetSelectedDropdownValue(driver, Elements.Forms_CountryRegion_Dropdown), "Select Country/Region");

            //----- Form TestPlan > EN Tab > R38 > T1: Verify Privacy Settings in All Forms [account is logged in] (AL-11345) -----//

            //----- Privacy Settings -----//

            //--- Expected Result: Prompt "You can change your privacy settings at any time by clicking on the unsubscribe link in emails sent from Analog Devices or in Analog’s Privacy Settings." is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_LegalDisclaimer_Text);

            //--- Expected Result: Privacy Settings is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_Link);

            //--- Expected Result: Privacy & Security Statement ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacyAndSecurityStatement_Link);

            //--- Expected Result: Authorized Partners ---//
            test.validateElementIsPresent(driver, Elements.Forms_AuthorizedPartners_Link);
        }

        [TestCase("en", TestName = "Verify that the Input Validations are Working as Expected for Valid Inputs in EN Locale")]
        public void Forms_TechnicalSupport_VerifyInputValidationsForValidInputs(string Locale)
        {
            //--- Technical Support Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + technicalSupport_form_url;

            //--- Action: Go to Technical Support Page ---//
            action.Navigate(driver, form_url);

            //----- Form TestPlan > EN Tab > R23 > T6_1: Verify Contents of  Amplifiers/Audio&Video/Converters/Linear/Power/RF/Sensors/Etc.  Request Type on Integrated technical support form -----//

            //--- Action: Select Request Type(bullet):  Amplifiers/Audio&Video/Converters/Linear/Power/RF/Sensors/Etc. ---//
            action.IClick(driver, Elements.Forms_Linear_RadioButton);

            //--- Expected Result: Linear  Section should be displayed ---//
            test.validateElementIsPresent(driver, Elements.Forms_Linear_Section);

            //----- Form TestPlan > EN Tab > R23 > T5_1: Verify Contents of DSP Processors & Tools only Request Type on Integrated technical support form -----//

            //----- Request Details -----//

            //--- Action: On Request Details section choose DSP Processors & Tools only radio button ---//
            action.IClick(driver, Elements.Forms_Processors_RadioButton);

            //--- Expected Result: DSP Processors & Tools only Section should be displayed ---//
            test.validateElementIsPresent(driver, Elements.Forms_HighSpeed_Section);

            //--- Expected Result: Type of Issue dropdown should be shown on Processor and DSP section ---//
            test.validateElementIsPresent(driver, Elements.Forms_TypeOfIssue_Dropdown);
        }

        [TestCase("en", TestName = "Verify that the Input Validations are Working as Expected for Invalid Inputs in EN Locale")]
        public void Forms_TechnicalSupport_VerifyInputValidationsForInvalidInputs(string Locale)
        {
            //--- Technical Support Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + technicalSupport_form_url;

            //--- Action: Go to Technical Support Page ---//
            action.Navigate(driver, form_url);

            //----- Form TestPlan > EN Tab > R23 > T4_2: Verify Integrated technical Support   Form      with invalid data -----//

            //--- Action: Enter invalid data on the Email Address field. (e.g. 123!@#$%@yahoo.com) ---//
            string email_input = "123!@#$%@yahoo.com";
            action.IDeleteValueOnFields(driver, Elements.Forms_TechnicalSupport_Email_InputBox);
            action.IType(driver, Elements.Forms_TechnicalSupport_Email_InputBox, email_input);
            action.IType(driver, Elements.Forms_TechnicalSupport_Email_InputBox, Keys.Tab);

            //--- Expected Result: The error message should be displayed: "Please enter a valid email address" ---//
            test.validateStringInstance(driver, util.GetText(driver, Elements.Forms_Email_InvalidInput_ErrorMessage), emailId_invalidInput_errorMessage);
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Input Validations are Working as Expected for Blank Inputs in EN Locale")]
        public void Forms_TechnicalSupport_VerifyInputValidationsForBlankInputs(string Locale)
        {
            //--- Technical Support Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + technicalSupport_form_url;

            //--- Action: Go to Technical Support Page ---//
            action.Navigate(driver, form_url);

            //----- Form TestPlan > EN Tab > R23 > T4_1: Verify Integrated technical Support   Form    with no data on mandatory fields -----//

            //--- Action: Click on the Submit button without entering data on the mandatory fields. ---//
            action.IClick(driver, Elements.Forms_Submit_Button);

            //----- About you -----//

            //--- Expected Result: The Email: "Email address is required" error message should be shown ---//
            test.validateStringInstance(driver, util.GetText(driver, Elements.Forms_TechnicalSupport_Email_BlankInput_ErrorMessage), email_blankInput_errorMessage);

            //--- Expected Result: The First Name: "First Name is Required" error message should be shown ---//
            test.validateStringInstance(driver, util.GetText(driver, Elements.Forms_TechnicalSupport_FirstName_BlankInput_ErrorMessage), firstName_blankInput_errorMessage);

            //--- Expected Result: The Last Name: "Last Name is Required" error message should be shown ---//
            test.validateStringInstance(driver, util.GetText(driver, Elements.Forms_TechnicalSupport_LastName_BlankInput_ErrorMessage), lastName_blankInput_errorMessage);

            //----- Your Detailed Information -----//

            //--- Expected Result: The Organization Name: "Organization Name is Required" error message should be shown ---//
            test.validateStringInstance(driver, util.GetText(driver, Elements.Forms_TechnicalSupport_OrganizationName_BlankInput_ErrorMessage), organizationName_blankInput_errorMessage);

            //--- Expected Result: The Address: "Address is Required" error message should be shown ---//
            test.validateStringInstance(driver, util.GetText(driver, Elements.Forms_TechnicalSupport_Address_BlankInput_ErrorMessage), address_blankInput_errorMessage);

            //--- Expected Result: The Telephone: "Telephone is Required" error message should be shown ---//
            test.validateStringInstance(driver, util.GetText(driver, Elements.Forms_TechnicalSupport_Telephone_BlankInput_ErrorMessage), telephone_blankInput_errorMessage);

            //--- Expected Result: The City "City is Required" error message should be shown ---//
            test.validateStringInstance(driver, util.GetText(driver, Elements.Forms_TechnicalSupport_City_BlankInput_ErrorMessage), city_blankInput_errorMessage);

            //--- Expected Result: The State/Province(dropdown):  "State is required" error message should be shown ---//
            test.validateStringInstance(driver, util.GetText(driver, Elements.Forms_TechnicalSupport_StateProvince_BlankInput_ErrorMessage), state_blankInput_errorMessage);

            //--- Expected Result: The Zip/Postal Code:  "Zip/Postal Code is Required" error message should be shown ---//
            test.validateStringInstance(driver, util.GetText(driver, Elements.Forms_TechnicalSupport_ZipCode_BlankInput_ErrorMessage), zipCode_blankInput_errorMessage);

            //--- Expected Result: The Country/Region(dropdown): "Country is required" error message should be shown ---//
            test.validateStringInstance(driver, util.GetText(driver, Elements.Forms_TechnicalSupport_CountryRegion_BlankInput_ErrorMessage), countryRegion_blankInput_errorMessage);

            //----- Your Project -----//

            //--- Expected Result: The End Application: "End Application is Required" error message should be shown ---//
            test.validateStringInstance(driver, util.GetText(driver, Elements.Forms_TechnicalSupport_WhatIsTheEndUseApplicationThesePartsWillBeUsedFor_BlankInput_ErrorMessage), whatIsTheEndUseApplicationThesePartsWillBeUsedFor_blankInput_errorMessage);

            //--- Expected Result: The Design Stage(dropdown): "Design Stage is required" error message should be shown ---//
            test.validateStringInstance(driver, util.GetText(driver, Elements.Forms_TechnicalSupport_WhatDesignCycleStageAreYouIn_BlankInput_ErrorMessage), whatDesignCycleStageAreYouIn_blankInput_errorMessage);

            //--- Expected Result: The Estimated Annual Volume: "Estimated Annual Volume is required" error message should be shown ---//
            test.validateStringInstance(driver, util.GetText(driver, Elements.Forms_TechnicalSupport_WhatIsYourEstimatedAnnualVolume_BlankInput_ErrorMessage), whatIsYourEstimatedAnnualVolume_blankInput_errorMessage);

            //----- Request Details -----//

            //--- Expected Result: The Select Request Type(bullet): "Request Type is required" error message should be shown ---//
            test.validateStringInstance(driver, util.GetText(driver, Elements.Forms_TechnicalSupport_SelectRequestType_BlankInput_ErrorMessage), selectRequestType_blankInput_errorMessage);

            //----- Form TestPlan > EN Tab > R23 > T6_1: Verify Contents of  Amplifiers/Audio&Video/Converters/Linear/Power/RF/Sensors/Etc.  Request Type on Integrated technical support form -----//
            driver.Navigate().Refresh();

            //----- Request Details -----//

            //--- Action: Select Request Type(bullet):  Amplifiers/Audio&Video/Converters/Linear/Power/RF/Sensors/Etc. ---//
            action.IClick(driver, Elements.Forms_Linear_RadioButton);

            //--- Expected Result: Click on submit button ---//
            action.IClick(driver, Elements.Forms_Submit_Button);

            //--- Expected Result: Subject:   "Please enter the Subject" ---//
            test.validateStringInstance(driver, util.GetText(driver, Elements.Forms_TechnicalSupport_Linear_Subject_BlankInput_ErrorMessage), subject_blankInput_errorMessage);

            //--- Expected Result: PartNumbers:  or Linear Product Category(dropdown):  "Please enter a Part Number or Linear Product Category" ---//
            test.validateStringInstance(driver, util.GetText(driver, Elements.Forms_TechnicalSupport_PartNumber_LinearProductCategory_BlankInput_ErrorMessage), partNumber_linearProductCategory_blankInput_errorMessage);

            //----- Form TestPlan > EN Tab > R23 > T5_2: Verify the error messages of mandatory fields on Request Details section  of Integrated technical support form  (Select Request Type: Processor and DSP) -----//
            driver.Navigate().Refresh();

            //----- Request Details -----//

            //--- Action: On Request Details section choose DSP Processors & Tools only radio button ---//
            action.IClick(driver, Elements.Forms_Processors_RadioButton);

            //--- Action: Click on submit button without any value on Request Detail section  (Select Request Type: Processor and DSP) ---//
            action.IClick(driver, Elements.Forms_Submit_Button);

            //--- Expected Result: Type Of Issue(DROPDOWN): "Please select a Type of Issue" Error message on Request Detail section should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_TechnicalSupport_TypeOfIssue_BlankInput_ErrorMessage, typeOfIssue_blankInput_errorMessage);

            //--- Expected Result: Processor(dropdown): "Please select a Processor" Error message on Request Detail section should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_TechnicalSupport_Processor_BlankInput_ErrorMessage, processor_blankInput_errorMessage);

            //--- Expected Result: Hardware Platform(dropdown): "Please select a Hardware Platform" Error message on Request Detail section should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_TechnicalSupport_HardwarePlatform_BlankInput_ErrorMessage, hardwarePlatform_blankInput_errorMessage);

            //--- Expected Result: Subject:  "Please enter the Subject" Error message on Request Detail section should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_TechnicalSupport_Processors_Subject_BlankInput_ErrorMessage, subject_blankInput_errorMessage);

            //--- Expected Result: Emulator(dropdown): "Please select Emulator" Error message on Request Detail section should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_TechnicalSupport_Emulator_BlankInput_ErrorMessage, emulator_blankInput_errorMessage);

            //--- Expected Result: Development Software:(PopUp): "Please select Software Development Too" Error message on Request Detail section should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_TechnicalSupport_DevelopmentSoftware_BlankInput_ErrorMessage, developmentSoftware_blankInput_errorMessage);

            //--- Expected Result: Target Software(PopUp): "Please select Target Software" Error message on Request Detail section should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_TechnicalSupport_TargetSoftware_BlankInput_ErrorMessage, targetSoftware_blankInput_errorMessage);

            //--- Expected Result: Host Operating System(dropdown): "Please select an Operating System" Error message on Request Detail section should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_TechnicalSupport_HostOperatingSystem_BlankInput_ErrorMessage, hostOperatingSystem_blankInput_errorMessage);

            //--- Expected Result: Request Description:  "Please enter the inquiry description" Error message on Request Detail section should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_TechnicalSupport_RequestDescription_BlankInput_ErrorMessage, requestDescription_blankInput_errorMessage);
        }
    }
}