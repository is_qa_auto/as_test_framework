﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System;
using System.Threading;

namespace AS_Test_Framework.Forms
{
    [TestFixture]
    public class Forms_AdiSimSrdDesignStudio : BaseSetUp
    {
        //--- ADIsimSRD Design Studio Form URL: ---//
        //--- DEV: https://form-dev.corpnt.analog.com/Form_Pages/RFComms/SRDDesignstudio.aspx ---//
        //--- QA: https://form-qa.corpnt.analog.com/Form_Pages/RFComms/SRDDesignstudio.aspx ---//
        //--- PROD: https://form.analog.com/Form_Pages/RFComms/SRDDesignstudio.aspx ---//

        public Forms_AdiSimSrdDesignStudio() : base() { }

        //--- URLs ---//
        string adiSimSrdDesignStudi_form_url = "Form_Pages/RFComms/SRDDesignstudio.aspx";
        string registration_page_url = "/app/registration";
        string adiSimSrdSoftwareRequest_page_url = "/adisimsrd-design-studio-thankyou.html";

        //--- Labels ---//
        string analogDevices_text = "Analog Devices";
        string countryRegion_text = "Country/Region";

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Elements are Present and Working as Expected when the User is Logged Out in EN Locale")]
        public void Forms_AdiSimSrdDesignStudio_VerifyElementsWhenUserIsLoggedOut(string Locale)
        {
            //--- ADIsimSRD Design Studio Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + adiSimSrdDesignStudi_form_url;

            //--- Action: Navigate to the given URL ---//
            action.Navigate(driver, form_url);

            //----- Form TestPlan > EN Tab > R31 > T1: Verify Page Title Section -----//

            //--- Expected Result: Title is displayed in the browser / tab ---//
            test.validateWindowTitleIsCorrect(driver, analogDevices_text);

            //----- ADIsimSRD Design Studio -----//

            //--- Expected Result: Title is present nad properly formatted above the form ---//
            test.validateElementIsPresent(driver, Elements.Forms_AdiSimSrdDesignStudio_Title_Label);

            //----- Form TestPlan > EN Tab > R31 > T2: Verify About You Section -----//

            //----- About you -----//

            //--- Action: The First Name field is present.---//
            test.validateElementIsPresent(driver, Elements.Forms_FirstName_InputBox);

            //--- Action: The Last Name fields is present.---//
            test.validateElementIsPresent(driver, Elements.Forms_LastName_InputBox);

            //----- Form TestPlan > EN Tab > R31 > T3: Verify Your Detailed Information Section -----//

            //----- Your detailed information -----//

            //--- Action: The Organization name field is present.---//
            test.validateElementIsPresent(driver, Elements.Forms_OrganizationName_InputBox);

            //--- Action: The City field is present.---//
            test.validateElementIsPresent(driver, Elements.Forms_City_InputBox);

            //----- Form TestPlan > EN Tab > R40 > T1: Verify all labels for Country is changed to "Country/Region" (IQ-4970/AL-12466) -----//

            //--- Expected Result: The Country label has been changed to Country/Region ---//
            test.validateStringInstance(driver, util.GetText(driver, Elements.Forms_Country_Label), countryRegion_text);

            //----- Form TestPlan > EN Tab > R31 > T3: Verify Your Detailed Information Section -----//

            //--- Action: The Country field is present.---//
            test.validateElementIsPresent(driver, Elements.Forms_CountryRegion_Dropdown);

            //--- Action: The Telephone field is present.---//
            test.validateElementIsPresent(driver, Elements.Forms_Telephone_InputBox);

            //--- Action: The Email field is present.---//
            test.validateElementIsPresent(driver, Elements.Forms_Email_InputBox);

            //----- Form TestPlan > EN Tab > R31 > T4: Verify Area of interest section -----//

            //----- Area of interest -----//

            //--- Expected Result: Question and text box is present ---//
            test.validateElementIsPresent(driver, Elements.Forms_WhatIsYourAreaOfInterestForThisApplication_TextArea);

            //----- Form TestPlan > EN Tab > R38 > T1: Verify Privacy Settings in All Forms [account is not logged in] (AL-11345) -----//

            //----- Privacy Settings -----//

            //--- Expected Result: Privacy Settings section is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_Section);

            //--- Expected Result: Prompt "You can change your privacy settings at any time by clicking on the unsubscribe link in emails sent from Analog Devices or in Analog’s Privacy Settings." is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_LegalDisclaimer_Text);

            //--- Expected Result: Privacy Settings is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_Link);

            //--- Expected Result: Privacy & Security Statement ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacyAndSecurityStatement_Link);

            //--- Expected Result: Authorized Partners ---//
            test.validateElementIsPresent(driver, Elements.Forms_AuthorizedPartners_Link);

            //----- Form TestPlan > EN Tab > R31 > T5: Verify Save Time To Login -----//

            //----- Save time by logging in or registering for myAnalog -----//

            if (!util.CheckElement(driver, Elements.Forms_Expanded_SaveTimeByLoggingInOrRegisteringForMyAnalog_Panel, 1))
            {
                //--- Action: Click the "Save time by logging in or registering for myAnalog" text in the Save Time To Login bar to expand or collapse ---//
                action.IClick(driver, Elements.Forms_SaveTimeByLoggingInOrRegisteringForMyAnalog_Link);
            }

            //--- Expected Result: Save time to login will Expand / Collpase ---//
            test.validateElementIsPresent(driver, Elements.Forms_Expanded_SaveTimeByLoggingInOrRegisteringForMyAnalog_Panel);

            if (util.CheckElement(driver, Elements.Forms_Expanded_SaveTimeByLoggingInOrRegisteringForMyAnalog_Panel_RegisterNow_Link, 1))
            {
                //--- Action: Click "Register now." Link ---//
                action.IOpenLinkInNewTab(driver, Elements.Forms_Expanded_SaveTimeByLoggingInOrRegisteringForMyAnalog_Panel_RegisterNow_Link);
                Thread.Sleep(1000);

                //--- Expected Result:Redirects to "Register now." Page ---//
                test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + registration_page_url);
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.Forms_Expanded_SaveTimeByLoggingInOrRegisteringForMyAnalog_Panel_RegisterNow_Link);
            }
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Form Submission is Working as Expected in EN Locale")]
        public void Forms_AdiSimSrdDesignStudio_VerifyFormSubmission(string Locale)
        {
            if (Util.Configuration.Environment.Equals("dev") || Util.Configuration.Environment.Equals("qa"))
            {
                //--- ADIsimSRD Design Studio Form URL ---//
                string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + adiSimSrdDesignStudi_form_url;

                //--- Action: Navigate to the given URL ---//
                action.Navigate(driver, form_url);

                //----- Form TestPlan > EN Tab > R31 > T14: Verify Thank You page url of ADIsimSRD Design Studio™ Request for Software -----//

                //--- Action: Complete the form ---//

                //----- About you -----//
                string firstName_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_FirstName_InputBox);
                action.IType(driver, Elements.Forms_FirstName_InputBox, firstName_input);

                string lastName_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_LastName_InputBox);
                action.IType(driver, Elements.Forms_LastName_InputBox, lastName_input);

                //----- Your detailed information -----//
                string organizationName_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_OrganizationName_InputBox);
                action.IType(driver, Elements.Forms_OrganizationName_InputBox, organizationName_input);

                string countryRegion_input = "US";
                action.ISelectFromDropdown(driver, Elements.Forms_CountryRegion_Dropdown, countryRegion_input);

                string city_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_City_InputBox);
                action.IType(driver, Elements.Forms_City_InputBox, city_input);

                string telephone_input = "123456789";
                action.IDeleteValueOnFields(driver, Elements.Forms_Telephone_InputBox);
                action.IType(driver, Elements.Forms_Telephone_InputBox, telephone_input);

                string email_input = "t3st_" + Util.Configuration.Environment.ToLower() + "_adiSimSrdDesignStudio_f0rm_" + util.GenerateRandomNumber() + "@mailinator.com";
                //Console.WriteLine("Email Input = " + email_input);
                action.IDeleteValueOnFields(driver, Elements.Forms_Email_InputBox);
                action.IType(driver, Elements.Forms_Email_InputBox, email_input);

                //----- Area of interest -----//
                string whatIsYourAreaOfInterestForThisApplication_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_WhatIsYourAreaOfInterestForThisApplication_TextArea);
                action.IType(driver, Elements.Forms_WhatIsYourAreaOfInterestForThisApplication_TextArea, whatIsYourAreaOfInterestForThisApplication_input);

                //----- Privacy Settings -----//
                action.ICheck(driver, Elements.Forms_Communication_Checkbox);

                //--- Action: submit ---//
                action.IClick(driver, Elements.Forms_Submit_Button);

                //--- Expected Result: thanks you page should be displayed. ---//
                test.validateStringInstance(driver, driver.Url, adiSimSrdSoftwareRequest_page_url);
            }
        }
    }
}