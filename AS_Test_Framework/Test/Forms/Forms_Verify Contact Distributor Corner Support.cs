﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using OpenQA.Selenium;
using System;
using System.Threading;

namespace AS_Test_Framework.Forms
{
    [TestFixture]
    public class Forms_ContactDistributorCornerSupport : BaseSetUp
    {
        //--- Contact Distributor Corner Support Form URL: ---//
        //--- DEV: https://form-dev.corpnt.analog.com/form_pages/support/distributor/customerservice.aspx ---//
        //--- QA: https://form-qa.corpnt.analog.com/form_pages/support/distributor/customerservice.aspx ---//
        //--- PROD: https://form.analog.com/form_pages/support/distributor/customerservice.aspx ---//

        public Forms_ContactDistributorCornerSupport() : base() { }

        //--- Login Credentials ---//
        string username = "f0rms_t3st_555@mailinator.com";
        string password = "Test_1234";

        //--- URLs ---//
        string contactDistributorCornerSupport_form_url = "form_pages/support/distributor/customerservice.aspx";
        string distributorCornerThankYou_page_url = "/dccustomerservice_thankyou.html";

        //--- Labels ---//
        string countryRegion_text = "Country/Region";
        string email_blankInput_errorMessage = "Email address is required";
        string name_blankInput_errorMessage = "Name is required";
        string organizationName_blankInput_errorMessage = "Organization name is required";
        string telephone_blankInput_errorMessage = "Telephone is required";
        string countryRegion_blankInput_errorMessage = "Country/Region is required";
        string whatTypeOfSupportDoYouNeed_blankInput_errorMessage = "Please select one";
        string email_invalidInput_errorMessage = "Please enter a valid email address";

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Elements are Present and Working as Expected when the User is Logged Out in EN Locale")]
        public void Forms_ContactDistributorCornerSupport_VerifyElementsWhenUserIsLoggedOut(string Locale)
        {
            //--- Contact Distributor Corner Support Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + contactDistributorCornerSupport_form_url;

            //--- Action: Navigate on the given URL ---//
            action.Navigate(driver, form_url);

            //----- Form TestPlan > EN Tab > R40 > T1: Verify all labels for Country is changed to "Country/Region" (IQ-4970/AL-12466) -----//

            //----- Your detailed contact information -----//

            //--- Expected Result: The Country label has been changed to Country/Region ---//
            test.validateStringInstance(driver, util.GetText(driver, Elements.Forms_Country_Label), countryRegion_text);

            //----- Form TestPlan > EN Tab > R38 > T1: Verify Privacy Settings in All Forms [account is not logged in] (AL-11345) -----//

            //----- Privacy Settings -----//

            //--- Expected Result: Privacy Settings section is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_Section);

            //--- Expected Result: Prompt "You can change your privacy settings at any time by clicking on the unsubscribe link in emails sent from Analog Devices or in Analog’s Privacy Settings." is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_LegalDisclaimer_Text);

            //--- Expected Result: Privacy Settings is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_Link);

            //--- Expected Result: Privacy & Security Statement ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacyAndSecurityStatement_Link);

            //--- Expected Result: Authorized Partners ---//
            test.validateElementIsPresent(driver, Elements.Forms_AuthorizedPartners_Link);

            //----- Form TestPlan > EN Tab > R25 > T3: Login using Save time to login section with valid data -----//

            //----- Save time by logging in or registering for myAnalog -----//

            if (!util.CheckElement(driver, Elements.Forms_Expanded_SaveTimeByLoggingInOrRegisteringForMyAnalog_Panel, 1))
            {
                //--- Action: Click on Save time by logging in or registering for myAnalog   link ---//
                action.IClick(driver, Elements.Forms_SaveTimeByLoggingInOrRegisteringForMyAnalog_Link);
            }

            //--- Expected Result: Save time to login section should be displayed ---//
            test.validateElementIsPresent(driver, Elements.Forms_Expanded_SaveTimeByLoggingInOrRegisteringForMyAnalog_Panel);
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Elements are Present and Working as Expected when the User is Logged In in EN Locale")]
        public void Forms_ContactDistributorCornerSupport_VerifyElementsWhenUserIsLoggedIn(string Locale)
        {
            //--- Contact Distributor Corner Support Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + contactDistributorCornerSupport_form_url;

            //--- Action: Navigate on the given URL ---//
            action.Navigate(driver, form_url);

            //----- Form TestPlan > EN Tab > R25 > T3: Login using Save time to login section with valid data -----//

            //----- Save time by logging in or registering for myAnalog -----//

            //--- Action: Login using valid data ---//
            action.ILogInViaSaveTimeByLoggingInPanel(driver, username, password);
            Thread.Sleep(1000);

            //----- About you -----//

            //--- Expected Result: The Email field should be  automatically populated ---//
            test.validateFieldIsNotEmpty(driver, Elements.Forms_Email_InputBox);

            //--- Expected Result: The Name field should be  automatically populated ---//
            test.validateFieldIsNotEmpty(driver, Elements.Forms_Name_InputBox);

            //----- Your detailed contact information -----//

            //--- Expected Result: The Organization Name field should be  automatically populated ---//
            test.validateFieldIsNotEmpty(driver, Elements.Forms_OrganizationName_InputBox);

            //--- Expected Result: The Country (dropdown) field should be  automatically populated ---//
            test.validateStringChanged(driver, util.GetSelectedDropdownValue(driver, Elements.Forms_CountryRegion_Dropdown), "Select Country/Region");

            //----- Form TestPlan > EN Tab > R38 > T1: Verify Privacy Settings in All Forms [account is logged in] (AL-11345) -----//

            //----- Privacy Settings -----//

            //--- Expected Result: Prompt "You can change your privacy settings at any time by clicking on the unsubscribe link in emails sent from Analog Devices or in Analog’s Privacy Settings." is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_LegalDisclaimer_Text);

            //--- Expected Result: Privacy Settings is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_Link);

            //--- Expected Result: Privacy & Security Statement ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacyAndSecurityStatement_Link);

            //--- Expected Result: Authorized Partners ---//
            test.validateElementIsPresent(driver, Elements.Forms_AuthorizedPartners_Link);
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Input Validations are Working as Expected for Invalid Inputs in EN Locale")]
        public void Forms_ContactDistributorCornerSupport_VerifyInputValidationsForInvalidInputs(string Locale)
        {
            //--- Contact Distributor Corner Support Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + contactDistributorCornerSupport_form_url;

            //--- Action: Navigate on the given URL ---//
            action.Navigate(driver, form_url);

            //----- Form TestPlan > EN Tab > R25 > T4_2: Submit the Contact Distributor Portal Form    with invalid data -----//

            //--- Action: Enter invalid data on the Email Address field. (e.g. 123!@#$%@yahoo.com) ---//
            string email_input = "123!@#$%@yahoo.com";
            action.IDeleteValueOnFields(driver, Elements.Forms_Email_InputBox);
            action.IType(driver, Elements.Forms_Email_InputBox, email_input);
            action.IType(driver, Elements.Forms_Email_InputBox, Keys.Tab);

            //--- Expected Result: The error message should be displayed: "Please enter a valid email address" ---//
            test.validateStringIsCorrect(driver, Elements.Forms_ContactDistributorCornerSupport_Email_InvalidInput_ErrorMessage, email_invalidInput_errorMessage);
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Input Validations are Working as Expected for Blank Inputs in EN Locale")]
        public void Forms_ContactDistributorCornerSupport_VerifyInputValidationsForBlankInputs(string Locale)
        {
            //--- Contact Distributor Corner Support Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + contactDistributorCornerSupport_form_url;

            //--- Action: Navigate on the given URL ---//
            action.Navigate(driver, form_url);

            //----- Form TestPlan > EN Tab > R25 > T4_1: Submit Contact Distributor Portal Form with no data on mandatory fields -----//

            //--- Action: Click on the Submit button without entering data on the mandatory fields. ---//
            action.IClick(driver, Elements.Forms_Submit_Button);

            //----- About you -----//

            //--- Expected Result: The Email: "Email address is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_ContactDistributorCornerSupport_Email_BlankInput_ErrorMessage, email_blankInput_errorMessage);

            //--- Expected Result: The Name: "Name is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_ContactDistributorCornerSupport_Name_BlankInput_ErrorMessage, name_blankInput_errorMessage);

            //----- Your detailed contact information -----//

            //--- Expected Result: The Your detailed contact information Organization Name: "Organization name is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_ContactDistributorCornerSupport_OrganizationName_BlankInput_ErrorMessage, organizationName_blankInput_errorMessage);

            //--- Expected Result: The Telephone: "Telephone is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_ContactDistributorCornerSupport_Telephone_BlankInput_ErrorMessage, telephone_blankInput_errorMessage);

            //--- Expected Result: The Country (dropdown): "Country is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_CountryRegion_BlankInput_ErrorMessage, countryRegion_blankInput_errorMessage);

            //----- Your preferences -----//

            //--- Expected Result: The What type of support do you need?: "Please select one" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_ContactDistributorCornerSupport_WhatTypeOfSupportDoYouNeed_BlankInput_ErrorMessage, whatTypeOfSupportDoYouNeed_blankInput_errorMessage);
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Form Submission is Working as Expected in EN Locale")]
        public void Forms_ContactDistributorCornerSupport_VerifyFormSubmission(string Locale)
        {
            if (Util.Configuration.Environment.Equals("dev") || Util.Configuration.Environment.Equals("qa"))
            {
                //--- Contact Distributor Corner Support Form URL ---//
                string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + contactDistributorCornerSupport_form_url;

                //--- Action: Navigate on the given URL ---//
                action.Navigate(driver, form_url);

                //----- Form TestPlan > EN Tab > R25 > T4_3: Submit Contact Distributor Portal Form with valid data (AL-2496) -----//

                //--- Action: Enter valid data on the mandatory fields. ---//

                //----- About you -----//
                string email_input = "t3st_" + Util.Configuration.Environment.ToLower() + "_contactDistributorCornerSupport_f0rm_" + util.GenerateRandomNumber() + "@mailinator.com";
                //Console.WriteLine("Email Input = " + email_input);
                action.IDeleteValueOnFields(driver, Elements.Forms_Email_InputBox);
                action.IType(driver, Elements.Forms_Email_InputBox, email_input);

                string name_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_Name_InputBox);
                action.IType(driver, Elements.Forms_Name_InputBox, name_input);

                //----- Your detailed contact information -----//
                string organizationName_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_OrganizationName_InputBox);
                action.IType(driver, Elements.Forms_OrganizationName_InputBox, organizationName_input);

                string telephone_input = "123456789";
                action.IDeleteValueOnFields(driver, Elements.Forms_Telephone_InputBox);
                action.IType(driver, Elements.Forms_Telephone_InputBox, telephone_input);

                string countryRegion_input = "US";
                action.ISelectFromDropdown(driver, Elements.Forms_CountryRegion_Dropdown, countryRegion_input);

                //----- Your preferences -----//
                action.IClick(driver, Elements.Forms_WhatTypeOfSupportDoYouNeed_RadioButtons);

                //----- Privacy Settings -----//
                action.ICheck(driver, Elements.Forms_Communication_Checkbox);

                //--- Action: Click on submit button ---//
                action.IClick(driver, Elements.Forms_Submit_Button);

                //--- Expected Result: Distributor Corner: Thank You page should be displayed ---//
                test.validateStringInstance(driver, driver.Url, distributorCornerThankYou_page_url);

                if (driver.Url.Contains(distributorCornerThankYou_page_url))
                {
                    action.ICheckMailinatorEmail(driver, email_input);

                    //--- Expected Result: The email from DistributorCornerAdmin@analog.com should be displayed ---//
                    test.validateStringIsCorrect(driver, Elements.Mailinator_LatestEmail_Sender, "DistributorCornerAdmin@analog.com");
                }
            }
        }
    }
}