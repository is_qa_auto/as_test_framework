﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using OpenQA.Selenium;
using System;
using System.Threading;

namespace AS_Test_Framework.Forms
{
    [TestFixture]
    public class Forms_AdiSimRfRequestForSoftware : BaseSetUp
    {
        //--- ADIsimRF REQUEST FOR SOFTWARE Form URL: ---//
        //--- DEV: https://form-dev.corpnt.analog.com/Form_Pages/RFComms/ADISimRF.aspx ---//
        //--- QA: https://form-qa.corpnt.analog.com/Form_Pages/RFComms/ADISimRF.aspx ---//
        //--- PROD: https://form.analog.com/Form_Pages/RFComms/ADISimRF.aspx ---//

        public Forms_AdiSimRfRequestForSoftware() : base() { }

        //--- Login Credentials ---//
        string username = "f0rms_t3st_555@mailinator.com";
        string password = "Test_1234";

        //--- URLs ---//
        string adiSimRfRequestForSoftware_form_url = "Form_Pages/RFComms/ADISimRF.aspx";
        string adiSimRf_page_url = "http://adisimrf.download.analog.com/ADISimRF/publish.htm";

        //--- Labels ---//
        string countryRegion_text = "Country/Region";
        string firstName_blankInput_errorMessage = "First Name is required";
        string lastName_blankInput_errorMessage = "Last name is required";
        string organizationName_blankInput_errorMessage = "Organization name is required";
        string city_blankInput_errorMessage = "City is required";
        string zipPostalCode_blankInput_errorMessage = "Zip/Postal Code is required";
        string countryRegion_blankInput_errorMessage = "Country/Region is required";
        string email_blankInput_errorMessage = "Email address is required";
        string email_invalidInput_errorMessage = "Please enter a valid email address";

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Elements are Present and Working as Expected when the User is Logged Out in EN Locale")]
        public void Forms_AdiSimRfRequestForSoftware_VerifyElementsWhenUserIsLoggedOut(string Locale)
        {
            //--- ADIsimRF REQUEST FOR SOFTWARE Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + adiSimRfRequestForSoftware_form_url;

            //--- Action: Navigate to the given URL ---//
            action.Navigate(driver, form_url);

            //----- Your detailed information -----//

            //----- Form TestPlan > EN Tab > R40 > T1: Verify all labels for Country is changed to "Country/Region" (IQ-4970/AL-12466) -----//

            //--- Expected Result: The Country label has been changed to Country/Region ---//
            test.validateStringInstance(driver, util.GetText(driver, Elements.Forms_Country_Label), countryRegion_text);

           //----- Privacy Settings -----//

            //----- Form TestPlan > EN Tab > R38 > T1: Verify Privacy Settings in All Forms [account is not logged in] (AL-11345) -----//

            //--- Expected Result: Privacy Settings section is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_Section);

            //--- Expected Result: Prompt "You can change your privacy settings at any time by clicking on the unsubscribe link in emails sent from Analog Devices or in Analog’s Privacy Settings." is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_LegalDisclaimer_Text);

            //--- Expected Result: Privacy Settings is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_Link);
            //--- Expected Result: Privacy & Security Statement ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacyAndSecurityStatement_Link);

            //--- Expected Result: Authorized Partners ---//
            test.validateElementIsPresent(driver, Elements.Forms_AuthorizedPartners_Link);

            //----- Save time by logging in or registering for myAnalog -----//

            //----- Form TestPlan > EN Tab > R9 > T5_1: Login using Save time to login section with valid data -----//

            if (!util.CheckElement(driver, Elements.Forms_Expanded_SaveTimeByLoggingInOrRegisteringForMyAnalog_Panel, 1))
            {
                //--- Action: Click on Save time by logging in or registering for myAnalog link ---//
                action.IClick(driver, Elements.Forms_SaveTimeByLoggingInOrRegisteringForMyAnalog_Link);
            }

            //--- Expected Result: Save time to login section should be displayed (AL-7439) ---//
            test.validateElementIsPresent(driver, Elements.Forms_Expanded_SaveTimeByLoggingInOrRegisteringForMyAnalog_Panel);
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Elements are Present and Working as Expected when the User is Logged In in EN Locale")]
        public void Forms_AdiSimRfRequestForSoftware_VerifyElementsWhenUserIsLoggedIn(string Locale)
        {
            //--- ADIsimRF REQUEST FOR SOFTWARE Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + adiSimRfRequestForSoftware_form_url;
            //--- Action: Navigate to the given URL ---//
            action.Navigate(driver, form_url);

            //----- Form TestPlan > EN Tab > R9 > T5_1: Login using Save time to login section with valid data -----//

            //--- Action: Login using valid data ---//
            action.ILogInViaSaveTimeByLoggingInPanel(driver, username, password);
            Thread.Sleep(2000);

            //----- About you -----//

            //--- Expected Result: The FirstName field should be  automatically populated ---//
            test.validateFieldIsNotEmpty(driver, Elements.Forms_FirstName_InputBox);
            //--- Expected Result: The Last Name field should be  automatically populated ---//
            test.validateFieldIsNotEmpty(driver, Elements.Forms_LastName_InputBox);

            //----- Your detailed information -----//

            //--- Expected Result: The Organization Name field should be  automatically populated ---//
            test.validateFieldIsNotEmpty(driver, Elements.Forms_OrganizationName_InputBox);

            //--- Expected Result: The State/Province(dropdown) field should be  automatically populated ---//
            test.validateStringChanged(driver, util.GetSelectedDropdownValue(driver, Elements.Forms_StateProvince_Dropdown), "Select State/Province");

            //--- Expected Result: The Zip/Postal Code field should be  automatically populated ---//
            test.validateFieldIsNotEmpty(driver, Elements.Forms_ZipPostalCode_InputBox);

            //--- Expected Result: The Country(dropdown) field should be  automatically populated ---//
            test.validateStringChanged(driver, util.GetSelectedDropdownValue(driver, Elements.Forms_CountryRegion_Dropdown), "Select Country/Region");

            //--- Expected Result: The Email field should be  automatically populated ---//
            test.validateFieldIsNotEmpty(driver, Elements.Forms_Email_InputBox);

            //----- Form TestPlan > EN Tab > R38 > T1: Verify Privacy Settings in All Forms [account is logged in] (AL-11345) -----//

            //----- Privacy Settings -----//

            //--- Expected Result: Prompt "You can change your privacy settings at any time by clicking on the unsubscribe link in emails sent from Analog Devices or in Analog’s Privacy Settings." is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_LegalDisclaimer_Text);

            //--- Expected Result: Privacy Settings is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_Link);

            //--- Expected Result: Privacy & Security Statement ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacyAndSecurityStatement_Link);

            //--- Expected Result: Authorized Partners ---//
            test.validateElementIsPresent(driver, Elements.Forms_AuthorizedPartners_Link);
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Input Validations are Working as Expected for Invalid Inputs in EN Locale")]
        public void Forms_AdiSimRfRequestForSoftware_VerifyInputValidationsForInvalidInputs(string Locale)
        {
            //--- ADIsimRF REQUEST FOR SOFTWARE Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + adiSimRfRequestForSoftware_form_url;

            //--- Action: Navigate to the given URL ---//
            action.Navigate(driver, form_url);

            //----- Form TestPlan > EN Tab > R9 > T6_2: Submit the ADIsimRF Form with invalid data -----//

            //--- Action: Enter invalid data on the Email Address field. (e.g. 123!@#$%@yahoo.com) ---//
            string email_input = "123!@#$%@yahoo.com";
            action.IDeleteValueOnFields(driver, Elements.Forms_Email_InputBox);
            action.IType(driver, Elements.Forms_Email_InputBox, email_input);
            action.IType(driver, Elements.Forms_Email_InputBox, Keys.Tab);

            //--- Expected Result: The error message below should be displayed: "Please enter a valid email address" ---//
            test.validateStringIsCorrect(driver, Elements.Forms_AdiSimRfRequestForSoftware_Email_InvalidInput_ErrorMessage, email_invalidInput_errorMessage);
      }

       [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Input Validations are Working as Expected for Blank Inputs in EN Locale")]
        public void Forms_AdiSimRfRequestForSoftware_VerifyInputValidationsForBlankInputs(string Locale)
        {
           //--- ADIsimRF REQUEST FOR SOFTWARE Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + adiSimRfRequestForSoftware_form_url;

            //--- Action: Navigate to the given URL ---//
            action.Navigate(driver, form_url);

           //----- Form TestPlan > EN Tab > R9 > T6_1: Submit ADIsimRF Form   with no data on mandatory fields  -----//

            //--- Action: Click on the Submit button without entering data on the mandatory fields. ---//
            action.IClick(driver, Elements.Forms_Submit_Button);

            //----- About you -----//

            //--- Expected Result: The FirstName:   "First Name is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_AdiSimRfRequestForSoftware_FirstName_BlankInput_ErrorMessage, firstName_blankInput_errorMessage);

           //--- Expected Result: The LastName:  "LastName is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_AdiSimRfRequestForSoftware_LastName_BlankInput_ErrorMessage, lastName_blankInput_errorMessage);

            //----- Your detailed information -----//

            //--- Expected Result: The Organization Name:  "Organization Name is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_AdiSimRfRequestForSoftware_OrganizationName_BlankInput_ErrorMessage, organizationName_blankInput_errorMessage);

          //--- Expected Result: The City: "City is required" error message should be show ---//
            test.validateStringIsCorrect(driver, Elements.Forms_AdiSimRfRequestForSoftware_City_BlankInput_ErrorMessage, city_blankInput_errorMessage);

           //--- Expected Result: The Zip/Postal Code: "Zip/Postal Code is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_ZipPostalCode_BlankInput_ErrorMessage, zipPostalCode_blankInput_errorMessage);

           //--- Expected Result: The Country(dropdown): "Country is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_CountryRegion_BlankInput_ErrorMessage, countryRegion_blankInput_errorMessage);

            //--- Expected Result: The Email: "Emailaddress is required" error message should be shown ---//
           test.validateStringIsCorrect(driver, Elements.Forms_AdiSimRfRequestForSoftware_Email_BlankInput_ErrorMessage, email_blankInput_errorMessage);
        }
        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Form Submission is Working as Expected in EN Locale")]
        public void Forms_AdiSimRfRequestForSoftware_VerifyFormSubmission(string Locale)
        {
           if (Util.Configuration.Environment.Equals("dev") || Util.Configuration.Environment.Equals("qa"))
            {
                //--- ADIsimRF REQUEST FOR SOFTWARE Form URL ---//
                string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + adiSimRfRequestForSoftware_form_url;

                //--- Action: Navigate to the given URL ---//
                action.Navigate(driver, form_url);

                //----- Form TestPlan > EN Tab > R9 > T6_3: Submit ADIsimRF  form with valid data -----//

                //--- Action: Enter valid data on the mandatory fields. ---//

                //----- About you -----//
                string firstName_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_FirstName_InputBox);
             action.IType(driver, Elements.Forms_FirstName_InputBox, firstName_input);

                string lastName_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_LastName_InputBox);
                action.IType(driver, Elements.Forms_LastName_InputBox, lastName_input);

                //----- Your detailed information -----//
                string organizationName_input = "This is Just A Test";
               action.IDeleteValueOnFields(driver, Elements.Forms_OrganizationName_InputBox);
                action.IType(driver, Elements.Forms_OrganizationName_InputBox, organizationName_input);

               string city_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_City_InputBox);
                action.IType(driver, Elements.Forms_City_InputBox, city_input);

                action.IClick(driver, Elements.Forms_StateProvince_Dropdown);
                action.IClick(driver, By.CssSelector("select[id$='tate']>option:nth-of-type(3)"));
                string zipPostalCode_input = "12345";
                action.IDeleteValueOnFields(driver, Elements.Forms_ZipPostalCode_InputBox);
                action.IType(driver, Elements.Forms_ZipPostalCode_InputBox, zipPostalCode_input);

                string countryRegion_input = "US";
                action.ISelectFromDropdown(driver, Elements.Forms_CountryRegion_Dropdown, countryRegion_input);

                string telephone_input = "123456789";
                action.IDeleteValueOnFields(driver, Elements.Forms_Telephone_InputBox);
                action.IType(driver, Elements.Forms_Telephone_InputBox, telephone_input);

                string email_input = "t3st_" + Util.Configuration.Environment.ToLower() + "_adiSimRfRequestForSoftware_f0rm_" + util.GenerateRandomNumber() + "@mailinator.com";
                //Console.WriteLine("Email Input = " + email_input);
                action.IDeleteValueOnFields(driver, Elements.Forms_Email_InputBox);
                action.IType(driver, Elements.Forms_Email_InputBox, email_input);
                //----- Privacy Settings -----//
                action.ICheck(driver, Elements.Forms_Communication_Checkbox);
                //--- Action: Click on the Submit button ---//
                action.IClick(driver, Elements.Forms_Submit_Button);

                //--- Expected Result: Page should be redirected to ADISimRF Page ---//
                test.validateStringInstance(driver, driver.Url, adiSimRf_page_url);
          }
        }
    }
}