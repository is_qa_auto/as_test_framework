﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using OpenQA.Selenium;
using System;
using System.Threading;

namespace AS_Test_Framework.Forms
{
    [TestFixture]
    public class Forms_ContactIOSubsystemsSupport : BaseSetUp
    {
        //--- Contact I/O Subsystems Support Form URL: ---//
        //--- DEV: https://form-dev.corpnt.analog.com/form_pages/ios/ask.aspx ---//
        //--- QA: https://form-qa.corpnt.analog.com/form_pages/ios/ask.aspx ---//
        //--- PROD: https://form.analog.com/form_pages/ios/ask.aspx ---//

        public Forms_ContactIOSubsystemsSupport() : base() { }

        //--- Login Credentials ---//
        string username = "f0rms_t3st_555@mailinator.com";
        string password = "Test_1234";

        //--- URLs ---//
        string dialogueFeedback_form_url = "form_pages/ios/ask.aspx";
        string contactIOSubsystemsSupport_page_url = "/ask_thankyou.html";

        //--- Labels ---//
        string countryRegion_text = "Country/Region";
        string email_blankInput_errorMessage = "Email address is required";
        string name_blankInput_errorMessage = "Name is required";
        string organizationName_blankInput_errorMessage = "Organization name is required";
        string addressLine_1_blankInput_errorMessage = "Address 1 is required";
        string telephone_blankInput_errorMessage = "Telephone is required";
        string city_blankInput_errorMessage = "City is required";
        string state_blankInput_errorMessage = "State is required";
        string zipPostalCode_blankInput_errorMessage = "Zip is required";
        string countryRegion_blankInput_errorMessage = "Country/Region is required";
        string productLine_blankInput_errorMessage = "Please select at least one product line";
        string requestQuestion_blankInput_errorMessage = "Question is required";
        string email_invalidInput_errorMessage = "Please enter a valid email address";

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Elements are Present and Working as Expected when the User is Logged Out in EN Locale")]
        public void Forms_ContactIOSubsystemsSupport_VerifyElementsWhenUserIsLoggedOut(string Locale)
        {
            //--- Contact I/O Subsystems Support Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + dialogueFeedback_form_url;

            //--- Action: Navigate to the given URL ---//
            action.Navigate(driver, form_url);

            //----- Form TestPlan > EN Tab > R40 > T1: Verify all labels for Country is changed to "Country/Region" (IQ-4970/AL-12466) -----//

            //----- Your detailed information -----//

            //--- Expected Result: The Country label has been changed to Country/Region ---//
            test.validateStringInstance(driver, util.GetText(driver, Elements.Forms_Country_Label), countryRegion_text);

            //----- Form TestPlan > EN Tab > R38 > T1: Verify Privacy Settings in All Forms [account is logged in] (AL-11345) -----//

            //----- Privacy Settings -----//

            //--- Expected Result: Prompt "You can change your privacy settings at any time by clicking on the unsubscribe link in emails sent from Analog Devices or in Analog’s Privacy Settings." is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_LegalDisclaimer_Text);

            //--- Expected Result: Privacy Settings is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_Link);

            //--- Expected Result: Privacy & Security Statement ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacyAndSecurityStatement_Link);

            //--- Expected Result: Authorized Partners ---//
            test.validateElementIsPresent(driver, Elements.Forms_AuthorizedPartners_Link);

            //----- Form TestPlan > EN Tab > R21 > T3_1: Login using Save time to login section with valid data (AL-7439) -----//

            if (!util.CheckElement(driver, Elements.Forms_Expanded_SaveTimeByLoggingInOrRegisteringForMyAnalog_Panel, 1))
            {
                //--- Action: Click on Save time by logging in or registering for myAnalog link ---//
                action.IClick(driver, Elements.Forms_SaveTimeByLoggingInOrRegisteringForMyAnalog_Link);
            }

            //--- Expected Result: Save time to login section should be displayed ---//
            test.validateElementIsPresent(driver, Elements.Forms_Expanded_SaveTimeByLoggingInOrRegisteringForMyAnalog_Panel);
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Elements are Present and Working as Expected when the User is Logged In in EN Locale")]
        public void Forms_ContactIOSubsystemsSupport_VerifyElementsWhenUserIsLoggedIn(string Locale)
        {
            //--- Contact I/O Subsystems Support Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + dialogueFeedback_form_url;

            //--- Action: Navigate to the given URL ---//
            action.Navigate(driver, form_url);

            //----- Form TestPlan > EN Tab > R21 > T3_1: Login using Save time to login section with valid data (AL-7439) -----//

            //----- Save time by logging in or registering for myAnalog -----//

            //--- Action: Login using valid data ---//
            action.ILogInViaSaveTimeByLoggingInPanel(driver, username, password);
            Thread.Sleep(1000);

            //----- About you -----//

            //--- Expected Result: The Email field should be  automatically populated ---//
            test.validateFieldIsNotEmpty(driver, Elements.Forms_Email_InputBox);

            //--- Expected Result: The Name field should be  automatically populated ---//
            test.validateFieldIsNotEmpty(driver, Elements.Forms_Name_InputBox);

            //----- Your detailed information -----//

            //--- Expected Result: The Organization Name field should be  automatically populated ---//
            test.validateFieldIsNotEmpty(driver, Elements.Forms_OrganizationName_InputBox);

            //--- Expected Result: The State/Province(Dropdown) field should be  automatically populated ---//
            test.validateStringChanged(driver, util.GetSelectedDropdownValue(driver, Elements.Forms_StateProvince_Dropdown), "Select State/Province");

            //--- Expected Result: The Zip/PostalCode field should be  automatically populated ---//
            test.validateFieldIsNotEmpty(driver, Elements.Forms_ZipPostalCode_InputBox);

            //--- Expected Result: The Country field should be  automatically populated ---//
            test.validateStringChanged(driver, util.GetSelectedDropdownValue(driver, Elements.Forms_CountryRegion_Dropdown), "Select Country/Region");

            //----- Form TestPlan > EN Tab > R38 > T1: Verify Privacy Settings in All Forms [account is not logged in] (AL-11345) -----//

            //----- Privacy Settings -----//

            //--- Expected Result: Privacy Settings section is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_Section);

            //--- Expected Result: Prompt "You can change your privacy settings at any time by clicking on the unsubscribe link in emails sent from Analog Devices or in Analog’s Privacy Settings." is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_LegalDisclaimer_Text);

            //--- Expected Result: Privacy Settings is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_Link);

            //--- Expected Result: Privacy & Security Statement ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacyAndSecurityStatement_Link);

            //--- Expected Result: Authorized Partners ---//
            test.validateElementIsPresent(driver, Elements.Forms_AuthorizedPartners_Link);
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Input Validations are Working as Expected for Invalid Inputs in EN Locale")]
        public void Forms_ContactIOSubsystemsSupport_VerifyInputValidationsForInvalidInputs(string Locale)
        {
            //--- Contact I/O Subsystems Support Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + dialogueFeedback_form_url;

            //--- Action: Navigate to the given URL ---//
            action.Navigate(driver, form_url);

            //----- Form TestPlan > EN Tab > R21 > T4_2: Submit the Contact I/O Subsystems Support Form    with invalid data -----//

            //--- Action: Enter invalid data on the Email Address field. (e.g. 123!@#$%@yahoo.com) ---//
            string email_input = "123!@#$%@yahoo.com";
            action.IDeleteValueOnFields(driver, Elements.Forms_Email_InputBox);
            action.IType(driver, Elements.Forms_Email_InputBox, email_input);
            action.IType(driver, Elements.Forms_Email_InputBox, Keys.Tab);

            //--- Expected Result: The error message should be displayed: "Please enter a valid email address" ---//
            test.validateStringIsCorrect(driver, Elements.Forms_ContactIOSubsystemsSupport_Email_InvalidInput_ErrorMessage, email_invalidInput_errorMessage);
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Input Validations are Working as Expected for Blank Inputs in EN Locale")]
        public void Forms_ContactIOSubsystemsSupport_VerifyInputValidationsForBlankInputs(string Locale)
        {
            //--- Contact I/O Subsystems Support Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + dialogueFeedback_form_url;

            //--- Action: Navigate to the given URL ---//
            action.Navigate(driver, form_url);

            //----- Form TestPlan > EN Tab > R21 > T4_1: Submit Contact I/O Subsystems Support  Form with no data on mandatory fields -----//

            //--- Action: Click on the Submit button without entering data on the mandatory fields. ---//
            action.IClick(driver, Elements.Forms_Submit_Button);

            //----- About you -----//

            //--- Expected Result: The Email : "Email address is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_ContactIOSubsystemsSupport_Email_BlankInput_ErrorMessage, email_blankInput_errorMessage);

            //--- Expected Result: The Name: "Name is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_ContactIOSubsystemsSupport_Name_BlankInput_ErrorMessage, name_blankInput_errorMessage);

            //----- Your detailed information -----//

            //--- Expected Result: The Organization Name: "Organization name is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_ContactIOSubsystemsSupport_OrganizationName_BlankInput_ErrorMessage, organizationName_blankInput_errorMessage);

            //--- Expected Result: The Address Line 1:  "Address 1 is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_ContactIOSubsystemsSupport_AddressLine_1_BlankInput_ErrorMessage, addressLine_1_blankInput_errorMessage);

            //--- Expected Result: The Telephone: "Telephone is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_ContactIOSubsystemsSupport_Telephone_BlankInput_ErrorMessage, telephone_blankInput_errorMessage);

            //--- Expected Result: The City: "City is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_ContactIOSubsystemsSupport_City_BlankInput_ErrorMessage, city_blankInput_errorMessage);

            //--- Expected Result: The State/Province(Dropdown):  "State/Province is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_StateProvince_BlankInput_ErrorMessage, state_blankInput_errorMessage);

            //--- Expected Result: The Zip/PostalCode:  "Zip/PostalCode is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_ContactIOSubsystemsSupport_ZipPostalCode_BlankInput_ErrorMessage, zipPostalCode_blankInput_errorMessage);

            //--- Expected Result: The Country(dropdown): "Country is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_CountryRegion_BlankInput_ErrorMessage, countryRegion_blankInput_errorMessage);

            //----- Product line -----//

            //--- Expected Result: The Product Line(checkbox): "Please select at least one product line" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_ContactIOSubsystemsSupport_ProductLine_BlankInput_ErrorMessage, productLine_blankInput_errorMessage);

            //--- Expected Result: The Request/Question: "Question is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_ContactIOSubsystemsSupport_RequestQuestion_BlankInput_ErrorMessage, requestQuestion_blankInput_errorMessage);
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Form Submission is Working as Expected in EN Locale")]
        public void Forms_ContactIOSubsystemsSupport_VerifyFormSubmission(string Locale)
        {
            if (Util.Configuration.Environment.Equals("dev") || Util.Configuration.Environment.Equals("qa"))
            {
                //--- Contact I/O Subsystems Support Form URL ---//
                string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + dialogueFeedback_form_url;

                //--- Action: Navigate to the given URL ---//
                action.Navigate(driver, form_url);

                //----- Form TestPlan > EN Tab > R21 > T4_3: Submit Contact I/O Subsystems Support  Form with valid data (AL-2496) -----//

                //--- Action: Enter valid data on the mandatory fields. ---//

                //----- About you -----//
                string email_input = "t3st_" + Util.Configuration.Environment.ToLower() + "_dialogueFeedback_f0rm_" + util.GenerateRandomNumber() + "@mailinator.com";
                //Console.WriteLine("Email Input = " + email_input);
                action.IDeleteValueOnFields(driver, Elements.Forms_Email_InputBox);
                action.IType(driver, Elements.Forms_Email_InputBox, email_input);

                string name_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_Name_InputBox);
                action.IType(driver, Elements.Forms_Name_InputBox, name_input);

                //----- Your detailed information -----//
                string organizationName_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_OrganizationName_InputBox);
                action.IType(driver, Elements.Forms_OrganizationName_InputBox, organizationName_input);

                string addressLine_1_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_AddressLine_1_InputBox);
                action.IType(driver, Elements.Forms_AddressLine_1_InputBox, addressLine_1_input);

                string telephone_input = "123456789";
                action.IDeleteValueOnFields(driver, Elements.Forms_Telephone_InputBox);
                action.IType(driver, Elements.Forms_Telephone_InputBox, telephone_input);

                string city_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_City_InputBox);
                action.IType(driver, Elements.Forms_City_InputBox, city_input);

                string zipPostalCode_input = "12345";
                action.IDeleteValueOnFields(driver, Elements.Forms_ZipPostalCode_InputBox);
                action.IType(driver, Elements.Forms_ZipPostalCode_InputBox, zipPostalCode_input);

                string countryRegion_input = "US";
                action.ISelectFromDropdown(driver, Elements.Forms_CountryRegion_Dropdown, countryRegion_input);

                action.IClick(driver, Elements.Forms_StateProvince_Dropdown);
                action.IClick(driver, By.CssSelector("select[id$='tate']>option:nth-of-type(3)"));

                //----- Product line -----//
                action.ICheck(driver, Elements.Forms_ProductLine_Checkboxes);

                string requestQuestion_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_RequestQuestion_TextArea);
                action.IType(driver, Elements.Forms_RequestQuestion_TextArea, requestQuestion_input);

                //----- Privacy Settings -----//
                action.ICheck(driver, Elements.Forms_Communication_Checkbox);

                //--- Action: Click on submit button ---//
                action.IClick(driver, Elements.Forms_Submit_Button);

                //--- Expected Result: Contact I/O Subsystems Support  Form Thank You page should be displayed ---//
                test.validateStringInstance(driver, driver.Url, contactIOSubsystemsSupport_page_url);

                if (driver.Url.Contains(contactIOSubsystemsSupport_page_url))
                {
                    action.ICheckMailinatorEmail(driver, email_input);

                    //--- Expected Result: The email from jim.kellogg@analog.com should be displayed ---//
                    test.validateStringIsCorrect(driver, Elements.Mailinator_LatestEmail_Sender, "jim.kellogg@analog.com");
                }
            }
        }
    }
}