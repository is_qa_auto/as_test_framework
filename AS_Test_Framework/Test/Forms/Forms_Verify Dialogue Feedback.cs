﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using OpenQA.Selenium;
using System;
using System.Threading;

namespace AS_Test_Framework.Forms
{
    [TestFixture]
    public class Forms_DialogueFeedbacks : BaseSetUp
    {
        //--- Dialogue Feedback Form URL: ---//
        //--- DEV: https://form-dev.corpnt.analog.com/Form_Pages/feedback/dialogue_feedback.aspx ---//
        //--- QA: https://form-qa.corpnt.analog.com/Form_Pages/feedback/dialogue_feedback.aspx ---//
        //--- PROD: https://form.analog.com/Form_Pages/feedback/dialogue_feedback.aspx ---//

        public Forms_DialogueFeedbacks() : base() { }

        //--- Login Credentials ---//
        string username = "f0rms_t3st_555@mailinator.com";
        string password = "Test_1234";

        //--- URLs ---//
        string dialogueFeedback_form_url = "Form_Pages/feedback/dialogue_feedback.aspx";
        string thankYou_page_url = "/Analog-Dialogue-Feedback-Form-Thank-You.html";

        //--- Labels ---//
        string countryRegion_text = "Country/Region";
        string name_blankInput_errorMessage = "Name is required";
        string email_blankInput_errorMessage = "Email address is required";
        string enterYourCommentsHere_blankInput_errorMessage = "Comments is required";
        string email_invalidInput_errorMessage = "Please enter a valid email address";

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Elements are Present and Working as Expected when the User is Logged Out in EN Locale")]
        public void Forms_DialogueFeedbacks_VerifyElementsWhenUserIsLoggedOut(string Locale)
        {
            //--- Dialogue Feedback Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + dialogueFeedback_form_url;

            //--- Action: Navigate to the given URL ---//
            action.Navigate(driver, form_url);

            //----- Form TestPlan > EN Tab > R40 > T1: Verify all labels for Country is changed to "Country/Region" (IQ-4970/AL-12466) -----//

            //----- About you -----//

            //--- Expected Result: The Country label has been changed to Country/Region ---//
            test.validateStringInstance(driver, util.GetText(driver, Elements.Forms_Country_Label), countryRegion_text);

            //----- Form TestPlan > EN Tab > R38 > T1: Verify Privacy Settings in All Forms [account is not logged in] (AL-11345) -----//

            //----- Privacy Settings -----//

            //--- Expected Result: Privacy Settings section is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_Section);

            //--- Expected Result: Prompt "You can change your privacy settings at any time by clicking on the unsubscribe link in emails sent from Analog Devices or in Analog’s Privacy Settings." is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_LegalDisclaimer_Text);

            //--- Expected Result: Privacy Settings is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_Link);

            //--- Expected Result: Privacy & Security Statement ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacyAndSecurityStatement_Link);

            //--- Expected Result: Authorized Partners ---//
            test.validateElementIsPresent(driver, Elements.Forms_AuthorizedPartners_Link);

            //----- Form TestPlan > EN Tab > R18 > T3: Login using Save time to login section with valid data -----//

            if (!util.CheckElement(driver, Elements.Forms_Expanded_SaveTimeByLoggingInOrRegisteringForMyAnalog_Panel, 1))
            {
                //--- Action: Click on Save time by logging in or registering for myAnalog   link ---//
                action.IClick(driver, Elements.Forms_SaveTimeByLoggingInOrRegisteringForMyAnalog_Link);
            }

            //--- Expected Result: Save time to login section should be displayed ---//
            test.validateElementIsPresent(driver, Elements.Forms_Expanded_SaveTimeByLoggingInOrRegisteringForMyAnalog_Panel);
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Elements are Present and Working as Expected when the User is Logged In in EN Locale")]
        public void Forms_DialogueFeedbacks_VerifyElementsWhenUserIsLoggedIn(string Locale)
        {
            //--- Dialogue Feedback Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + dialogueFeedback_form_url;

            //--- Action: Navigate to the given URL ---//
            action.Navigate(driver, form_url);

            //----- Form TestPlan > EN Tab > R18 > T3: Login using Save time to login section with valid data -----//

            //----- Save time by logging in or registering for myAnalog -----//

            //--- Action: Login using valid data ---//
            action.ILogInViaSaveTimeByLoggingInPanel(driver, username, password);
            Thread.Sleep(1000);

            driver.Navigate().Refresh();
            Thread.Sleep(1000);

            //----- About you -----//

            //--- Expected Result: The Name field should be  automatically populated ---//
            test.validateFieldIsNotEmpty(driver, Elements.Forms_Name_InputBox);

            //--- Expected Result: The Company field should be  automatically populated ---//
            test.validateFieldIsNotEmpty(driver, Elements.Forms_Company_InputBox);

            //--- Expected Result: The Country(dropdown) field should be  automatically populated ---//
            test.validateStringChanged(driver, util.GetSelectedDropdownValue(driver, Elements.Forms_CountryRegion_Dropdown), "Select Country/Region");

            //--- Expected Result: The Email field should be  automatically populated ---//
            test.validateFieldIsNotEmpty(driver, Elements.Forms_Email_InputBox);

            //----- Form TestPlan > EN Tab > R38 > T1: Verify Privacy Settings in All Forms [account is logged in] (AL-11345) -----//

            //----- Privacy Settings -----//

            //--- Expected Result: Prompt "You can change your privacy settings at any time by clicking on the unsubscribe link in emails sent from Analog Devices or in Analog’s Privacy Settings." is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_LegalDisclaimer_Text);

            //--- Expected Result: Privacy Settings is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_Link);

            //--- Expected Result: Privacy & Security Statement ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacyAndSecurityStatement_Link);

            //--- Expected Result: Authorized Partners ---//
            test.validateElementIsPresent(driver, Elements.Forms_AuthorizedPartners_Link);
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Input Validations are Working as Expected for Invalid Inputs in EN Locale")]
        public void Forms_DialogueFeedbacks_VerifyInputValidationsForInvalidInputs(string Locale)
        {
            //--- Dialogue Feedback Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + dialogueFeedback_form_url;

            //--- Action: Navigate to the given URL ---//
            action.Navigate(driver, form_url);

            //----- Form TestPlan > EN Tab > R18 > T4_2: Submit the Dialogue Feedback  Form    with invalid data -----//

            //--- Action: Enter invalid data on the Email Address field. (e.g. 123!@#$%@yahoo.com) ---//
            string email_input = "123!@#$%@yahoo.com";
            action.IDeleteValueOnFields(driver, Elements.Forms_Email_InputBox);
            action.IType(driver, Elements.Forms_Email_InputBox, email_input);
            action.IType(driver, Elements.Forms_Email_InputBox, Keys.Tab);

            //--- Expected Result: The error message should be displayed: "Please enter a valid email address" ---//
            test.validateStringIsCorrect(driver, Elements.Forms_DialogueFeedbacks_Email_InvalidInput_ErrorMessage, email_invalidInput_errorMessage);
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Input Validations are Working as Expected for Blank Inputs in EN Locale")]
        public void Forms_DialogueFeedbacks_VerifyInputValidationsForBlankInputs(string Locale)
        {
            //--- Dialogue Feedback Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + dialogueFeedback_form_url;

            //--- Action: Navigate to the given URL ---//
            action.Navigate(driver, form_url);

            //----- Form TestPlan > EN Tab > R18 > T4_1: Submit Dialogue Feedback form    with no data on mandatory fields  -----//

            //--- Action: Click on the Submit button without entering data on the mandatory fields. ---//
            action.IClick(driver, Elements.Forms_Submit_Button);

            //----- About you -----//

            //--- Expected Result: The Name: "Name is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_DialogueFeedbacks_Name_BlankInput_ErrorMessage, name_blankInput_errorMessage);

            //--- Expected Result: The Email: "Email address is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_DialogueFeedbacks_Email_BlankInput_ErrorMessage, email_blankInput_errorMessage);

            //----- Your comments -----//

            //--- Expected Result: The Enter your comments here: "Comments is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_DialogueFeedbacks_EnterYourCommentsHere_BlankInput_ErrorMessage, enterYourCommentsHere_blankInput_errorMessage);
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Form Submission is Working as Expected in EN Locale")]
        public void Forms_DialogueFeedbacks_VerifyFormSubmission(string Locale)
        {
            if (Util.Configuration.Environment.Equals("dev") || Util.Configuration.Environment.Equals("qa"))
            {
                //--- Dialogue Feedback Form URL ---//
                string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + dialogueFeedback_form_url;

                //--- Action: Navigate to the given URL ---//
                action.Navigate(driver, form_url);

                //----- Form TestPlan > EN Tab > R18 > T4_3: Submit Dialogue Feedback   form with valid data (AL-2496) -----//

                //--- Action: Enter valid data on the mandatory fields. ---//

                //----- About you -----//
                string name_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_Name_InputBox);
                action.IType(driver, Elements.Forms_Name_InputBox, name_input);

                string countryRegion_input = "US";
                action.ISelectFromDropdown(driver, Elements.Forms_CountryRegion_Dropdown, countryRegion_input);

                string email_input = "t3st_" + Util.Configuration.Environment.ToLower() + "_dialogueFeedback_f0rm_" + util.GenerateRandomNumber() + "@mailinator.com";
                //Console.WriteLine("Email Input = " + email_input);
                action.IDeleteValueOnFields(driver, Elements.Forms_Email_InputBox);
                action.IType(driver, Elements.Forms_Email_InputBox, email_input);

                //----- Your comments -----//
                string enterYourCommentsHere_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_Comments_TextArea);
                action.IType(driver, Elements.Forms_Comments_TextArea, enterYourCommentsHere_input);

                //----- Privacy Settings -----//
                action.ICheck(driver, Elements.Forms_Communication_Checkbox);

                //--- Action: Click on submit button ---//
                action.IClick(driver, Elements.Forms_Submit_Button);

                //--- Expected Result: The Support Thank You page should be displayed ---//
                test.validateStringInstance(driver, driver.Url, thankYou_page_url);

                if (driver.Url.Contains(thankYou_page_url))
                {
                    action.ICheckMailinatorEmail(driver, email_input);

                    //--- Expected Result: The email from dialogue.editor@analog.com should be displayed ---//
                    test.validateStringIsCorrect(driver, Elements.Mailinator_LatestEmail_Sender, "dialogue.editor@analog.com");
                }
            }
        }
    }
}