﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using OpenQA.Selenium;
using System;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.Forms
{
    [TestFixture]
    public class Forms_VisualDspLicenseRegistration : BaseSetUp
    {
        //--- VisualDSP++ License Registration Form URL: ---//
        //--- DEV: https://form-dev.corpnt.analog.com/Form_Pages/dsp/products/visualDSPRegistration.aspx ---//
        //--- QA: https://form-qa.corpnt.analog.com/Form_Pages/dsp/products/visualDSPRegistration.aspx ---//
        //--- PROD: https://form.analog.com/Form_Pages/dsp/products/visualDSPRegistration.aspx ---//

        public Forms_VisualDspLicenseRegistration() : base() { }

        //--- Login Credentials ---//
        string username = "f0rms_t3st_555@mailinator.com";
        string password = "Test_1234";

        //--- URLs ---//
        string visualDspLicenseRegistration_form_url = "Form_Pages/dsp/products/visualDSPRegistration.aspx";
        string visualDspTestDriveRegistration_form_url = "/visualDSPTestDrive.aspx";
        string thankYou_page_url = "/visualdsp_registration_thank_you.html";

        //--- Labels ---//
        string countryRegion_text = "Country/Region";
        string email_blankInput_errorMessage = "Email address is required";
        string firstName_blankInput_errorMessage = "First Name is required";
        string lastName_blankInput_errorMessage = "Last Name is required";
        string organizationName_errorMessage = "Organization name is required";
        string addressLine_1_blankInput_errorMessage = "Address Line 1 is required";
        string telephone_blankInput_errorMessage = "Telephone is required";
        string city_blankInput_errorMessage = "City is required";
        string zipPostalCode_blankInput_errorMessage = "Zip/Postal Code is required";
        string countryRegion_blankInput_errorMessage = "Country/Region is required";
        string stateProvince_blankInput_errorMessage = "State/Province is required";
        string serialNo_blankInput_errorMessage = "Please provide the Serial #";
        string hostId_blankInput_errorMessage = "Please provide a Host(Machine) ID";
        string selectDistributor_blankInput_errorMessage = "Please select a value from dropdown";
        string whichEmbeddedProcessorAreYouEvaluating_blankInput_errorMessage = "Please select the embedded processor you are evaluating";
        string whatIsThePrimaryMarketSegmentThatYourApplicationTargets_blankInput_errorMessage = "Please select the primary market segment that your application is targeting";
        string whatIsThePrimaryTypeOfApplicationThatYouDevelop_blankInput_errorMessage = "Please select the primary type of application that you develop";
        string email_invalidInput_errorMessage = "Please enter a valid email address";

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Elements are Present and Working as Expected when the User is Logged Out in EN Locale")]
        public void Forms_VisualDspLicenseRegistration_VerifyElementsWhenUserIsLoggedOut(string Locale)
        {
            //--- VisualDSP++ License Registration Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + visualDspLicenseRegistration_form_url;

            //--- Action: Navigate to the given URL ---//
            action.Navigate(driver, form_url);

            //----- Form TestPlan > EN Tab > R40 > T1: Verify all labels for Country is changed to "Country/Region" (IQ-4970/AL-12466) -----//

            //----- Your detailed information -----//

            //--- Expected Result: The Country label has been changed to Country/Region ---//
            test.validateStringInstance(driver, util.GetText(driver, Elements.Forms_Country_Label), countryRegion_text);

            //----- Form TestPlan > EN Tab > R38 > T1: Verify Privacy Settings in All Forms [account is not logged in] (AL-11345) -----//

            //----- Privacy Settings -----//

            //--- Expected Result: Privacy Settings section is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_Section);

            //--- Expected Result: Prompt "You can change your privacy settings at any time by clicking on the unsubscribe link in emails sent from Analog Devices or in Analog’s Privacy Settings." is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_LegalDisclaimer_Text);

            //--- Expected Result: Privacy Settings is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_Link);

            //--- Expected Result: Privacy & Security Statement ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacyAndSecurityStatement_Link);

            //--- Expected Result: Authorized Partners ---//
            test.validateElementIsPresent(driver, Elements.Forms_AuthorizedPartners_Link);

            //----- Form TestPlan > EN Tab > R11 > T3: Verify VisualDSPLicenseRegistration Form description -----//

            //----- VisualDSP++ License Registration -----//

            //--- Expected Result: VisualDSPLicenseRegistration description should be shown ---//
            test.validateElementIsPresent(driver, Elements.Forms_VisualDspLicenseRegistration_Description_Text);

            if (util.CheckElement(driver, Elements.Forms_VisualDspLicenseRegistration_TestDriveRegistrationForm_Link, 1))
            {
                //--- Action: Click on the Test Drive Registration Form link on the description ---//
                action.IOpenLinkInNewTab(driver, Elements.Forms_VisualDspLicenseRegistration_TestDriveRegistrationForm_Link);

                //--- Expected Result: VisualDSP++ Test Drive Registration form should be displayed ---//
                test.validateStringInstance(driver, driver.Url, visualDspTestDriveRegistration_form_url);

                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles.First());
            }

            //----- Form TestPlan > EN Tab > R11 > T4_1: Login using Save time to login section with valid data (AL-7439) -----//

            //----- Save time by logging in or registering for myAnalog -----//

            if (!util.CheckElement(driver, Elements.Forms_Expanded_SaveTimeByLoggingInOrRegisteringForMyAnalog_Panel, 1))
            {
                //--- Action: Click on Save time by logging in or registering for myAnalog link ---//
                action.IClick(driver, Elements.Forms_SaveTimeByLoggingInOrRegisteringForMyAnalog_Link);
            }

            //--- Expected Result: Save time to login section should be displayed (AL-7439) ---//
            test.validateElementIsPresent(driver, Elements.Forms_Expanded_SaveTimeByLoggingInOrRegisteringForMyAnalog_Panel);
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Elements are Present and Working as Expected when the User is Logged In in EN Locale")]
        public void Forms_VisualDspLicenseRegistration_VerifyElementsWhenUserIsLoggedIn(string Locale)
        {
            //--- VisualDSP++ License Registration Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + visualDspLicenseRegistration_form_url;

            //--- Action: Navigate to the given URL ---//
            action.Navigate(driver, form_url);

            //----- Form TestPlan > EN Tab > R11 > T4_1: Login using Save time to login section with valid data (AL-7439) -----//

            //----- Save time by logging in or registering for myAnalog -----//

            //--- Action: Login using valid data ---//
            action.ILogInViaSaveTimeByLoggingInPanel(driver, username, password);
            Thread.Sleep(1000);

            //----- About you -----//

            //--- Expected Result: The Email Id field should be  automatically populated ---//
            test.validateElementIsPresent(driver, Elements.Forms_Email_InputBox);

            //--- Expected Result: The First Name field should be  automatically populated ---//
            test.validateElementIsPresent(driver, Elements.Forms_FirstName_InputBox);

            //--- Expected Result: The Last Name field should be  automatically populated ---//
            test.validateElementIsPresent(driver, Elements.Forms_FirstName_InputBox);

            //----- Your detailed information -----//

            //--- Expected Result: The Organization Name field should be  automatically populated ---//
            test.validateElementIsPresent(driver, Elements.Forms_OrganizationName_InputBox);

            //--- Expected Result: The Zip/Postal Code field should be  automatically populated ---//
            test.validateElementIsPresent(driver, Elements.Forms_ZipPostalCode_InputBox);

            //--- Expected Result: The Country(dropdown) field should be  automatically populated ---//
            test.validateStringChanged(driver, util.GetSelectedDropdownValue(driver, Elements.Forms_CountryRegion_Dropdown), "Select Country/Region");

            //--- Expected Result: The State/Province(dropdown) field should be  automatically populated ---//
            test.validateStringChanged(driver, util.GetSelectedDropdownValue(driver, Elements.Forms_StateProvince_Dropdown), "Select State/Province");

            //----- Form TestPlan > EN Tab > R11 > T5: Verify the behavior of the disabled items on VisualDSPLicenseRegistration -----//

            //----- Your serial number -----//

            //--- Expected Result: Which product are you registering? Dropdown should be disabled ---//
            test.validateElementIsNotEnabled(driver, Elements.Forms_WhichProductAreYouRegistering_Dropdown);

            //----- Your project -----//

            //--- Expected Result: Select Distributor (dropdown) should be disabled ---//
            test.validateElementIsNotEnabled(driver, Elements.Forms_SelectDistributor_Dropdown);

            //--- Expected Result: Which embedded processor are you evaluating(dropdown)? should be disabled ---//
            test.validateElementIsNotEnabled(driver, Elements.Forms_VisualDspLicenseRegistration_WhichEmbeddedProcessorAreYouEvaluating_Dropdown);

            //--- Expected Result: What is the primary type of application that you develop(dropdown)? should be disabled ---//
            test.validateElementIsNotEnabled(driver, Elements.Forms_VisualDspTestDriveRegistration_WhatIsThePrimaryTypeOfApplicationThatYouDevelop_Dropdown);

            //----- Form TestPlan > EN Tab > R38 > T1: Verify Privacy Settings in All Forms [account is logged in] (AL-11345) -----//

            //----- Privacy Settings -----//

            //--- Expected Result: Prompt "You can change your privacy settings at any time by clicking on the unsubscribe link in emails sent from Analog Devices or in Analog’s Privacy Settings." is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_LegalDisclaimer_Text);

            //--- Expected Result: Privacy Settings is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_Link);

            //--- Expected Result: Privacy & Security Statement ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacyAndSecurityStatement_Link);

            //--- Expected Result: Authorized Partners ---//
            test.validateElementIsPresent(driver, Elements.Forms_AuthorizedPartners_Link);
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Input Validations are Working as Expected for Valid Inputs in EN Locale")]
        public void Forms_VisualDspLicenseRegistration_VerifyInputValidationsForValidInputs(string Locale)
        {
            //--- VisualDSP++ License Registration Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + visualDspLicenseRegistration_form_url;

            //--- Action: Navigate to the given URL ---//
            action.Navigate(driver, form_url);

            //----- Form TestPlan > EN Tab > R11 > T5: Verify the behavior of the disabled items on VisualDSPLicenseRegistration -----//

            //--- Action: On Your Serial Number section put a valid serial number value (e.g. ENG-160-256-26020356-462) ---//
            string serialNo_input = "ENG-160-256-26020356-462";
            action.IDeleteValueOnFields(driver, Elements.Forms_SerialNo_InputBox);
            action.IType(driver, Elements.Forms_SerialNo_InputBox, serialNo_input);
            action.IType(driver, Elements.Forms_SerialNo_InputBox, Keys.Tab);

            //--- Expected Result: Which product are you registering? Dropdown should be enabled ---//
            test.validateElementIsEnabled(driver, Elements.Forms_WhichProductAreYouRegistering_Dropdown);

            //--- Action: Check on  Which product are you registering? Dropdown (e.g. VDSP-BLKFN-PC-FULL) ---//
            action.IClick(driver, Elements.Forms_WhichProductAreYouRegistering_Dropdown);
            action.IClick(driver, By.CssSelector("select[id$='Tool_Model']>option:nth-of-type(2)"));

            //--- Expected Result: On Your Project section the Which embedded processor are you evaluating? dropdown should  be enabled ---//
            test.validateElementIsEnabled(driver, Elements.Forms_VisualDspLicenseRegistration_WhichEmbeddedProcessorAreYouEvaluating_Dropdown);

            //--- Action: On Your Project section , on How did you purchase VisualDSP++? Checkboxes choose ADI authorized Distributor checkbox (e.g.  ADI authorized Distributor) ---//
            action.ICheck(driver, Elements.Forms_AdiAuthorizedDistributor_Checkbox);

            //--- Expected Result: Select Distributor dropdown should be enabled ---//
            test.validateElementIsEnabled(driver, Elements.Forms_SelectDistributor_Dropdown);

            //--- Action: On What is the primary market segment that your application targets? dropdown choose a value (e.g. Automotive) ---//
            action.IClick(driver, Elements.Forms_VisualDspTestDriveRegistration_WhatIsThePrimaryMarketSegmentThatYourApplicationTargets_Dropdown);
            action.IClick(driver, By.CssSelector("select[id$='Primary_Market_Segment']>option:nth-of-type(2)"));

            //--- Expected Result: What is the primary type of application that you develop? dropdown  Should be enabled ---//     
            test.validateElementIsEnabled(driver, Elements.Forms_VisualDspTestDriveRegistration_WhatIsThePrimaryTypeOfApplicationThatYouDevelop_Dropdown);
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Input Validations are Working as Expected for Invalid Inputs in EN Locale")]
        public void Forms_VisualDspLicenseRegistration_VerifyInputValidationsForInvalidInputs(string Locale)
        {
            //--- VisualDSP++ License Registration Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + visualDspLicenseRegistration_form_url;

            //--- Action: Navigate to the given URL ---//
            action.Navigate(driver, form_url);

            //----- Form TestPlan > EN Tab > R11 > T6_2: Submit the VisualDSPLicenseRegistration Form    with invalid data -----//

            //--- Action: Enter invalid data on the Email Address field. (e.g. 123!@#$%@yahoo.com) ---//
            string emailId_input = "123!@#$%@yahoo.com";
            action.IDeleteValueOnFields(driver, Elements.Forms_Email_InputBox);
            action.IType(driver, Elements.Forms_Email_InputBox, emailId_input);
            action.IType(driver, Elements.Forms_Email_InputBox, Keys.Tab);

            //--- Expected Result: The error message below should be displayed: "Please enter a valid email address" ---//
            test.validateStringIsCorrect(driver, Elements.Forms_VisualDspLicenseRegistration_Email_InvalidInput_ErrorMessage, email_invalidInput_errorMessage);
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Input Validations are Working as Expected for Blank Inputs in EN Locale")]
        public void Forms_VisualDspLicenseRegistration_VerifyInputValidationsForBlankInputs(string Locale)
        {
            //--- VisualDSP++ License Registration Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + visualDspLicenseRegistration_form_url;

            //--- Action: Navigate to the given URL ---//
            action.Navigate(driver, form_url);

            //----- Form TestPlan > EN Tab > R11 > T6_1: Submit VisualDSPLicenseRegistration Form   with no data on mandatory fields -----//

            //--- Action: Click on the Submit button without entering data on the mandatory fields. ---//
            action.IClick(driver, Elements.Forms_Submit_Button);

            //----- About you -----//

            //--- Expected Result: The Email Id:  "Email address is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_VisualDspLicenseRegistration_Email_BlankInput_ErrorMessage, email_blankInput_errorMessage);

            //--- Expected Result: The First Name:   "First Name is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_FirstName_BlankInput_ErrorMessage, firstName_blankInput_errorMessage);

            //--- Expected Result: The Last Name: "Last Name is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_LastName_BlankInput_ErrorMessage, lastName_blankInput_errorMessage);

            //----- Your detailed information -----//

            //--- Expected Result: The Organization name : "Organization Name is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_OrganizationName_BlankInput_ErrorMessage, organizationName_errorMessage);

            //--- Expected Result: The Address 1:  "Address 1 is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_Address_1_BlankInput_ErrorMessage, addressLine_1_blankInput_errorMessage);

            //--- Expected Result: The Telephone: "Telephone is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_VisualDspLicenseRegistration_Telephone_BlankInput_ErrorMessage, telephone_blankInput_errorMessage);

            //--- Expected Result: The City: "City is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_City_BlankInput_ErrorMessage, city_blankInput_errorMessage);

            //--- Expected Result: The Zip/Postal Code: "Zip/Postal Code is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_ZipPostalCode_BlankInput_ErrorMessage, zipPostalCode_blankInput_errorMessage);

            //--- Expected Result: The Country(dropdown): "Country is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_CountryRegion_BlankInput_ErrorMessage, countryRegion_blankInput_errorMessage);

            //--- Expected Result: The State/Province(dropdown): "State Province is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_StateProvince_BlankInput_ErrorMessage, stateProvince_blankInput_errorMessage);

            //----- Your serial number -----//

            //--- Expected Result: The Serial#: "Please provide the Serial #" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_SerialNo_BlankInput_ErrorMessage, serialNo_blankInput_errorMessage);

            //----- Your host ID -----//

            //--- Expected Result: The Host ID:  "Please provide a Host(Machine) ID" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_HostId_BlankInput_ErrorMessage, hostId_blankInput_errorMessage);

            //----- Your project -----//

            //--- Expected Result: The How did you purchase Visual DSP++: "Please select a value from dropdown" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_VisualDspLicenseRegistration_SelectDistributor_BlankInput_ErrorMessage, selectDistributor_blankInput_errorMessage);

            //--- Expected Result: The Which embedded processor are you evaluating?(dropdown): "Please select the embedded processor you are evaluating" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_WhichEmbeddedProcessorAreYouEvaluating_BlankInput_ErrorMessage, whichEmbeddedProcessorAreYouEvaluating_blankInput_errorMessage);

            //--- Expected Result: The What is the primary market segment that your application targets?(dropdown): "Please select the primary market segment that your application is targeting" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_WhatIsThePrimaryMarketSegmentThatYourApplicationTargets_BlankInput_ErrorMessage, whatIsThePrimaryMarketSegmentThatYourApplicationTargets_blankInput_errorMessage);

            //--- Expected Result: The What is the primary type of application that you develop?(dropdown): "Please select the primary type of application that you develop" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_WhatIsThePrimaryTypeOfApplicationThatYouDevelop_BlankInput_ErrorMessage, whatIsThePrimaryTypeOfApplicationThatYouDevelop_blankInput_errorMessage);
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Form Submission is Working as Expected in EN Locale")]
        public void Forms_VisualDspLicenseRegistration_VerifyFormSubmission(string Locale)
        {
            if (Util.Configuration.Environment.Equals("dev") || Util.Configuration.Environment.Equals("qa"))
            {
                //--- VisualDSP++ License Registration Form URL ---//
                string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + visualDspLicenseRegistration_form_url;

                //--- Action: Navigate to the given URL ---//
                action.Navigate(driver, form_url);

                //----- Form TestPlan > EN Tab > R11 > T6_3: Submit VisualDSPLicenseRegistration  form with valid data (AL-2496) -----//

                //--- Action: Enter valid data on the mandatory fields. ---//

                //----- About you -----//
                string emailId_input = "t3st_" + Util.Configuration.Environment.ToLower() + "_visualDspLicenseRegistrationForm_f0rm_" + util.GenerateRandomNumber() + "@mailinator.com";
                //Console.WriteLine("Email Id Input = " + emailId_input);
                action.IDeleteValueOnFields(driver, Elements.Forms_Email_InputBox);
                action.IType(driver, Elements.Forms_Email_InputBox, emailId_input);

                string firstName_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_FirstName_InputBox);
                action.IType(driver, Elements.Forms_FirstName_InputBox, firstName_input);

                string lastName_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_LastName_InputBox);
                action.IType(driver, Elements.Forms_LastName_InputBox, lastName_input);

                //----- Your detailed information -----//
                string organizationName_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_OrganizationName_InputBox);
                action.IType(driver, Elements.Forms_OrganizationName_InputBox, organizationName_input);

                string address_1_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_Address_1_InputBox);
                action.IType(driver, Elements.Forms_Address_1_InputBox, address_1_input);

                string telephone_input = "123456789";
                action.IDeleteValueOnFields(driver, Elements.Forms_Telephone_InputBox);
                action.IType(driver, Elements.Forms_Telephone_InputBox, telephone_input);

                string city_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_City_InputBox);
                action.IType(driver, Elements.Forms_City_InputBox, city_input);

                string zipPostalCode_input = "12345";
                action.IDeleteValueOnFields(driver, Elements.Forms_ZipPostalCode_InputBox);
                action.IType(driver, Elements.Forms_ZipPostalCode_InputBox, zipPostalCode_input);

                string countryRegion_input = "US";
                action.ISelectFromDropdown(driver, Elements.Forms_CountryRegion_Dropdown, countryRegion_input);

                action.IClick(driver, Elements.Forms_StateProvince_Dropdown);
                action.IClick(driver, By.CssSelector("select[id$='tate']>option:nth-of-type(3)"));

                //----- Your serial number -----//
                string serialNo_input = "ENG-160-256-26020356-462";
                action.IDeleteValueOnFields(driver, Elements.Forms_SerialNo_InputBox);
                action.IType(driver, Elements.Forms_SerialNo_InputBox, serialNo_input);

                action.IClick(driver, Elements.Forms_WhichProductAreYouRegistering_Dropdown);
                action.IClick(driver, By.CssSelector("select[id$='Tool_Model']>option:nth-of-type(2)"));

                //----- Your host ID -----//
                string hostId_input = "7C37E458";
                action.IDeleteValueOnFields(driver, Elements.Forms_HostId_InputBox);
                action.IType(driver, Elements.Forms_HostId_InputBox, hostId_input);

                //----- Your project -----//
                action.ICheck(driver, Elements.Forms_SelectDistributor_Checkboxes);

                if (driver.FindElement(Elements.Forms_SelectDistributor_Dropdown).Enabled)
                {
                    action.IClick(driver, Elements.Forms_SelectDistributor_Dropdown);
                    action.IClick(driver, By.CssSelector("select[id$='Distributor']>option:nth-of-type(2)"));
                }

                action.IClick(driver, Elements.Forms_VisualDspLicenseRegistration_WhichEmbeddedProcessorAreYouEvaluating_Dropdown);
                action.IClick(driver, By.CssSelector("select[id$='Evaluating_Which_DSP']>option:nth-of-type(2)"));

                action.IClick(driver, Elements.Forms_VisualDspTestDriveRegistration_WhatIsThePrimaryMarketSegmentThatYourApplicationTargets_Dropdown);
                action.IClick(driver, By.CssSelector("select[id$='Primary_Market_Segment']>option:nth-of-type(2)"));

                action.IClick(driver, Elements.Forms_VisualDspTestDriveRegistration_WhatIsThePrimaryTypeOfApplicationThatYouDevelop_Dropdown);
                action.IClick(driver, By.CssSelector("select[id$='Application_Type']>option:nth-of-type(2)"));

                //----- Privacy Settings -----//
                action.ICheck(driver, Elements.Forms_Communication_Checkbox);

                //--- Action: Click on submit button ---//
                action.IClick(driver, Elements.Forms_Submit_Button);

                //--- Expected Result: Visual DSP Registration Thank You Page should be displayed ---//
                test.validateStringInstance(driver, driver.Url, thankYou_page_url);
            }
        }
    }
}