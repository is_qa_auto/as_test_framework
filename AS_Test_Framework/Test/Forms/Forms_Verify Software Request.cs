﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using OpenQA.Selenium;
using System;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.Forms
{
    [TestFixture]
    public class Forms_SoftwareRequest : BaseSetUp
    {
        //--- Software Request Form URL: ---//
        //--- DEV: https://form-dev.corpnt.analog.com/Form_Pages/softwaremodules/SRF.aspx ---//
        //--- QA: https://form-qa.corpnt.analog.com/Form_Pages/softwaremodules/SRF.aspx ---//
        //--- PROD: https://form.analog.com/Form_Pages/softwaremodules/SRF.aspx ---//

        public Forms_SoftwareRequest() : base() { }

        //--- Login Credentials ---//
        string username = "f0rms_t3st_555@mailinator.com";
        string password = "Test_1234";

        //--- URLs ---//
        string softwareRequest_form_url = "Form_Pages/softwaremodules/SRF.aspx";
        string login_page_url = "b2clogin";
        string software_page_url = "/software.html";
        string thankYou_page_url = "/software_arf_form_thank_you.html";

        //--- Labels ---//
        string softwareRecipientInformation_txt = "Software Recipient Information";
        string commercialInformation_txt = "Commercial information";
        string softwareRequested_txt = "Software requested";
        string countryRegion_text = "Country/Region";
        string pleaseSelect_txt = "Please Select";
        string pleaseSelectOne_txt = "Please select one";
        string firstName_blankInput_errorMessage = "First name is required";
        string email_blankInput_errorMessage = "Email address is required";
        string companyName_blankInput_errorMessage = "Company Name is required";
        string addressLine_1_blankInput_errorMessage = "Address 1 is required";
        string city_blankInput_errorMessage = "City is required";
        string zipPostalCode_blankInput_errorMessage = "Zip/Postal code is required";
        string lastName_blankInput_errorMessage = "Last name is required";
        string companyWebsite_blankInput_errorMessage = "Organization Website is required";
        string stateProvince_blankInput_errorMessage = "State/Province is required";
        string countryRegion_blankInput_errorMessage = "Country/Region is required";
        string phoneNumber_blankInput_errorMessage = "Phone Number is required";
        string whatMarketsAreYouPrimarilyFocusedOn_blankInput_errorMessage = "Please select up to 2 market areas you are focusing on";
        string whatStageIsYourApplicationIn_blankInput_errorMessage = "Please select the appropriate stage for your application";
        string whenWillYourApplicationBeAvailableInTheMarketplace_blankInput_errorMessage = "Please select when your application will be available";
        string howManyProcessorsAreYouLikelyToOrderOnAnAnnualBasis_blankInput_errorMessage = "Please select the number of DSP's you are likely to order on an annual basis";
        string howConfidentAreYouOfAchievingTheseVolumes_blankInput_errorMessage = "Please select your confidence levels for\r\nachieving these volumes";
        string brieflyDescribeYourApplication_blankInput_errorMessage = "Please provide a brief description of your application";
        string targetHardware_blankInput_errorMessage = "Please select the Target Hardware(Processor)";
        string softwareRequested_blankInput_errorMessage = "Select at least one software requested";
        string email_invalidInput_errorMessage = "Please enter a valid email address";
        string whatMarketsAreYouPrimarilyFocusedOn_invalidInput_errorMessage = "Please select up to 2 market areas you are focusing on";

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Elements are Present and Working as Expected when the User is Logged Out in EN Locale")]
        public void Forms_SoftwareRequest_VerifyElementsWhenUserIsLoggedOut(string Locale)
        {
            //--- Software Request Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + softwareRequest_form_url;

            //--- Action: Go to Software Request form ---//
            action.Navigate(driver, form_url);

            //----- Form TestPlan > EN Tab > R6 > T11: Verify New SRF form to support Secure Software Downloads (SSD) (IQ-6139/AL-13148, IQ-7846/AL-14846) -----//

            //--- Expected Result: The System displays the B2C Login page ---//
            test.validateStringInstance(driver, driver.Url, login_page_url);
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Elements are Present and Working as Expected when the User is Logged In in EN Locale")]
        public void Forms_SoftwareRequest_VerifyElementsWhenUserIsLoggedIn(string Locale)
        {
            //--- Software Request Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + softwareRequest_form_url;

            //--- Action: Go to Software Request form ---//
            action.Navigate(driver, form_url);

            if (driver.Url.Contains(login_page_url))
            {
                //--- Action: Enter Email Address in the 'Email' field ---//
                //--- Action: Enter Password in the 'Password' field ---//
                //--- Action: Click the Login button ---//
                action.ILogin(driver, username, password);
            }

            //----- Header Section -----//

            //----- Form TestPlan > EN Tab > R6 > T11: Verify New SRF form to support Secure Software Downloads (SSD) -----//

            //--- Expected Result: Software Request Form Label should be Present ---//
            test.validateElementIsPresent(driver, Elements.Forms_SoftwareRequest_Label);

            if (util.CheckElement(driver, Elements.Forms_VisitTheAdiSoftwarePortal_Link, 2))
            {
                //--- Action: Click the Visit the ADI Software Portal hyperlink ---//
                action.IOpenLinkInNewTab(driver, Elements.Forms_VisitTheAdiSoftwarePortal_Link);
                Thread.Sleep(5000);

                //--- Expected Result: sThe link should redirect to the Software page ---//
                test.validateStringInstance(driver, driver.Url, software_page_url);

                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles.First());
            }

            //----- Software Recipient Information -----//

            //----- Form TestPlan > EN Tab > R6 > T2: Verify Software Recipient Information section (AL-8834) -----//

            //--- Expected Result: Title should be "Software Recipient Information" ---//
            test.validateStringIsCorrect(driver, Elements.Forms_SoftwareRecipientInformation_Label, softwareRecipientInformation_txt);

            //----- Form TestPlan > EN Tab > R6 > T11: Verify New SRF form to support Secure Software Downloads (SSD) -----//

            //--- Expected Result: First Name should be Present ---//
            test.validateElementIsPresent(driver, Elements.Forms_FirstName_InputBox);

            //--- Expected Result: Email should be Present ---//
            test.validateElementIsPresent(driver, Elements.Forms_Email_InputBox);

            //--- Expected Result: Company Name should be Present ---//
            test.validateElementIsPresent(driver, Elements.Forms_CompanyName_InputBox);

            //--- Expected Result: Address Line1 should be Present ---//
            test.validateElementIsPresent(driver, Elements.Forms_SoftwareRequest_AddressLine_1_InputBox);

            //--- Expected Result: City should be Present ---//
            test.validateElementIsPresent(driver, Elements.Forms_City_InputBox);

            //--- Expected Result: Zip/Postal Code should be Present ---//
            test.validateElementIsPresent(driver, Elements.Forms_ZipPostalCode_InputBox);

            //--- Expected Result: Last Name should be Present ---//
            test.validateElementIsPresent(driver, Elements.Forms_LastName_InputBox);

            //--- Expected Result: Job title should be Present ---//
            test.validateElementIsPresent(driver, Elements.Forms_JobTitle_InputBox);

            //--- Expected Result: Company Website should be Present ---//
            test.validateElementIsPresent(driver, Elements.Forms_CompanyWebsite_InputBox);

            //--- Expected Result: Address Line 2 should be Present ---//
            test.validateElementIsPresent(driver, Elements.Forms_SoftwareRequest_AddressLine_2_InputBox);

            //--- Expected Result: Phone number should be Present ---//
            test.validateElementIsPresent(driver, Elements.Forms_PhoneNumber_InputBox);

            //----- Form TestPlan > EN Tab > R40 > T1: Verify all labels for Country is changed to "Country/Region" (IQ-4970/AL-12466) -----//

            //--- Expected Result: The Country label has been changed to Country/Region ---//
            test.validateStringInstance(driver, util.GetText(driver, Elements.Forms_Country_Label), countryRegion_text);

            //----- Form TestPlan > EN Tab > R6 > T2: Verify Software Recipient Information section (AL-8834) -----//

            //--- Expected Result: The default value should be the State/Province of user's account ---//
            test.validateStringChanged(driver, util.GetSelectedDropdownValue(driver, Elements.Forms_StateProvince_Dropdown), "Select State/Province");

            //--- Expected Result: The default value should be the Country of user's account ---//
            test.validateStringChanged(driver, util.GetSelectedDropdownValue(driver, Elements.Forms_CountryRegion_Dropdown), "Select Country/Region");

            //----- Commercial information -----//

            //----- Form TestPlan > EN Tab > R6 > T5: Verify "Commercial information" section (AL-8834) -----//

            //--- Expected Result: Scetion title should be "Commercial information " ---//
            test.validateStringIsCorrect(driver, Elements.Forms_CommercialInformation_Label, commercialInformation_txt);

            //----- Form TestPlan > EN Tab > R6 > T11: Verify New SRF form to support Secure Software Downloads (SSD) -----//

            //--- Expected Result: ADI Sales Representative Email Address should be Present ---//
            test.validateElementIsPresent(driver, Elements.Forms_AdiSalesRepresentative_InputBox);

            //--- Expected Result: What markets are you primarily focused on (select up to two)? should be Present ---//
            test.validateElementIsPresent(driver, Elements.Forms_WhatMarketsAreYouPrimarilyFocusedOn_Checkboxes);

            //--- Expected Result: What stage is your application in? should be Present ---//
            test.validateElementIsPresent(driver, Elements.Forms_WhatStageIsYourApplicationIn_Dropdown);
            if (util.CheckElement(driver, Elements.Forms_WhatStageIsYourApplicationIn_Dropdown, 2))
            {
                //----- Form TestPlan > EN Tab > R6 > T5: Verify "Commercial information" section (AL-8834) -----//

                //--- Expected Result: The default value on all dropdowns should be "Please Select" ---//
                test.validateSelectedValueIsCorrect(driver, Elements.Forms_WhatStageIsYourApplicationIn_Dropdown, pleaseSelect_txt);
            }

            //--- Expected Result: When will your application be available in the marketplace? should be Present ---//
            test.validateElementIsPresent(driver, Elements.Forms_WhenWillYourApplicationBeAvailableInTheMarketplace_Dropdown);
            if (util.CheckElement(driver, Elements.Forms_WhenWillYourApplicationBeAvailableInTheMarketplace_Dropdown, 2))
            {
                //----- Form TestPlan > EN Tab > R6 > T5: Verify "Commercial information" section (AL-8834) -----//

                //--- Expected Result: The default value on all dropdowns should be "Please Select" ---//
                test.validateSelectedValueIsCorrect(driver, Elements.Forms_WhenWillYourApplicationBeAvailableInTheMarketplace_Dropdown, pleaseSelect_txt);
            }

            //--- Expected Result: How many processors are you likely to order on an annual basis?should be Present ---//
            test.validateElementIsPresent(driver, Elements.Forms_HowManyProcessorsAreYouLikelyToOrderOnAnAnnualBasis_Dropdown);
            if (util.CheckElement(driver, Elements.Forms_HowManyProcessorsAreYouLikelyToOrderOnAnAnnualBasis_Dropdown, 2))
            {
                //----- Form TestPlan > EN Tab > R6 > T5: Verify "Commercial information" section (AL-8834) -----//

                //--- Expected Result: The default value on all dropdowns should be "Please Select" ---//
                test.validateSelectedValueIsCorrect(driver, Elements.Forms_HowManyProcessorsAreYouLikelyToOrderOnAnAnnualBasis_Dropdown, pleaseSelect_txt);
            }

            //--- Expected Result: How confident are you of achieving these volumes? should be Present ---//
            test.validateElementIsPresent(driver, Elements.Forms_HowConfidentAreYouOfAchievingTheseVolumes_Dropdown);
            if (util.CheckElement(driver, Elements.Forms_HowConfidentAreYouOfAchievingTheseVolumes_Dropdown, 2))
            {
                //----- Form TestPlan > EN Tab > R6 > T5: Verify "Commercial information" section (AL-8834) -----//

                //--- Expected Result: The default value on all dropdowns should be "Please Select" ---//
                test.validateSelectedValueIsCorrect(driver, Elements.Forms_HowConfidentAreYouOfAchievingTheseVolumes_Dropdown, pleaseSelect_txt);
            }

            //--- Expected Result: Briefly describe your application fields should be Present ---//
            test.validateElementIsPresent(driver, Elements.Forms_BrieflyDescribeYourApplication_TextArea);

            //----- Software requested -----//

            //----- Form TestPlan > EN Tab > R6 > T6: Verify the Software requested section (AL-8834) -----//

            //--- Expected Result: Section title sould be "Software requested" ---//
            test.validateStringIsCorrect(driver, Elements.Forms_SoftwareRequested_Label, softwareRequested_txt);

            //----- Form TestPlan > EN Tab > R6 > T11: Verify New SRF form to support Secure Software Downloads (SSD) -----//

            //--- Expected Result: Description is displayed above the Software requested section ---//
            test.validateElementIsPresent(driver, Elements.Forms_SoftwareRequested_Section_Description_Text);

            //--- Expected Result: Target hardware dropdown should be Present ---//
            test.validateElementIsPresent(driver, Elements.Forms_TargetHardware_Dropdown);
            if (util.CheckElement(driver, Elements.Forms_TargetHardware_Dropdown, 2))
            {
                //----- Form TestPlan > EN Tab > R6 > T6: Verify the Software requested section (AL-8834) -----//

                //--- Expected Result: Default value of the Target Hardware field should be "Please select one" ---//
                test.validateSelectedValueIsCorrect(driver, Elements.Forms_TargetHardware_Dropdown, pleaseSelectOne_txt);
            }

            //----- Form TestPlan > EN Tab > R6 > T6: Verify the Software requested section (AL-8834) -----//

            //--- Expected Result: The text should be present under Do you require Evaluation or Production Code at this stage? Field ---//
            test.validateElementIsPresent(driver, Elements.Forms_DoYouRequireEvaluationOrProductionCodeAtThisStage_Note);

            //----- Privacy Settings -----//

            //----- Form TestPlan > EN Tab > R38 > T1: Verify Privacy Settings in All Forms [account is logged in] (AL-11345) -----//

            //--- Expected Result: Prompt "You can change your privacy settings at any time by clicking on the unsubscribe link in emails sent from Analog Devices or in Analog’s Privacy Settings." is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_LegalDisclaimer_Text);

            //----- Form TestPlan > EN Tab > R6 > T11: Verify New SRF form to support Secure Software Downloads (SSD) -----//

            //--- Expected Result: You can change your privacy settings at any time by clicking on the unsubscribe link in emails sent from Analog Devices or in Analog’s Privacy Settings. should be Present ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_Description_Text);

            //--- Expected Result: Privacy Settings should be Present ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_Link);

            //--- Expected Result: Privacy & Security Statement should be Present ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacyAndSecurityStatement_Link);

            //--- Expected Result: Authorized Partners should be Present ---//
            test.validateElementIsPresent(driver, Elements.Forms_AuthorizedPartners_Link);
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Input Validations are Working as Expected for Valid Inputs in EN Locale")]
        public void Forms_SoftwareRequest_VerifyInputValidationsForValidInputs(string Locale)
        {
            //--- Software Request Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + softwareRequest_form_url;

            //--- Action: Go to Software Request form ---//
            action.Navigate(driver, form_url);

            if (driver.Url.Contains(login_page_url))
            {
                //--- Action: Enter Email Address in the 'Email' field ---//
                //--- Action: Enter Password in the 'Password' field ---//
                //--- Action: Click the Login button ---//
                action.ILogin(driver, username, password);
            }

            //----- Software requested -----//

            //----- Form TestPlan > EN Tab > R6 > T11: Verify New SRF form to support Secure Software Downloads (SSD) -----//

            //--- Action: Select Target hardware dropdown ---//
            action.IClick(driver, Elements.Forms_TargetHardware_Dropdown);
            int targetHardware_values_count = util.GetCount(driver, By.CssSelector("select[id*='TargetHardware']>option"));
            Random rand = new Random();
            int random_number = rand.Next(2, targetHardware_values_count);
            action.IClick(driver, By.CssSelector("select[id*='TargetHardware']>option:nth-of-type(" + random_number + ")"));
            Thread.Sleep(5000);

            //--- Expected Result: Software requested checkboxes - will be displayed after selecting a value from Target hardware dropdown ---//
            test.validateElementIsPresent(driver, Elements.Forms_SoftwareRequest_Checkboxes);
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Input Validations are Working as Expected for Invalid Inputs in EN Locale")]
        public void Forms_SoftwareRequest_VerifyInputValidationsForInvalidInputs(string Locale)
        {
            //--- Software Request Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + softwareRequest_form_url;

            //--- Action: Go to Software Request form ---//
            action.Navigate(driver, form_url);

            if (driver.Url.Contains(login_page_url))
            {
                //--- Action: Enter Email Address in the 'Email' field ---//
                //--- Action: Enter Password in the 'Password' field ---//
                //--- Action: Click the Login button ---//
                action.ILogin(driver, username, password);
            }

            //----- Software Recipient Information -----//

            //----- Form TestPlan > EN Tab > R6 > T8_1: Submit the Software Request  Form  with invalid data -----//

            //--- Action: Enter invalid data on the Email Address field. (e.g. 123!@#$%@yahoo.com) ---//
            string email_input = "123!@#$%@yahoo.com";
            action.IDeleteValueOnFields(driver, Elements.Forms_Email_InputBox);
            action.IType(driver, Elements.Forms_Email_InputBox, email_input);
            action.IType(driver, Elements.Forms_Email_InputBox, Keys.Tab);

            //--- Expected Result: The error message below should be displayed: "Please enter a valid email address" ---//
            test.validateStringIsCorrect(driver, Elements.Forms_SoftwareRequest_Email_InvalidInput_ErrorMessage, email_invalidInput_errorMessage);

            //----- Commercial information -----//

            //----- Form TestPlan > EN Tab > R6 > T5: Verify "Commercial information" section (AL-8834) -----//

            //--- Action: Select more that 2 markets ---//
            action.ICheck(driver, By.CssSelector("table[id$='MarketsPrimarilyFocussedOn']>tbody>tr:nth-of-type(1)>td:nth-of-type(2)"));
            action.ICheck(driver, By.CssSelector("table[id$='MarketsPrimarilyFocussedOn']>tbody>tr:nth-of-type(2)>td:nth-of-type(1)"));
            action.ICheck(driver, By.CssSelector("table[id$='MarketsPrimarilyFocussedOn']>tbody>tr:nth-of-type(2)>td:nth-of-type(2)"));

            //--- Action: Click Submit button ---//
            action.IClick(driver, Elements.Forms_Submit_Button);

            //--- Expected Result: An error message should appear: "Please select upto 2 market areas you are focussing on" ---//
            test.validateStringIsCorrect(driver, Elements.Forms_SoftwareRequest_WhatMarketsAreYouPrimarilyFocusedOn_InvalidInput_ErrorMessage, whatMarketsAreYouPrimarilyFocusedOn_invalidInput_errorMessage);
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Input Validations are Working as Expected for Blank Inputs in EN Locale")]
        public void Forms_SoftwareRequest_VerifyInputValidationsForBlankInputs(string Locale)
        {
            //--- Software Request Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + softwareRequest_form_url;

            //--- Action: Go to Software Request form ---//
            action.Navigate(driver, form_url);

            if (driver.Url.Contains(login_page_url))
            {
                //--- Action: Enter Email Address in the 'Email' field ---//
                //--- Action: Enter Password in the 'Password' field ---//
                //--- Action: Click the Login button ---//
                action.ILogin(driver, username, password);
                action.GivenIAcceptCookies(driver);

                //----- Software Recipient Information -----//
                action.IDeleteValueOnFields(driver, Elements.Forms_FirstName_InputBox);
                action.IType(driver, Elements.Forms_FirstName_InputBox, Keys.Tab);
                action.IDeleteValueOnFields(driver, Elements.Forms_Email_InputBox);
                action.IType(driver, Elements.Forms_Email_InputBox, Keys.Tab);
                action.IDeleteValueOnFields(driver, Elements.Forms_CompanyName_InputBox);
                action.IType(driver, Elements.Forms_CompanyName_InputBox, Keys.Tab);
                action.IDeleteValueOnFields(driver, Elements.Forms_SoftwareRequest_AddressLine_1_InputBox);
                action.IType(driver, Elements.Forms_SoftwareRequest_AddressLine_1_InputBox, Keys.Tab);
                action.IDeleteValueOnFields(driver, Elements.Forms_City_InputBox);
                action.IType(driver, Elements.Forms_City_InputBox, Keys.Tab);
                action.IDeleteValueOnFields(driver, Elements.Forms_ZipPostalCode_InputBox);
                action.IType(driver, Elements.Forms_ZipPostalCode_InputBox, Keys.Tab);

                action.IDeleteValueOnFields(driver, Elements.Forms_LastName_InputBox);
                action.IType(driver, Elements.Forms_LastName_InputBox, Keys.Tab);
                action.IDeleteValueOnFields(driver, Elements.Forms_CompanyWebsite_InputBox);
                action.IType(driver, Elements.Forms_CompanyWebsite_InputBox, Keys.Tab);
                Thread.Sleep(1000);
                action.IClick(driver, Elements.Forms_StateProvince_Dropdown);
                action.IClick(driver, By.CssSelector("select[id$='tate']>option:nth-of-type(1)"));
                action.IClick(driver, Elements.Forms_CountryRegion_Dropdown);
                action.IClick(driver, By.CssSelector("select[id$='country']>option:nth-of-type(1)"));
                action.IDeleteValueOnFields(driver, Elements.Forms_PhoneNumber_InputBox);
                action.IType(driver, Elements.Forms_PhoneNumber_InputBox, Keys.Tab);
            }

            //----- Form TestPlan > EN Tab > R6 > T7_2: Submit Software Request Form with no data on mandatory fields  -----//

            //--- Action: Click on the Submit button without entering data on the mandatory fields. ---//
            action.IClick(driver, Elements.Forms_Submit_Button);

            //----- Software Recipient Information -----//

            //--- Expected Result: The First Name : "First name is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_SoftwareRequest_FirstName_BlankInput_ErrorMessage, firstName_blankInput_errorMessage);

            //--- Expected Result: The Email: "Email address is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_SoftwareRequest_Email_BlankInput_ErrorMessage, email_blankInput_errorMessage);

            //--- Expected Result: The Full Company Name(include Inc. or Ltd.,etc.) : "Full Company Name is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_SoftwareRequest_CompanyName_BlankInput_ErrorMessage, companyName_blankInput_errorMessage);

            //--- Expected Result: The Address Line 1 : "Address Line 1 is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_SoftwareRequest_AddressLine_1_BlankInput_ErrorMessage, addressLine_1_blankInput_errorMessage);

            //--- Expected Result: The City : "City is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_SoftwareRequest_City_BlankInput_ErrorMessage, city_blankInput_errorMessage);

            //--- Expected Result: The  Zip/Postal Code:  "Zip/Postal Code is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_SoftwareRequest_ZipPostalCode_BlankInput_ErrorMessage, zipPostalCode_blankInput_errorMessage);

            //--- Expected Result: The Last Name: "Last name is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_SoftwareRequest_LastName_BlankInput_ErrorMessage, lastName_blankInput_errorMessage);

            //--- Expected Result: The Company Website: "Organization Website is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_SoftwareRequest_CompanyWebsite_BlankInput_ErrorMessage, companyWebsite_blankInput_errorMessage);

            //--- Expected Result: The State/Province : "State/Province is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_StateProvince_BlankInput_ErrorMessage, stateProvince_blankInput_errorMessage);

            //--- Expected Result: The Country(dropdown): "Country is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_CountryRegion_BlankInput_ErrorMessage, countryRegion_blankInput_errorMessage);

            //--- Expected Result: The  Phone Number : "Phone Number is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_SoftwareRequest_PhoneNumber_BlankInput_ErrorMessage, phoneNumber_blankInput_errorMessage);

            //----- Commercial information -----//

            //--- Expected Result: The What markets are you primarily focused on (select up to two)?: "Please select up to 2 market areas you are focusing on" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_SoftwareRequest_WhatMarketsAreYouPrimarilyFocusedOn_BlankInput_ErrorMessage, whatMarketsAreYouPrimarilyFocusedOn_blankInput_errorMessage);

            //--- Expected Result: The What stage is your application in?: "Please select the appropriate stage for your application" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_SoftwareRequest_WhatStageIsYourApplicationIn_BlankInput_ErrorMessage, whatStageIsYourApplicationIn_blankInput_errorMessage);

            //--- Expected Result: The When will your application be available in the marketplace?: "Please select when your application will be available" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_SoftwareRequest_WhenWillYourApplicationBeAvailableInTheMarketplace_BlankInput_ErrorMessage, whenWillYourApplicationBeAvailableInTheMarketplace_blankInput_errorMessage);

            //--- Expected Result: The How many processors are you likely to order on an annual basis?: "Please select the number of DSP's you are likely to order on an annual basis" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_SoftwareRequest_HowManyProcessorsAreYouLikelyToOrderOnAnAnnualBasis_BlankInput_ErrorMessage, howManyProcessorsAreYouLikelyToOrderOnAnAnnualBasis_blankInput_errorMessage);

            //--- Expected Result: The How confident are you of achieving these volumes?: "Please select your confidence levels for achieving these volumes" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_SoftwareRequest_HowConfidentAreYouOfAchievingTheseVolumes_BlankInput_ErrorMessage, howConfidentAreYouOfAchievingTheseVolumes_blankInput_errorMessage);

            //--- Expected Result: The "Briefly describe your application: Please provide a brief description of your application" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_SoftwareRequest_BrieflyDescribeYourApplication_BlankInput_ErrorMessage, brieflyDescribeYourApplication_blankInput_errorMessage);

            //----- Software requested -----//

            //--- Expected Result: The Target hardware: "Please select the Target Hardware(Processor)" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_SoftwareRequest_TargetHardware_BlankInput_ErrorMessage, targetHardware_blankInput_errorMessage);

            //----- Form TestPlan > EN Tab > R6 > T6: Verify the Software requested section (AL-8834) -----//

            action.IClick(driver, Elements.Forms_TargetHardware_Dropdown);
            action.IClick(driver, By.CssSelector("select[id*='TargetHardware']>option:nth-of-type(2)"));
            Thread.Sleep(1000);

            action.IClick(driver, Elements.Forms_Submit_Button);
            Thread.Sleep(3000);

            //--- Expected Result: The error message should display as below Software requested: "Select at least one software requested"---//
            test.validateStringIsCorrect(driver, Elements.Forms_SoftwareRequested_BlankInput_ErrorMessage, softwareRequested_blankInput_errorMessage);
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Form Submission is Working as Expected in EN Locale")]
        public void Forms_SoftwareRequest_VerifyFormSubmission(string Locale)
        {
            if (Util.Configuration.Environment.Equals("dev") || Util.Configuration.Environment.Equals("qa"))
            {
                //--- Software Request Form URL ---//
                string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + softwareRequest_form_url;

                //--- Action: Go to Software Request form ---//
                action.Navigate(driver, form_url);

                if (driver.Url.Contains(login_page_url))
                {
                    //--- Action: Enter Email Address in the 'Email' field ---//
                    //--- Action: Enter Password in the 'Password' field ---//
                    //--- Action: Click the Login button ---//
                    action.ILogin(driver, username, password);
                }

                //----- Form TestPlan > EN Tab > R6 > T13: Verify Email Address in SRF Email (Email is changed) (IQ-8843/AL-15538) -----//

                //--- Action: Change the Email Address ---//
                string email_input = "t3st_" + Util.Configuration.Environment.ToLower() + "_softwareRequestForm_f0rm_" + util.GenerateRandomNumber() + "@mailinator.com";
                //Console.WriteLine("Email Input = " + email_input);
                action.IDeleteValueOnFields(driver, Elements.Forms_Email_InputBox);
                action.IType(driver, Elements.Forms_Email_InputBox, email_input);

                //----- Form TestPlan > EN Tab > R6 > T8_2: Submit Software Request  form with valid data -----//

                //--- Action: Enter valid data on the mandatory fields. ---//

                //----- Software Recipient Information -----//
                string firstName_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_FirstName_InputBox);
                action.IType(driver, Elements.Forms_FirstName_InputBox, firstName_input);

                string companyName_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_CompanyName_InputBox);
                action.IType(driver, Elements.Forms_CompanyName_InputBox, companyName_input);

                string addressLine_1_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_SoftwareRequest_AddressLine_1_InputBox);
                action.IType(driver, Elements.Forms_SoftwareRequest_AddressLine_1_InputBox, addressLine_1_input);

                string city_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_City_InputBox);
                action.IType(driver, Elements.Forms_City_InputBox, city_input);

                string zipPostalCode_input = "12345";
                action.IDeleteValueOnFields(driver, Elements.Forms_ZipPostalCode_InputBox);
                action.IType(driver, Elements.Forms_ZipPostalCode_InputBox, zipPostalCode_input);

                string lastName_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_LastName_InputBox);
                action.IType(driver, Elements.Forms_LastName_InputBox, lastName_input);

                string companyWebsite_input = "www.test.com";
                action.IDeleteValueOnFields(driver, Elements.Forms_CompanyWebsite_InputBox);
                action.IType(driver, Elements.Forms_CompanyWebsite_InputBox, companyWebsite_input);

                string countryRegion_input = "US";
                action.ISelectFromDropdown(driver, Elements.Forms_CountryRegion_Dropdown, countryRegion_input);

                action.IClick(driver, Elements.Forms_StateProvince_Dropdown);
                action.IClick(driver, By.CssSelector("select[id$='tate']>option:nth-of-type(3)"));

                string phoneNumber_input = "123456789";
                action.IDeleteValueOnFields(driver, Elements.Forms_PhoneNumber_InputBox);
                action.IType(driver, Elements.Forms_PhoneNumber_InputBox, phoneNumber_input);

                //----- Commercial information -----//
                action.ICheck(driver, By.CssSelector("table[id$='MarketsPrimarilyFocussedOn']>tbody>tr:nth-of-type(1)>td:nth-of-type(2)"));
                action.ICheck(driver, By.CssSelector("table[id$='MarketsPrimarilyFocussedOn']>tbody>tr:nth-of-type(2)>td:nth-of-type(1)"));

                action.IClick(driver, Elements.Forms_WhatStageIsYourApplicationIn_Dropdown);
                action.IClick(driver, By.CssSelector("select[id$='WhatStageIsYourApplicationIn']>option:nth-of-type(2)"));

                action.IClick(driver, Elements.Forms_WhenWillYourApplicationBeAvailableInTheMarketplace_Dropdown);
                action.IClick(driver, By.CssSelector("select[id$='WhenWillYourApplicationBeAvailable']>option:nth-of-type(2)"));

                action.IClick(driver, Elements.Forms_HowManyProcessorsAreYouLikelyToOrderOnAnAnnualBasis_Dropdown);
                action.IClick(driver, By.CssSelector("select[id$='HowManyDSPsAreYouLikelyToOrder']>option:nth-of-type(2)"));

                action.IClick(driver, Elements.Forms_HowConfidentAreYouOfAchievingTheseVolumes_Dropdown);
                action.IClick(driver, By.CssSelector("select[id$='HowConfidentAreYouAchievingVolumes']>option:nth-of-type(2)"));

                string brieflyDescribeYourApplication_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_BrieflyDescribeYourApplication_TextArea);
                action.IType(driver, Elements.Forms_BrieflyDescribeYourApplication_TextArea, brieflyDescribeYourApplication_input);

                //----- Software requested -----//
                action.IClick(driver, Elements.Forms_TargetHardware_Dropdown);
                action.IClick(driver, By.CssSelector("select[id*='TargetHardware']>option:nth-of-type(2)"));

                action.IClick(driver, Elements.Forms_ProcessorSoc_Dropdown);
                action.IClick(driver, By.CssSelector("select[id$='ProductNo']>option:nth-of-type(2)"));

                if (driver.FindElement(Elements.Forms_HardwarePlatform_Dropdown).Enabled)
                {
                    action.IClick(driver, Elements.Forms_HardwarePlatform_Dropdown);
                    action.IClick(driver, By.CssSelector("select[id$='HardwarePlatformDropDown']>option:nth-of-type(2)"));
                }

                action.ICheck(driver, Elements.Forms_SoftwareRequest_Checkboxes);

                string additionalCommentsOrInstructions_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_Comments_TextArea);
                action.IType(driver, Elements.Forms_Comments_TextArea, additionalCommentsOrInstructions_input);

                //----- Privacy Settings -----//
                action.ICheck(driver, Elements.Forms_Communication_Checkbox);

                //--- Action: Click on submit button ---//
                action.IClick(driver, Elements.Forms_Submit_Button);
                Thread.Sleep(6000);

                //--- Expected Result: It should successfully redirect on Software Request Form Thank You page ---//
                test.validateStringInstance(driver, driver.Url, thankYou_page_url);

                if (driver.Url.Contains(thankYou_page_url))
                {
                    action.ICheckMailinatorEmail(driver, email_input);

                    //--- Expected Result: The email from software.module.request@analog.com should be displayed ---//
                    test.validateStringInstance(driver, util.GetText(driver, Elements.Mailinator_LatestEmail_Sender), "software.module.request@analog.");
                }
            }
        }
    }
}