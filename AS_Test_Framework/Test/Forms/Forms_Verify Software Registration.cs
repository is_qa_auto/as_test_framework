﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using OpenQA.Selenium;
using System;
using System.Threading;

namespace AS_Test_Framework.Forms
{
    [TestFixture]
    public class Forms_SoftwareRegistration : BaseSetUp
    {
        //--- Software Registration Form URL: ---//
        //--- DEV: https://form-dev.corpnt.analog.com/form_pages/processors/swRegistration.aspx ---//
        //--- QA: https://form-qa.corpnt.analog.com/form_pages/processors/swRegistration.aspx ---//
        //--- PROD: https://form.analog.com/form_pages/processors/swRegistration.aspx ---//

        public Forms_SoftwareRegistration() : base() { }

        //--- Login Credentials ---//
        string username = "f0rms_t3st_555@mailinator.com";
        string password = "Test_1234";

        //--- URLs ---//
        string softwareRegistration_form_url = "form_pages/processors/swRegistration.aspx";
        string thankYouForRegistering_page_url = "/crosscore_thankyou.html";

        //--- Labels ---//
        string countryRegion_text = "Country/Region";
        string emailId_blankInput_errorMessage = "Email address is required";
        string firstName_blankInput_errorMessage = "First Name is required";
        string lastName_blankInput_errorMessage = "Last Name is required";
        string organizationName_blankInput_errorMessage = "Organization name is required";
        string addressLine_1_blankInput_errorMessage = "Address Line 1 is required";
        string telephone_blankInput_errorMessage = "Telephone is required";
        string city_blankInput_errorMessage = "City is required";
        string stateProvince_blankInput_errorMessage = "State/Province is required";
        string zipPostalCode_blankInput_errorMessage = "Zip/Postal Code is required";
        string countryRegion_blankInput_errorMessage = "Country/Region is required";
        string pleaseProvideYourSerialNumberExactlyAsItAppearsOnTheMediaThatYouReceived_blankInput_errorMessage = "Please provide the Serial #";
        string hostId_blankInput_errorMessage = "Please provide a Host(Machine) ID";
        string ifOutsideTheUsOrCanadaPleaseEnterTheRegion_blankInput_errorMessage = "Please provide other State/Province";
        string emailId_invalidInput_errorMessage = "Please enter a valid email address";

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Elements are Present and Working as Expected when the User is Logged Out in EN Locale")]
        public void Forms_SoftwareRegistration_VerifyElementsWhenUserIsLoggedOut(string Locale)
        {
            //--- Software Registration Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + softwareRegistration_form_url;

            //--- Action: Navigate to the given URL ---//
            action.Navigate(driver, form_url);

            //----- Form TestPlan > EN Tab > R40 > T1: Verify all labels for Country is changed to "Country/Region" (IQ-4970/AL-12466) -----//

            //----- Your detailed information -----//

            //--- Expected Result: The Country label has been changed to Country/Region ---//
            test.validateStringInstance(driver, util.GetText(driver, Elements.Forms_SoftwareRegistration_Country_Label), countryRegion_text);

            //----- Form TestPlan > EN Tab > R38 > T1: Verify Privacy Settings in All Forms [account is not logged in] (AL-11345) -----//

            //----- Privacy Settings -----//

            //--- Expected Result: Privacy Settings section is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_Section);

            //--- Expected Result: Prompt "You can change your privacy settings at any time by clicking on the unsubscribe link in emails sent from Analog Devices or in Analog’s Privacy Settings." is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_LegalDisclaimer_Text);

            //--- Expected Result: Privacy Settings is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_Link);

            //--- Expected Result: Privacy & Security Statement ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacyAndSecurityStatement_Link);

            //--- Expected Result: Authorized Partners ---//
            test.validateElementIsPresent(driver, Elements.Forms_AuthorizedPartners_Link);

            //----- Form TestPlan > EN Tab > R26 > T3: Login using Save time to login section with valid data -----//

            //----- Save time by logging in or registering for myAnalog -----//

            if (!util.CheckElement(driver, Elements.Forms_Expanded_SaveTimeByLoggingInOrRegisteringForMyAnalog_Panel, 1))
            {
                //--- Action: Click on Save time by logging in or registering for myAnalog   link ---//
                action.IClick(driver, Elements.Forms_SaveTimeByLoggingInOrRegisteringForMyAnalog_Link);
            }

            //--- Expected Result: Save time to login section should be displayed ---//
            test.validateElementIsPresent(driver, Elements.Forms_Expanded_SaveTimeByLoggingInOrRegisteringForMyAnalog_Panel);
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Elements are Present and Working as Expected when the User is Logged In in EN Locale")]
        public void Forms_SoftwareRegistration_VerifyElementsWhenUserIsLoggedIn(string Locale)
        {
            //--- Software Registration Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + softwareRegistration_form_url;

            //--- Action: Navigate to the given URL ---//
            action.Navigate(driver, form_url);

            //----- Form TestPlan > EN Tab > R26 > T3: Login using Save time to login section with valid data -----//

            //----- Save time by logging in or registering for myAnalog -----//

            //--- Action: Login using valid data ---//
            action.ILogInViaSaveTimeByLoggingInPanel(driver, username, password);
            Thread.Sleep(1000);

            driver.Navigate().Refresh();
            Thread.Sleep(1000);

            //----- About you -----//

            //--- Expected Result: The Email Id field should be  automatically populated ---//
            test.validateFieldIsNotEmpty(driver, Elements.Forms_EmailId_InputBox);

            //--- Expected Result: The FirstName field should be  automatically populated ---//
            test.validateFieldIsNotEmpty(driver, Elements.Forms_FirstName_InputBox);

            //--- Expected Result: The Last Name field should be  automatically populated ---//
            test.validateFieldIsNotEmpty(driver, Elements.Forms_LastName_InputBox);

            //----- Your detailed contact information -----//

            //--- Expected Result: The Organization Name field should be  automatically populated ---//
            test.validateFieldIsNotEmpty(driver, Elements.Forms_OrganizationName_InputBox);

            //--- Expected Result: The State/Province field should be  automatically populated ---//
            test.validateStringChanged(driver, util.GetSelectedDropdownValue(driver, Elements.Forms_StateProvince_Dropdown), "Select State/Province");

            //--- Expected Result: The Zip/Postal Code field should be  automatically populated ---//
            test.validateFieldIsNotEmpty(driver, Elements.Forms_ZipPostalCode_InputBox);

            //--- Expected Result: The Country field should be  automatically populated ---//
            test.validateStringChanged(driver, util.GetSelectedDropdownValue(driver, Elements.Forms_CountryRegion_Dropdown), "Select Country/Region");

            //----- Form TestPlan > EN Tab > R38 > T1: Verify Privacy Settings in All Forms [account is logged in] (AL-11345) -----//

            //----- Privacy Settings -----//

            //--- Expected Result: Prompt "You can change your privacy settings at any time by clicking on the unsubscribe link in emails sent from Analog Devices or in Analog’s Privacy Settings." is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_LegalDisclaimer_Text);

            //--- Expected Result: Privacy Settings is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_Link);

            //--- Expected Result: Privacy & Security Statement ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacyAndSecurityStatement_Link);

            //--- Expected Result: Authorized Partners ---//
            test.validateElementIsPresent(driver, Elements.Forms_AuthorizedPartners_Link);
        }

        [TestCase("en", TestName = "Verify that the Input Validations are Working as Expected for Invalid Inputs in EN Locale")]
        public void Forms_SoftwareRegistration_VerifyInputValidationsForInvalidInputs(string Locale)
        {
            //--- Software Registration Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + softwareRegistration_form_url;

            //--- Action: Navigate to the given URL ---//
            action.Navigate(driver, form_url);

            //----- Form TestPlan > EN Tab > R26 > T4_2: Submit the Software Registration Form    with invalid data -----//

            //----- About you -----//

            //--- Action: Enter invalid data on the Email Address field. (e.g. 123!@#$%@yahoo.com) ---//
            string email_input = "123!@#$%@yahoo.com";
            action.IDeleteValueOnFields(driver, Elements.Forms_Email_InputBox);
            action.IType(driver, Elements.Forms_Email_InputBox, email_input);
            action.IType(driver, Elements.Forms_Email_InputBox, Keys.Tab);

            //--- Expected Result: The error message should be displayed: "Please enter a valid email address" ---//
            test.validateStringIsCorrect(driver, Elements.Forms_SoftwareRegistration_EmailId_InvalidInput_ErrorMessage, emailId_invalidInput_errorMessage);

            //----- Form TestPlan > EN Tab > R26 > T4_3: Verify Software Registration Form error messages (AL-2631) -----//

            //----- Your detailed contact information -----//

            //--- Action: On State/Province dropdown select value "Outside the US, Mexico, or Canada" ---//
            action.IClick(driver, Elements.Forms_StateProvince_Dropdown);
            action.IClick(driver, By.CssSelector("select[id$='tate']>option:nth-of-type(2)"));

            //--- Action: Leave the "If Outside the US or Canada, please enter the region" empty ---//
            action.IDeleteValueOnFields(driver, Elements.Forms_IfOutsideTheUsOrCanadaPleaseEnterTheRegion_InputBox);
            action.IType(driver, Elements.Forms_IfOutsideTheUsOrCanadaPleaseEnterTheRegion_InputBox, Keys.Tab);

            //--- Action: Submit the form ---//
            action.IClick(driver, Elements.Forms_Submit_Button);

            //--- Expected Result: The error message should be displayed: "Please provide other state/Province".---//
            test.validateStringIsCorrect(driver, Elements.Forms_IfOutsideTheUsOrCanadaPleaseEnterTheRegion_BlankInput_ErrorMessage, ifOutsideTheUsOrCanadaPleaseEnterTheRegion_blankInput_errorMessage);
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Input Validations are Working as Expected for Blank Inputs in EN Locale")]
        public void Forms_SoftwareRegistration_VerifyInputValidationsForBlankInputs(string Locale)
        {
            //--- Software Registration Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + softwareRegistration_form_url;

            //--- Action: Navigate to the given URL ---//
            action.Navigate(driver, form_url);

            //----- Form TestPlan > EN Tab > R26 > T4_1: Submit Software Registration Form with no data on mandatory fields -----//

            //--- Action: Click on the Submit button without entering data on the mandatory fields. ---//
            action.IClick(driver, Elements.Forms_Submit_Button);

            //----- About you -----//

            //--- Expected Result: The Email Id: "Email address is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_SoftwareRegistration_EmailId_BlankInput_ErrorMessage, emailId_blankInput_errorMessage);

            //--- Expected Result: The FirstName: "First Name is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_FirstName_BlankInput_ErrorMessage, firstName_blankInput_errorMessage);

            //--- Expected Result: The Last Name: "Last Name is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_LastName_BlankInput_ErrorMessage, lastName_blankInput_errorMessage);

            //----- Your detailed contact information -----//

            //--- Expected Result: The Organization Name: "Organization name is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_OrganizationName_BlankInput_ErrorMessage, organizationName_blankInput_errorMessage);

            //--- Expected Result: The Address: "Address Line 1 is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_Address_1_BlankInput_ErrorMessage, addressLine_1_blankInput_errorMessage);

            //--- Expected Result: The Telephone: "Telephone is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_SoftwareRegistration_Telephone_BlankInput_ErrorMessage, telephone_blankInput_errorMessage);

            //--- Expected Result: The City: "City is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_City_BlankInput_ErrorMessage, city_blankInput_errorMessage);

            //--- Expected Result: The State/Province: "State/Province is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_StateProvince_BlankInput_ErrorMessage, stateProvince_blankInput_errorMessage);

            //--- Expected Result: The Zip/Postal Code: "Zip/Postal Code is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_ZipPostalCode_BlankInput_ErrorMessage, zipPostalCode_blankInput_errorMessage);

            //--- Expected Result: The Country: "Country is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_CountryRegion_BlankInput_ErrorMessage, countryRegion_blankInput_errorMessage);

            //----- Your serial number and host ID -----//

            //--- Expected Result: The Please provide your serial number exactly as it appears on the media that you received: "Please provide the Serial #" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_PleaseProvideYourSerialNumberExactlyAsItAppearsOnTheMediaThatYouReceived_BlankInput_ErrorMessage, pleaseProvideYourSerialNumberExactlyAsItAppearsOnTheMediaThatYouReceived_blankInput_errorMessage);

            //--- Expected Result: The Host ID: "Please provide a Host(Machine) ID" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_SoftwareRegistration_HostId_BlankInput_ErrorMessage, hostId_blankInput_errorMessage);
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Form Submission is Working as Expected in EN Locale")]
        public void Forms_SoftwareRegistration_VerifyFormSubmission(string Locale)
        {
            if (Util.Configuration.Environment.Equals("dev") || Util.Configuration.Environment.Equals("qa"))
            {
                //--- ADI Site Search Support Form URL ---//
                string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + softwareRegistration_form_url;

                //--- Action: Navigate to the given URL ---//
                action.Navigate(driver, form_url);

                //----- Form TestPlan > EN Tab > R26 > T4_5: Submit Software Registration Form with valid data (AL-2496) -----//

                //--- Action: Enter valid data on the mandatory fields. ---//

                //----- About you -----//
                string emailId_input = "t3st_" + Util.Configuration.Environment.ToLower() + "_softwareRegistration_f0rm_" + util.GenerateRandomNumber() + "@mailinator.com";
                //Console.WriteLine("Email Id Input = " + emailId_input);
                action.IDeleteValueOnFields(driver, Elements.Forms_EmailId_InputBox);
                action.IType(driver, Elements.Forms_EmailId_InputBox, emailId_input);

                string firstName_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_FirstName_InputBox);
                action.IType(driver, Elements.Forms_FirstName_InputBox, firstName_input);

                string lastName_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_LastName_InputBox);
                action.IType(driver, Elements.Forms_LastName_InputBox, lastName_input);

                //----- Your detailed contact information -----//
                string organizationName_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_OrganizationName_InputBox);
                action.IType(driver, Elements.Forms_OrganizationName_InputBox, organizationName_input);

                string address_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_Address_InputBox);
                action.IType(driver, Elements.Forms_Address_InputBox, address_input);

                string telephone_input = "123456789";
                action.IDeleteValueOnFields(driver, Elements.Forms_Telephone_InputBox);
                action.IType(driver, Elements.Forms_Telephone_InputBox, telephone_input);

                string city_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_City_InputBox);
                action.IType(driver, Elements.Forms_City_InputBox, city_input);

                string zipPostalCode_input = "12345";
                action.IDeleteValueOnFields(driver, Elements.Forms_ZipPostalCode_InputBox);
                action.IType(driver, Elements.Forms_ZipPostalCode_InputBox, zipPostalCode_input);

                string countryRegion_input = "US";
                action.ISelectFromDropdown(driver, Elements.Forms_CountryRegion_Dropdown, countryRegion_input);

                action.IClick(driver, Elements.Forms_StateProvince_Dropdown);
                action.IClick(driver, By.CssSelector("select[id$='tate']>option:nth-of-type(3)"));

                //----- Your serial number and host ID -----//
                string pleaseProvideYourSerialNumberExactlyAsItAppearsOnTheMediaThatYouReceived_input = "ADI-CCES-4IVV-TYXV-8776-SQFQ-X4PZ-BFE7-WS01";
                action.IDeleteValueOnFields(driver, Elements.Forms_SerialNo_InputBox);
                action.IType(driver, Elements.Forms_SerialNo_InputBox, pleaseProvideYourSerialNumberExactlyAsItAppearsOnTheMediaThatYouReceived_input);

                string hostId_input = "fcaa149534d0 d8fc93726307";
                action.IDeleteValueOnFields(driver, Elements.Forms_HostId_InputBox);
                action.IType(driver, Elements.Forms_HostId_InputBox, hostId_input);

                //----- Privacy Settings -----//
                action.ICheck(driver, Elements.Forms_Communication_Checkbox);

                //--- Action: Click on Submit button ---//
                action.IClick(driver, Elements.Forms_Submit_Button);

                //--- Expected Result: Thank you for registering your CrossCore software product page should be displayed ---//
                test.validateStringInstance(driver, driver.Url, thankYouForRegistering_page_url);

                if (driver.Url.Contains(thankYouForRegistering_page_url))
                {
                    action.ICheckMailinatorEmail(driver, emailId_input);

                    //--- Expected Result: The email from Analog Devices, Inc. (noreply@analog.com) should be displayed ---//
                    test.validateStringIsCorrect(driver, Elements.Mailinator_LatestEmail_Sender, "Analog Devices, Inc.");
                }
            }
        }
    }
}