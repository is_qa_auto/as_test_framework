﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using OpenQA.Selenium;
using System;
using System.Threading;

namespace AS_Test_Framework.Forms
{
    [TestFixture]
    public class Forms_VisualDspTestDriveRegistration : BaseSetUp
    {
        //--- VisualDSP++ Test Drive Registration Form URL: ---//
        //--- DEV: https://form-dev.corpnt.analog.com/form_pages/processors/visualdsptestdrive.aspx ---//
        //--- QA: https://form-qa.corpnt.analog.com/form_pages/processors/visualdsptestdrive.aspx ---//
        //--- PROD: https://form.analog.com/form_pages/processors/visualdsptestdrive.aspx ---//

        public Forms_VisualDspTestDriveRegistration() : base() { }

        //--- Login Credentials ---//
        string username = "f0rms_t3st_555@mailinator.com";
        string password = "Test_1234";

        //--- URLs ---//
        string visualDspTestDriveRegistration_form_url = "form_pages/processors/visualdsptestdrive.aspx";
        string testDriveConfirm_blackfin_page_url = "/test_drive_confirm_blackfin.html";
        string testDriveConfirmSharc_page_url = "/test_drive_confirm_sharc.html";
        string testDriveConfirmTigerSharc_page_url = "/test_drive_confirm_tigersharc.html";
        string testDriveConfirmAdsp21xx_page_url = "/test_drive_confirm_adsp21xx.html";
        string visualDsp5_1_page_url = "/vdsp-bf-sh-ts.html";

        //--- Labels ---//
        string blackfinVisualDsp_txt = "Blackfin VisualDSP++";
        string sharcVisualDsp_txt = "SHARC VisualDSP++";
        string tigerSharcVisualDsp_txt = "TigerSHARC VisualDSP++";
        string adsp21xxVisualDsp_txt = "ADSP 21XX VisualDSP++";
        string email_blankInput_errorMessage = "Email address is required";
        string firstName_blankInput_errorMessage = "First Name is required";
        string lastName_blankInput_errorMessage = "Last Name is required";
        string organizationName_blankInput_errorMessage = "Organization name is required";
        string address_1_blankInput_errorMessage = "Address Line 1 is required";
        string telephone_blankInput_errorMessage = "Telephone is required";
        string city_blankInput_errorMessage = "City is required";
        string zipPostalCode_blankInput_errorMessage = "Zip/Postal Code is required";
        string countryRegion_blankInput_errorMessage = "Country/Region is required";
        string state_blankInput_errorMessage = "State/Province is required";
        string whichTestDriveAreYouRegistering_blankInput_errorMessage = "Please select the test drive you are registering for";
        string howDidYouObtainThisTestDrive_blankInput_errorMessage = "Please select as to how did you obtain the test drive";
        string whichEmbeddedProcessorAreYouEvaluating_blankInput_errorMessage = "Please select the embedded processor you are evaluating";
        string whatIsThePrimaryMarketSegmentThatYourApplicationTargets_blankInput_errorMessage = "Please select the primary market segment that your application is targeting";
        string whatIsThePrimaryTypeOfApplicationThatYouDevelop_blankInput_errorMessage = "Please select the primary type of application that you develop";
        string email_invalidInput_errorMessage = "Please enter a valid email address";

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Elements are Present and Working as Expected when the User is Logged Out in EN Locale")]
        public void Forms_VisualDspTestDriveRegistration_VerifyElementsWhenUserIsLoggedOut(string Locale)
        {
            //--- VisualDSP++ Test Drive Registration Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + visualDspTestDriveRegistration_form_url;

            //--- Action: Navigate to the given URL ---//
            action.Navigate(driver, form_url);

            //----- Your test drive -----//

            //----- Form TestPlan > EN Tab > R4 > T4_3: Verify the behavior of the disabled items on VisualDSP++ Test Drive Registration -----//

            //--- Expected Result: Other (specifybelow): Textbox should be disabled on the page ---//
            test.validateElementIsNotEnabled(driver, Elements.Forms_HowDidYouObtainThisTestDrive_Other_InputBox);

            //--- Expected Result: Which embedded processor are you evaluating? dropdown should be disabled on the page ---//
            test.validateElementIsNotEnabled(driver, Elements.Forms_VisualDspTestDriveRegistration_WhichEmbeddedProcessorAreYouEvaluating_Dropdown);

            //----- Your Project -----//

            //--- Expected Result: What is the primary type of application that you develop? (dropdown) should be disabled on the page ---//
            test.validateElementIsNotEnabled(driver, Elements.Forms_VisualDspTestDriveRegistration_WhatIsThePrimaryTypeOfApplicationThatYouDevelop_Dropdown);

            //----- Privacy Settings -----//

            //----- Form TestPlan > EN Tab > R38 > T1: Verify Privacy Settings in All Forms [account is not logged in] (AL-11345) -----//

            //--- Expected Result: Privacy Settings section is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_Section);

            //--- Expected Result: Prompt "You can change your privacy settings at any time by clicking on the unsubscribe link in emails sent from Analog Devices or in Analog’s Privacy Settings." is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_LegalDisclaimer_Text);

            //--- Expected Result: Privacy Settings is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_Link);

            //--- Expected Result: Privacy & Security Statement ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacyAndSecurityStatement_Link);

            //--- Expected Result: Authorized Partners ---//
            test.validateElementIsPresent(driver, Elements.Forms_AuthorizedPartners_Link);

            //----- Save time by logging in or registering for myAnalog  -----//

            //----- Form TestPlan > EN Tab > R4 > T3: Login using Save time to login section with valid data -----//

            if (!util.CheckElement(driver, Elements.Forms_Expanded_SaveTimeByLoggingInOrRegisteringForMyAnalog_Panel, 1))
            {
                //--- Action: Click on Save time by logging in or registering for myAnalog   link ---//
                action.IClick(driver, Elements.Forms_SaveTimeByLoggingInOrRegisteringForMyAnalog_Link);
            }

            //--- Expected Result: Save time to login section should be displayed  ---//
            test.validateElementIsPresent(driver, Elements.Forms_Expanded_SaveTimeByLoggingInOrRegisteringForMyAnalog_Panel);
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Elements are Present and Working as Expected when the User is Logged In in EN Locale")]
        public void Forms_VisualDspTestDriveRegistration_VerifyElementsWhenUserIsLoggedIn(string Locale)
        {
            //--- VisualDSP++ Test Drive Registration Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + visualDspTestDriveRegistration_form_url;

            //--- Action: Navigate to the given URL ---//
            action.Navigate(driver, form_url);

            //----- Save time by logging in or registering for myAnalog -----//

            //----- Form TestPlan > EN Tab > R4 > T3: Login using Save time to login section with valid data -----//

            if (!util.CheckElement(driver, Elements.Forms_Expanded_SaveTimeByLoggingInOrRegisteringForMyAnalog_Panel, 1))
            {
                //--- Action: Click on Save time by logging in or registering for myAnalog   link ---//
                action.IClick(driver, Elements.Forms_SaveTimeByLoggingInOrRegisteringForMyAnalog_Link);
            }

            //--- Action: Login using valid data ---//
            action.ILogInViaSaveTimeByLoggingInPanel(driver, username, password);
            Thread.Sleep(1000);

            driver.Navigate().Refresh();

            //----- About you -----//

            //--- Expected Result: The Email Id field should be  automatically populated ---//
            test.validateFieldIsNotEmpty(driver, Elements.Forms_EmailId_InputBox);

            //--- Expected Result: The First Name field should be  automatically populated ---//
            test.validateFieldIsNotEmpty(driver, Elements.Forms_FirstName_InputBox);

            //--- Expected Result: The Last Name field should be  automatically populated ---//
            test.validateFieldIsNotEmpty(driver, Elements.Forms_LastName_InputBox);

            //----- Your Detailed Information -----//

            //--- Expected Result: The Organization Name field should be  automatically populated ---//
            test.validateFieldIsNotEmpty(driver, Elements.Forms_OrganizationName_InputBox);

            //--- Expected Result: The Zip/Postal Code field should be  automatically populated ---//
            test.validateFieldIsNotEmpty(driver, Elements.Forms_ZipPostalCode_InputBox);

            //--- Expected Result: The Country(dropdown) should be  automatically populated ---//
            test.validateStringChanged(driver, util.GetSelectedDropdownValue(driver, Elements.Forms_CountryRegion_Dropdown), "Select Country/Region");

            //--- Expected Result: The State/Provinces(dropdown) should be  automatically populated ---//
            test.validateStringChanged(driver, util.GetSelectedDropdownValue(driver, Elements.Forms_StateProvince_Dropdown), "Select State/Province");

            //----- Privacy Settings -----//

            //----- Form TestPlan > EN Tab > R38 > T1: Verify Privacy Settings in All Forms [account is logged in] (AL-11345) -----//

            //--- Expected Result: Prompt "You can change your privacy settings at any time by clicking on the unsubscribe link in emails sent from Analog Devices or in Analog’s Privacy Settings." is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_LegalDisclaimer_Text);

            //--- Expected Result: Privacy Settings is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_Link);

            //--- Expected Result: Privacy & Security Statement ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacyAndSecurityStatement_Link);

            //--- Expected Result: Authorized Partners ---//
            test.validateElementIsPresent(driver, Elements.Forms_AuthorizedPartners_Link);
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Input Validations are Working as Expected for Valid Inputs in EN Locale")]
        public void Forms_VisualDspTestDriveRegistration_VerifyInputValidationsForValidInputs(string Locale)
        {
            //--- VisualDSP++ Test Drive Registration Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + visualDspTestDriveRegistration_form_url;

            //--- Action: Navigate to the given URL ---//
            action.Navigate(driver, form_url);

            //----- Your test drive -----//

            //----- Form TestPlan > EN Tab > R4 > T4_3: Verify the behavior of the disabled items on VisualDSP++ Test Drive Registration  -----//

            //--- Action: Choose any bullet on Which Test Drive are you registering? ---//
            action.IClick(driver, Elements.Forms_WhichTestDriveAreYouRegistering_RadioButtons);

            //--- Expected Result: The Which embedded processor are you evaluating? Dropdown should be enabled ---//
            //--- Note: AL-17876 has been logged for the Issue in PROD ---//
            test.validateElementIsEnabled(driver, Elements.Forms_VisualDspTestDriveRegistration_WhichEmbeddedProcessorAreYouEvaluating_Dropdown);

            //--- Action: Choose Other   bullet  on  How did you obtain this Test Drive? ---//
            action.IClick(driver, Elements.Forms_HowDidYouObtainThisTestDrive_Other_RadioButton);

            //--- Expected Result: The Other field should be enabled ---//
            test.validateElementIsEnabled(driver, Elements.Forms_HowDidYouObtainThisTestDrive_Other_InputBox);

            //--- Action: Provide a value on other textbox (e.g. asdtest)---//
            string other_input = "asdtest";
            action.IDeleteValueOnFields(driver, Elements.Forms_HowDidYouObtainThisTestDrive_Other_InputBox);
            action.IType(driver, Elements.Forms_HowDidYouObtainThisTestDrive_Other_InputBox, other_input);
            action.IType(driver, Elements.Forms_HowDidYouObtainThisTestDrive_Other_InputBox, Keys.Tab);

            //--- Action: Choose on Which embedded processor are you evaluating? Dropdown ---//
            action.IClick(driver, Elements.Forms_VisualDspTestDriveRegistration_WhichEmbeddedProcessorAreYouEvaluating_Dropdown);
            action.IClick(driver, By.CssSelector("select[id$='evaluating_dsp']>option:nth-of-type(2)"));

            //----- Your Project -----//

            //--- Action: Choose on What is the primary market segment that your application targets? Dropdown ---//
            action.IClick(driver, Elements.Forms_VisualDspTestDriveRegistration_WhatIsThePrimaryMarketSegmentThatYourApplicationTargets_Dropdown);
            action.IClick(driver, By.CssSelector("select[id$='Primary_Market_Segment']>option:nth-of-type(2)"));

            //--- Expected Result:  What is the primary type of application that you develop? Dropdown will be enabled ---//
            test.validateElementIsEnabled(driver, Elements.Forms_VisualDspTestDriveRegistration_WhatIsThePrimaryTypeOfApplicationThatYouDevelop_Dropdown);
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Input Validations are Working as Expected for Invalid Inputs in EN Locale")]
        public void Forms_VisualDspTestDriveRegistration_VerifyInputValidationsForInvalidInputs(string Locale)
        {
            //--- VisualDSP++ Test Drive Registration Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + visualDspTestDriveRegistration_form_url;

            //--- Action: Navigate to the given URL ---//
            action.Navigate(driver, form_url);

            //----- Form TestPlan > EN Tab > R4 > T4_2: Submit the Visual DSP++ Test Drive Registration  Form    with invalid data -----//

            //--- Action: Enter invalid data on the Email Address field. (e.g. 123!@#$%@yahoo.com) ---//
            string emailId_input = "123!@#$%@yahoo.com";
            action.IDeleteValueOnFields(driver, Elements.Forms_EmailId_InputBox);
            action.IType(driver, Elements.Forms_EmailId_InputBox, emailId_input);
            action.IType(driver, Elements.Forms_EmailId_InputBox, Keys.Tab);

            //--- Expected Result: The error message below should be displayed: "Please enter a valid email address" ---//
            test.validateStringIsCorrect(driver, Elements.Forms_VisualDspTestDriveRegistration_Email_InvalidInput_ErrorMessage, email_invalidInput_errorMessage);
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Input Validations are Working as Expected for Blank Inputs in EN Locale")]
        public void Forms_VisualDspTestDriveRegistration_VerifyInputValidationsForBlankInputs(string Locale)
        {
            //--- VisualDSP++ Test Drive Registration Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + visualDspTestDriveRegistration_form_url;

            //--- Action: Navigate to the given URL ---//
            action.Navigate(driver, form_url);

            //----- Privacy Settings -----//

            //----- Form TestPlan > EN Tab > R4 > T4_1: Submit Visual DSP++ Test Drive Registration  Form    with no data -----//

            //--- Action: Click on the Submit button without entering data on the mandatory fields. ---//
            action.IClick(driver, Elements.Forms_Submit_Button);

            //----- About you -----//

            //--- Expected Result: Email: "Email is required" is displayed ---//
            test.validateStringIsCorrect(driver, Elements.Forms_VisualDspTestDriveRegistration_Email_BlankInput_ErrorMessage, email_blankInput_errorMessage);

            //--- Expected Result: FirstName:  "FirstName  is required" is displayed ---//
            test.validateStringIsCorrect(driver, Elements.Forms_FirstName_BlankInput_ErrorMessage, firstName_blankInput_errorMessage);

            //--- Expected Result: LastName:  "Last Name  is required" is displayed ---//
            test.validateStringIsCorrect(driver, Elements.Forms_LastName_BlankInput_ErrorMessage, lastName_blankInput_errorMessage);

            //----- Your Detailed Information -----//

            //--- Expected Result: Organization Name:   "Organization name  is required" is displayed ---//
            test.validateStringIsCorrect(driver, Elements.Forms_OrganizationName_BlankInput_ErrorMessage, organizationName_blankInput_errorMessage);

            //--- Expected Result: Address 1:   "Address Line 1 is required" is displayed ---//
            test.validateStringIsCorrect(driver, Elements.Forms_Address_1_BlankInput_ErrorMessage, address_1_blankInput_errorMessage);

            //--- Expected Result: Telephone:  "Telephone l is required" is displayed ---//
            test.validateStringIsCorrect(driver, Elements.Forms_VisualDspTestDriveRegistration_Telephone_BlankInput_ErrorMessage, telephone_blankInput_errorMessage);

            //--- Expected Result: City:  "City  is required" is displayed ---//
            test.validateStringIsCorrect(driver, Elements.Forms_City_BlankInput_ErrorMessage, city_blankInput_errorMessage);

            //--- Expected Result: Zip/Postal Code:  "Zip/Postal Code is  is required" is displayed ---//
            test.validateStringIsCorrect(driver, Elements.Forms_ZipPostalCode_BlankInput_ErrorMessage, zipPostalCode_blankInput_errorMessage);

            //--- Expected Result: Country(dropdown): "Country is required" is displayed ---//
            test.validateStringIsCorrect(driver, Elements.Forms_CountryRegion_BlankInput_ErrorMessage, countryRegion_blankInput_errorMessage);

            //--- Expected Result: State/Provinces(dropdown):  "State/Province is required" is displayed ---//
            test.validateStringIsCorrect(driver, Elements.Forms_StateProvince_BlankInput_ErrorMessage, state_blankInput_errorMessage);

            //----- Your test drive -----//

            //--- Expected Result: Which Test Drive are you registering? "Please select the test drive you are registering for" is displayed ---//
            test.validateStringIsCorrect(driver, Elements.Forms_VisualDspTestDriveRegistration_WhichTestDriveAreYouRegistering_BlankInput_ErrorMessage, whichTestDriveAreYouRegistering_blankInput_errorMessage);

            //--- Expected Result: How did you obtain this Test Drive? "Please select as to how did you obtain the test drive" is displayed ---//
            test.validateStringIsCorrect(driver, Elements.Forms_VisualDspTestDriveRegistration_HowDidYouObtainThisTestDrive_BlankInput_ErrorMessage, howDidYouObtainThisTestDrive_blankInput_errorMessage);

            //--- Expected Result: Which embedded processor are you evaluating)? "Please select the embedded processor you are evaluating" is displayed ---//
            test.validateStringIsCorrect(driver, Elements.Forms_WhichEmbeddedProcessorAreYouEvaluating_BlankInput_ErrorMessage, whichEmbeddedProcessorAreYouEvaluating_blankInput_errorMessage);

            //----- Your Project -----//

            //--- Expected Result: What is the primary market segment that your application targets? "Please select the primary market segment that your application targets" is displayed ---//
            test.validateStringIsCorrect(driver, Elements.Forms_WhatIsThePrimaryMarketSegmentThatYourApplicationTargets_BlankInput_ErrorMessage, whatIsThePrimaryMarketSegmentThatYourApplicationTargets_blankInput_errorMessage);

            //--- Expected Result: What is the primary type of application that you develop? "Please select the primary type of application that you develop" is displayed ---//
            test.validateStringIsCorrect(driver, Elements.Forms_VisualDspTestDriveRegistration_WhatIsThePrimaryTypeOfApplicationThatYouDevelop_BlankInput_ErrorMessage, whatIsThePrimaryTypeOfApplicationThatYouDevelop_blankInput_errorMessage);
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Form Submission is Working as Expected in EN Locale")]
        public void Forms_VisualDspTestDriveRegistration_VerifyFormSubmission(string Locale)
        {
            if (Util.Configuration.Environment.Equals("dev") || Util.Configuration.Environment.Equals("qa"))
            {
                //--- VisualDSP++ Test Drive Registration Form URL ---//
                string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + visualDspTestDriveRegistration_form_url;

                //--- Action: Navigate to the given URL ---//
                action.Navigate(driver, form_url);

                //--- Action: Enter valid data on the mandatory fields. ---//

                //----- About you -----//
                string emailId_input = "visualDspTestDriveRegistration_f0rm_t3st_" + Util.Configuration.Environment.ToLower() + "_" + util.GenerateRandomNumber() + "@mailinator.com";
                //Console.WriteLine("Email Id Input = " + emailId_input);
                action.IDeleteValueOnFields(driver, Elements.Forms_EmailId_InputBox);
                action.IType(driver, Elements.Forms_EmailId_InputBox, emailId_input);

                string firstName_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_FirstName_InputBox);
                action.IType(driver, Elements.Forms_FirstName_InputBox, firstName_input);

                string lastName_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_LastName_InputBox);
                action.IType(driver, Elements.Forms_LastName_InputBox, lastName_input);

                //----- Your Detailed Information -----//
                string organizationName_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_OrganizationName_InputBox);
                action.IType(driver, Elements.Forms_OrganizationName_InputBox, organizationName_input);

                string address_1_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_Address_1_InputBox);
                action.IType(driver, Elements.Forms_Address_1_InputBox, address_1_input);

                string telephone_input = "123456789";
                action.IDeleteValueOnFields(driver, Elements.Forms_Telephone_InputBox);
                action.IType(driver, Elements.Forms_Telephone_InputBox, telephone_input);

                string city_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_City_InputBox);
                action.IType(driver, Elements.Forms_City_InputBox, city_input);

                string zipPostalCode_input = "12345";
                action.IDeleteValueOnFields(driver, Elements.Forms_ZipPostalCode_InputBox);
                action.IType(driver, Elements.Forms_ZipPostalCode_InputBox, zipPostalCode_input);

                string countryRegion_input = "US";
                action.ISelectFromDropdown(driver, Elements.Forms_CountryRegion_Dropdown, countryRegion_input);

                action.IClick(driver, Elements.Forms_StateProvince_Dropdown);
                action.IClick(driver, By.CssSelector("select[id$='tate']>option:nth-of-type(3)"));

                //----- Your test drive -----//
                int whichTestDriveAreYouRegistering_radioButton_count = util.GetCount(driver, Elements.Forms_WhichTestDriveAreYouRegistering_RadioButtons);
                Random rand = new Random();
                int random_number = rand.Next(1, whichTestDriveAreYouRegistering_radioButton_count);
                action.IClick(driver, By.CssSelector("table[id$='rbtnlstTestDrive']>tbody>tr:nth-of-type("+ random_number + ") * input"));
                string selected_testDrive = util.GetText(driver, By.CssSelector("table[id$='rbtnlstTestDrive']>tbody>tr:nth-of-type(" + random_number + ") * label"));
                //Console.WriteLine("Selected Test Drive = " + selected_testDrive);

                action.IClick(driver, Elements.Forms_HowDidYouObtainThisTestDrive_RadioButtons);

                action.IClick(driver, Elements.Forms_VisualDspTestDriveRegistration_WhichEmbeddedProcessorAreYouEvaluating_Dropdown);
                action.IClick(driver, By.CssSelector("select[id$='evaluating_dsp']>option:nth-of-type(2)"));

                //----- Your Project -----//
                action.IClick(driver, Elements.Forms_VisualDspTestDriveRegistration_WhatIsThePrimaryMarketSegmentThatYourApplicationTargets_Dropdown);
                action.IClick(driver, By.CssSelector("select[id$='Primary_Market_Segment']>option:nth-of-type(2)"));

                action.IClick(driver, Elements.Forms_VisualDspTestDriveRegistration_WhatIsThePrimaryTypeOfApplicationThatYouDevelop_Dropdown);
                action.IClick(driver, By.CssSelector("select[id$='Application_Type']>option:nth-of-type(2)"));

                //----- Privacy Settings -----//
                action.ICheck(driver, Elements.Forms_Communication_Checkbox);

                //--- Action: Click on the Submit button ---//
                action.IClick(driver, Elements.Forms_Submit_Button);

                if (selected_testDrive.Equals(blackfinVisualDsp_txt))
                {
                    //----- Form TestPlan > EN Tab > R4 > T5_1: Verify the Thank You page for Blackfin VisualDSP++ test drive -----//

                    //--- Expected Result: Page should be redirected to Thank you for registering your free 90-day test drive of VisualDSP++ for Blackfin Processors ---//
                    test.validateStringInstance(driver, driver.Url, testDriveConfirm_blackfin_page_url);
                }
                else if (selected_testDrive.Equals(sharcVisualDsp_txt))
                {
                    //----- Form TestPlan > EN Tab > R4 > T5_2: Verify the Thank You page for SHARC VisualDSP++ test drive -----//

                    //--- Expected Result: Page should be redirected to Thank You For Registering Your Free 90-Day Test Drive Of VisualDSP++ For SHARC Processors ---//
                    test.validateStringInstance(driver, driver.Url, testDriveConfirmSharc_page_url);
                }
                else if (selected_testDrive.Equals(tigerSharcVisualDsp_txt))
                {
                    //----- Form TestPlan > EN Tab > R4 > T5_3: Verify the Thank You page for TigerSHARC VisualDSP++ test drive -----//

                    //--- Action: Click on the Submit button ---//
                    action.IClick(driver, Elements.Forms_Submit_Button);

                    //--- Expected Result: Page should be redirected to Thank You For Registering Your 90-Day Trial of VisualDSP++ For TigerSharc Processors ---//
                    test.validateStringInstance(driver, driver.Url, testDriveConfirmTigerSharc_page_url);
                }
                else if (selected_testDrive.Equals(adsp21xxVisualDsp_txt))
                {
                    //----- Form TestPlan > EN Tab > R4 > T5_4: Verify the Thank You page for ADSP 21XX VisualDSP++ test drive -----//

                    //--- Expected Result: Page should be redirected to Thank You for Registering your 90-day trial of VisualDSP++ for the ADSP-21xx DSPs ---//
                    test.validateStringInstance(driver, driver.Url, testDriveConfirmAdsp21xx_page_url);
                }

                if (!selected_testDrive.Equals(adsp21xxVisualDsp_txt))
                {
                    //----- Form TestPlan > EN Tab > R4 > T4_4: Submit Visual DSP++ Test Drive Registration  Form   with valid data -----//

                    //--- Action: Click on Download Test Drive button ---//
                    action.IOpenLinkInNewTab(driver, Elements.ThankYouForRegisteringYourFree90DayTestDrive_DownloadTestDrive_Button);
                    Thread.Sleep(1000);

                    //--- Expected Result: The page should be redirected to VISUALDSP++ 5.1 detail page ---//
                    test.validateStringInstance(driver, driver.Url, visualDsp5_1_page_url);

                    if (driver.Url.Contains(visualDsp5_1_page_url))
                    {
                        action.ICheckMailinatorEmail(driver, emailId_input);

                        //--- Expected Result: The email from processor.tools.testdrive@analog.com should be displayed ---//
                        test.validateStringIsCorrect(driver, Elements.Mailinator_LatestEmail_Sender, "processor.tools.testdrive@analog.");
                    }
                }
            }
        }
    }
}