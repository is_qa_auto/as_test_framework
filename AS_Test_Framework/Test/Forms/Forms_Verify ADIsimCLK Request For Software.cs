﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using OpenQA.Selenium;
using System;
using System.Threading;

namespace AS_Test_Framework.Forms
{
    [TestFixture]
    public class Forms_AdiSimClkRequestForSoftware : BaseSetUp
    {
        //--- ADIsimCLK Request For Software Form URL: ---//
        //--- DEV: https://form-dev.corpnt.analog.com/Form_Pages/RFComms/ADIsimCLK.aspx ---//
        //--- QA: https://form-qa.corpnt.analog.com/Form_Pages/RFComms/ADIsimCLK.aspx ---//
        //--- PROD: https://form.analog.com/Form_Pages/RFComms/ADIsimCLK.aspx ---//

        public Forms_AdiSimClkRequestForSoftware() : base() { }

        //--- Login Credentials ---//
        string username = "f0rms_t3st_555@mailinator.com";
        string password = "Test_1234";

        //--- URLs ---//
        string adiSimClkRequestForSoftware_form_url = "Form_Pages/RFComms/ADIsimCLK.aspx";
        string adiSimClkVersionRequestForSoftware_page_url = "/adisimclk_thankyou.html";
        string adiSimClkReferenceDesignFiles_page_url = "/adisimclk-reference-design-files.html";

        //--- Labels ---//
        string countryRegion_text = "Country/Region";
        string emailId_blankInput_errorMessage = "Email address is required";
        string firstName_blankInput_errorMessage = "First Name is required";
        string lastName_blankInput_errorMessage = "Last Name is required";
        string jobTitle_blankInput_errorMessage = "Please Select Job Title";
        string organizationName_blankInput_errorMessage = "Organization name is required";
        string address_blankInput_errorMessage = "Address Line 1 is required";
        string telephone_blankInput_errorMessage = "Telephone is required";
        string city_blankInput_errorMessage = "City is required";
        string state_blankInput_errorMessage = "State/Province is required";
        string zipPostalCode_blankInput_errorMessage = "Zip/Postal Code is required";
        string countryRegion_blankInput_errorMessage = "Country/Region is required";
        string whatIsYourCircuitApplication_blankInput_errorMessage = "Please Select What is your circuit application?";
        string whatIsYourEndMarket_blankInput_errorMessage = "Please Select What is your end market?";
        string inputFrequency_blankInput_errorMessage = "Input Frequency is required";
        string outputFrequency_blankInput_errorMessage = "Output Frequency is required";
        string emailId_invalidInput_errorMessage = "Please enter a valid email address";

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Elements are Present and Working as Expected when the User is Logged Out in EN Locale")]
        public void Forms_AdiSimClkRequestForSoftware_VerifyElementsWhenUserIsLoggedOut(string Locale)
        {
            //--- ADIsimCLK Request For Software Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + adiSimClkRequestForSoftware_form_url;

            //--- Action: Open "ADIsimCLK Request For Software" form ---//
            action.Navigate(driver, form_url);

            //----- Form TestPlan > EN Tab > R40 > T1: Verify all labels for Country is changed to "Country/Region" (IQ-4970/AL-12466) -----//

            //----- Your detailed information -----//

            //--- Expected Result: The Country label has been changed to Country/Region ---//
            test.validateStringInstance(driver, util.GetText(driver, Elements.Forms_Country_Label), countryRegion_text);

            //----- Form TestPlan > EN Tab > R38 > T1: Verify Privacy Settings in All Forms [account is not logged in] (AL-11345) -----//

            //----- Privacy Settings -----//

            //--- Expected Result: Privacy Settings section is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_Section);

            //--- Expected Result: Prompt "You can change your privacy settings at any time by clicking on the unsubscribe link in emails sent from Analog Devices or in Analog’s Privacy Settings." is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_LegalDisclaimer_Text);

            //--- Expected Result: Privacy Settings is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_Link);

            //--- Expected Result: Privacy & Security Statement ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacyAndSecurityStatement_Link);

            //--- Expected Result: Authorized Partners ---//
            test.validateElementIsPresent(driver, Elements.Forms_AuthorizedPartners_Link);

            //----- Form TestPlan > EN Tab > R14 > T3: Login using Save time to login section with valid data -----//

            //----- Save time by logging in or registering for myAnalog -----//

            if (!util.CheckElement(driver, Elements.Forms_Expanded_SaveTimeByLoggingInOrRegisteringForMyAnalog_Panel, 1))
            {
                //--- Action: Click on Save time by logging in or registering for myAnalog   link ---//
                action.IClick(driver, Elements.Forms_SaveTimeByLoggingInOrRegisteringForMyAnalog_Link);
            }

            //--- Expected Result: Save time to login section should be displayed ---//
            test.validateElementIsPresent(driver, Elements.Forms_Expanded_SaveTimeByLoggingInOrRegisteringForMyAnalog_Panel);
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Elements are Present and Working as Expected when the User is Logged In in EN Locale")]
        public void Forms_AdiSimClkRequestForSoftware_VerifyElementsWhenUserIsLoggedIn(string Locale)
        {
            //--- ADIsimCLK Request For Software Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + adiSimClkRequestForSoftware_form_url;

            //--- Action: Open "ADIsimCLK Request For Software" form ---//
            action.Navigate(driver, form_url);

            //----- Form TestPlan > EN Tab > R14 > T3: Login using Save time to login section with valid data -----//

            //----- Save time by logging in or registering for myAnalog -----//

            //--- Action: Login using valid data ---//
            action.ILogInViaSaveTimeByLoggingInPanel(driver, username, password);
            Thread.Sleep(1000);

            //----- About you -----//

            //--- Expected Result: The Email Id field should be  automatically populated ---//
            test.validateFieldIsNotEmpty(driver, Elements.Forms_Email_InputBox);

            //--- Expected Result: The Frist Name field should be  automatically populated ---//
            test.validateFieldIsNotEmpty(driver, Elements.Forms_FirstName_InputBox);

            //--- Expected Result: The Last Name field should be  automatically populated ---//
            test.validateFieldIsNotEmpty(driver, Elements.Forms_LastName_InputBox);

            //----- Your detailed information -----//

            //--- Expected Result: The Organization Name field should be  automatically populated ---//
            test.validateFieldIsNotEmpty(driver, Elements.Forms_OrganizationName_InputBox);

            //--- Expected Result: The State/ Province (dropdown) field should be  automatically populated ---//
            test.validateStringChanged(driver, util.GetSelectedDropdownValue(driver, Elements.Forms_StateProvince_Dropdown), "Select State/Province");

            //--- Expected Result: The Zip/Postal Code field should be  automatically populated ---//
            test.validateFieldIsNotEmpty(driver, Elements.Forms_ZipPostalCode_InputBox);

            //--- Expected Result: The Country(dropdown) field should be  automatically populated ---//
            test.validateStringChanged(driver, util.GetSelectedDropdownValue(driver, Elements.Forms_CountryRegion_Dropdown), "Select Country/Region");

            //----- Form TestPlan > EN Tab > R38 > T1: Verify Privacy Settings in All Forms [account is logged in] (AL-11345) -----//

            //----- Privacy Settings -----//

            //--- Expected Result: Prompt "You can change your privacy settings at any time by clicking on the unsubscribe link in emails sent from Analog Devices or in Analog’s Privacy Settings." is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_LegalDisclaimer_Text);

            //--- Expected Result: Privacy Settings is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_Link);

            //--- Expected Result: Privacy & Security Statement ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacyAndSecurityStatement_Link);

            //--- Expected Result: Authorized Partners ---//
            test.validateElementIsPresent(driver, Elements.Forms_AuthorizedPartners_Link);
        }

        [TestCase("en", TestName = "Verify that the Input Validations are Working as Expected for Invalid Inputs in EN Locale")]
        public void Forms_AdiSimClkRequestForSoftware_VerifyInputValidationsForInvalidInputs(string Locale)
        {
            //--- ADIsimCLK Request For Software Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + adiSimClkRequestForSoftware_form_url;

            //--- Action: Open "ADIsimCLK Request For Software" form ---//
            action.Navigate(driver, form_url);

            //----- Form TestPlan > EN Tab > R14 > T4_2: Submit the ADISIMCLK  Form    with invalid data -----//

            //--- Action: Enter invalid data on the Email Address field. (e.g. 123!@#$%@yahoo.com) ---//
            string email_input = "123!@#$%@yahoo.com";
            action.IDeleteValueOnFields(driver, Elements.Forms_Email_InputBox);
            action.IType(driver, Elements.Forms_Email_InputBox, email_input);
            action.IType(driver, Elements.Forms_Email_InputBox, Keys.Tab);

            //--- Expected Result: The error message should be displayed: "Please enter a valid email address" ---//
            test.validateStringIsCorrect(driver, Elements.Forms_AdiSimClkRequestForSoftware_EmailId_InvalidInput_ErrorMessage, emailId_invalidInput_errorMessage);
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Input Validations are Working as Expected for Blank Inputs in EN Locale")]
        public void Forms_AdiSimClkRequestForSoftware_VerifyInputValidationsForBlankInputs(string Locale)
        {
            //--- ADIsimCLK Request For Software Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + adiSimClkRequestForSoftware_form_url;

            //--- Action: Open "ADIsimCLK Request For Software" form ---//
            action.Navigate(driver, form_url);

            //----- Form TestPlan > EN Tab > R14 > T4_1: Submit ADISIMCLK  Form   with no data on mandatory fields -----//

            //--- Action: Click on the Submit button without entering data on the mandatory fields. ---//
            action.IClick(driver, Elements.Forms_Submit_Button);

            //----- About you -----//

            //--- Expected Result: The Email Id: "Email address is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_AdiSimClkRequestForSoftware_EmailId_BlankInput_ErrorMessage, emailId_blankInput_errorMessage);

            //--- Expected Result: The FristName: "First Name is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_FirstName_BlankInput_ErrorMessage, firstName_blankInput_errorMessage);

            //--- Expected Result: The LastName: "Last Name is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_LastName_BlankInput_ErrorMessage, lastName_blankInput_errorMessage);

            //----- Form TestPlan > EN Tab > R14 > T6: Verify the CLK form update and drop down change. (AL-8895) -----//

            //--- Expected Result: An error message "Please Select Job Title" should be displayed under "Job Title" drop down menu field. ---//
            test.validateStringIsCorrect(driver, Elements.Forms_JobTitle_Dropdown_BlankInput_ErrorMessage, jobTitle_blankInput_errorMessage);

            //----- Your detailed information -----//

            //--- Expected Result: The Organization Name: "Organization name is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_OrganizationName_BlankInput_ErrorMessage, organizationName_blankInput_errorMessage);

            //--- Expected Result: The Address: "Address Line 1 is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_Address_BlankInput_ErrorMessage, address_blankInput_errorMessage);

            //--- Expected Result: The Telephone: "Telephone is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_AdiSimClkRequestForSoftware_Telephone_BlankInput_ErrorMessage, telephone_blankInput_errorMessage);

            //--- Expected Result: The City:  "City is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_City_BlankInput_ErrorMessage, city_blankInput_errorMessage);

            //--- Expected Result: The State/ Province (dropdown): "State/ Province is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_StateProvince_BlankInput_ErrorMessage, state_blankInput_errorMessage);

            //--- Expected Result: The Zip/Postal Code: "Zip/Postal Code is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_ZipPostalCode_BlankInput_ErrorMessage, zipPostalCode_blankInput_errorMessage);

            //--- Expected Result: The Country(dropdown): "Country is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_CountryRegion_BlankInput_ErrorMessage, countryRegion_blankInput_errorMessage);

            //----- Applications Data -----//

            //--- Expected Result: The What is your circuit application?: "Please Select What is your circuit application?" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_WhatIsYourCircuitApplication_BlankInput_ErrorMessage, whatIsYourCircuitApplication_blankInput_errorMessage);

            //--- Expected Result: The What is your end market?: "Please Select What is your end market?" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_WhatIsYourEndMarket_BlankInput_ErrorMessage, whatIsYourEndMarket_blankInput_errorMessage);

            //--- Expected Result: The Input Frequency: "Input Frequency is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_InputFrequency_BlankInput_ErrorMessage, inputFrequency_blankInput_errorMessage);

            //--- Expected Result: The Output Frequency: "Output  Frequency is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_OutputFrequency_BlankInput_ErrorMessage, outputFrequency_blankInput_errorMessage);
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Form Submission is Working as Expected in EN Locale")]
        public void Forms_AdiSimClkRequestForSoftware_VerifyFormSubmission(string Locale)
        {
            if (Util.Configuration.Environment.Equals("dev") || Util.Configuration.Environment.Equals("qa"))
            {
                //--- ADIsimCLK Request For Software Form URL ---//
                string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + adiSimClkRequestForSoftware_form_url;

                //--- Action: Open "ADIsimCLK Request For Software" form ---//
                action.Navigate(driver, form_url);

                //----- Form TestPlan > EN Tab > R14 > T4_3: Submit ADISIMCLK form with valid data (AL-2496) -----//

                //--- Action: Enter valid data on the mandatory fields. ---//

                //----- About you -----//
                string email_input = "t3st_" + Util.Configuration.Environment.ToLower() + "_adiSimClkRequestForSoftware_f0rm_" + util.GenerateRandomNumber() + "@mailinator.com";
                //Console.WriteLine("Email Id Input = " + email_input);
                action.IDeleteValueOnFields(driver, Elements.Forms_Email_InputBox);
                action.IType(driver, Elements.Forms_Email_InputBox, email_input);

                string firstName_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_FirstName_InputBox);
                action.IType(driver, Elements.Forms_FirstName_InputBox, firstName_input);

                string lastName_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_LastName_InputBox);
                action.IType(driver, Elements.Forms_LastName_InputBox, lastName_input);

                action.IClick(driver, Elements.Forms_JobTitle_Dropdown);
                action.IClick(driver, By.CssSelector("select[id$='Title']>option:nth-of-type(2)"));

                //----- Your detailed information -----//
                string organizationName_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_OrganizationName_InputBox);
                action.IType(driver, Elements.Forms_OrganizationName_InputBox, organizationName_input);

                string address_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_Address_InputBox);
                action.IType(driver, Elements.Forms_Address_InputBox, address_input);

                string telephone_input = "123456789";
                action.IDeleteValueOnFields(driver, Elements.Forms_Telephone_InputBox);
                action.IType(driver, Elements.Forms_Telephone_InputBox, telephone_input);

                string countryRegion_input = "US";
                action.ISelectFromDropdown(driver, Elements.Forms_CountryRegion_Dropdown, countryRegion_input);

                action.IClick(driver, Elements.Forms_StateProvince_Dropdown);
                action.IClick(driver, By.CssSelector("select[id$='tate']>option:nth-of-type(3)"));

                string city_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_City_InputBox);
                action.IType(driver, Elements.Forms_City_InputBox, city_input);

                string zipPostalCode_input = "12345";
                action.IDeleteValueOnFields(driver, Elements.Forms_ZipPostalCode_InputBox);
                action.IType(driver, Elements.Forms_ZipPostalCode_InputBox, zipPostalCode_input);

                //----- Applications Data -----//
                action.IClick(driver, Elements.Forms_WhatIsYourCircuitApplication_Dropdown);
                action.IClick(driver, By.CssSelector("select[id$='CircuitApp']>option:nth-of-type(2)"));

                action.IClick(driver, Elements.Forms_WhatIsYourEndMarket_Dropdown);
                action.IClick(driver, By.CssSelector("select[id$='EndMarket']>option:nth-of-type(2)"));

                string inputFrequency_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_InputFrequency_InputBox);
                action.IType(driver, Elements.Forms_InputFrequency_InputBox, inputFrequency_input);

                string outputFrequency_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_OutputFrequency_InputBox);
                action.IType(driver, Elements.Forms_OutputFrequency_InputBox, outputFrequency_input);

                //----- Privacy Settings -----//
                action.ICheck(driver, Elements.Forms_Communication_Checkbox);

                //--- Action: Click on submit button ---//
                action.IClick(driver, Elements.Forms_Submit_Button);

                //--- Expected Result: ADIsimCLK Version Request for Software Page should be displayed ---//
                test.validateStringInstance(driver, driver.Url, adiSimClkVersionRequestForSoftware_page_url);

                if (driver.Url.Contains(adiSimClkVersionRequestForSoftware_page_url))
                {
                    if (util.CheckElement(driver, Elements.AdiSimClkReferenceDesignFiles_AdiSimClkReferenceDesignFiles_Link, 1))
                    {
                        //--- Action: Click the ADIsimCLK™ Reference Design Files on the Thank You page ---//
                        action.IOpenLinkInNewTab(driver, Elements.AdiSimClkReferenceDesignFiles_AdiSimClkReferenceDesignFiles_Link);
                        Thread.Sleep(2000);

                        //--- Expected Result: ADIsimCLK™ Reference Design Files page should be displayed ---//
                        test.validateStringInstance(driver, driver.Url, adiSimClkReferenceDesignFiles_page_url);
                    }
                }
            }
        }
    }
}