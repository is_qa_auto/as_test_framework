﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using OpenQA.Selenium;
using System;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.Forms
{
    [TestFixture]
    public class Forms_JapanTechnicalSupport : BaseSetUp
    {
        //--- Japan Technical Support Form URL: ---//
        //--- DEV: https://form-dev.corpnt.analog.com/form_pages/japan/technicalsupport.aspx ---//
        //--- QA: https://form-qa.corpnt.analog.com/form_pages/japan/technicalsupport.aspx ---//
        //--- PROD: https://form.analog.com/form_pages/japan/technicalsupport.aspx ---//

        public Forms_JapanTechnicalSupport() : base() { }

        //--- Login Credentials ---//
        string username = "f0rms_t3st_555@mailinator.com";
        string password = "Test_1234";

        //--- URLs ---//
        string japanTechnicalSupport_form_url = "form_pages/japan/technicalsupport.aspx";
        string registration_page_url = "/app/registration";
        string japanTechnicalSupport_thankYou_page_url = "/contact-techsupport-thankyou.html";

        /****Removing this as the recent change requires user to login first****/
        //[Test, Category("Forms"), Category("NonCore"), Retry(2)]
        //[TestCase("jp", TestName = "Verify that the Elements are Present and Working as Expected when the User is Logged Out in JP Locale")]
        //public void Forms_JapanTechnicalSupport_VerifyElementsWhenUserIsLoggedOut(string Locale)
        //{
        //    //--- Japan Technical Support Form URL ---//
        //    string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + japanTechnicalSupport_form_url;

        //    //--- Action: Open Japan Technical Support Form ---//
        //    action.Navigate(driver, form_url);

        //    //----- Form TestPlan > JP Tab > R1 > T4_1: Submit Japan Technical Support  Form  with no data on mandatory fields  -----//
        //    action.ILogin(driver, username, password);
        //    //----- プライバシー設定 (Privacy Settings) -----//

        //    //--- Expected Result: 送信(Submit) button should be disabled. ---//
        //    test.validateElementIsEnabled(driver, Elements.Forms_Submit_Button);
        
        //    //----- Form TestPlan > JP Tab > R1 > T3_1: Verify the 技術的なお問合せはmyAnalogに登録／ログインが必要です (Save time by logging in or registering for myAnalog) accordion (AL-5704, IQ-7685/AL-1493)-----//

        //    //----- 技術的なお問合せはmyAnalogに登録／ログインが必要です (Save time by logging in or registering for myAnalog) -----//

        //    //--- Expected Result: Upon page load, the myAnalogにご登録済みの方はログイン+が可能です。 accordion should be expanded. ---//
        //    //test.validateElementIsPresent(driver, Elements.Forms_Expanded_SaveTimeByLoggingInOrRegisteringForMyAnalog_Panel);

        //    if (util.CheckElement(driver, Elements.Forms_Expanded_SaveTimeByLoggingInOrRegisteringForMyAnalog_Panel_RegisterNow_Link, 1))
        //    {
        //        //--- Action: Click the 新規登録 hyperlink ---//
        //        action.IOpenLinkInNewTab(driver, Elements.Forms_Expanded_SaveTimeByLoggingInOrRegisteringForMyAnalog_Panel_RegisterNow_Link);
        //        Thread.Sleep(1000);

        //        //--- Expected Result: The System displays the Registration page ---//
        //        test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + registration_page_url);

        //        driver.Close();
        //        driver.SwitchTo().Window(driver.WindowHandles.Last());

        //        //--- Action: Click the 'myAnalogへログイン' button ---//
        //        action.IClick(driver, Elements.Forms_Expanded_SaveTimeByLoggingInOrRegisteringForMyAnalog_Panel_LoginToMyAnalog_Button);
        //        Thread.Sleep(3000);

        //        //--- Expected Result: The System displays the B2C Login page ---//
        //        test.validateStringInstance(driver, driver.Url, "b2clogin.com");
        //    }
        //    else
        //    {
        //        test.validateElementIsPresent(driver, Elements.Forms_Expanded_SaveTimeByLoggingInOrRegisteringForMyAnalog_Panel_RegisterNow_Link);
        //    }
        //}

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("jp", TestName = "Verify that the Elements are Present and Working as Expected when the User is Logged In in JP Locale")]
        public void Forms_JapanTechnicalSupport_VerifyElementsWhenUserIsLoggedIn(string Locale)
        {
            //--- Japan Technical Support Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + japanTechnicalSupport_form_url;

            //--- Action: Open Japan Technical Support Form ---//
            action.Navigate(driver, form_url);

            //----- Form TestPlan > JP Tab > R1 > T3_4: Login using Save time to login section with valid data -----//

            //----- 技術的なお問合せはmyAnalogに登録／ログインが必要です (Save time by logging in or registering for myAnalog) -----//

            //--- Action: Login using valid data ---//
            action.ILogin(driver, username, password);
            Thread.Sleep(2000);

            driver.Navigate().Refresh();
            Thread.Sleep(1000);

            //----- お客様情報 (Customer Information) -----//

            //--- Expected Result: The メールアドレス (Email Address) field should be automatically populated and disabled ---//
            test.validateFieldIsNotEmpty(driver, Elements.Forms_Email_InputBox);
            test.validateElementIsNotEnabled(driver, Elements.Forms_Email_InputBox);

            //--- Expected Result: The 姓 (Surname) field should be automatically populated and disabled ---//
            test.validateFieldIsNotEmpty(driver, Elements.Forms_FamilyName_InputBox);
            test.validateElementIsNotEnabled(driver, Elements.Forms_FamilyName_InputBox);

            //--- Expected Result: The 名 (Name) field should be automatically populated and disabled ---//
            test.validateFieldIsNotEmpty(driver, Elements.Forms_GivenName_InputBox);
            test.validateElementIsNotEnabled(driver, Elements.Forms_GivenName_InputBox);

            //--- Expected Result: The 社名＆団体名 (Company Name and Organization) field should be automatically populated and disabled ---//
            test.validateFieldIsNotEmpty(driver, Elements.Forms_CompanyName_InputBox);
            test.validateElementIsNotEnabled(driver, Elements.Forms_CompanyName_InputBox);
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("jp", TestName = "Verify that the Input Validations are Working as Expected for Blank Inputs in JP Locale")]
        public void Forms_JapanTechnicalSupport_VerifyInputValidationsForBlankInputs(string Locale)
        {
            //--- Japan Technical Support Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + japanTechnicalSupport_form_url;

            //--- Action: Open Japan Technical Support Form ---//
            action.Navigate(driver, form_url);

            //----- Form TestPlan > JP Tab > R3 > T1: Verify that the "Distributor with dealings" field is removed from the Japanese Technical Support Form page (IQ-5620/AL-13053) -----//

            //----- 技術的なお問合せはmyAnalogに登録／ログインが必要です (Save time by logging in or registering for myAnalog) -----//

            //--- Action: Login using valid data ---//
            action.ILogin(driver, username, password);

            //--- Action: Click on submit button ---//
            action.IClick(driver, Elements.Forms_Submit_Button);
            Thread.Sleep(4000);

            //----- お客様情報 (Customer Information) -----//

            //--- Expected Result: セイ textbox: Error message is displayed ---//
            test.validateElementIsPresent(driver, Elements.Forms_JapanTechnicalSupport_FamilyNamePronounciation_BlankInput_ErrorMessage);

            //--- Expected Result: メイ textbox: Error message is displayed ---//
            test.validateElementIsPresent(driver, Elements.Forms_JapanTechnicalSupport_GivenNamePronounciation_BlankInput_ErrorMessage);

            //----- 設計について (About Design) -----//

            //--- Expected Result: アプリケーション dropdown box: Error message is displayed ---//
            test.validateElementIsPresent(driver, Elements.Forms_JapanTechnicalSupport_DescribeApplication_BlankInput_ErrorMessage);

            //--- Expected Result: 製品番号 textbox: Error message is displayed ---//
            test.validateElementIsPresent(driver, Elements.Forms_JapanTechnicalSupport_PartNumber_BlankInput_ErrorMessage);

            //--- Expected Result: 年間数量 dropdown box: Error message is displayed ---//
            test.validateElementIsPresent(driver, Elements.Forms_JapanTechnicalSupport_Volume_BlankInput_ErrorMessage);

            //--- Expected Result: ご質問内容 textbox: Error message is displayed ---//
            test.validateElementIsPresent(driver, Elements.Forms_JapanTechnicalSupport_QuestionsComments_BlankInput_ErrorMessage);
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("jp", TestName = "Verify that the Form Submission is Working as Expected in JP Locale")]
        public void Forms_JapanTechnicalSupport_VerifyFormSubmission(string Locale)
        {
            if (Util.Configuration.Environment.Equals("dev") || Util.Configuration.Environment.Equals("qa"))
            {
                //--- Japan Technical Support Form URL ---//
                string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + japanTechnicalSupport_form_url;

                //--- Action: Open Japan Technical Support Form ---//
                action.Navigate(driver, form_url);

                //----- Form TestPlan > JP Tab > R1 > T4_3: Submit Japan Technical Support  Form with valid data (AL-2496) -----//

                //--- Action: Enter valid data on the mandatory fields. ---//

                //----- 技術的なお問合せはmyAnalogに登録／ログインが必要です (Save time by logging in or registering for myAnalog) -----//
                string email_input = "t3st_japanTechnicalSupport_f0rm_555@mailinator.com";
                //Console.WriteLine("Email Input = " + email_input);
                action.ILogin(driver, email_input, password);

                //----- お客様情報 (Customer Information) -----//
                if (driver.FindElement(Elements.Forms_FamilyNamePronounciation_InputBox).Enabled)
                {
                    string familyNamePronounciation_input = "This is Just A Test";
                    action.IDeleteValueOnFields(driver, Elements.Forms_FamilyNamePronounciation_InputBox);
                    action.IType(driver, Elements.Forms_FamilyNamePronounciation_InputBox, familyNamePronounciation_input);
                }

                if (driver.FindElement(Elements.Forms_GivenNamePronounciation_InputBox).Enabled)
                {
                    string givenNamePronounciation_input = "This is Just A Test";
                    action.IDeleteValueOnFields(driver, Elements.Forms_GivenNamePronounciation_InputBox);
                    action.IType(driver, Elements.Forms_GivenNamePronounciation_InputBox, givenNamePronounciation_input);
                }

                if (driver.FindElement(Elements.Forms_SectionGroupName_InputBox).Enabled)
                {
                    string sectionGroupName_input = "This is Just A Test";
                    action.IDeleteValueOnFields(driver, Elements.Forms_SectionGroupName_InputBox);
                    action.IType(driver, Elements.Forms_SectionGroupName_InputBox, sectionGroupName_input);
                }

                if (driver.FindElement(Elements.Forms_Telephone_InputBox).Enabled)
                {
                    string telephone_input = "123456789";
                    action.IDeleteValueOnFields(driver, Elements.Forms_Telephone_InputBox);
                    action.IType(driver, Elements.Forms_Telephone_InputBox, telephone_input);
                }

                if (driver.FindElement(Elements.Forms_ZipPostalCode_InputBox).Enabled)
                {
                    string zipCode_input = "12345";
                    action.IDeleteValueOnFields(driver, Elements.Forms_ZipPostalCode_InputBox);
                    action.IType(driver, Elements.Forms_ZipPostalCode_InputBox, zipCode_input);
                }

                if (driver.FindElement(Elements.Forms_Prefecture_InputBox).Enabled)
                {
                    string prefecture_input = "This is Just A Test";
                    action.IDeleteValueOnFields(driver, Elements.Forms_Prefecture_InputBox);
                    action.IType(driver, Elements.Forms_Prefecture_InputBox, prefecture_input);
                }

                if (driver.FindElement(Elements.Forms_CityAndOthers_InputBox).Enabled)
                {
                    string cityAndOthers_input = "This is Just A Test";
                    action.IDeleteValueOnFields(driver, Elements.Forms_CityAndOthers_InputBox);
                    action.IType(driver, Elements.Forms_CityAndOthers_InputBox, cityAndOthers_input);
                }

                if (driver.FindElement(Elements.Forms_TheOthers_InputBox).Enabled)
                {
                    string theOthers_input = "This is Just A Test";
                    action.IDeleteValueOnFields(driver, Elements.Forms_TheOthers_InputBox);
                    action.IType(driver, Elements.Forms_TheOthers_InputBox, theOthers_input);
                }

                //----- 設計について (About Design) -----//

                if (driver.FindElement(Elements.Forms_DescribeApplication_Dropdown).Enabled)
                {
                    action.IClick(driver, Elements.Forms_DescribeApplication_Dropdown);
                    action.IClick(driver, By.CssSelector("select[id$='DescribeApplication']>option:nth-of-type(2)"));
                }

                if (driver.FindElement(Elements.Forms_PartNumber_InputBox).Enabled)
                {
                    string partNumber_input = "This is Just A Test";
                    action.IDeleteValueOnFields(driver, Elements.Forms_PartNumber_InputBox);
                    action.IType(driver, Elements.Forms_PartNumber_InputBox, partNumber_input);
                }

                if (driver.FindElement(Elements.Forms_Volume_Dropdown).Enabled)
                {
                    action.IClick(driver, Elements.Forms_Volume_Dropdown);
                    action.IClick(driver, By.CssSelector("select[id$='Volume']>option:nth-of-type(2)"));
                }

                if (driver.FindElement(Elements.Forms_DevelopmentSchedule_Dropdown).Enabled)
                {
                    action.IClick(driver, Elements.Forms_DevelopmentSchedule_Dropdown);
                    action.IClick(driver, By.CssSelector("select[id*='DevelopmentSchedule']>option:nth-of-type(2)"));
                }

                if (driver.FindElement(Elements.Forms_BestEstimate_Dropdown).Enabled)
                {
                    action.IClick(driver, Elements.Forms_BestEstimate_Dropdown);
                    action.IClick(driver, By.CssSelector("select[id$='BestEstimate']>option:nth-of-type(2)"));
                }

                if (driver.FindElement(Elements.Forms_QuestionsComments_TextArea).Enabled)
                {
                    string questionsComments_input = "This is Just A Test";
                    action.IDeleteValueOnFields(driver, Elements.Forms_QuestionsComments_TextArea);
                    action.IType(driver, Elements.Forms_QuestionsComments_TextArea, questionsComments_input);
                }

                //----- プライバシー設定 (Privacy Settings) -----//
                action.ICheck(driver, Elements.Forms_Communication_Checkbox);

                //--- Action: Click on submit button ---//
                action.IClick(driver, Elements.Forms_Submit_Button);

                //--- Expected Result: Japan Technical Support Thank You page should be displayed ---//
                test.validateStringInstance(driver, driver.Url, japanTechnicalSupport_thankYou_page_url);

                //if (driver.Url.Contains(japanTechnicalSupport_thankYou_page_url))
                //{
                //    action.ICheckMailinatorEmail(driver, email_input);

                //    //--- Expected Result: The user will receive a mail from "crmdev.apps.japan@analog.com" ---//
                //    test.validateStringIsCorrect(driver, Elements.Mailinator_LatestEmail_Sender, "crmdev.apps.japan@analog.com");
                //}             
            }
        }
    }
}