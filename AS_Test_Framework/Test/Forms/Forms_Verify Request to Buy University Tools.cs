﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using OpenQA.Selenium;
using System;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.Forms
{
    [TestFixture]
    public class Forms_RequestToBuyUniversityTools : BaseSetUp
    {
        //--- Request to Buy University Tools Form URL: ---//
        //--- DEV: https://form-dev.corpnt.analog.com/form_pages/dsp/buyuniversitytools.aspx ---//
        //--- QA: https://form-qa.corpnt.analog.com/form_pages/dsp/buyuniversitytools.aspx ---//
        //--- PROD: https://form.analog.com/form_pages/dsp/buyuniversitytools.aspx ---//

        public Forms_RequestToBuyUniversityTools() : base() { }

        //--- Login Credentials ---//
        string username = "f0rms_t3st_555@mailinator.com";
        string password = "Test_1234";

        //--- URLs ---//
        string requestToBuyUniversityTools_form_url = "form_pages/dsp/buyuniversitytools.aspx";
        string registration_page_url = "/app/registration";
        string salesAndDistribution_page_url = "/sales-distribution.html";
        string thankYou_page_url = "/universityDonation_thankyou.html";

        //--- Labels ---//
        string countryRegion_text = "Country/Region";
        string courseResearch_1_blankInput_errorMessage = "Course/Research is required";
        string numberOfStudents_1_blankInput_errorMessage = "Number of students is required";
        string email_blankInput_errorMessage = "Email address is required";
        string university_blankInput_errorMessage = "University is required";
        string title_blankInput_errorMessage = "Title is required";
        string fax_blankInput_errorMessage = "Fax is required";
        string addressLine_1_blankInput_errorMessage = "Address is required";
        string name_blankInput_errorMessage = "Name is required";
        string department_blankInput_errorMessage = "Department is required";
        string telephone_blankInput_errorMessage = "Telephone is required";
        string city_blankInput_errorMessage = "City is required";
        string stateProvince_blankInput_errorMessage = "State/Province is required";
        string zipPostalCode_blankInput_errorMessage = "Zip is required";
        string countryRegion_blankInput_errorMessage = "Country/Region is required";
        string quantity_invalidInput_errorMessage = "Value must\r\nbe a whole number between 1 and 999.";
        string email_invalidInput_errorMessage = "Please enter a valid email address";

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Elements are Present and Working as Expected when the User is Logged Out in EN Locale")]
        public void Forms_RequestToBuyUniversityTools_VerifyElementsWhenUserIsLoggedOut(string Locale)
        {
            //--- Request to Buy University Tools Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + requestToBuyUniversityTools_form_url;

            //--- Action: Access Buy University Tools form page ---//
            action.Navigate(driver, form_url);

            //----- Provide shipping address for development tools -----//

            //----- Form TestPlan > EN Tab > R40 > T1: Verify all labels for Country is changed to "Country/Region" (IQ-4970/AL-12466) -----//

            //--- Expected Result: The Country label has been changed to Country/Region ---//
            test.validateStringInstance(driver, util.GetText(driver, Elements.Forms_Country_Label), countryRegion_text);

            //----- Privacy Settings -----//

            //----- Form TestPlan > EN Tab > R38 > T1: Verify Privacy Settings in All Forms [account is not logged in] (AL-11345) -----//

            //--- Expected Result: Privacy Settings section is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_Section);

            //--- Expected Result: Prompt "You can change your privacy settings at any time by clicking on the unsubscribe link in emails sent from Analog Devices or in Analog’s Privacy Settings." is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_LegalDisclaimer_Text);

            //--- Expected Result: Privacy Settings is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_Link);

            //--- Expected Result: Privacy & Security Statement ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacyAndSecurityStatement_Link);

            //--- Expected Result: Authorized Partners ---//
            test.validateElementIsPresent(driver, Elements.Forms_AuthorizedPartners_Link);

            //----- Save time by logging in or registering for myAnalog -----//

            //----- Form TestPlan > EN Tab > R2 > T3_1: Login using Save time to login section with valid data -----//

            if (!util.CheckElement(driver, Elements.Forms_Expanded_SaveTimeByLoggingInOrRegisteringForMyAnalog_Panel, 1))
            {
                //--- Action: Click on Save time by logging in or registering for myAnalog link ---//
                action.IClick(driver, Elements.Forms_SaveTimeByLoggingInOrRegisteringForMyAnalog_Link);
            }

            //--- Expected Result: Save time to login section should be displayed (AL-7439) ---//
            test.validateElementIsPresent(driver, Elements.Forms_Expanded_SaveTimeByLoggingInOrRegisteringForMyAnalog_Panel);

            //----- Form TestPlan > EN Tab > R2 > T16: Remove the dependency of old URL of myAnalog in Forms (IQ-5851/AL-13134) -----//

            if (util.CheckElement(driver, Elements.Forms_Expanded_SaveTimeByLoggingInOrRegisteringForMyAnalog_Panel_RegisterNow_Link, 1))
            {
                //--- Action: Click the Register now ---//
                action.IOpenLinkInNewTab(driver, Elements.Forms_Expanded_SaveTimeByLoggingInOrRegisteringForMyAnalog_Panel_RegisterNow_Link);

                //--- Expected Result: The Link should redirect to the Registration Page ---//
                test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + registration_page_url);

                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles.Last());
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.Forms_Expanded_SaveTimeByLoggingInOrRegisteringForMyAnalog_Panel_RegisterNow_Link);
            }

            //----- Select the development tools to purchase -----//

            //----- Form TestPlan > EN Tab > R2 > T4: Verify the Table on Request to Buy University Tools Form -----//

            //--- Expected Result: Table Title should be shown ---//
            test.validateElementIsPresent(driver, Elements.Forms_SelectTheDevelopmentToolsToPurchase_Section_Label);

            //--- Expected Result: Table Description should be shown ---//
            test.validateElementIsPresent(driver, Elements.Forms_SelectTheDevelopmentToolsToPurchase_Section_Description_Text);

            //--- Expected Result: Table itslef on Request to buy university tools should be shown ---//
            test.validateElementIsPresent(driver, Elements.Forms_SelectTheDevelopmentToolsToPurchase_Section_Table);

            //--- Expected Result: Reminder message should be shown ---//
            test.validateElementIsPresent(driver, Elements.Forms_SelectTheDevelopmentToolsToPurchase_Section_Reminder_Message);

            if (util.CheckElement(driver, Elements.Forms_SelectTheDevelopmentToolsToPurchase_Section_Distributor_Link, 1))
            {
                //--- Action: Click on Distributor link on the reminder message on the he Select the development tools to purchase table ---//
                action.IOpenLinkInNewTab(driver, Elements.Forms_SelectTheDevelopmentToolsToPurchase_Section_Distributor_Link);
                Thread.Sleep(3000);

                //--- Expected Result: It should redirect on Sales and Distributors page ---//
                test.validateStringInstance(driver, driver.Url, salesAndDistribution_page_url);
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.Forms_SelectTheDevelopmentToolsToPurchase_Section_Distributor_Link);
            }
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Elements are Present and Working as Expected when the User is Logged In in EN Locale")]
        public void Forms_RequestToBuyUniversityTools_VerifyElementsWhenUserIsLoggedIn(string Locale)
        {
            //--- Request to Buy University Tools Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + requestToBuyUniversityTools_form_url;

            //--- Action: Access Buy University Tools form page ---//
            action.Navigate(driver, form_url);

            //----- Save time by logging in or registering for myAnalog -----//

            //----- Form TestPlan > EN Tab > R2 > T3_1: Login using Save time to login section with valid data -----//

            //--- Action: Login using valid data ---//
            action.ILogInViaSaveTimeByLoggingInPanel(driver, username, password);
            Thread.Sleep(3000);

            //----- Provide shipping address for development tools -----//

            //--- Expected Result: The Email field should be populated ---//
            test.validateFieldIsNotEmpty(driver, Elements.Forms_Email_InputBox);

            //--- Expected Result: The Name field should be populated ---//
            test.validateFieldIsNotEmpty(driver, Elements.Forms_Name_InputBox);

            //--- Expected Result: The State/Province(Dropdown) should be populated ---//
            test.validateStringChanged(driver, util.GetSelectedDropdownValue(driver, Elements.Forms_StateProvince_Dropdown), "Select State/Province");

            //--- Expected Result: The Zip/ Postal Code field should be populated ---//
            test.validateFieldIsNotEmpty(driver, Elements.Forms_ZipPostalCode_InputBox);

            //--- Expected Result: The Country(dropdown) field should be populated ---//
            test.validateStringChanged(driver, util.GetSelectedDropdownValue(driver, Elements.Forms_CountryRegion_Dropdown), "Select Country/Region");

            //----- Form TestPlan > EN Tab > R38 > T1: Verify Privacy Settings in All Forms [account is logged in] (AL-11345) -----//

            //----- Privacy Settings -----//

            //--- Expected Result: Prompt "You can change your privacy settings at any time by clicking on the unsubscribe link in emails sent from Analog Devices or in Analog’s Privacy Settings." is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_LegalDisclaimer_Text);

            //--- Expected Result: Privacy Settings is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_Link);

            //--- Expected Result: Privacy & Security Statement ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacyAndSecurityStatement_Link);

            //--- Expected Result: Authorized Partners ---//
            test.validateElementIsPresent(driver, Elements.Forms_AuthorizedPartners_Link);
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Input Validations are Working as Expected for Valid Inputs in EN Locale")]
        public void Forms_RequestToBuyUniversityTools_VerifyInputValidationsForValidInputs(string Locale)
        {
            //--- Request to Buy University Tools Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + requestToBuyUniversityTools_form_url;

            //--- Action: Access Buy University Tools form page ---//
            action.Navigate(driver, form_url);

            //----- Select the development tools to purchase -----//

            //----- Form TestPlan > EN Tab > R2 > T4: Verify the Table on Request to Buy University Tools Form -----//

            //--- Expected Result: Quantity textboxes should be enabled ---//
            test.validateElementIsEnabled(driver, Elements.Forms_SelectTheDevelopmentToolsToPurchase_Section_Quantity_InputBoxes);

            //--- Action: Enter a number on quantity text box for any model number (e.g. 2) ---//
            string quantity_input = "2";
            action.IDeleteValueOnFields(driver, Elements.Forms_SelectTheDevelopmentToolsToPurchase_Section_Quantity_InputBoxes);
            action.IType(driver, Elements.Forms_SelectTheDevelopmentToolsToPurchase_Section_Quantity_InputBoxes, quantity_input);
            action.IType(driver, Elements.Forms_SelectTheDevelopmentToolsToPurchase_Section_Quantity_InputBoxes, Keys.Tab);

            //--- Expected Result: The total column  text box of that specific model should have the total amount depending on the number of the quantity and price of the model (price * quantity = total) ---//
            test.validateStringInstance(driver, util.GetInputBoxValue(driver, Elements.Forms_SelectTheDevelopmentToolsToPurchase_Section_ReadOnly_Total_InputBoxes), (Int32.Parse(util.ExtractNumber(driver, Elements.Forms_SelectTheDevelopmentToolsToPurchase_Section_Price_Values)) * Int32.Parse(quantity_input)).ToString("0.##"));

            //----- Form TestPlan > EN Tab > R2 > T7: Verify the value on the Total column is NOT editable (AL-5929) -----//

            //--- Expected Result: The Total cost should not be editable ---//
            test.validateElementIsPresent(driver, Elements.Forms_SelectTheDevelopmentToolsToPurchase_Section_ReadOnly_Total_InputBoxes);
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Input Validations are Working as Expected for Invalid Inputs in EN Locale")]
        public void Forms_RequestToBuyUniversityTools_VerifyInputValidationsForInvalidInputs(string Locale)
        {
            //--- Request to Buy University Tools Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + requestToBuyUniversityTools_form_url;

            //--- Action: Access Buy University Tools form page ---//
            action.Navigate(driver, form_url);

            //----- Select the development tools to purchase -----//

            //----- Form TestPlan > EN Tab > R2 > T5_1: Verify the Quantity fields on the Select the development tools to purchase table are NOT accepting invalid values (AL-5969) -----//

            //--- Action: Enter special character on the quantity field on the first tool (e.g. !@#)---//
            string quantity_input = "!@#";
            action.IDeleteValueOnFields(driver, Elements.Forms_SelectTheDevelopmentToolsToPurchase_Section_Quantity_InputBoxes);
            action.IType(driver, Elements.Forms_SelectTheDevelopmentToolsToPurchase_Section_Quantity_InputBoxes, quantity_input);
            action.IType(driver, Elements.Forms_SelectTheDevelopmentToolsToPurchase_Section_Quantity_InputBoxes, Keys.Tab);

            //--- Expected Result: The total column  text box of that specific model should not have any value ---//
            test.validateStringInstance(driver, "", util.GetInputBoxValue(driver, Elements.Forms_SelectTheDevelopmentToolsToPurchase_Section_ReadOnly_Total_InputBoxes));

            //--- Expected Result: The error message below should be displayed: "Value must be a whole number between 1 and 999" ---//
            test.validateStringIsCorrect(driver, Elements.Forms_SelectTheDevelopmentToolsToPurchase_Section_Quantity_Range_ErrorMessage, quantity_invalidInput_errorMessage);

            //----- Provide shipping address for development tools -----//

            //----- Form TestPlan > EN Tab > R2 > T8_2: Submit the Request to Buy University Tools  form with invalid data -----//

            //--- Action: Enter invalid data on the Email Address field. (e.g. 123!@#$%@yahoo.com) ---//
            string email_input = "123!@#$%@yahoo.com";
            action.IDeleteValueOnFields(driver, Elements.Forms_Email_InputBox);
            action.IType(driver, Elements.Forms_Email_InputBox, email_input);
            action.IType(driver, Elements.Forms_Email_InputBox, Keys.Tab);

            //--- Expected Result: The error message below should be displayed: "Please enter a valid email address" ---//
            test.validateStringIsCorrect(driver, Elements.Forms_RequestToBuyUniversityTools_Email_InvalidInput_ErrorMessage, email_invalidInput_errorMessage);
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Input Validations are Working as Expected for Blank Inputs in EN Locale")]
        public void Forms_RequestToBuyUniversityTools_VerifyInputValidationsForBlankInputs(string Locale)
        {
            //--- Request to Buy University Tools Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + requestToBuyUniversityTools_form_url;

            //--- Action: Access Buy University Tools form page ---//
            action.Navigate(driver, form_url);

            //----- Privacy Settings -----//

            //----- Form TestPlan > EN Tab > R2 > T8_1: Submit the Request to Buy University Tools  form with no data -----//

            //--- Action: Click on the Submit button without entering data on the mandatory fields ---//
            action.IClick(driver, Elements.Forms_Submit_Button);

            //----- Describe course(s) and/or research in which requested development tools will be used -----//

            //--- Expected Result: The error messages below should be displayed in Course/Research (1):  "Course/ Research is required" ---//
            test.validateStringIsCorrect(driver, Elements.Forms_RequestToBuyUniversityTools_CourseResearch_1_BlankInput_ErrorMessage, courseResearch_1_blankInput_errorMessage);

            //--- Expected Result: The error messages below should be displayed in # of students(1): "Number rof students is required" ---//
            test.validateStringIsCorrect(driver, Elements.Forms_RequestToBuyUniversityTools_NumberOfStudents_1_BlankInput_ErrorMessage, numberOfStudents_1_blankInput_errorMessage);

            //----- Provide shipping address for development tools -----//

            //--- Expected Result: The error messages below should be displayed in Email: "Email is required" ---//
            test.validateStringIsCorrect(driver, Elements.Forms_RequestToBuyUniversityTools_Email_BlankInput_ErrorMessage, email_blankInput_errorMessage);

            //--- Expected Result: The error messages below should be displayed in University: "University is required" ---//
            test.validateStringIsCorrect(driver, Elements.Forms_RequestToBuyUniversityTools_University_BlankInput_ErrorMessage, university_blankInput_errorMessage);

            //--- Expected Result: The error messages below should be displayed in Title: "Title is required" ---//
            test.validateStringIsCorrect(driver, Elements.Forms_RequestToBuyUniversityTools_Title_BlankInput_ErrorMessage, title_blankInput_errorMessage);

            //--- Expected Result: The error messages below should be displayed in Fax: "Fax is required" ---//
            test.validateStringIsCorrect(driver, Elements.Forms_RequestToBuyUniversityTools_Fax_BlankInput_ErrorMessage, fax_blankInput_errorMessage);

            //--- Expected Result: The error messages below should be displayed in Address Line 1: "Address is required" ---//
            test.validateStringIsCorrect(driver, Elements.Forms_RequestToBuyUniversityTools_AddressLine_1_BlankInput_ErrorMessage, addressLine_1_blankInput_errorMessage);

            //--- Expected Result: The error messages below should be displayed in Name: "Name is required" ---//
            test.validateStringIsCorrect(driver, Elements.Forms_RequestToBuyUniversityTools_Name_BlankInput_ErrorMessage, name_blankInput_errorMessage);

            //--- Expected Result: The error messages below should be displayed in Department: "Department is required" ---//
            test.validateStringIsCorrect(driver, Elements.Forms_RequestToBuyUniversityTools_Department_BlankInput_ErrorMessage, department_blankInput_errorMessage);

            //--- Expected Result: The error messages below should be displayed in Telephone: "Telephone is required" ---//
            test.validateStringIsCorrect(driver, Elements.Forms_RequestToBuyUniversityTools_Telephone_BlankInput_ErrorMessage, telephone_blankInput_errorMessage);

            //--- Expected Result: The error messages below should be displayed in City: "City is required" ---//
            test.validateStringIsCorrect(driver, Elements.Forms_RequestToBuyUniversityTools_City_BlankInput_ErrorMessage, city_blankInput_errorMessage);

            //--- Expected Result: The error messages below should be displayed in State/Province(dropdown): "State/ Province is required" ---//
            test.validateStringIsCorrect(driver, Elements.Forms_StateProvince_BlankInput_ErrorMessage, stateProvince_blankInput_errorMessage);

            //--- Expected Result: The error messages below should be displayed in Zip/Postal Code: "Zip is required" ---//
            test.validateStringIsCorrect(driver, Elements.Forms_RequestToBuyUniversityTools_ZipPostalCode_BlankInput_ErrorMessage, zipPostalCode_blankInput_errorMessage);

            //--- Expected Result: The error messages below should be displayed in Country(dropdown): "Country is required" ---//
            test.validateStringIsCorrect(driver, Elements.Forms_CountryRegion_BlankInput_ErrorMessage, countryRegion_blankInput_errorMessage);
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Form Submission is Working as Expected in EN Locale")]
        public void Forms_RequestToBuyUniversityTools_VerifyFormSubmission(string Locale)
        {
            if (Util.Configuration.Environment.Equals("dev") || Util.Configuration.Environment.Equals("qa"))
            {
                //--- Request to Buy University Tools Form URL ---//
                string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + requestToBuyUniversityTools_form_url;

                //--- Action: Access Buy University Tools form page ---//
                action.Navigate(driver, form_url);

                //----- Describe course(s) and/or research in which requested development tools will be used -----//

                //----- Form TestPlan > EN Tab > R2 > T8_3: Submit the Request to Buy University Tools  form with valid data -----//

                //--- Action: Enter valid data on the mandatory fields of Describe course(s) and/or research in which requested development tools will be used section ---//
                string courseResearch_1_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_CourseResearch_1_InputBox);
                action.IType(driver, Elements.Forms_CourseResearch_1_InputBox, courseResearch_1_input);

                string numberOfStudents_1_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_NumberOfStudents_1_InputBox);
                action.IType(driver, Elements.Forms_NumberOfStudents_1_InputBox, numberOfStudents_1_input);

                //----- Provide shipping address for development tools -----//

                //--- Action: Enter valid data on the mandatory fields of Provide Shipping address for development tools section ---//
                string email_input = "t3st_" + Util.Configuration.Environment.ToLower() + "_requestToBuyUniversityTools_f0rm_" +util.GenerateRandomNumber() + "@mailinator.com";
                //Console.WriteLine("Email Input = " + email_input);
                action.IDeleteValueOnFields(driver, Elements.Forms_Email_InputBox);
                action.IType(driver, Elements.Forms_Email_InputBox, email_input);

                string university_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_University_InputBox);
                action.IType(driver, Elements.Forms_University_InputBox, university_input);

                string title_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_Title_InputBox);
                action.IType(driver, Elements.Forms_Title_InputBox, title_input);

                string fax_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_Fax_InputBox);
                action.IType(driver, Elements.Forms_Fax_InputBox, fax_input);

                string addressLine_1_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_AddressLine_1_InputBox);
                action.IType(driver, Elements.Forms_AddressLine_1_InputBox, addressLine_1_input);

                string name_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_Name_InputBox);
                action.IType(driver, Elements.Forms_Name_InputBox, name_input);

                string department_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_Department_InputBox);
                action.IType(driver, Elements.Forms_Department_InputBox, department_input);

                string telephone_input = "123456789";
                action.IDeleteValueOnFields(driver, Elements.Forms_Telephone_InputBox);
                action.IType(driver, Elements.Forms_Telephone_InputBox, telephone_input);

                string city_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_City_InputBox);
                action.IType(driver, Elements.Forms_City_InputBox, city_input);

                string zipPostalCode_input = "12345";
                action.IDeleteValueOnFields(driver, Elements.Forms_ZipPostalCode_InputBox);
                action.IType(driver, Elements.Forms_ZipPostalCode_InputBox, zipPostalCode_input);

                string countryRegion_input = "US";
                action.ISelectFromDropdown(driver, Elements.Forms_CountryRegion_Dropdown, countryRegion_input);

                action.IClick(driver, Elements.Forms_StateProvince_Dropdown);
                action.IClick(driver, By.CssSelector("select[id$='tate']>option:nth-of-type(3)"));

                //----- Privacy Settings -----//
                action.ICheck(driver, Elements.Forms_Communication_Checkbox);

                //--- Action: Click on the Submit button ---//
                action.IClick(driver, Elements.Forms_Submit_Button);

                //--- Expected Result: Page should be redirected to Request to Buy University Tools  thank you page should be displayed ---//
                test.validateStringInstance(driver, driver.Url, thankYou_page_url);
            }
        }
    }
}