﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using OpenQA.Selenium;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.Forms
{
    [TestFixture]
    public class Forms_SoftwareDownload : BaseSetUp
    {
        //--- Software Download Form URL: ---//
        //--- DEV: https://form-dev.corpnt.analog.com/Form_Pages/softwaremodules/SoftwareModuleDownload.aspx?file=LSI3 ---//
        //--- QA: https://form-qa.corpnt.analog.com/Form_Pages/softwaremodules/SoftwareModuleDownload.aspx?file=LSI3 ---//
        //--- PROD: https://form.analog.com/Form_Pages/softwaremodules/SoftwareModuleDownload.aspx?file=LSI3 ---//

        public Forms_SoftwareDownload() : base() { }

        //--- Login Credentials ---//
        string username = "f0rms_t3st_555@mailinator.com";
        string password = "Test_1234";

        //--- URLs ---//
        string softwareDownload_form_url = "Form_Pages/softwaremodules/SoftwareModuleDownload.aspx?file=LSI3";
        string softwareModulesDownloadListing_page_url = "analog.com/dw/sdks.aspx?file=LSI3";
        string download_page_url = "analog.com/dw/download.aspx?file=LSI3";

        //--- Labels ---//
        string countryRegion_text = "Country/Region";
        string city_blankInput_errorMessage = "City is required";
        string telephone_blankInput_errorMessage = "Phone number is required";
        string whenWillYourApplicationBeAvailableInTheMarketplace_blankInput_errorMessage = "Please select When will your application be available in the marketplace";
        string howManyProcessorsAreYouLikelyToOrderOnAnAnnualBasis_blankInput_errorMessage = "Please select How many DSPs are you likely to order on an annual basis";
        string howConfidentAreYouOfAchievingTheseVolumes_blankInput_errorMessage = "Please select How confident are you of achieving these volumes";

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Elements are Present and Working as Expected when the User is Logged Out in EN Locale")]
        public void Forms_SoftwareDownload_VerifyElementsWhenUserIsLoggedOut(string Locale)
        {
            //--- Software Download Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + softwareDownload_form_url;

            //--- Action: Navigate to the given URL ---//
            action.Navigate(driver, form_url);

            //----- Your detailed contact information -----//

            //----- Form TestPlan > EN Tab > R40 > T1: Verify all labels for Country is changed to "Country/Region" (IQ-4970/AL-12466) -----//

            //--- Expected Result: The Country label has been changed to Country/Region ---//
            test.validateStringInstance(driver, util.GetText(driver, Elements.Forms_Country_Label), countryRegion_text);

            //----- Privacy Settings -----//

            //----- Form TestPlan > EN Tab > R38 > T1: Verify Privacy Settings in All Forms [account is not logged in] (AL-11345) -----//

            //--- Expected Result: Privacy Settings section is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_Section);

            //--- Expected Result: Prompt "You can change your privacy settings at any time by clicking on the unsubscribe link in emails sent from Analog Devices or in Analog’s Privacy Settings." is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_LegalDisclaimer_Text);

            //--- Expected Result: Privacy Settings is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_Link);

            //--- Expected Result: Privacy & Security Statement ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacyAndSecurityStatement_Link);

            //--- Expected Result: Authorized Partners ---//
            test.validateElementIsPresent(driver, Elements.Forms_AuthorizedPartners_Link);

            //----- Save time by logging in or registering for myAnalog -----//

            //----- Form TestPlan > EN Tab > R3 > T3_1: Verify Save time to login section  -----//

            if (!util.CheckElement(driver, Elements.Forms_Expanded_SaveTimeByLoggingInOrRegisteringForMyAnalog_Panel, 1))
            {
                //--- Action: Click on the Save to myAnalog link ---//
                action.IClick(driver, Elements.Forms_SaveTimeByLoggingInOrRegisteringForMyAnalog_Link);
            }

            //--- Expected Result: Save to MyAnalog flyout should be displayed. (AL-7439) ---//
            test.validateElementIsPresent(driver, Elements.Forms_Expanded_SaveTimeByLoggingInOrRegisteringForMyAnalog_Panel);
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Elements are Present and Working as Expected when the User is Logged In in EN Locale")]
        public void Forms_SoftwareDownload_VerifyElementsWhenUserIsLoggedIn(string Locale)
        {
            //--- Software Download Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + softwareDownload_form_url;

            //--- Action: Navigate to the given URL ---//
            action.Navigate(driver, form_url);

            //----- Save time by logging in or registering for myAnalog -----//

            //----- Form TestPlan > EN Tab > R3 > T3_1: Verify Save time to login section  -----//

            //--- Action: Login using valid data ---//
            action.ILogInViaSaveTimeByLoggingInPanel(driver, username, password);
            Thread.Sleep(1000);

            driver.Navigate().Refresh();
            Thread.Sleep(1000);

            //----- About you -----//

            //--- Expected Result: First Name field should be populated ---//
            test.validateFieldIsNotEmpty(driver, Elements.Forms_FirstName_InputBox);

            //--- Expected Result: Last Name field should be populated ---//
            test.validateFieldIsNotEmpty(driver, Elements.Forms_LastName_InputBox);

            //----- Your detailed contact information -----//

            //--- Expected Result: Organization Name field should be populated ---//
            test.validateFieldIsNotEmpty(driver, Elements.Forms_OrganizationName_InputBox);

            //--- Expected Result: State/Province (Dropdown) should be populated ---//
            test.validateStringChanged(driver, util.GetSelectedDropdownValue(driver, Elements.Forms_StateProvince_Dropdown), "Select State/Province");

            //--- Expected Result: Country (dropdown) field should be populated ---//
            test.validateStringChanged(driver, util.GetSelectedDropdownValue(driver, Elements.Forms_CountryRegion_Dropdown), "Select Country/Region");

            //----- Privacy Settings -----//

            //----- Form TestPlan > EN Tab > R38 > T1: Verify Privacy Settings in All Forms [account is logged in] (AL-11345) -----//

            //--- Expected Result: Prompt "You can change your privacy settings at any time by clicking on the unsubscribe link in emails sent from Analog Devices or in Analog’s Privacy Settings." is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_LegalDisclaimer_Text);

            //--- Expected Result: Privacy Settings is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_Link);

            //--- Expected Result: Privacy & Security Statement ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacyAndSecurityStatement_Link);

            //--- Expected Result: Authorized Partners ---//
            test.validateElementIsPresent(driver, Elements.Forms_AuthorizedPartners_Link);
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Input Validations are Working as Expected for Valid Inputs in EN Locale")]
        public void Forms_SoftwareDownload_VerifyInputValidationsForValidInputs(string Locale)
        {
            //--- Software Download Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + softwareDownload_form_url;

            //--- Action: Navigate to the given URL ---//
            action.Navigate(driver, form_url);

            //----- Development environment -----//

            //----- Form TestPlan > EN Tab > R3 > T4: Verify the Development environment section (AL-7229) -----//

            //--- Action: Select 'Other' on the Target hardware dropdown ---//
            action.ISelectFromDropdownByStringText(driver, Elements.Forms_TargetHardware_Dropdown, "Other");

            //--- Expected Result: The text box (*If Other, please specify) will appear --//
            test.validateElementIsPresent(driver, Elements.Forms_SoftwareDownload_IfOtherPleaseSpecify_InputBox);
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Input Validations are Working as Expected for Blank Inputs in EN Locale")]
        public void Forms_SoftwareDownload_VerifyInputValidationsForBlankInputs(string Locale)
        {
            //--- Software Download Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + softwareDownload_form_url;

            //--- Action: Navigate to the given URL ---//
            action.Navigate(driver, form_url);

            //----- Development environment -----//

            //----- Form TestPlan > EN Tab > R3 > T4: Verify the Development environment section (AL-7229) -----//

            //--- Action: Select 'Other' on the Target hardware dropdown ---//
            action.ISelectFromDropdownByStringText(driver, Elements.Forms_TargetHardware_Dropdown, "Other");

            //----- Privacy Settings -----//

            //--- Action: Click on submit button ---//
            action.IClick(driver, Elements.Forms_Submit_Button);

            //----- Development environment -----//

            //--- Expected Result: An error validation message should be seen below the  *If Other, please specify text box.---//
            test.validateElementIsPresent(driver, Elements.Forms_SoftwareDownload_IfOtherPleaseSpecify_BlankInput_ErrorMessage);

            //----- Form TestPlan > EN Tab > R3 > T5_1: Submit the Software Module Download Form after login -----//

            //----- Your detailed contact information -----//

            //--- Expected Result: The error messages should be shown below City: "City is required" ---//
            test.validateStringIsCorrect(driver, Elements.Forms_SoftwareDownload_City_BlankInput_ErrorMessage, city_blankInput_errorMessage);

            //--- Expected Result: The error messages should be shown below Telephone: "Phone number is required" ---//
            test.validateStringIsCorrect(driver, Elements.Forms_SoftwareDownload_Telephone_BlankInput_ErrorMessage, telephone_blankInput_errorMessage);

            //----- Your preferences -----//

            //--- Expected Result: The error messages should be shown below When will your application be available in the marketplace?(dropdown): "Please select When will your application be available in the marketplace" ---//
            test.validateStringIsCorrect(driver, Elements.Forms_SoftwareDownload_WhenWillYourApplicationBeAvailableInTheMarketplace_BlankInput_ErrorMessage, whenWillYourApplicationBeAvailableInTheMarketplace_blankInput_errorMessage);

            //--- Expected Result: The error messages should be shown below How many processors are you likely to order on an annual basis?(dropdown): "Please select How many DSPs are you likely to order on an annual basis" ---//
            test.validateStringIsCorrect(driver, Elements.Forms_SoftwareDownload_HowManyProcessorsAreYouLikelyToOrderOnAnAnnualBasis_BlankInput_ErrorMessage, howManyProcessorsAreYouLikelyToOrderOnAnAnnualBasis_blankInput_errorMessage);

            //--- Expected Result: The error messages should be shown below How confident are you of achieving these volumes?: "Please select How confident are you of achieving these volumes" ---//
            test.validateStringIsCorrect(driver, Elements.Forms_SoftwareDownload_HowConfidentAreYouOfAchievingTheseVolumes_BlankInput_ErrorMessage, howConfidentAreYouOfAchievingTheseVolumes_blankInput_errorMessage);
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Form Submission is Working as Expected in EN Locale")]
        public void Forms_SoftwareDownload_VerifyFormSubmission(string Locale)
        {
            if (Util.Configuration.Environment.Equals("dev") || Util.Configuration.Environment.Equals("qa"))
            {
                //--- Software Download Form URL ---//
                string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + softwareDownload_form_url;

                //--- Action: Navigate to the given URL ---//
                action.Navigate(driver, form_url);

                //----- Save time by logging in or registering for myAnalog -----//

                //--- Action: Login using valid data ---//
                action.ILogInViaSaveTimeByLoggingInPanel(driver, username, password);

                //----- Form TestPlan > EN Tab > R3 > T5_2: Submit the Software Module Download Form  with valid data -----//

                //--- Action: Enter valid data on the mandatory fields. ---//

                //----- About you -----//
                string firstName_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_FirstName_InputBox);
                action.IType(driver, Elements.Forms_FirstName_InputBox, firstName_input);

                string lastName_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_LastName_InputBox);
                action.IType(driver, Elements.Forms_LastName_InputBox, lastName_input);

                //----- Your detailed contact information -----//
                string organizationName_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_OrganizationName_InputBox);
                action.IType(driver, Elements.Forms_OrganizationName_InputBox, organizationName_input);

                string city_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_City_InputBox);
                action.IType(driver, Elements.Forms_City_InputBox, city_input);

                action.IClick(driver, Elements.Forms_StateProvince_Dropdown);
                action.IClick(driver, By.CssSelector("select[id$='tate']>option:nth-of-type(3)"));

                string countryRegion_input = "US";
                action.ISelectFromDropdown(driver, Elements.Forms_CountryRegion_Dropdown, countryRegion_input);

                string telephone_input = "123456789";
                action.IDeleteValueOnFields(driver, Elements.Forms_Telephone_InputBox);
                action.IType(driver, Elements.Forms_Telephone_InputBox, telephone_input);

                action.IClick(driver, Elements.Forms_DoYouHaveAnAdiContact_No_RadioButton);

                //----- Your preferences -----//
                action.IClick(driver, Elements.Forms_SoftwareDownload_WhatIsThePrimaryMarketSegmentThatYourApplicationTargets_Dropdown);
                action.IClick(driver, By.CssSelector("select[id$='PrimaryMarketSegment']>option:nth-of-type(1)"));

                action.IClick(driver, Elements.Forms_SoftwareDownload_WhatIsThePrimaryTypeOfApplicationThatYouDevelop_Dropdown);
                action.IClick(driver, By.CssSelector("select[id$='PrimaryApplication']>option:nth-of-type(1)"));

                action.IClick(driver, Elements.Forms_WhenWillYourApplicationBeAvailableInTheMarketplace_Dropdown);
                action.IClick(driver, By.CssSelector("select[id$='WhenWillYourApplicationBeAvailable']>option:nth-of-type(2)"));

                action.IClick(driver, Elements.Forms_HowManyProcessorsAreYouLikelyToOrderOnAnAnnualBasis_Dropdown);
                action.IClick(driver, By.CssSelector("select[id$='HowManyDSPsAreYouLikelyToOrder']>option:nth-of-type(2)"));

                action.IClick(driver, Elements.Forms_HowConfidentAreYouOfAchievingTheseVolumes_Dropdown);
                action.IClick(driver, By.CssSelector("select[id$='HowConfidentAreYouAchievingVolumes']>option:nth-of-type(2)"));

                //----- Development environment -----//
                action.IClick(driver, Elements.Forms_TargetHardware_Dropdown);
                action.IClick(driver, By.CssSelector("select[id*='TargetHardware']>option:nth-of-type(1)"));

                //----- Privacy Settings -----//
                action.ICheck(driver, Elements.Forms_Communication_Checkbox);
            
                //--- Action: Click on the Submit button ---//
                action.IClick(driver, Elements.Forms_Submit_Button);

                if (driver.Url.Contains("b2clogin"))
                {
                    action.ILogin(driver, username, password);
                }

                //--- Expected Result: Software Module and Software Development Kit (SDK) Download page should be displayed ---//
                //--- Note: AL-16357 has been logged for the Issue ---//
                test.validateStringInstance(driver, driver.Url, softwareModulesDownloadListing_page_url);

                if (driver.Url.Contains(softwareModulesDownloadListing_page_url))
                {
                    if (util.CheckElement(driver, Elements.SoftwareModulesDownloadListing_Software_Link, 1))
                    {
                        //--- Action: Click on the Software link ---//
                        action.IOpenLinkInNewTab(driver, Elements.SoftwareModulesDownloadListing_Software_Link);
                        Thread.Sleep(1000);

                        //--- Expected Result: The page should be redirected to Download Software page ---//
                        test.validateStringInstance(driver, driver.Url, download_page_url);

                        driver.Close();
                        driver.SwitchTo().Window(driver.WindowHandles.Last());
                    }
                    else
                    {
                        test.validateElementIsPresent(driver, Elements.SoftwareModulesDownloadListing_Software_Link);
                    }

                    if (util.CheckElement(driver, Elements.SoftwareModulesDownloadListing_Download_Button, 1))
                    {
                        //--- Action: Click on the Download button ---//
                        action.IOpenLinkInNewTab(driver, Elements.SoftwareModulesDownloadListing_Download_Button);
                        Thread.Sleep(1000);

                        //--- Expected Result: The page should be redirected to Download Software page ---//
                        test.validateStringInstance(driver, driver.Url, download_page_url);

                        driver.Close();
                        driver.SwitchTo().Window(driver.WindowHandles.Last());
                    }
                    else
                    {
                        test.validateElementIsPresent(driver, Elements.SoftwareModulesDownloadListing_Download_Button);
                    }
                }
            }
        }
    }
}