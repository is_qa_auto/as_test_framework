﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using OpenQA.Selenium;
using System;
using System.Threading;

namespace AS_Test_Framework.Forms
{
    [TestFixture]
    public class Forms_AdiSiteSearchSupport : BaseSetUp
    {
        //--- ADI Site Search Support Form URL: ---//
        //--- DEV: https://form-dev.corpnt.analog.com/form_pages/corporate/searchResultsSurvey.aspx ---//
        //--- QA: https://form-qa.corpnt.analog.com/form_pages/corporate/searchResultsSurvey.aspx ---//
        //--- PROD: https://form.analog.com/form_pages/corporate/searchResultsSurvey.aspx ---//

        public Forms_AdiSiteSearchSupport() : base() { }

        //--- Login Credentials ---//
        string username = "f0rms_t3st_555@mailinator.com";
        string password = "Test_1234";

        //--- URLs ---//
        string adiSiteSearchSupport_form_url = "form_pages/corporate/searchResultsSurvey.aspx";
        string adiSiteSearchSupport_page_url = "/search_feedback_form_thank_you.html";

        //--- Labels ---//
        string countryRegion_text = "Country/Region";
        string pleaseProvideYourEmailIfYouWishSomeoneToContactYouRegardingThisInformation_blankInput_errorMessage = "Email address is required";
        string pleaseProvideYourEmailIfYouWishSomeoneToContactYouRegardingThisInformation_invalidInput_errorMessage = "Please enter a valid email address";

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Elements are Present and Working as Expected when the User is Logged Out in EN Locale")]
        public void Forms_AdiSiteSearchSupport_VerifyElementsWhenUserIsLoggedOut(string Locale)
        {
            //--- ADI Site Search Support Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + adiSiteSearchSupport_form_url;

            //--- Action: Navigate to the given URL ---//
            action.Navigate(driver, form_url);

            //----- Form TestPlan > EN Tab > R40 > T1: Verify all labels for Country is changed to "Country/Region" (IQ-4970/AL-12466) -----//

            //----- Your contact information -----//

            //--- Expected Result: The Country label has been changed to Country/Region ---//
            test.validateStringInstance(driver, util.GetText(driver, Elements.Forms_Country_Label), countryRegion_text);

            //----- Form TestPlan > EN Tab > R24 > T3: Login using Save time to login section with valid data -----//

            //----- Save time by logging in or registering for myAnalog -----//

            if (!util.CheckElement(driver, Elements.Forms_Expanded_SaveTimeByLoggingInOrRegisteringForMyAnalog_Panel, 1))
            {
                //--- Action: Click on Save time by logging in or registering for myAnalog   link ---//
                action.IClick(driver, Elements.Forms_SaveTimeByLoggingInOrRegisteringForMyAnalog_Link);
            }

            //--- Expected Result: Save time to login section should be displayed ---//
            test.validateElementIsPresent(driver, Elements.Forms_Expanded_SaveTimeByLoggingInOrRegisteringForMyAnalog_Panel);
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Elements are Present and Working as Expected when the User is Logged In in EN Locale")]
        public void Forms_AdiSiteSearchSupport_VerifyElementsWhenUserIsLoggedIn(string Locale)
        {
            //--- ADI Site Search Support Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + adiSiteSearchSupport_form_url;

            //--- Action: Navigate to the given URL ---//
            action.Navigate(driver, form_url);

            //----- Form TestPlan > EN Tab > R24 > T3: Login using Save time to login section with valid data -----//

            //----- Save time by logging in or registering for myAnalog -----//

            //--- Action: Login using valid data ---//
            action.ILogInViaSaveTimeByLoggingInPanel(driver, username, password);
            Thread.Sleep(1000);

            //----- Your contact information -----//

            //--- Expected Result: The Please provide your email if you wish someone to contact you regarding this information field should be  automatically populated ---//
            test.validateFieldIsNotEmpty(driver, Elements.Forms_PleaseProvideYourEmailIfYouWishSomeoneToContactYouRegardingThisInformation_InputBox);
        }

        [TestCase("en", TestName = "Verify that the Input Validations are Working as Expected for Invalid Inputs in EN Locale")]
        public void Forms_AdiSiteSearchSupport_VerifyInputValidationsForInvalidInputs(string Locale)
        {
            //--- ADI Site Search Support Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + adiSiteSearchSupport_form_url;

            //--- Action: Navigate to the given URL ---//
            action.Navigate(driver, form_url);

            //----- Form TestPlan > EN Tab > R24 > T4_2: Submit the Site Search Support Form    with invalid data -----//

            //----- Your contact information -----//

            //--- Action: Enter invalid data on the Email Address field. ---//
            string pleaseProvideYourEmailIfYouWishSomeoneToContactYouRegardingThisInformation_input = "123!@#$%@yahoo.com";
            action.IDeleteValueOnFields(driver, Elements.Forms_PleaseProvideYourEmailIfYouWishSomeoneToContactYouRegardingThisInformation_InputBox);
            action.IType(driver, Elements.Forms_PleaseProvideYourEmailIfYouWishSomeoneToContactYouRegardingThisInformation_InputBox, pleaseProvideYourEmailIfYouWishSomeoneToContactYouRegardingThisInformation_input);
            action.IType(driver, Elements.Forms_PleaseProvideYourEmailIfYouWishSomeoneToContactYouRegardingThisInformation_InputBox, Keys.Tab);

            //--- Expected Result: The error message should be displayed: "Please enter a valid email address" ---//
            test.validateStringIsCorrect(driver, Elements.Forms_AdiSiteSearchSupport_PleaseProvideYourEmailIfYouWishSomeoneToContactYouRegardingThisInformation_InvalidInput_ErrorMessage, pleaseProvideYourEmailIfYouWishSomeoneToContactYouRegardingThisInformation_invalidInput_errorMessage);
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Input Validations are Working as Expected for Blank Inputs in EN Locale")]
        public void Forms_AdiSiteSearchSupport_VerifyInputValidationsForBlankInputs(string Locale)
        {
            //--- ADI Site Search Support Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + adiSiteSearchSupport_form_url;

            //--- Action: Navigate to the given URL ---//
            action.Navigate(driver, form_url);

            //----- Form TestPlan > EN Tab > R24 > T4_1: Submit Site Search Support Form with no data on mandatory fields -----//

            //--- Action: Click on the Submit button without entering data on the mandatory fields. ---//
            action.IClick(driver, Elements.Forms_Submit_Button);

            //----- Your contact information -----//

            //--- Expected Result: The Your contact information Please provide your email if you wish someone to contact you regarding this information: "Email address is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_PleaseProvideYourEmailIfYouWishSomeoneToContactYouRegardingThisInformation_BlankInput_ErrorMessage, pleaseProvideYourEmailIfYouWishSomeoneToContactYouRegardingThisInformation_blankInput_errorMessage);            
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Form Submission is Working as Expected in EN Locale")]
        public void Forms_AdiSiteSearchSupport_VerifyFormSubmission(string Locale)
        {
            if (Util.Configuration.Environment.Equals("dev") || Util.Configuration.Environment.Equals("qa"))
            {
                //--- ADI Site Search Support Form URL ---//
                string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + adiSiteSearchSupport_form_url;

                //--- Action: Navigate to the given URL ---//
                action.Navigate(driver, form_url);

                //----- Form TestPlan > EN Tab > R24 > T4_3: Submit Site Search Support Form with valid data -----//

                //--- Action: Enter valid data on the mandatory fields. ---//

                //----- Your contact information -----//
                string pleaseProvideYourEmailIfYouWishSomeoneToContactYouRegardingThisInformation_input = "t3st_" + Util.Configuration.Environment.ToLower() + "_adiSiteSearchSupport_f0rm_" + util.GenerateRandomNumber() + "@mailinator.com";
                //Console.WriteLine("Email Input = " + pleaseProvideYourEmailIfYouWishSomeoneToContactYouRegardingThisInformation_input);
                action.IDeleteValueOnFields(driver, Elements.Forms_PleaseProvideYourEmailIfYouWishSomeoneToContactYouRegardingThisInformation_InputBox);
                action.IType(driver, Elements.Forms_PleaseProvideYourEmailIfYouWishSomeoneToContactYouRegardingThisInformation_InputBox, pleaseProvideYourEmailIfYouWishSomeoneToContactYouRegardingThisInformation_input);

                string countryRegion_input = "US";
                action.ISelectFromDropdown(driver, Elements.Forms_CountryRegion_Dropdown, countryRegion_input);

                //----- Privacy Settings -----//
                action.ICheck(driver, Elements.Forms_Communication_Checkbox);

                //--- Action: Click on submit button ---//
                action.IClick(driver, Elements.Forms_Submit_Button);

                //--- Expected Result: ADI Site Search Support Thank You page should be displayed ---//
                test.validateStringInstance(driver, driver.Url, adiSiteSearchSupport_page_url);

                if (driver.Url.Contains(adiSiteSearchSupport_page_url))
                {
                    action.ICheckMailinatorEmail(driver, pleaseProvideYourEmailIfYouWishSomeoneToContactYouRegardingThisInformation_input);

                    //--- Expected Result: The email from external.webmaster@analog.com should be displayed ---//
                    test.validateStringIsCorrect(driver, Elements.Mailinator_LatestEmail_Sender, "external.webmaster@analog.com");
                }
            }
        }
    }
}