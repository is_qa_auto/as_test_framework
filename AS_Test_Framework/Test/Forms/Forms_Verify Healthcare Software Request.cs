﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using OpenQA.Selenium;
using System;
using System.Threading;

namespace AS_Test_Framework.Forms
{
    [TestFixture]
    public class Forms_HealthcareSoftwareRequest : BaseSetUp
    {
        //--- Healthcare Software Request Form URL: ---//
        //--- DEV: https://form-dev.corpnt.analog.com/form_pages/HSRF/HSRF.aspx ---//
        //--- QA: https://form-qa.corpnt.analog.com/form_pages/HSRF/HSRF.aspx ---//
        //--- PROD: https://form.analog.com/form_pages/HSRF/HSRF.aspx ---//

        public Forms_HealthcareSoftwareRequest() : base() { }

        //--- Login Credentials ---//
        string username = "f0rms_t3st_555@mailinator.com";
        string password = "Test_1234";

        //--- URLs ---//
        string healthcareSoftwareRequest_form_url = "form_pages/HSRF/HSRF.aspx";
        string adiHealthcareRequestForSoftware_page_url = "/healthcare-software-request-thankyou.html";

        //--- Labels ---//
        string email_subject = "Application for access to Health Care Software";
        string email_blankInput_errorMessage = "Email address is required";
        string firstName_blankInput_errorMessage = "First name is required";
        string lastName_blankInput_errorMessage = "Last name is required";
        string organizationName_blankInput_errorMessage = "Organization name is required";
        string addressLine_1_blankInput_errorMessage = "Address 1 is required";
        string phoneNumber_blankInput_errorMessage = "Phone Number is required";
        string city_blankInput_errorMessage = "City is required";
        string stateProvince_blankInput_errorMessage = "State/Province is required";
        string zipPostalCode_blankInput_errorMessage = "Zip/Postal code is required";
        string companyWebsite_blankInput_errorMessage = "Organization Website is required";
        string adiSalesRepresentativeContact_blankInput_errorMessage = "ADI sales representative/contact is required";
        string whatMarketsAreYouPrimarilyFocusedOn_blankInput_errorMessage = "Please select up to 2 market areas you are focused on";
        string whatStageIsYourApplicationIn_blankInput_errorMessage = "Please select the appropriate stage for your application";
        string whenWillYourApplicationBeAvailableInTheMarketplace_blankInput_errorMessage = "Please select when your application will be available";
        string whatIsYourTargetPlatformProcessorsAndOperatingSystem_blankInput_errorMessage = "Please select target platform/processors and operating system";
        string brieflyDescribeYourApplication_blankInput_errorMessage = "Please provide a brief description of your application";

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Elements are Present and Working as Expected when the User is Logged In in EN Locale")]
        public void Forms_HealthcareSoftwareRequest_VerifyElementsWhenUserIsLoggedIn(string Locale)
        {
            //--- Healthcare Software Request Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + healthcareSoftwareRequest_form_url;

            //--- Action: Navigate to the given URL ---//
            action.Navigate(driver, form_url);

            //----- Form TestPlan > EN Tab > R29 > T7: Verify the email text field -----//

            //----- Save time by logging in or registering for myAnalog -----//

            //--- Action: Log in valid credential ---//
            action.ILogInViaSaveTimeByLoggingInPanel(driver, username, password);
            Thread.Sleep(2000);

            driver.Navigate().Refresh();
            Thread.Sleep(1000);

            //----- About you -----//

            //--- Expected Result: the email text field has been made read only ---//
            test.validateElementIsNotEnabled(driver, Elements.Forms_Email_InputBox);

            //----- Form TestPlan > EN Tab > R38 > T1: Verify Privacy Settings in All Forms [account is logged in] (AL-11345) -----//

            //----- Privacy Settings -----//

            //--- Expected Result: Prompt "You can change your privacy settings at any time by clicking on the unsubscribe link in emails sent from Analog Devices or in Analog’s Privacy Settings." is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_LegalDisclaimer_Text);

            //--- Expected Result: Privacy Settings is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_Link);

            //--- Expected Result: Privacy & Security Statement ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacyAndSecurityStatement_Link);

            //--- Expected Result: Authorized Partners ---//
            test.validateElementIsPresent(driver, Elements.Forms_AuthorizedPartners_Link);
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Input Validations are Working as Expected for Blank Inputs in EN Locale")]
        public void Forms_HealthcareSoftwareRequest_VerifyInputValidationsForBlankInputs(string Locale)
        {
            //--- Healthcare Software Request Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + healthcareSoftwareRequest_form_url;

            //--- Action: Navigate to the given URL ---//
            action.Navigate(driver, form_url);

            //----- Form TestPlan > EN Tab > R29 > T5_1: Submit Healthcare Software Request Form with no data on mandatory fields -----//

            //--- Action: Click on the Submit button without entering data on the mandatory fields. ---//
            action.IClick(driver, Elements.Forms_Submit_Button);

            //----- Healthcare Software Request Form -----//

            //--- Expected Result: The Terms and Conditions: "Please select the checkbox" error message should be shown ---//
            test.validateElementIsPresent(driver, Elements.Forms_HealthcareSoftwareRequest_IAgreeToTheTermsAndConditions_BlankInput_ErrorMessage);

            //----- About you -----//

            //--- Expected Result: The Email Id: "Email address is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_HealthcareSoftwareRequest_EmailId_BlankInput_ErrorMessage, email_blankInput_errorMessage);

            //--- Expected Result: The First Name: "First name is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_HealthcareSoftwareRequest_FirstName_BlankInput_ErrorMessage, firstName_blankInput_errorMessage);

            //--- Expected Result: The Last Name: "Last name is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_HealthcareSoftwareRequest_LastName_BlankInput_ErrorMessage, lastName_blankInput_errorMessage);

            //----- Your detailed contact information -----//

            //--- Expected Result: The Organization Name: "Organization name is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_HealthcareSoftwareRequest_OrganizationName_BlankInput_ErrorMessage, organizationName_blankInput_errorMessage);

            //--- Expected Result: The Address Line 1: "Address 1 is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_HealthcareSoftwareRequest_AddressLine_1_BlankInput_ErrorMessage, addressLine_1_blankInput_errorMessage);

            //--- Expected Result: The Phone Number: "Phone Number is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_HealthcareSoftwareRequest_PhoneNumber_BlankInput_ErrorMessage, phoneNumber_blankInput_errorMessage);

            //--- Expected Result: The City: "City is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_HealthcareSoftwareRequest_City_BlankInput_ErrorMessage, city_blankInput_errorMessage);

            //--- Expected Result: The State/Province: "State/Province is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_StateProvince_BlankInput_ErrorMessage, stateProvince_blankInput_errorMessage);

            //--- Expected Result: The Zip/Postal code: "Zip/Postal code is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_HealthcareSoftwareRequest_ZipPostalCode_BlankInput_ErrorMessage, zipPostalCode_blankInput_errorMessage);

            //--- Expected Result: The Country: "Country is required" error message should be shown ---//
            test.validateElementIsPresent(driver, Elements.Forms_CountryRegion_BlankInput_ErrorMessage);

            //--- Expected Result: The Company Website: "Organization Website is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_HealthcareSoftwareRequest_CompanyWebsite_BlankInput_ErrorMessage, companyWebsite_blankInput_errorMessage);

            //----- Commercial information -----//

            //--- Expected Result: The ADI sales representative/contact: "ADI sales representative/contact is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_HealthcareSoftwareRequest_AdiSalesRepresentativeContact_BlankInput_ErrorMessage, adiSalesRepresentativeContact_blankInput_errorMessage);

            //--- Expected Result: The What markets are you primarily focused on (select up to two)?: "Please select up to 2 market areas you are focusing on" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_HealthcareSoftwareRequest_WhatMarketsAreYouPrimarilyFocusedOn_BlankInput_ErrorMessage, whatMarketsAreYouPrimarilyFocusedOn_blankInput_errorMessage);

            //--- Expected Result: The What stage is your application in?: "Please select the appropriate stage for your application" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_HealthcareSoftwareRequest_WhatStageIsYourApplicationIn_BlankInput_ErrorMessage, whatStageIsYourApplicationIn_blankInput_errorMessage);

            //--- Expected Result: The When will your application be available in the marketplace?: "Please select when your application will be available" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_HealthcareSoftwareRequest_WhenWillYourApplicationBeAvailableInTheMarketplace_BlankInput_ErrorMessage, whenWillYourApplicationBeAvailableInTheMarketplace_blankInput_errorMessage);

            //--- Expected Result: The What is your target platform/ processors and operating system: "Please select target platform/processors and operating system" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_HealthcareSoftwareRequest_WhatIsYourTargetPlatformProcessorsAndOperatingSystem_BlankInput_ErrorMessage, whatIsYourTargetPlatformProcessorsAndOperatingSystem_blankInput_errorMessage);

            //--- Expected Result: The Briefly describe your application: "Please provide a brief description of your application" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_HealthcareSoftwareRequest_BrieflyDescribeYourApplication_BlankInput_ErrorMessage, brieflyDescribeYourApplication_blankInput_errorMessage);
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Form Submission is Working as Expected in EN Locale")]
        public void Forms_HealthcareSoftwareRequest_VerifyFormSubmission(string Locale)
        {
            if (Util.Configuration.Environment.Equals("dev") || Util.Configuration.Environment.Equals("qa"))
            {
                //--- ADI Site Search Support Form URL ---//
                string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + healthcareSoftwareRequest_form_url;

                //--- Action: Navigate to the given URL ---//
                action.Navigate(driver, form_url);

                //----- Form TestPlan > EN Tab > R29 > T5_3: Submit the Healthcare Software Request Form with valid data (AL-2496) -----//

                //--- Action: Enter valid data on the mandatory fields ---//

                //----- Save time by logging in or registering for myAnalog -----//
                string email_input = "t3st_healthcareSoftwareRequest_f0rm_555@mailinator.com";
                //Console.WriteLine("Email Input = " + email_input);
                action.ILogInViaSaveTimeByLoggingInPanel(driver, email_input, password);

                //----- Healthcare Software Request Form -----//
                action.ICheck(driver, Elements.Forms_IAgreeToTheTermsAndConditions_Checkbox);

                //----- About you -----//
                string firstName_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_FirstName_InputBox);
                action.IType(driver, Elements.Forms_FirstName_InputBox, firstName_input);

                string lastName_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_LastName_InputBox);
                action.IType(driver, Elements.Forms_LastName_InputBox, lastName_input);

                //----- Your detailed contact information -----//
                string organizationName_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_OrganizationName_InputBox);
                action.IType(driver, Elements.Forms_OrganizationName_InputBox, organizationName_input);

                string addressLine_1_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_SoftwareRequest_AddressLine_1_InputBox);
                action.IType(driver, Elements.Forms_SoftwareRequest_AddressLine_1_InputBox, addressLine_1_input);

                string phoneNumber_input = "123456789";
                action.IDeleteValueOnFields(driver, Elements.Forms_PhoneNumber_InputBox);
                action.IType(driver, Elements.Forms_PhoneNumber_InputBox, phoneNumber_input);

                string city_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_City_InputBox);
                action.IType(driver, Elements.Forms_City_InputBox, city_input);

                string countryRegion_input = "US";
                action.ISelectFromDropdown(driver, Elements.Forms_CountryRegion_Dropdown, countryRegion_input);

                action.IClick(driver, Elements.Forms_StateProvince_Dropdown);
                action.IClick(driver, By.CssSelector("select[id$='tate']>option:nth-of-type(3)"));

                string zipPostalCode_input = "12345";
                action.IDeleteValueOnFields(driver, Elements.Forms_ZipPostalCode_InputBox);
                action.IType(driver, Elements.Forms_ZipPostalCode_InputBox, zipPostalCode_input);

                string companyWebsite_input = "www.test.com";
                action.IDeleteValueOnFields(driver, Elements.Forms_CompanyWebsite_InputBox);
                action.IType(driver, Elements.Forms_CompanyWebsite_InputBox, companyWebsite_input);

                //----- Commercial information -----//
                string adiSalesRepresentativeContact_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_AdiSalesRepresentative_InputBox);
                action.IType(driver, Elements.Forms_AdiSalesRepresentative_InputBox, adiSalesRepresentativeContact_input);

                action.ICheck(driver, By.CssSelector("table[id$='MarketsPrimarilyFocussedOn']>tbody>tr:nth-of-type(1)>td:nth-of-type(2)"));
                action.ICheck(driver, By.CssSelector("table[id$='MarketsPrimarilyFocussedOn']>tbody>tr:nth-of-type(2)>td:nth-of-type(1)"));

                action.IClick(driver, Elements.Forms_WhatStageIsYourApplicationIn_Dropdown);
                action.IClick(driver, By.CssSelector("select[id$='WhatStageIsYourApplicationIn']>option:nth-of-type(2)"));

                action.IClick(driver, Elements.Forms_WhenWillYourApplicationBeAvailableInTheMarketplace_Dropdown);
                action.IClick(driver, By.CssSelector("select[id$='WhenWillYourApplicationBeAvailable']>option:nth-of-type(2)"));

                string whatIsYourTargetPlatformProcessorsAndOperatingSystem_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_WhatIsYourTargetPlatformProcessorsAndOperatingSystem_TextArea);
                action.IType(driver, Elements.Forms_WhatIsYourTargetPlatformProcessorsAndOperatingSystem_TextArea, whatIsYourTargetPlatformProcessorsAndOperatingSystem_input);

                string brieflyDescribeYourApplication_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_BrieflyDescribeYourApplication_TextArea);
                action.IType(driver, Elements.Forms_BrieflyDescribeYourApplication_TextArea, brieflyDescribeYourApplication_input);

                //----- Privacy Settings -----//
                action.ICheck(driver, Elements.Forms_Communication_Checkbox);

                //--- Action: Click on the Submit button ---//
                action.IClick(driver, Elements.Forms_Submit_Button);
                Thread.Sleep(1000);

                //--- Expected Result: ADI Healthcare Request for Software Thank You page should be displayed ---//
                test.validateStringInstance(driver, driver.Url, adiHealthcareRequestForSoftware_page_url);

                if (driver.Url.Contains(adiHealthcareRequestForSoftware_page_url))
                {
                    //----- Form TestPlan > EN Tab > R29 > T7: Verify the email received after submitting HSRF form -----//
                    action.ICheckMailinatorEmail(driver, email_input);

                    //--- Expected Result: to the user who is submitting the form having mail subject as :-"DEV TESTING-Application for access to Health Care Software". ---//
                    test.validateStringInstance(driver, util.GetText(driver, Elements.Mailinator_LatestEmail_Subj), email_subject);

                    //--- Expected Result: to the user who is submitting the form having sender as "healthcare-support@analog.com". ---//
                    test.validateStringIsCorrect(driver, Elements.Mailinator_LatestEmail_Sender, "healthcare-support@analog.com");
                }
            }
        }
    }
}