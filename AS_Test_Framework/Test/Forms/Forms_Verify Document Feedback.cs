﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using OpenQA.Selenium;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.Forms
{
    [TestFixture]
    public class Forms_DocumentFeedback : BaseSetUp
    {
        //--- Document Feedback Form URL: ---//
        //--- DEV: https://form-dev.corpnt.analog.com/form_pages/Feedback/DocumentFeedback.aspx ---//
        //--- QA: https://form-qa.corpnt.analog.com/form_pages/Feedback/DocumentFeedback.aspx ---//
        //--- PROD: https://form.analog.com/form_pages/Feedback/DocumentFeedback.aspx ---//

        public Forms_DocumentFeedback() : base() { }

        //--- Login Credentials ---//
        string username = "f0rms_t3st_555@mailinator.com";
        string password = "Test_1234";

        //--- URLs ---//
        string documentFeedback_form_url = "form_pages/Feedback/DocumentFeedback.aspx";
        string technicalSupport_page_url = "/technical-support.html";
        string customerServiceCenter_page_url = "/customer-service-resources.html";
        string adiWebSiteSupport_form_url = "/websiteInfo.aspx";

        //--- Labels ---//
        string countryRegion_text = "Country/Region";
        string priority_blankInput_errorMessage = "Priority is required";
        string email_blankInput_errorMessage = "Email address is required";
        string organizationName_blankInput_errorMessage = "Organization name is required";
        string countryRegion_blankInput_errorMessage = "Country/Region is required";
        string fullName_blankInput_errorMessage = "Full name is required";
        string telephone_blankInput_errorMessage = "Telephone is Required";
        string documentTitle_blankInput_errorMessage = "File Name is Required";
        string partNumber_blankInput_errorMessage = "Please enter a Document Title or Part Number";
        string email_invalidInput_errorMessage = "Please enter a valid email address";

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Elements are Present and Working as Expected when the User is Logged Out in EN Locale")]
        public void Forms_DocumentFeedback_VerifyElementsWhenUserIsLoggedOut(string Locale)
        {
            //--- Document Feedback Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + documentFeedback_form_url;

            //--- Action: Navigate to the given URL ---//
            action.Navigate(driver, form_url);

            //----- About you -----//

            //----- Form TestPlan > EN Tab > R40 > T1: Verify all labels for Country is changed to "Country/Region" (IQ-4970/AL-12466) -----//

            //--- Expected Result: The Country label has been changed to Country/Region ---//
            test.validateStringInstance(driver, util.GetText(driver, Elements.Forms_Country_Label), countryRegion_text);

            //----- Privacy Settings -----//

            //----- Form TestPlan > EN Tab > R38 > T1: Verify Privacy Settings in All Forms [account is not logged in] (AL-11345) -----//

            //--- Expected Result: Privacy Settings section is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_Section);

            //--- Expected Result: Prompt "You can change your privacy settings at any time by clicking on the unsubscribe link in emails sent from Analog Devices or in Analog’s Privacy Settings." is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_LegalDisclaimer_Text);

            //--- Expected Result: Privacy Settings is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_Link);

            //--- Expected Result: Privacy & Security Statement ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacyAndSecurityStatement_Link);

            //--- Expected Result: Authorized Partners ---//
            test.validateElementIsPresent(driver, Elements.Forms_AuthorizedPartners_Link);

            //----- Document Feedback -----//

            //----- Form TestPlan > EN Tab > R12 > T4: Verify the Priority section below the Document Feedback title (AL-4689) -----//

            //--- Expected Result: The Critical radio button should be displayed ---//
            test.validateElementIsPresent(driver, Elements.Forms_Critical_RadioButton);

            //--- Expected Result: The Major radio button should be displayed ---//
            test.validateElementIsPresent(driver, Elements.Forms_Major_RadioButton);

            //--- Expected Result: The Minor radio button should be displayed ---//
            test.validateElementIsPresent(driver, Elements.Forms_Minor_RadioButton);

            //--- Expected Result: The Not a document error. Please click one of the following: radio button should be displayed ---//
            test.validateElementIsPresent(driver, Elements.Forms_NotADocumentError_RadioButton);

            //--- Expected Result: The Technical Support link should be displayed ---//
            test.validateElementIsPresent(driver, Elements.Forms_TechnicalSupport_Link);

            if (util.CheckElement(driver, Elements.Forms_TechnicalSupport_Link, 1))
            {
                //--- Action: Click on Technical Support link ---//
                action.IOpenLinkInNewTab(driver, Elements.Forms_TechnicalSupport_Link);
                Thread.Sleep(2000);

                //--- Expected Result: Technical support page should be displayed ---//
                test.validateStringInstance(driver, driver.Url, technicalSupport_page_url);

                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles.Last());
            }

            //--- Expected Result: The EngineerZone Support Forum link should be displayed ---//
            test.validateElementIsPresent(driver, Elements.Forms_EngineerZoneSupportForum_Link);

            //--- Expected Result: The Customer Service link should be displayed ---//
            test.validateElementIsPresent(driver, Elements.Forms_CustomerService_Link);

            if (util.CheckElement(driver, Elements.Forms_CustomerService_Link, 1))
            {
                //--- Action: Click on Customer Service link ---//
                action.IOpenLinkInNewTab(driver, Elements.Forms_CustomerService_Link);
                Thread.Sleep(1000);

                //--- Expected Result: Customer Service  page should be displayed ---//
                test.validateStringInstance(driver, driver.Url, customerServiceCenter_page_url);

                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles.Last());
            }

            //--- Expected Result: The Website Feedback link should be displayed ---//
            test.validateElementIsPresent(driver, Elements.Forms_WebsiteFeedback_Link);

            if (util.CheckElement(driver, Elements.Forms_WebsiteFeedback_Link, 1))
            {
                //--- Action: Click on Website Feedback link ---//
                action.IOpenLinkInNewTab(driver, Elements.Forms_WebsiteFeedback_Link);
                Thread.Sleep(1000);

                //--- Expected Result: Website Feed Back form should be displayed ---//
                test.validateStringInstance(driver, driver.Url, adiWebSiteSupport_form_url);

                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles.Last());
            }

            //----- Form TestPlan > EN Tab > R12 > T3: Login using Save time to login section with valid data -----//

            //----- Save time by logging in or registering for myAnalog -----//

            if (!util.CheckElement(driver, Elements.Forms_Expanded_SaveTimeByLoggingInOrRegisteringForMyAnalog_Panel, 1))
            {
                //--- Action: Click on Save time by logging in or registering for myAnalog   link ---//
                action.IClick(driver, Elements.Forms_SaveTimeByLoggingInOrRegisteringForMyAnalog_Link);
            }

            //--- Expected Result: Save time to login section should be displayed ---//
            test.validateElementIsPresent(driver, Elements.Forms_Expanded_SaveTimeByLoggingInOrRegisteringForMyAnalog_Panel);
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Elements are Present and Working as Expected when the User is Logged In in EN Locale")]
        public void Forms_DocumentFeedback_VerifyElementsWhenUserIsLoggedIn(string Locale)
        {
            //--- Document Feedback Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + documentFeedback_form_url;

            //--- Action: Navigate to the given URL ---//
            action.Navigate(driver, form_url);

            //----- Form TestPlan > EN Tab > R12 > T3: Login using Save time to login section with valid data -----//

            //----- Save time by logging in or registering for myAnalog -----//

            //--- Action: Login using valid data ---//
            action.ILogInViaSaveTimeByLoggingInPanel(driver, username, password);
            Thread.Sleep(2000);

            driver.Navigate().Refresh();
            Thread.Sleep(1000);

            //----- About you -----//

            //--- Expected Result: The Email field should be  automatically populated ---//
            test.validateFieldIsNotEmpty(driver, Elements.Forms_Email_InputBox);

            //--- Expected Result: The Organization name field should be  automatically populated ---//
            test.validateFieldIsNotEmpty(driver, Elements.Forms_OrganizationName_InputBox);

            //--- Expected Result: The Country(dropdown) field should be  automatically populated ---//
            test.validateStringChanged(driver, util.GetSelectedDropdownValue(driver, Elements.Forms_CountryRegion_Dropdown), "Select Country/Region");

            //--- Expected Result: The Full name field should be  automatically populated ---//
            test.validateFieldIsNotEmpty(driver, Elements.Forms_FullName_InputBox);

            //----- Form TestPlan > EN Tab > R38 > T1: Verify Privacy Settings in All Forms [account is logged in] (AL-11345) -----//

            //----- Privacy Settings -----//

            //--- Expected Result: Prompt "You can change your privacy settings at any time by clicking on the unsubscribe link in emails sent from Analog Devices or in Analog’s Privacy Settings." is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_LegalDisclaimer_Text);

            //--- Expected Result: Privacy Settings is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_Link);

            //--- Expected Result: Privacy & Security Statement ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacyAndSecurityStatement_Link);

            //--- Expected Result: Authorized Partners ---//
            test.validateElementIsPresent(driver, Elements.Forms_AuthorizedPartners_Link);
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Input Validations are Working as Expected for Valid Inputs in EN Locale")]
        public void Forms_DocumentFeedback_VerifyInputValidationsForValidInputs(string Locale)
        {
            //--- Document Feedback Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + documentFeedback_form_url;

            //--- Action: Navigate to the given URL ---//
            action.Navigate(driver, form_url);

            //----- Form TestPlan > EN Tab > R12 > T7_1: Verify the Page # field on the About the Document section with valid values (AL-4689) -----//

            //--- Action: Enter integers on the field (Page #) ---//
            string pageNumber_input = "123456789";
            action.IDeleteValueOnFields(driver, Elements.Forms_PageNumber_InputBox);
            action.IType(driver, Elements.Forms_PageNumber_InputBox, pageNumber_input);
            action.IType(driver, Elements.Forms_PageNumber_InputBox, Keys.Tab);

            //--- Expected Result: The integers should be accepted on the field ---//
            test.validateStringInstance(driver, pageNumber_input, util.GetInputBoxValue(driver, Elements.Forms_PageNumber_InputBox));

            //--- Action: Enter any special character on the field (Page #) (e.g. !@#$%^&*()) ---//
            pageNumber_input = "!@#$%^&*()";
            action.IDeleteValueOnFields(driver, Elements.Forms_PageNumber_InputBox);
            action.IType(driver, Elements.Forms_PageNumber_InputBox, pageNumber_input);
            action.IType(driver, Elements.Forms_PageNumber_InputBox, Keys.Tab);

            //--- Expected Result: The integers should be accepted on the field ---//
            test.validateStringInstance(driver, pageNumber_input, util.GetInputBoxValue(driver, Elements.Forms_PageNumber_InputBox));
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Input Validations are Working as Expected for Invalid Inputs in EN Locale")]
        public void Forms_DocumentFeedback_VerifyInputValidationsForInvalidInputs(string Locale)
        {
            //--- Document Feedback Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + documentFeedback_form_url;

            //--- Action: Navigate to the given URL ---//
            action.Navigate(driver, form_url);

            //----- Form TestPlan > EN Tab > R12 > T5_2: Submit the Document Feedback Fom with invalid data -----//

            //--- Action: Enter invalid data on the Email Address field. (e.g. 123!@#$%@yahoo.com) ---//
            string email_input = "123!@#$%@yahoo.com";
            action.IDeleteValueOnFields(driver, Elements.Forms_Email_InputBox);
            action.IType(driver, Elements.Forms_Email_InputBox, email_input);
            action.IType(driver, Elements.Forms_Email_InputBox, Keys.Tab);

            //--- Expected Result: The error message below should be displayed: "Please enter a valid email address" ---//
            test.validateStringIsCorrect(driver, Elements.Forms_DocumentFeedback_Email_InvalidInput_ErrorMessage, email_invalidInput_errorMessage);

            //----- Form TestPlan > EN Tab > R12 > T7_2: Verify the Page # field on the About the Document section with invalid values -----//

            //--- Action: Enter alpha characters on the field (Page #) ---//
            string pageNumber_input = util.RandomString();
            action.IDeleteValueOnFields(driver, Elements.Forms_PageNumber_InputBox);
            action.IType(driver, Elements.Forms_PageNumber_InputBox, pageNumber_input);
            action.IType(driver, Elements.Forms_PageNumber_InputBox, Keys.Tab);

            //--- Expected Result: The alpha characters should NOT be accepted on the field ---//
            test.validateStringInstance(driver, "", util.GetInputBoxValue(driver, Elements.Forms_PageNumber_InputBox));
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Input Validations are Working as Expected for Blank Inputs in EN Locale")]
        public void Forms_DocumentFeedback_VerifyInputValidationsForBlankInputs(string Locale)
        {
            //--- Document Feedback Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + documentFeedback_form_url;

            //--- Action: Navigate to the given URL ---//
            action.Navigate(driver, form_url);

            //----- Form TestPlan > EN Tab > R12 > T5_1: Submit Document Feedback Form with no data on mandatory fields (AL-11149 ) -----//

            //--- Action: Click on the Submit button without entering data on the mandatory fields. ---//
            action.IClick(driver, Elements.Forms_Submit_Button);

            //----- Document Feedback -----//

            //--- Expected Result: The Priority: "Priority is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_DocumentFeedback_Priority_BlankInput_ErrorMessage, priority_blankInput_errorMessage);

            //----- About you -----//

            //--- Expected Result: The Email: "Email address is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_Email_BlankInput_ErrorMessage, email_blankInput_errorMessage);

            //--- Expected Result: The Organization name : "Organization name is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_OrganizationName_BlankInput_ErrorMessage, organizationName_blankInput_errorMessage);

            //--- Expected Result: The Country(dropdown): "Country is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_CountryRegion_BlankInput_ErrorMessage, countryRegion_blankInput_errorMessage);

            //--- Expected Result: The Full name:  "Full name is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_FullName_BlankInput_ErrorMessage, fullName_blankInput_errorMessage);

            //--- Expected Result: The Telephone: "Telephone is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_Telephone_BlankInput_ErrorMessage, telephone_blankInput_errorMessage);

            //----- About the Document -----//

            //--- Expected Result: The Enter the Document Filename or Title:   "File Name is Required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_DocumentFeedback_DocumentTitle_BlankInput_ErrorMessage, documentTitle_blankInput_errorMessage);

            //--- Action: Select or, Enter Part Number: field ---//       
            //--- Action: Click on Tab key on your keyboard ---//
            action.IDeleteValueOnFields(driver, Elements.Forms_PartNumber_InputBox);
            action.IType(driver, Elements.Forms_PartNumber_InputBox, Keys.Tab);

            //--- Expected Result: "Please enter a Document Title or Part Number" on or, Enter Part Number: field should be displayed ---//
            test.validateStringIsCorrect(driver, Elements.Forms_PartNumber_BlankInput_ErrorMessage, partNumber_blankInput_errorMessage);
        }
    }
}