﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using OpenQA.Selenium;
using System;

namespace AS_Test_Framework.Forms
{
    [TestFixture]
    public class Forms_Hittite_AdiHmcPllDesignAndEvaluationSoftware : BaseSetUp
    {
        //--- ADI HMC PLL Design and Evaluation Software Download Form URL: ---//
        //--- DEV: https://www-dev.cldnet.analog.com/en/design-center/evaluation-hardware-and-software/PLL-design-and-evaluation-software.html ---//
        //--- QA: https://www-qa.cldnet.analog.com/en/design-center/evaluation-hardware-and-software/PLL-design-and-evaluation-software.htmlp ---//
        //--- PROD: https://www.analog.com/en/design-center/evaluation-hardware-and-software/PLL-design-and-evaluation-software.html ---//

        public Forms_Hittite_AdiHmcPllDesignAndEvaluationSoftware() : base() { }

        //--- URLs ---//
        string hittite_form_url = "/design-center/evaluation-hardware-and-software/PLL-design-and-evaluation-software.html ";

        //--- Labels ---//
        string email_subject = "Analog Devices HMC Product Software Download";
        string contactName_blankInput_errorMessage = "Name is required";
        string email_blankInput_errorMessage = "Email address is required";
        string companyName_blankInput_errorMessage = "Company Name is required";
        string licenseAgreement_blankInput_errorMessage = "Please select the checkbox";
        string email_invalidInput_errorMessage = "Please enter a valid email address";

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Input Validations are Working as Expected for Invalid Inputs in EN Locale")]
        public void Forms_Hittite_AdiHmcPllDesignAndEvaluationSoftware_VerifyInputValidationsForInvalidInputs(string Locale)
        {
            //--- Action: Navigate to the given URL ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + hittite_form_url);

            //----- Form TestPlan > Hittite Tab > R1 > T2_2: Verify submit form when filled with invalid inputs (AL-7845) -----//

            driver.SwitchTo().Frame(driver.FindElement(By.CssSelector("iframe[src*='hittitedownload']")));

            //----- About you -----//

            //--- Action: Fill in Email with invalida data (e.g. @, test@test, test.test)---//
            string email_input = "test@test";
            action.IDeleteValueOnFields(driver, Elements.Forms_Email_InputBox);
            action.IType(driver, Elements.Forms_Email_InputBox, email_input);
            action.IType(driver, Elements.Forms_Email_InputBox, Keys.Tab);

            //--- Expected Result: Error message saying "Please enter a valid email address" should be displayed ---//
            test.validateStringIsCorrect(driver, Elements.HittiteForms_AdiHmcPllDesignAndEvaluationSoftware_Email_InvalidInput_ErrorMessage, email_invalidInput_errorMessage);
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Input Validations are Working as Expected for Blank Inputs in EN Locale")]
        public void Forms_Hittite_AdiHmcPllDesignAndEvaluationSoftware_VerifyInputValidationsForBlankInputs(string Locale)
        {
            //--- Action: Navigate to the given URL ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + hittite_form_url);

            //----- Form TestPlan > Hittite Tab > R1 > T2_1: Verify submit form when all fields are blank (AL-7332) -----//

            driver.SwitchTo().Frame(driver.FindElement(By.CssSelector("iframe[src*='hittitedownload']")));

            //--- Action: Click on submit button ---//
            action.IClick(driver, Elements.Forms_Submit_Button);

            //----- About you -----//

            //--- Expected Result: The Contact Name: "Name is required" error message should be displayed  ---//
            test.validateStringInstance(driver, util.GetText(driver, Elements.Forms_ContactName_BlankInput_ErrorMessage), contactName_blankInput_errorMessage);

            //--- Expected Result: The Email: "Email address is required" error message should be displayed  ---//
            test.validateStringInstance(driver, util.GetText(driver, Elements.HittiteForms_AdiHmcPllDesignAndEvaluationSoftware_Email_BlankInput_ErrorMessage), email_blankInput_errorMessage);

            //--- Expected Result: The Company Name: "Company Name is required" error message should be displayed  ---//
            test.validateStringInstance(driver, util.GetText(driver, Elements.Forms_CompanyName_BlankInput_ErrorMessage), companyName_blankInput_errorMessage);

            //--- Expected Result: The License Agreement: "Please select the checkbox" error message should be displayed  ---//
            test.validateStringInstance(driver, util.GetText(driver, Elements.HittiteForms_AdiHmcPllDesignAndEvaluationSoftware_LicenseAgreement_BlankInput_ErrorMessage), licenseAgreement_blankInput_errorMessage);
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Form Submission is Working as Expected in EN Locale")]
        public void Forms_Hittite_AdiHmcPllDesignAndEvaluationSoftware_VerifyFormSubmission(string Locale)
        {
            if (Util.Configuration.Environment.Equals("dev") || Util.Configuration.Environment.Equals("qa"))
            {
                //--- Action: Navigate to the given URL ---//
                action.Navigate(driver, Configuration.Env_Url + Locale + hittite_form_url);

                //----- Form TestPlan > Hittite Tab > R1 > T3: Verify Received Email -----//

                driver.SwitchTo().Frame(driver.FindElement(By.CssSelector("iframe[src*='hittitedownload']")));

                //--- Action: Fill in all field with valid data ---//

                //----- About you -----//
                string contactName_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_ContactName_InputBox);
                action.IType(driver, Elements.Forms_ContactName_InputBox, contactName_input);

                string email_input = "t3st_" + Util.Configuration.Environment.ToLower() + "_adiHmcPllDesignAndEvaluationSoftware_f0rm_" + util.GenerateRandomNumber() + "@mailinator.com";
                //Console.WriteLine("Email Input = " + email_input);
                action.IDeleteValueOnFields(driver, Elements.Forms_Email_InputBox);
                action.IType(driver, Elements.Forms_Email_InputBox, email_input);

                string companyName_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_CompanyName_InputBox);
                action.IType(driver, Elements.Forms_CompanyName_InputBox, companyName_input);

                string countryRegion_input = "US";
                action.ISelectFromDropdown(driver, Elements.Forms_CountryRegion_Dropdown, countryRegion_input);

                action.ICheck(driver, Elements.Forms_Agree_Checkbox);

                //----- Privacy Settings -----//
                action.ICheck(driver, Elements.Forms_Communication_Checkbox);

                //--- Action: Click on the Submit button ---//
                action.IClick(driver, Elements.Forms_Submit_Button);

                //--- Action: Open email used to download the HMCdownloads ---//
                action.ICheckMailinatorEmail(driver, email_input);

                //--- Expected Result: Analog Devices HMC Product Software Download should be received ---//
                test.validateStringInstance(driver, util.GetText(driver, Elements.Mailinator_LatestEmail_Subj), email_subject);
            }
        }
    }
}