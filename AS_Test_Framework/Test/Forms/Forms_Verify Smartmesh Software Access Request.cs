﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using OpenQA.Selenium;
using System;
using System.Threading;

namespace AS_Test_Framework.Forms
{
    [TestFixture]
    public class Forms_SmartmeshSoftwareAccessRequest : BaseSetUp
    {
        //--- Smartmesh Software Access Request Form URL: ---//
        //--- DEV: https://form-dev.corpnt.analog.com/Form_Pages/LTC/SmartMeshSAR.aspx ---//
        //--- QA: https://form-qa.corpnt.analog.com/Form_Pages/LTC/SmartMeshSAR.aspx ---//
        //--- PROD: https://form.analog.com/Form_Pages/LTC/SmartMeshSAR.aspx ---//

        public Forms_SmartmeshSoftwareAccessRequest() : base() { }

        //--- Login Credentials ---//
        string username = "f0rms_t3st_555@mailinator.com";
        string password = "Test_1234";

        //--- URLs ---//
        string smartmeshSoftwareAccessRequest_form_url = "Form_Pages/LTC/SmartMeshSAR.aspx";
        string softwareAgreementRequestForm_thankYou_page_url = "/software_arf_form_thank_you.html";

        //--- Labels ---//
        string productSoftwareDownloadRequest_text = "Product Software Download Request";
        string countryRegion_text = "Country/Region";

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Elements are Present and Working as Expected when the User is Logged Out in EN Locale")]
        public void Forms_SmartmeshSoftwareAccessRequest_VerifyElementsWhenUserIsLoggedOut(string Locale)
        {
            //--- Smartmesh Software Access Request Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + smartmeshSoftwareAccessRequest_form_url;

            //--- Action: Navigate from the given URL ---//
            action.Navigate(driver, form_url);

            //----- Form TestPlan > EN Tab > R37 > T1: Verify the section of Smart mesh form (AL-10985) -----//

            //----- Product Software Download Request -----//

            //--- Expected Result: The title "Product Software Download Request" should be displayed. ---//
            test.validateStringIsCorrect(driver, Elements.Forms_SmartmeshSoftwareAccessRequest_Title_Label, productSoftwareDownloadRequest_text);

            //--- Expected Result: Instructions text should be present ---//
            test.validateElementIsPresent(driver, Elements.Forms_Instructions_Text);

            //----- Form TestPlan > EN Tab > R40 > T1: Verify all labels for Country is changed to "Country/Region" (IQ-4970/AL-12466) -----//

            //----- Smartmesh Software Access Request -----//

            //--- Expected Result: The Country label has been changed to Country/Region ---//
            test.validateStringInstance(driver, util.GetText(driver, Elements.Forms_Country_Label), countryRegion_text);

            //----- Form TestPlan > EN Tab > R38 > T1: Verify Privacy Settings in All Forms [account is not logged in] (AL-11345) -----//

            //----- Privacy Settings -----//

            //--- Expected Result: Privacy Settings section is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_Section);

            //--- Expected Result: Prompt "You can change your privacy settings at any time by clicking on the unsubscribe link in emails sent from Analog Devices or in Analog’s Privacy Settings." is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_LegalDisclaimer_Text);

            //--- Expected Result: Privacy Settings is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_Link);

            //--- Expected Result: Privacy & Security Statement ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacyAndSecurityStatement_Link);

            //--- Expected Result: Authorized Partners ---//
            test.validateElementIsPresent(driver, Elements.Forms_AuthorizedPartners_Link);

            //----- Form TestPlan > EN Tab > R37 > T4: Login using Save time to login section with valid data -----//

            //----- Save time by logging in or registering for myAnalog -----//

            if (!util.CheckElement(driver, Elements.Forms_Expanded_SaveTimeByLoggingInOrRegisteringForMyAnalog_Panel, 1))
            {
                //--- Action: Click on Save time by logging in or registering for my Analog   link ---//
                action.IClick(driver, Elements.Forms_SaveTimeByLoggingInOrRegisteringForMyAnalog_Link);
            }

            //--- Expected Result: Save time to login section should be displayed ---//
            test.validateElementIsPresent(driver, Elements.Forms_Expanded_SaveTimeByLoggingInOrRegisteringForMyAnalog_Panel);
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Elements are Present and Working as Expected when the User is Logged In in EN Locale")]
        public void Forms_SmartmeshSoftwareAccessRequest_VerifyElementsWhenUserIsLoggedIn(string Locale)
        {
            //--- Smartmesh Software Access Request Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + smartmeshSoftwareAccessRequest_form_url;

            //--- Action: Navigate from the given URL ---//
            action.Navigate(driver, form_url);

            //----- Form TestPlan > EN Tab > R37 > T4: Login using Save time to login section with valid data -----//

            //----- Save time by logging in or registering for myAnalog -----//

            //--- Action: Login using valid data ---//
            action.ILogInViaSaveTimeByLoggingInPanel(driver, username, password);
            Thread.Sleep(4000);

            //--- Expected Result: Save time to login section should not be displayed ---//
            test.validateElementIsNotPresent(driver, Elements.Forms_Expanded_SaveTimeByLoggingInOrRegisteringForMyAnalog_Panel);

            //----- Form TestPlan > EN Tab > R38 > T1: Verify Privacy Settings in All Forms [account is logged in] (AL-11345) -----//

            //----- Privacy Settings -----//

            //--- Expected Result: Prompt "You can change your privacy settings at any time by clicking on the unsubscribe link in emails sent from Analog Devices or in Analog’s Privacy Settings." is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_LegalDisclaimer_Text);

            //--- Expected Result: Privacy Settings is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_Link);

            //--- Expected Result: Privacy & Security Statement ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacyAndSecurityStatement_Link);

            //--- Expected Result: Authorized Partners ---//
            test.validateElementIsPresent(driver, Elements.Forms_AuthorizedPartners_Link);
        }

        [TestCase("en", TestName = "Verify that the Input Validations are Working as Expected for Valid Inputs in EN Locale")]
        public void Forms_SmartmeshSoftwareAccessRequest_VerifyInputValidationsForValidInputs(string Locale)
        {
            //--- Smartmesh Software Access Request Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + smartmeshSoftwareAccessRequest_form_url;

            //--- Action: Navigate from the given URL ---//
            action.Navigate(driver, form_url);

            //----- Form TestPlan > EN Tab > R37 > T5: Verify Mandatory Fields -----//

            //----- Smartmesh Software Access Request -----//

            //--- Action: Select "Other" in radio button for product description---//
            action.IClick(driver, Elements.Forms_ProductDescription_Other_RadioButton);

            //--- Expected Result: The For which product(s) do you need information? Field will be enabled ---//
            test.validateElementIsEnabled(driver, Elements.Forms_ForWhichProductsDoYouNeedInformation_InputBox);

            //--- Action: Click "SmartMesh WirelessHART"  in radio button for product description ---//
            action.IClick(driver, Elements.Forms_ProductDescription_SmartMeshWirelessHart_RadioButton);

            //--- Expected Result: The For which product(s) do you need information? Field will be disabled ---//
            test.validateElementIsNotEnabled(driver, Elements.Forms_ForWhichProductsDoYouNeedInformation_InputBox);

            //--- Action: Select "Other, please specify below" in radio button for software version ---//
            action.IClick(driver, Elements.Forms_SoftwareVersion_OtherPleaseSpecifyBelow_RadioButton);

            //--- Expected Result: "Specific Version" field will be enabled ---//
            test.validateElementIsEnabled(driver, Elements.Forms_SpecificVersion_InputBox);

            //--- Action: Click "Current Version"  in radio button for software version ---//
            action.IClick(driver, Elements.Forms_SoftwareVersion_CurrentVersion_RadioButton);

            //--- Expected Result: "Specific Version" field will be disabled ---//
            test.validateElementIsNotEnabled(driver, Elements.Forms_SpecificVersion_InputBox);
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Input Validations are Working as Expected for Blank Inputs in EN Locale")]
        public void Forms_SmartmeshSoftwareAccessRequest_VerifyInputValidationsForBlankInputs(string Locale)
        {
            //--- Smartmesh Software Access Request Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + smartmeshSoftwareAccessRequest_form_url;

            //--- Action: Navigate from the given URL ---//
            action.Navigate(driver, form_url);

            //----- Form TestPlan > EN Tab > R37 > T5: Verify Mandatory Fields -----//

            //--- Action: Do not enter input on target date field ---//
            //--- Action: Click on the Submit button ---//
            action.IClick(driver, Elements.Forms_Submit_Button);

            //----- Smartmesh Software Access Request -----//

            //--- Expected Result: An error will show that the Email field is required. ---//
            test.validateElementIsPresent(driver, Elements.Forms_SmartmeshSoftwareAccessRequest_Email_BlankInput_ErrorMessage);

            //--- Expected Result: An error will show that the First Name field is required. ---//
            test.validateElementIsPresent(driver, Elements.Forms_SmartmeshSoftwareAccessRequest_FirstName_BlankInput_ErrorMessage);

            //--- Expected Result: An error will show that the Last Name field is required. ---//
            test.validateElementIsPresent(driver, Elements.Forms_SmartmeshSoftwareAccessRequest_FirstName_BlankInput_ErrorMessage);

            //--- Expected Result: An error will show that the Address 1 field is required. ---//
            test.validateElementIsPresent(driver, Elements.Forms_SmartmeshSoftwareAccessRequest_Address_1_BlankInput_ErrorMessage);

            //--- Expected Result: An error will show that the City field is required. ---//
            test.validateElementIsPresent(driver, Elements.Forms_SmartmeshSoftwareAccessRequest_City_BlankInput_ErrorMessage);

            //--- Expected Result: An error will show that the Country field is required. ---//
            test.validateElementIsPresent(driver, Elements.Forms_SmartmeshSoftwareAccessRequest_CountryRegion_BlankInput_ErrorMessage);

            //--- Expected Result: An error will show that the State/Province field is required. ---//
            test.validateElementIsPresent(driver, Elements.Forms_SmartmeshSoftwareAccessRequest_StateProvince_BlankInput_ErrorMessage);

            //--- Expected Result: An error will show that the Organization Name field is required. ---//
            test.validateElementIsPresent(driver, Elements.Forms_SmartmeshSoftwareAccessRequest_OrganizationName_BlankInput_ErrorMessage);

            //--- Expected Result: An error will show that the Phone Number field is required. ---//
            test.validateElementIsPresent(driver, Elements.Forms_SmartmeshSoftwareAccessRequest_PhoneNumber_BlankInput_ErrorMessage);

            //--- Expected Result: An error will show that the Product Description field is required. ---//
            test.validateElementIsPresent(driver, Elements.Forms_SmartmeshSoftwareAccessRequest_ProductDescription_BlankInput_ErrorMessage);

            //--- Expected Result: An error will show that the Software version field is required. ---//
            test.validateElementIsPresent(driver, Elements.Forms_SmartmeshSoftwareAccessRequest_SoftwareVersion_BlankInput_ErrorMessage);
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Form Submission is Working as Expected in EN Locale")]
        public void Forms_SmartmeshSoftwareAccessRequest_VerifyFormSubmission(string Locale)
        {
            if (Util.Configuration.Environment.Equals("dev") || Util.Configuration.Environment.Equals("qa"))
            {
                //--- Smartmesh Software Access Request Form URL ---//
                string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + smartmeshSoftwareAccessRequest_form_url;

                //--- Action: Navigate from the given URL ---//
                action.Navigate(driver, form_url);

                //----- Form TestPlan > EN Tab > R37 > T6: Verify can submit the form successfully. -----//

                //--- Action: Enter and select valid values on all fields ---//

                //----- Smartmesh Software Access Request -----//
                string email_input = "t3st_" + Util.Configuration.Environment.ToLower() + "_smartmeshSoftwareAccessRequest_f0rm_" + util.GenerateRandomNumber() + "@mailinator.com";
                //Console.WriteLine("Email Input = " + email_input);
                action.IDeleteValueOnFields(driver, Elements.Forms_Email_InputBox);
                action.IType(driver, Elements.Forms_Email_InputBox, email_input);

                string firstName_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_FirstName_InputBox);
                action.IType(driver, Elements.Forms_FirstName_InputBox, firstName_input);

                string lastName_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_LastName_InputBox);
                action.IType(driver, Elements.Forms_LastName_InputBox, lastName_input);

                string address_1_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_SmartmeshSoftwareAccessRequest_Address_1_InputBox);
                action.IType(driver, Elements.Forms_SmartmeshSoftwareAccessRequest_Address_1_InputBox, address_1_input);

                string city_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_City_InputBox);
                action.IType(driver, Elements.Forms_City_InputBox, city_input);

                string countryRegion_input = "US";
                action.ISelectFromDropdown(driver, Elements.Forms_CountryRegion_Dropdown, countryRegion_input);

                action.IClick(driver, Elements.Forms_StateProvince_Dropdown);
                action.IClick(driver, By.CssSelector("select[id$='tate']>option:nth-of-type(3)"));

                string organizationName_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_OrganizationName_InputBox);
                action.IType(driver, Elements.Forms_OrganizationName_InputBox, organizationName_input);

                string phoneNumber_input = "123456789";
                action.IDeleteValueOnFields(driver, Elements.Forms_PhoneNumber_InputBox);
                action.IType(driver, Elements.Forms_PhoneNumber_InputBox, phoneNumber_input);

                action.IClick(driver, Elements.Forms_ProductDescription_RadioButtons);

                action.IClick(driver, Elements.Forms_SoftwareVersion_RadioButtons);

                //----- Privacy Settings -----//
                action.ICheck(driver, Elements.Forms_Communication_Checkbox);

                action.IClick(driver, Elements.Forms_Submit_Button);

                //--- Expected Result: The page is redirected to the Software: Agreement Request Form Thank You---//
                test.validateStringInstance(driver, driver.Url, softwareAgreementRequestForm_thankYou_page_url);
            }
        }
    }
}