﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using OpenQA.Selenium;
using System;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.Forms
{
    [TestFixture]
    public class Forms_DesignFilePackage : BaseSetUp
    {
        //--- Design File Package Form URL: ---//
        //--- DEV: https://form-dev.corpnt.analog.com/Form_Pages/securedownloads/designFilePackage.aspx?prodID=ad9250 ---//
        //--- QA: https://form-qa.corpnt.analog.com/Form_Pages/securedownloads/designFilePackage.aspx?prodID=ad9250 ---//
        //--- PROD: https://form.analog.com/Form_Pages/securedownloads/designFilePackage.aspx?prodID=ad9250 ---//

        public Forms_DesignFilePackage() : base() { }

        //--- Login Credentials ---//
        string username = "f0rms_t3st_555@mailinator.com";
        string password = "Test_1234";

        //--- URLs ---//
        string designFilePackage_form_url = "Form_Pages/securedownloads/designFilePackage.aspx?prodID=ad9250";
        string registration_page_url = "/app/registration";
        string amiModelDownload_thankYou_page_url = "/ami-model-download-thank-you.html";

        //--- Labels ---//
        string analogDevices_text = "Analog Devices";
        string countryRegion_text = "Country/Region";
        string state_blankInput_errorMessage = "State/Province is required";
        string simulationToolUsed_blankInput_errorMessage = "Simulation Tool is required";

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Elements are Present and Working as Expected when the User is Logged Out in EN Locale")]
        public void Forms_DesignFilePackage_VerifyElementsWhenUserIsLoggedOut(string Locale)
        {
            //--- Design File Package Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + designFilePackage_form_url;

            //--- Action: Navigate to the given URL ---//
            action.Navigate(driver, form_url);

            //----- Form TestPlan > EN Tab > R30 > T1: Verify Page Title Section -----//

            //--- Expected Result: Title is displayed in the browser / tab ---//
            test.validateWindowTitleIsCorrect(driver, analogDevices_text);

            //----- Design File Package -----//

            //--- Expected Result: Title is present nad properly formatted above the form ---//
            test.validateElementIsPresent(driver, Elements.Forms_Title_Label);

            //----- Form TestPlan > EN Tab > R30 > T2: Verify About You Section -----//

            //----- About you -----//

            //--- Expected Result: The Full Name field is present. ---//
            test.validateElementIsPresent(driver, Elements.Forms_FullName_InputBox);

            //--- Expected Result: The Job Title field is present. ---//
            test.validateElementIsPresent(driver, Elements.Forms_JobTitle_InputBox);

            //--- Expected Result: The Application field is present. ---//
            test.validateElementIsPresent(driver, Elements.Forms_Application_Dropdown);

            //--- Expected Result: The Email field is present. ---//
            test.validateElementIsPresent(driver, Elements.Forms_Email_InputBox);

            //----- Form TestPlan > EN Tab > R30 > T3: Verify Your Detailed Information Section -----//

            //----- Your detailed information -----//

            //--- Expected Result: the Company name field is present. ---//
            test.validateElementIsPresent(driver, Elements.Forms_CompanyName_InputBox);

            //--- Expected Result: the Address field is present. ---//
            test.validateElementIsPresent(driver, Elements.Forms_Address_InputBox);

            //--- Expected Result: the Telephone field is present. ---//
            test.validateElementIsPresent(driver, Elements.Forms_Telephone_InputBox);

            //--- Expected Result: the City field is present. ---//
            test.validateElementIsPresent(driver, Elements.Forms_City_InputBox);

            //--- Expected Result: the Zip/Postal code field is present. ---//
            test.validateElementIsPresent(driver, Elements.Forms_ZipPostalCode_InputBox);

            //----- Form TestPlan > EN Tab > R40 > T1: Verify all labels for Country is changed to "Country/Region" (IQ-4970/AL-12466) -----//

            //--- Expected Result: The Country label has been changed to Country/Region ---//
            test.validateStringInstance(driver, util.GetText(driver, Elements.Forms_DesignFilePackage_Country_Label), countryRegion_text);

            //----- Form TestPlan > EN Tab > R30 > T3: Verify Your Detailed Information Section -----//

            //--- Expected Result: the Country field is present. ---//
            test.validateElementIsPresent(driver, Elements.Forms_CountryRegion_Dropdown);

            //--- Expected Result: the State field is present. ---//
            test.validateElementIsPresent(driver, Elements.Forms_StateProvince_Dropdown);

            //--- Expected Result: the If Outside the US or Canada, please enter the region field is present. ---//
            test.validateElementIsPresent(driver, Elements.Forms_IfOutsideTheUsOrCanadaPleaseEnterTheRegion_InputBox);

            //----- Form TestPlan > EN Tab > R30 > T4: Verify About Your Project Section -----//

            //----- About your project -----//

            //--- Expected Result: Your Detailed Information section is present in the page with the Simulation Tool Used field. ---//
            test.validateElementIsPresent(driver, Elements.Forms_SimulationToolUsed_Dropdown);

            //--- Expected Result: Your Detailed Information section is present in the page with the If other,Please specify field. ---//
            test.validateElementIsPresent(driver, Elements.Forms_IfOtherPleaseSpecify_InputBox);

            //----- Form TestPlan > EN Tab > R38 > T1: Verify Privacy Settings in All Forms [account is not logged in] (AL-11345) -----//

            //----- Privacy Settings -----//

            //--- Expected Result: Privacy Settings section is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_Section);

            //--- Expected Result: Prompt "You can change your privacy settings at any time by clicking on the unsubscribe link in emails sent from Analog Devices or in Analog’s Privacy Settings." is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_LegalDisclaimer_Text);

            //--- Expected Result: Privacy Settings is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_Link);

            //--- Expected Result: Privacy & Security Statement ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacyAndSecurityStatement_Link);

            //--- Expected Result: Authorized Partners ---//
            test.validateElementIsPresent(driver, Elements.Forms_AuthorizedPartners_Link);

            //----- Form TestPlan > EN Tab > R30 > T5: Verify Save Time To Login -----//

            //----- Save time by logging in or registering for myAnalog -----//

            if (!util.CheckElement(driver, Elements.Forms_Expanded_SaveTimeByLoggingInOrRegisteringForMyAnalog_Panel, 1))
            {
                //--- Action: Click the "Save time by logging in or registering for myAnalog" text in the Save Time To Login bar to expand or collapse ---//
                action.IClick(driver, Elements.Forms_SaveTimeByLoggingInOrRegisteringForMyAnalog_Link);
            }

            //--- Expected Result: Save time to login will Expand / Collpase ---//
            test.validateElementIsPresent(driver, Elements.Forms_Expanded_SaveTimeByLoggingInOrRegisteringForMyAnalog_Panel);

            if (util.CheckElement(driver, Elements.Forms_Expanded_SaveTimeByLoggingInOrRegisteringForMyAnalog_Panel_RegisterNow_Link, 1))
            {
                //--- Action: Click "Register now." Link ---//
                action.IOpenLinkInNewTab(driver, Elements.Forms_Expanded_SaveTimeByLoggingInOrRegisteringForMyAnalog_Panel_RegisterNow_Link);

                //--- Expected Result: Redirects to "Register now." Page ---//
                test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + registration_page_url);

                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles.Last());
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.Forms_Expanded_SaveTimeByLoggingInOrRegisteringForMyAnalog_Panel_RegisterNow_Link);
            }
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Elements are Present and Working as Expected when the User is Logged In in EN Locale")]
        public void Forms_DesignFilePackage_VerifyElementsWhenUserIsLoggedIn(string Locale)
        {
            //--- Design File Package Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + designFilePackage_form_url;

            //--- Action: Navigate to the given URL ---//
            action.Navigate(driver, form_url);

            //----- Form TestPlan > EN Tab > R30 > T9: Verify that Form is successfully submitted as Registered user -----//

            //----- Save time by logging in or registering for myAnalog -----//

            //--- Action: Login using valid credential ---//
            action.ILogInViaSaveTimeByLoggingInPanel(driver, username, password);
            Thread.Sleep(2000);

            driver.Navigate().Refresh();
            Thread.Sleep(1000);

            //----- About you -----//

            //--- Expected Result: The Full Name field should be populated ---//
            test.validateFieldIsNotEmpty(driver, Elements.Forms_FullName_InputBox);

            //--- Expected Result: The Email field should be populated ---//
            test.validateFieldIsNotEmpty(driver, Elements.Forms_Email_InputBox);

            //----- Your detailed information -----//

            //--- Expected Result: The Company Name field should be populated ---//
            test.validateFieldIsNotEmpty(driver, Elements.Forms_CompanyName_InputBox);

            //--- Expected Result: The Country field should be populated ---//
            test.validateStringChanged(driver, util.GetSelectedDropdownValue(driver, Elements.Forms_CountryRegion_Dropdown), "Select Country/Region");

            //--- Expected Result: The State field should be populated ---//
            test.validateStringChanged(driver, util.GetSelectedDropdownValue(driver, Elements.Forms_StateProvince_Dropdown), "Select State/Province");

            //----- Form TestPlan > EN Tab > R38 > T1: Verify Privacy Settings in All Forms [account is logged in] (AL-11345) -----//

            //----- Privacy Settings -----//

            //--- Expected Result: Prompt "You can change your privacy settings at any time by clicking on the unsubscribe link in emails sent from Analog Devices or in Analog’s Privacy Settings." is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_LegalDisclaimer_Text);

            //--- Expected Result: Privacy Settings is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_Link);

            //--- Expected Result: Privacy & Security Statement ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacyAndSecurityStatement_Link);

            //--- Expected Result: Authorized Partners ---//
            test.validateElementIsPresent(driver, Elements.Forms_AuthorizedPartners_Link);
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Input Validations are Working as Expected for Valid Inputs in EN Locale")]
        public void Forms_DesignFilePackage_VerifyInputValidationsForValidInputs(string Locale)
        {
            //--- Design File Package Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + designFilePackage_form_url;

            //--- Action: Navigate to the given URL ---//
            action.Navigate(driver, form_url);

            //----- Form TestPlan > EN Tab > R30 > T4: Verify About Your Project Section -----//

            //----- About your project -----//

            //--- Action: Select Value from the Simulation Tool Used drop down (any in the list aside from others) ---//
            action.IClick(driver, Elements.Forms_SimulationToolUsed_Dropdown);
            action.IClick(driver, By.CssSelector("select[id$='ddlSimulation']>option:nth-of-type(2)"));

            //--- Expected Result: If other,Please specify field is not active and not edittable. ---//
            test.validateElementIsNotEnabled(driver, Elements.Forms_IfOtherPleaseSpecify_InputBox);

            //--- Action: Select Others as Value from the Simulation Tool Used drop down ---//
            string simulationToolUsed_input = "NA";
            action.ISelectFromDropdown(driver, Elements.Forms_SimulationToolUsed_Dropdown, simulationToolUsed_input);

            //--- Expected Result: If other,Please specify field will be activated and edittable. ---//
            test.validateElementIsEnabled(driver, Elements.Forms_IfOtherPleaseSpecify_InputBox);
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Input Validations are Working as Expected for Blank Inputs in EN Locale")]
        public void Forms_DesignFilePackage_VerifyInputValidationsForBlankInputs(string Locale)
        {
            //--- Design File Package Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + designFilePackage_form_url;

            //--- Action: Navigate to the given URL ---//
            action.Navigate(driver, form_url);

            //----- Form TestPlan > EN Tab > R30 > T12: Submit AD9250 Design File Package Form with no data on mandatory fields (IQ-7801/AL-14963)-----//

            //--- Action: Click on the Submit button without entering data on the mandatory fields. ---//
            action.IClick(driver, Elements.Forms_Submit_Button);

            //----- Your detailed information -----//

            //--- Expected Result: The State: "State/Province is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_DesignFilePackage_StateProvince_BlankInput_ErrorMessage, state_blankInput_errorMessage);

            //----- About your project -----//

            //--- Expected Result: The Simulation Tool Used: "Simulation Tool is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_DesignFilePackage_SimulationToolUsed_BlankInput_ErrorMessage, simulationToolUsed_blankInput_errorMessage);
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Form Submission is Working as Expected in EN Locale")]
        public void Forms_DesignFilePackage_VerifyFormSubmission(string Locale)
        {
            if (Util.Configuration.Environment.Equals("dev") || Util.Configuration.Environment.Equals("qa"))
            {
                //--- Design File Package Form URL ---//
                string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + designFilePackage_form_url;

                //--- Action: Navigate to the given URL ---//
                action.Navigate(driver, form_url);

                //----- Form TestPlan > EN Tab > R30 > T9: Verify that Form is successfully submitted as Registered user -----//

                //--- Action: Enter valid values in the fields ---//

                //----- About you -----//
                string fullName_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_FullName_InputBox);
                action.IType(driver, Elements.Forms_FullName_InputBox, fullName_input);

                string jobTitle_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_JobTitle_InputBox);
                action.IType(driver, Elements.Forms_JobTitle_InputBox, jobTitle_input);

                action.IClick(driver, Elements.Forms_Application_Dropdown);
                action.IClick(driver, By.CssSelector("select[id$='ddlApplication']>option:nth-of-type(3)"));

                string email_input = "t3st_" + Util.Configuration.Environment.ToLower() + "_designFilePackage_f0rm_" + util.GenerateRandomNumber() + "@mailinator.com";
                //Console.WriteLine("Email Input = " + email_input);
                action.IDeleteValueOnFields(driver, Elements.Forms_Email_InputBox);
                action.IType(driver, Elements.Forms_Email_InputBox, email_input);

                //----- Your detailed information -----//
                string companyName_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_CompanyName_InputBox);
                action.IType(driver, Elements.Forms_CompanyName_InputBox, companyName_input);

                string address_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_Address_InputBox);
                action.IType(driver, Elements.Forms_Address_InputBox, address_input);

                string telephone_input = "123456789";
                action.IDeleteValueOnFields(driver, Elements.Forms_Telephone_InputBox);
                action.IType(driver, Elements.Forms_Telephone_InputBox, telephone_input);

                string city_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_City_InputBox);
                action.IType(driver, Elements.Forms_City_InputBox, city_input);

                string countryRegion_input = "US";
                action.ISelectFromDropdown(driver, Elements.Forms_CountryRegion_Dropdown, countryRegion_input);

                action.IClick(driver, Elements.Forms_StateProvince_Dropdown);
                action.IClick(driver, By.CssSelector("select[id$='tate']>option:nth-of-type(3)"));

                string zipPostalCode_input = "12345";
                action.IDeleteValueOnFields(driver, Elements.Forms_ZipPostalCode_InputBox);
                action.IType(driver, Elements.Forms_ZipPostalCode_InputBox, zipPostalCode_input);

                //----- About your project -----//
                action.IClick(driver, Elements.Forms_SimulationToolUsed_Dropdown);
                action.IClick(driver, By.CssSelector("select[id$='ddlSimulation']>option:nth-of-type(2)"));

                if (driver.FindElement(Elements.Forms_IfOtherPleaseSpecify_InputBox).Enabled)
                {
                    string ifOtherPleaseSpecify_input = "This is Just A Test";
                    action.IDeleteValueOnFields(driver, Elements.Forms_IfOtherPleaseSpecify_InputBox);
                    action.IType(driver, Elements.Forms_IfOtherPleaseSpecify_InputBox, ifOtherPleaseSpecify_input);
                }

                //----- Privacy Settings -----//
                action.ICheck(driver, Elements.Forms_Communication_Checkbox);

                //--- Action: Click Submit button ---//
                action.IClick(driver, Elements.Forms_Submit_Button);

                //--- Expected Result: The page will redirect to the AMI Model Download - Thank You page ---//
                test.validateStringInstance(driver, driver.Url, amiModelDownload_thankYou_page_url);
            }
        }
    }
}