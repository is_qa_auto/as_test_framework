﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using OpenQA.Selenium;
using System;
using System.Threading;

namespace AS_Test_Framework.Forms
{
    [TestFixture]
    public class Forms_CatalinaDesign : BaseSetUp
    {
        //--- Catalina Design Form URL: ---//
        //--- DEV: https://form-dev.corpnt.analog.com/form_pages/Catalina/CatalinaDesign.aspx?ProdID=AD9364 ---//
        //--- QA: https://form-qa.corpnt.analog.com/form_pages/Catalina/CatalinaDesign.aspx?ProdID=AD9364 ---//
        //--- PROD: https://form.analog.com/form_pages/Catalina/CatalinaDesign.aspx?ProdID=AD9364 ---//

        public Forms_CatalinaDesign() : base() { }

        //--- Login Credentials ---//
        string username = "f0rms_t3st_555@mailinator.com";
        string password = "Test_1234";

        //--- URLs ---//
        string catalinaDesign_form_url = "form_pages/Catalina/CatalinaDesign.aspx?ProdID=AD9364";
        string ad9364DesignFileDownloadAgreement_page_url = "icense/licensing-agreement/ad9364.html";
        string sdrIntegratedTransceiverDesignResources_page_url = "/integrated-rf-agile-transceiver-design-resources.html";

        //--- Labels ---//
        string form_title = "AD9364 Design File Package";
        string countryRegion_text = "Country/Region";

        string Name_blankInput_errorMessage = "Name is required";
        string jobTitle_blankInput_errorMessage = "Job Title is required";
        string application_blankInput_errorMessage = "Application is required";
        string email_blankInput_errorMessage = "Email address is required";
        string companyName_blankInput_errorMessage = "Company name is required";
        string address_blankInput_errorMessage = "Address 1 is required";
        string telephone_blankInput_errorMessage = "Telephone is required";
        string city_blankInput_errorMessage = "City is required";
        string zipPostalCode_blankInput_errorMessage = "Zip/Postal Code is required";
        string countryRegion_blankInput_errorMessage = "Country/Region is required";
        string state_blankInput_errorMessage = "State/Province is required";
        string ifOutsideTheUsOrCanadaPleaseEnterTheRegion_blankInput_errorMessage = "Please provide other State/Province";
        string email_invalidInput_errorMessage = "Please enter a valid email address";

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Elements are Present and Working as Expected when the User is Logged Out in EN Locale")]
        public void Forms_CatalinaDesign_VerifyElementsWhenUserIsLoggedOut(string Locale)
        {
            //--- Catalina Design Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + catalinaDesign_form_url;

            //--- Action: Navigate to the given URL ---//
            action.Navigate(driver, form_url);

            //----- Form TestPlan > EN Tab > R15 > T3: Verify Catalina Design File Form Title  -----//

            //----- AD9364 Design File Package -----//

            //--- Expected Result: Form Title should be AD9364 Design File Package since the url used above has a query parameter of model number AD9364 ---//
            test.validateStringIsCorrect(driver, Elements.Forms_Title_Label, form_title);

            //----- Form TestPlan > EN Tab > R40 > T1: Verify all labels for Country is changed to "Country/Region" (IQ-4970/AL-12466) -----//

            //----- Your detailed information -----//

            //--- Expected Result: The Country label has been changed to Country/Region ---//
            test.validateStringInstance(driver, util.GetText(driver, Elements.Forms_CatalinaDesign_Country_Label), countryRegion_text);

            //----- Form TestPlan > EN Tab > R38 > T1: Verify Privacy Settings in All Forms [account is not logged in] (AL-11345) -----//

            //----- Privacy Settings -----//

            //--- Expected Result: Privacy Settings section is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_Section);

            //--- Expected Result: Prompt "You can change your privacy settings at any time by clicking on the unsubscribe link in emails sent from Analog Devices or in Analog’s Privacy Settings." is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_LegalDisclaimer_Text);

            //--- Expected Result: Privacy Settings is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_Link);

            //--- Expected Result: Privacy & Security Statement ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacyAndSecurityStatement_Link);

            //--- Expected Result: Authorized Partners ---//
            test.validateElementIsPresent(driver, Elements.Forms_AuthorizedPartners_Link);

            //----- Form TestPlan > EN Tab > R15 > T4_1: Login using Save time to login section with valid data (AL-7439) -----//

            //----- Save time by logging in or registering for myAnalog -----//

            if (!util.CheckElement(driver, Elements.Forms_Expanded_SaveTimeByLoggingInOrRegisteringForMyAnalog_Panel, 1))
            {
                //--- Action: Click on Save time by logging in or registering for myAnalog  link ---//
                action.IClick(driver, Elements.Forms_SaveTimeByLoggingInOrRegisteringForMyAnalog_Link);
            }

            //--- Expected Result: Save time to login section should be displayed ---//
            test.validateElementIsPresent(driver, Elements.Forms_Expanded_SaveTimeByLoggingInOrRegisteringForMyAnalog_Panel);
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Elements are Present and Working as Expected when the User is Logged In in EN Locale")]
        public void Forms_CatalinaDesign_VerifyElementsWhenUserIsLoggedIn(string Locale)
        {
            //--- Catalina Design Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + catalinaDesign_form_url;

            //--- Action: Navigate to the given URL ---//
            action.Navigate(driver, form_url);

            //----- Form TestPlan > EN Tab > R15 > T4_1: Login using Save time to login section with valid data (AL-7439) -----//

            //--- Action: Login using valid data ---//
            action.ILogInViaSaveTimeByLoggingInPanel(driver, username, password);
            Thread.Sleep(1000);

            //----- About you -----//

            //--- Expected Result: The Full Name field should be  automatically populated ---//
            test.validateFieldIsNotEmpty(driver, Elements.Forms_FirstName_InputBox);

            //--- Expected Result: The Full Name field should be  automatically populated ---//
            test.validateFieldIsNotEmpty(driver, Elements.Forms_LastName_InputBox);

            //--- Expected Result: The Email field should be  automatically populated ---//
            test.validateFieldIsNotEmpty(driver, Elements.Forms_Email_InputBox);

            //----- Your detailed information -----//

            //--- Expected Result: The Company Name field should be  automatically populated ---//
            test.validateFieldIsNotEmpty(driver, Elements.Forms_CompanyName_InputBox);

            //--- Expected Result: The Zip/Postal Code field should be  automatically populated ---//
            test.validateFieldIsNotEmpty(driver, Elements.Forms_ZipPostalCode_InputBox);

            //--- Expected Result: The Country(dropdown) field should be  automatically populated ---//
            test.validateStringChanged(driver, util.GetSelectedDropdownValue(driver, Elements.Forms_CountryRegion_Dropdown), "Select Country/Region");

            //--- Expected Result: The State(dropdown) field should be  automatically populated ---//
            test.validateStringChanged(driver, util.GetSelectedDropdownValue(driver, Elements.Forms_StateProvince_Dropdown), "Select State/Province");

            //----- Form TestPlan > EN Tab > R38 > T1: Verify Privacy Settings in All Forms [account is logged in] (AL-11345) -----//

            //----- Privacy Settings -----//

            //--- Expected Result: Prompt "You can change your privacy settings at any time by clicking on the unsubscribe link in emails sent from Analog Devices or in Analog’s Privacy Settings." is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_LegalDisclaimer_Text);

            //--- Expected Result: Privacy Settings is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_Link);

            //--- Expected Result: Privacy & Security Statement ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacyAndSecurityStatement_Link);

            //--- Expected Result: Authorized Partners ---//
            test.validateElementIsPresent(driver, Elements.Forms_AuthorizedPartners_Link);
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Input Validations are Working as Expected for Invalid Inputs in EN Locale")]
        public void Forms_CatalinaDesign_VerifyInputValidationsForInvalidInputs(string Locale)
        {
            //--- Catalina Design Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + catalinaDesign_form_url;

            //--- Action: Navigate to the given URL ---//
            action.Navigate(driver, form_url);

            //----- Form TestPlan > EN Tab > R15 > T5_2: Submit the Catalina Design Form with invalid data -----//

            //--- Action: Enter invalid data on the Email Address field. (e.g. 123!@#$%@yahoo.com) ---//
            string email_input = "123!@#$%@yahoo.com";
            action.IDeleteValueOnFields(driver, Elements.Forms_Email_InputBox);
            action.IType(driver, Elements.Forms_Email_InputBox, email_input);
            action.IType(driver, Elements.Forms_Email_InputBox, Keys.Tab);

            //--- Expected Result: The error message below should be displayed: "Please enter a valid email address" ---//
            test.validateStringIsCorrect(driver, Elements.Forms_CatalinaDesign_Email_InvalidInput_ErrorMessage, email_invalidInput_errorMessage);

            //----- Form TestPlan > EN Tab > R15 > T5_3: Verify Catalina Design Form error messages (AL-2631) -----//

            //--- Action: On State/Province dropdown select value "Outside the US, Mexico, or Canada" ---//
            action.IClick(driver, Elements.Forms_StateProvince_Dropdown);
            action.IClick(driver, By.CssSelector("select[id$='tate']>option:nth-of-type(2)"));

            //--- Action: Leave the "If Outside the US or Canada, please enter the region" empty ---//
            action.IDeleteValueOnFields(driver, Elements.Forms_IfOutsideTheUsOrCanadaPleaseEnterTheRegion_InputBox);
            action.IType(driver, Elements.Forms_IfOutsideTheUsOrCanadaPleaseEnterTheRegion_InputBox, Keys.Tab);

            //--- Action: Submit the form ---//
            action.IClick(driver, Elements.Forms_Submit_Button);

            //--- Expected Result: The error message below the textbox should be displayed: "Please provide other state/Province". ---//
            test.validateStringIsCorrect(driver, Elements.Forms_IfOutsideTheUsOrCanadaPleaseEnterTheRegion_BlankInput_ErrorMessage, ifOutsideTheUsOrCanadaPleaseEnterTheRegion_blankInput_errorMessage);
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Input Validations are Working as Expected for Blank Inputs in EN Locale")]
        public void Forms_CatalinaDesign_VerifyInputValidationsForBlankInputs(string Locale)
        {
            //--- Catalina Design Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + catalinaDesign_form_url;

            //--- Action: Navigate to the given URL ---//
            action.Navigate(driver, form_url);

            //----- Form TestPlan > EN Tab > R15 > T5_1: Submit Catalina Design form with no data on mandatory fields -----//

            //--- Action: Click on the Submit button without entering data on the mandatory fields. ---//
            action.IClick(driver, Elements.Forms_Submit_Button);

            //----- About you -----//

            //--- Expected Result: The Full Name: "Name is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_FirstName_BlankInput_ErrorMessage_Catalina, Name_blankInput_errorMessage);
            test.validateStringIsCorrect(driver, Elements.Forms_LastName_BlankInput_ErrorMessage_Catalina, Name_blankInput_errorMessage);

            //--- Expected Result: The Job Title: "Job Title  is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_JobTitle_InputBox_BlankInput_ErrorMessage, jobTitle_blankInput_errorMessage);

            //--- Expected Result: The Application(dropdown): "Application  is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_Application_BlankInput_ErrorMessage, application_blankInput_errorMessage);

            //--- Expected Result: The Email: "Email address is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_CatalinaDesign_Email_BlankInput_ErrorMessage, email_blankInput_errorMessage);

            //----- Your detailed information -----//

            //--- Expected Result: The Company Name: "Company Name is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_CompanyName_BlankInput_ErrorMessage, companyName_blankInput_errorMessage);

            //--- Expected Result: The Address:  "Address 1 is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_CatalinaDesign_Address_BlankInput_ErrorMessage, address_blankInput_errorMessage);

            //--- Expected Result: The Telephone: "Telephone is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_CatalinaDesign_Telephone_BlankInput_ErrorMessage, telephone_blankInput_errorMessage);

            //--- Expected Result: The City: "City is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_CatalinaDesign_City_BlankInput_ErrorMessage, city_blankInput_errorMessage);

            //--- Expected Result: The Zip/Postal Code: "Zip/Postal Code is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_CatalinaDesign_ZipPostalCode_BlankInput_ErrorMessage, zipPostalCode_blankInput_errorMessage);

            //--- Expected Result: The Country(dropdown): "Country is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_CatalinaDesign_CountryRegion_BlankInput_ErrorMessage, countryRegion_blankInput_errorMessage);

            //--- Expected Result: The State(dropdown): "State is required" error message should be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_CatalinaDesign_StateProvince_BlankInput_ErrorMessage, state_blankInput_errorMessage);
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Form Submission is Working as Expected in EN Locale")]
        public void Forms_CatalinaDesign_VerifyFormSubmission(string Locale)
        {
            if (Util.Configuration.Environment.Equals("dev") || Util.Configuration.Environment.Equals("qa"))
            {
                //--- Catalina Design Form URL ---//
                string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + catalinaDesign_form_url;

                //--- Action: Navigate to the given URL ---//
                action.Navigate(driver, form_url);

                //----- Form TestPlan > EN Tab > R15 > T5_4: Submit Catalina Design form with valid data (AL-2496) -----//

                //--- Action: Enter valid data on the mandatory fields. ---//

                //----- About you -----//
                string fullName_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_FirstName_InputBox);
                action.IType(driver, Elements.Forms_FirstName_InputBox, fullName_input);

                action.IDeleteValueOnFields(driver, Elements.Forms_LastName_InputBox);
                action.IType(driver, Elements.Forms_LastName_InputBox, fullName_input);

                string jobTitle_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_JobTitle_InputBox);
                action.IType(driver, Elements.Forms_JobTitle_InputBox, jobTitle_input);

                action.IClick(driver, Elements.Forms_Application_Dropdown);
                action.IClick(driver, By.CssSelector("select[id$='ddlApplication']>option:nth-of-type(3)"));

                string email_input = "t3st_" + Util.Configuration.Environment.ToLower() + "_catalinaDesign_f0rm_" + util.GenerateRandomNumber() + "@mailinator.com";
                //Console.WriteLine("Email Input = " + email_input);
                action.IDeleteValueOnFields(driver, Elements.Forms_Email_InputBox);
                action.IType(driver, Elements.Forms_Email_InputBox, email_input);

                //----- Your detailed information -----//
                string companyName_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_CompanyName_InputBox);
                action.IType(driver, Elements.Forms_CompanyName_InputBox, companyName_input);

                string address_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_Address_InputBox);
                action.IType(driver, Elements.Forms_Address_InputBox, address_input);

                string telephone_input = "123456789";
                action.IDeleteValueOnFields(driver, Elements.Forms_Telephone_InputBox);
                action.IType(driver, Elements.Forms_Telephone_InputBox, telephone_input);

                string city_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_City_InputBox);
                action.IType(driver, Elements.Forms_City_InputBox, city_input);

                string zipPostalCode_input = "12345";
                action.IDeleteValueOnFields(driver, Elements.Forms_ZipPostalCode_InputBox);
                action.IType(driver, Elements.Forms_ZipPostalCode_InputBox, zipPostalCode_input);

                string countryRegion_input = "US";
                action.ISelectFromDropdown(driver, Elements.Forms_CountryRegion_Dropdown, countryRegion_input);

                action.IClick(driver, Elements.Forms_StateProvince_Dropdown);
                action.IClick(driver, By.CssSelector("select[id$='tate']>option:nth-of-type(3)"));

                //----- Privacy Settings -----//
                action.ICheck(driver, Elements.Forms_Communication_Checkbox);

                //--- Action: Click on submit button  ---//
                action.IClick(driver, Elements.Forms_Submit_Button);

                //--- Expected Result: AD9364 Design File Download Agreement should be displayed ---//
                test.validateStringInstance(driver, driver.Url, ad9364DesignFileDownloadAgreement_page_url);

                if (driver.Url.Contains(ad9364DesignFileDownloadAgreement_page_url))
                {
                    if (util.CheckElement(driver, Elements.AdiSimClkReferenceDesignFiles_AdiSimClkReferenceDesignFiles_Link, 1))
                    {
                        //--- Action: Click on I Decline button ---//
                        action.IClick(driver, Elements.SdrIntegratedTransceiverDesignResources_IDecline_Button);

                        //--- Expected Result: Page should be redirected to AD9361 AND AD9364 INTEGRATED RF AGILE TRANSCEIVER DESIGN RESOURCES  page ---//
                        test.validateStringInstance(driver, driver.Url, sdrIntegratedTransceiverDesignResources_page_url);
                    }
                }
            }
        }
    }
}