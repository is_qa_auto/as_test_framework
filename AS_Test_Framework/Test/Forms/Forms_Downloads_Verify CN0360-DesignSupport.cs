﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;

namespace AS_Test_Framework.Forms
{
    [TestFixture]
    public class Forms_Downloads_CN0360DesignSupport : BaseSetUp
    {
        //--- Downloads Form URL: ---//
        //--- DEV: https://form-dev.corpnt.analog.com/form_pages/securedownloads/cftlsecuredownload.aspx?returnUrl=media/en/reference-design-documentation/design-integration-files/CN0360-DesignSupport.zip ---//
        //--- QA: https://form-qa.corpnt.analog.com/form_pages/securedownloads/cftlsecuredownload.aspx?returnUrl=media/en/reference-design-documentation/design-integration-files/CN0360-DesignSupport.zip ---//
        //--- PROD: https://form.analog.com/form_pages/securedownloads/cftlsecuredownload.aspx?returnUrl=media/en/reference-design-documentation/design-integration-files/CN0360-DesignSupport.zip ---//

        public Forms_Downloads_CN0360DesignSupport() : base() { }

        //--- URLs ---//
        string downloads_form_url = "form_pages/securedownloads/cftlsecuredownload.aspx?returnUrl=media/en/reference-design-documentation/design-integration-files/CN0360-DesignSupport.zip";

        //--- Labels ---//
        string countryRegion_text = "Country/Region";
        string firstName_blankInput_errorMessage = "Length of first name must be between 1 and 25";
        string lastName_blankInput_errorMessage = "Length of last name must be between 1 and 25";
        string email_blankInput_errorMessage = "Length of email must be between 6 and 80";
        string designStage_blankInput_errorMessage = "Please select design Stage";
        string company_blankInput_errorMessage = "Length of company must be between 1 and 25";
        string countryRegion_blankInput_errorMessage = "Please select a country";
         
        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Elements are Present and Working as Expected in EN Locale")]
        public void Forms_Downloads_CN0360DesignSupport_VerifyElements(string Locale)
        {
            //--- Downloads Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + downloads_form_url;

            //--- Action: Access the cftlsecuredownload Form page ---//
            action.Navigate(driver, form_url);

            //----- Form TestPlan > EN Tab > R40 > T1: Verify all labels for Country is changed to "Country/Region" (IQ-4970/AL-12466) -----//

            //--- Expected Result: The Country label has been changed to Country/Region ---//
            test.validateStringInstance(driver, util.GetText(driver, Elements.DownloadsForms_Country_Label), countryRegion_text);

            //----- Form TestPlan > EN Tab > R36 > T1_1: Verify the consent available in downloadable forms -----//

            //----- Privacy Settings -----//

            //--- Expected Result: Privacy Settings is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_Section);

            //--- Expected Result: <checkbox> Yes, I’d like to receive communications from Analog Devices and authorized partners related to ADI’s products and services. is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_Communication_Checkbox);

            //--- Expected Result: You can change your privacy settings at any time by clicking on the unsubscribe link in emails sent from Analog Devices or in Analog’s Privacy Settings. is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_LegalDisclaimer_Text);

            //--- Expected Result: Privacy Settings is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_Link);

            //--- Expected Result: Privacy & Security Statement is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacyAndSecurityStatement_Link);

            //--- Expected Result: Authorized Partners is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_AuthorizedPartners_Link);
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Input Validations are Working as Expected for Valid Inputs in EN Locale")]
        public void Forms_Downloads_CN0360DesignSupport_VerifyInputValidationsForValidInputs(string Locale)
        {
            //--- Downloads Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + downloads_form_url;

            //--- Action: Access the cftlsecuredownload Form page ---//
            action.Navigate(driver, form_url);

            //----- Form TestPlan > EN Tab > R36 > T1: Verify the Download Form (AL-11547) -----//

            //--- Action: Click the X button ---//
            action.IClick(driver, Elements.DownloadsForms_X_Button);

            //--- Expected Result: The Download Form will be closed. ---//
            test.validateElementIsNotPresent(driver, Elements.DownloadsForm);
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Input Validations are Working as Expected for Blank Inputs in EN Locale")]
        public void Forms_Downloads_CN0360DesignSupport_VerifyInputValidationsForBlankInputs(string Locale)
        {
            //--- Downloads Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + downloads_form_url;

            //--- Action: Access the cftlsecuredownload Form page ---//
            action.Navigate(driver, form_url);

            //----- Form TestPlan > EN Tab > R36 > T1: Verify the Download Form (AL-11547) -----//

            //--- Action: Without any input, click the Download button ---//
            action.IClick(driver, Elements.DownloadsForms_Download_Button);

            //--- Expected Result: First Name: "Length of first name must be between 1 and 25." ---//
            test.validateStringInstance(driver, util.GetText(driver, Elements.DownloadsForms_FirstName_BlankInput_ErrorMessage), firstName_blankInput_errorMessage);

            //--- Expected Result: Last Name: "Length of last name must be between 1 and 25." ---//
            test.validateStringInstance(driver, util.GetText(driver, Elements.DownloadsForms_LastName_BlankInput_ErrorMessage), lastName_blankInput_errorMessage);

            //--- Expected Result: Email: "Length of email must be between 6 and 80." ---//
            test.validateStringInstance(driver, util.GetText(driver, Elements.DownloadsForms_Email_BlankInput_ErrorMessage), email_blankInput_errorMessage);

            //--- Expected Result: Design Stage: "Please select design stage" ---//
            test.validateStringInstance(driver, util.GetText(driver, Elements.DownloadsForms_DesignStage_BlankInput_ErrorMessage), designStage_blankInput_errorMessage);

            //--- Expected Result: Company:  "Length of company must be between 1 and 25" (IQ-9191/ITA-217) ---//
            test.validateStringInstance(driver, util.GetText(driver, Elements.DownloadsForms_Company_BlankInput_ErrorMessage), company_blankInput_errorMessage);

            //--- Expected Result: Country/Region: "Please select a country" ---//
            test.validateStringInstance(driver, util.GetText(driver, Elements.DownloadsForms_CountryRegion_BlankInput_ErrorMessag), countryRegion_blankInput_errorMessage);
        }
    }
}