﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using OpenQA.Selenium;
using System;
using System.Threading;

namespace AS_Test_Framework.Forms
{
    [TestFixture]
    public class Forms_CustomerServiceSupport : BaseSetUp
    {
        //--- Customer Service Support Form URL: ---//
        //--- DEV: https://form-dev.corpnt.analog.com/form_pages/support/customerservice.aspx ---//
        //--- QA: https://form-qa.corpnt.analog.com/form_pages/support/customerservice.aspx ---//
        //--- PROD: https://form.analog.com/form_pages/support/customerservice.aspx ---//

        public Forms_CustomerServiceSupport() : base() { }

        //--- Login Credentials ---//
        string username = "f0rms_t3st_555@mailinator.com";
        string password = "Test_1234";

        //--- URLs ---//
        string customerServiceSupport_form_url = "form_pages/support/customerservice.aspx";
        string customerServiceSupport_page_url = "/customer-service-support.html";

        //--- Labels ---//
        string countryRegion_text = "Country/Region";
        string email_subject = "Your Analog Devices Web Site Inquiry";
        string whatTypeOfSupportDoYouNeed_blankInput_errorMessage = "Please select at least one type of support";
        string emailId_blankInput_errorMessage = "Email address is required";
        string firstName_blankInput_errorMessage = "First Name is required";
        string lastName_blankInput_errorMessage = "Last Name is required";
        string organizationName_blankInput_errorMessage = "Organization name is required";
        string telephone_blankInput_errorMessage = "Telephone is required";
        string countryRegion_blankInput_errorMessage = "Country/Region is required";
        string pleaseWriteYourQuestionsOrCommentsHere_blankInput_errorMessage = "Question / Comment is required";
        string emailId_invalidInput_errorMessage = "Please enter a valid email address";

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Elements are Present and Working as Expected when the User is Logged Out in EN Locale")]
        public void Forms_CustomerServiceSupport_VerifyElementsWhenUserIsLoggedOut(string Locale)
        {
            //--- Customer Service Support Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + customerServiceSupport_form_url;

            //--- Action: Open the Customer Service Support form page ---//
            action.Navigate(driver, form_url);

            //----- Form TestPlan > EN Tab > R40 > T1: Verify all labels for Country is changed to "Country/Region" (IQ-4970/AL-12466) -----//

            //----- Your detailed information -----//

            //--- Expected Result: The Country label has been changed to Country/Region ---//
            test.validateStringInstance(driver, util.GetText(driver, Elements.Forms_CustomerServiceSupport_Country_Label), countryRegion_text);

            //----- Form TestPlan > EN Tab > R38 > T1: Verify Privacy Settings in All Forms [account is not logged in] (AL-11345) -----//

            //----- Privacy Settings -----//

            //--- Expected Result: Privacy Settings section is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_Section);

            //--- Expected Result: Prompt "You can change your privacy settings at any time by clicking on the unsubscribe link in emails sent from Analog Devices or in Analog’s Privacy Settings." is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_LegalDisclaimer_Text);

            //--- Expected Result: Privacy Settings is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_Link);

            //--- Expected Result: Privacy & Security Statement ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacyAndSecurityStatement_Link);

            //--- Expected Result: Authorized Partners ---//
            test.validateElementIsPresent(driver, Elements.Forms_AuthorizedPartners_Link);

            //----- Form TestPlan > EN Tab > R22 > T4: Login using Save time to login section with valid data -----//

            //----- Save time by logging in or registering for myAnalog -----//

            if (!util.CheckElement(driver, Elements.Forms_Expanded_SaveTimeByLoggingInOrRegisteringForMyAnalog_Panel, 1))
            {
                //--- Action: Click on Save time by logging in or registering for myAnalog   link ---//
                action.IClick(driver, Elements.Forms_SaveTimeByLoggingInOrRegisteringForMyAnalog_Link);
            }

            //--- Expected Result: Save time to login section should be displayed ---//
            test.validateElementIsPresent(driver, Elements.Forms_Expanded_SaveTimeByLoggingInOrRegisteringForMyAnalog_Panel);
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Elements are Present and Working as Expected when the User is Logged In in EN Locale")]
        public void Forms_CustomerServiceSupport_VerifyElementsWhenUserIsLoggedIn(string Locale)
        {
            //--- Customer Service Support Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + customerServiceSupport_form_url;

            //--- Action: Open the Customer Service Support form page ---//
            action.Navigate(driver, form_url);

            //----- Form TestPlan > EN Tab > R22 > T4: Login using Save time to login section with valid data (AL-7657) -----//

            //----- Save time by logging in or registering for myAnalog -----//

            //--- Action: Login using valid data ---//
            action.ILogInViaSaveTimeByLoggingInPanel(driver, username, password);
            Thread.Sleep(1000);

            driver.Navigate().Refresh();

            //----- About you -----//

            //--- Expected Result: The Email Id field should be  automatically populated --//
            test.validateFieldIsNotEmpty(driver, Elements.Forms_CustomerServiceSupport_EmailId_InputBox);

            //--- Expected Result: The First Name field should be  automatically populated --//
            test.validateFieldIsNotEmpty(driver, Elements.Forms_FirstName_InputBox);

            //--- Expected Result: The Last Name field should be  automatically populated --//
            test.validateFieldIsNotEmpty(driver, Elements.Forms_LastName_InputBox);

            //----- Your detailed information -----//

            //--- Expected Result: The Organization Name field should be  automatically populated --//
            test.validateFieldIsNotEmpty(driver, Elements.Forms_OrganizationName_InputBox);

            //--- Expected Result: The Country(dropdown) field should be  automatically populated --//
            test.validateStringChanged(driver, util.GetSelectedDropdownValue(driver, Elements.Forms_CountryRegion_Dropdown), "Select Country/Region");

            //----- Form TestPlan > EN Tab > R22 > T14: Verify Postal/Zipcode on all customer service support form pages (IQ-7686/AL-14970) -----//

            //--- Expected Result: Zip Code is showing in the form ---//
            test.validateFieldIsNotEmpty(driver, Elements.Forms_ZipPostalCode_InputBox);

            //----- Form TestPlan > EN Tab > R38 > T1: Verify Privacy Settings in All Forms [account is logged in] (AL-11345) -----//

            //----- Privacy Settings -----//

            //--- Expected Result: Prompt "You can change your privacy settings at any time by clicking on the unsubscribe link in emails sent from Analog Devices or in Analog’s Privacy Settings." is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_LegalDisclaimer_Text);

            //--- Expected Result: Privacy Settings is available ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacySettings_Link);

            //--- Expected Result: Privacy & Security Statement ---//
            test.validateElementIsPresent(driver, Elements.Forms_PrivacyAndSecurityStatement_Link);

            //--- Expected Result: Authorized Partners ---//
            test.validateElementIsPresent(driver, Elements.Forms_AuthorizedPartners_Link);
        }

        [TestCase("en", TestName = "Verify that the Input Validations are Working as Expected for Invalid Inputs in EN Locale")]
        public void Forms_CustomerServiceSupport_VerifyInputValidationsForInvalidInputs(string Locale)
        {
            //--- Customer Service Support Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + customerServiceSupport_form_url;

            //--- Action: Open the Customer Service Support form page ---//
            action.Navigate(driver, form_url);

            //----- Form TestPlan > EN Tab > R22 > T4_2: Submit the Customer Service Support Form with invalid data -----//

            //----- About you -----//

            //--- Action: Enter invalid data on the Email Address field. (e.g. 123!@#$%@yahoo.com) ---//
            string emailId_input = "123!@#$%@yahoo.com";
            action.IDeleteValueOnFields(driver, Elements.Forms_CustomerServiceSupport_EmailId_InputBox);
            action.IType(driver, Elements.Forms_CustomerServiceSupport_EmailId_InputBox, emailId_input);
            action.IType(driver, Elements.Forms_CustomerServiceSupport_EmailId_InputBox, Keys.Tab);

            //--- Expected Result: The error message below should be displayed: "Please enter a valid email address" ---//
            test.validateStringIsCorrect(driver, Elements.Forms_CustomerServiceSupport_EmailId_InvalidInput_ErrorMessage, emailId_invalidInput_errorMessage);

            //----- Form TestPlan > EN Tab > R22 > T4_3: Submit Customer Service Support  Form with valid data -----//

            //----- Your Question or Comment -----//

            //--- Action: In Comment Text area, input a value that is more than 3000 characters ---//
            string pleaseWriteYourQuestionsOrCommentsHere_input = "zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz";
            action.IDeleteValueOnFields(driver, Elements.Forms_Comments_TextArea);
            action.IType(driver, Elements.Forms_Comments_TextArea, pleaseWriteYourQuestionsOrCommentsHere_input);
            action.IType(driver, Elements.Forms_Comments_TextArea, Keys.Tab);

            //--- Expected Result: There must be an error message that indicates that the inputted value is more than the limit. (AL-7059) ---//
            test.validateElementIsPresent(driver, Elements.Forms_PleaseWriteYourQuestionsOrCommentsHere_InvalidInput_ErrorMessage);
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Input Validations are Working as Expected for Blank Inputs in EN Locale")]
        public void Forms_CustomerServiceSupport_VerifyInputValidationsForBlankInputs(string Locale)
        {
            //--- Customer Service Support Form URL ---//
            string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + customerServiceSupport_form_url;

            //--- Action: Open the Customer Service Support form page ---//
            action.Navigate(driver, form_url);

            //----- Form TestPlan > EN Tab > R22 > T4_1: Submit Customer Service Support  Form    with no data on mandatory fields -----//

            //----- Privacy Settings -----//

            //--- Action: Check Privacy Settings checkbox (IQ-9066/AL-15978) ---//
            action.ICheck(driver, Elements.Forms_Communication_Checkbox);

            //--- Action: Click on the Submit button without entering data on the mandatory fields. ---//
            action.IClick(driver, Elements.Forms_Submit_Button);

            //----- Type of support -----//

            //--- Expected Result: The What type of support do you need(bullet)? "Please select at least one type of support" error messages be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_CustomerServiceSupport_WhatTypeOfSupportDoYouNeed_InvalidInput_ErrorMessage, whatTypeOfSupportDoYouNeed_blankInput_errorMessage);

            //----- About you -----//

            //--- Expected Result: The Email Id : "Email address is required" error messages be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_CustomerServiceSupport_EmailId_BlankInput_ErrorMessage, emailId_blankInput_errorMessage);

            //--- Expected Result: The Frist Name: "First name is required" error messages be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_CustomerServiceSupport_FirstName_BlankInput_ErrorMessage, firstName_blankInput_errorMessage);

            //--- Expected Result: The Last Name: "Last name  is required" error messages be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_CustomerServiceSupport_LastName_BlankInput_ErrorMessage, lastName_blankInput_errorMessage);

            //----- Your detailed information -----//

            //--- Expected Result: The Organization Name: "Organization is required" error messages be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_CustomerServiceSupport_OrganizationName_BlankInput_ErrorMessage, organizationName_blankInput_errorMessage);

            //--- Expected Result: The Telephone: "Telephone is required" error messages be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_CustomerServiceSupport_Telephone_BlankInput_ErrorMessage, telephone_blankInput_errorMessage);

            //--- Expected Result: The Country(dropdown): "Country is required" error messages be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_CustomerServiceSupport_CountryRegion_BlankInput_ErrorMessage, countryRegion_blankInput_errorMessage);

            //----- Form TestPlan > EN Tab > R22 > T16: Verify zipcode as mandatory field (IQ-9067/AL-15668) -----//

            //--- Expected Result: Error message for blank zipcode must be displayed ---//
            test.validateElementIsPresent(driver, Elements.Forms_CustomerServiceSupport_ZipCode_InvalidInput_ErrorMessage);

            //----- Your Question or Comment -----//

            //--- Expected Result: The Please write your questions or comments here: "Question / Comment is required" error messages be shown ---//
            test.validateStringIsCorrect(driver, Elements.Forms_PleaseWriteYourQuestionsOrCommentsHere_BlankInput_ErrorMessage, pleaseWriteYourQuestionsOrCommentsHere_blankInput_errorMessage);
        }

        [Test, Category("Forms"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Form Submission is Working as Expected in EN Locale")]
        public void Forms_CustomerServiceSupport_VerifyFormSubmission(string Locale)
        {
            if (Util.Configuration.Environment.Equals("dev") || Util.Configuration.Environment.Equals("qa"))
            {
                //--- Customer Service Support Form URL ---//
                string form_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + customerServiceSupport_form_url;

                //--- Action: Open the Customer Service Support form page ---//
                action.Navigate(driver, form_url);

                //----- Form TestPlan > EN Tab > R22 > T4_3: Submit Customer Service Support  Form with valid data -----//

                //--- Action: Enter valid data on the mandatory fields. ---//

                //----- Type of support -----//
                action.IClick(driver, Elements.Forms_WhatTypeOfSupportDoYouNeed_RadioButtons);

                //----- About you -----//
                string emailId_input = "t3st_" + Util.Configuration.Environment.ToLower() + "_customerServiceSupport_f0rm_" + util.GenerateRandomNumber() + "@mailinator.com";
                //Console.WriteLine("Email Id Input = " + emailId_input);
                action.IDeleteValueOnFields(driver, Elements.Forms_CustomerServiceSupport_EmailId_InputBox);
                action.IType(driver, Elements.Forms_CustomerServiceSupport_EmailId_InputBox, emailId_input);

                string firstName_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_FirstName_InputBox);
                action.IType(driver, Elements.Forms_FirstName_InputBox, firstName_input);

                string lastName_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_LastName_InputBox);
                action.IType(driver, Elements.Forms_LastName_InputBox, lastName_input);

                //----- Your detailed information -----//
                string organizationName_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_OrganizationName_InputBox);
                action.IType(driver, Elements.Forms_OrganizationName_InputBox, organizationName_input);

                string telephone_input = "123456789";
                action.IDeleteValueOnFields(driver, Elements.Forms_Telephone_InputBox);
                action.IType(driver, Elements.Forms_Telephone_InputBox, telephone_input);

                string countryRegion_input = "US";
                action.ISelectFromDropdown(driver, Elements.Forms_CountryRegion_Dropdown, countryRegion_input);

                string zipCode_input = "12345";
                action.IDeleteValueOnFields(driver, Elements.Forms_ZipPostalCode_InputBox);
                action.IType(driver, Elements.Forms_ZipPostalCode_InputBox, zipCode_input);

                //----- Your Question or Comment -----//
                string pleaseWriteYourQuestionsOrCommentsHere_input = "This is Just A Test";
                action.IDeleteValueOnFields(driver, Elements.Forms_Comments_TextArea);
                action.IType(driver, Elements.Forms_Comments_TextArea, pleaseWriteYourQuestionsOrCommentsHere_input);

                //----- Privacy Settings -----//
                action.ICheck(driver, Elements.Forms_Communication_Checkbox);

                //--- Action: Click on submit button ---//
                action.IClick(driver, Elements.Forms_Submit_Button);

                //--- Expected Result: Customer Service Support Thank You page should be displayed ---//
                test.validateStringInstance(driver, driver.Url, customerServiceSupport_page_url);

                if (driver.Url.Contains(customerServiceSupport_page_url))
                {
                    action.ICheckMailinatorEmail(driver, emailId_input);

                    //--- Expected Result: The email from the support user depending on what country should be displayed ---//
                    test.validateStringIsCorrect(driver, Elements.Mailinator_LatestEmail_Sender, "buyonline.customerservice@analog.");

                    //----- Form TestPlan > EN Tab > R22 > T13: Verify the Subject in email received. -----//

                    //--- Expected Result: Subject should be displayed- Your Analog Devices Web Site Inquiry ---//
                    test.validateStringInstance(driver, util.GetText(driver, Elements.Mailinator_LatestEmail_Subj), email_subject);
                }
            }
        }
    }
}