﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System;
using System.Threading;
using OpenQA.Selenium;
using System.Linq;

namespace AS_Test_Framework.ToolsAnomalie
{

    [TestFixture]
    public class ToolsAnomalie_Navigation : BaseSetUp
    {
       
        public ToolsAnomalie_Navigation() : base() { }

        [Test, Category("Tools Anomalie"), Category("Core")]
        [TestCase("en", TestName = "Verify user can navigate to Tools anomalie via design center page for EN Locale")]
        [TestCase("cn", TestName = "Verify user can navigate to Tools anomalie via design center page for CN Locale")]
        [TestCase("jp", TestName = "Verify user can navigate to Tools anomalie via design center page for JP Locale")]
        [TestCase("ru", TestName = "Verify user can navigate to Tools anomalie via design center page for RU Locale")]
        public void ToolsAnomalie_VerifyNavigation(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/index.html");
            action.IClick(driver, Elements.MainNavigation_DesignCenterTab);
            action.IClick(driver, By.CssSelector("div[class='designcenter menu content expanded']>section[class='design-center-section']>div[class='row featured-section-sub']>div>div:nth-child(1) h3>a"));
            action.IClick(driver, By.CssSelector("div[name='adi_twocolumn_content_section']>div:nth-child(4) p>a[class='text-link']"));
            action.IClick(driver, By.CssSelector("div[name='adi_twocolumn_content_section']>div:nth-child(8) p>a[class='text-link']"));
            test.validateScreenByUrl(driver,Configuration.Env_Url + Locale + "/design-center/evaluation-hardware-and-software/software/software-tools-anomalies-search.html");
        }
    }
}
