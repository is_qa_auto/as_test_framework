﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System;
using System.Threading;
using OpenQA.Selenium;
using System.Linq;

namespace AS_Test_Framework.ToolsAnomalie
{

    [TestFixture]
    public class ToolsAnomalie_SearchPage : BaseSetUp
    {
        public ToolsAnomalie_SearchPage() : base() { }

        [Test, Category("Tools Anomalie"), Category("Core")]
        [TestCase("en", TestName = "Verify the tools anomalie search component is present and working as expected in tools anomalie page for EN Locale")]
        [TestCase("cn", TestName = "Verify the tools anomalie search component is present and working as expected in tools anomalie page for CN Locale")]
        [TestCase("jp", TestName = "Verify the tools anomalie search component is present and working as expected in tools anomalie page for JP Locale")]
        [TestCase("ru", TestName = "Verify the tools anomalie search component is present and working as expected in tools anomalie page for RU Locale")]
        public void ToolsAnomalie_VerifySearchPageComponent(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/design-center/evaluation-hardware-and-software/software/software-tools-anomalies-search.html");
            test.validateElementIsPresent(driver, Elements.ToolsAnomalie_Families_Dropdown);
            test.validateElementIsPresent(driver, Elements.ToolsAnomalie_Products_Dropdown);
            test.validateElementIsPresent(driver, Elements.ToolsAnomalie_Components_Dropdown);
            test.validateElementIsPresent(driver, Elements.ToolsAnomalie_ReferenceNumbers_TextField);
            test.validateElementIsPresent(driver, Elements.ToolsAnomalie_Keyword_TextField);
            test.validateElementIsPresent(driver, Elements.ToolsAnomalie_Search_Btn);
        }

        [Test, Category("Tools Anomalie"), Category("Core")]
        [TestCase("en", TestName = "Verify search using invalid keyword")]
        public void ToolsAnomalie_VerifySearchWithInvalidKeyword(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/design-center/evaluation-hardware-and-software/software/software-tools-anomalies-search.html");
            string invalid_keyword = "adsada";
            action.IType(driver, Elements.ToolsAnomalie_Keyword_TextField, invalid_keyword);
            action.IClick(driver, Elements.ToolsAnomalie_Search_Btn);
            test.validateStringIsCorrect(driver, Elements.ToolsAnomalie_SERP_SearchResult_Label, "Search Results (0):");
        }

        [Test, Category("Tools Anomalie"), Category("Core")]
        [TestCase("en", TestName = "Verify search using single valid keyword")]
        public void ToolsAnomalie_VerifySearchWithSingleKeyword(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/design-center/evaluation-hardware-and-software/software/software-tools-anomalies-search.html");
            string valid_keyword = "audio";
            action.IType(driver, Elements.ToolsAnomalie_Keyword_TextField, valid_keyword);
            action.IClick(driver, Elements.ToolsAnomalie_Search_Btn);
            test.validateElementIsPresent(driver, Elements.ToolsAnomalie_SERP_SearchList);
        }

        [Test, Category("Tools Anomalie"), Category("Core")]
        [TestCase("en", TestName = "Verify search using multiple valid keyword")]
        public void ToolsAnomalie_VerifySearchWithMultipleKeyword(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/design-center/evaluation-hardware-and-software/software/software-tools-anomalies-search.html");
            string valid_keyword = "audio, processor";
            action.IType(driver, Elements.ToolsAnomalie_Keyword_TextField, valid_keyword);
            action.IClick(driver, Elements.ToolsAnomalie_Search_Btn);
            test.validateElementIsPresent(driver, Elements.ToolsAnomalie_SERP_SearchList);
        }

        [Test, Category("Tools Anomalie"), Category("Core")]
        [TestCase("en", TestName = "Verify search using invalid reference")]
        public void ToolsAnomalie_VerifySearchWithInvalidReference(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/design-center/evaluation-hardware-and-software/software/software-tools-anomalies-search.html");
            string invalid_keyword = "!@!@";
            action.IType(driver, Elements.ToolsAnomalie_ReferenceNumbers_TextField, invalid_keyword);
            action.IClick(driver, Elements.ToolsAnomalie_Search_Btn);
            test.validateStringIsCorrect(driver, Elements.ToolsAnomalie_SERP_SearchResult_Label, "Search Results (0):");
        }

        [Test, Category("Tools Anomalie"), Category("Core")]
        [TestCase("en", TestName = "Verify search using single valid reference")]
        public void ToolsAnomalie_VerifySearchWithSingleReference(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/design-center/evaluation-hardware-and-software/software/software-tools-anomalies-search.html");
            string valid_keyword = "VDSP-24";
            action.IType(driver, Elements.ToolsAnomalie_ReferenceNumbers_TextField, valid_keyword);
            action.IClick(driver, Elements.ToolsAnomalie_Search_Btn);
            test.validateElementIsPresent(driver, Elements.ToolsAnomalie_SERP_SearchList);
        }

        [Test, Category("Tools Anomalie"), Category("Core")]
        [TestCase("en", TestName = "Verify search using multiple valid reference")]
        public void ToolsAnomalie_VerifySearchWithMultipleReference(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/design-center/evaluation-hardware-and-software/software/software-tools-anomalies-search.html");
            string valid_keyword = "VDSP-2418, VDSP-2430";
            action.IType(driver, Elements.ToolsAnomalie_ReferenceNumbers_TextField, valid_keyword);
            action.IClick(driver, Elements.ToolsAnomalie_Search_Btn);
            test.validateElementIsPresent(driver, Elements.ToolsAnomalie_SERP_SearchList);
        }
    }
}
