﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System;
using System.Threading;
using OpenQA.Selenium;
using System.Linq;

namespace AS_Test_Framework.ToolsAnomalie
{

    [TestFixture]
    public class ToolsAnomalie_PrintAndMyAnalogWidget : BaseSetUp
    {
        public ToolsAnomalie_PrintAndMyAnalogWidget() : base() { }

        [Test, Category("Tools Anomalie"), Category("Core")]
        [TestCase("en", "Software and Tools Anomalies Search", TestName = "Verify that page title, print and myAnalog widget is present in tools anomalie page for EN Locale")]
        [TestCase("cn", "软件和工具异常情况搜索", TestName = "Verify that page title, print and myAnalog widget is present in tools anomalie page for CN Locale")]
        [TestCase("jp", "ソフトウェアおよびツール異常の検索", TestName = "Verify that page title, print and myAnalog widget is present in tools anomalie page for JP Locale")]
        [TestCase("ru", "Поиск по аномалиям программного обеспечения и инструментам разработки", TestName = "Verify that page title, print and myAnalog widget is present in tools anomalie page for RU Locale")]
        public void ToolsAnomalie_VerifyBreadcrumbs(string Locale, string CurrentPage)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/design-center/evaluation-hardware-and-software/software/software-tools-anomalies-search.html");
            test.validateStringIsCorrect(driver, Elements.ToolsAnomalie_PageTitle, CurrentPage);
            test.validateElementIsPresent(driver, Elements.Print_Widget_Btn);
            test.validateElementIsPresent(driver, Elements.MyAnalog_Widget_Btn);
        }
    }
}
