﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System;
using System.Threading;
using OpenQA.Selenium;
using System.Linq;

namespace AS_Test_Framework.ToolsAnomalie
{

    [TestFixture]
    public class ToolsAnomalie_Breadcrumbs : BaseSetUp
    {
        public ToolsAnomalie_Breadcrumbs() : base() { }

        [Test, Category("Tools Anomalie"), Category("Core")]
        [TestCase("en", "Design Center", "Evaluation Hardware & Software", "Software", "Tools Anomalies", TestName = "Verify that breadcrumbs is present and working as expected in tools anomalie page for EN Locale")]
        [TestCase("cn", "设计资源", "评估硬件与软件", "软件", "软件和工具异常情况搜索", TestName = "Verify that breadcrumbs is present and working as expected in tools anomalie page for CN Locale")]
        [TestCase("jp", "設計支援", "評価用 ハードウェア & ソフトウェア", "ソフトウェア", "ツール異常の検索", TestName = "Verify that breadcrumbs is present and working as expected in tools anomalie page for JP Locale")]
        [TestCase("ru", "Ресурсы", "Оценочные аппаратные средства и ПО", "Программное обеспечение", "Поиск по аномалиям программного обеспечения и инструментам разработки", TestName = "Verify that breadcrumbs is present and working as expected in tools anomalie page for RU Locale")]
        public void ToolsAnomalie_VerifyBreadcrumbs(string Locale, string FirstLevelCat, string SecondLevelCat, string ThirdLevelCat, string CurrentPage)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/design-center/evaluation-hardware-and-software/software/software-tools-anomalies-search.html");
            test.validateElementIsPresent(driver, Elements.Applications_BreadCrumbs);
            test.validateStringIsCorrect(driver, By.CssSelector("div[name='adi_breadcrumb'] ol>li:nth-child(2)>span>a"), FirstLevelCat);
            test.validateStringIsCorrect(driver, By.CssSelector("div[name='adi_breadcrumb'] ol>li:nth-child(3)>span>a"), SecondLevelCat);
            test.validateStringIsCorrect(driver, By.CssSelector("div[name='adi_breadcrumb'] ol>li:nth-child(4)>span>a"), ThirdLevelCat);
            test.validateStringIsCorrect(driver, By.CssSelector("div[name='adi_breadcrumb'] li[class='active']>span"), CurrentPage);
            action.IClick(driver, By.CssSelector("div[name='adi_breadcrumb'] ol>li:nth-child(2)>span>a"));
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/design-center.html");
            driver.Navigate().Back();
            action.IClick(driver, By.CssSelector("div[name='adi_breadcrumb'] ol>li:nth-child(3)>span>a"));
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/design-center/evaluation-hardware-and-software.html");
            driver.Navigate().Back();
            action.IClick(driver, By.CssSelector("div[name='adi_breadcrumb'] ol>li:nth-child(4)>span>a"));
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/design-center/evaluation-hardware-and-software/software.html");
            driver.Navigate().Back();
            test.validateElementIsPresent(driver, Elements.Applications_BreadCrumbs_Home);
            action.IClick(driver, Elements.Applications_BreadCrumbs_Home);
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/index.html");
        }
    }
}
