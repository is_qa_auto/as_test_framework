﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System;
using System.Threading;
using OpenQA.Selenium;
using System.Linq;

namespace AS_Test_Framework.ToolsAnomalie
{

    [TestFixture]
    public class ToolsAnomalie_SearchResultPage : BaseSetUp
    {
        public ToolsAnomalie_SearchResultPage() : base() { }


        [Test, Category("Tools Anomalie"), Category("Core")]
        [TestCase("en", TestName = "Verify search result component is present")]
        public void ToolsAnomalie_VerifySearchResultComponent(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/design-center/evaluation-hardware-and-software/software/software-tools-anomalies-search.html");
            action.IClick(driver, Elements.ToolsAnomalie_Search_Btn);
            test.validateElementIsPresent(driver, Elements.ToolsAnomalie_SERP_Criteria);
            test.validateElementIsPresent(driver, Elements.ToolsAnomalie_SERP_Back_Btn);
            test.validateElementIsPresent(driver, Elements.ToolsAnomalie_SERP_NewSearch_Btn);
        }

        [Test, Category("Tools Anomalie"), Category("Core")]
        [TestCase("en", TestName = "Verify selected keyword/criteria is present")]
        public void ToolsAnomalie_VerifySearchKeywordsArePresent(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/design-center/evaluation-hardware-and-software/software/software-tools-anomalies-search.html");
            action.IClick(driver, Elements.ToolsAnomalie_Search_Btn);
            test.validateStringInstance(driver, util.GetText(driver, By.CssSelector("div[class='panel panel-primary ta-search-criteria'] div[class='clearfix']")), "Family(s): All families");
            test.validateStringInstance(driver, util.GetText(driver, By.CssSelector("div[class='panel panel-primary ta-search-criteria'] div[class='clearfix']")), "Product(s): All products");
            test.validateStringInstance(driver, util.GetText(driver, By.CssSelector("div[class='panel panel-primary ta-search-criteria'] div[class='clearfix']")), "Component(s): All components");
            test.validateStringInstance(driver, util.GetText(driver, By.CssSelector("div[class='panel panel-primary ta-search-criteria'] div[class='clearfix']")), "Keyword(s): All");
            test.validateStringInstance(driver, util.GetText(driver, By.CssSelector("div[class='panel panel-primary ta-search-criteria'] div[class='clearfix']")), "Component(s): All");
        }

        [Test, Category("Tools Anomalie"), Category("Core")]
        [TestCase("en", TestName = "Verify back button is working")]
        public void ToolsAnomalie_VerifyBackButton(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/design-center/evaluation-hardware-and-software/software/software-tools-anomalies-search.html");
            string valid_keyword = "audio";
            string valid_keyword2 = "CCES-2570";
            action.IType(driver, Elements.ToolsAnomalie_Keyword_TextField, valid_keyword);
            action.IType(driver, Elements.ToolsAnomalie_ReferenceNumbers_TextField, valid_keyword2);
            action.IClick(driver, Elements.ToolsAnomalie_Search_Btn);
            action.IClick(driver, Elements.ToolsAnomalie_SERP_Back_Btn);
            test.validateString(driver, util.ReturnAttribute(driver, Elements.ToolsAnomalie_Keyword_TextField, "value"), valid_keyword);
            test.validateString(driver, util.ReturnAttribute(driver, Elements.ToolsAnomalie_ReferenceNumbers_TextField, "value"), valid_keyword2);

        }

        [Test, Category("Tools Anomalie"), Category("Core")]
        [TestCase("en", TestName = "Verify new search button is working")]
        public void ToolsAnomalie_VerifyNewSearchButton(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/design-center/evaluation-hardware-and-software/software/software-tools-anomalies-search.html");
            string valid_keyword = "audio";
            string valid_keyword2 = "CCES-2570";
            action.IType(driver, Elements.ToolsAnomalie_Keyword_TextField, valid_keyword);
            action.IType(driver, Elements.ToolsAnomalie_ReferenceNumbers_TextField, valid_keyword2);
            action.IClick(driver, Elements.ToolsAnomalie_Search_Btn);
            action.IClick(driver, Elements.ToolsAnomalie_SERP_NewSearch_Btn);
            test.validateString(driver, util.ReturnAttribute(driver, Elements.ToolsAnomalie_Keyword_TextField, "value"), "");
            test.validateString(driver, util.ReturnAttribute(driver, Elements.ToolsAnomalie_ReferenceNumbers_TextField, "value"), "");
        }

        [Test, Category("Tools Anomalie"), Category("Core")]
        [TestCase("en", TestName = "Verify search list is present and working as expected")]
        public void ToolsAnomalie_VerifySearchList(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/design-center/evaluation-hardware-and-software/software/software-tools-anomalies-search.html");
            action.IClick(driver, Elements.ToolsAnomalie_Search_Btn);
            string count = util.GetCount(driver, By.CssSelector("div[class='ta-search-results'] a")).ToString();
            test.validateString(driver, "Search Results (" + count + "):", util.GetText(driver, Elements.ToolsAnomalie_SERP_SearchResult_Label));
            string ID = util.ReturnAttribute(driver, By.CssSelector("div[class='ta-search-results'] a"), "id");
            action.IClick(driver, By.CssSelector("div[class='ta-search-results'] a"));
            test.validateElementIsPresent(driver, By.CssSelector("div[id='" + ID.Replace("Toggle", "div") + "']>ul"));
            action.IClick(driver, By.CssSelector("div[class='ta-search-results'] a"));
            test.validateElementIsNotPresent(driver, By.CssSelector("div[id='" + ID.Replace("Toggle", "div") + "']>ul"));
        }
    }
}
