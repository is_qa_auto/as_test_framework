﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Threading;

namespace AS_Test_Framework.DSP
{
    [TestFixture]
    public class DSP_Overview_CompatiblePartsSection : BaseSetUp
    {
        public DSP_Overview_CompatiblePartsSection() : base() { }

        //--- URLs ---//
        string dsp_url = "/adswt-cces";
        string pdp_url = "/products/";
        string dsp_overview_url = "#software-overview";

        [Test, Category("DSP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Compatible Parts Section is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Compatible Parts Section is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Compatible Parts Section is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Compatible Parts Section is Present and Working as Expected in RU Locale")]
        public void DSP_Overview_VerifyCompatiblePartsSection(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + dsp_url);

            if (util.CheckElement(driver, Elements.DSP_Overview_CompatibleParts_Sec, 2))
            {
                //----- R3 > T1: Verify Compatible Parts section (AL-10843) -----//

                //--- Expected Result: List of product are displayed ---//
                test.validateElementIsPresent(driver, Elements.DSP_Overview_CompatibleParts_Product_Links);

                if (util.CheckElement(driver, Elements.DSP_Overview_CompatibleParts_Product_Links, 2))
                {
                    //--- Expected Result: Default compatible parts is 10. ---//
                    test.validateCountIsEqual(driver, 10, util.GetCount(driver, Elements.DSP_Overview_CompatibleParts_Product_Links));

                    if (util.CheckElement(driver, Elements.DSP_Overview_CompatibleParts_ViewAllLess_Link, 2))
                    {
                        //--- Action: Click View All link ---//
                        action.IClick(driver, Elements.DSP_Overview_CompatibleParts_ViewAllLess_Link);

                        //--- Expected Result: The remaining compatible parts should expand. ---//
                        int expanded_compatibleParts_product_count = util.GetCount(driver, Elements.DSP_Overview_CompatibleParts_Product_Links);
                        test.validateCountIsGreaterOrEqual(driver, expanded_compatibleParts_product_count, 11);

                        //--- Action: Click View less link ---//
                        action.IClick(driver, Elements.DSP_Overview_CompatibleParts_ViewAllLess_Link);

                        if (expanded_compatibleParts_product_count > 30)
                        {
                            //--- Expected Result: If the compatible parts displayed is more that 30, clicking view less link should anchor back to overview section ---//
                            test.validateStringInstance(driver, driver.Url, dsp_overview_url);
                        }

                        //--- Expected Result: The content should collapsed back to 10 compatible parts ---//
                        test.validateCountIsEqual(driver, 10, util.GetCount(driver, Elements.DSP_Overview_CompatibleParts_Product_Links));
                    }

                    action.IClick(driver, Elements.DSP_StickyHeader_Overview);

                    //--- Action: Click each product link ---//
                    action.IOpenLinkInNewTab(driver, Elements.DSP_Overview_CompatibleParts_Product_Links);
                    Thread.Sleep(2000);

                    //--- Expected Result: It will redirect to its respective Product Detailed Page ---//
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + pdp_url);
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.DSP_Overview_CompatibleParts_Sec);
            }
        }
    }
}