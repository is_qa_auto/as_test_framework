﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;

namespace AS_Test_Framework.DSP
{
    [TestFixture]
    public class DSP_Overview_SoftwareImages : BaseSetUp
    {
        public DSP_Overview_SoftwareImages() : base() { }

        //--- URLs ---//
        string dsp_url = "/adswt-cces";

        [Test, Category("DSP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Software Images are Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Software Images are Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Software Images are Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Software Images are Present and Working as Expected in RU Locale")]
        public void DSP_Overview_VerifySoftwareImages(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + dsp_url);

            //----- R1 > T1: Verify the display of image on overview pane -----//

            //--- Expected Result: Big image is seen at the center ---//
            test.validateElementIsPresent(driver, Elements.DSP_Overview_SoftwareImage);

            if (util.CheckElement(driver, Elements.DSP_Overview_SoftwareImage, 2))
            {
                //--- Action: Click image ---//
                action.IClick(driver, Elements.DSP_Overview_SoftwareImage);

                //--- Expected Result: Pop-up window will appear ---//
                test.validateElementIsPresent(driver, Elements.DSP_Overview_Software_PopUp);

                if (util.CheckElement(driver, Elements.DSP_Overview_Software_PopUp, 2))
                {
                    //--- Expected Result: "The following are available: Image tittle, Option to download image, Option to print image and Image ---//
                    test.validateElementIsPresent(driver, Elements.DSP_Overview_Software_PopUp_Title);
                    test.validateElementIsPresent(driver, Elements.DSP_Overview_Software_PopUp_Download_Icon);
                    test.validateElementIsPresent(driver, Elements.DSP_Overview_Software_PopUp_Print_Icon);
                    test.validateElementIsPresent(driver, Elements.DSP_Overview_Software_PopUp_Img);
                }
            }
        }
    }
}