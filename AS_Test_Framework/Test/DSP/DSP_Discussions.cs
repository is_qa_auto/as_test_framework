﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;

namespace AS_Test_Framework.DSP
{
    [TestFixture]
    public class DSP_Discussions : BaseSetUp
    {
        public DSP_Discussions() : base() { }

        //--- URLs ---//
        string dsp_url = "/adswt-cces";

        [Test, Category("DSP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Discussions Section is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Discussions Section is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Discussions Section is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Discussions Section is Present and Working as Expected in RU Locale")]
        public void DSP_Discussions_VerifyDiscussionsSection(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + dsp_url);

            //----- R10 > T1: Verify Discussion section -----//

            //--- Expected Result: Discussions section label is Present ---//
            test.validateElementIsPresent(driver, Elements.DSP_Discussions_Sec_Lbl);
        }
    }
}