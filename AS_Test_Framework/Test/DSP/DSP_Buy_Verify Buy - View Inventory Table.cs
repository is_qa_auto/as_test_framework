﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Linq;
using System.Threading;
using System;

namespace AS_Test_Framework.DSP
{
    [TestFixture]
    public class DSP_Buy_BuyViewInventoryTable : BaseSetUp
    {
        public DSP_Buy_BuyViewInventoryTable() : base() { }

        //--- URLs ---//
        string dsp_url = "/adswt-cces";
        string shoppingCart_page_url = "shoppingcart";

        [Test, Category("DSP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Buy - View Inventory Table is Present and Working as Expected in EN Locale", Category = "Smoke_ADIWeb")]
        [TestCase("cn", TestName = "Verify that the Buy - View Inventory Table is Present and Working as Expected in CN Locale", Category = "Smoke_ADIWeb")]
        [TestCase("jp", TestName = "Verify that the Buy - View Inventory Table is Present and Working as Expected in JP Locale", Category = "Smoke_ADIWeb")]
        [TestCase("ru", TestName = "Verify that the Buy - View Inventory Table is Present and Working as Expected in RU Locale")]
        public void DSP_Buy_VerifyBuyViewInventoryTable(string Locale)
        {
            string initial_steps = "1. Go to " + Configuration.Env_Url + Locale + dsp_url;
            string scenario = "";

            action.INavigate(driver, Configuration.Env_Url + Locale + dsp_url);

            if (util.CheckElement(driver, Elements.DSP_Buy_SelectACountry_Dd, 2))
            {
                //----- R11 > T4: Verify "Check Inventory" button underBuy section table. -----//

                initial_steps = initial_steps + "<br>2. Select US as a country in Buy table section";
                action.IClick(driver, Elements.DSP_Buy_SelectACountry_Dd_Btn);
                action.IClick(driver, By.CssSelector("section[id$='buy'] * div>div:nth-of-type(1) * div[class$='country-filter'] * a[data-id='US']"));

                if (util.CheckElement(driver, Elements.DSP_Buy_CheckInventory_Btn, 2))
                {
                    //--- Action: Click Check Inventory button ---//
                    initial_steps = initial_steps + "<br>3. Click check inventory button";
                    action.IClick(driver, Elements.DSP_Buy_CheckInventory_Btn);

                    //--- Expected Result: New table column should slide-in ---//
                    scenario = "Verify that inventory table will slide-in after clicking check inventory button";
                    test.validateElementIsPresentv2(driver, Elements.DSP_Buy_ViewInventory_Tbl, scenario, initial_steps);

                    //----- R11 > T5: Verify Add to Cart button should be disabled when no item is selected on Buy Section -----//

                    //--- Expected Result: Add to Cart button should remain disabled. ---//
                    scenario = "Verify that by default, disabled add to cart button is present";
                    test.validateElementIsPresentv2(driver, Elements.DSP_Buy_Disabled_AddToCart_Btn, scenario, initial_steps);

                    if (util.CheckElement(driver, Elements.DSP_Buy_ViewInventory_Tbl_Select_Cb, 2))
                    {
                        int buy_viewInventory_tbl_row_count = util.GetCount(driver, By.CssSelector("section[id$='buy'] * div[class$='grid-clone view-inventory']>table>tbody>tr"));

                        for (int locator_ctr = 1; locator_ctr <= buy_viewInventory_tbl_row_count; locator_ctr++)
                        {
                            if (util.CheckElement(driver, By.CssSelector("section[id$='buy'] * div[class$='grid-clone view-inventory']>table>tbody>tr:nth-of-type(" + locator_ctr + ") * input[class^='check']"), 2))
                            {
                                string model_name = util.ReturnAttribute(driver, By.CssSelector("section[id$='buy'] * div[class$='grid-clone view-inventory']>table>tbody>tr:nth-of-type(" + locator_ctr + ")"), "data-id");

                                //----- R11 > T8: Verify Add to Cart button function (not logged-in) -----//

                                //--- Action: Select items on Check Inventory/Purchase ---//
                                initial_steps = initial_steps + "<br>4. Tick select check box of product - " + model_name;
                                action.IClick(driver, By.CssSelector("section[id$='buy'] * div[class$='grid-clone view-inventory']>table>tbody>tr:nth-of-type(" + locator_ctr + ") * input[class^='check']"));

                                //--- Expected Result: Add to Cart button should become enabled. ---//
                                scenario = "Verify that Add to cart will be enabled";
                                test.validateElementIsEnabledv2(driver, Elements.DSP_Buy_AddToCart_Btn, scenario, initial_steps);

                                if (driver.FindElement(Elements.DSP_Buy_AddToCart_Btn).Enabled)
                                {
                                   string current_url = driver.Url;

                                    //--- Action: Click Add to Cart button ---//
                                    initial_steps = initial_steps + "<br>5. Click add to cart button";
                                    action.IClick(driver, Elements.DSP_Buy_AddToCart_Btn);
                                    Thread.Sleep(10000);

                                    //if (driver.Url.Contains(current_url))
                                    //{
                                    //    driver.SwitchTo().Window(driver.WindowHandles.Last());
                                    //}

                                    ////--- Expected Result: Add to Cart button should redirect to Shopping Cart. ---//
                                    //test.validateStringInstance(driver, driver.Url, shoppingCart_page_url);

                                    //if (driver.Url.Contains(shoppingCart_page_url))
                                    //{
                                        if (util.CheckElement(driver, Elements.Shopping_Cart_SelectShippingCountry_ErrorMsg, 2))
                                        {
                                            initial_steps = initial_steps + "<br>6. If in shopping cart, country selected is empty/not allowed for distribution select US as a country";
                                            action.IClick(driver, Elements.Shopping_Cart_Country_Dropdown);
                                            action.IClick(driver, By.CssSelector("li[value='US']>a"));
                                        }

                                    //--- Expected Result: The selected product that you've added should be shown on shopping cart ---//
                                    scenario = "Verify that selected product will be displayed in shopping cart - cart table";
                                    test.validateStringIsCorrectv2(driver, Elements.Shopping_Cart_AddedModel, model_name, scenario, initial_steps);
                                    //}
                                }

                                break;
                            }
                        }
                    }
                }
                else
                {
                    scenario = "Verify that enabled check inventory button is displayed in buy table";
                    test.validateElementIsPresentv2(driver, Elements.DSP_Buy_CheckInventory_Btn,scenario, initial_steps);
                }
            }
            else
            {
                scenario = "Verify that country dropdown is present in buy table";
                test.validateElementIsPresentv2(driver, Elements.DSP_Buy_SelectACountry_Dd, scenario, initial_steps);
            }
        }       
    }
}