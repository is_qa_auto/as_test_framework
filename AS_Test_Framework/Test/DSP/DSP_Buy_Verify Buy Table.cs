﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Linq;

namespace AS_Test_Framework.DSP
{
    [TestFixture]
    public class DSP_Buy_BuyTable : BaseSetUp
    {
        public DSP_Buy_BuyTable() : base() { }

        //--- Login Credentials ---//
        string username = "myanalog_t3st_111@mailinator.com";
        string password = "Test_1234";

        //--- URLs ---//
        string dsp_url = "/adswt-cces";

        //--- Labels ---//
        string[] buy_col_headers = { "Model", "Description", "Price", "RoHS" };
        string country_selected = "United States";

        [Test, Category("DSP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Buy Table is Present and Working as Expected when the User is Logged Out in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Buy Table is Present and Working as Expected when the User is Logged Out in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Buy Table is Present and Working as Expected when the User is Logged Out in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Buy Table is Present and Working as Expected when the User is Logged Out in RU Locale")]
        public void DSP_Buy_VerifyBuyTableWhenUserIsLoggedOut(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + dsp_url);

            if (util.CheckElement(driver, Elements.DSP_Buy_Tbl, 2))
            {
                for (int locator_ctr = 1, label_ctr = 0; locator_ctr <= buy_col_headers.Length; locator_ctr++, label_ctr++)
                {
                    //----- R11 > T2: Verify Buy Online Table on Buy Section  -----//

                    //--- Expected Result: The Buy online table should have the following content : Model | Description | Price | RoHS | ---//
                    if (Locale.Equals("en"))
                    {
                        test.validateStringIsCorrect(driver, By.CssSelector("section[id$='buy'] * div[class$='evaluation table-responsive sample-table']>table>thead>tr>th:nth-of-type(" + locator_ctr + ")"), buy_col_headers[label_ctr]);
                    }
                    else
                    {
                        test.validateElementIsPresent(driver, By.CssSelector("section[id$='buy'] * div[class$='evaluation table-responsive sample-table']>table>thead>tr>th:nth-of-type(" + locator_ctr + ")"));
                    }

                    //--- Expected Result: Model column contains Model number --//
                    test.validateElementIsPresent(driver, Elements.DSP_Buy_Tbl_Model_Col_Val);

                    //--- Expected Result: Description of every model or DSP-Software product is available ---//
                    test.validateElementIsPresent(driver, Elements.DSP_Buy_Tbl_Description_Col_Val);

                    //--- Expected Result: Price value is in Dollar ($) denomination ---//
                    test.validateStringInstance(driver, util.GetText(driver, Elements.DSP_Buy_Tbl_Price_Col_Val), "$");

                    //--- Expected Result: Yes/No is available ---//
                    if (Locale.Equals("en"))
                    {
                        if (util.GetText(driver, Elements.DSP_Buy_Tbl_Rohs_Col_Val).Equals("Yes"))
                        {
                            test.validateStringIsCorrect(driver, Elements.DSP_Buy_Tbl_Rohs_Col_Val, "Yes");
                        }
                        else
                        {
                            test.validateStringIsCorrect(driver, Elements.DSP_Buy_Tbl_Rohs_Col_Val, "No");
                        }
                    }
                    else
                    {
                        test.validateElementIsPresent(driver, Elements.DSP_Buy_Tbl_Rohs_Col_Val);
                    }
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.DSP_Buy_Tbl);
            }

            //----- R11 > T3: Verify Back, Select a Country and Check Inventory button behavior on Buy Section  -----//

            //--- Expected Result: The Back button should be disabled on page load. ---//
            //test.validateElementIsNotEnabled(driver, Elements.DSP_Buy_Back_Btn);
            test.validateElementIsPresent(driver, Elements.DSP_Buy_Disabled_Back_Btn);

            //--- Expected Result: The  Check Inventory button should be disabled on page load. ---//
            //test.validateElementIsNotEnabled(driver, Elements.DSP_Buy_CheckInventory_Btn);
            test.validateElementIsPresent(driver, Elements.DSP_Buy_Disabled_CheckInventory_Btn);

            //--- Expected Result: 'Select a Country' should be a default selection in Select a Country dropdown on page load. ---//
            if (Locale.Equals("en"))
            {
                test.validateStringIsCorrect(driver, Elements.DSP_Buy_SelectACountry_Dd_Selected_Val, "Select a country");
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.DSP_Buy_SelectACountry_Dd);
            }

            if (util.CheckElement(driver, Elements.DSP_Buy_SelectACountry_Dd, 2))
            {
                //--- Action: Click Select a Country Dropdown ---//
                action.IClick(driver, Elements.DSP_Buy_SelectACountry_Dd_Btn);

                //--- Expected Result: Should be able to see the lists of all countries. ---//
                test.validateElementIsPresent(driver, Elements.DSP_Buy_SelectACountry_Dd_Open);

                //--- Action: Select a country from Select a Country dropdown ---//
                string country_txt = util.GetText(driver, By.CssSelector("section[id$='buy'] * div>div:nth-of-type(1) * div[class$='country-filter'] * a[data-id='VN']"));
                action.IClick(driver, By.CssSelector("section[id$='buy'] * div>div:nth-of-type(1) * div[class$='country-filter'] * a[data-id='VN']"));

                //--- Expected Result: Country should be selected  ---//
                test.validateStringIsCorrect(driver, Elements.DSP_Buy_SelectACountry_Dd_Selected_Val, country_txt);
            }
        }

        [Test, Category("DSP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Buy Table is Present and Working as Expected when the User is Logged In in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Buy Table is Present and Working as Expected when the User is Logged In in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Buy Table is Present and Working as Expected when the User is Logged In in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Buy Table is Present and Working as Expected when the User is Logged In in RU Locale")]
        public void DSP_Buy_VerifyBuyTableWhenUserIsLoggedIn(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + dsp_url);

            if (util.CheckElement(driver, Elements.MyAnalog_Widget_Btn, 2))
            {
                action.ILogInViaMyAnalogWidget(driver, username, password);

                //----- R11 > T9: Verify Country dropdown as a registered user on Buy section -----//

                //--- Expected Result: Country Dropdown should be the same country set on the account information of the account used ---//
                test.validateStringIsCorrect(driver, Elements.DSP_Buy_SelectACountry_Dd_Selected_Val, country_selected);

                if (!util.GetText(driver, Elements.DSP_Buy_SelectACountry_Dd_Selected_Val).Equals("Select a country"))
                {
                    if (util.CheckElement(driver, Elements.DSP_Buy_CheckInventory_Btn, 2))
                    {
                        //--- Action: Click on Check Inventory button ---//
                        action.IClick(driver, Elements.DSP_Buy_CheckInventory_Btn);

                        //--- Expected Result: The Buy Table will change into distributor table ---//
                        test.validateElementIsPresent(driver, Elements.DSP_Buy_ViewInventory_Tbl);
                    }
                    else
                    {
                        test.validateElementIsPresent(driver, Elements.DSP_Buy_CheckInventory_Btn);
                    }
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.MyAnalog_Widget_Btn);
            }
        }
    }
}