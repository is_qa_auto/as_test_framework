﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;

namespace AS_Test_Framework.DSP
{
    [TestFixture]
    public class DSP_Buy_HeaderTitle : BaseSetUp
    {
        public DSP_Buy_HeaderTitle() : base() { }

        //--- URLs ---//
        string dsp_url = "/adswt-cces";

        [Test, Category("DSP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Buy Header Title is Present in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Buy Header Title is Present in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Buy Header Title is Present in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Buy Header Title is Present in RU Locale")]
        public void DSP_Buy_VerifyHeaderTitle(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + dsp_url);

            //----- R11 > T1: Verify the Buy title  -----//

            //--- Expected Result: Buy section label is present ---//
            test.validateElementIsPresent(driver, Elements.DSP_Buy_Section_Lbl);

            //--- Expected Result: Reminder should be present ---//
            test.validateElementIsPresent(driver, Elements.DSP_Buy_Reminder_Txt);
        }
    }
}