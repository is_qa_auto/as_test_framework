﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;

namespace AS_Test_Framework.DSP
{
    [TestFixture]
    public class DSP_RelatedHardware_Sections : BaseSetUp
    {
        public DSP_RelatedHardware_Sections() : base() { }

        //--- URLs ---//
        string dsp_url = "/adswt-cces";
        string evalBoard_url_format = "/evaluation-boards-kits/";

        //--- Labels ---//
        string seeAll_txt = "See All";

        int deafult_relatedHardware_count = 4;

        [Test, Category("DSP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Sections with 4 or Less Related Hardwares is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Sections with 4 or Less Related Hardwares is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Sections with 4 or Less Related Hardwares is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Sections with 4 or Less Related Hardwares is Present and Working as Expected in RU Locale")]
        public void DSP_RelatedHardware_VerifySectionsWithFourOrLessRelatedHardwares(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + dsp_url);

            if (util.CheckElement(driver, Elements.DSP_RelatedHardware_ReferenceDesigns_List, 2))
            {
                int relatedHardware_section_count = util.GetCount(driver, By.CssSelector("section[id$='relatedhardware']>div>div"));

                for (int ctr = 1; ctr <= relatedHardware_section_count; ctr++)
                {
                    if (util.CheckElement(driver, By.CssSelector("section[id$='relatedhardware'] * div[class*='reference-designs-list']:nth-of-type(" + ctr + ")"), 2) && util.GetCount(driver, By.CssSelector("section[id$='relatedhardware'] * div[class*='reference-designs-list']:nth-of-type(" + ctr + ") * a[target]")) <= deafult_relatedHardware_count)
                    {
                        //----- R9 > T2_1: Verify the Non-accordion Sections under Related Hardware that has "4 or less Related Hardwares" -----//

                        //--- Expected Result: There's a Section Header present for each section. ---//
                        test.validateElementIsPresent(driver, By.CssSelector("section[id$='relatedhardware'] * div[class$='Title']:nth-of-type(" + (ctr - 1) + ") * h3"));

                        //--- Expected Result: Each will have a Link. ---//
                        test.validateElementIsPresent(driver, By.CssSelector("section[id$='relatedhardware'] * div[class*='reference-designs-list']:nth-of-type(" + ctr + ") * a[target]"));

                        //--- Expected Result: Each will have a Description. ---//
                        test.validateElementIsPresent(driver, By.CssSelector("section[id$='relatedhardware'] * div[class*='reference-designs-list']:nth-of-type(" + ctr + ") * div[class$='Info']"));

                        //--- Expected Result: The See All Button should not be present. ---//
                        test.validateElementIsNotPresent(driver, By.CssSelector("section[id$='relatedhardware'] * div[class*='reference-designs-list']:nth-of-type(" + ctr + ") a[class*='MoreButton']"));

                        break;
                    }
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.DSP_RelatedHardware_ReferenceDesigns_List);
            }
        }

        [Test, Category("DSP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Sections with More than 4 Related Hardwares is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Sections with More than 4 Related Hardwares is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Sections with More than 4 Related Hardwares is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Sections with More than 4 Related Hardwares is Present and Working as Expected in RU Locale")]
        public void DSP_RelatedHardware_VerifySectionsWithMoreThanFourRelatedHardwares(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + dsp_url);

            if (util.CheckElement(driver, Elements.DSP_RelatedHardware_ReferenceDesigns_List, 2))
            {
                int relatedHardware_section_count = util.GetCount(driver, By.CssSelector("section[id$='relatedhardware']>div>div"));

                for (int ctr = 1; ctr <= relatedHardware_section_count; ctr++)
                {
                    if (util.CheckElement(driver, By.CssSelector("section[id$='relatedhardware'] * div[class*='reference-designs-list']:nth-of-type(" + ctr + ")"), 2) && util.GetCount(driver, By.CssSelector("section[id$= 'relatedhardware'] * div[class*='reference-designs-list']:nth-of-type(" + ctr + ") * a[target]")) > deafult_relatedHardware_count)
                    {
                        //----- R9 > T2_2: Verify the Non-accordion Sections under Related Hardware that has "more than 4 Related Hardwares" -----//

                        //--- Locators ---//
                        string dsp_sectionHeader_txt_locator = "section[id$='relatedhardware'] * div[class$='Title']:nth-of-type(" + (ctr - 1) + ") * h3";
                        string dsp_links_locator = "section[id$= 'relatedhardware'] * div[class*='reference-designs-list']:nth-of-type(" + ctr + ") * a[target]";
                        string dsp_expandedView_links_locator = "section[id$= 'relatedhardware'] * div[class*='reference-designs-list']:nth-of-type(" + ctr + ") ul>li:nth-of-type(5) * a[target]";
                        string dsp_seeAll_viewLess_btn_locator = "section[id$='relatedhardware'] * div[class*='reference-designs-list']:nth-of-type(" + ctr + ") a[class*='MoreButton']";

                        int relatedHardwares_count = util.GetCount(driver, By.CssSelector(dsp_links_locator));
                        string sectionHeader_txt = util.GetText(driver, By.CssSelector(dsp_sectionHeader_txt_locator));

                        //--- Expected Result: There's a Section Header present for each section. ---//
                        test.validateElementIsPresent(driver, By.CssSelector(dsp_sectionHeader_txt_locator));

                        //--- Expected Result: Each will have a Link. ---//
                        test.validateElementIsPresent(driver, By.CssSelector(dsp_links_locator));

                        //--- Expected Result: Each will have a Description. ---//
                        test.validateElementIsPresent(driver, By.CssSelector("section[id$='relatedhardware'] * div[class*='reference-designs-list']:nth-of-type(" + ctr + ") * div[class$='Info']"));

                        //--- Expected Result: The See All Button should be present. ---//
                        test.validateElementIsPresent(driver, By.CssSelector(dsp_seeAll_viewLess_btn_locator));

                        if (util.CheckElement(driver, By.CssSelector(dsp_seeAll_viewLess_btn_locator), 2))
                        {
                            //--- Expected Result: For EN Locale: See All<Total Number of Related Hardwares under the Section> <Section Name> ---//
                            //--- Example: See All 10 Daughter Boards ---//
                            if (Locale.Equals("en"))
                            {
                                test.validateStringInstance(driver, seeAll_txt + " " + relatedHardwares_count + " " + sectionHeader_txt, util.GetText(driver, By.CssSelector(dsp_seeAll_viewLess_btn_locator)));
                            }
                            else
                            {
                                test.validateStringInstance(driver, relatedHardwares_count.ToString(), Regex.Match(util.GetText(driver, By.CssSelector(dsp_seeAll_viewLess_btn_locator)), @"\d+").Value);
                            }

                            //--- Action: Click the See All Button ---//
                            action.IClick(driver, By.CssSelector(dsp_seeAll_viewLess_btn_locator));

                            //--- Expected Result: All the Related Hardwares under the Section will be dispalyed. ---//
                            test.validateCountIsEqual(driver, relatedHardwares_count, util.GetCount(driver, By.CssSelector("section[id$= 'relatedhardware'] * div[class*='reference-designs-list']:nth-of-type(" + ctr + ") * li[style*='list-item'] * a")));

                            //--- Expected Result: The View Less Button will be displayed. ---//
                            test.validateElementIsPresent(driver, By.CssSelector(dsp_seeAll_viewLess_btn_locator));

                            //--- Expected Result: Each will have a Link. ---//
                            test.validateElementIsPresent(driver, By.CssSelector(dsp_expandedView_links_locator));

                            //--- Expected Result: Each will have a Description. ---//
                            test.validateElementIsPresent(driver, By.CssSelector("section[id$= 'relatedhardware'] * div[class*='reference-designs-list']:nth-of-type(" + ctr + ") ul>li:nth-of-type(5) * div[class$='Info']"));

                            if (util.CheckElement(driver, By.CssSelector(dsp_expandedView_links_locator), 2))
                            {
                                //--- Action: Click on Each Product Link ---//
                                action.IOpenLinkInNewTab(driver, By.CssSelector(dsp_expandedView_links_locator));
                                Thread.Sleep(1000);

                                //--- Expected Result: An Evalboard page is opened in a new tab ---//
                                test.validateStringInstance(driver, driver.Url, evalBoard_url_format);

                                driver.Close();
                                driver.SwitchTo().Window(driver.WindowHandles.First());
                            }

                            //--- Action: Click the View Less Button ---//
                            action.IClick(driver, By.CssSelector(dsp_seeAll_viewLess_btn_locator));

                            //--- Expected Result: The See All Button will be displayed. ---//
                            test.validateElementIsPresent(driver, By.CssSelector(dsp_seeAll_viewLess_btn_locator));

                            //--- Expected Result: Only the 1st Row of Related Hardwares will be displayed. ---//
                            test.validateCountIsEqual(driver, deafult_relatedHardware_count, util.GetCount(driver, By.CssSelector("section[id$= 'relatedhardware'] * div[class*='reference-designs-list']:nth-of-type(" + ctr + ") * li[style*='list-item'] * a")));
                        }

                        break;
                    }
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.DSP_RelatedHardware_ReferenceDesigns_List);
            }
        }
    }
}