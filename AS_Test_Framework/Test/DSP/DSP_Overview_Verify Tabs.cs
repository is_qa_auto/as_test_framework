﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Threading;

namespace AS_Test_Framework.DSP
{
    [TestFixture]
    public class DSP_Overview_Tabs : BaseSetUp
    {
        public DSP_Overview_Tabs() : base() { }

        //--- URLs ---//
        string dsp_url = "/adswt-cces";
        string findSaleOfficeAndDistributor_page_url = "/find-sale-office-distributor.html";
        string salesAndDistribution_jp_page_url = "/jp-sales-and-disti.html";

        [Test, Category("DSP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Features and Benefits is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Features and Benefits is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Features and Benefits is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Features and Benefits is Present and Working as Expected in RU Locale")]
        public void DSP_Overview_VerifyFeaturesAndBenefits(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + dsp_url);

            if (util.CheckElement(driver, Elements.DSP_Overview_FeaturesAndBenefits_Link, 2))
            {
                //----- R3 > T2: Verify Feature & Benefits tab -----//

                if (!util.CheckElement(driver, By.CssSelector("li[class='active']>a[href$='features-and-benefits']"), 2))
                {
                    //--- Action: Select Features & Benefits ---//
                    action.IClick(driver, Elements.DSP_Overview_FeaturesAndBenefits_Link);

                    //--- Expected Result: Features & Benefits is selected ---//
                    test.validateElementIsPresent(driver, By.CssSelector("li[class='active']>a[href$='features-and-benefits']"));
                }

                //--- Expected Result: Content is available ---//
                test.validateElementIsPresent(driver, Elements.DSP_Overview_FeaturesAndBenefits_Content);
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.DSP_Overview_FeaturesAndBenefits_Link);
            }
        }

        [Test, Category("DSP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Product Details is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Product Details is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Product Details is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Product Details is Present and Working as Expected in RU Locale")]
        public void DSP_Overview_VerifyProductDetails(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + dsp_url);

            if (util.CheckElement(driver, Elements.DSP_Overview_ProductDetails_Link, 2))
            {
                //----- R3 > T3: Verify the Product Details tab -----//

                if (!util.CheckElement(driver, By.CssSelector("li[class='active']>a[href$='product-details']"), 2))
                {
                    //--- Action: Select Product Details Tab ---//
                    action.IClick(driver, Elements.DSP_Overview_ProductDetails_Link);

                    //--- Expected Result: Product Details tab is selected ---//
                    test.validateElementIsPresent(driver, By.CssSelector("li[class='active']>a[href$='product-details']"));
                }

                //--- Expected Result: Content is availbale ---//
                test.validateElementIsPresent(driver, Elements.DSP_Overview_ProductDetails_Content);

                if (util.CheckElement(driver, Elements.DSP_Overview_CrossCoreSupport_Sec, 2))
                {
                    //----- R3 > T4: Verify CROSSCORE ® Support section -----//

                    //--- Expected Result: "The following are available: Tel Number, Email Support link and Visit the CrossCore ® Support Community link ---//
                    test.validateElementIsPresent(driver, Elements.DSP_Overview_CrossCoreSupport_Content);
                    test.validateElementIsPresent(driver, Elements.DSP_Overview_CrossCoreSupport_ContactSupport_Link);

                    if (util.CheckElement(driver, Elements.DSP_Overview_CrossCoreSupport_ContactSupport_Link, 2))
                    {
                        //--- Action: Click Email Support link ---//
                        action.IOpenLinkInNewTab(driver, Elements.DSP_Overview_CrossCoreSupport_ContactSupport_Link);
                        Thread.Sleep(1000);

                        //--- Expected Result: Link will redirect to Find Sale Office and Distributor page ---//
                        if (Locale.Equals("jp"))
                        {
                            test.validateStringInstance(driver, driver.Url, salesAndDistribution_jp_page_url);
                        }
                        else
                        {
                            test.validateStringInstance(driver, driver.Url, findSaleOfficeAndDistributor_page_url);
                        }
                    }
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.DSP_Overview_ProductDetails_Link);
            }
        }
    }
}