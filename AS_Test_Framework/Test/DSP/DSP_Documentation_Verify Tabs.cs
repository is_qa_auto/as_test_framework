﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Text.RegularExpressions;
using System.Threading;
using System.Linq;
using System;

namespace AS_Test_Framework.DSP
{
    [TestFixture]
    public class DSP_Documentation_Tabs : BaseSetUp
    {
        public DSP_Documentation_Tabs() : base() { }

        //--- URLs ---//
        string dsp_url = "/adswt-cces";

        //--- Labels ---//
        string viewAll_txt = "View All";
        string softwareManuals_txt = "Software Manuals";
        string videos_txt = "Videos";

        [Test, Category("DSP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the View All Tab is Present and Working as Expected in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the View All Tab is Present and Working as Expected in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the View All Tab is Present and Working as Expected in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the View All Tab is Present and Working as Expected in RU Locale")]
        public void DSP_Documentation_VerifyViewAllTab(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + dsp_url);

            //----- R8 > T1: Verify Documentation section -----//

            //--- Expected Result: Documentation section label is present ---//
            test.validateElementIsPresent(driver, Elements.DSP_Documentation_Sec_Lbl);

            if (util.CheckElement(driver, Elements.DSP_Documentation_Tabs, 2))
            {
                int downloadsAndRelatedSoftwareTabs_count = util.GetCount(driver, Elements.DSP_Documentation_Tabs);

                for (int ctr = 1; ctr <= downloadsAndRelatedSoftwareTabs_count; ctr++)
                {
                    if (util.GetText(driver, By.CssSelector("section[id$='documentation'] * div[class$='tabs']>ul>li:nth-of-type(" + ctr + ")>a")).Contains(viewAll_txt))
                    {
                        //----- R8 > T2: Verify View all tab -----//

                        if (!util.CheckElement(driver, By.CssSelector("section[id$='documentation'] * div[class$='tabs']>ul>li[class$='active']:nth-of-type(" + ctr + ")>a"), 2))
                        {
                            //--- Action: Click View All Tab ---//
                            action.IClick(driver, By.CssSelector("section[id$='documentation'] * div[class$='tabs']>ul>li:nth-of-type(" + ctr + ")>a"));
                        }

                        //--- Expected Result: List of Documentation component are available ---//
                        test.validateElementIsPresent(driver, Elements.DSP_Documentation_Tabs_Content);

                        //--- Expected Result: All available list of components for documentation is sum up and display in View all counter (AL-11066) ---//
                        test.validateCountIsEqual(driver, util.GetCount(driver, By.CssSelector("section[id$='documentation'] * div[class='tab-content']>div[class*='active'] * div[class*='col-sm-12']")), Int32.Parse(Regex.Match(util.GetText(driver, By.CssSelector("section[id$='documentation'] * div[class$='tabs']>ul>li:nth-of-type(" + ctr + ")>a")), @"\d+").Value));

                        break;
                    }

                    if (ctr == downloadsAndRelatedSoftwareTabs_count)
                    {
                        test.validateElementIsPresent(driver, By.CssSelector("section[id$='documentation'] * div[class$='tabs']>ul>li:nth-of-type(" + (ctr + 1) + ")>a"));
                    }
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.DSP_Documentation_Tabs);
            }
        }

        [Test, Category("DSP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Software Manuals Tab is Present and Working as Expected in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Software Manuals Tab is Present and Working as Expected in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Software Manuals Tab is Present and Working as Expected in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Software Manuals Tab is Present and Working as Expected in RU Locale")]
        public void DSP_Documentation_VerifySoftwareManualsTab(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + dsp_url);

            if (util.CheckElement(driver, Elements.DSP_Documentation_Tabs, 2))
            {
                int downloadsAndRelatedSoftwareTabs_count = util.GetCount(driver, Elements.DSP_Documentation_Tabs);

                for (int ctr = 1; ctr <= downloadsAndRelatedSoftwareTabs_count; ctr++)
                {
                    if (util.GetText(driver, By.CssSelector("section[id$='documentation'] * div[class$='tabs']>ul>li:nth-of-type(" + ctr + ")>a")).Contains(softwareManuals_txt))
                    {
                        //----- R8 > T3: Verify Software Manuals -----//

                        if (!util.CheckElement(driver, By.CssSelector("section[id$='documentation'] * div[class$='tabs']>ul>li[class$='active']:nth-of-type(" + ctr + ")>a"), 2))
                        {
                            action.IClick(driver, By.CssSelector("section[id$='documentation'] * div[class$='tabs']>ul>li:nth-of-type(" + ctr + ")>a"));
                        }

                        //--- Expected Result: List of Software Manuals are available in the list ---//
                        test.validateElementIsPresent(driver, Elements.DSP_Documentation_Tab_Links);

                        //--- Expected Result: PDF icon with file size is available in each Software manuals ---//
                        test.validateElementIsPresent(driver, Elements.DSP_Documentation_Tab_FileType_Icons);
                        test.validateElementIsPresent(driver, Elements.DSP_Documentation_Tab_FileSize_Texts);

                        break;
                    }

                    if (ctr == downloadsAndRelatedSoftwareTabs_count)
                    {
                        test.validateElementIsPresent(driver, By.CssSelector("section[id$='documentation'] * div[class$='tabs']>ul>li:nth-of-type(" + (ctr + 1) + ")>a"));
                    }
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.DSP_Documentation_Tabs);
            }
        }

        [Test, Category("DSP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Videos Tab is Present and Working as Expected in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Videos Tab is Present and Working as Expected in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Videos Tab is Present and Working as Expected in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Videos Tab is Present and Working as Expected in RU Locale")]
        public void DSP_Documentation_VerifyVideosTab(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + dsp_url);

            if (util.CheckElement(driver, Elements.DSP_Documentation_Tabs, 2))
            {
                int downloadsAndRelatedSoftwareTabs_count = util.GetCount(driver, Elements.DSP_Documentation_Tabs);

                for (int ctr = 1; ctr <= downloadsAndRelatedSoftwareTabs_count; ctr++)
                {
                    if (util.GetText(driver, By.CssSelector("section[id$='documentation'] * div[class$='tabs']>ul>li:nth-of-type(" + ctr + ")>a")).Contains(videos_txt))
                    {
                        //----- R8 > T4: Verify Videos (AL-11573) -----//

                        if (!util.CheckElement(driver, By.CssSelector("section[id$='documentation'] * div[class$='tabs']>ul>li[class$='active']:nth-of-type(" + ctr + ")>a"), 2))
                        {
                            //--- Action: Under Documentation section click Videos tab ---//
                            action.IClick(driver, By.CssSelector("section[id$='documentation'] * div[class$='tabs']>ul>li:nth-of-type(" + ctr + ")>a"));
                        }

                        //--- Expected Result: Each tiles should dislay an image ---//
                        test.validateElementIsPresent(driver, Elements.DSP_Documentation_Tab_Video_Thumbnails);

                        //--- Expected Result: List of Videos are available in the list ---//
                        test.validateElementIsPresent(driver, Elements.DSP_Documentation_Tab_Links);

                        if (util.CheckElement(driver, Elements.DSP_Documentation_Tab_Links, 2))
                        {
                            string VideoName = util.GetText(driver, Elements.DSP_Documentation_Tab_Links);
                            //--- Action: Click each available link ---//
                            action.IClick(driver, Elements.DSP_Documentation_Tab_Links);
                            driver.SwitchTo().Window(driver.WindowHandles.Last());
                            //For AL-18321
                            test.validateStringInstance(driver, driver.Url, "/videos/");
                            test.validateStringIsCorrect(driver, By.CssSelector("div[name='adi_breadcrumb'] li[class='active']>span"), VideoName);

                            //Removed due to AL-18321
                            ////--- Expected Result: Pop-up window will appear ---//
                            //test.validateElementIsPresent(driver, Elements.DSP_Documentation_Tab_Video_PopUp);

                            //if (util.CheckElement(driver, Elements.DSP_Documentation_Tab_Video_PopUp, 2))
                            //{
                            //    //--- Action: Click Close icon ---//
                            //    action.IClick(driver, Elements.DSP_Documentation_Tab_Video_PopUp_X_Btn);

                            //    //--- Expected Result: Pop-up window will be close ---//
                            //    test.validateElementIsNotPresent(driver, Elements.DSP_Documentation_Tab_Video_PopUp);
                            //}
                        }

                        break;
                    }

                    if (ctr == downloadsAndRelatedSoftwareTabs_count)
                    {
                        test.validateElementIsPresent(driver, By.CssSelector("section[id$='documentation'] * div[class$='tabs']>ul>li:nth-of-type(" + (ctr + 1) + ")>a"));
                    }
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.DSP_Documentation_Tabs);
            }
        }
    }
}