﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;

namespace AS_Test_Framework.DSP
{
    [TestFixture]
    public class DSP_RelatedHardware_HeaderTitle : BaseSetUp
    {
        public DSP_RelatedHardware_HeaderTitle() : base() { }

        //--- URLs ---//
        string dsp_url = "/adswt-cces";

        //--- Labels ---//
        string relatedHardware_txt = "Related Hardware";

        [Test, Category("DSP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Related Hardware Header Title is Present and Correct in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Related Hardware Header Title is Present in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Related Hardware Header Title is Present in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Related Hardware Header Title is Present in RU Locale")]
        public void DSP_RelatedHardware_VerifyHeaderTitle(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + dsp_url);

            //----- R9 > T1: Verify the Related Hardware Title -----//
            int relatedHardwares_count = util.GetCount(driver, Elements.DSP_RelatedHardware_Links);

            //--- Expected Result: The header title "Related Hardware" must be displayed ---//
            //--- Expected Result: There should be a Counter displayed beside "Related Hardware" title enclosed by parenthesis showing the Total Number of available Related Hardware(s). ---//
            if (Locale.Equals("en"))
            {
                test.validateStringInstance(driver, relatedHardware_txt + " (" + relatedHardwares_count + ")", util.GetText(driver, Elements.DSP_RelatedHardware_Section_Lbl));
            }
            else
            {
                test.validateStringInstance(driver, util.GetText(driver, Elements.DSP_RelatedHardware_Section_Lbl), "(" + relatedHardwares_count + ")");
            }
        }
    }
}