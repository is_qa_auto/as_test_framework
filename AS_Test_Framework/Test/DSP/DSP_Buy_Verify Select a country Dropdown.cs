﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;

namespace AS_Test_Framework.DSP
{
    [TestFixture]
    public class DSP_Buy_SelectACountryDropdown : BaseSetUp
    {
        public DSP_Buy_SelectACountryDropdown() : base() { }

        //--- URLs ---//
        string dsp_url = "/adswt-cces";

        [Test, Category("DSP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Select a country Dropdown is Present and Working as Expected when the Same Country is Selected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Select a country Dropdown is Present and Working as Expected when the Same Country is Selected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Select a country Dropdown is Present and Working as Expected when the Same Country is Selected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Select a country Dropdown is Present and Working as Expected when the Same Country is Selected in RU Locale")]
        public void DSP_Buy_VerifySelectACountryDropdownWhenSameCountryIsSelected(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + dsp_url);

            if (util.CheckElement(driver, Elements.DSP_Buy_SelectACountry_Dd, 2))
            {
                //----- R11 > T6: Verify Select a Country dropdown behavior on Buy Section (When user select same country) -----//

                //--- Inputs ---//
                string selectACountry_dd_input = "US";

                //--- Action: Select a country from Select a Country dropdown ---//
                action.IClick(driver, Elements.DSP_Buy_SelectACountry_Dd_Btn);
                action.IClick(driver, By.CssSelector("section[id$='buy'] * div>div:nth-of-type(1) * div[class$='country-filter'] * a[data-id='" + selectACountry_dd_input + "']"));

                //--- Expected Result: Check Inventory button should be enabled ---//
                test.validateElementIsEnabled(driver, Elements.DSP_Buy_CheckInventory_Btn);

                if (driver.FindElement(Elements.DSP_Buy_CheckInventory_Btn).Enabled)
                {
                    //--- Action: Click Check Inventory button  ---//
                    action.IClick(driver, Elements.DSP_Buy_CheckInventory_Btn);

                    if (util.CheckElement(driver, Elements.DSP_Buy_ViewInventory_Tbl, 2))
                    {
                        //--- Action: Select the same Country from Country dropdown ---//
                        action.IClick(driver, Elements.DSP_Buy_SelectACountry_Dd_Btn);
                        action.IClick(driver, By.CssSelector("section[id$='buy'] * div>div:nth-of-type(1) * div[class$='country-filter'] * a[data-id='" + selectACountry_dd_input + "']"));

                        //--- Expected Result: The Check Inventory button should remain disabled  ---//
                        test.validateElementIsPresent(driver, Elements.DSP_Buy_Disabled_CheckInventory_Btn);
                    }
                    else
                    {
                        test.validateElementIsPresent(driver, Elements.DSP_Buy_ViewInventory_Tbl);
                    }
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.DSP_Buy_SelectACountry_Dd);
            }
        }

        [Test, Category("DSP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Select a country Dropdown is Present and Working as Expected when a Different Country is Selected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Select a country Dropdown is Present and Working as Expected when a Different Country is Selected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Select a country Dropdown is Present and Working as Expected when a Different Country is Selected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Select a country Dropdown is Present and Working as Expected when a Different Country is Selected in RU Locale")]
        public void DSP_Buy_VerifySelectACountryDropdownWhenDifferentCountryIsSelected(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + dsp_url);

            if (util.CheckElement(driver, Elements.DSP_Buy_SelectACountry_Dd, 2))
            {
                //----- R11 > T7: Verify Select a Country dropdown behavior on Buy Section (When user select different country) -----//

                //--- Inputs ---//
                string selectACountry_dd_input1 = "US";

                //--- Action: Select a country from Select a Country dropdown ---//
                action.IClick(driver, Elements.DSP_Buy_SelectACountry_Dd_Btn);
                action.IClick(driver, By.CssSelector("section[id$='buy'] * div>div:nth-of-type(1) * div[class$='country-filter'] * a[data-id='" + selectACountry_dd_input1 + "']"));

                if (driver.FindElement(Elements.DSP_Buy_CheckInventory_Btn).Enabled)
                {
                    //--- Action: Click Check Inventory button  ---//
                    action.IClick(driver, Elements.DSP_Buy_CheckInventory_Btn);

                    if (util.CheckElement(driver, Elements.DSP_Buy_ViewInventory_Tbl, 2))
                    {
                        //--- Inputs ---//
                        string selectACountry_dd_input2 = "MX";

                        //--- Action: Select again the a country ---//
                        action.IClick(driver, Elements.DSP_Buy_SelectACountry_Dd_Btn);
                        action.IClick(driver, By.CssSelector("section[id$='buy'] * div>div:nth-of-type(1) * div[class$='country-filter'] * a[data-id='" + selectACountry_dd_input2 + "']"));

                        //--- Expected Result: The page should switch back to initial view ---//
                        test.validateElementIsPresent(driver, Elements.DSP_Buy_Tbl);

                        //--- Expected Result: The Check Inentory button becomes enabled ---//
                        test.validateElementIsEnabled(driver, Elements.DSP_Buy_CheckInventory_Btn);
                    }
                    else
                    {
                        test.validateElementIsPresent(driver, Elements.DSP_Buy_ViewInventory_Tbl);
                    }
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.DSP_Buy_SelectACountry_Dd);
            }
        }
    }
}