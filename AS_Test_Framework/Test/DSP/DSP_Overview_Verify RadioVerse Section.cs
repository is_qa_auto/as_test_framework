﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Linq;
using System.Threading;
using OpenQA.Selenium;
using System;
using System.Text.RegularExpressions;

namespace AS_Test_Framework.DSP
{
    [TestFixture]
    public class DSP_Overview_RadioVerseSection : BaseSetUp
    {
        public DSP_Overview_RadioVerseSection() : base() { }

        //--- URLs ---//
        string dsp_withMarketsAndTechnology_url = "/design-center/evaluation-hardware-and-software/software/a2b-software.html";
        string applications_page_url_format = "/applications/";
        //--- Labels ---//
        string marketsAndTechnology_txt = "Markets & Technology";

        [Test, Category("PDP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Markets and Technology is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Markets and Technology is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Markets and Technology is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Markets and Technology is Present and Working as Expected in RU Locale")]
        public void PDP_Overview_VerifyMarketsAndTechnology(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + dsp_withMarketsAndTechnology_url);

            int sectionHeader_count = util.GetCount(driver, By.CssSelector("div[class^='radioverse']>h2"));

            for (int sectionHeader_ctr = 1; sectionHeader_ctr <= sectionHeader_count; sectionHeader_ctr++)
            {
                if (util.GetText(driver, By.CssSelector("div[class^='radioverse']>h2:nth-of-type(" + sectionHeader_ctr + ")")).Equals(marketsAndTechnology_txt))
                {
                    //----- R4 > T5: Verify the Markets & technology section -----//

                    /*****this is for AL-16901*****/
                    test.validateElementIsPresent(driver, By.CssSelector("div[class^='radioverse'] ul>li[class='accordionGroup']"));
                    test.validateElementIsPresent(driver, By.CssSelector("div[class^='radioverse'] ul>li[class='accordionGroup']:nth-child(1)>div[class='accordionGroup__toggle collapsed']"));
                    string SubItemCount = Regex.Match(util.GetText(driver, By.CssSelector("div[class^='radioverse'] ul>li[class='accordionGroup']>div>a>span")), @"\d+").Value;
                    /***expand the accordion***/
                    action.IClick(driver, By.CssSelector("div[class^='radioverse'] ul>li[class='accordionGroup']:nth-child(1)>div[class='accordionGroup__toggle collapsed'] span[class='accordionGroup__toggle__icon']"));
                    test.validateElementIsPresent(driver, By.CssSelector("div[class^='radioverse'] ul>li[class='accordionGroup']:nth-child(1)>div[class='accordionGroup__toggle']"));
                    test.validateElementIsPresent(driver, By.CssSelector("div[class^='radioverse'] ul>li[class='accordionGroup']:nth-child(1) div[class='accordionGroup__body__inner']"));
                    test.validateCountIsEqual(driver, Int32.Parse(SubItemCount), util.GetCount(driver, By.CssSelector("div[class^='radioverse'] ul>li[class='accordionGroup']:nth-child(1) div[class='accordionGroup__body__inner']>ul>li")));

                    if (util.CheckElement(driver, Elements.CFTL_Overview_MarketsAndTechnology_Links, 2))
                    {
                        //--- Action: Clicking on a Markets link ---//
                        action.IOpenLinkInNewTab(driver, Elements.CFTL_Overview_MarketsAndTechnology_Links);
                        Thread.Sleep(2000);

                        //--- Expected Result: Clicking the Markets link should go correspnding Application pages ---//
                        test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + applications_page_url_format);
                    }
                    else
                    {
                        test.validateElementIsPresent(driver, Elements.CFTL_Overview_MarketsAndTechnology_Links);
                    }
                    driver.SwitchTo().Window(driver.WindowHandles.First());
                    action.IOpenLinkInNewTab(driver, Elements.PDP_MarketsAndTechnology_Link);
                    Thread.Sleep(2000);

                    //--- Expected Result: Clicking the Markets link should go correspnding Application pages ---//
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + applications_page_url_format);

                    break;
                }
            }
        }
    }
}