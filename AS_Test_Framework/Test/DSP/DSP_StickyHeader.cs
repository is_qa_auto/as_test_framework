﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;

namespace AS_Test_Framework.DSP
{
    [TestFixture]
    public class DSP_StickyHeader : BaseSetUp
    {
        public DSP_StickyHeader() : base() { }

        //--- URLs ---//
        string dsp_url = "/adswt-cces";
        string dsp_overview_url = "#software-overview";
        string dsp_downloadsAndRelatedSoftware_url = "#software-relatedsoftware";
        string dsp_licensing_url = "#software-licensing";
        string dsp_systemsRequirements_url = "#software-requirement";
        string dsp_documentation_url = "#software-documentation";
        string dsp_relatedHardware_url = "#software-relatedhardware";
        string dsp_discussions_url = "#software-discussions";
        string dsp_buy_url = "#software-buy";

        [Test, Category("DSP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Sticky Header is Present and Working as Expected in EN Locale", Category = "Smoke_ADIWeb")]
        [TestCase("cn", TestName = "Verify that the Sticky Header is Present and Working as Expected in CN Locale", Category = "Smoke_ADIWeb")]
        [TestCase("jp", TestName = "Verify that the Sticky Header is Present and Working as Expected in JP Locale", Category = "Smoke_ADIWeb")]
        [TestCase("ru", TestName = "Verify that the Sticky Header is Present and Working as Expected in RU Locale")]
        public void DSP_VerifyStickyHeader(string Locale)
        {
            string initial_steps = "1. Go to " + Configuration.Env_Url + Locale + dsp_url;
            string scenario = "";
            action.INavigate(driver, Configuration.Env_Url + Locale + dsp_url);

            if (util.CheckElement(driver, Elements.DSP_StickyHeader_Sec, 2))
            {
                //----- R1: Verify Sticky Header -----//

                //--- Expected Result: Description is available at the upper right of the page ---//
                scenario = "Verify that DSP Description is available at the upper right part of the page";
                test.validateElementIsPresentv2(driver, Elements.DSP_Desc_Txt, scenario, initial_steps);

                //----- R1 > T1: Verify when Overview menu tab is selected -----//
                if (util.CheckElement(driver, Elements.DSP_StickyHeader_Overview, 2))
                {
                    //--- Action: Click Overview tab ---//
                    action.IClick(driver, Elements.DSP_StickyHeader_Overview);

                    //--- Expected Result: It anchored to overview section properly ---//
                    scenario = "Verify that page will anchored to overview section upon clicking the Overview Tab in sticky header";
                    test.validateStringInstancev2(driver, driver.Url, dsp_overview_url,scenario , initial_steps + "<br>2. Click Overview Tab");
                }

                //----- R1 > T2: Verify when Downloads & Related Software menu tab is selected -----//
                if (util.CheckElement(driver, Elements.DSP_StickyHeader_DownloadsAndRelatedSoftware, 2))
                {
                    //--- Action: Click Downloads & Related Software tab ---//
                    action.IClick(driver, Elements.DSP_StickyHeader_DownloadsAndRelatedSoftware);

                    //--- Expected Result: It anchored to Downloads And Related Software section properly ---//
                    scenario = "Verify that page will anchored to Downloads And Related Software section upon clicking the Overview Tab in sticky header";
                    test.validateStringInstancev2(driver, driver.Url, dsp_downloadsAndRelatedSoftware_url, scenario, initial_steps + "<br>2. Click Download and Related Software Tab in sticky Header");
                }

                //----- R1 > T3: Verify when Licensing menu tab is selected -----//
                if (util.CheckElement(driver, Elements.DSP_StickyHeader_Licensing, 2))
                {
                    //--- Action: Click Licensing tab ---//
                    action.IClick(driver, Elements.DSP_StickyHeader_Licensing);

                    //--- Expected Result: It anchored to Licensing section properly ---//
                    scenario = "Verify that page will anchored to Licensing section upon clicking the Licensing tab in sticky header";
                    test.validateStringInstancev2(driver, driver.Url, dsp_licensing_url, scenario, initial_steps + "<br>2. Click licensing tab in sticky Header");
                }

                //----- R1 > T5: Verify when System Requirements tab is selected -----//
                if (util.CheckElement(driver, Elements.DSP_StickyHeader_SystemsRequirements, 2))
                {
                    //--- Action: Click System Requirements tab ---//
                    action.IClick(driver, Elements.DSP_StickyHeader_SystemsRequirements);

                    //--- Expected Result: It anchored to Systems Requirements section properly ---//
                    scenario = "Verify that page will anchored to Systems Requirements section upon clicking the Systems Requirements tab in sticky header";
                    test.validateStringInstancev2(driver, driver.Url, dsp_systemsRequirements_url, scenario, initial_steps + "<br>2. Click Systems Requirements tab in sticky Header");
                }

                //----- R1 > T6: Verify when Documentation tab is selected -----//
                if (util.CheckElement(driver, Elements.DSP_StickyHeader_Documentation, 2))
                {
                    //--- Action: Click Documentation tab ---//
                    action.IClick(driver, Elements.DSP_StickyHeader_Documentation);

                    //--- Expected Result: It anchored to Documentation section properly ---//
                    scenario = "Verify that page will anchored to Documentation section upon clicking the Documentation tab in sticky header";
                    test.validateStringInstancev2(driver, driver.Url, dsp_documentation_url, scenario, initial_steps + "<br>2. Click Documentation tab in sticky Header");
                }

                //----- R1 > T7: Verify when Related Hardware tab is selected -----//
                if (util.CheckElement(driver, Elements.DSP_StickyHeader_RelatedHardware, 2))
                {
                    //--- Action: Click Related Hardware tab ---//
                    action.IClick(driver, Elements.DSP_StickyHeader_RelatedHardware);

                    //--- Expected Result: It anchored to Related Hardware section properly ---//
                    scenario = "Verify that page will anchored to related hardware section upon clicking the related hardware tab in sticky header";
                    test.validateStringInstancev2(driver, driver.Url, dsp_relatedHardware_url, scenario, initial_steps + "<br>2. Click related hardware tab in sticky Header");
                }

                //----- R1 > T8: Verify when Discussion tab is selected -----//
                if (util.CheckElement(driver, Elements.DSP_StickyHeader_Discussions, 2))
                {
                    //--- Action: Click Discussion tab ---//
                    action.IClick(driver, Elements.DSP_StickyHeader_Discussions);

                    //--- Expected Result: It anchored to Discussions section properly ---//
                    scenario = "Verify that page will anchored to discussions section upon clicking the discussion tab in sticky header";
                    test.validateStringInstancev2(driver, driver.Url, dsp_discussions_url, scenario, initial_steps + "<br>2. Click discussion tab in sticky Header");
                }

                //----- R1 > T9: Verify when Buy tab is selected -----//
                if (util.CheckElement(driver, Elements.DSP_StickyHeader_Buy, 2))
                {
                    //--- Action: Click Buy tab ---//
                    action.IClick(driver, Elements.DSP_StickyHeader_Buy);

                    //--- Expected Result: It anchored to Buy section properly ---//
                    scenario = "Verify that page will anchored to Buy section upon clicking the buy tab in sticky header";
                    test.validateStringInstancev2(driver, driver.Url, dsp_buy_url, scenario, initial_steps + "<br>2. Click buy tab in sticky Header");
                }

                //--- Expected Result: Top icon is available ---//

                scenario = "Verify that Top Icon will be present once user scroll down to the bottom of the page";
                test.validateElementIsPresentv2(driver, Elements.DSP_Top_Btn, scenario, initial_steps + "<br>2. Click buy tab in sticky Header");

                //----- R1 > T11: Verify the "Show More" and "Show Less.." link in the DSP product description. -----//
                if (util.CheckElement(driver, Elements.DSP_ShowMoreLess_Link, 2))
                {
                    //--- Action: Click on the "Show More.." link in the product description. ---//
                    action.IClick(driver, Elements.DSP_ShowMoreLess_Link);

                    //--- Expected Result: "Show Less.." link must be present/displayed at the end of product description. ---//
                    scenario = "Verify that show less link will be displayed in the DSP description";
                    test.validateElementIsPresentv2(driver, Elements.DSP_ShowMoreLess_Link, scenario, initial_steps + "<br>2. Click Show More link in the production description");
                }
            }
            else
            {
                scenario = "Verify that sticky header is present in DSP page";
                test.validateElementIsPresentv2(driver, Elements.DSP_StickyHeader_Sec, scenario, initial_steps);
            }
        }
    }
}