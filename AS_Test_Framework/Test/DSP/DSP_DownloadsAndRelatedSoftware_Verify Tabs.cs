﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Text.RegularExpressions;
using System.Threading;

namespace AS_Test_Framework.DSP
{
    [TestFixture]
    public class DSP_DownloadsAndRelatedSoftware_Tabs : BaseSetUp
    {
        public DSP_DownloadsAndRelatedSoftware_Tabs() : base() { }

        //--- URLs ---//
        string dsp_url = "/adswt-cces";
        string dsp_url_format = "/design-center/evaluation-hardware-and-software/software/";

        //--- Labels ---//
        string viewAll_txt = "View All";
        string productDownloads_txt = "Product Downloads";
        string middleware_txt = "Middleware";

        [Test, Category("DSP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the View All Tab is Present and Working as Expected in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the View All Tab is Present and Working as Expected in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the View All Tab is Present and Working as Expected in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the View All Tab is Present and Working as Expected in RU Locale")]
        public void DSP_DownloadsAndRelatedSoftware_VerifyViewAllTab(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + dsp_url);

            if (util.CheckElement(driver, Elements.DSP_DownloadsAndRelatedSoftware_Tabs, 2))
            {
                int downloadsAndRelatedSoftwareTabs_count = util.GetCount(driver, Elements.DSP_DownloadsAndRelatedSoftware_Tabs);

                for (int ctr = 1; ctr <= downloadsAndRelatedSoftwareTabs_count; ctr++)
                {
                    if (util.GetText(driver, By.CssSelector("section[id$='relatedsoftware'] * div[class$='tabs']>ul>li:nth-of-type(" + ctr + ")>a")).Contains(viewAll_txt))
                    {
                        //----- R4 > T2: Verify the Tabs (AL-10941) -----//

                        if (!util.CheckElement(driver, By.CssSelector("section[id$='relatedsoftware'] * div[class$='tabs']>ul>li[class$='active']:nth-of-type(" + ctr + ")>a"), 2))
                        {
                            //--- Action: Select View All tab ---//
                            action.IClick(driver, By.CssSelector("section[id$='relatedsoftware'] * div[class$='tabs']>ul>li:nth-of-type(" + ctr + ")>a"));
                        }

                        //--- Expected Result: View all is selected ---//
                        test.validateElementIsPresent(driver, By.CssSelector("section[id$='relatedsoftware'] * div[class$='tabs']>ul>li[class$='active']:nth-of-type(" + ctr + ")>a"));

                        ////--- Get the Total Count of Links under the Product Downloads Section ---//
                        //int productDownloads_sec_count = 0;
                        //if (util.CheckElement(driver, Elements.DSP_DownloadsAndRelatedSoftware_ProductDownloads_Tab_Secs, 2))
                        //{
                        //    productDownloads_sec_count = util.GetCount(driver, Elements.DSP_DownloadsAndRelatedSoftware_ProductDownloads_Tab_Secs);
                        //}

                        ////--- Get the Total Count of Links under the Middleware Section ---//
                        //int middleware_links_count = 0;
                        //if (util.CheckElement(driver, Elements.DSP_DownloadsAndRelatedSoftware_Middleware_Tab_Links, 2))
                        //{
                        //    middleware_links_count = util.GetCount(driver, Elements.DSP_DownloadsAndRelatedSoftware_Middleware_Tab_Links);
                        //}

                        ////--- Expected Result: Number of available component is sum up and displayed beneath each components ---//
                        //test.validateStringInstance(driver, (productDownloads_sec_count + middleware_links_count).ToString(), Regex.Match(util.GetText(driver, By.CssSelector("section[id$='relatedsoftware'] * div[class$='tabs']>ul>li:nth-of-type(" + ctr + ")>a")), @"\d+").Value);

                        break;
                    }

                    if (ctr == downloadsAndRelatedSoftwareTabs_count)
                    {
                        test.validateElementIsPresent(driver, By.CssSelector("section[id$='relatedsoftware'] * div[class$='tabs']>ul>li:nth-of-type(" + (ctr + 1) + ")>a"));
                    }
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.DSP_DownloadsAndRelatedSoftware_Tabs);
            }
        }

        [Test, Category("DSP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Product Downloads Tab is Present and Working as Expected in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Product Downloads Tab is Present and Working as Expected in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Product Downloads Tab is Present and Working as Expected in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Product Downloads Tab is Present and Working as Expected in RU Locale")]
        public void DSP_DownloadsAndRelatedSoftware_VerifyProductDownloadsTab(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + dsp_url);

            if (util.CheckElement(driver, Elements.DSP_DownloadsAndRelatedSoftware_Tabs, 2))
            {
                int downloadsAndRelatedSoftwareTabs_count = util.GetCount(driver, Elements.DSP_DownloadsAndRelatedSoftware_Tabs);

                for (int ctr = 1; ctr <= downloadsAndRelatedSoftwareTabs_count; ctr++)
                {
                    if (util.GetText(driver, By.CssSelector("section[id$='relatedsoftware'] * div[class$='tabs']>ul>li:nth-of-type(" + ctr + ")>a")).Contains(productDownloads_txt))
                    {
                        //----- R4 > T3: Verify Product Downloads (AL-12222) -----//

                        //--- Action: Click Product Downloads ---//
                        action.IClick(driver, By.CssSelector("section[id$='relatedsoftware'] * div[class$='tabs']>ul>li:nth-of-type(" + ctr + ")>a"));

                        //--- Expected Result: List of Product Downloads are displayed ---//
                        test.validateElementIsPresent(driver, Elements.DSP_DownloadsAndRelatedSoftware_ProductDownloads_Tab_Secs);

                        if (util.CheckElement(driver, Elements.DSP_DownloadsAndRelatedSoftware_ProductDownloads_Tab_Secs, 2))
                        {
                            //--- Expected Result: Number of available component is sum up and displayed beneath each components ---//
                            test.validateStringInstance(driver, util.GetCount(driver, Elements.DSP_DownloadsAndRelatedSoftware_ProductDownloads_Tab_Secs).ToString(), Regex.Match(util.GetText(driver, By.CssSelector("section[id$='relatedsoftware'] * div[class$='tabs']>ul>li:nth-of-type(" + ctr + ")>a")), @"\d+").Value);
                        }

                        break;
                    }

                    if (ctr == downloadsAndRelatedSoftwareTabs_count)
                    {
                        test.validateElementIsPresent(driver, By.CssSelector("section[id$='relatedsoftware'] * div[class$='tabs']>ul>li:nth-of-type(" + (ctr + 1) + ")>a"));
                    }
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.DSP_DownloadsAndRelatedSoftware_Tabs);
            }
        }

        [Test, Category("DSP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Middleware Tab is Present and Working as Expected in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Middleware Tab is Present and Working as Expected in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Middleware Tab is Present and Working as Expected in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Middleware Tab is Present and Working as Expected in RU Locale")]
        public void DSP_DownloadsAndRelatedSoftware_VerifyMiddlewareTab(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + dsp_url);

            if (util.CheckElement(driver, Elements.DSP_DownloadsAndRelatedSoftware_Tabs, 2))
            {
                int downloadsAndRelatedSoftwareTabs_count = util.GetCount(driver, Elements.DSP_DownloadsAndRelatedSoftware_Tabs);

                for (int ctr = 1; ctr <= downloadsAndRelatedSoftwareTabs_count; ctr++)
                {
                    if (util.GetText(driver, By.CssSelector("section[id$='relatedsoftware'] * div[class$='tabs']>ul>li:nth-of-type(" + ctr + ")>a")).Contains(middleware_txt))
                    {
                        //----- R4 > T4: Verify Middleware -----//

                        //--- Action: Click Middleware ---//
                        action.IClick(driver, By.CssSelector("section[id$='relatedsoftware'] * div[class$='tabs']>ul>li:nth-of-type(" + ctr + ")>a"));

                        //--- Expected Result: List of Middleware are displayed ---//
                        test.validateElementIsPresent(driver, Elements.DSP_DownloadsAndRelatedSoftware_Middleware_Tab_Links);

                        if (util.CheckElement(driver, Elements.DSP_DownloadsAndRelatedSoftware_Middleware_Tab_Links, 2))
                        {
                            //--- Expected Result: Number of available component is sum up and displayed beneath each components ---//
                            test.validateStringInstance(driver, util.GetCount(driver, Elements.DSP_DownloadsAndRelatedSoftware_Middleware_Tab_Links).ToString(), Regex.Match(util.GetText(driver, By.CssSelector("section[id$='relatedsoftware'] * div[class$='tabs']>ul>li:nth-of-type(" + ctr + ")>a")), @"\d+").Value);

                            //--- Action: Click available links ---//
                            action.IOpenLinkInNewTab(driver, Elements.DSP_DownloadsAndRelatedSoftware_Middleware_Tab_Links);
                            Thread.Sleep(1000);

                            //--- Expected Result: A DSP Software product page is opened in a new tab ---//
                            test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + dsp_url_format);
                        }

                        break;
                    }

                    if (ctr == downloadsAndRelatedSoftwareTabs_count)
                    {
                        test.validateElementIsPresent(driver, By.CssSelector("section[id$='relatedsoftware'] * div[class$='tabs']>ul>li:nth-of-type(" + (ctr + 1) + ")>a"));
                    }
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.DSP_DownloadsAndRelatedSoftware_Tabs);
            }
        }
    }
}