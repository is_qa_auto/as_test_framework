﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;

namespace AS_Test_Framework.DSP
{
    [TestFixture]
    public class DSP_SystemsRequirements : BaseSetUp
    {
        public DSP_SystemsRequirements() : base() { }

        //--- URLs ---//
        string dsp_url = "/adswt-cces";

        [Test, Category("DSP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Systems Requirements Section is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Systems Requirements Section is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Systems Requirements Section is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Systems Requirements Section is Present and Working as Expected in RU Locale")]
        public void DSP_SystemsRequirements_VerifySystemsRequirementsSection(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + dsp_url);

            //----- R7 > T1: Verify Systems Requirements section -----//

            //--- Expected Result: Content is Present (IQ-8991/AL-12205) ---//
            test.validateElementIsPresent(driver, Elements.DSP_SystemsRequirements_Content);
        }
    }
}