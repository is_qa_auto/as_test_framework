﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;

namespace AS_Test_Framework.DSP
{
    [TestFixture]
    public class DSP_Buy_BackButton : BaseSetUp
    {
        public DSP_Buy_BackButton() : base() { }

        //--- URLs ---//
        string dsp_url = "/adswt-cces";

        [Test, Category("DSP"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Back Button is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Back Button is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Back Button is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Back Button is Present and Working as Expected in RU Locale")]
        public void DSP_Buy_VerifyBackButton(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + dsp_url);

            if (util.CheckElement(driver, Elements.DSP_Buy_SelectACountry_Dd, 2))
            {
                //----- R11 > T4: Verify "Check Inventory" button underBuy section table. -----//

                action.IClick(driver, Elements.DSP_Buy_SelectACountry_Dd_Btn);
                action.IClick(driver, By.CssSelector("section[id$='buy'] * div>div:nth-of-type(1) * div[class$='country-filter'] * a[data-id='US']"));

                if (util.CheckElement(driver, Elements.DSP_Buy_CheckInventory_Btn, 2))
                {
                    //--- Action: Click Check Inventory button ---//
                    action.IClick(driver, Elements.DSP_Buy_CheckInventory_Btn);

                    //--- Expected Result: Back button should be enabled. ---//
                    test.validateElementIsEnabled(driver, Elements.DSP_Buy_Back_Btn);

                    if (driver.FindElement(Elements.DSP_Buy_Back_Btn).Enabled)
                    {
                        //--- Action: Click Back button ---//
                        action.IClick(driver, Elements.DSP_Buy_Back_Btn);

                        //--- Expected Result: Original table column should slide back. ---//
                        test.validateElementIsPresent(driver, Elements.DSP_Buy_Tbl);
                    }
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.DSP_Buy_CheckInventory_Btn);
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.DSP_Buy_SelectACountry_Dd);
            }
        }
    }
}