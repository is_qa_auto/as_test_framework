﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System;
using System.Threading;
using OpenQA.Selenium;
using System.Linq;

namespace AS_Test_Framework.Support
{

    [TestFixture]
    public class Support_EngrZone : BaseSetUp
    {
        public Support_EngrZone() : base() { }

        [Test, Category("Support"), Category("Core")]
        [TestCase("en", "Select Community", TestName = "Core - Verify ADI Engineer Zone section in support page is present in EN Locale")]
        public void Support_VerifySupportTab_TechSupport(string Locale, string SelectedValue)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/support.html");
            test.validateElementIsPresent(driver, Elements.Support_AskEngineerZone_Dropdown);
            test.validateStringIsCorrect(driver, By.CssSelector("div[class*='dropdown ez-dropdown']>a>span"), SelectedValue);
            action.IClick(driver, Elements.Support_AskEngineerZone_Dropdown);
            test.validateElementIsPresent(driver, Elements.SupportTab_EZ_Community_DropDown_List);
            action.IClick(driver, By.CssSelector("div[class='dropdown ez-dropdown open']>ul[class='dropdown-menu support-dropdown-community']>li>a"));
            test.validateScreenByUrl(driver, "https://ez.analog.com/");
            driver.Navigate().Back();
            test.validateElementIsPresent(driver, Elements.Support_AskEngineerZone_SupportLink);
            action.IClick(driver, Elements.Support_AskEngineerZone_SupportLink);
            test.validateStringInstance(driver, driver.Url, Locale + "/about-adi/landing-pages/001/ADI_Community_Weblog_Terms_of_Use.html");
            //action.Navigate(driver, Configuration.Env_Url + Locale + "/index.html");
            //action.IClick(driver, Elements.MainNavigation_SupportTab);

            //test.validateElementIsPresent(driver, Elements.Support_EngineerZone_RegisterLink);
            //test.validateElementIsPresent(driver, Elements.Support_EngineerZone_PartnerLink);

            //action.IClick(driver, Elements.Support_EngineerZone_RegisterLink);
            //test.validateScreenByUrl(driver, Configuration.Env_Url.Replace("www", "my") + Locale + "/app/registration");
            //action.Navigate(driver, Configuration.Env_Url + Locale + "/index.html");
            //action.IClick(driver, Elements.Support_EngineerZone_PartnerLink);
            //test.validateStringInstance(driver, driver.Url, "analog.com/partnerzone");

        }

    }
}
