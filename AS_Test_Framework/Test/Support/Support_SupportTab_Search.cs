﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System;
using System.Threading;
using OpenQA.Selenium;
using System.Linq;

namespace AS_Test_Framework.Support
{

    [TestFixture]
    public class Support_SupportTab_Search : BaseSetUp
    {
        public Support_SupportTab_Search() : base() { }

        [Test, Category("Support"), Category("Core")]
        [TestCase("en", TestName = "Core - Verify search within support functionality in EN Locale", Category = "Smoke_ADIWeb")]
        [TestCase("cn", TestName = "Core - Verify search within support functionality in CN Locale", Category = "Smoke_ADIWeb")]
        [TestCase("jp", TestName = "Core - Verify search within support functionality in JP Locale", Category = "Smoke_ADIWeb")]
        [TestCase("ru", TestName = "Core - Verify search within support functionality in RU Locale")]
        public void Support_VerifySupportTab_Search(string Locale)
        {
            string initial_steps = "1. Go to " + Configuration.Env_Url + Locale + "/index.html";
            string scenario = "";

            action.INavigate(driver, Configuration.Env_Url + Locale + "/index.html");
            action.IClick(driver, Elements.MainNavigation_SupportTab);
            scenario = "Verify that search input box is present in support fly-out";
            test.validateElementIsPresentv2(driver, Elements.Support_Search_InputBox, scenario, initial_steps + "<br>2. Click on support tab in main menu navigation");

            scenario = "Verify that search submit button is present in support fly-out";
            test.validateElementIsPresentv2(driver, Elements.Support_Submit_Button, scenario, initial_steps + "<br>2. Click on support tab in main menu navigation");

            action.IClick(driver, Elements.Support_Submit_Button);
            scenario = "Verify that user will be redirected to support search result when clicking support search button";
            test.validateScreenByUrlv2(driver, Configuration.Env_Url + Locale+ "/support/searchsupport.html?q=", scenario, initial_steps + "<br>2. Click on support tab in main menu navigation<br>3. Click search submit button");

            scenario = "Verify that \"Please initiate new search\" will be displayed in support search result page";
            test.validateElementIsPresentv2(driver, Elements.GlobalSearchResults_NoSearchResults_Txt, scenario, initial_steps + "<br>2. Click on support tab in main menu navigation<br>3. Click search submit button");

            action.IClick(driver, Elements.MainNavigation_SupportTab);
            action.IType(driver, Elements.Support_Search_InputBox, "Analog");
            action.IClick(driver, Elements.Support_Submit_Button);
            Thread.Sleep(5000);
            scenario = "Verify that search result is present when searching in support using valid keyword";
            test.validateElementIsPresentv2(driver, Elements.GlobalSearchResults_SearchResultList, scenario, initial_steps + "<br>2. Click on support tab in main menu navigation<br>3. Enter valid keyword - \"Analog\" in search input box<br>4. Click search submit button");

            action.IClick(driver, Elements.MainNavigation_SupportTab);
            action.IType(driver, Elements.Support_Search_InputBox, "SADSA");
            action.IClick(driver, Elements.Support_Submit_Button);
            Thread.Sleep(5000);
            scenario = "Verify that \"No results found\" text is displayed when searching using invalid keyword";
            test.validateElementIsPresentv2(driver, Elements.GlobalSearchResults_InvalidSearchResults_Txt_Line1, scenario, initial_steps + "<br>2. Click on support tab in main menu navigation<br>3. Enter invalid keyword - \"SADSA\" in search input box<br>4. Click search submit button");
        }
    }
}
