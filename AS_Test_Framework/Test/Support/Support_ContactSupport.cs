﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System;
using System.Threading;
using OpenQA.Selenium;
using System.Linq;

namespace AS_Test_Framework.Support
{

    [TestFixture]
    public class Support_ContactSupport : BaseSetUp
    {
        public Support_ContactSupport() : base() { }

        [Test, Category("Support"), Category("Core")]
        [TestCase("en", TestName = "Core - Verify that list of regions is displayed in Regional Contact Info for EN Locale")]
        public void Support_VerifyContactSuport(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/support/technical-support.html");
            test.validateColorIsCorrect(driver, util.GetCssValue(driver, By.CssSelector("div[class='col-md-3 column-secondary hidden-xs']"), "background-color"), "#fafafa");
            test.validateElementIsPresent(driver, Elements.Support_Regional_List);
            for(int x = 1; util.CheckElement(driver, By.CssSelector("ul[class='map-space regional-list']>li:nth-child(" + x + ")"), 5); x++)
            { 
                action.IClick(driver, By.CssSelector("ul[class='map-space regional-list']>li:nth-child(" + x + ")>a"));
                Thread.Sleep(2000);
                test.validateElementIsPresent(driver, Elements.Support_Regional_Result);
                if (!util.GetText(driver, By.CssSelector("ul[class='map-space regional-list']>li:nth-child(" + x + ")>a")).Equals("Japan")) {
                    if (util.GetText(driver, By.CssSelector("ul[class='map-space regional-list']>li:nth-child(" + x + ")>a")).Equals("China")) {
                        test.validateStringIsCorrect(driver, By.CssSelector("div[id='results'] * div[class='contact']>div[class='email']>a"), "china.support@analog.com");
                        test.validateStringIsCorrect(driver, By.CssSelector("div[id='results'] * div[class='contact']:nth-of-type(2)>div[class='email']>a"), "processor.china@analog.com");
                    }
                    else if (util.GetText(driver, By.CssSelector("ul[class='map-space regional-list']>li:nth-child(" + x + ")>a")).Equals("Asia Pacific"))
                    {
                        test.validateStringIsCorrect(driver, By.CssSelector("div[id='results'] * div[class='contact']>div[class='email']>a"), "cic.asia@analog.com");
                    }
                    else if (util.GetText(driver, By.CssSelector("ul[class='map-space regional-list']>li:nth-child(" + x + ")>a")).Equals(" North America"))
                    {
                        test.validateStringIsCorrect(driver, By.CssSelector("div[id='results'] * div[class='contact']>div[class='email']>a"), "cic.americas@analog.com");
                    }
                    test.validateElementIsPresent(driver, By.CssSelector("div[id='results'] a[name='EngineerZone']"));
                    action.IClick(driver, By.CssSelector("div[id='results'] a[name='EngineerZone']"));
                    test.validateStringInstance(driver, driver.Url, "ez.analog.com/");
                    driver.Navigate().Back();
                    if (util.GetText(driver, By.CssSelector("ul[class='map-space regional-list']>li:nth-child(" + x + ")>a")).Equals("China"))
                    {
                        action.IClick(driver, By.CssSelector("ul[class='map-space regional-list']>li:nth-child(" + x + ")>a"));
                        test.validateElementIsPresent(driver, By.CssSelector("div[id='results'] a[name='Chinese_website']"));
                        action.IClick(driver, By.CssSelector("div[id='results'] a[name='Chinese_website']"));
                        test.validateStringInstance(driver, driver.Url, "analog.com/cn/support/landing-pages/002/cic-support-portal.html");
                        driver.Navigate().Back();
                    }
                    
                }
            }
        }

        [Test, Category("Support"), Category("Core")]
        [TestCase("en", TestName = "Core - Validate that the user can send Customer Service inquiry through the \"Contact Support\" box")]
        public void Support_VerifyContactSuport1(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/support.html");
            action.IClick(driver, By.CssSelector("div[name='adi_tabcontrol']>div>ul>li:nth-child(3)>div>div:nth-child(2)>a"));
            test.validateScreenByUrl(driver, Configuration.Env_Url.Replace("www", "form").Replace("cldnet","corpnt") + "Form_Pages/support/customerService.aspx");
        }

        [Test, Category("Support"), Category("Core")]
        [TestCase("en", TestName = "Core - Validate that the user can send Technical inquiry through the \"Contact Support\" box")]
        public void Support_VerifyContactSuport2(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/support.html");
            action.IClick(driver, By.CssSelector("div[name='adi_tabcontrol']>div>ul>li:nth-child(3)>div>div:nth-child(3)>a"));
            test.validateScreenByUrl(driver, Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + "form_pages/support/integrated/techsupport.aspx");
        }

        [Test, Category("Support"), Category("Core")]
        [TestCase("en", TestName = "Core - Validate the Contact Customer Service option in the Customer Service Landing Page")]
        public void Support_VerifyContactSuport3(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/support/customer-service-resources.html");
            test.validateElementIsPresent(driver, Elements.Support_ContactSupport_SubmitBtn);
            action.IClick(driver, Elements.Support_ContactSupport_SubmitBtn);
            driver.SwitchTo().Window(driver.WindowHandles.Last());
            test.validateStringInstance(driver, driver.Url, "Form_Pages/support/customerService.aspx");
        }

        [Test, Category("Support"), Category("Core")]
        [TestCase("en", TestName = "Core - Validate the background-color of Box header and search button in Product Model Search is correct")]
        public void Support_VerifyContactSuport4(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/support/customer-service-resources/customer-service/view-export-classification.html");
            driver.SwitchTo().Frame(driver.FindElement(By.CssSelector("div[id='rte-body'] iframe")));
            test.validateColorIsCorrect(driver, util.GetCssValue(driver, By.CssSelector("div[id='productPanel']>div[class='panel-heading']"), "background-color"), "#25a449");
            test.validateColorIsCorrect(driver, util.GetCssValue(driver, By.CssSelector("input[id='Button2']"), "background-color"), "#25a449");
        }

        [Test, Category("Support"), Category("Core")]
        [TestCase("en", TestName = "Core - Verify that the links under Sales and Customer Service is present in the central nav component")]
        public void Support_VerifyContactSuport5(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/support/customer-service-resources.html");
            test.validateElementIsPresent(driver, By.CssSelector("ul[class='nav nav-list mid-col-text']>li:nth-child(1)>ul>li>a"));
            test.validateElementIsPresent(driver, By.CssSelector("ul[class='nav nav-list mid-col-text']>li:nth-child(2)>ul>li>a"));
            action.Navigate(driver, Configuration.Env_Url + Locale + "/support/technical-support.html");
            test.validateElementIsPresent(driver, By.CssSelector("ul[class='nav nav-list mid-col-text']>li:nth-child(2)>ul>li>a"));
        }


    }
}
