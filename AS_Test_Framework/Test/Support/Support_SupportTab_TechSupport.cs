﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System;
using System.Threading;
using OpenQA.Selenium;
using System.Linq;

namespace AS_Test_Framework.Support
{

    [TestFixture]
    public class Support_SupportTab_TechSupport : BaseSetUp
    {
        public Support_SupportTab_TechSupport() : base() { }

        [Test, Category("Support"), Category("Core")]
        [TestCase("en", "ADI Technical Support", TestName = "Core - Verify Tech Support Section in support tab is present in EN Locale")]
        [TestCase("cn", "ADI 提交您的问题", TestName = "Core - Verify Tech Support Section in support tab is present in CN Locale")]
        [TestCase("jp", "ADI 技術的なお問合せ", TestName = "Core - Verify Tech Support Section in support tab is present in JP Locale")]
        [TestCase("ru", "Техническая поддержка", TestName = "Core - Verify Tech Support Section in support tab is present in RU Locale")]
        public void Support_VerifySupportTab_TechSupport(string Locale, string TechHeader)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/index.html");
            action.IClick(driver, Elements.MainNavigation_SupportTab);
            test.validateStringIsCorrect(driver, By.CssSelector("div[class='support menu content expanded']>section[class='support-section']>div[class='row featured-section bg-grey'] div[class='container'] div[class='col-md-4']:nth-of-type(2) h3>a"), TechHeader);
            /*********Header lInk**********/
            action.IClick(driver, By.CssSelector("div[class='support menu content expanded']>section[class='support-section']>div[class='row featured-section bg-grey'] div[class='container'] div[class='col-md-4']:nth-of-type(2) h3>a"));
            if (Locale.Equals("cn"))
            {
                test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/support/cn_tech-support.html");
            }
            else if (Locale.Equals("jp"))
            {
                test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/support/jp_tech-support.html");
            }
            else
            {
                test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/support/technical-support.html");
            }
            test.validateColorIsCorrect(driver, util.GetCssValue(driver, Elements.MainNavigation_SupportTab, "background-color"), "#25a449");
            action.IClick(driver, Elements.MainNavigation_ApplicationsTab);
            test.validateColorIsCorrect(driver, util.GetCssValue(driver, Elements.MainNavigation_SupportTab, "background-color"), "#25a449");

            /*********Ask Technical Support***********/
            action.IClick(driver, Elements.MainNavigation_SupportTab);
            action.IClick(driver, By.CssSelector("div[class='support menu content expanded']>section[class='support-section']>div[class='row featured-section bg-grey'] div[class='container'] div[class='col-md-4']:nth-of-type(2) ul>li:nth-child(1)"));
            if (Locale.Equals("cn"))
            {
                test.validateStringInstance(driver, driver.Url, "form_pages/support/integrated/Techsupport_china.aspx?locale=zh");
            }
            else if (Locale.Equals("jp"))
            {
                test.validateStringInstance(driver, driver.Url, "jp/support/jp_tech-support.html");
            }
            else
            {
                test.validateStringInstance(driver, driver.Url, "form_pages/support/integrated/techsupport.aspx");
            }


            /*********Regional Support***********/
            action.Navigate(driver, Configuration.Env_Url + Locale + "/index.html");
            action.IClick(driver, Elements.MainNavigation_SupportTab);
            action.IClick(driver, By.CssSelector("div[class='support menu content expanded']>section[class='support-section']>div[class='row featured-section bg-grey'] div[class='container'] div[class='col-md-4']:nth-of-type(2) ul>li:nth-child(2)"));

            if (Locale.Equals("cn"))
            {
                test.validateStringInstance(driver, driver.Url, "/support/cn_tech-support.html");
            }
            else if (Locale.Equals("jp"))
            {
                test.validateStringInstance(driver, driver.Url, "/support/jp_tech-support.html");
            }
            else
            {
                test.validateStringInstance(driver, driver.Url, "/support/technical-support.html");
            }
        }

        [Test, Category("Support"), Category("Core")]
        [TestCase("en",  TestName = "Core - Verify the Product Security Response Center page in EN Locale")]
        public void Support_VerifySupportTab_TechSupport1(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/support/technical-support/product-security-response-center.html");
            test.validateString(driver, "Product Security Response | Analog Devices", driver.Title);
            test.validateElementIsPresent(driver, By.CssSelector("div[class='header-main']>h1"));
        }

        [Test, Category("Support"), Category("Core")]
        [TestCase("en", TestName = "Core - Verify Quality and Reliability section in EN Locale")]
        [TestCase("cn", TestName = "Core - Verify Quality and Reliability section in CN Locale")]
        [TestCase("jp", TestName = "Core - Verify Quality and Reliability section in JP Locale")]
        [TestCase("ru", TestName = "Core - Verify Quality and Reliability section in RU Locale")]
        public void Support_VerifySupportTab_TechSupport2(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/index.html");
            action.IClick(driver, Elements.MainNavigation_SupportTab);
            action.IClick(driver, By.CssSelector("div[class='row featured-section-sub'] div[class='col-md-4']:nth-of-type(1) h3>a"));
            test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + "/about-adi/quality-reliability.html");
            action.IClick(driver, Elements.MainNavigation_SupportTab);
            string[] sublink_url =  { "/about-adi/quality-reliability/material-declarations.html",
                                      "/design-center/packaging-quality-symbols-footprints/package-index.html",
                                      "/about-adi/quality-reliability/reliability-data.html"
                                    };
            for (int x = 1, y=0; util.CheckElement(driver, By.CssSelector("div[class='row featured-section-sub'] div[class='col-md-4']:nth-of-type(1) ul>li:nth-child(" + x + ")"), 1); x++, y++) {
                
                action.IClick(driver, By.CssSelector("div[class='row featured-section-sub'] div[class='col-md-4']:nth-of-type(1) ul>li:nth-child(" + x + ")>a"));
                test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + sublink_url[y]);
                action.IClick(driver, Elements.MainNavigation_SupportTab);
            }
        }


        [Test, Category("Support"), Category("Core")]
        [TestCase("en", TestName = "Core - Verify Sales section in EN Locale")]
        [TestCase("cn", TestName = "Core - Verify Sales section in CN Locale")]
        [TestCase("jp", TestName = "Core - Verify Sales section in JP Locale")]
        [TestCase("ru", TestName = "Core - Verify Sales section in RU Locale")]
        public void Support_VerifySupportTab_TechSupport3(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/index.html");
            action.IClick(driver, Elements.MainNavigation_SupportTab);
            //action.IClick(driver, By.CssSelector("div[class='row featured-section-sub'] div[class='col-md-4']:nth-of-type(2) h3>a"));
            //test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + "/support/customer-service-resources/sales.html");
            //action.IClick(driver, Elements.MainNavigation_SupportTab);
            string[] sublink_url_enru =  {
                                            "/support/customer-service-resources/sales/buy-products.html",
                                            "/support/customer-service-resources/sales/sample-products.html",
                                            "/support/customer-service-resources/sales/find-sale-office-distributor.html",
                                            "/support/customer-service-resources/sales/pricing-availability-information.html",
                                            "/support/customer-service-resources/sales/product-life-cycle-information.html",
                                            "/support/customer-service-resources/sales/terms-and-conditions.html"
                                        };

            string[] sublink_url_cn =  {
                                            "/support/landing-pages/002/china-bol-activity.html",
                                            "/support/customer-service-resources/sales/buy-products.html",
                                            "/support/customer-service-resources/sales/sample-products.html",
                                            "/about-adi/landing-pages/002/sales-and-distributors.html",
                                            "/support/customer-service-resources/sales/pricing-availability-information.html",
                                            "/support/customer-service-resources/sales/product-life-cycle-information.html",
                                            "/support/customer-service-resources/sales/terms-and-conditions.html"
                                        };

            string[] sublink_url_jp =  {
                                            "/support/customer-service-resources/sales/buy-products.html",
                                            "/support/customer-service-resources/sales/sample-products.html",
                                            "/support/landing-pages/003/jp-sales-and-disti-support.html",
                                            "/support/customer-service-resources/sales/pricing-availability-information.html",
                                            "/support/customer-service-resources/sales/product-life-cycle-information.html",
                                            "/support/customer-service-resources/sales/terms-and-conditions.html"
                                        };

            for (int x = 1, y = 0; util.CheckElement(driver, By.CssSelector("div[class='row featured-section-sub'] div[class='col-md-4']:nth-of-type(2) ul>li:nth-child(" + x + ")"), 1); x++, y++)
            {

                action.IClick(driver, By.CssSelector("div[class='row featured-section-sub'] div[class='col-md-4']:nth-of-type(2) ul>li:nth-child(" + x + ")>a"));
                if (Locale.Equals("en") || Locale.Equals("ru"))
                {
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + sublink_url_enru[y]);
                }
                else if ((Locale.Equals("cn")))
                {
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + sublink_url_cn[y]);
                }
                else
                {
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + sublink_url_jp[y]);
                }
                action.IClick(driver, Elements.MainNavigation_SupportTab);
            }
        }

        [Test, Category("Support"), Category("Core")]
        [TestCase("en", TestName = "Core - Verify Customer Service section in EN Locale")]
        [TestCase("cn", TestName = "Core - Verify Customer Service section in CN Locale")]
        [TestCase("jp", TestName = "Core - Verify Customer Service section in JP Locale")]
        [TestCase("ru", TestName = "Core - Verify Customer Service section in RU Locale")]
        public void Support_VerifySupportTab_TechSupport4(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/index.html");
            action.IClick(driver, Elements.MainNavigation_SupportTab);
           
            string[] sublink_url_en =  {
                                            "/support/customer-service-resources/customer-service/view-export-classification.html",
                                            "",
                                            "",
                                            "",
                                            "/support/customer-service-resources/customer-service/view-shipping-options-rates.html",
                                             "/support/customer-service-resources/sales/terms-and-conditions.html",
                                            "",
                                            "Form_Pages/support/customerService.aspx",
                                            ""                         
                                        };

            for (int x = 1, y = 0; util.CheckElement(driver, By.CssSelector("div[class='row featured-section-sub'] div[class='col-md-4']:nth-of-type(3) ul>li:nth-child(" + x + ")"), 1); x++, y++)
            {
                if (!util.ReturnAttribute(driver, By.CssSelector("div[class='row featured-section-sub'] div[class='col-md-4']:nth-of-type(3) ul>li:nth-child(" + x + ")>a"), "href").Contains("https://my"))
                {
                    string Url = util.ReturnAttribute(driver, By.CssSelector("div[class='row featured-section-sub'] div[class='col-md-4']:nth-of-type(3) ul>li:nth-child(" + x + ")>a"), "href");
                    action.IClick(driver, By.CssSelector("div[class='row featured-section-sub'] div[class='col-md-4']:nth-of-type(3) ul>li:nth-child(" + x + ")>a"));
                    if (Url.Contains("request-return.html") || Url.Contains("https://form") || Url.Contains("submit-customer-service-request.html"))
                    {
                        if (Locale.Equals("jp"))
                        {
                            test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + "/support/customer-service-resources/customer-service/submit-customer-service-request.html");
                        }
                        else
                        {
                            test.validateStringInstance(driver, driver.Url.ToLower(), sublink_url_en[y].ToLower());
                        }
                    }
                    else { 
                        test.validateStringInstance(driver, driver.Url.ToLower(), "analog.com/" + Locale + sublink_url_en[y].ToLower());
                    }
                    action.IClick(driver, Elements.MainNavigation_SupportTab);

                }
            }
        }
    }
}
