﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System;
using System.Threading;
using OpenQA.Selenium;
using System.Linq;

namespace AS_Test_Framework.Support
{

    [TestFixture]
    public class Support_SupportTab_EngrZone : BaseSetUp
    {
        public Support_SupportTab_EngrZone() : base() { }

        [Test, Category("Support"), Category("Core")]
        [TestCase("en", TestName = "Core - Verify ADI Engineer Zone section in support tab is present in EN Locale")]
        [TestCase("cn", TestName = "Core - Verify ADI Engineer Zone section in support tab is present in CN Locale")]
        [TestCase("jp", TestName = "Core - Verify ADI Engineer Zone section in support tab is present in JP Locale")]
        [TestCase("ru", TestName = "Core - Verify ADI Engineer Zone section in support tab is present in RU Locale")]
        public void Support_VerifySupportTab_TechSupport(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/index.html");
            action.IClick(driver, Elements.MainNavigation_SupportTab);
            test.validateElementIsPresent(driver, Elements.Support_EngineerZone_Header);
            action.IClick(driver, Elements.Support_EngineerZone_Header);
            if (Locale.Equals("cn"))
            {
                test.validateScreenByUrl(driver, "https://ez.analog.com/cn/");
            }
            else {
                test.validateScreenByUrl(driver, "https://ez.analog.com/");
            }
            action.Navigate(driver, Configuration.Env_Url + Locale + "/index.html");
            action.IClick(driver, Elements.MainNavigation_SupportTab);
            test.validateElementIsPresent(driver, Elements.SupportTab_EZ_Community_DropDown);
            action.IClick(driver, Elements.SupportTab_EZ_Community_DropDown);
            test.validateElementIsPresent(driver, Elements.SupportTab_EZ_Community_DropDown_List);
            action.IClick(driver, Elements.SupportTab_EZ_Community_DropDown);
            test.validateElementIsPresent(driver, Elements.Support_EngineerZone_SupportLink);
            action.IClick(driver, Elements.Support_EngineerZone_SupportLink);
            test.validateStringInstance(driver, driver.Url, Locale + "/about-adi/landing-pages/001/adi_community_weblog_terms_of_use.html");
            action.Navigate(driver, Configuration.Env_Url + Locale + "/index.html");
            action.IClick(driver, Elements.MainNavigation_SupportTab);

            test.validateElementIsPresent(driver, Elements.Support_EngineerZone_RegisterLink);
            test.validateElementIsPresent(driver, Elements.Support_EngineerZone_PartnerLink);
            action.IClick(driver, Elements.Support_EngineerZone_RegisterLink); if (Locale.Equals("cn"))
            {
                test.validateStringInstance(driver, driver.Url, "zh/app/registration");
            }
            else
            {
                test.validateStringInstance(driver, driver.Url, Locale + "/app/registration");
            }
            action.Navigate(driver, Configuration.Env_Url + Locale + "/index.html");
            action.IClick(driver, Elements.MainNavigation_SupportTab);
            action.IClick(driver, Elements.Support_EngineerZone_PartnerLink);
            test.validateStringInstance(driver, driver.Url, "ez.analog.com/partnerzone");

        }

    }
}
