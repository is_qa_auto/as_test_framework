﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System;
using System.Threading;
using OpenQA.Selenium;
using System.Linq;

namespace AS_Test_Framework.Support
{

    [TestFixture]
    public class Support_SupportTab_CustomerSupport : BaseSetUp
    {
        public Support_SupportTab_CustomerSupport() : base() { }

        [Test, Category("Support"), Category("Core")]
        [TestCase("en", "ADI Customer Service Center", TestName = "Core - Verify ADI Customer Service section in support tab is present in EN Locale")]
        [TestCase("cn", "ADI 客户服务中心", TestName = "Core - Verify ADI Customer Service section in support tab is present in CN Locale")]
        [TestCase("jp", "カスタマーサービス", TestName = "Core - Verify ADI Customer Service section in support tab is present in JP Locale")]
        [TestCase("ru", "Центр поддержки заказчиков ADI", TestName = "Core - Verify ADI Customer Service section in support tab is present in RU Locale")]
        public void Support_VerifySupportTab_CustomerSupport(string Locale, string CustomerSupportHeader)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/index.html");
            action.IClick(driver, Elements.MainNavigation_SupportTab);
            test.validateStringIsCorrect(driver, By.CssSelector("div[class='support menu content expanded']>section[class='support-section']>div[class='row featured-section bg-grey'] div[class='container'] div[class='col-md-4']:nth-of-type(4) h3>a"), CustomerSupportHeader);
            test.validateElementIsPresent(driver, By.CssSelector("div[class='support menu content expanded']>section[class='support-section']>div[class='row featured-section bg-grey'] div[class='container'] div[class='col-md-4']:nth-of-type(4) ul>li>a"));

            action.IClick(driver, By.CssSelector("div[class='support menu content expanded']>section[class='support-section']>div[class='row featured-section bg-grey'] div[class='container'] div[class='col-md-4']:nth-of-type(4) h3>a"));
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/support/customer-service-resources.html");

            action.IClick(driver, Elements.MainNavigation_SupportTab);
            action.IClick(driver, By.CssSelector("div[class='support menu content expanded']>section[class='support-section']>div[class='row featured-section bg-grey'] div[class='container'] div[class='col-md-4']:nth-of-type(4) ul>li>a"));
            if (Locale.Equals("cn"))
            {
                test.validateScreenByUrl(driver, Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + "Form_Pages/support/CustomerService_China.aspx");
            }
            else if (Locale.Equals("jp"))
            {
                test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/support/customer-service-resources/customer-service/submit-customer-service-request.html");
            }
            else
            {
                test.validateScreenByUrl(driver, Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + "Form_Pages/support/customerService.aspx");
            }
        }

    }
}

