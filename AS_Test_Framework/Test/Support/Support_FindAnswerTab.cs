﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System;
using System.Threading;
using OpenQA.Selenium;
using System.Linq;

namespace AS_Test_Framework.Support
{

    [TestFixture]
    public class Support_FindAnswerTab : BaseSetUp
    {
        public Support_FindAnswerTab() : base() { }

        [Test, Category("Support"), Category("Core")]
        [TestCase("en", TestName = "Core - Verify Find Answer Tab in Support page is present and working in EN Locale")]
        public void Support_VerifySupport_FindAnswerTab(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/support.html");
            action.IType(driver, Elements.Support_FindAnswerTab_SearchBox, "Analog");
            Thread.Sleep(2000);
            //used the same element in reference design as it has the same locator.
            test.validateElementIsPresent(driver, Elements.DesignCenter_ReferenceDesigns_Search_Sec_SearchByKeywordOrDesignNumber_AutoSuggest);
            action.IClick(driver, Elements.Support_FindAnswerTab_SearchButton);
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/support/searchsupport.html?q=Analog");
            test.validateElementIsPresent(driver, Elements.GlobalSearchResults_SearchResultList);
            driver.Navigate().Back();
            action.IType(driver, Elements.Support_FindAnswerTab_SearchBox, "Analog");
            Thread.Sleep(3000);
            action.IMouseOverTo(driver, By.CssSelector("div[class='tt-menu tt-open']>div>p>a"));
            action.IClick(driver, By.CssSelector("div[class='tt-menu tt-open']>div>p>span"));
            test.validateStringInstance(driver, driver.Url, Configuration.Env_Url + Locale + "/support/searchsupport.html?q=");
            test.validateElementIsPresent(driver, Elements.GlobalSearchResults_SearchResultList);
           
        }

    }
}

