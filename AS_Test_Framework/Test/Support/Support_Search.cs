﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System;
using System.Threading;
using OpenQA.Selenium;
using System.Linq;

namespace AS_Test_Framework.Support
{

    [TestFixture]
    public class Support_Search : BaseSetUp
    {
        public Support_Search() : base() { }

        [Test, Category("Support"), Category("Core")]
        [TestCase("en", TestName = "Core - Validate that the search bar is functional in EN Locale")]
        public void Support_VerifySearch(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/support/searchsupport.html");
            Thread.Sleep(2000);
            action.IType(driver, Elements.GlobalSearchResults_Search_Txtbox, "amplifiers");
            action.IClick(driver, Elements.GlobalSearchResults_Search_Btn);
            test.validateElementIsPresent(driver, Elements.GlobalSearchResults_SearchResultList);
            test.validateColorIsCorrect(driver, util.GetCssValue(driver, By.CssSelector("button[class='btn btn-primary button-main']"), "background-color"), "#25a449");
            test.validateColorIsCorrect(driver, util.GetCssValue(driver, By.CssSelector("span[class='next']>a"), "background-color"), "#25a449");
            //test.validateColorIsCorrect(driver, util.GetCssValue(driver, By.CssSelector("div[name='adi_threecolumn_leftnavigation_section']>ul[class='nav left_nav']>li[class='active']"), "background-color"), "#25a449");
            action.IClick(driver, By.CssSelector("span[class='next']>a"));
            test.validateColorIsCorrect(driver, util.GetCssValue(driver, By.CssSelector("span[class='prev ']>a"), "background-color"), "#25a449");
        }


        [Test, Category("Support"), Category("Core")]
        [TestCase("en", TestName = "Core - Verify that the Sort menu is present on the search page in EN Locale")]
        public void Support_VerifySearch1(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/support/searchsupport.html");
            action.IType(driver, Elements.GlobalSearchResults_Search_Txtbox, "amplifiers");
            action.IClick(driver, Elements.GlobalSearchResults_Search_Btn);
            test.validateElementIsPresent(driver, Elements.GlobalSearchResults_Sort_Menu);
            action.IClick(driver, Elements.GlobalSearchResults_Sort_Menu);
            test.validateElementIsPresent(driver, Elements.GlobalSearchResults_Sort_Menu_Newest);
            test.validateElementIsPresent(driver, Elements.GlobalSearchResults_Sort_Menu_Oldest);
            test.validateElementIsPresent(driver, Elements.GlobalSearchResults_Sort_Menu_Relevancy);
        }

        [Test, Category("Support"), Category("Core")]
        [TestCase("en", TestName = "Core - Verify that \"Show results instead for\" is being displayed in EN Locale")]
        //[TestCase("cn", TestName = "Core - Verify that \"Show results instead for\" is being displayed in CN Locale")]
        //[TestCase("jp", TestName = "Core - Verify that \"Show results instead for\" is being displayed in JP Locale")]
        //[TestCase("ru", TestName = "Core - Verify that \"Show results instead for\" is being displayed in RU Locale")]
        public void Support_VerifySearch2(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/support/searchsupport.html");
            action.IType(driver, Elements.GlobalSearchResults_Search_Txtbox, "adsp-ac582");
            action.IClick(driver, Elements.GlobalSearchResults_Search_Btn);
            test.validateElementIsPresent(driver, Elements.GlobalSearchResults_ShowingResultsFor_Txt);
            test.validateElementIsPresent(driver, Elements.GlobalSearchResults_ShowResultsInsteadFor_Txt);
        }

        [Test, Category("Support"), Category("Core")]
        [TestCase("en", "EngineerZone", TestName = "Core - Verify that \"EngineerZone\" label is present at the header of search result item in EN Locale")]
        //[TestCase("cn", "中文技术论坛", TestName = "Core - Verify that \"Show results instead for\" is being displayed in CN Locale")]
        [TestCase("jp", "EngineerZone", TestName = "Core - Verify that \"EngineerZone\" label is present at the header of search result item in JP Locale")]
        [TestCase("ru", "EngineerZone", TestName = "Core - Verify that \"EngineerZone\" label is present at the header of search result item in RU Locale")]
        public void Support_VerifySearch3(string Locale, string EngineerZoneLabel)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/support/searchsupport.html");
            action.IType(driver, Elements.GlobalSearchResults_Search_Txtbox, "*");
            action.IClick(driver, Elements.GlobalSearchResults_Search_Btn);
            action.IClick(driver, By.CssSelector("li[data-facetcategory='resource_type_fac_s']>a"));
            action.IClick(driver, By.CssSelector("li[data-facetcategory='resource_type_fac_s']>ul>li[class=' checkbox']>a"));
            test.validateStringIsCorrect(driver, By.CssSelector("div[class='search-results-items']>div>span[class='title-category']"), EngineerZoneLabel);
        }
    }
}
