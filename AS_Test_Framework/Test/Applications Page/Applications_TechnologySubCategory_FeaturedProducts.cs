﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Collections.Generic;

namespace AS_Test_Framework.Applications
{

    [TestFixture]
    public class Applications_TechnologySubCategory_FeaturedProducts : BaseSetUp
    {
        public Applications_TechnologySubCategory_FeaturedProducts() : base() { }

        [Test, Category("Applications"), Category("Core")]
        [TestCase("en", "Featured Products", TestName = "Verify featured product is present in Technology - Sub Category for EN Locale")]
        [TestCase("cn", "特色产品", TestName = "Verify featured product is present in Technology - Sub Category for CN Locale")]
        [TestCase("jp", "主な製品", TestName = "Verify featured product is present in Technology - Sub Category for JP Locale")]
        [TestCase("ru", "Специальные продукты", TestName = "Verify featured product is present in Technology - Sub Category for RU Locale")]
        public void ApplicationsPage_VerifyTechnologyFeaturedProducts_1(string Locale, string FeaturedProductsLabel)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/applications/technology/a2b-audio-bus.html");
            test.validateStringIsCorrect(driver, Elements.Applications_Technology_FeaturedProduct_Header, FeaturedProductsLabel);
            test.validateElementIsPresent(driver, By.CssSelector("div[id='section-heading0'] div[class='adi-accordion alternate-state  ']"));
            test.validateElementIsPresent(driver, By.CssSelector("div[id='section-heading0'] div[class='adi-accordion alternate-state  '] div[class='section-bar collapsed'] h3"));
            test.validateElementIsPresent(driver, By.CssSelector("div[id='section-heading0'] div[class='adi-accordion alternate-state  '] div[class='section-bar collapsed'] span[class*='textblock']"));
        }
    }
}