﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Collections.Generic;
using System.Threading;
using System.Linq;

namespace AS_Test_Framework.Applications
{

    [TestFixture]
    public class Applications_SignalChains_Diagram : BaseSetUp
    {
        public Applications_SignalChains_Diagram() : base() { }

        [Test, Category("Applications"), Category("Core")]
        [TestCase("en", TestName = "Verify parts used table will be displayed when clicking signal chain parts in EN Locale")]
        [TestCase("cn", TestName = "Verify parts used table will be displayed when clicking signal chain parts in CN Locale")]
        [TestCase("jp", TestName = "Verify parts used table will be displayed when clicking signal chain parts in JP Locale")]
        [TestCase("ru", TestName = "Verify parts used table will be displayed when clicking signal chain parts in RU Locale")]
        public void ApplicationsPage_VerifySignalChainsDiagram1(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url +  Locale + "/applications/markets/instrumentation-and-measurement-pavilion-home/electronic-test-and-measurement/precision-measurement.html");
            ((IJavaScriptExecutor)driver).ExecuteScript("arguments[0].click();", driver.FindElement(By.CssSelector("area[alt^='integrated-smu_pmu_subsystem']")));
            Thread.Sleep(3000);
            test.validateElementIsPresent(driver, Elements.Applications_SignalChain_SelectedPart_Title);
            test.validateElementIsPresent(driver, Elements.Applications_SignalChain_SelectedPart_Table);
            
        }

        [Test, Category("Applications"), Category("Core")]
        [TestCase("en", TestName = "Verify if user can expand/collapsed parts used table in EN Locale")]
        [TestCase("cn", TestName = "Verify if user can expand/collapsed parts used table in CN Locale")]
        [TestCase("jp", TestName = "Verify if user can expand/collapsed parts used table in JP Locale")]
        [TestCase("ru", TestName = "Verify if user can expand/collapsed parts used table in RU Locale")]
        public void ApplicationsPage_VerifySignalChainsDiagram2(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/applications/markets/instrumentation-and-measurement-pavilion-home/electronic-test-and-measurement/precision-measurement.html");
            ((IJavaScriptExecutor)driver).ExecuteScript("arguments[0].click();", driver.FindElement(By.CssSelector("area[alt^='integrated-smu_pmu_subsystem']")));
            Thread.Sleep(3000);
            action.IClick(driver, Elements.Applications_SignalChain_SelectedPart_CloseBtn);
            test.validateElementIsNotPresent(driver, By.CssSelector("div[class='scd-partTable table-responsive']"));
            action.IClick(driver, Elements.Applications_SignalChain_SelectedPart_ExpandBtn);
            test.validateElementIsPresent(driver, By.CssSelector("div[class='scd-partTable table-responsive']"));
        }
        [Test, Category("Applications"), Category("Core")]
        [TestCase("en", TestName = "Verify product # in parts used table redirects user to PDP in EN Locale")]
        [TestCase("cn", TestName = "Verify product # in parts used table redirects user to PDP in CN Locale")]
        [TestCase("jp", TestName = "Verify product # in parts used table redirects user to PDP in JP Locale")]
        [TestCase("ru", TestName = "Verify product # in parts used table redirects user to PDP in RU Locale")]
        public void ApplicationsPage_VerifySignalChainsDiagram3(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/applications/markets/instrumentation-and-measurement-pavilion-home/electronic-test-and-measurement/precision-measurement.html");
            ((IJavaScriptExecutor)driver).ExecuteScript("arguments[0].click();", driver.FindElement(By.CssSelector("area[alt^='integrated-smu_pmu_subsystem']")));
            Thread.Sleep(3000);
            string productNo = util.GetText(driver, By.CssSelector("div[class='scd-partTable table-responsive'] tr[class='scd-part-row'] a"));
            action.IClick(driver, By.CssSelector("div[class='scd-partTable table-responsive'] tr[class='scd-part-row'] a"));
            driver.SwitchTo().Window(driver.WindowHandles.Last());
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/products/" + productNo.ToLower() + ".html");

        }
    }
}