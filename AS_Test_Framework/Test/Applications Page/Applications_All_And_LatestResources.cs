﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Collections.Generic;

namespace AS_Test_Framework.Applications
{

    [TestFixture]
    public class Applications_All_And_LatestResources : BaseSetUp
    {
        public Applications_All_And_LatestResources() : base() { }

        [Test, Category("Applications"), Category("Core")]
        [TestCase("en", "All Resources", "Latest Resources", TestName = "Verify that All And Latest Resources section is present in Markets Page for EN Locale")]
        [TestCase("cn", "所有资源", "最新优势资源", TestName = "Verify that All And Latest Resources section is present in Markets Page for CN Locale")]
        [TestCase("jp", "関連資料", "最新情報", TestName = "Verify that All And Latest Resources section is present in Markets Page for JP Locale")]
        [TestCase("ru", "Все ресурсы", "Актуальные ресурсы по теме", TestName = "Verify that All And Latest Resources section is present in Markets Page for RU Locale")]
        public void ApplicationsPage_VerifyResources(string Locale, string AllResources, string LatestResources)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/applications/markets/automotive-pavilion-home/vehicle-electrification.html");
            test.validateStringIsCorrect(driver, Elements.Applications_AllResources_Label, AllResources);
            test.validateStringIsCorrect(driver, Elements.Applications_LatestResource_Label, LatestResources);
        }

        [Test, Category("Applications"), Category("Core")]
        [TestCase("en", "All Resources", "Latest Resources", TestName = "Verify that All And Latest Resources section is present in Technology Page for EN Locale")]
        [TestCase("cn", "所有资源", "最新优势资源", TestName = "Verify that All And Latest Resources section is present in Technology Page for CN Locale")]
        [TestCase("jp", "関連資料", "最新情報", TestName = "Verify that All And Latest Resources section is present in Technology Page for JP Locale")]
        [TestCase("ru", "Все ресурсы", "Актуальные ресурсы по теме", TestName = "Verify that All And Latest Resources section is present in Technology Page for RU Locale")]
        public void ApplicationsPage_VerifyResources2(string Locale, string AllResources, string LatestResources)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/applications/technology/precision-sensor-interface/temperature-sensing/thermocouple.html");
            test.validateStringIsCorrect(driver, Elements.Applications_AllResources_Label, AllResources);
            test.validateStringIsCorrect(driver, Elements.Applications_LatestResource_Label, LatestResources);
        }
    }
}