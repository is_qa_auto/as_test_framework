﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Collections.Generic;

namespace AS_Test_Framework.Applications
{

    [TestFixture]
    public class Applications_BreadCrumbs : BaseSetUp
    {
        public Applications_BreadCrumbs() : base() { }

        [Test, Category("Applications"), Category("Core")]
        [TestCase("en", "Applications", "Markets", TestName = "Verify that Breadcrumbs is present in Applications Page for EN Locale")]
        [TestCase("cn", "应用", "领域", TestName = "Verify that Breadcrumbs is present in Applications Page for CN Locale")]
        [TestCase("jp", "アプリケーション", "マーケット", TestName = "Verify that Breadcrumbs is present in Applications Page for JP Locale")]
        [TestCase("ru", "Применение", "Области применения", TestName = "Verify that Breadcrumbs is present in Applications Page for RU Locale")]
        public void ApplicationsPage_VerifyBreadCrumbs(string Locale, string FirstLevelCat, string CurrentPage)
        {
            action.Navigate(driver, Configuration.Env_Url +  Locale + "/applications/markets.html");
            test.validateElementIsPresent(driver, Elements.Applications_BreadCrumbs);
            test.validateStringIsCorrect(driver, By.CssSelector("div[name='adi_breadcrumb'] li[class='active']>span"), CurrentPage);
            test.validateStringIsCorrect(driver, By.CssSelector("div[name='adi_breadcrumb'] ol>li:nth-child(2)>span>a"), FirstLevelCat);
            action.IClick(driver, By.CssSelector("div[name='adi_breadcrumb'] ol>li:nth-child(2)>span>a"));
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/applications.html");
            driver.Navigate().Back();
            test.validateElementIsPresent(driver, Elements.Applications_BreadCrumbs_Home);
            action.IClick(driver, Elements.Applications_BreadCrumbs_Home);
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/index.html");
        }

        [Test, Category("Applications"), Category("Core")]
        [TestCase("en", "Applications", "Markets", "Automotive", TestName = "Verify that breadcrumbs is present in applications page sub-level category for EN Locale")]
        [TestCase("cn", "应用", "领域", "汽车", TestName = "Verify that breadcrumbs is present in applications page sub-level category for CN Locale")]
        [TestCase("jp", "アプリケーション", "マーケット", "オートモーティブ", TestName = "Verify that breadcrumbs is present in applications page sub-level category for JP Locale")]
        [TestCase("ru", "Применение", "Области применения", "Автомобильная электроника", TestName = "Verify that breadcrumbs is present in applications page sub-level category for RU Locale")]
        public void ApplicationsPage_VerifyBreadCrumbs2(string Locale,  string FirstLevelCat, string SecondLevelCat, string CurrentPage)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/applications/markets/automotive-pavilion-home.html");
            test.validateElementIsPresent(driver, Elements.Applications_BreadCrumbs);
            test.validateStringIsCorrect(driver, By.CssSelector("div[name='adi_breadcrumb'] ol>li:nth-child(2)>span>a"), FirstLevelCat);
            test.validateStringIsCorrect(driver, By.CssSelector("div[name='adi_breadcrumb'] ol>li:nth-child(3)>span>a"), SecondLevelCat);
            test.validateStringIsCorrect(driver, By.CssSelector("div[name='adi_breadcrumb'] li[class='active']>span"), CurrentPage);
            action.IClick(driver, By.CssSelector("div[name='adi_breadcrumb'] ol>li:nth-child(2)>span>a"));
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/applications.html");
            driver.Navigate().Back();
            action.IClick(driver, By.CssSelector("div[name='adi_breadcrumb'] ol>li:nth-child(3)>span>a"));
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/applications/markets.html");
            driver.Navigate().Back();
            test.validateElementIsPresent(driver, Elements.Applications_BreadCrumbs_Home);
            action.IClick(driver, Elements.Applications_BreadCrumbs_Home);
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/index.html");
        }

        [Test, Category("Applications"), Category("Core")]
        [TestCase("en", "Applications", "Markets", "Automotive", "Vehicle Electrification Systems", TestName = "Verify that breadcrumbs is present in applications page sub-level child category for EN Locale")]
        [TestCase("cn", "应用", "领域", "汽车", "汽车电气化", TestName = "Verify that breadcrumbs is present in applications page sub-level child category for CN Locale")]
        [TestCase("jp", "アプリケーション", "マーケット", "オートモーティブ", "自動車充電システム",  TestName = "Verify that breadcrumbs is present in applications page sub-level child category for JP Locale")]
        [TestCase("ru", "Применение", "Области применения", "Автомобильная электроника", "Электрификация транспортных средств", TestName = "Verify that breadcrumbs is present in applications page sub-level child category for RU Locale")]
        public void ApplicationsPage_VerifyBreadCrumbs3(string Locale, string FirstLevelCat, string SecondLevelCat, string ThirdLevelCat, string CurrentPage)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/applications/markets/automotive-pavilion-home/vehicle-electrification.html");
            test.validateElementIsPresent(driver, Elements.Applications_BreadCrumbs);
            test.validateStringIsCorrect(driver, By.CssSelector("div[name='adi_breadcrumb'] ol>li:nth-child(2)>span>a"), FirstLevelCat);
            test.validateStringIsCorrect(driver, By.CssSelector("div[name='adi_breadcrumb'] ol>li:nth-child(3)>span>a"), SecondLevelCat);
            test.validateStringIsCorrect(driver, By.CssSelector("div[name='adi_breadcrumb'] ol>li:nth-child(4)>span>a"), ThirdLevelCat);
            test.validateStringIsCorrect(driver, By.CssSelector("div[name='adi_breadcrumb'] li[class='active']>span"), CurrentPage);
            action.IClick(driver, By.CssSelector("div[name='adi_breadcrumb'] ol>li:nth-child(2)>span>a"));
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/applications.html");
            driver.Navigate().Back();
            action.IClick(driver, By.CssSelector("div[name='adi_breadcrumb'] ol>li:nth-child(3)>span>a"));
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/applications/markets.html");
            driver.Navigate().Back();
            action.IClick(driver, By.CssSelector("div[name='adi_breadcrumb'] ol>li:nth-child(4)>span>a"));
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/applications/markets/automotive-pavilion-home.html");
            driver.Navigate().Back();
            test.validateElementIsPresent(driver, Elements.Applications_BreadCrumbs_Home);
            action.IClick(driver, Elements.Applications_BreadCrumbs_Home);
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/index.html");
        }
    }
}