﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Collections.Generic;

namespace AS_Test_Framework.Applications
{

    [TestFixture]
    public class Applications_CategoryLandingPage : BaseSetUp
    {
        public Applications_CategoryLandingPage() : base() { }

        [Test, Category("Applications"), Category("Core")]
        [TestCase("en", "Markets", "Technology Solutions", TestName = "Verify that Accordion Header is present in Applications Page for EN Locale", Category = "Smoke_ADIWeb")]
        [TestCase("cn", "领域", "技术解决方案", TestName = "Verify that Accordion Header is present in Applications Page for CN Locale", Category = "Smoke_ADIWeb")]
        [TestCase("jp", "マーケット", "テクノロジー", TestName = "Verify that Accordion Header is present in Applications Page for JP Locale", Category = "Smoke_ADIWeb")]
        [TestCase("ru", "Области применения", "Технологии", TestName = "Verify that Accordion Header is present in Applications Page for RU Locale")]
        public void ApplicationsPage_VerifyHeaderAccordion(string Locale, string MarketAccorionHeader, string TechAccordionHeader)
        {
            string initial_steps = "1. Go to " + Configuration.Env_Url + Locale + "/applications/markets.html";
            string scenario = "";

            action.INavigate(driver, Configuration.Env_Url +  Locale + "/applications/markets.html");
            scenario = "verify that Market accordion header is present and correct";
            test.validateStringIsCorrectv2(driver, By.CssSelector("div[class='section-heading clearfix'] h3"), MarketAccorionHeader, scenario, initial_steps);
            scenario = "verify that background-color of market accordion header is correct";
            test.validateColorIsCorrectv2(driver, util.GetCssValue(driver, By.CssSelector("div[class='section-heading clearfix']"), "background-color"), "#1E4056", scenario, initial_steps, By.CssSelector("div[class='section-heading clearfix']"));

            action.INavigate(driver, Configuration.Env_Url + Locale + "/applications/technology.html");
            initial_steps = "1. Go to " + Configuration.Env_Url + Locale + "/applications/technology.html";

            scenario = "verify that Technology accordion header is present and correct";
            test.validateStringIsCorrectv2(driver, By.CssSelector("div[class='section-heading clearfix'] h3"), TechAccordionHeader, scenario, initial_steps);
            scenario = "verify that background-color of Technology accordion header is correct";
            test.validateColorIsCorrectv2(driver, util.GetCssValue(driver, By.CssSelector("div[class='section-heading clearfix']"), "background-color"), "#1E4056", scenario, initial_steps, By.CssSelector("div[class='section-heading clearfix']"));
        }

        [Test, Category("Applications"), Category("Core")]
        [TestCase("en", "See More Products", "See More Resources", TestName = "Verify that See More Products/Resources in Market Landing page is present and working as expected for EN Locale")]
        [TestCase("cn", "查看更多产品", "更多资源", TestName = "Verify that See More Products/Resources in Market Landing page is present and working as expected for CN Locale")]
        [TestCase("jp", "その他の関連製品", "その他の関連リソース", TestName = "Verify that See More Products/Resources in Market Landing page is present and working as expected for JP Locale")]
        [TestCase("ru", "Посмотреть больше продуктов", "Смотреть еще ресурсы", TestName = "Verify that See More Products/Resources in Market Landing page is present and working as expected for RU Locale")]
        public void ApplicationsPage_VerifySeeMore(string Locale, string SeeMoreProducts, string SeeMoreResources)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/applications/markets.html");
            string Url = util.ReturnAttribute(driver, By.CssSelector("div[class='product-content']>div:nth-child(1) h3>a"), "href");
            test.validateElementIsPresent(driver, By.CssSelector("div[class='product-content']>div:nth-child(1) div[class='col-md-6 offset-mobile-box-sd-rt-16 padding-des-rt-32 override-mg-16']>ul>li>a"));
            test.validateStringIsCorrect(driver, By.CssSelector("div[class='product-content']>div:nth-child(1) div[class='col-md-3 offset-mobile-box-sd-16 padding-des-rt-32'] a[class='mobile_btn']"), SeeMoreProducts);
            test.validateStringIsCorrect(driver, By.CssSelector("div[class='product-content']>div:nth-child(1) div[class='col-md-3 offset-mobile-box-sd-16'] a[class='mobile_btn']"), SeeMoreResources);
            action.IClick(driver, By.CssSelector("div[class='product-content']>div:nth-child(1) div[class='col-md-3 offset-mobile-box-sd-16 padding-des-rt-32'] a[class='mobile_btn']"));
            test.validateScreenByUrl(driver, Url);
            driver.Navigate().Back();
            action.IClick(driver, By.CssSelector("div[class='product-content']>div:nth-child(1) div[class='col-md-3 offset-mobile-box-sd-16'] a[class='mobile_btn']"));
            test.validateScreenByUrl(driver, Url + "#resource-list");
        }
       
    }
}