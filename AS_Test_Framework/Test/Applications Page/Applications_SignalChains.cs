﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Collections.Generic;

namespace AS_Test_Framework.Applications
{

    [TestFixture]
    public class Applications_SignalChains : BaseSetUp
    {
        public Applications_SignalChains() : base() { }

        [Test, Category("Applications"), Category("Core")]
        [TestCase("en", "Interactive Signal Chains", TestName = "Verify if the Interactive Signal Chains pop up are present for first time user in EN Locale")]
        [TestCase("cn", "互动式信号链", TestName = "Verify if the Interactive Signal Chains pop up are present for first time user in CN Locale")]
        [TestCase("jp", "シグナル・チェーン", TestName = "Verify if the Interactive Signal Chains pop up are present for first time user in JP Locale")]
        [TestCase("ru", "Интерактивные сигнальные цепочки", TestName = "Verify if the Interactive Signal Chains pop up are present for first time user in RU Locale")]
        public void ApplicationsPage_VerifySignalChains1(string Locale, string PopupLabel)
        {
            action.Navigate(driver, Configuration.Env_Url +  Locale + "/applications/markets/building-technology/heating-ventilation-and-air-conditioning.html");
            test.validateStringIsCorrect(driver, Elements.Applications_SignaChains_Popup_Header, PopupLabel);
            driver.Navigate().Refresh();
            test.validateElementIsNotPresent(driver, Elements.Applications_SignaChains_Popup);
            test.validateElementIsPresent(driver, Elements.Applications_SignalChains_Diagram);
        }

        [Test, Category("Applications"), Category("Core")]
        [TestCase("en", TestName = "Verify title of the selected Signal Chain is displayed above the signal chain diagram for EN Locale")]
        [TestCase("cn", TestName = "Verify title of the selected Signal Chain is displayed above the signal chain diagram for CN Locale")]
        [TestCase("jp", TestName = "Verify title of the selected Signal Chain is displayed above the signal chain diagram for JP Locale")]
        [TestCase("ru", TestName = "Verify title of the selected Signal Chain is displayed above the signal chain diagram for RU Locale")]
        public void ApplicationsPage_VerifySignalChains2(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/applications/markets/instrumentation-and-measurement-pavilion-home/electronic-test-and-measurement/precision-measurement.html");
            string selected_diagram = util.GetText(driver, By.CssSelector("ul[class='list-unstyled options-list options-list-campaign tabAccordion hidden-xs pavillion-options-list']>li[class='active']>a>h3"));
            test.validateStringIsCorrect(driver, Elements.Applications_SignalChain_Title, selected_diagram);

        }

        [Test, Category("Applications"), Category("Core")]
        [TestCase("en", TestName = "Verify pagination is present and working in signal chain diagram for EN Locale")]
        [TestCase("cn", TestName = "Verify pagination is present and working in signal chain diagram for CN Locale")]
        [TestCase("jp", TestName = "Verify pagination is present and working in signal chain diagram for JP Locale")]
        [TestCase("ru", TestName = "Verify pagination is present and working in signal chain diagram for RU Locale")]
        public void ApplicationsPage_VerifySignalChains3(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/applications/markets/instrumentation-and-measurement-pavilion-home/electronic-test-and-measurement/precision-measurement.html");
            test.validateElementIsPresent(driver, Elements.Applications_SignalChain_NextBtn);
            test.validateElementIsPresent(driver, By.CssSelector("div[class='nav-direction nav-prev nav-dir prev disabled']"));
            action.IClick(driver, Elements.Applications_SignalChain_NextBtn);
            test.validateElementIsPresent(driver, By.CssSelector("div[class='nav-direction nav-next nav-dir next disabled']"));
            test.validateElementIsPresent(driver, Elements.Applications_SignalChain_PrevBtn);

        }


    }
}