﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Collections.Generic;

namespace AS_Test_Framework.Applications
{

    [TestFixture]
    public class Applications_SubCategoryLandingPage : BaseSetUp
    {
        public Applications_SubCategoryLandingPage() : base() { }
        
        [Test, Category("Applications"), Category("Core")]
        [TestCase("en",  TestName = "Verify that Pavilion hero carousel is present in Applications Sub Level Category for EN Locale")]
        [TestCase("cn",  TestName = "Verify that Pavilion hero carousel is present in Applications Sub Level Category for CN Locale")]
        [TestCase("jp",  TestName = "Verify that Pavilion hero carousel is present in Applications Sub Level Category for JP Locale")]
        [TestCase("ru",  TestName = "Verify that Pavilion hero carousel is present in Applications Sub Level Category for RU Locale")]
        public void ApplicationsPage_VerifyPavillionLandingPage(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/applications/markets/aerospace-and-defense-pavilion-home.html");
            test.validateElementIsPresent(driver, By.CssSelector("div[class='owl-item active'] div[class='home-hero-bg'] img"));
        }

       
        [Test, Category("Applications"), Category("Core")]
        [TestCase("en", TestName = "Verify that RF Applcations back button is present and working in Markets Landing page for EN Locale")]
        public void ApplicationsPage_VerifyRFBackButton(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/applications/landing-pages/001/from-airport-terminal.html");
            test.validateElementIsPresent(driver, By.CssSelector(" div[class='secondary-nav-block']>h3>a"));
            action.IClick(driver, By.CssSelector(" div[class='secondary-nav-block']>h3>a"));
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/applications/technology/rf-leadership/rf-applications.html");
        }

        [Test, Category("Applications"), Category("Core")]
        [TestCase("en", TestName = "Verify that Dynamic Table is present in Applications sub category for EN Locale")]
        [TestCase("cn", TestName = "Verify that Dynamic Table is present in Applications sub category for CN Locale")]
        [TestCase("jp", TestName = "Verify that Dynamic Table is present in Applications sub category for JP Locale")]
        [TestCase("ru", TestName = "Verify that Dynamic Table is present in Applications sub category for RU Locale")]
        public void ApplicationsPage_VerifyDynamicTable(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/applications/technology/mems-switch.html");
            test.validateElementIsPresent(driver, Elements.Applications_PSTDynamicTable);
        }

        [Test, Category("Applications"), Category("Core")]
        [TestCase("en", "Aerospace and Defense", "Reference Designs", TestName = "Verify the Market First Level Callout is present and working for EN Locale")]
        [TestCase("cn", "航空航天和防务", "参考电路", TestName = "Verify the Market First Level Callout is present and working for CN Locale")]
        [TestCase("jp", "航空宇宙 ＆ 防衛", "リファレンス設計", TestName = "Verify the Market First Level Callout is present and working for JP Locale")]
        [TestCase("ru", "Авиация, космос, оборона", "Типовые проекты", TestName = "Verify the Market First Level Callout is present and working for RU Locale")]
        public void ApplicationsPage_VerifyMarketFirstLevelCallout(string Locale, string CatLabel, string RefDesign)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/applications/markets/aerospace-and-defense-pavilion-home.html");
            test.validateStringIsCorrect(driver, By.CssSelector("div[class='row application-secondary-nav header title'] h3>a"), CatLabel + " " + RefDesign);
            action.IClick(driver, By.CssSelector("div[class='row application-secondary-nav header title'] h3>a"));
            test.validateStringInstance(driver, driver.Url, Configuration.Env_Url + Locale + "/design-center/search.html?q");

            string[] UrlWithNoCallOut = {
                                            //"/applications/markets/building-technology.html",
                                            "/applications/markets/security-and-surveillance.html",
                                            "/applications/markets/data-center.html"
                                        };

            for (int x = 1, y=0; x <= UrlWithNoCallOut.Length; x++,y++) {
                action.Navigate(driver, Configuration.Env_Url + Locale + UrlWithNoCallOut[y]);
                test.validateElementIsNotPresent(driver, By.CssSelector("div[class='row application-secondary-nav header title'] h3>a"));
            }
        }


    }
}