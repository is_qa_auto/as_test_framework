﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Collections.Generic;

namespace AS_Test_Framework.Applications
{

    [TestFixture]
    public class Applications_PrintLinks : BaseSetUp
    {
        public Applications_PrintLinks() : base() { }

        [Test, Category("Applications"), Category("Core")]
        [TestCase("en", TestName = "Verify that Print is present in Sub-Level Category of Applications Page for EN Locale")]
        [TestCase("cn", TestName = "Verify that Print is present in Sub-Level Category of Applications Page for CN Locale")]
        [TestCase("jp", TestName = "Verify that Print is present in Sub-Level Category of Applications Page for JP Locale")]
        [TestCase("ru", TestName = "Verify that Print is present in Sub-Level Category of Applications Page for RU Locale")]
        public void ApplicationsPage_VerifyPrint(string Locale)
        {
            /*****Market Sub Category*******/
            action.Navigate(driver, Configuration.Env_Url +  Locale + "/applications/markets/aerospace-and-defense-pavilion-home.html");
            test.validateElementIsPresent(driver, Elements.Applications_Print);
            /*****Technology Sub Category*******/
            action.Navigate(driver, Configuration.Env_Url + Locale + "/applications/technology/3d-time-of-flight.html");
            test.validateElementIsPresent(driver, Elements.Applications_Print);
        }

        [Test, Category("Applications"), Category("Core")]
        [TestCase("en", TestName = "Verify that Print is present in Sub-Level Child Category of Applications Page for EN Locale")]
        [TestCase("cn", TestName = "Verify that Print is present in Sub-Level Child Category of Applications Page for CN Locale")]
        [TestCase("jp", TestName = "Verify that Print is present in Sub-Level Child Category of Applications Page for JP Locale")]
        [TestCase("ru", TestName = "Verify that Print is present in Sub-Level Child Category of Applications Page for RU Locale")]
        public void ApplicationsPage_VerifyPrint2(string Locale)
        {
            /*****Market Sub Child Category*******/
            action.Navigate(driver, Configuration.Env_Url + Locale + "/applications/markets/aerospace-and-defense-pavilion-home/phased-array-solution.html");
            test.validateElementIsPresent(driver, Elements.Applications_Print);
            /*****Tech Sub Child Category*******/
            action.Navigate(driver, Configuration.Env_Url + Locale + "/applications/technology/smartmesh-pavilion-home/smartmesh-technology.html");
            test.validateElementIsPresent(driver, Elements.Applications_Print);
        }

    }
}