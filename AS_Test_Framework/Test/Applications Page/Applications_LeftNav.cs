﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Collections.Generic;

namespace AS_Test_Framework.Applications
{

    [TestFixture]
    public class Applications_LeftNav : BaseSetUp
    {
        public Applications_LeftNav() : base() { }
        
        [Test, Category("Applications"), Category("Core")]
        [TestCase("en", TestName = "Verify Left Navigation is present in Markets - Sub Child Level Category for EN Locale")]
        [TestCase("cn", TestName = "Verify Left Navigation is present in Markets - Sub Child Level Category for CN Locale")]
        [TestCase("jp", TestName = "Verify Left Navigation is present in Markets - Sub Child Level Category for JP Locale")]
        [TestCase("ru", TestName = "Verify Left Navigation is present in Markets - Sub Child Level Category for RU Locale")]
        public void ApplicationsPage_VerifyLeftNav1(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/applications/markets/automotive-pavilion-home/vehicle-electrification/dc-dc-conversion.html");
            test.validateElementIsPresent(driver, Elements.Applications_LeftNav);
            test.validateColorIsCorrect(driver, util.GetCssValue(driver,By.CssSelector("div[class='col-md-12 floater-menu-container'] div[name='adi_products_left_navigation'] ul[id*='secondary']>li[class='active']"), "background-color"), "#1e4056");
            string OtherLeftNavPage = util.GetText(driver, By.CssSelector("div[class='col-md-12 floater-menu-container'] div[name='adi_products_left_navigation']>ul>li:last-child>div>a"));
            action.IClick(driver, By.CssSelector("div[class='col-md-12 floater-menu-container'] div[name='adi_products_left_navigation']>ul>li:last-child>div>a"));
            test.validateStringIsCorrect(driver, By.CssSelector("div[class='container page-title '] h1"), OtherLeftNavPage);
            test.validateStringIsCorrect(driver, By.CssSelector("div[class='col-md-12 floater-menu-container'] div[name='adi_products_left_navigation']>ul>li[class*='active'] a"), OtherLeftNavPage);
        }


        [Test, Category("Applications"), Category("Core")]
        [TestCase("en", TestName = "Verify Left Navigation is present in Technology - Sub Child Level Category for EN Locale")]
        [TestCase("cn", TestName = "Verify Left Navigation is present in Technology - Sub Child Level Category for CN Locale")]
        [TestCase("jp", TestName = "Verify Left Navigation is present in Technology - Sub Child Level Category for JP Locale")]
        [TestCase("ru", TestName = "Verify Left Navigation is present in Technology - Sub Child Level Category for RU Locale")]
        public void ApplicationsPage_VerifyLeftNav2(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/applications/technology/precision-sensor-interface/temperature-sensing/thermocouple.html");
            test.validateElementIsPresent(driver, Elements.Applications_LeftNav);
            test.validateColorIsCorrect(driver, util.GetCssValue(driver, By.CssSelector("div[class='col-md-12 floater-menu-container'] div[name='adi_products_left_navigation'] ul[id*='secondary']>li[class='active']"), "background-color"), "#1e4056");
            string OtherLeftNavPage = util.GetText(driver, By.CssSelector("div[class='col-md-12 floater-menu-container'] div[name='adi_products_left_navigation']>ul>li:last-child a"));
            action.IClick(driver, By.CssSelector("div[class='col-md-12 floater-menu-container'] div[name='adi_products_left_navigation']>ul>li:last-child a"));
            test.validateStringInstance(driver, OtherLeftNavPage, util.GetText(driver, By.CssSelector("div[class='container page-title '] h1")));
            test.validateStringIsCorrect(driver, By.CssSelector("div[class='col-md-12 floater-menu-container'] div[name='adi_products_left_navigation']>ul>li[class*='active'] a"), OtherLeftNavPage);

        }
    }
}