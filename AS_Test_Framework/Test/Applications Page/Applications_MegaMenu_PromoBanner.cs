﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Collections.Generic;
using System.Threading;

namespace AS_Test_Framework.Applications
{

    [TestFixture]
    public class Applications_MegaMenu_PromoBanner : BaseSetUp
    {
        public Applications_MegaMenu_PromoBanner() : base() { }

        [Test, Category("Applications"), Category("Core")]
        [TestCase("en", TestName = "Verify Promo Banner in applications tab of mega menu is present in core pages for EN Locale")]
        [TestCase("cn", TestName = "Verify Promo Banner in applications tab of mega menu is present in core pages for CN Locale")]
        [TestCase("jp", TestName = "Verify Promo Banner in applications tab of mega menu is present in core pages for JP Locale")]
        [TestCase("ru", TestName = "Verify Promo Banner in applications tab of mega menu is present in core pages for RU Locale")]
        public void ApplicationsPage_VerifyPromoBanner1(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/index.html");
            action.IClick(driver, Elements.MainNavigation_ApplicationsTab);
            Thread.Sleep(2000);
            test.validateElementIsPresent(driver, Elements.Applications_MenuNav_PromoBanner);
            test.validateElementIsPresent(driver, Elements.Applications_MenuNav_PromoBanner_LearnMore);
            action.IClick(driver, Elements.Applications_MenuNav_PromoBanner_LearnMore);
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/signals.html");
        }

        [Test, Category("Applications"), Category("Non-Core")]
        [TestCase("en", TestName = "Verify Promo Banner in applications tab of mega menu is present in non-core pages for EN Locale")]
        [TestCase("cn", TestName = "Verify Promo Banner in applications tab of mega menu is present in non-core pages for CN Locale")]
        [TestCase("jp", TestName = "Verify Promo Banner in applications tab of mega menu is present in non-core pages for JP Locale")]
        [TestCase("ru", TestName = "Verify Promo Banner in applications tab of mega menu is present in non-core pages for RU Locale")]
        public void ApplicationsPage_VerifyPromoBanner2(string Locale)
        {
            if (Locale.Equals("cn"))
            {
                action.Navigate(driver, Configuration.Env_Url.Replace("cldnet", "corpnt").Replace("www", "shoppingcart") + "/ShoppingCartPage.aspx?locale=zh");
            }
            else
            { 
                action.Navigate(driver, Configuration.Env_Url.Replace("cldnet", "corpnt").Replace("www", "shoppingcart") + "/ShoppingCartPage.aspx?locale=" + Locale);
            }
            action.IClick(driver, Elements.MainNavigation_ApplicationsTab);
            Thread.Sleep(2000);
            test.validateElementIsPresent(driver, Elements.Applications_MenuNav_PromoBanner);
            test.validateElementIsPresent(driver, Elements.Applications_MenuNav_PromoBanner_LearnMore);
            action.IClick(driver, Elements.Applications_MenuNav_PromoBanner_LearnMore);
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/signals.html");
        }
    }
}