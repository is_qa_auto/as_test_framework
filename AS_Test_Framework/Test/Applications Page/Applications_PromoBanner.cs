﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Collections.Generic;

namespace AS_Test_Framework.Applications
{

    [TestFixture]
    public class Applications_PromoBanner : BaseSetUp
    {
        public Applications_PromoBanner() : base() { }

        [Test, Category("Applications"), Category("Core")]
        [TestCase("en", TestName = "Verify Promo Banner is present in Technology Page for EN Locale")]
        [TestCase("cn", TestName = "Verify Promo Banner is present in Technology Page for CN Locale")]
        [TestCase("jp", TestName = "Verify Promo Banner is present in Technology Page for JP Locale")]
        [TestCase("ru", TestName = "Verify Promo Banner is present in Technology Page for RU Locale")]
        public void ApplicationsPage_VerifyPromoBanner1(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/applications/technology/internet-of-things.html");
            test.validateElementIsPresent(driver, Elements.Applications_PromoBanner);
        }
    }
}