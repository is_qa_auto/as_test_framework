﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Collections.Generic;
using System.Threading;

namespace AS_Test_Framework.Applications
{

    [TestFixture]
    public class Applications_SaveToAnalog : BaseSetUp
    {
        public Applications_SaveToAnalog() : base() { }

        [Test, Category("Applications"), Category("Core")]
        [TestCase("en", TestName = "Verify that Save To Analog is present in Sub-Level Category of Applications Page for EN Locale")]
        public void ApplicationsPage_VerifySaveToAnalogWidget(string Locale)
        {
            /*****Market Sub Category*******/

            action.IRemoveMarketInMyAnalog(driver, Locale, "aries.sorosoro@analog.com", "Test_1234", "Aerospace and Defense");
            action.Navigate(driver, Configuration.Env_Url + Locale + "/applications/markets/aerospace-and-defense-pavilion-home.html");
            test.validateElementIsPresent(driver, Elements.PDP_MyAnalogWidget_Btn);
            action.ILogInViaMyAnalogWidget(driver, "aries.sorosoro@analog.com", "Test_1234");
            //action.IClick(driver, Elements.PDP_MyAnalogWidget_Btn);
            test.validateElementIsPresent(driver, Elements.PDP_SaveToMyAnalog_Widget);
            action.IClick(driver, By.CssSelector("div[class='saveTo createProject category'] button[id='saveProductToMyanalog']"));
            Thread.Sleep(2000);
            test.validateElementIsPresent(driver, By.CssSelector("div[class='saved-message']>h5"));

            /*****Technology Sub Category*******/
            action.Navigate(driver, Configuration.Env_Url + Locale + "/applications/technology/3d-time-of-flight.html");
            string PageName = util.GetText(driver, By.CssSelector("div[name='adi_breadcrumb'] li[class='active']>span"));
            action.IRemoveItemInMyResources(driver, Locale, "aries.sorosoro@analog.com", "Test_1234", PageName);
            action.Navigate(driver, Configuration.Env_Url + Locale + "/applications/technology/3d-time-of-flight.html");
            test.validateElementIsPresent(driver, Elements.PDP_MyAnalogWidget_Btn);
            //action.IClick(driver, Elements.PDP_MyAnalogWidget_Btn);
            action.ILogInViaMyAnalogWidget(driver, "aries.sorosoro@analog.com", "Test_1234");
            test.validateElementIsPresent(driver, Elements.PDP_SaveToMyAnalog_Widget);
            action.IClick(driver, By.CssSelector("button[id='saveProductToMyanalog']"));
            Thread.Sleep(2000);
            test.validateElementIsPresent(driver, By.CssSelector("div[class='saved-message']>h5"));

        }
        [Test, Category("Applications"), Category("Core")]
        [TestCase("en", TestName = "Verify that Save To Analog is present in Sub-Level Child Category of Applications Page for EN Locale")]
        public void ApplicationsPage_VerifySaveToAnalogWidget2(string Locale)
        {
            /*****Market Sub Child Category*******/
            action.IRemoveItemInMyResources(driver, Locale, "aries.sorosoro@analog.com", "Test_1234", "Phased Array");
            action.Navigate(driver, Configuration.Env_Url + Locale + "/applications/markets/aerospace-and-defense-pavilion-home/phased-array-solution.html");
            test.validateElementIsPresent(driver, Elements.PDP_MyAnalogWidget_Btn);
            //action.IClick(driver, Elements.PDP_MyAnalogWidget_Btn);
            action.ILogInViaMyAnalogWidget(driver, "aries.sorosoro@analog.com", "Test_1234");
            test.validateElementIsPresent(driver, Elements.PDP_SaveToMyAnalog_Widget);
            action.IClick(driver, By.CssSelector("div[class='saveTo createProject'] button[id='saveProductToMyanalog']"));
            Thread.Sleep(2000);
            test.validateElementIsPresent(driver, By.CssSelector("div[class='saved-message']>h5"));

            /*****Technology Sub Category*******/
            action.IRemoveItemInMyResources(driver, Locale, "aries.sorosoro@analog.com", "Test_1234", "SmartMesh Technology");
            action.Navigate(driver, Configuration.Env_Url + Locale + "/applications/technology/smartmesh-pavilion-home/smartmesh-technology.html");
            test.validateElementIsPresent(driver, Elements.PDP_MyAnalogWidget_Btn);
            //action.IClick(driver, Elements.PDP_MyAnalogWidget_Btn);
            action.ILogInViaMyAnalogWidget(driver, "aries.sorosoro@analog.com", "Test_1234");
            test.validateElementIsPresent(driver, Elements.PDP_SaveToMyAnalog_Widget);
            action.IClick(driver, By.CssSelector("button[id='saveProductToMyanalog']"));
            Thread.Sleep(2000);
            test.validateElementIsPresent(driver, By.CssSelector("div[class='saved-message']>h5"));
        }
    }
}