﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Collections.Generic;

namespace AS_Test_Framework.Applications
{

    [TestFixture]
    public class Applications_ReferenceDesign : BaseSetUp
    {
        public Applications_ReferenceDesign() : base() { }
        
        [Test, Category("Applications"), Category("Core")]
        [TestCase("en", "Reference Designs", TestName = "Verify Reference Design Accordion is present in Market Sub child level child category for EN Locale")]
        [TestCase("cn", "参考设计", TestName = "Verify Reference Design Accordion is present in Market Sub child level child category for CN Locale")]
        [TestCase("jp", "リファレンス・デザイン", TestName = "Verify Reference Design Accordion is present in Market Sub child level child category for JP Locale")]
        [TestCase("ru", "Типовые проекты", TestName = "Verify Reference Design Accordion is present in Market Sub child level child category for RU Locale")]
        public void Applications_VerifyReferenceDesign1(string Locale, string RefDesignLabel)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/applications/markets/automotive-pavilion-home/vehicle-electrification.html");
            test.validateStringIsCorrect(driver, By.CssSelector("div[id='section-heading1'] div[class='adi-accordion alternate-state col-md-offset-2 container-offset '] div[class='accordion-title pavillion-heading'] h3"), RefDesignLabel); ;
            test.validateElementIsPresent(driver, By.CssSelector("div[id='section-heading1'] div[class='adi-accordion alternate-state col-md-offset-2 container-offset ']"));
            test.validateElementIsPresent(driver, By.CssSelector("div[id='section-heading1'] div[class='adi-accordion alternate-state col-md-offset-2 container-offset '] div[class='section-bar collapsed'] h3"));
            test.validateElementIsPresent(driver, By.CssSelector("div[id='section-heading1'] div[class='adi-accordion alternate-state col-md-offset-2 container-offset '] div[class='section-bar collapsed'] span[class*='textblock']"));

        }

        [Test, Category("Applications"), Category("Core")]
        [TestCase("en", TestName = "Verify expand/collapsed functionality of Reference Design Accordion in Markets - Sub Level Category is working for EN Locale")]
        [TestCase("cn", TestName = "Verify expand/collapsed functionality of Reference Design Accordion in Markets - Sub Level Category is working for CN Locale")]
        [TestCase("jp", TestName = "Verify expand/collapsed functionality of Reference Design Accordion in Markets - Sub Level Category is working for JP Locale")]
        [TestCase("ru", TestName = "Verify expand/collapsed functionality of Reference Design Accordion in Markets - Sub Level Category is working for RU Locale")]
        public void ApplicationsPage_VerifyFeaturedProducts_2(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/applications/markets/automotive-pavilion-home/vehicle-electrification.html");
            test.validateElementIsPresent(driver, By.CssSelector("div[id='section-heading1'] div[class='accordion-expand-all']>a[class='expand']"));
            action.IClick(driver, By.CssSelector("div[id='section-heading1'] div[class='accordion-expand-all']>a[class='expand']"));
            test.validateElementIsNotPresent(driver, By.CssSelector("div[id='section-heading1'] div[class='accordion-expand-all']>a[class='expand']"));
            test.validateElementIsPresent(driver, By.CssSelector("div[id='section-heading1'] div[class='accordion-expand-all']>a[class='collapse']"));
            test.validateElementIsPresent(driver, By.CssSelector("div[id='section-heading1'] div[class='adi-accordion alternate-state col-md-offset-2 container-offset  expand-all']"));
            action.IClick(driver, By.CssSelector("div[id='section-heading1'] div[class='accordion-expand-all']>a[class='collapse']"));
            test.validateElementIsPresent(driver, By.CssSelector("div[id='section-heading1'] div[class='adi-accordion alternate-state col-md-offset-2 container-offset']"));
        }

        [Test, Category("Applications"), Category("Core")]
        [TestCase("en", TestName = "Verify Product Details of Reference Design Accordion in Markets - Sub Level Category is present for EN Locale")]
        [TestCase("cn", TestName = "Verify Product Details of Reference Design Accordion in Markets - Sub Level Category is present for CN Locale")]
        [TestCase("jp", TestName = "Verify Product Details of Reference Design Accordion in Markets - Sub Level Category is present for JP Locale")]
        [TestCase("ru", TestName = "Verify Product Details of Reference Design Accordion in Markets - Sub Level Category is present for RU Locale")]
        public void ApplicationsPage_VerifyFeaturedProducts_3(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/applications/markets/automotive-pavilion-home/vehicle-electrification.html");
            action.IClick(driver, By.CssSelector("div[id='section-heading1'] div[class='accordion-expand-all']>a[class='expand']"));
            test.validateElementIsPresent(driver, By.CssSelector("div[id='section-heading1'] div[class='product-content'] h3>a"));
            test.validateElementIsPresent(driver, By.CssSelector("div[id='section-heading1'] div[class='product-content'] img"));
            test.validateElementIsPresent(driver, By.CssSelector("div[id='section-heading1'] div[class='product-content'] div[class='summary']"));
            test.validateElementIsPresent(driver, By.CssSelector("div[id='section-heading1'] div[class='product-content'] a[class='btn btn-primary']"));
            test.validateElementIsPresent(driver, By.CssSelector("div[id='section-heading1'] div[class='product-content'] a[class='btn btn-primary']:nth-of-type(2)"));
            string Prod_No = util.GetText(driver, By.CssSelector("div[id='section-heading1'] div[class='product-content'] h3>a"));
            action.IClick(driver, By.CssSelector("div[id='section-heading1'] div[class='product-content'] h3>a"));
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/design-center/reference-designs/circuits-from-the-lab/" + Prod_No.ToUpper() + ".html");

        }

        [Test, Category("Applications"), Category("Core")]
        [TestCase("en", TestName = "Verify View Pricing and View Detailed Information button is present in Reference Design Accordion in Markets - Sub Level Category is present for EN Locale")]
        [TestCase("cn", TestName = "Verify View Pricing and View Detailed Information button is present in Reference Design Accordion in Markets - Sub Level Category is present for CN Locale")]
        [TestCase("jp", TestName = "Verify View Pricing and View Detailed Information button is present in Reference Design Accordion in Markets - Sub Level Category is present for JP Locale")]
        [TestCase("ru", TestName = "Verify View Pricing and View Detailed Information button is present in Reference Design Accordion in Markets - Sub Level Category is present for RU Locale")]
        public void ApplicationsPage_VerifyFeaturedProducts_4(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/applications/markets/automotive-pavilion-home/vehicle-electrification.html");
            action.IClick(driver, By.CssSelector("div[id='section-heading1'] div[class='accordion-expand-all']>a[class='expand']"));
            string ProductNo = util.GetText(driver, By.CssSelector("div[id='section-heading1'] div[class='section-heading pavillion-section-heading collapsed']>h3"));
            /****check if view pricing is working****/
            action.IClick(driver, By.CssSelector("div[id='section-heading1'] div[class='adi-accordion alternate-state col-md-offset-2 container-offset  expand-all'] div[class='product-content'] div[class='col-md-8 clearfix']>a:nth-of-type(2)"));
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/design-center/reference-designs/circuits-from-the-lab/" + ProductNo.ToUpper() + ".html#rd-sampleproducts");
            driver.Navigate().Back();
            /****check if view detailed info is working****/
            action.IClick(driver, By.CssSelector("div[id='section-heading1'] div[class='accordion-expand-all']>a[class='expand']"));
            action.IClick(driver, By.CssSelector("div[id='section-heading1'] div[class='adi-accordion alternate-state col-md-offset-2 container-offset  expand-all'] div[class='product-content'] div[class='col-md-8 clearfix']>a:nth-of-type(1)"));
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/design-center/reference-designs/circuits-from-the-lab/" + ProductNo.ToUpper() + ".html");


        }

    }
}