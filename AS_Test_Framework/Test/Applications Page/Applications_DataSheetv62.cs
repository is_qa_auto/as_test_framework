﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Collections.Generic;

namespace AS_Test_Framework.Applications
{

    [TestFixture]
    public class Applications_DataSheetv62 : BaseSetUp
    {
        public Applications_DataSheetv62() : base() { }

        [Test, Category("Applications"), Category("Core")]
        [TestCase("en", TestName = "Verify Data sheet v62 is present in Markets Page for EN Locale")]
        [TestCase("cn", TestName = "Verify Data sheet v62 is present in Markets Page for CN Locale")]
        [TestCase("jp", TestName = "Verify Data sheet v62 is present in Markets Page for JP Locale")]
        [TestCase("ru", TestName = "Verify Data sheet v62 is present in Markets Page for RU Locale")]
        public void ApplicationsPage_VerifyResources(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/applications/markets/aerospace-and-defense-pavilion-home/aerospace-defense-capabilities/enhanced-product-listing.html");
            test.validateElementIsPresent(driver, Elements.Applications_DataSheetv62_Link);
        }
    }
}