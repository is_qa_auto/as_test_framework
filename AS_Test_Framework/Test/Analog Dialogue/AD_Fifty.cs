﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Linq;

namespace AS_Test_Framework.AnalogDialogue
{

    [TestFixture]
    public class AD_Fifty : BaseSetUp
    {
        public AD_Fifty() : base() { }

        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", TestName = "Verify Hero Image is Present in Analog Dialogue 50th Years page")]
        public void AD_Fifty_l(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/analog-dialogue/50-years.html");
            test.validateElementIsPresent(driver, By.CssSelector("div[class='owl-item active'] * div img"));
        }

        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", TestName = "Verify Analog Dialogue 50th Years Logo")]
        public void AD_Fifty2(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/analog-dialogue/50-years.html");
            test.validateElementIsPresent(driver, Elements.AD_50_Logo);
        }

        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", TestName = "Verify Subscribe to Analog Dialogue Button is present and working as expected")]
        public void AD_Fifty3(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/analog-dialogue/50-years.html");
            test.validateElementIsPresent(driver, By.CssSelector("span[class='analog-rounded-button']>a"));
            action.IClick(driver, By.CssSelector("span[class='analog-rounded-button']>a"));
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/analog-dialogue.html?subscribe");
            test.validateElementIsPresent(driver, Elements.AD_Subscribe_Section);
            action.IClick(driver, Elements.AD_Subscribe_Btn);
            test.validateElementIsNotPresent(driver, Elements.AD_Subscribe_Section);
        }

        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", TestName = "Verify \"Looking Back with Ray Stata\" Section")]
        public void AD_Fifty4(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/analog-dialogue/50-years.html");
            test.validateElementIsPresent(driver, By.CssSelector("img[alt='Ray Stata Remembers']"));
            action.IClick(driver, By.CssSelector("a[href='#looking-back']"));
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/analog-dialogue/50-years.html#looking-back");
        }
        
        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", TestName = "Verify The Best of Analog Dialogue Section")]
        public void AD_Fifty5(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/analog-dialogue/50-years.html");
            test.validateElementIsPresent(driver, By.CssSelector("section[data-type='personalized-content']"));

        }

        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", TestName = "Verify the Subsricbe to Analog Dialogue banner")]
        public void AD_Fifty6(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/analog-dialogue/50-years.html");
            test.validateStringIsCorrect(driver, By.CssSelector("div[class='bar']>h4>span"), "Subscribe to Analog Dialogue");
            action.IClick(driver, By.CssSelector("div[class='bar']>h4>a"));
            driver.SwitchTo().Window(driver.WindowHandles.Last());
            test.validateStringInstance(driver, driver.Url, "analog-dialogue.html?subscribe");
            test.validateElementIsPresent(driver, Elements.AD_Subscribe_Section);
        }
    }
}
