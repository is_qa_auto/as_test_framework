﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.AnalogDialogue
{

    [TestFixture]
    public class AD_RAQ_Article_Page : BaseSetUp
    {
        public AD_RAQ_Article_Page() : base() { }

        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", TestName = "Verify RAQ page element")]
        public void AD_RAQ1(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/analog-dialogue/raqs/rAq-issUe-137.HTML");
            test.validateElementIsPresent(driver, By.CssSelector("section[class='col-lg-8 col-md-8 col-sm-12 col-xs-12 leftCol']>h1"));
            test.validateElementIsPresent(driver, Elements.AD_RAQ_Page_Author);
            test.validateElementIsPresent(driver, Elements.AD_RAQ_Page_Download);
        }

        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", TestName = "Verify author link is working as expected")]
        public void AD_RAQ2(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/analog-dialogue/raqs/raq-issue-137.HTML");
            string authorAnchor = util.ReturnAttribute(driver, Elements.AD_RAQ_Page_Author, "href");
            action.IClick(driver, Elements.AD_RAQ_Page_Author);
            test.validateScreenByUrl(driver, authorAnchor);

        }

        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", TestName = "Verify the Author Section")]
        public void AD_RAQ3(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/analog-dialogue/raqs/raq-issue-151.html");
            test.validateElementIsPresent(driver, Elements.AD_RAQ_Page_Author_Img);
            test.validateElementIsPresent(driver, Elements.AD_RAQ_Page_Author_Name);
            test.validateElementIsPresent(driver, Elements.AD_RAQ_Page_Author_Email);
            test.validateElementIsPresent(driver, Elements.AD_RAQ_Page_Author_Details);
        }

        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", TestName = "Verify the Related Product and Related Article Section")]
        public void AD_RAQ4(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/analog-dialogue/raqs/raq-issue-151.html");
            test.validateElementIsPresent(driver, Elements.AD_RAQ_Page_Related_Articles);
            test.validateElementIsPresent(driver, Elements.AD_RAQ_Page_Related_Products);
            test.validateElementIsPresent(driver, By.CssSelector("section[class='related-products'] * p:nth-of-type(2)"));
            test.validateElementIsPresent(driver, By.CssSelector("section[class='related-products'] ul>li>a"));
            string Products = util.GetText(driver, By.CssSelector("section[class='related-products'] ul>li>a"));
            action.IClick(driver, By.CssSelector("section[class='related-products'] ul>li>a"));
            driver.SwitchTo().Window(driver.WindowHandles.Last());
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/products/" + Products.ToLower() + ".html");
            driver.SwitchTo().Window(driver.WindowHandles.First());

            action.IClick(driver, By.CssSelector("section[class='related-categories']:nth-of-type(2) ul>li span[class='accordionGroup__toggle__icon']"));

            string ProductCat = util.GetText(driver, By.CssSelector("section[class='related-categories']:nth-of-type(2)>ul>li>div[id*='Category'] ul>li>a"));
            action.IClick(driver, By.CssSelector("section[class='related-categories']:nth-of-type(2)>ul>li>div[id*='Category'] ul>li>a"));
            driver.SwitchTo().Window(driver.WindowHandles.Last());
            test.validateStringIsCorrect(driver, Elements.Category_Header_Title, ProductCat);
        }
    }
}
