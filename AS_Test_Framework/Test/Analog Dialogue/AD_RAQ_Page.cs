﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.AnalogDialogue
{

    [TestFixture]
    public class AD_RAQ_Page : BaseSetUp
    {
        public AD_RAQ_Page() : base() { }

        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", TestName = "Verify there are default 3 RAQs displayed with full text/description in EN Locale")]
        [TestCase("cn", TestName = "Verify there are default 3 RAQs displayed with full text/description in CN Locale")]
        [TestCase("jp", TestName = "Verify there are default 3 RAQs displayed with full text/description in JP Locale")]
        public void AD_RAQ1(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/analog-dialogue/raqs.html");
            test.validateCountIsEqual(driver, 3, util.GetCount(driver, By.CssSelector("div[class='content raq-list']>div>section")));
        }

        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", TestName = "Verify if the RAQ articles images are present and working in EN Locale")]
        [TestCase("cn", TestName = "Verify if the RAQ articles images are present and working in CN Locale")]
        [TestCase("jp", TestName = "Verify if the RAQ articles images are present and working in JP Locale")]
        public void AD_RAQ2(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/analog-dialogue/raqs.html");
            for (int x = 1; util.CheckElement(driver, By.CssSelector("div[class='content raq-list']>div>section:nth-of-type(" + x + ")"), 1); x++)
            {
                test.validateElementIsPresent(driver, By.CssSelector("div[class='content raq-list']>div>section:nth-of-type(" + x + ") img"));
                string article_title = util.GetText(driver, By.CssSelector("div[class='content raq-list']>div>section:nth-of-type(" + x + ") h2"));
                action.IClick(driver, By.CssSelector("div[class='content raq-list']>div>section:nth-of-type(" + x + ") img"));
                test.validateStringIsCorrect(driver, By.CssSelector("section[class='col-lg-8 col-md-8 col-sm-12 col-xs-12 leftCol']>h1"), article_title);
            }
        }

        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", TestName = "Verify that the view the Answer link will redirect to the specific RAQ page in EN Locale")]
        [TestCase("cn", TestName = "Verify that the view the Answer link will redirect to the specific RAQ page in CN Locale")]
        [TestCase("jp", TestName = "Verify that the view the Answer link will redirect to the specific RAQ page in JP Locale")]
        public void AD_RAQ3(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/analog-dialogue/raqs.html");
            for (int x = 1; util.CheckElement(driver, By.CssSelector("div[class='content raq-list']>div>section:nth-of-type(" + x + ")"), 1); x++)
            {
                test.validateElementIsPresent(driver, By.CssSelector("div[class='content raq-list']>div>section:nth-of-type(" + x + ")>div:nth-child(2) a"));
                string article_title = util.GetText(driver, By.CssSelector("div[class='content raq-list']>div>section:nth-of-type(" + x + ") h2"));
                action.IClick(driver, By.CssSelector("div[class='content raq-list']>div>section:nth-of-type(" + x + ")>div:nth-child(2) a"));
                test.validateStringIsCorrect(driver, By.CssSelector("section[class='col-lg-8 col-md-8 col-sm-12 col-xs-12 leftCol']>h1"), article_title);
            }
        }
        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", TestName = "Verify there are default 8 RAQS displayed for the RAQs at the bottom and clicking Load More RAQs button will display more RAQ in EN Locale")]
        [TestCase("cn", TestName = "Verify there are default 8 RAQS displayed for the RAQs at the bottom and clicking Load More RAQs button will display more RAQ in CN Locale")]
        [TestCase("jp", TestName = "Verify there are default 8 RAQS displayed for the RAQs at the bottom and clicking Load More RAQs button will display more RAQ in JP Locale")]
        public void AD_RAQ4(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/analog-dialogue/raqs.html");
            int RAQ_count = util.GetCount(driver, By.CssSelector("ul[class='row more-raqs']>li"));
            test.validateCountIsEqual(driver, 8, util.GetCount(driver, By.CssSelector("ul[class='row more-raqs']>li[class*='active']")));
            action.IClick(driver, Elements.AD_RAQ_Page_ShowMore);
            test.validateCountIsEqual(driver, 16, util.GetCount(driver, By.CssSelector("ul[class='row more-raqs']>li[class*='active']")));
            for ( ; util.GetCount(driver, By.CssSelector("ul[class='row more-raqs']>li[class*='active']")) < RAQ_count ; )
            {
                action.IClick(driver, Elements.AD_RAQ_Page_ShowMore);
            }
            test.validateElementIsNotPresent(driver, Elements.AD_RAQ_Page_ShowMore);
        }


    }
}
