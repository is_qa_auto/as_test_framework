﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Linq;

namespace AS_Test_Framework.AnalogDialogue
{

    [TestFixture]
    public class AD_Homepage_AD50 : BaseSetUp
    {
        public AD_Homepage_AD50() : base() { }

        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", TestName = "Verify Analog Dialogue Turn 50 section in EN Locale")]
        [TestCase("cn", TestName = "Verify Analog Dialogue Turn 50 section in CN Locale")]
        [TestCase("jp", TestName = "Verify Analog Dialogue Turn 50 section in JP Locale")]
        public void AD_RAQ1(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/analog-dialogue.html");
            test.validateElementIsPresent(driver, By.CssSelector("img[alt='50th Anniversary']"));
            test.validateElementIsPresent(driver, By.CssSelector("div[class='row fca-row one-columnspot top-spacing-32 bottom-spacing-32 '] div[class='clearfix rte'] * a"));
            action.IClick(driver, By.CssSelector("div[class='row fca-row one-columnspot top-spacing-32 bottom-spacing-32 '] div[class='clearfix rte'] * a"));
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/analog-dialogue/50-years.html");
        }
    }
}
