﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Linq;

namespace AS_Test_Framework.AnalogDialogue
{

    [TestFixture]
    public class AD_Header_ShortHeader : BaseSetUp
    {
        public AD_Header_ShortHeader() : base() { }

        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", TestName = "Verify the elements in the Header in EN Locale", Category = "Smoke_ADIWeb")]
        [TestCase("cn", TestName = "Verify the elements in the Header in CN Locale", Category = "Smoke_ADIWeb")]
        [TestCase("jp", TestName = "Verify the elements in the Header in JP Locale", Category = "Smoke_ADIWeb")]
        public void AD_ShortHeader_Elements(string Locale)
        {
            string initial_steps = "1. Go to " + Configuration.Env_Url + Locale + "/analog-dialogue.html";
            string scenario = "";

            action.INavigate(driver, Configuration.Env_Url + Locale + "/analog-dialogue.html");

            scenario = "Verify that EZ link is currently present in Analog Dialogue short header";
            test.validateElementIsPresentv2(driver, Elements.EZ_Link, scenario, initial_steps);

            scenario = "Verify that Wiki link is currently present in Analog Dialogue short header";
            test.validateElementIsPresentv2(driver, Elements.Wiki_Link, scenario, initial_steps);

            scenario = "Verify that Shopping Cart link is currently present in Analog Dialogue short header";
            test.validateElementIsPresentv2(driver, Elements.Cart_Icon, scenario, initial_steps);

            scenario = "Verify that Regional link is currently present in Analog Dialogue short header";
            test.validateElementIsPresentv2(driver, Elements.Regional_Link, scenario, initial_steps);

            scenario = "Verify that Careers link is currently present in Analog Dialogue short header";
            test.validateElementIsPresentv2(driver, Elements.Careers_Link, scenario, initial_steps);

            scenario = "Verify that myAnalog link is currently present in Analog Dialogue short header";
            test.validateElementIsPresentv2(driver, Elements.myAnalog_Link, scenario, initial_steps);

            scenario = "Verify that Global Search textbox is currently not present in Analog Dialogue short header";
            test.validateElementIsNotPresentv2(driver, Elements.GlobalSearchTextField, scenario, initial_steps);

            scenario = "Verify that Main Menu Navigation is currently not present in Analog Dialogue short header";
            test.validateElementIsNotPresentv2(driver, By.CssSelector("div[class='container primary']>ul[class='nav navbar-nav']"), scenario, initial_steps);
        }

        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", TestName = "Verify that RU locale is present in Language menu of Analog Dialogue")]
        public void AD_ShortHeader_Language(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/analog-dialogue.html");
            action.IClick(driver, Elements.Regional_Link);
            test.validateElementIsPresent(driver, Elements.Language_Menu_Russian);
        }
    }
}
