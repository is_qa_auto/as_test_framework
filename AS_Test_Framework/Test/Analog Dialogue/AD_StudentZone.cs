﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Linq;

namespace AS_Test_Framework.AnalogDialogue
{

    [TestFixture]
    public class AD_StudentZOne : BaseSetUp
    {
        public AD_StudentZOne() : base() { }

        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", TestName = "Verify \"More StudentZone Articles\" is present and working in EN Loacle", Category = "Smoke_ADIWeb")]
        [TestCase("cn", TestName = "Verify \"More StudentZone Articles\" is present and working in CN Locale", Category = "Smoke_ADIWeb")]
        [TestCase("jp", TestName = "Verify \"More StudentZone Articles\" is present and working in JP Locale", Category = "Smoke_ADIWeb")]
        public void AD_ShortHeader_Elements(string Locale)
        {
            string initial_steps = "1. Go to " + Configuration.Env_Url + Locale + "/analog-dialogue.html";
            string scenario = "";

            action.INavigate(driver, Configuration.Env_Url + Locale + "/analog-dialogue.html");
            scenario = "Verify that View More button is present in student zone section";
            test.validateElementIsPresentv2(driver, Elements.AD_StudentZone_ViewMore_Btn, scenario , initial_steps);

            action.IClick(driver, Elements.AD_StudentZone_ViewMore_Btn);
            scenario = "Verify that user will be redirected to analog dialogue search when clicking view more button";
            test.validateStringInstancev2(driver, driver.Url, Locale + "/analog-dialogue/search.html?q=studentzone", scenario, initial_steps + "<br>2. Click on View more button in student zone section");
        }
    }
}
