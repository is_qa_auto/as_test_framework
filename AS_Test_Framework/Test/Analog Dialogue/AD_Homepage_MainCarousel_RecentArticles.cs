﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Linq;

namespace AS_Test_Framework.AnalogDialogue
{

    [TestFixture]
    public class AD_Homepage_MainCarousel_RecentArticles : BaseSetUp
    {
        public AD_Homepage_MainCarousel_RecentArticles() : base() { }

        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", TestName = "Verify that the view recent article link is present and workng in EN Locale")]
        [TestCase("cn", TestName = "Verify that the view recent article link is present and workng in CN Locale")]
        [TestCase("jp", TestName = "Verify that the view recent article link is present and workng in JP Locale")]
        public void AD_Main_Carousel(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/analog-dialogue.html");
            test.validateElementIsPresent(driver, Elements.AD_Carousel_ViewRecent_Link);
            action.IClick(driver, Elements.AD_Carousel_ViewRecent_Link);
            test.validateStringInstance(driver, driver.Url, Configuration.Env_Url + Locale + "/analog-dialogue/search.html");
        }
    }
}
