﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Linq;

namespace AS_Test_Framework.AnalogDialogue
{

    [TestFixture]
    public class AD_Homepage_PastJournals : BaseSetUp
    {
        public AD_Homepage_PastJournals() : base() { }

        [Test, Category("Analog Dialogue"), Retry(2), Category("Core")]
        [TestCase("en", "Past Journals", TestName = "Verify the Section Title is present in EN Locale", Category = "Smoke_ADIWeb")]
        [TestCase("cn", "过往期刊", TestName = "Verify the Section Title is present in CN Locale", Category = "Smoke_ADIWeb")]
        [TestCase("jp", "バックナンバー", TestName = "Verify the Section Title is present  in JP Locale", Category = "Smoke_ADIWeb")]
        public void AD_PastJournals(string Locale, string Label)
        {
            string initial_steps = "1. Go to " + Configuration.Env_Url + Locale + "/analog-dialogue.html";
            string scenario = "Verify that Past Journal Title is present and correct";

            action.INavigate(driver, Configuration.Env_Url + Locale + "/analog-dialogue.html");
            test.validateStringIsCorrectv2(driver, Elements.AD_PastJournals_Section_Title, Label, scenario, initial_steps);
        }

        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", TestName = "Verify the slider is present in EN Locale")]
        [TestCase("cn", TestName = "Verify the slider is present in CN Locale")]
        [TestCase("jp", TestName = "Verify the slider is present in JP Locale")]
        public void AD_PastJournals_2(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/analog-dialogue.html");
            test.validateElementIsPresent(driver, Elements.AD_PastJournals_Slider);
        }

        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", TestName = "Verify the Archives button is present and working as expected in EN Locale")]
        [TestCase("cn", TestName = "Verify the Archives button is present and working as expected in CN Locale")]
        [TestCase("jp", TestName = "Verify the Archives button is present and working as expected in JP Locale")]
        public void AD_PastJournals_3(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/analog-dialogue.html");
            test.validateElementIsPresent(driver, Elements.AD_PastJournals_Archives);
            action.IClick(driver, Elements.AD_PastJournals_Archives);
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/analog-dialogue/archives.html");
        }

        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", TestName = "Verify the volume label is present in EN Locale")]
        [TestCase("cn", TestName = "Verify the volume label is present in CN Locale")]
        [TestCase("jp", TestName = "Verify the volume label is present in JP Locale")]
        public void AD_PastJournals_4(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/analog-dialogue.html");
            test.validateElementIsPresent(driver, By.CssSelector("div[class='row past-journals'] * ul>li>a>span"));
        }
    }
}
