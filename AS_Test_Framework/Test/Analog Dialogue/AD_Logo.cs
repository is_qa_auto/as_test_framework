﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Linq;

namespace AS_Test_Framework.AnalogDialogue
{

    [TestFixture]
    public class AD_Logo : BaseSetUp
    {
        public AD_Logo() : base() { }

        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", TestName = "Verify the AD Logo is present in EN Locale")]
        [TestCase("cn", TestName = "Verify the AD Logo is present in CN Locale")]
        [TestCase("jp", TestName = "Verify the AD Logo is present in JP Locale")]
        public void AD_ShortHeader_Elements(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/analog-dialogue.html");
            test.validateElementIsPresent(driver, By.CssSelector("div[class='col-xs-8 col-md-6 col-lg-5']>a>img"));
            action.IClick(driver, By.CssSelector("div[class='col-xs-8 col-md-6 col-lg-5']>a>img"));
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/analog-dialogue.html");
        }
    }
}
