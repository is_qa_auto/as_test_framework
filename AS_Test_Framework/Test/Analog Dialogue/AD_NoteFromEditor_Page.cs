﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Linq;

namespace AS_Test_Framework.AnalogDialogue
{

    [TestFixture]
    public class AD_NoteFromEditor_Page : BaseSetUp
    {
        public AD_NoteFromEditor_Page() : base() { }

        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", "A Note From the Editor", TestName = "Verify Editor's Note page in EN Locale")]
        [TestCase("cn", "编者寄语", TestName = "Verify Editor's Note page in CN Locale")]
        [TestCase("jp", "編集後記", TestName = "Verify Editor's Note page in JP Locale")]
        public void AD_Main_Carousel(string Locale, string Label)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/analog-dialogue/note-from-the-editor.html");
            test.validateElementIsPresent(driver, Elements.AD_AboutAD_NavigationBar);
            test.validateStringIsCorrect(driver, By.CssSelector("section[class='col-lg-8 col-md-8 col-sm-12 col-xs-12 leftCol']>h1"), Label);
            test.validateElementIsPresent(driver, Elements.AD_AboutAD_Meet_the_Editor_Section);
        }
    }
}
