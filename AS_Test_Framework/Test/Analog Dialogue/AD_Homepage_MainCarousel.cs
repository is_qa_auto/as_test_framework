﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Linq;

namespace AS_Test_Framework.AnalogDialogue
{

    [TestFixture]
    public class AD_Homepage_MainCarousel : BaseSetUp
    {
        public AD_Homepage_MainCarousel() : base() { }

        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", TestName = "Verify the main article in AD carousel is present in EN Locale")]
        [TestCase("cn", TestName = "Verify the main article in AD carousel is present in CN Locale")]
        [TestCase("jp", TestName = "Verify the main article in AD carousel is present in JP Locale")]
        public void AD_Main_Carousel(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/analog-dialogue.html");
            test.validateElementIsPresent(driver, Elements.AD_Carousel_MainArticle);
            test.validateElementIsPresent(driver, Elements.AD_Carousel_MainArticle_Img);
            test.validateElementIsPresent(driver, Elements.AD_Carousel_MainArticle_Date);
            test.validateElementIsPresent(driver, Elements.AD_Carousel_MainArticle_Title);
            test.validateElementIsPresent(driver, Elements.AD_Carousel_MainArticle_Author_Img);
            test.validateElementIsPresent(driver, Elements.AD_Carousel_MainArticle_Author_Name);
        }

        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", TestName = "Verify the sub article in AD carousel is present in EN Locale")]
        [TestCase("cn", TestName = "Verify the sub article in AD carousel is present in CN Locale")]
        [TestCase("jp", TestName = "Verify the sub article in AD carousel is present in JP Locale")]
        public void AD_Sub_Carousel(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/analog-dialogue.html");
            test.validateElementIsPresent(driver, Elements.AD_Carousel_SubArticle_1);
            test.validateElementIsPresent(driver, Elements.AD_Carousel_SubArticle_2);
            test.validateElementIsPresent(driver, Elements.AD_Carousel_SubArticle_1_Author);
            test.validateElementIsPresent(driver, Elements.AD_Carousel_SubArticle_2_Author);
        }
    }
}
