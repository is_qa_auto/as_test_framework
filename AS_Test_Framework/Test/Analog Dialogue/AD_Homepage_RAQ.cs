﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Linq;

namespace AS_Test_Framework.AnalogDialogue
{

    [TestFixture]
    public class AD_Homepage_RAQ : BaseSetUp
    {
        public AD_Homepage_RAQ() : base() { }

        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", "Rarely Asked Questions", TestName = "Verify RAQ Section in EN Locale", Category = "Smoke_ADIWeb")]
        [TestCase("cn", "非常见问题解答", TestName = "Verify RAQ Section in CN Locale", Category = "Smoke_ADIWeb")]
        [TestCase("jp", "アナログ・デバイセズに寄せられた珍問／難問集", TestName = "Verify RAQ Section in JP Locale", Category = "Smoke_ADIWeb")]
        public void AD_RAQ1(string Locale, string Label)
        {
            string initial_steps = "1. Go to " + Configuration.Env_Url + Locale + "/analog-dialogue.html";
            string scenario = "";

            action.INavigate(driver, Configuration.Env_Url + Locale + "/analog-dialogue.html");
            scenario = "Verify that RAQ Title is present and correct";
            test.validateStringIsCorrectv2(driver, Elements.AD_RAQ_Title, Label, scenario, initial_steps);

            scenario = "Verify that RAQ Image is present";
            test.validateElementIsPresentv2(driver, Elements.AD_RAQ_Img, scenario, initial_steps);

            scenario = "Verify that View the answer button is present in RAQ";
            test.validateElementIsPresentv2(driver, Elements.AD_RAQ_ViewTheAnswer, scenario, initial_steps);

            //scenario = "Verify that RAQ"
            //test.validateElementIsPresentv2(driver, Elements.AD_RAQ_Img);
            scenario = "Verify that RAQ slider/expander button is present";
            test.validateElementIsPresentv2(driver, Elements.AD_RAQ_Expander, scenario, initial_steps);

            scenario = "Verify that RAQ description is present";
            test.validateElementIsPresentv2(driver, By.CssSelector("div[class='raq-content'] * div[class='col-md-12']>p:nth-of-type(2)"), scenario, initial_steps);

            scenario = "Verify that author is present in RAQ";
            test.validateElementIsPresentv2(driver, By.CssSelector("div[class='raq-content'] * div[class='col-md-12']>p[class='byline']"), scenario, initial_steps);
        }

        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", TestName = "Verify the VIEW THE ANSWER button is present and working in EN Locale")]
        [TestCase("cn", TestName = "Verify the VIEW THE ANSWER button is present and working in CN Locale")]
        [TestCase("jp", TestName = "Verify the VIEW THE ANSWER button is present and working in JP Locale")]
        public void AD_RAQ2(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/analog-dialogue.html");
            action.IClick(driver, Elements.AD_RAQ_ViewTheAnswer);
            test.validateStringInstance(driver, driver.Url, Configuration.Env_Url + Locale + "/analog-dialogue/raqs/");
        }

        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", TestName = "Verify user can extract/collapse RAQ section in EN Locale")]
        [TestCase("cn", TestName = "Verify user can extract/collapse RAQ section in CN Locale")]
        [TestCase("jp", TestName = "Verify user can extract/collapse RAQ section in JP Locale")]
        public void AD_RAQ3(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/analog-dialogue.html");
            action.IClick(driver, Elements.AD_RAQ_Expander);
            test.validateElementIsPresent(driver, By.CssSelector("div[class='raq-wrapper expanded']"));
            test.validateCountIsEqual(driver, 2, util.GetCount(driver, By.CssSelector("div[class='row more-raqs']")));
            test.validateElementIsPresent(driver, By.CssSelector("a[class='btn btn-default home-btn']:nth-of-type(2)"));
            test.validateElementIsPresent(driver, By.CssSelector("div[class='row more-raqs']:nth-of-type(1) * Img"));
            test.validateElementIsPresent(driver, By.CssSelector("div[class='row more-raqs']:nth-of-type(1) * a>strong"));
            action.IClick(driver, Elements.AD_RAQ_Expander);
            test.validateElementIsNotPresent(driver, By.CssSelector("div[class='raq-wrapper expanded']"));
        }

        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", TestName = "Verify user will be redirected to RAQ page when clicking More RAQ Button in EN Locale")]
        [TestCase("cn", TestName = "Verify user will be redirected to RAQ page when clicking More RAQ Button in CN Locale")]
        [TestCase("jp", TestName = "Verify user will be redirected to RAQ page when clicking More RAQ Button in JP Locale")]
        public void AD_RAQ4(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/analog-dialogue.html");
            action.IClick(driver, Elements.AD_RAQ_Expander);
            action.IClick(driver, By.CssSelector("a[class='btn btn-default home-btn']:nth-of-type(2)"));
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/analog-dialogue/raqs.html");
        }
    }
}
