﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Linq;
using System;
using System.Threading;

namespace AS_Test_Framework.AnalogDialogue
{

    [TestFixture]
    public class AD_Subscribe_ExistingBlockedAndExpiredUser : BaseSetUp
    {
        public AD_Subscribe_ExistingBlockedAndExpiredUser() : base() { }

        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", TestName = "Verify the Subscription using an already subscribed email address in EN Locale")]
        [TestCase("cn", TestName = "Verify the Subscription using an already subscribed email address in CN Locale")]
        [TestCase("jp", TestName = "Verify the Subscription using an already subscribed email address in JP Locale")]
        public void AD_Subscribe1(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/analog-dialogue.html");
            action.IClick(driver, Elements.AD_Subscribe_Btn);
            action.IType(driver, Elements.AD_Subscribe_EmailAddress_TextField, "aries.sorosoro@analog.com");
            action.IClick(driver, Elements.Registration_TermsOfUse_ChxBox);
            action.IClick(driver, Elements.Registration_Consent_Comm);
            action.IClick(driver, Elements.AD_Subscribe_Submit_Btn);

            if (Locale.Equals("en"))
            {
                test.validateStringIsCorrect(driver, By.CssSelector("div[class='alert alert-danger']"), "Good news! You already have an Analog Dialogue subscription. Watch for the next issue in your inbox.");
            }
            else
            {
                test.validateElementIsPresent(driver, By.CssSelector("div[class='alert alert-danger']"));
            }
        }


        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", TestName = "Verify the Subscription using a blocked email address in EN Locale")]
        [TestCase("cn", TestName = "Verify the Subscription using a blocked email address in CN Locale")]
        [TestCase("jp", TestName = "Verify the Subscription using a blocked email address in JP Locale")]
        public void AD_Subscribe2(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/analog-dialogue.html");
            action.IClick(driver, Elements.AD_Subscribe_Btn);
            action.IType(driver, Elements.AD_Subscribe_EmailAddress_TextField, "blocked_email_for_ad@mailinator.com");
            action.IClick(driver, Elements.Registration_TermsOfUse_ChxBox);
            action.IClick(driver, Elements.Registration_Consent_Comm);
            action.IClick(driver, Elements.AD_Subscribe_Submit_Btn);
            if (Locale.Equals("en"))
            {
                test.validateStringIsCorrect(driver, By.CssSelector("div[class='alert alert-danger']"), "The email you provided has been blocked, but we may be able to help. Please contact us at external.webmaster@analog.com");
            }
            else {
                test.validateElementIsPresent(driver, By.CssSelector("div[class='alert alert-danger']"));
            }
        }

        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", TestName = "Verify the Subscription using an expired email address in EN Locale")]
        [TestCase("cn", TestName = "Verify the Subscription using an expired email address in CN Locale")]
        [TestCase("jp", TestName = "Verify the Subscription using an expired email address in JP Locale")]
        public void AD_Subscribe3(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/analog-dialogue.html");
            action.IClick(driver, Elements.AD_Subscribe_Btn);
            action.IType(driver, Elements.AD_Subscribe_EmailAddress_TextField, "expired_email_for_ad@mailinator.com");
            action.IClick(driver, Elements.Registration_TermsOfUse_ChxBox);
            action.IClick(driver, Elements.Registration_Consent_Comm);
            action.IClick(driver, Elements.AD_Subscribe_Submit_Btn);
            if (Locale.Equals("en"))
            {
                test.validateStringIsCorrect(driver, By.CssSelector("div[class='alert alert-danger']"), "Your password has expired, but help is available. Please contact us at external.webmaster@analog.com");
            }
            else
            {
                test.validateElementIsPresent(driver, By.CssSelector("div[class='alert alert-danger']"));
            }
        }
    }
}
