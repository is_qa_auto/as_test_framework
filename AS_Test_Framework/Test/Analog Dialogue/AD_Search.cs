﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Linq;

namespace AS_Test_Framework.AnalogDialogue
{

    [TestFixture]
    public class AD_Search : BaseSetUp
    {
        public AD_Search() : base() { }

        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", TestName = "Verify Search ghost text is present and correct in EN Locale")]
        public void AD_Search_Elements1(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/analog-dialogue.html");
            test.validateString(driver, "Search Analog Dialogue", util.ReturnAttribute(driver, Elements.AD_Search_TextField, "placeholder"));
        }

        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", TestName = "Verify clear search button is present and working in EN Locale")]
        public void AD_Search_Elements2(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/analog-dialogue.html");
            action.IType(driver, Elements.AD_Search_TextField, "Test");
            test.validateElementIsPresent(driver, Elements.AD_ClearSearch_Btn);
            action.IClick(driver, Elements.AD_ClearSearch_Btn);
            test.validateString(driver, "", util.ReturnAttribute(driver, Elements.AD_Search_TextField, "value"));
            action.IType(driver, Elements.AD_Search_TextField, "Test");
            action.IDeleteValueOnFieldUsingBackSpace(driver, Elements.AD_Search_TextField, "Test");
            test.validateString(driver, "", util.ReturnAttribute(driver, Elements.AD_Search_TextField, "value"));

        }

        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", "Please initiate new search", "No results found.", TestName = "Verify when no input or invalid string is entered in the Search text box in EN Locale")]
        [TestCase("cn", "请开始新的搜索", "无搜索结果。", TestName = "Verify when no input or invalid string is entered in the Search text box in CN Locale")]
        [TestCase("jp", "新しいキーワードで再検索してください", "お探しのキーワードで検索されるページがありませんでした", TestName = "Verify when no input or invalid string is entered in the Search text box in JP Locale")]

        public void AD_Search_Elements3(string Locale, string NoResultString, string NoResultString2)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/analog-dialogue.html");
            action.IClick(driver, Elements.AD_Search_Btn);
            test.validateStringIsCorrect(driver, Elements.GlobalSearchResults_NoSearchResults_Txt, NoResultString);
            action.IType(driver, Elements.AD_Search_TextField, "ASDADSADAS");
            action.IClick(driver, Elements.AD_Search_Btn);
            test.validateStringIsCorrect(driver, Elements.GlobalSearchResults_NoSearchResults_Txt, NoResultString2);
        }

        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", TestName = "Verify when valid string is entered in the Search text box in EN Locale", Category = "Smoke_ADIWeb")]
        [TestCase("cn", TestName = "Verify when valid string is entered in the Search text box in CN Locale", Category = "Smoke_ADIWeb")]
        [TestCase("jp", TestName = "Verify when valid string is entered in the Search text box in JP Locale", Category = "Smoke_ADIWeb")]

        public void AD_Search_Elements4(string Locale)
        {
            string initial_step = "1. Go to " + Configuration.Env_Url + Locale + "/analog-dialogue.html<br>"
                          +   "<br>2. Enter Keyword \"weenies\" in the analog dialogue search textbox"
                          +   "<br>3. Click Search Button";
            string scenario = "";

            action.INavigate(driver, Configuration.Env_Url + Locale + "/analog-dialogue.html");
            action.IType(driver, Elements.AD_Search_TextField, "weenies");
            action.IClick(driver, Elements.AD_Search_Btn);

            scenario = "Verify that user can do search in analog dialogue page";
            test.validateElementIsPresentv2(driver, Elements.GlobalSearchResults_SearchResultList, scenario, initial_step);
        }

        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", TestName = "Verify auto-suggest is displaying when user enters a keyword in search textbox - EN Locale")]
        [TestCase("cn", TestName = "Verify auto-suggest is displaying when user enters a keyword in search textbox - CN Locale")]
        [TestCase("jp", TestName = "Verify auto-suggest is displaying when user enters a keyword in search textbox - JP Locale")]

        public void AD_Search_Elements5(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/analog-dialogue.html");
            action.IType(driver, Elements.AD_Search_TextField, "MA");
            test.validateElementIsPresent(driver, Elements.AD_AutoSuggest);
            action.IDeleteValueOnFields(driver, Elements.AD_Search_TextField);
            action.IType(driver, Elements.AD_Search_TextField, "*");
            test.validateElementIsPresent(driver, Elements.AD_AutoSuggest);
            action.IClick(driver, Elements.AD_Search_Btn);
            test.validateElementIsNotPresent(driver, By.CssSelector("li[class*='active'][data-facetcategory='multipleanalogdialogtypeselection_sm']"));
        }

        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", TestName = "Verify search by author is working")]
        public void AD_Search_Filter6(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/analog-dialogue.html");
            action.IType(driver, Elements.AD_Search_TextField, "Victor Khasiev");
            action.IClick(driver, Elements.AD_Search_Btn);
            test.validateStringIsCorrect(driver, By.CssSelector("p[class='author']"), "By Victor Khasiev");

            action.IType(driver, Elements.AD_Search_TextField, "John P. O'Connell");
            action.IClick(driver, Elements.AD_Search_Btn);
            test.validateStringIsCorrect(driver, By.CssSelector("p[class='author']"), "By John P. O'Connell");
        }
    }
}
