﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Linq;

namespace AS_Test_Framework.AnalogDialogue
{

    [TestFixture]
    public class AD_AboutAD : BaseSetUp
    {
        public AD_AboutAD() : base() { }

        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", TestName = "Verify the elements on the About Page is present in EN Locale")]
        [TestCase("cn", TestName = "Verify the elements on the About Page is present in CN Locale")]
        [TestCase("jp", TestName = "Verify the elements on the About Page is present in JP Locale")]
        public void AD_AboutAD_Elements(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/analog-dialogue.html");
            action.IClick(driver, By.CssSelector("ul[class='nav navbar-nav']>li:nth-child(1)>a"));
            test.validateElementIsPresent(driver, Elements.AD_AboutAD_Article);
            test.validateElementIsPresent(driver, Elements.AD_AboutAD_Meet_the_Editor_Section);
            test.validateElementIsPresent(driver, Elements.AD_AboutAD_NavigationBar);
            test.validateStringIsCorrect(driver, By.CssSelector("ul[class='nav navbar-nav']>li>div[class='rr rr-right']>div"), "1967");
        }


        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", TestName = "Verify Meet Editor contents in EN Locale")]
        [TestCase("cn", TestName = "Verify Meet Editor contents in CN Locale")]
        [TestCase("jp", TestName = "Verify Meet Editor contents in JP Locale")]
        public void AD_AboutAD_MeetTheEdior_Elements(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/analog-dialogue.html");
            action.IClick(driver, By.CssSelector("ul[class='nav navbar-nav']>li:nth-child(1)>a"));
            test.validateElementIsPresent(driver, By.CssSelector("section[class='about meet-the-editor']>img"));
            test.validateElementIsPresent(driver, By.CssSelector("img[class='signature']"));
        }
    }
}
