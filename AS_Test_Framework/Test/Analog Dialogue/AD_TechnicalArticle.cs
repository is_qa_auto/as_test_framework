﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Linq;

namespace AS_Test_Framework.AnalogDialogue
{

    [TestFixture]
    public class AD_TechArticle : BaseSetUp
    {
        public AD_TechArticle() : base() { }

        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", TestName = "Verify the Images in a Technical Article Page")]
        public void AD_TechnicalArticle_Image(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/technical-articles/high-efficiency-6-phase-80a-step-down-regulator-using-ltm4630-x3.html");
            action.IClick(driver, By.CssSelector("div[class='container article-container'] div[class='image-container']>a"));
            test.validateElementIsPresent(driver, By.CssSelector("div[id='image-modal']"));
            action.IClick(driver, By.CssSelector("div[id='image-modal'] button[class='close']"));
            test.validateElementIsNotPresent(driver, By.CssSelector("div[id='image-modal']"));
            action.IClick(driver, By.CssSelector("div[class='container article-container'] div[class='image-container'] div[class='icon-zoom']"));
            test.validateElementIsPresent(driver, By.CssSelector("div[id='image-modal']"));
        }

        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", TestName = "Verify that Social Media Widgets are present in a Technical Article Page")]
        public void AD_TechnicalArticle_Social(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/technical-articles/high-efficiency-6-phase-80a-step-down-regulator-using-ltm4630-x3.html");
            test.validateElementIsPresent(driver, Elements.AD_RAQ_Page_SocialMedia_Widget);
            test.validateElementIsPresent(driver, Elements.AD_RAQ_Page_SocialMedia_FB);
            test.validateElementIsPresent(driver, Elements.AD_RAQ_Page_SocialMedia_Line);
            test.validateElementIsPresent(driver, Elements.AD_RAQ_Page_SocialMedia_LinkedIn);
            test.validateElementIsPresent(driver, Elements.AD_RAQ_Page_SocialMedia_Twitter);
        }

        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", TestName = "Verify author link is working as expected in article pages")]
        public void AD_TechArticle_Author(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/technical-articles/high-efficiency-6-phase-80a-step-down-regulator-using-ltm4630-x3.html");
            string authorAnchor = util.ReturnAttribute(driver, Elements.AD_RAQ_Page_Author, "href");
            action.IClick(driver, Elements.AD_RAQ_Page_Author);
            test.validateScreenByUrl(driver, authorAnchor);

        }

        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", TestName = "Verify the Author Section is present in Article Pages")]
        public void AD_RAQ3(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/technical-articles/high-efficiency-6-phase-80a-step-down-regulator-using-ltm4630-x3.html");
            test.validateElementIsPresent(driver, Elements.AD_RAQ_Page_Author_Img);
            test.validateElementIsPresent(driver, Elements.AD_RAQ_Page_Author_Name);
            test.validateElementIsPresent(driver, Elements.AD_RAQ_Page_Author_Email);
            test.validateElementIsPresent(driver, Elements.AD_RAQ_Page_Author_Details);
        }
    }
}
