﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.AnalogDialogue
{

    [TestFixture]
    public class AD_Article_Page : BaseSetUp
    {
        public AD_Article_Page() : base() { }

        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", TestName = "Verify Article page element")]
        public void AD_Art1(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/analog-dialogue/articles/complex-mixers-zif-architecture-advanced-algorithms-black-magic-next-generation-sdr-transceivers.html");
            test.validateElementIsPresent(driver, Elements.AD_AboutAD_NavigationBar);
            test.validateElementIsPresent(driver, By.CssSelector("div[class='article-info-left']>ul[class='nav navbar-nav']>li>div[class='rr rr-left']"));
            test.validateElementIsPresent(driver, By.CssSelector("div[class='article-info-left']>ul[class='nav navbar-nav']>li>div[class='rr rr-left']"));
            test.validateElementIsPresent(driver, By.CssSelector("section[class='col-lg-8 col-md-8 col-sm-12 col-xs-12 leftCol']>h1"));
            test.validateElementIsPresent(driver, Elements.AD_RAQ_Page_Author);
            test.validateElementIsPresent(driver, Elements.AD_RAQ_Page_Download);
        }

        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", TestName = "Verify author link is working as expected")]
        public void AD_RAQ2(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/analog-dialogue/raqs/raq-issue-137.HTML");
            string authorAnchor = util.ReturnAttribute(driver, Elements.AD_RAQ_Page_Author, "href");
            action.IClick(driver, Elements.AD_RAQ_Page_Author);
            test.validateScreenByUrl(driver, authorAnchor);

        }

        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", TestName = "Verify the Author Section")]
        public void AD_RAQ3(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/analog-dialogue/raqs/raq-issue-151.html");
            test.validateElementIsPresent(driver, Elements.AD_RAQ_Page_Author_Img);
            test.validateElementIsPresent(driver, Elements.AD_RAQ_Page_Author_Name);
            test.validateElementIsPresent(driver, Elements.AD_RAQ_Page_Author_Email);
            test.validateElementIsPresent(driver, Elements.AD_RAQ_Page_Author_Details);
        }

        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", TestName = "Verify the Lightbox function on RTE")]
        public void AD_RAQ5(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/analog-dialogue/articles/adi-tof-depth-sensing-technology-enabling-new-and-emerging-applications-beyond-consumer.html");
            action.IClick(driver, By.CssSelector("div[class='icon-zoom']"));
            test.validateElementIsPresent(driver, By.CssSelector("div[id='image-modal']"));
        }

        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", TestName = "Verify if the Add description in \"myAnalog widget\" is working")]
        public void AD_RAQ6(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/analog-dialogue/articles/adi-tof-depth-sensing-technology-enabling-new-and-emerging-applications-beyond-consumer.html");
            action.IClick(driver, Elements.AD_MyAnalog_Widget);
            //action.IClick(driver, Elements.MyAnalog_Widget_LogInToMyAnalog_Btn);
            action.ILogin(driver, "aries.sorosoro@analog.com", "Test_1234");
            //action.IClick(driver, Elements.AD_MyAnalog_Widget);
            action.IClick(driver, Elements.MyAnalog_Widget_CreateNewProject_Tab);
            action.IClick(driver, Elements.MyAnalog_Widget_AddDescription_Button);
            test.validateElementIsPresent(driver, Elements.MyAnalog_Widget_AddDescription_TextArea);
        }
    }
}
