﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Linq;

namespace AS_Test_Framework.AnalogDialogue
{

    [TestFixture]
    public class AD_Homepage_NoteFromEditor : BaseSetUp
    {
        public AD_Homepage_NoteFromEditor() : base() { }

        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", "A Note From\r\nthe EDITOR", TestName = "Verify Editor's Note section is present and workng in EN Locale", Category = "Smoke_ADIWeb")]
        [TestCase("cn", "编者寄语", TestName = "Verify Editor's Note section is present and workng in CN Locale", Category = "Smoke_ADIWeb")]
        [TestCase("jp", "編集後記", TestName = "Verify Editor's Note section is present and workng in JP Locale", Category = "Smoke_ADIWeb")]
        public void AD_Main_Carousel(string Locale, string Label)
        {
            string initial_steps = "1. Go to " + Configuration.Env_Url + Locale + "/analog-dialogue.html";
            string scenario = "";

            action.INavigate(driver, Configuration.Env_Url + Locale + "/analog-dialogue.html");
            
            scenario = "Verify that Note From the Editor section is present";
            test.validateElementIsPresentv2(driver, Elements.AD_NoteFromEditor, scenario, initial_steps);

            scenario = "Verify that Note from editor header is present and correct";
            test.validateStringIsCorrectv2(driver, Elements.AD_NoteFromEditor_Label, Label, scenario, initial_steps);

            scenario = "Verify that Note from editor icon is present";
            test.validateElementIsPresentv2(driver, Elements.AD_NoteFromEditor_Icon, scenario , initial_steps);

            scenario = "Verify that note from editor content is present";
            test.validateElementIsPresentv2(driver, Elements.AD_NoteFromEditor_Content, scenario, initial_steps);

            scenario = "Verify that More button is present in note from editor section";
            test.validateElementIsPresentv2(driver, Elements.AD_NoteFromEditor_More_Btn, scenario, initial_steps);

            scenario = "Verify that user will be redirected to Note From Editor page when clicking more button";
            action.IClick(driver, Elements.AD_NoteFromEditor_More_Btn);
            test.validateScreenByUrlv2(driver, Configuration.Env_Url + Locale + "/analog-dialogue/note-from-the-editor.html", scenario, initial_steps + "<br>2. Click More button in Note from editor section");
        }
    }
}
