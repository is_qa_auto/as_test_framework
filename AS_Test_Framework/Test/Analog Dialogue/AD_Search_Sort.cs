﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Linq;
using System;

namespace AS_Test_Framework.AnalogDialogue
{

    [TestFixture]
    public class AD_Search_Sort : BaseSetUp
    {
        public AD_Search_Sort() : base() { }

        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", TestName = "Verify Newest, Oldest and Relevancy is available in Sort Dropdown in Search result page of Analog Dialogue EN Locale")]
        [TestCase("cn", TestName = "Verify Newest, Oldest and Relevancy is available in Sort Dropdown in Search result page of Analog Dialogue CN Locale")]
        [TestCase("jp", TestName = "Verify Newest, Oldest and Relevancy is available in Sort Dropdown in Search result page of Analog Dialogue JP Locale")]
        public void AD_Search_Sort1(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/analog-dialogue/search.html?q=*");
            test.validateElementIsPresent(driver, Elements.GlobalSearchResults_Sort_Menu);
            test.validateElementIsPresent(driver, Elements.GlobalSearchResults_Sort_Menu_Newest);
            test.validateElementIsPresent(driver, Elements.GlobalSearchResults_Sort_Menu_Oldest);
            test.validateElementIsPresent(driver, Elements.GlobalSearchResults_Sort_Menu_Relevancy);
        }

        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", TestName = "Verify user can sort Analog Dialogue search result based on Oldest date - EN Locale")]
        [TestCase("cn", TestName = "Verify user can sort Analog Dialogue search result based on Oldest date - CN Locale")]
        [TestCase("jp", TestName = "Verify user can sort Analog Dialogue search result based on Oldest date - JP Locale")]
        public void AD_Search_Sort2(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/analog-dialogue/search.html?q=*");
            action.ISelectFromDropdown(driver, Elements.GlobalSearchResults_Sort_Menu, "ASC");
            int searchCount = util.GetCount(driver, By.CssSelector("div[class='search-results-items']>ul>li"));
            object[] ArticleDates = new object[searchCount];

            for (int x = 1, y = 0; x <= searchCount; x++, y++)
            {
                DateTime d;
                DateTime.TryParse("01 " + util.GetText(driver, By.CssSelector("div[class='search-results-items']>ul>li:nth-child(" + x + ") * p[class='date'] ")), out d);
                ArticleDates[y] = d;
                Console.WriteLine(ArticleDates[y]);
            }

            test.validateStringIfInAscenading(ArticleDates);
        }

        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", TestName = "Verify user can sort Analog Dialogue search result based on Newest date - EN Locale")]
        [TestCase("cn", TestName = "Verify user can sort Analog Dialogue search result based on Newest date - CN Locale")]
        [TestCase("jp", TestName = "Verify user can sort Analog Dialogue search result based on Newest date - JP Locale")]
        public void AD_Search_Sort3(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/analog-dialogue/search.html?q=*");
            action.ISelectFromDropdown(driver, Elements.GlobalSearchResults_Sort_Menu, "DESC");
            int searchCount = util.GetCount(driver, By.CssSelector("div[class='search-results-items']>ul>li"));
            object[] ArticleDates = new object[searchCount];

            for (int x = 1, y = 0; x <= searchCount; x++, y++)
            {
                DateTime d;
                DateTime.TryParse("01 " + util.GetText(driver, By.CssSelector("div[class='search-results-items']>ul>li:nth-child(" + x + ") * p[class='date'] ")), out d);
                ArticleDates[y] = d;
                Console.WriteLine(ArticleDates[y]);
            }

            test.validateStringIfInDescending(ArticleDates);
        }
    }
}
