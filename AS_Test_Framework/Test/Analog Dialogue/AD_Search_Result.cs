﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Linq;

namespace AS_Test_Framework.AnalogDialogue
{

    [TestFixture]
    public class AD_Search_Result : BaseSetUp
    {
        public AD_Search_Result() : base() { }

        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", TestName = "Verify the Search Filters in Analog Dialogue EN Locale")]
        [TestCase("cn", TestName = "Verify the Search Filters in Analog Dialogue CN Locale")]
        [TestCase("jp", TestName = "Verify the Search Filters in Analog Dialogue JP Locale")]
        public void AD_Search_Filter1(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/analog-dialogue.html");
            action.IType(driver, Elements.AD_Search_TextField, "*");
            action.IClick(driver, Elements.AD_Search_Btn);
            test.validateElementIsPresent(driver, Elements.AD_Search_Reset_Filters);
            test.validateElementIsPresent(driver, By.CssSelector("li[data-facetcategory='multipleanalogdialogtypeselection_sm']"));
            test.validateElementIsPresent(driver, By.CssSelector("li[data-facetcategory='market_l1_fac_sm']"));
            test.validateElementIsPresent(driver, By.CssSelector("li[data-facetcategory='prod_cat_l1_fac_sm']"));
        }

        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", TestName = "Verify the Search Results for Articles in Analog Dialogue EN Locale")]
        [TestCase("cn", TestName = "Verify the Search Results for Articles in Analog Dialogue CN Locale")]
        [TestCase("jp", TestName = "Verify the Search Results for Articles in Analog Dialogue JP Locale")]
        public void AD_Search_Filter2(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/analog-dialogue.html");
            action.IType(driver, Elements.AD_Search_TextField, "*");
            action.IClick(driver, Elements.AD_Search_Btn);
            action.IClick(driver, By.CssSelector("li[data-facetcategory='multipleanalogdialogtypeselection_sm']>a"));
            action.IClick(driver, By.CssSelector("li[data-facetcategory='multipleanalogdialogtypeselection_sm']>ul>li>a"));
            test.validateElementIsPresent(driver, By.CssSelector("div[class='search-results-items']>ul>li:nth-child(1) * img"));
            test.validateElementIsPresent(driver, By.CssSelector("div[class='search-results-items']>ul>li:nth-child(1) * h2[class='title']"));
            test.validateElementIsPresent(driver, By.CssSelector("div[class='search-results-items']>ul>li:nth-child(1) * p[class='date']"));
            test.validateElementIsPresent(driver, By.CssSelector("div[class='search-results-items']>ul>li:nth-child(1) * p[class='excerpt']"));
            test.validateElementIsPresent(driver, By.CssSelector("div[class='search-results-items']>ul>li:nth-child(1) * p[class='author']"));
            test.validateElementIsPresent(driver, By.CssSelector("div[class='search-results-items']>ul>li:nth-child(1) * p[class='volume']"));
            string Volume_No = util.GetText(driver, By.CssSelector("div[class='search-results-items']>ul>li:nth-child(1) * p[class='volume']")).Split(',')[0];
            action.IClick(driver, By.CssSelector("div[class='search-results-items']>ul>li:nth-child(1) * h2[class='title']>a"));
            test.validateStringIsCorrect(driver, By.CssSelector("div[class='rr rr-right']"), Volume_No.Replace("Volume", "VOL"));
        }

        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", TestName = "Verify the Search Results for RAQ in Analog Dialogue EN Locale")]
        [TestCase("cn", TestName = "Verify the Search Results for RAQ in Analog Dialogue CN Locale")]
        [TestCase("jp", TestName = "Verify the Search Results for RAQ in Analog Dialogue JP Locale")]
        public void AD_Search_Filter3(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/analog-dialogue.html");
            action.IType(driver, Elements.AD_Search_TextField, "*");
            action.IClick(driver, Elements.AD_Search_Btn);
            action.IClick(driver, By.CssSelector("li[data-facetcategory='multipleanalogdialogtypeselection_sm']>a"));
            action.IClick(driver, By.CssSelector("li[data-facetcategory='multipleanalogdialogtypeselection_sm']>ul>li:nth-child(2)>a"));
            test.validateElementIsPresent(driver, By.CssSelector("div[class='search-results-items']>ul>li:nth-child(1) * img"));
            test.validateElementIsPresent(driver, By.CssSelector("div[class='search-results-items']>ul>li:nth-child(1) * h2[class='title']"));
            test.validateElementIsPresent(driver, By.CssSelector("div[class='search-results-items']>ul>li:nth-child(1) * p[class='date']"));
            test.validateElementIsPresent(driver, By.CssSelector("div[class='search-results-items']>ul>li:nth-child(1) * p[class='excerpt']"));
            test.validateElementIsPresent(driver, By.CssSelector("div[class='search-results-items']>ul>li:nth-child(1) * p[class='author']"));
            test.validateElementIsPresent(driver, By.CssSelector("div[class='search-results-items']>ul>li:nth-child(1) * p[class='volume']"));
        }

        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", TestName = "Verify pagination is present in search result page EN Locale")]
        [TestCase("cn", TestName = "Verify pagination is present in search result page CN Locale")]
        [TestCase("jp", TestName = "Verify pagination is present in search result page JP Locale")]
        public void AD_Search_Filter4(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/analog-dialogue.html");
            action.IType(driver, Elements.AD_Search_TextField, "*");
            action.IClick(driver, Elements.AD_Search_Btn);
            test.validateElementIsPresent(driver, By.CssSelector("div[class='pagination-control pagination-top pull-right visible-desktop']"));
        }

        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", "Showing results for", "Show results instead for", TestName = "Verify that \"Show results instead for\" is being displayed in EN Locale")]
        [TestCase("cn", "显示的搜索结果是", "替代产品", TestName = "Verify that \"Show results instead for\" is being displayed in CN Locale")]
        [TestCase("jp", "次の検索結果を表示しています：", "元の検索キーワード：", TestName = "Verify that \"Show results instead for\" is being displayed in JP Locale")]
        public void AD_Search_Filter5(string Locale, string ResultFor, string ResultInstead)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/analog-dialogue.html");
            action.IType(driver, Elements.AD_Search_TextField, "engineerzne");
            action.IClick(driver, Elements.AD_Search_Btn);
            test.validateStringIsCorrect(driver, Elements.GlobalSearchResults_ShowingResultsFor_Txt, ResultFor + " engineerzone");
            test.validateStringIsCorrect(driver, Elements.GlobalSearchResults_ShowResultsInsteadFor_Txt, ResultInstead + " engineerzne");
            action.IClick(driver, By.CssSelector("div[class='suggestion-container']>h4>a"));
            test.validateElementIsNotPresent(driver, Elements.GlobalSearchResults_ShowingResultsFor_Txt);
            test.validateElementIsNotPresent(driver, Elements.GlobalSearchResults_ShowResultsInsteadFor_Txt);
        }

        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", TestName = "Verify Inner search is present in search result page EN Locale")]
        [TestCase("cn", TestName = "Verify Inner search is present in search result page CN Locale")]
        [TestCase("jp", TestName = "Verify Inner search is present in search result page JP Locale")]
        public void AD_Search_Filter6(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/analog-dialogue.html");
            action.IType(driver, Elements.AD_Search_TextField, "*");
            action.IClick(driver, Elements.AD_Search_Btn);
            test.validateElementIsPresent(driver, Elements.GlobalSearchResults_Search_Txtbox);
            test.validateString(driver, "Analog Dialogue Search", util.ReturnAttribute(driver, Elements.GlobalSearchResults_Search_Txtbox, "placeholder"));
        }

    }
}
