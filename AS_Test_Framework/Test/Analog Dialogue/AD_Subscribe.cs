﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Linq;
using System;
using System.Threading;

namespace AS_Test_Framework.AnalogDialogue
{

    [TestFixture]
    public class AD_Subscribe : BaseSetUp
    {
        public AD_Subscribe() : base() { }

        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", "SUBSCRIBE", "FOLLOW US", TestName = "Verify Subscribe is present and working in EN Locale")]
        [TestCase("cn", "订阅", "索取印刷版本", TestName = "Verify Subscribe is present and working in CN Locale")]
        [TestCase("jp", "英語ニュースレター", "日本語ニュースレター", TestName = "Verify Subscribe is present and working in JP Locale")]
        public void AD_Subscribe1(string Locale, string SubscribeLabel, string FollowUsLabel)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/analog-dialogue.html");
            test.validateElementIsPresent(driver, Elements.AD_Subscribe_Btn);
            action.IClick(driver, Elements.AD_Subscribe_Btn);
            test.validateElementIsPresent(driver, Elements.AD_Subscribe_Section);
            test.validateStringIsCorrect(driver, By.CssSelector("div[id='subscribe'] * div[class='col-lg-8 col-md-8 col-sm-12 col-xs-12']>h3"), SubscribeLabel);
            test.validateStringIsCorrect(driver, By.CssSelector("div[id='subscribe'] * div[class='col-lg-4 col-md-4 col-sm-12 col-xs-12']>h3"), FollowUsLabel);
            action.IClick(driver, Elements.AD_Subscribe_Btn);
            test.validateElementIsNotPresent(driver, Elements.AD_Subscribe_Section);
        }

        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", "Enter Your Email Address", TestName = "Verify SUBSCRIBE section in EN Locale")]
        [TestCase("cn", "输入您的邮箱地址",  TestName = "Verify SUBSCRIBE section in CN Locale")]
        [TestCase("jp", "メールアドレスを入力してください", TestName = "Verify SUBSCRIBE section in JP Locale")]
        public void AD_Subscribe2(string Locale, string GhostText)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/analog-dialogue.html");
            action.IClick(driver, Elements.AD_Subscribe_Btn);
            test.validateString(driver, GhostText, util.ReturnAttribute(driver, Elements.AD_Subscribe_EmailAddress_TextField, "placeholder"));
            test.validateElementIsPresent(driver, Elements.AD_Subscribe_Submit_Btn);
            test.validateElementIsNotEnabled(driver, Elements.AD_Subscribe_Submit_Btn);
            test.validateElementIsPresent(driver, By.CssSelector("div[id='subscribe'] * div[class='col-lg-8 col-md-8 col-sm-12 col-xs-12']>p"));
        }

        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", "Enter Your Email Address", TestName = "Verify Follow Us section in EN Locale")]
        [TestCase("cn", "输入您的邮箱地址", TestName = "Verify Follow Us section in CN Locale")]
        [TestCase("jp", "メールアドレスを入力してください", TestName = "Verify Follow Us section in JP Locale")]
        public void AD_Subscribe3(string Locale, string GhostText)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/analog-dialogue.html");
            action.IClick(driver, Elements.AD_Subscribe_Btn);
            test.validateElementIsPresent(driver, By.CssSelector("div[id='subscribe'] * div[class='col-lg-4 col-md-4 col-sm-12 col-xs-12']>p"));
            if (Locale.Equals("en"))
            {
                test.validateElementIsPresent(driver, Elements.AD_Subscribe_FBFollow);
            }
            else
            { 
                test.validateElementIsPresent(driver, By.CssSelector("div[class='col-lg-4 col-md-4 col-sm-12 col-xs-12']>p>a"));
            }
        }

        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", "* I agree to allow Analog Devices to use my personal data to improve my user experience through personalization and analytics.", "* Yes, I’d like to receive communications from Analog Devices and authorized partners related to ADI’s products and services.", TestName = "Verify the consent in Subscribe in Analog Dialogue in EN Locale")]
        [TestCase("cn", "* 本人同意存储及处理个人资料，以便通过个性化分析来改善个人体验", "* 是的，我愿意接收来自Analog Devices 以及授权合作伙伴提供的相关 ADI 产品服务信息。", TestName = "Verify the consent in Subscribe in Analog Dialogue in CN Locale")]
        [TestCase("jp", "* 私は、ウェブサイト上での個人向けコンテンツの最適化による体験の向上を目的とした、個人データの収集と分析に同意します。", "* アナログ・デバイセズまたはその正規販売代理店から、製品やそのサポートに関する連絡が来る場合があることを承諾する。", TestName = "Verify the consent in Subscribe in Analog Dialogue in JP Locale")]
        public void AD_Subscribe4(string Locale, string TermsOfUse_lbl, string CommunicationConsent_lbl)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/analog-dialogue.html");
            action.IClick(driver, Elements.AD_Subscribe_Btn);
            test.validateElementIsPresent(driver, Elements.Registration_TermsOfUse_ChxBox);
            test.validateStringIsCorrect(driver, By.CssSelector("label[for='termsofuse']"), TermsOfUse_lbl);
            test.validateElementIsPresent(driver, Elements.Registration_Consent_Comm);
            test.validateStringIsCorrect(driver, By.CssSelector("label[for='communication']"), CommunicationConsent_lbl);

            /****validate if submit button will be enabled if user only checks the communication consent****/
            action.IClick(driver, Elements.Registration_Consent_Comm);
            action.IType(driver, Elements.AD_Subscribe_EmailAddress_TextField, "aries.sorosoro@analog.com");
            Console.WriteLine(util.ReturnAttribute(driver, Elements.AD_Subscribe_Submit_Btn, "disabled"));
            test.validateElementIsNotEnabled(driver, Elements.AD_Subscribe_Submit_Btn);
            /****validate if submit button will be enabled if user only checks the terms of use****/
            action.IClick(driver, Elements.Registration_Consent_Comm);
            action.IClick(driver, Elements.Registration_TermsOfUse_ChxBox);
            test.validateElementIsNotEnabled(driver, Elements.AD_Subscribe_Submit_Btn);

            /****validate if submit button will be enabled if user both checks the terms of use and communicaton consent****/
            action.IClick(driver, Elements.Registration_Consent_Comm);
            test.validateElementIsEnabled(driver, Elements.AD_Subscribe_Submit_Btn);
        }

        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", TestName = "Verify the the error message when entering invalid email account in EN Locale")]
        [TestCase("cn", TestName = "Verify the the error message when entering invalid email account in CN Locale")]
        [TestCase("jp", TestName = "Verify the the error message when entering invalid email account in JP Locale")]
        public void AD_Subscribe5(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/analog-dialogue.html");
            action.IClick(driver, Elements.AD_Subscribe_Btn);
            action.IType(driver, Elements.AD_Subscribe_EmailAddress_TextField, "aries.sorosoro@analog");
            action.IType(driver, Elements.AD_Subscribe_EmailAddress_TextField, Keys.Tab);
            test.validateElementIsPresent(driver, By.CssSelector("div[class='alert alert-danger']"));
        }
    }
}
