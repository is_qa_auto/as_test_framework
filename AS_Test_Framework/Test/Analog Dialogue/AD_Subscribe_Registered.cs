﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Linq;
using System;
using System.Threading;

namespace AS_Test_Framework.AnalogDialogue
{

    [TestFixture]
    public class AD_Subscribe_Registered : BaseSetUp
    {
        public AD_Subscribe_Registered() : base() { }
        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", TestName = "Verify that user can subscribe to analog dialogue using registered account")]
        public void AD_Subscribe2(string Locale)
        {
            string Email = util.Generate_EmailAddress();
            action.Navigate(driver, Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/app/registration");
            action.ICreateNewUser(driver, Email, "Test Subscribe", "Registered", "Test_1234", "Test_City", "12345", "Analog");
            driver.Navigate().GoToUrl(Configuration.Env_Url + Locale + "/index.html");
            action.ILoginViaMyAnalogMegaMenu(driver, Email, "Test_1234");
            driver.Navigate().GoToUrl(Configuration.Env_Url + Locale + "/analog-dialogue.html");
            action.IClick(driver, Elements.AD_Subscribe_Btn);
            action.IType(driver, Elements.AD_Subscribe_EmailAddress_TextField, Email);
            action.IClick(driver, Elements.Registration_TermsOfUse_ChxBox);
            action.IClick(driver, Elements.Registration_Consent_Comm);
            action.IClick(driver, Elements.AD_Subscribe_Submit_Btn);
            test.validateElementIsPresent(driver, By.CssSelector("div[class='alert alert-success']"));
        //    action.Navigate(driver, Configuration.Env_Url.Replace("cldnet", "corpnt").Replace("www", "my") + Locale + "/app/subscriptions");
        //    action.ILogin(driver, Email, "Test_1234");
        //    test.validateString(driver, "true", util.ReturnAttribute(driver, By.CssSelector("input[id*='option-Analog Dialogue']"), "checked").ToString());
        }
    }
}
