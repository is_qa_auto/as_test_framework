﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Linq;

namespace AS_Test_Framework.AnalogDialogue
{

    [TestFixture]
    public class AD_Homepage_PastJournals_Journals : BaseSetUp
    {
        public AD_Homepage_PastJournals_Journals() : base() { }

        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", TestName = "Verify that user can open and close Journal by clicking the journal thumbnail in Past Journal section")]
        public void AD_Journals(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/analog-dialogue.html");
            action.IClick(driver, By.CssSelector("div[class='row past-journals'] * ul>li>a"));
            test.validateElementIsPresent(driver, Elements.AD_PastJournals_ModalPopup);
            action.IClick(driver, Elements.AD_PastJournals_ModalPopup_Close);
        }

        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", TestName = "Verify Journal Elements")]
        public void AD_Journal2(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/analog-dialogue.html");
            string JournalNo = util.GetText(driver, By.CssSelector("div[class='row past-journals'] * ul>li>a>span"));
            action.IClick(driver, By.CssSelector("div[class='row past-journals'] * ul>li>a"));
            test.validateStringIsCorrect(driver, Elements.AD_PastJournals_ModalPopup_Title , JournalNo.ToUpper().Replace(",",""));
            test.validateElementIsPresent(driver, Elements.AD_PastJournals_ModalPopup_Img);
            test.validateElementIsPresent(driver, Elements.AD_PastJournals_ModalPopup_Download);
        }
    }
}
