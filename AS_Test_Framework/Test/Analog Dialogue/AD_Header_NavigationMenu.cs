﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Linq;

namespace AS_Test_Framework.AnalogDialogue
{

    [TestFixture]
    public class AD_Header_NavigationMenu : BaseSetUp
    {
        public AD_Header_NavigationMenu() : base() { }

        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", TestName = "Verify that navigation menu in Analog Dialogue is present in EN Locale", Category = "Smoke_ADIWeb")]
        [TestCase("cn", TestName = "Verify that navigation menu in Analog Dialogue is present in CN Locale", Category = "Smoke_ADIWeb")]
        [TestCase("jp", TestName = "Verify that navigation menu in Analog Dialogue is present in JP Locale", Category = "Smoke_ADIWeb")]
        public void AD_NavigationMenu1(string Locale)
        {
            string[] NavigationalMenuEn = {
                                        "ABOUT",
                                        "50 YEARS",
                                        "ARCHIVES",
                                        "STUDENTZONE",
                                        "RAQS",
                                        "RESOURCES"
                                        };

            string[] NavigationalMenuCn = {
                                        "前言",
                                        "模拟对话50周年",
                                        "往期回顾",
                                        "学子专区",
                                        "非常见问题解答",
                                        "资源"
                                        };

            string[] NavigationalMenuJp = {
                                        "アナログ・ダイアログについて",
                                        "50周年",
                                        "バックナンバー",
                                        "STUDENTZONE",
                                        "珍問／難問集（RAQ）",
                                        "リソース",
                                        };

            string initial_steps = "1. Go to " + Configuration.Env_Url + Locale + "/analog-dialogue.html";
            string scenario = "";

            action.INavigate(driver, Configuration.Env_Url + Locale + "/analog-dialogue.html");

            for (int x = 1, y = 0; util.CheckElement(driver, By.CssSelector("ul[class='nav navbar-nav']>li:nth-child(" + x + ")"), 1); x++, y++) {
                if (Locale.Equals("en"))
                {
                    scenario = "Verify that navigation menu label is correct";
                    test.validateStringIsCorrectv2(driver, By.CssSelector("ul[class='nav navbar-nav']>li:nth-child(" + x + ")>a"), NavigationalMenuEn[y], scenario, initial_steps);
                }
                else if (Locale.Equals("cn"))
                {
                    scenario = "Verify that navigation menu label is correct";
                    test.validateStringIsCorrectv2(driver, By.CssSelector("ul[class='nav navbar-nav']>li:nth-child(" + x + ")>a"), NavigationalMenuCn[y], scenario, initial_steps);
                }
                else
                {
                    scenario = "Verify that navigation menu label is correct";
                    test.validateStringIsCorrectv2(driver, By.CssSelector("ul[class='nav navbar-nav']>li:nth-child(" + x + ")>a"), NavigationalMenuJp[y], scenario, initial_steps);
                }
            }
        }


        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", TestName = "Verify that Analog Dialogue navigation menu redirection in EN Locale")]
        [TestCase("cn", TestName = "Verify that Analog Dialogue navigation menu redirection in CN Locale")]
        [TestCase("jp", TestName = "Verify that Analog Dialogue navigation menu redirection in JP Locale")]
        public void AD_NavigationMenu2(string Locale)
        {
            string[] NavigationalMenuRedirection = {
                                        "/analog-dialogue/about-analog-dialogue.html",
                                        "/analog-dialogue/50-years.html",
                                        "/analog-dialogue/archives.html",
                                        "/analog-dialogue/studentzone.html",
                                        "/analog-dialogue/raqs.html",
                                        };

            action.Navigate(driver, Configuration.Env_Url + Locale + "/analog-dialogue.html");
            for (int x = 1, y = 0; NavigationalMenuRedirection.Length != y; x++, y++)
            {
                action.IClick(driver, By.CssSelector("ul[class='nav navbar-nav']>li:nth-child(" + x + ")>a"));
                test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + NavigationalMenuRedirection[y]);
            }
        }

        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", TestName = "Verify that Resources Tab is working and all its menu tabs are redirecting to correct page")]
        public void AD_NavigationMenu3(string Locale)
        {
            string[] NavigationalMenuRedirection = {
                                        "/analog-dialogue/lt-journal.html",
                                        "/search.html",
                                        "/search.html",
                                        "/search.html",
                                        "/education/education-library/videos.html",
                                        "/education/education-library/webcasts.html",
                                        "/search.html"
                                        };

            action.Navigate(driver, Configuration.Env_Url + Locale + "/analog-dialogue.html");
            action.IClick(driver, By.CssSelector("ul[class='nav navbar-nav']>li:nth-child(6)>a"));
            test.validateElementIsPresent(driver, By.CssSelector("ul[class='nav navbar-nav']>li:nth-child(6)>ul[class='dropdown-menu']"));
            for (int x = 1, y = 0; util.CheckElement(driver, By.CssSelector("ul[class='nav navbar-nav']>li:nth-child(6)>ul[class='dropdown-menu']>li:nth-child(" + x + ")"), 1); x++, y++)
            {
                string ResourcesTab = util.GetText(driver, By.CssSelector("ul[class='nav navbar-nav']>li:nth-child(6)>ul[class='dropdown-menu']>li:nth-child(" + x + ")>a"));
                action.IClick(driver, By.CssSelector("ul[class='nav navbar-nav']>li:nth-child(6)>ul[class='dropdown-menu']>li:nth-child(" + x + ")>a"));
                test.validateStringInstance(driver, driver.Url, NavigationalMenuRedirection[y]);
                if (ResourcesTab.Equals("Technical Articles"))
                {
                    test.validateStringIsCorrect(driver, By.CssSelector("li[data-facetcategory='resource_type_fac_s']>ul[class='nav collapsed']>li[class='filter-value base-pivot  checkbox on non-zero']>ul>li[class='filter-value  child-pivot  checkbox on non-zero']>a>span"), "Technical Articles");
                }
                else if (ResourcesTab.Equals("Technical Books")) {
                    test.validateStringIsCorrect(driver, By.CssSelector("li[data-facetcategory='resource_type_fac_s']>ul[class='nav collapsed']>li[class='filter-value base-pivot  checkbox on non-zero']>ul>li[class='filter-value  child-pivot  checkbox on non-zero']>a>span"), "Technical Books");
                }
                else if (ResourcesTab.Equals("FAQs"))
                {
                    test.validateStringIsCorrect(driver, By.CssSelector("li[data-facetcategory='resource_type_fac_s']>ul[class='nav collapsed']>li[class='filter-value base-pivot  checkbox on non-zero']>ul>li[class='filter-value  child-pivot  checkbox on non-zero']>a>span"), "Frequently Asked Questions");
                }
                else if (ResourcesTab.Equals("Application Notes"))
                {
                    test.validateStringIsCorrect(driver, By.CssSelector("li[data-facetcategory='resource_type_fac_s']>ul[class='nav collapsed']>li[class='filter-value base-pivot  checkbox on non-zero']>ul>li[class='filter-value  child-pivot  checkbox on non-zero']>a>span"), "Application Notes");
                }
            }
        }
    }
}
