﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Linq;
using System;
using System.Threading;

namespace AS_Test_Framework.AnalogDialogue
{

    [TestFixture]
    public class AD_Subscribe_NonRegistered : BaseSetUp
    {
        public AD_Subscribe_NonRegistered() : base() { }

        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", TestName = "Verify user can subscribe in analog dialogue using un-registered email in EN Locale", Category = "Smoke_ADIWeb")]
        [TestCase("cn", TestName = "Verify user can subscribe in analog dialogue using un-registered email in CN Locale", Category = "Smoke_ADIWeb")]
        [TestCase("jp", TestName = "Verify user can subscribe in analog dialogue using un-registered email in JP Locale", Category = "Smoke_ADIWeb")]
        public void AD_Subscribe1(string Locale)
        {
            string Email = util.Generate_EmailAddress();
            string initial_steps = "1. Go to " + Configuration.Env_Url + Locale + "/analog-dialogue.html"
                             + "<br>2. Click Subscribe Button"
                             + "<br>3. Type \"" + Email + "\" in the email text box"
                             + "<br>4. Accept Terms of Use by checking the terms of use checkbox"
                             + "<br>5. Accept Communication consent by checking the communication consent checkbox"
                             + "<br>6. Click Submit Button";
            string scenario = "";

            action.INavigate(driver, Configuration.Env_Url + Locale + "/analog-dialogue.html");
            action.IClick(driver, Elements.AD_Subscribe_Btn);
            action.IType(driver, Elements.AD_Subscribe_EmailAddress_TextField, Email);
            action.IClick(driver, Elements.Registration_TermsOfUse_ChxBox);
            action.IClick(driver, Elements.Registration_Consent_Comm);
            action.IClick(driver, Elements.AD_Subscribe_Submit_Btn);

            scenario = "Verify that success message box will be displayed after successfully subscribing to analog dialogue";
            test.validateElementIsPresentv2(driver, By.CssSelector("div[class='alert alert-success']"), scenario, initial_steps);

            scenario = "Verify that submit button will be disabled after successfully subscribing to analog dialogue";
            test.validateElementIsNotEnabled(driver, Elements.AD_Subscribe_Submit_Btn);
        }

        //[Test, Category("Analog Dialogue"), Category("Core")]
        //[TestCase("en", TestName = "Verify that Analog Dialogue under Newsletters should be checked afer registering the account")]
        //public void AD_Subscribe2(string Locale)
        //{
        //    string Email = util.Generate_EmailAddress();
        //    action.Navigate(driver, Configuration.Env_Url + Locale + "/analog-dialogue.html");
        //    action.IClick(driver, Elements.AD_Subscribe_Btn);
        //    action.IType(driver, Elements.AD_Subscribe_EmailAddress_TextField, Email);
        //    action.IClick(driver, Elements.Registration_TermsOfUse_ChxBox);
        //    action.IClick(driver, Elements.AD_Subscribe_Submit_Btn);
        //    action.Navigate(driver, Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/app/registration");
        //    action.ICreateNewUser(driver, Email, "Test Subscribe", "UnRegistered", "Test_1234", "12345", "Analog");
        //    action.Navigate(driver, Configuration.Env_Url.Replace("cldnet", "corpnt").Replace("www", "my") + Locale + "/app/subscriptions");
        //    action.ILogin(driver, Email, "Test_1234");
        //    test.validateString(driver, "true", util.ReturnAttribute(driver, By.CssSelector("input[id*='option-Analog Dialogue']"), "checked").ToString());
        //}
    }
}
