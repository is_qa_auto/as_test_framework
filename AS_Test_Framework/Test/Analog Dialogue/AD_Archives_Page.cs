﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.AnalogDialogue
{

    [TestFixture]
    public class AD_Archives_Page : BaseSetUp
    {
        public AD_Archives_Page() : base() { }

        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", TestName = "Verify Archives Page Element in EN Locale")]
        [TestCase("cn", TestName = "Verify Archives Page Element in CN Locale")]
        [TestCase("jp", TestName = "Verify Archives Page Element in JP Locale")]
        public void AD_Archives1(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/analog-dialogue/archives.html");
            test.validateElementIsPresent(driver, By.CssSelector("div[id='archives-app'] h1"));
            test.validateElementIsPresent(driver, Elements.AD_Archives_Sort_DD);
            test.validateElementIsPresent(driver, Elements.AD_Archives_Year_DD);
            test.validateStringIsCorrect(driver, Elements.AD_Archives_Page_Title, "Archives");
            test.validateElementIsPresent(driver, By.CssSelector("div[id='archives-app'] * div[class='grid row']>div[class='grid item'] img"));
            test.validateElementIsPresent(driver, By.CssSelector("div[id='archives-app'] * div[class='grid row']>div[class='grid item'] span[class='volume details']"));
        }

        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", TestName = "Verify that Sort by Year is present and working as expected", Category = "Smoke_ADIWeb")]
        public void AD_Archives2(string Locale)
        {
            string initial_steps = "1. Go to " + Configuration.Env_Url + Locale + "/analog-dialogue/archives.html";
            string scenario = "";

            action.INavigate(driver, Configuration.Env_Url + Locale + "/analog-dialogue/archives.html");

            scenario = "Verify the default selected option for Year Dropdown"; 
            test.validateStringIsCorrectv2(driver, Elements.AD_Archives_Year_DD, "All", scenario, initial_steps);

            initial_steps = initial_steps + "<br>2. Click Year dropdown";
            action.IClick(driver, Elements.AD_Archives_Year_DD);
            scenario = "Verify that Year Menu will be displayed once user clicks on Year dropdown";
            test.validateElementIsPresentv2(driver, Elements.AD_Archives_Year_Menu, scenario, initial_steps);

            int YearCount = util.GetCount(driver, By.CssSelector("ul[aria-labelledby='YEAR']>li"));
            object[] Year = new object[YearCount];
            for (int x = 2, y = 0; x <= YearCount; x++, y++)
            {
                Year[y] = util.GetText(driver, By.CssSelector("ul[aria-labelledby='YEAR']>li:nth-child(" + x + ")")).Trim();
            }
            scenario = "Verify that all year displayed in Year dropdown is in Descending order";
            test.validateStringIfInDescendingv2(Year, scenario, initial_steps);

            action.IClick(driver, By.CssSelector("ul[aria-labelledby='YEAR']>li span[id='1990s']"));
            initial_steps = initial_steps + "<br>3. Click year \"1990\"";
            scenario = "Verify that sub menu for selected year is displayed";
            test.validateElementIsPresentv2(driver, By.CssSelector("ul[aria-labelledby='YEAR']>li>div[class='dropdown sub-menu open']>ul[aria-labelledby='1990s']"),scenario, initial_steps);

            int YearCountSubCount = util.GetCount(driver, By.CssSelector("ul[aria-labelledby='YEAR']>li>div[class='dropdown sub-menu open']>ul[aria-labelledby='1990s']>li"));
            object[] YearSubMenu = new object[YearCountSubCount];
            for (int x = 2, y = 0; x <= YearCount; x++, y++)
            {
                YearSubMenu[y] = util.GetText(driver, By.CssSelector("ul[aria-labelledby='YEAR']>li>div[class='dropdown sub-menu open']>ul[aria-labelledby='1990s']>li:nth-child(" + x + ")")).Trim();
            }
            scenario = "Verify that all year displayed in sub menu of year dropdown is in Descending order";
            test.validateStringIfInDescendingv2(YearSubMenu, scenario, initial_steps);
        }

        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", TestName = "Verify that Sort by Newest/Oldest is present and working as expected")]
        public void AD_Archives3(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/analog-dialogue/archives.html");
            test.validateStringIsCorrect(driver, Elements.AD_Archives_Sort_DD, "Newest");
            action.IClick(driver, Elements.AD_Archives_Sort_DD);
            test.validateElementIsPresent(driver, Elements.AD_Archives_Sort_Menu);
            object[] ArticleVolumeNo = new object[10];
            for (int x = 1, y = 0; x <= 10; x++, y++)
            {
                ArticleVolumeNo[y] = util.GetText(driver, By.CssSelector("div[id='archives-app'] div[class='grid row']>div:nth-child(" + x +") span[class='volume name']")).Trim();
            }
            test.validateStringIfInDescending(ArticleVolumeNo);

            action.IClick(driver, By.CssSelector("ul[aria-labelledby='SORT']>li span[data-value='Oldest']"));
           
            ArticleVolumeNo = new object[10];
            for (int x = 1, y = 0; x <= 10; x++, y++)
            {
                ArticleVolumeNo[y] = util.GetText(driver, By.CssSelector("div[id='archives-app'] div[class='grid row']>div:nth-child(" + x + ") span[class='volume name']")).Trim();
            }
            test.validateStringIfInAscenading(ArticleVolumeNo);
        }

        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", TestName = "Verify Article Lightbox will be displayed when user clicks on Article thumbnail in Archives page", Category = "Smoke_ADIWeb")]
        public void AD_Archives4(string Locale)
        {
            string initial_steps = "1. Go to " + Configuration.Env_Url + Locale + "/analog-dialogue/archives.html";
            string scenario = "";

            action.INavigate(driver, Configuration.Env_Url + Locale + "/analog-dialogue/archives.html");
            action.IClick(driver, By.CssSelector("div[id='archives-app'] div[class='grid row']>div:nth-child(1) img"));

            scenario = "Verify that archives lighbox will be displayed";
            test.validateElementIsPresentv2(driver, Elements.AD_Arcives_lightbox, scenario, initial_steps + "<br>2. Click any archives thumbnail");

            scenario = "Verify that article link is present in Archives lightbox";
            test.validateElementIsPresentv2(driver, By.CssSelector("div[class='archives details modal'] div[class='modal-body']>div>div:nth-child(2) a"), scenario, initial_steps + "<br>2. Click any archives thumbnail");

            scenario = "Verify that Download link is presnet in archives lightbox";
            test.validateElementIsPresentv2(driver, Elements.AD_Archives_lightbox_DL, scenario, initial_steps + "<br>2. Click any archives thumbnail");

            action.IClick(driver, Elements.AD_Archives_lightbox_Close);
            scenario = "Verify that archives lightbox will be closed after clicking close button";
            test.validateElementIsNotPresentv2(driver, Elements.AD_Arcives_lightbox, scenario, initial_steps + "<br>2. Click any archives thumbnail<br>3. Click close button");

            action.IClick(driver, By.CssSelector("div[id='archives-app'] div[class='grid row']>div:nth-child(1) img"));
            string IssueTitle = util.GetText(driver, By.CssSelector("div[class='archives details modal'] div[class='modal-body']>div>div:nth-child(2) a"));
            action.IClick(driver, By.CssSelector("div[class='archives details modal'] div[class='modal-body']>div>div:nth-child(2) a"));
            scenario = "Verify that user will be redirected to correct archives article";
            test.validateStringIsCorrectv2(driver, By.CssSelector("section[class='col-lg-8 col-md-8 col-sm-12 col-xs-12 leftCol']>h1"), IssueTitle, scenario, initial_steps + "<br>2. Click any archives thumbnail<br>3. Click any issues/article link in archives lightbox");
        }
    }
}
