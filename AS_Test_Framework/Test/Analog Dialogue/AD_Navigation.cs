﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Linq;

namespace AS_Test_Framework.AnalogDialogue
{

    [TestFixture]
    public class AD_Navigation : BaseSetUp
    {
        public AD_Navigation() : base() { }
        /***Removed as script cannot properly find a search result that navigates to AD*****/
        //[Test, Category("Analog Dialogue"), Category("Core")]
        //[TestCase("en", TestName = "Verify that user can navigate Analog dialogue through Predictive Search in EN Locale")]
        //[TestCase("cn", TestName = "Verify that user can navigate Analog dialogue through Predictive Search in CN Locale")]
        //[TestCase("jp", TestName = "Verify that user can navigate Analog dialogue through Predictive Search in JP Locale")]
        //public void AD_NAvigate_PredictiveSearch(string Locale)
        //{
        //    action.Navigate(driver, Configuration.Env_Url + Locale + "/index.html");
        //    action.IType(driver, Elements.GlobalSearchTextField, "Analog Dialogue");
        //    action.IClick(driver, Elements.GlobalSearch_PredictiveSearch_Search_Link);
        //    test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/analog-dialogue.html#");
        //}


        [Test, Category("Analog Dialogue"), Category("Core")]
        [TestCase("en", "Analog Dialogue", TestName = "Verify that user can Navigate from the Search page in EN Locale")]
        [TestCase("cn", "《模拟对话》", TestName = "Verify that user can Navigate from the Search page in CN Locale")]
        [TestCase("jp", "アナログ・ダイアログ", TestName = "Verify that user can Navigate from the Search page in JP Locale")]
        public void AD_NAvigate_Search(string Locale, string Keyword)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/index.html");
            action.IType(driver, Elements.GlobalSearchTextField, Keyword);
            action.IClick(driver, Elements.GlobalSearch_SearchIcon);
            for (int x = 1; util.CheckElement(driver, By.CssSelector("div[class='search-results-items']>div:nth-child(" + x + ")"), 1); x++)
            {
                if (util.GetText(driver, By.CssSelector("div[class='search-results-items']>div:nth-child(" + x + ")>a")).Equals(Keyword))
                {
                    action.IClick(driver, By.CssSelector("div[class='search-results-items']>div:nth-child(" + x + ")>a"));
                    test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "analog-dialogue.html");
                    break;
                }
            }
        }
    }
}
