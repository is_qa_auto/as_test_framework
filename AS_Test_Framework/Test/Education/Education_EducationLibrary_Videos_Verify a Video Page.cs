﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;

namespace AS_Test_Framework.Education
{
    [TestFixture]
    public class Education_EducationLibrary_Videos_VideoPage : BaseSetUp
    {
        public Education_EducationLibrary_Videos_VideoPage() : base() { }

        [Test, Category("Education"), Category("Core"), Retry(2)]
        //[TestCase("en", TestName = "Verify that the Video Page is Working as Expected in EN Locale")]
        [TestCase("cn", "/education/education-library/videos/1239747868001.html", TestName = "Verify that the Video Page is Working as Expected in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Video Page is Working as Expected in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Video Page is Working as Expected in RU Locale")]
        public void Education_EducationLibrary_Videos_VerifyVideoPage(string Locale, string VideoPage_Url)
        {
            //--- Action: Go to the Video Page ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + VideoPage_Url);

            //----- Education TestPlan > Desktop Tab > R11 > T6: Verify if breadcrumb title is visible (IQ-8700/ ALA-13536) -----//

            //--- Expected Result: breadcrumb title is visible ---//
            test.validateElementIsPresent(driver, Elements.Breadcrumb_CurrentPage_Txt);

            //--- Expected Result: The title tag same as value present in <h1 class="header1"> ---//
            string page_title = util.GetText(driver, Elements.Education_VideoPage_Page_Title_Lbl);
            test.validateWindowTitleIsCorrect(driver, page_title);
        }
    }
}