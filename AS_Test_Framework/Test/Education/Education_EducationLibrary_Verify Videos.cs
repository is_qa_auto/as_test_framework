﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.Education
{
    [TestFixture]
    public class Education_EducationLibrary_Videos : BaseSetUp
    {
        public Education_EducationLibrary_Videos() : base() { }

        //--- URLs ---//
        string videos_page_url = "/education/education-library/videos";
        string videoPage_url_format = "/videos/";
        string search_page_url = "/search.html";

        //--- Labels ---//
        string educationLibrary_txt = "Education Library";
        string videos_txt = "Videos";

        [Test, Category("Education"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Left-rail is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Left-rail is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Left-rail is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Left-rail is Present and Working as Expected in RU Locale")]
        public void Education_EducationLibrary_VerifyLeftRail(string Locale)
        {
            //--- Action: Go to the Videos Page ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + videos_page_url);

            //----- Education TestPlan > Desktop Tab > R5 > T1: Verify the Left-rail -----//

            if (Locale.Equals("en"))
            {
                //--- Expected Result: "Education Library" will be highlighted in Violet. ---//
                test.validateStringIsCorrect(driver, Elements.Education_EducationLibrary_LeftRail_Selected_Sec_Link, educationLibrary_txt);
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.Education_EducationLibrary_LeftRail_Selected_Sec_Link);
            }

            if (Locale.Equals("en"))
            {
                //--- Expected Result: "Videos" under Education Library will be in Bold. ---//
                test.validateStringIsCorrect(driver, Elements.Education_EducationLibrary_LeftRail_Selected_Val_Link, videos_txt);
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.Education_EducationLibrary_LeftRail_Selected_Val_Link);
            }
        }

        [Test, Category("Education"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the 1st Component is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the 1st Component is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the 1st Component is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the 1st Component is Present and Working as Expected in RU Locale")]
        public void Education_EducationLibrary_Verify1stComponent(string Locale)
        {
            //--- Action: Go to the Videos Page ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + videos_page_url);

            //----- Education TestPlan > Desktop Tab > R11 > T2: Verify 1st component (component with background video) (AL-9269) -----//

            //--- Expected Result: Analog Devices Video Portal text is available on the first component ---//
            test.validateElementIsPresent(driver, Elements.Education_Videos_AnalogDevicesVideoPortal_Lbl);

            //--- Expected Result: First component has a background video ---//
            test.validateElementIsPresent(driver, Elements.Education_Videos_Bg_Vid);
        }

        [Test, Category("Education"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Videos Search is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Videos Search is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Videos Search is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Videos Search is Present and Working as Expected in RU Locale")]
        public void Education_EducationLibrary_VerifyVideosSearch(string Locale)
        {
            //--- Action: Go to the Videos Page ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + videos_page_url);

            if (util.CheckElement(driver, Elements.Education_Videos_Search_InputBx, 1) && util.CheckElement(driver, Elements.Education_Videos_Search_Btn, 1))
            {
                string search_input = null;

                //----- Education TestPlan > Desktop Tab > R2 > T9: Verify the actions that can be done for the videos accessed in the page -----//

                //--- Action: Enter any search keyword into Analog Devices Video Portal search bar ---//
                if (Locale.Equals("ru"))
                {
                    search_input = "adi";
                }
                else
                {
                    search_input = "and";
                }
                action.IDeleteValueOnFields(driver, Elements.Education_Videos_Search_InputBx);
                action.IType(driver, Elements.Education_Videos_Search_InputBx, search_input);

                //--- Expected Result: The predictive search dropdown appears ---//
                test.validateElementIsPresent(driver, Elements.Education_Videos_Search_PredictiveSearch);

                if (util.CheckElement(driver, Elements.Education_Videos_Search_PredictiveSearch, 1))
                {
                    //--- Action: Click the first item on dropdown ---//
                    action.IOpenLinkInNewTab(driver, Elements.Education_Videos_Search_PredictiveSearch_Item_Links);
                    Thread.Sleep(1000);

                    //--- Expected Result: The page should redirect to the page link ---//
                    test.validateStringInstance(driver, driver.Url, videoPage_url_format);

                    driver.Close();
                    driver.SwitchTo().Window(driver.WindowHandles.First());
                }

                //----- Education TestPlan > Desktop Tab > R11 > T5: Verify videos search should point to global search. (IQ-4339/AL-11339) -----//

                //--- Action: Go to the Videos Page ---//
                driver.Navigate().GoToUrl(Configuration.Env_Url + Locale + videos_page_url);

                //--- Action: Enter any search keyword into Analog Devices Video Portal search bar ---//
                search_input = "*";
                action.IDeleteValueOnFields(driver, Elements.Education_Videos_Search_InputBx);
                action.IType(driver, Elements.Education_Videos_Search_InputBx, search_input);

                //--- Action: Click search button ---//
                action.IClick(driver, Elements.Education_Videos_Search_Btn);

                //--- Expected Result: The page should redirect to global search ---//
                test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + search_page_url);
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.Education_Videos_Search_InputBx);
                test.validateElementIsPresent(driver, Elements.Education_Videos_Search_Btn);
            }
        }

        [Test, Category("Education"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Search Filter Menus are Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Search Filter Menus are Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Search Filter Menus are Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Search Filter Menus are Present and Working as Expected in RU Locale")]
        public void Education_EducationLibrary_VerifySearchFilterMenus(string Locale)
        {
            //--- Action: Go to the Videos Page ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + videos_page_url);
            Thread.Sleep(1000);

            //----- Education TestPlan > Desktop Tab > R11 > T2.1: Verify available dropdown on 1st video component (AL-9331) -----//

            //--- Expected Result: The menus are available ---//
            test.validateElementIsPresent(driver, Elements.Education_Videos_Search_Filter_Menus);

            if (util.CheckElement(driver, Elements.Education_Videos_ProductCategories_Dd, 1))
            {
                //--- PRODUCT CATEGORIES - DROPDOWN ---//

                //----- Education TestPlan > Desktop Tab > R5 > T2_1: Verify the Videos page in EN Locale -----//

                //--- Action: Click on the Product Categories drop down ---//
                action.IClick(driver, Elements.Education_Videos_ProductCategories_Dd);

                //--- Expected Result: Dropdowns should display properly. ---//
                test.validateElementIsPresent(driver, Elements.Education_Videos_ProductCategories_Dd_Menu);

                if (util.CheckElement(driver, Elements.Education_Videos_ProductCategories_Dd_Menu_Val_Lbls, 1))
                {
                    string selected_productCategories_dd_val = util.GetText(driver, Elements.Education_Videos_ProductCategories_Dd_Menu_Val_Lbls);
                    string current_url = driver.Url;

                    //--- Action: Select any value ---//
                    action.IClick(driver, Elements.Education_Videos_ProductCategories_Dd_Menu_Val_Lbls);

                    bool switchTo_lastTab = false;
                    if (driver.Url.Contains(current_url))
                    {
                        switchTo_lastTab = true;
                        driver.SwitchTo().Window(driver.WindowHandles.Last());
                    }

                    //--- Expected Result: The selected value in the drop down should be checked/selected in the Filters section. ---//
                    test.validateStringIsCorrect(driver, Elements.GlobalSearchResults_ProductCategories_Selected_Filter_Lbl, selected_productCategories_dd_val);

                    if (Locale.Equals("en"))
                    {
                        //--- Expected Result: Videos should be checked/selected in the Filters section ---//
                        test.validateStringIsCorrect(driver, Elements.GlobalSearchResults_Resources_Selected_Filter_Lbl, videos_txt);
                    }
                    else
                    {
                        test.validateElementIsPresent(driver, Elements.GlobalSearchResults_Resources_Selected_Filter_Lbl);
                    }

                    if (switchTo_lastTab == true)
                    {
                        driver.Close();
                        driver.SwitchTo().Window(driver.WindowHandles.First());
                    }
                    else
                    {
                        driver.Navigate().GoToUrl(Configuration.Env_Url + Locale + videos_page_url);
                    }
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.Education_Videos_ProductCategories_Dd_Menu_Val_Lbls);
                }
            }

            if (util.CheckElement(driver, Elements.Education_Videos_Markets_Dd, 1))
            {
                //--- MARKETS - DROPDOWN ---//

                //----- Education TestPlan > Desktop Tab > R5 > T2_1: Verify the Videos page in EN Locale -----//

                //--- Action: Click on the Markets drop down ---//
                action.IClick(driver, Elements.Education_Videos_Markets_Dd);

                if (util.CheckElement(driver, Elements.Education_Videos_Markets_Dd_Menu_Val_Lbls, 1))
                {
                    string selected_markets_dd_val = util.GetText(driver, Elements.Education_Videos_Markets_Dd_Menu_Val_Lbls);
                    string current_url = driver.Url;

                    //--- Action: Select any value ---//
                    action.IClick(driver, Elements.Education_Videos_Markets_Dd_Menu_Val_Lbls);

                    bool switchTo_lastTab = false;
                    if (driver.Url.Contains(current_url))
                    {
                        switchTo_lastTab = true;
                        driver.SwitchTo().Window(driver.WindowHandles.Last());
                    }

                    //--- Expected Result: The selected value in the drop down should be checked/selected in the Filters section. ---//
                    test.validateStringIsCorrect(driver, Elements.GlobalSearchResults_Markets_Selected_Filter_Lbl, selected_markets_dd_val);

                    if (Locale.Equals("en"))
                    {
                        //--- Expected Result: Videos should be checked/selected in the Filters section ---//
                        test.validateStringIsCorrect(driver, Elements.GlobalSearchResults_Resources_Selected_Filter_Lbl, videos_txt);
                    }
                    else
                    {
                        test.validateElementIsPresent(driver, Elements.GlobalSearchResults_Resources_Selected_Filter_Lbl);
                    }

                    if (switchTo_lastTab == true)
                    {
                        driver.Close();
                        driver.SwitchTo().Window(driver.WindowHandles.First());
                    }
                    else
                    {
                        driver.Navigate().GoToUrl(Configuration.Env_Url + Locale + videos_page_url);
                    }
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.Education_Videos_Markets_Dd_Menu_Val_Lbls);
                }
            }

            if (util.CheckElement(driver, Elements.Education_Videos_Tradeshows_Dd, 1))
            {
                //--- TRADESHOWS - DROPDOWN ---//

                //----- Education TestPlan > Desktop Tab > R5 > T2_1: Verify the Videos page in EN Locale -----//

                //--- Action: Click on the Tradeshows drop down ---//
                action.IClick(driver, Elements.Education_Videos_Tradeshows_Dd);

                if (util.CheckElement(driver, Elements.Education_Videos_Tradeshows_Dd_Menu_Val_Lbls, 1))
                {
                    string selected_tradeshows_dd_val = util.GetText(driver, Elements.Education_Videos_Tradeshows_Dd_Menu_Val_Lbls);
                    string current_url = driver.Url;

                    //--- Action: Select any value ---//
                    action.IClick(driver, Elements.Education_Videos_Tradeshows_Dd_Menu_Val_Lbls);

                    if (driver.Url.Contains(current_url))
                    {
                        driver.SwitchTo().Window(driver.WindowHandles.Last());
                    }

                    //--- Expected Result: The selected value in the drop down should be checked/selected in the Filters section. ---//
                    test.validateStringIsCorrect(driver, Elements.GlobalSearchResults_Tradeshows_Selected_Filter_Lbl, selected_tradeshows_dd_val);

                    if (Locale.Equals("en"))
                    {
                        //--- Expected Result: Videos should be checked/selected in the Filters section ---//
                        test.validateStringIsCorrect(driver, Elements.GlobalSearchResults_Resources_Selected_Filter_Lbl, videos_txt);
                    }
                    else
                    {
                        test.validateElementIsPresent(driver, Elements.GlobalSearchResults_Resources_Selected_Filter_Lbl);
                    }
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.Education_Videos_Tradeshows_Dd_Menu_Val_Lbls);
                }
            }
        }

        //[Test, Category("Education"), Category("Core"), Retry(2)]
        //[TestCase("en", TestName = "Verify that the 2nd Component is Present and Working as Expected in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the 2nd Component is Present and Working as Expected in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the 2nd Component is Present and Working as Expected in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the 2nd Component is Present and Working as Expected in RU Locale")]
        //public void Education_EducationLibrary_Verify2ndComponent(string Locale)
        //{
        //    //--- Action: Go to the Videos Page ---//
        //    action.Navigate(driver, Configuration.Env_Url + Locale + videos_page_url);

        //    //----- Education TestPlan > Desktop Tab > R11 > T3.1: Verify 2nd component (three video and one promo image) -----//

        //    //--- Expected Result: Three video image is displayed ---//
        //    test.validateElementIsPresent(driver, Elements.Education_Videos_2ndComponent_Large_Vid);
        //    test.validateCountIsEqual(driver, util.GetCount(driver, Elements.Education_Videos_2ndComponent_Small_Vids), 2);
        //}

        [Test, Category("Education"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the 3rd Component is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the 3rd Component is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the 3rd Component is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the 3rd Component is Present and Working as Expected in RU Locale")]
        public void Education_EducationLibrary_Verify3rdComponent(string Locale)
        {
            //--- Action: Go to the Videos Page ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + videos_page_url);
            Thread.Sleep(1000);

            //----- Education TestPlan > Desktop Tab > R11 > T4: 3rd component (dynamic facet video carousel) -----//

            if (util.CheckElement(driver, Elements.Education_Videos_Select_Dds, 1))
            {
                //--- Action: Click Select Market dropdown list ---//
                action.IClick(driver, Elements.Education_Videos_Select_Dds);

                //--- Expected Result: List of Market are available in the list ---//
                test.validateElementIsPresent(driver, Elements.Education_Videos_Select_Dds_Menu);
            }

            //--- Expected Result: All Video must have its corresponding thumbnail (IQ-5257/AL-12697) ---//
            test.validateElementIsPresent(driver, Elements.Education_Videos_3rdComponent_Vid_Thumbnails);

            if (util.CheckElement(driver, Elements.Education_Videos_3rdComponent_Vid_Thumbnails, 1))
            {
                //--- Action: Click each video ---//
                action.IClick(driver, Elements.Education_Videos_3rdComponent_Vid_Thumbnails);

                //--- Expected Result: Video pop-up window appear ---//
                test.validateElementIsPresent(driver, Elements.Education_Videos_Video_DialogBx);

                test.validateElementIsPresent(driver, Elements.Education_Videos_Video_DialogBx_Vid);

                if (util.CheckElement(driver, Elements.Education_Videos_Video_DialogBx_X_Btn, 1))
                {
                    //--- Action: Click pop-up window close button ---//
                    action.IClick(driver, Elements.Education_Videos_Video_DialogBx_X_Btn);

                    //--- Expected Result: Pop-up window close ---//
                    test.validateElementIsNotPresent(driver, Elements.Education_Videos_Video_DialogBx);
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.Education_Videos_Video_DialogBx_X_Btn);

                    driver.Navigate().GoToUrl(Configuration.Env_Url + Locale + videos_page_url);
                }
            }
        }
    }
}