﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.Education
{
    [TestFixture]
    public class Education_MegaMenu : BaseSetUp
    {
        public Education_MegaMenu() : base() { }

        //--- URLs ---//
        string home_page_url = "/index.html";
        string educationLibrary_page_url = "/education-library.html";
        string tutorials_page_url = "/tutorials.html";
        string videos_page_url = "/videos.html";
        string webcasts_page_url = "/webcasts.html";
        string search_page_url = "/search.html";
        string educationEngagement_page_url = "/university-engagement.html";
        string northAmerica_page_url = "/north-america.html";
        string anveshanFellowship_page_url = "/india-anveshan.html";
        string philippines_page_url = "/philippines.html";
        string modulesAndCourseware_page_url = "/courses-and-tutorials.html";
        string activeLearningModules_page_url = "/active-learning-module.html";

        //--- Labels ---//
        string faqs_txt = "FAQs";
        string technicalArticles_txt = "Technical Articles";
        string technicalBooks_txt = "Technical Books";
        string trainingandTutorials_txt = "Training and Tutorials";
        string whitePapersAndCaseStudies_txt = "White Papers and Case Studies";
        string technicalDocumentations_txt = "Technical Documentations";
        string frequentlyAskedQuestions_txt = "Frequently Asked Questions";
        string education_txt = "Education";

        //--- Locators ---//
        string EducationTab_EducationLibrary_Sec_Locator = "div[class='education__nav']>div:nth-of-type(1)";

        [Test, Category("Education"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Education Library Section in the Education Mega Menu is Present and Working as Expected as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Education Library Section in the Education Mega Menu is Present and Working as Expected as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Education Library Section in the Education Mega Menu is Present and Working as Expected as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Education Library Section in the Education Mega Menu is Present and Working as Expected as Expected in RU Locale")]
        public void Education_MegaMenu_VerifyEducationLibraryInEducationMegaMenu(string Locale)
        {
            //--- Action: Navigate to Analog Homepage ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + home_page_url);
            Thread.Sleep(2000);

            if (util.CheckElement(driver, Elements.MainNavigation_EducationTab, 1))
            {
                //--- Action: Click the Education tab ---//
                if (!util.CheckElement(driver, Elements.MainNavigation_EducationTab_Expanded, 1))
                {
                    action.IClick(driver, Elements.MainNavigation_EducationTab);
                    Thread.Sleep(1000);
                }

                if (util.CheckElement(driver, Elements.MainNavigation_EducationTab_Expanded, 1))
                {
                    //----- Education TestPlan > Desktop Tab > R1 > T1: Verify Mega Menu Redesign - Education (Navigation & Layout) (IQ-9338/AL-16399) -----//

                    //--- Expected Result: Each column has a hyperlinked header ---//
                    test.validateElementIsPresent(driver, Elements.MainNavigation_EducationTab_EducationLibrary_Link);

                    //--- Expected Result: Each column has a hyperlinked sublinks ---//
                    test.validateElementIsPresent(driver, Elements.MainNavigation_EducationTab_EducationLibrary_Sec_Links);

                    //----- Education TestPlan > Desktop Tab > R1 > T2: Verify Mega Menu Redesign - Education (Education Library section) (IQ-9338/AL-16399) -----//
                    if (util.CheckElement(driver, Elements.MainNavigation_EducationTab_EducationLibrary_Link, 1))
                    {
                        //--- Action: Hover over the Education Library label ---//
                        action.IMouseOverTo(driver, Elements.MainNavigation_EducationTab_EducationLibrary_Link);

                        //--- Expected Result: Education Library label - Font color is Dark moderate magenta - #7c4a8a ---//
                        test.validateColorIsCorrect(driver, util.GetCssValue(driver, Elements.MainNavigation_EducationTab_EducationLibrary_Link, "text-decoration-color"), "#7c4a8a");

                        //--- Action: Click the Education Library label ---//
                        string current_url = driver.Url;
                        action.IClick(driver, Elements.MainNavigation_EducationTab_EducationLibrary_Link);

                        if (driver.Url.Contains(current_url))
                        {
                            driver.Close();
                            driver.SwitchTo().Window(driver.WindowHandles.Last());
                        }

                        //--- Expected Result: The System displays the Education Library page ---//
                        test.validateStringInstance(driver, driver.Url, educationLibrary_page_url);

                        //if (driver.Url.Contains(educationLibrary_page_url))
                        //{
                            ////--- Expected Result: The Education tab remains in active state (Font color is White - #ffffff) ---//
                            //test.validateColorIsCorrect(driver, util.GetCssValue(driver, Elements.MainNavigation_EducationTab, "text-decoration-color"), "#ffffff");

                            ////--- Expected Result: The Education tab remains in active state (Tab color is Dark moderate magenta - #7c4a8a) ---//
                            //test.validateColorIsCorrect(driver, util.GetCssValue(driver, Elements.MainNavigation_EducationTab, "background-color"), "#7c4a8a");

                            //if (util.CheckElement(driver, Elements.MainNavigation_SupportTab, 1))
                            //{
                            //    //--- Action: Click the Support tab ---//
                            //    action.IClick(driver, Elements.MainNavigation_SupportTab);

                            //    //--- Expected Result: The Education tab remains in active state (Font color is White - #ffffff) ---//
                            //    test.validateColorIsCorrect(driver, util.GetCssValue(driver, Elements.MainNavigation_EducationTab, "text-decoration-color"), "#ffffff");

                            //    //--- Expected Result: The Education tab remains in active state (Tab color is Dark moderate magenta - #7c4a8a) ---//
                            //    test.validateColorIsCorrect(driver, util.GetCssValue(driver, Elements.MainNavigation_EducationTab, "background-color"), "#7c4a8a");
                            //}
                        //}
                    }

                    //--- Action: Click the Education tab ---//
                    if (!util.CheckElement(driver, Elements.MainNavigation_EducationTab_Expanded, 1))
                    {
                        action.IClick(driver, Elements.MainNavigation_EducationTab);
                    }

                    //--- ANALOG CIRCUIT DESIGN TUTORIALS - LINK ---//
                    if (util.CheckElement(driver, Elements.MainNavigation_EducationTab_AnalogCircuitDesignTutorials_Link, 1))
                    {
                        //--- Action: Hover over the Analog Circuit Design Tutorials label ---//
                        action.IMouseOverTo(driver, Elements.MainNavigation_EducationTab_AnalogCircuitDesignTutorials_Link);

                        //--- Expected Result: Analog Circuit Design Tutorials label - Font color is Dark moderate magenta - #7c4a8a ---//
                        test.validateColorIsCorrect(driver, util.GetCssValue(driver, Elements.MainNavigation_EducationTab_AnalogCircuitDesignTutorials_Link, "text-decoration-color"), "#7c4a8a");

                        //--- Action: Click the Analog Circuit Design Tutorials label ---//  
                        string current_url = driver.Url;
                        action.IClick(driver, Elements.MainNavigation_EducationTab_AnalogCircuitDesignTutorials_Link);

                        //--- Action: If the Linked Opened in a New Tab, Switch to the Last Tab ---//
                        if (driver.Url.Contains(current_url))
                        {
                            driver.Close();
                            driver.SwitchTo().Window(driver.WindowHandles.Last());
                        }

                        //--- Expected Result: The System displays the Analog Circuit Design Tutorials page ---//
                        test.validateStringInstance(driver, driver.Url, tutorials_page_url);
                    }

                    //--- Action: Click the Education tab ---//
                    if (!util.CheckElement(driver, Elements.MainNavigation_EducationTab_Expanded, 1))
                    {
                        action.IClick(driver, Elements.MainNavigation_EducationTab);
                    }

                    //--- VIDEOS - LINK ---//
                    if (util.CheckElement(driver, Elements.MainNavigation_EducationTab_Videos_Link, 1))
                    {
                        //--- Action: Click the Videos label ---//
                        string current_url = driver.Url;
                        action.IClick(driver, Elements.MainNavigation_EducationTab_Videos_Link);

                        //--- Action: If the Linked Opened in a New Tab, Switch to the Last Tab ---//
                        if (driver.Url.Contains(current_url))
                        {
                            driver.Close();
                            driver.SwitchTo().Window(driver.WindowHandles.Last());
                        }

                        //--- Expected Result: The System displays the Videos page ---//
                        test.validateStringInstance(driver, driver.Url, videos_page_url);
                    }

                    //--- Action: Click the Education tab ---//
                    if (!util.CheckElement(driver, Elements.MainNavigation_EducationTab_Expanded, 1))
                    {
                        action.IClick(driver, Elements.MainNavigation_EducationTab);
                    }

                    //--- WEBCASTS - LINK ---//
                    if (util.CheckElement(driver, Elements.MainNavigation_EducationTab_Webcasts_Link, 1))
                    {
                        //--- Action: Click the Webcasts label ---//
                        string current_url = driver.Url;
                        action.IClick(driver, Elements.MainNavigation_EducationTab_Webcasts_Link);

                        //--- Action: If the Linked Opened in a New Tab, Switch to the Last Tab ---//
                        if (driver.Url.Contains(current_url))
                        {
                            driver.Close();
                            driver.SwitchTo().Window(driver.WindowHandles.Last());
                        }

                        //--- Expected Result: The System displays the Webcasts page ---//
                        test.validateStringInstance(driver, driver.Url, webcasts_page_url);
                    }

                    if (Locale.Equals("en"))
                    {
                        //--- Action: Click the Education tab ---//
                        if (!util.CheckElement(driver, Elements.MainNavigation_EducationTab_Expanded, 1))
                        {
                            action.IClick(driver, Elements.MainNavigation_EducationTab);
                        }

                        int educationLibrary_sec_sub_links_count = util.GetCount(driver, By.CssSelector(EducationTab_EducationLibrary_Sec_Locator + ">ul>li>a"));

                        bool faqs_link = false;
                        bool technicalArticles_link = false;
                        bool technicalBooks_link = false;
                        bool trainingandTutorials_link = false;
                        bool whitePapersAndCaseStudies_link = false;

                        for (int educationLibrary_sec_sub_links_ctr = 1; educationLibrary_sec_sub_links_ctr <= educationLibrary_sec_sub_links_count; educationLibrary_sec_sub_links_ctr++)
                        {
                            //--- Action: Click the Education tab ---//
                            if (!util.CheckElement(driver, Elements.MainNavigation_EducationTab_Expanded, 1))
                            {
                                action.IClick(driver, Elements.MainNavigation_EducationTab);
                            }

                            if (util.CheckElement(driver, By.CssSelector(EducationTab_EducationLibrary_Sec_Locator + ">ul>li:nth-of-type(" + educationLibrary_sec_sub_links_ctr + ")>a[href*='/search.html']"), 2))
                            {
                                if (faqs_link == true && technicalArticles_link == true && technicalBooks_link == true && trainingandTutorials_link == true && whitePapersAndCaseStudies_link == true)
                                {
                                    break;
                                }
                                else if (util.GetText(driver, By.CssSelector(EducationTab_EducationLibrary_Sec_Locator + ">ul>li:nth-of-type(" + educationLibrary_sec_sub_links_ctr + ")>a")).Contains(faqs_txt))
                                {
                                    //--- FAQS - LINK ---//
                                    faqs_link = true;
                                    string faqs_link_locator = EducationTab_EducationLibrary_Sec_Locator + ">ul>li:nth-of-type(" + educationLibrary_sec_sub_links_ctr + ")>a";

                                    //--- Action: Click the FAQs label ---//
                                    string current_url = driver.Url;
                                    action.IClick(driver, By.CssSelector(faqs_link_locator));

                                    if (driver.Url.Contains(current_url))
                                    {
                                        driver.Close();
                                        driver.SwitchTo().Window(driver.WindowHandles.Last());
                                    }

                                    //--- Expected Result: The System displays the search results page ---//
                                    test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + search_page_url);

                                    if (driver.Url.Contains(educationLibrary_page_url))
                                    {
                                        //--- Expected Result: The Technical Documentations resource pre-selected in Resources filters ---//
                                        test.validateStringIsCorrect(driver, Elements.GlobalSearchResults_Resources_Selected_Filter_Lbl, technicalDocumentations_txt);

                                        //--- Expected Result: The sub-menu Frequently Asked Questions pre-selected in Resources filters ---//
                                        test.validateStringIsCorrect(driver, Elements.GlobalSearchResults_Resources_Selected_Subfilter_Lbl, frequentlyAskedQuestions_txt);
                                    }
                                }
                                else if (util.GetText(driver, By.CssSelector(EducationTab_EducationLibrary_Sec_Locator + ">ul>li:nth-of-type(" + educationLibrary_sec_sub_links_ctr + ")>a")).Contains(technicalArticles_txt))
                                {
                                    //--- TECHNICAL ARTICLES - LINK ---//
                                    technicalArticles_link = true;
                                    string technicalArticles_link_locator = EducationTab_EducationLibrary_Sec_Locator + ">ul>li:nth-of-type(" + educationLibrary_sec_sub_links_ctr + ")>a";

                                    //--- Action: Click the Technical Articles label ---//
                                    string current_url = driver.Url;
                                    action.IClick(driver, By.CssSelector(technicalArticles_link_locator));

                                    if (driver.Url.Contains(current_url))
                                    {
                                        driver.Close();
                                        driver.SwitchTo().Window(driver.WindowHandles.Last());
                                    }

                                    //--- Expected Result: The System displays the search results page ---//
                                    test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + search_page_url);

                                    if (driver.Url.Contains(educationLibrary_page_url))
                                    {
                                        //--- Expected Result: The Technical Documentations resource pre-selected in Resources filters ---//
                                        test.validateStringIsCorrect(driver, Elements.GlobalSearchResults_Resources_Selected_Filter_Lbl, technicalDocumentations_txt);

                                        //--- Expected Result: The sub-menu Technical Articles pre-selected in Resources filters ---//
                                        test.validateStringIsCorrect(driver, Elements.GlobalSearchResults_Resources_Selected_Subfilter_Lbl, technicalArticles_txt);
                                    }
                                }
                                else if (util.GetText(driver, By.CssSelector(EducationTab_EducationLibrary_Sec_Locator + ">ul>li:nth-of-type(" + educationLibrary_sec_sub_links_ctr + ")>a")).Contains(technicalBooks_txt))
                                {
                                    //---  TECHNICAL BOOKS - LINK ---//
                                    technicalBooks_link = true;
                                    string technicalBooks_link_locator = EducationTab_EducationLibrary_Sec_Locator + ">ul>li:nth-of-type(" + educationLibrary_sec_sub_links_ctr + ")>a";

                                    //--- Action: Click the Technical Books label ---//
                                    string current_url = driver.Url;
                                    action.IClick(driver, By.CssSelector(technicalBooks_link_locator));

                                    if (driver.Url.Contains(current_url))
                                    {
                                        driver.Close();
                                        driver.SwitchTo().Window(driver.WindowHandles.Last());
                                    }

                                    //--- Expected Result: The System displays the search results page ---//
                                    test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + search_page_url);

                                    if (driver.Url.Contains(educationLibrary_page_url))
                                    {
                                        //--- Expected Result: The Education resource pre-selected in Resources filters ---//
                                        test.validateStringIsCorrect(driver, Elements.GlobalSearchResults_Resources_Selected_Filter_Lbl, education_txt);

                                        //--- Expected Result: The its sub-menu Technical Books pre-selected in Resources filters ---//
                                        test.validateStringIsCorrect(driver, Elements.GlobalSearchResults_Resources_Selected_Subfilter_Lbl, technicalBooks_txt);
                                    }
                                }
                                else if (util.GetText(driver, By.CssSelector(EducationTab_EducationLibrary_Sec_Locator + ">ul>li:nth-of-type(" + educationLibrary_sec_sub_links_ctr + ")>a")).Contains(trainingandTutorials_txt))
                                {
                                    //--- TRAINING AND TUTORIALS - LINK ---//
                                    trainingandTutorials_link = true;
                                    string trainingandTutorials_link_locator = EducationTab_EducationLibrary_Sec_Locator + ">ul>li:nth-of-type(" + educationLibrary_sec_sub_links_ctr + ")>a";

                                    //--- Action: Click the Training and Tutorials label ---//
                                    string current_url = driver.Url;
                                    action.IClick(driver, By.CssSelector(trainingandTutorials_link_locator));

                                    if (driver.Url.Contains(current_url))
                                    {
                                        driver.Close();
                                        driver.SwitchTo().Window(driver.WindowHandles.Last());
                                    }

                                    //--- Expected Result: The System displays the search results page ---//
                                    test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + search_page_url);

                                    if (driver.Url.Contains(educationLibrary_page_url))
                                    {
                                        //--- Expected Result: The Education resource in Resources filters ---//
                                        test.validateStringIsCorrect(driver, Elements.GlobalSearchResults_Resources_Selected_Filter_Lbl, technicalDocumentations_txt);
                                    }
                                }
                                else if (util.GetText(driver, By.CssSelector(EducationTab_EducationLibrary_Sec_Locator + ">ul>li:nth-of-type(" + educationLibrary_sec_sub_links_ctr + ")>a")).Contains(whitePapersAndCaseStudies_txt))
                                {
                                    //--- WHITE PAPERS AND CASE STUDIES - LINK ---//
                                    whitePapersAndCaseStudies_link = true;
                                    string whitePapersAndCaseStudies_link_locator = EducationTab_EducationLibrary_Sec_Locator + ">ul>li:nth-of-type(" + educationLibrary_sec_sub_links_ctr + ")>a";

                                    //--- Action: Click the White Papers and Case Studies label ---//
                                    string current_url = driver.Url;
                                    action.IClick(driver, By.CssSelector(whitePapersAndCaseStudies_link_locator));

                                    if (driver.Url.Contains(current_url))
                                    {
                                        driver.Close();
                                        driver.SwitchTo().Window(driver.WindowHandles.Last());
                                    }

                                    //--- Expected Result: The System displays the search results page ---//
                                    test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + search_page_url);

                                    if (driver.Url.Contains(educationLibrary_page_url))
                                    {
                                        //--- Expected Result: The White Papers and Case Studies resource pre-selected in Resources filters ---//
                                        test.validateStringIsCorrect(driver, Elements.GlobalSearchResults_Resources_Selected_Filter_Lbl, whitePapersAndCaseStudies_txt);
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.MainNavigation_EducationTab_Expanded);
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.MainNavigation_EducationTab);
            }
        }

        [Test, Category("Education"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Education Engagement Section in the Education Mega Menu is Present and Working as Expected as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Education Engagement Section in the Education Mega Menu is Present and Working as Expected as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Education Engagement Section in the Education Mega Menu is Present and Working as Expected as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Education Engagement Section in the Education Mega Menu is Present and Working as Expected as Expected in RU Locale")]
        public void Education_MegaMenu_VerifyEducationEngagementInEducationMegaMenu(string Locale)
        {
            //--- Action: Navigate to Analog Homepage ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + home_page_url);
            Thread.Sleep(2000);

            if (util.CheckElement(driver, Elements.MainNavigation_EducationTab, 1))
            {
                //--- Action: Click the Education tab ---//
                if (!util.CheckElement(driver, Elements.MainNavigation_EducationTab_Expanded, 1))
                {
                    action.IClick(driver, Elements.MainNavigation_EducationTab);
                    Thread.Sleep(1000);
                }

                if (util.CheckElement(driver, Elements.MainNavigation_EducationTab_Expanded, 1))
                {
                    //----- Education TestPlan > Desktop Tab > R1 > T1: Verify Mega Menu Redesign - Education (Navigation & Layout) (IQ-9338/AL-16399) -----//

                    //--- Expected Result: Each column has a hyperlinked header ---//
                    if (Locale.Equals("cn"))
                    {
                        test.validateElementIsPresent(driver, Elements.MainNavigation_EducationTab_EducationEngagement_China_Link);
                    }
                    else
                    {
                        test.validateElementIsPresent(driver, Elements.MainNavigation_EducationTab_EducationEngagement_Link);
                    }
                    //--- Expected Result: Each column has a hyperlinked sublinks ---//
                    test.validateElementIsPresent(driver, Elements.MainNavigation_EducationTab_EducationEngagement_Sec_Links);

                    //----- Education TestPlan > Desktop Tab > R1 > T3: Verify Mega Menu Redesign - Education (Education Engagement section) (IQ-9338/AL-16399) -----//
                    if (util.CheckElement(driver, Elements.MainNavigation_EducationTab_EducationEngagement_Link, 1))
                    {
                        //--- Action: Hover over the Education Engagement label ---//
                        action.IMouseOverTo(driver, Elements.MainNavigation_EducationTab_EducationEngagement_Link);

                        //--- Expected Result: Education Engagement label - Font color is Dark moderate magenta - #7c4a8a ---//
                        test.validateColorIsCorrect(driver, util.GetCssValue(driver, Elements.MainNavigation_EducationTab_EducationEngagement_Link, "text-decoration-color"), "#7c4a8a");

                        //--- Action: Click the Education Engagement label ---//
                        string current_url = driver.Url;
                        action.IClick(driver, Elements.MainNavigation_EducationTab_EducationEngagement_Link);

                        if (driver.Url.Contains(current_url))
                        {
                            driver.Close();
                            driver.SwitchTo().Window(driver.WindowHandles.Last());
                        }

                        //--- Expected Result: The System displays the Education Engagement page ---//
                        test.validateStringInstance(driver, driver.Url, educationEngagement_page_url);

                        //if (driver.Url.Contains(educationEngagement_page_url))
                        //{
                        //    //--- Expected Result: The Education tab remains in active state (Font color is White - #ffffff) ---//
                        //    test.validateColorIsCorrect(driver, util.GetCssValue(driver, Elements.MainNavigation_EducationTab, "text-decoration-color"), "#ffffff");

                        //    //--- Expected Result: The Education tab remains in active state (Tab color is Dark moderate magenta - #7c4a8a) ---//
                        //    test.validateColorIsCorrect(driver, util.GetCssValue(driver, Elements.MainNavigation_EducationTab, "background-color"), "#7c4a8a");

                        //    if (util.CheckElement(driver, Elements.MainNavigation_SupportTab, 1))
                        //    {
                        //        //--- Action: Click the Support tab ---//
                        //        action.IClick(driver, Elements.MainNavigation_SupportTab);

                        //        //--- Expected Result: The Education tab remains in active state (Font color is White - #ffffff) ---//
                        //        test.validateColorIsCorrect(driver, util.GetCssValue(driver, Elements.MainNavigation_EducationTab, "text-decoration-color"), "#ffffff");

                        //        //--- Expected Result: The Education tab remains in active state (Tab color is Dark moderate magenta - #7c4a8a) ---//
                        //        test.validateColorIsCorrect(driver, util.GetCssValue(driver, Elements.MainNavigation_EducationTab, "background-color"), "#7c4a8a");
                        //    }
                        //}
                    }

                    //--- Action: Click the Education tab ---//
                    if (!util.CheckElement(driver, Elements.MainNavigation_EducationTab_Expanded, 1))
                    {
                        action.IClick(driver, Elements.MainNavigation_EducationTab);
                    }

                    //--- NORTH AMERICA - LINK ---//
                    if (util.CheckElement(driver, Elements.MainNavigation_EducationTab_NorthAmerica_Link, 1))
                    {
                        //--- Action: Hover over the North America label ---//
                        action.IMouseOverTo(driver, Elements.MainNavigation_EducationTab_NorthAmerica_Link);

                        //--- Expected Result: North America label - Font color is Dark moderate magenta - #7c4a8a ---//
                        test.validateColorIsCorrect(driver, util.GetCssValue(driver, Elements.MainNavigation_EducationTab_NorthAmerica_Link, "text-decoration-color"), "#7c4a8a");

                        //--- Action: Click the North America label ---//  
                        string current_url = driver.Url;
                        action.IClick(driver, Elements.MainNavigation_EducationTab_NorthAmerica_Link);

                        //--- Action: If the Linked Opened in a New Tab, Switch to the Last Tab ---//
                        if (driver.Url.Contains(current_url))
                        {
                            driver.Close();
                            driver.SwitchTo().Window(driver.WindowHandles.Last());
                        }

                        //--- Expected Result: The System displays the North America page ---//
                        test.validateStringInstance(driver, driver.Url, northAmerica_page_url);
                    }

                    //--- Action: Click the Education tab ---//
                    if (!util.CheckElement(driver, Elements.MainNavigation_EducationTab_Expanded, 1))
                    {
                        action.IClick(driver, Elements.MainNavigation_EducationTab);
                    }

                    //--- INDIA - LINK ---//
                    if (util.CheckElement(driver, Elements.MainNavigation_EducationTab_India_Link, 1))
                    {
                        //--- Action: Click the India label ---//
                        string current_url = driver.Url;
                        action.IClick(driver, Elements.MainNavigation_EducationTab_India_Link);

                        //--- Action: If the Linked Opened in a New Tab, Switch to the Last Tab ---//
                        if (driver.Url.Contains(current_url))
                        {
                            driver.Close();
                            driver.SwitchTo().Window(driver.WindowHandles.Last());
                        }

                        //--- Expected Result: The System displays the India (ANVESHAN 2020 FELLOWSHIP) page ---//
                        if (Locale.Equals("cn") || Locale.Equals("jp"))
                        {
                            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/index.html");
                        }
                        else
                        {
                            test.validateStringInstance(driver, driver.Url, anveshanFellowship_page_url);
                        }
                    }

                    //--- Action: Click the Education tab ---//
                    if (!util.CheckElement(driver, Elements.MainNavigation_EducationTab_Expanded, 1))
                    {
                        action.IClick(driver, Elements.MainNavigation_EducationTab);
                    }

                    //--- PHILIPPINES - LINK ---//
                    if (util.CheckElement(driver, Elements.MainNavigation_EducationTab_Philippines_Link, 1))
                    {
                        //--- Action: Click the  Philippines label ---//
                        string current_url = driver.Url;
                        action.IClick(driver, Elements.MainNavigation_EducationTab_Philippines_Link);

                        //--- Action: If the Linked Opened in a New Tab, Switch to the Last Tab ---//
                        if (driver.Url.Contains(current_url))
                        {
                            driver.Close();
                            driver.SwitchTo().Window(driver.WindowHandles.Last());
                        }

                        //--- Expected Result: The System displays the  Philippines page ---//
                        test.validateStringInstance(driver, driver.Url, philippines_page_url);
                    }
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.MainNavigation_EducationTab_Expanded);
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.MainNavigation_EducationTab);
            }
        }

        [Test, Category("Education"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Modules & Courseware Section in the Education Mega Menu is Present and Working as Expected as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Modules & Courseware Section in the Education Mega Menu is Present and Working as Expected as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Modules & Courseware Section in the Education Mega Menu is Present and Working as Expected as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Modules & Courseware Section in the Education Mega Menu is Present and Working as Expected as Expected in RU Locale")]
        public void Education_MegaMenu_VerifyModulesAndCoursewareInEducationMegaMenu(string Locale)
        {
            //--- Action: Navigate to Analog Homepage ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + home_page_url);
            Thread.Sleep(3000);

            if (util.CheckElement(driver, Elements.MainNavigation_EducationTab, 1))
            {
                //--- Action: Click the Education tab ---//
                if (!util.CheckElement(driver, Elements.MainNavigation_EducationTab_Expanded, 1))
                {
                    action.IClick(driver, Elements.MainNavigation_EducationTab);
                    Thread.Sleep(1000);
                }

                if (util.CheckElement(driver, Elements.MainNavigation_EducationTab_Expanded, 1))
                {
                    //----- Education TestPlan > Desktop Tab > R1 > T1: Verify Mega Menu Redesign - Education (Navigation & Layout) (IQ-9338/AL-16399) -----//

                    //--- Expected Result: Each column has a hyperlinked header ---//
                    test.validateElementIsPresent(driver, Elements.MainNavigation_EducationTab_ModulesAndCourseware_Link);

                    //--- Expected Result: Each column has a hyperlinked sublinks ---//
                    test.validateElementIsPresent(driver, Elements.MainNavigation_EducationTab_ModulesAndCourseware_Sec_Links);

                    //----- Education TestPlan > Desktop Tab > R1 > T4: Verify Mega Menu Redesign - Education (Modules & Courseware section) (IQ-9338/AL-16399) -----//
                    if (util.CheckElement(driver, Elements.MainNavigation_EducationTab_ModulesAndCourseware_Link, 1))
                    {
                        //--- Action: Hover over the Modules & Courseware label ---//
                        action.IMouseOverTo(driver, Elements.MainNavigation_EducationTab_ModulesAndCourseware_Link);

                        //--- Expected Result: Modules & Courseware label - Font color is Dark moderate magenta - #7c4a8a ---//
                        test.validateColorIsCorrect(driver, util.GetCssValue(driver, Elements.MainNavigation_EducationTab_ModulesAndCourseware_Link, "text-decoration-color"), "#7c4a8a");

                        //--- Action: Click the Modules & Courseware label ---//
                        string current_url = driver.Url;
                        action.IClick(driver, Elements.MainNavigation_EducationTab_ModulesAndCourseware_Link);

                        if (driver.Url.Contains(current_url))
                        {
                            driver.Close();
                            driver.SwitchTo().Window(driver.WindowHandles.Last());
                        }

                        //--- Expected Result: The System displays the Modules & Courseware page ---//
                        test.validateStringInstance(driver, driver.Url, modulesAndCourseware_page_url);

                        if (driver.Url.Contains(modulesAndCourseware_page_url))
                        {
                            //--- Expected Result: The Education tab remains in active state (Font color is White - #ffffff) ---//
                            test.validateColorIsCorrect(driver, util.GetCssValue(driver, Elements.MainNavigation_EducationTab, "text-decoration-color"), "#ffffff");

                            //--- Expected Result: The Education tab remains in active state (Tab color is Dark moderate magenta - #7c4a8a) ---//
                            test.validateColorIsCorrect(driver, util.GetCssValue(driver, Elements.MainNavigation_EducationTab, "background-color"), "#7c4a8a");

                            if (util.CheckElement(driver, Elements.MainNavigation_SupportTab, 1))
                            {
                                //--- Action: Click the Support tab ---//
                                action.IClick(driver, Elements.MainNavigation_SupportTab);

                                //--- Expected Result: The Education tab remains in active state (Font color is White - #ffffff) ---//
                                test.validateColorIsCorrect(driver, util.GetCssValue(driver, Elements.MainNavigation_EducationTab, "text-decoration-color"), "#ffffff");

                                //--- Expected Result: The Education tab remains in active state (Tab color is Dark moderate magenta - #7c4a8a) ---//
                                test.validateColorIsCorrect(driver, util.GetCssValue(driver, Elements.MainNavigation_EducationTab, "background-color"), "#7c4a8a");
                            }
                        }
                    }

                    //--- Action: Click the Education tab ---//
                    if (!util.CheckElement(driver, Elements.MainNavigation_EducationTab_Expanded, 1))
                    {
                        action.IClick(driver, Elements.MainNavigation_EducationTab);
                    }

                    //--- ACTIVE LEARNING MODULES - LINK ---//
                    if (util.CheckElement(driver, Elements.MainNavigation_EducationTab_ActiveLearningModules_Link, 1))
                    {
                        //--- Action: Hover over the  Active Learning Modules label ---//
                        action.IMouseOverTo(driver, Elements.MainNavigation_EducationTab_ActiveLearningModules_Link);

                        //--- Expected Result: Active Learning Modules label - Font color is Dark moderate magenta - #7c4a8a ---//
                        test.validateColorIsCorrect(driver, util.GetCssValue(driver, Elements.MainNavigation_EducationTab_ActiveLearningModules_Link, "text-decoration-color"), "#7c4a8a");

                        //--- Action: Click the  Active Learning Modules label ---//  
                        string current_url = driver.Url;
                        action.IClick(driver, Elements.MainNavigation_EducationTab_ActiveLearningModules_Link);

                        //--- Action: If the Linked Opened in a New Tab, Switch to the Last Tab ---//
                        if (driver.Url.Contains(current_url))
                        {
                            driver.Close();
                            driver.SwitchTo().Window(driver.WindowHandles.Last());
                        }

                        //--- Expected Result: The System displays the  Active Learning Modules page ---//
                        test.validateStringInstance(driver, driver.Url, activeLearningModules_page_url);
                    }

                    //--- Action: Click the Education tab ---//
                    if (!util.CheckElement(driver, Elements.MainNavigation_EducationTab_Expanded, 1))
                    {
                        action.IClick(driver, Elements.MainNavigation_EducationTab);
                    }
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.MainNavigation_EducationTab_Expanded);
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.MainNavigation_EducationTab);
            }
        }
    }
}