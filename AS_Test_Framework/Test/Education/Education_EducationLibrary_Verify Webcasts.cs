﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;

namespace AS_Test_Framework.Education
{
    [TestFixture]
    public class Education_EducationLibrary_Webcasts : BaseSetUp
    {
        public Education_EducationLibrary_Webcasts() : base() { }

        //--- URLs ---//
        string webcasts_page_url = "/education/education-library/webcasts.html";
        string search_page_url = "/search.html";

        //--- Labels ---//
        string educationLibrary_txt = "Education Library";
        string webcasts_txt = "Webcasts";

        [Test, Category("Education"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Left Rail Navigation is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Left Rail Navigation is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Left Rail Navigation is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Left Rail Navigation is Present and Working as Expected in RU Locale")]
        public void Education_EducationLibrary_Webcasts_VerifyLeftRailNavigation(string Locale)
        {
            //--- Action: Go to the Webcasts Page ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + webcasts_page_url);

            //----- Webcasts TestPlan > Desktop Tab > R1 > T1: Verify Webcasts landing page -----//

            if (Locale.Equals("en"))
            {
                //--- Expected Result: Education Library should be selected on navigation rail ---//
                test.validateStringIsCorrect(driver, Elements.Education_EducationLibrary_LeftRail_Selected_Sec_Link, educationLibrary_txt);
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.Education_EducationLibrary_LeftRail_Selected_Sec_Link);
            }

            if (Locale.Equals("en"))
            {
                //--- Expected Result: Webcasts will be on bold. ---//
                test.validateStringIsCorrect(driver, Elements.Education_EducationLibrary_LeftRail_Selected_Val_Link, webcasts_txt);
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.Education_EducationLibrary_LeftRail_Selected_Val_Link);
            }
        }

        [Test, Category("Education"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Webcasts Page is Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Webcasts Page is Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Webcasts Page is Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Webcasts Page is Working as Expected in RU Locale")]
        public void Education_EducationLibrary_VerifyWebcastsPage(string Locale)
        {
            //--- Action: Go to the Webcasts Page ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + webcasts_page_url);

            //----- Webcasts TestPlan > Desktop Tab > R1 > T2: Verify the Page Header -----//

            //--- Expected Result: Title should be displayed. ---//
            test.validateElementIsPresent(driver, Elements.Education_Webcasts_Page_Title_Lbl);

            //--- Expected Result: Page Description should be displayed. ---//
            test.validateElementIsPresent(driver, Elements.Education_Webcasts_Page_Desc_Txt);
        }

        [Test, Category("Education"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that Find Webcasts is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that Find Webcasts is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that Find Webcasts is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that Find Webcasts is Present and Working as Expected in RU Locale")]
        public void Education_EducationLibrary_Webcasts_VerifyFindWebcasts(string Locale)
        {
            //--- Action: Go to the Webcasts Page ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + webcasts_page_url);

            //----- Webcasts TestPlan > Desktop Tab > R1 > T1: Verify Webcasts landing page -----//

            //--- Expected Result: The Search Section should be present in webcasts landing page ---//
            test.validateElementIsPresent(driver, Elements.Education_Webcasts_FindWebcasts_InputBx);
            test.validateElementIsPresent(driver, Elements.Education_Webcasts_FindWebcasts_Search_Btn);

            if (util.CheckElement(driver, Elements.Education_Webcasts_FindWebcasts_InputBx, 1) && util.CheckElement(driver, Elements.Education_Webcasts_FindWebcasts_Search_Btn, 1))
            {
                string search_input = null;

                //--- Action: Type any text on the search textbox (ex: webcast) ---//
                search_input = "adi";
                action.IDeleteValueOnFields(driver, Elements.Education_Webcasts_FindWebcasts_InputBx);
                action.IType(driver, Elements.Education_Webcasts_FindWebcasts_InputBx, search_input);

                //--- Expected Result: The auto-suggest search dropdown must be displayed ---//
                test.validateElementIsPresent(driver, Elements.Education_Webcasts_FindWebcasts_AutoSuggest_Dd);

                //----- Education TestPlan > Desktop Tab > R2 > T1: Verify the contents of Education Library from the Education Drop down -----//

                //--- Action: click search button ---//
                action.IClick(driver, Elements.Education_Webcasts_FindWebcasts_Search_Btn);

                //--- Expected Result: The page should redirect to global search ---//
                test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + search_page_url);
            }
        }

        [Test, Category("Education"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that Browse by Specified Category is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that Browse by Specified Category is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that Browse by Specified Category is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that Browse by Specified Category is Present and Working as Expected in RU Locale")]
        public void Education_EducationLibrary_Webcasts_VerifyBrowseBySpecifiedCategory(string Locale)
        {
            //--- Action: Go to the Webcasts Page ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + webcasts_page_url);

            if (util.CheckElement(driver, Elements.Education_Webcasts_BrowseBySpecifiedCategory_ProductCategories_Dd, 1))
            {
                //----- Education TestPlan > Desktop Tab > R2 > T7: Verify the the user can access the Webcast Landing page -----//

                //--- Action: Click on Product Categories ---//
                action.IClick(driver, Elements.Education_Webcasts_BrowseBySpecifiedCategory_ProductCategories_Dd);

                //--- Expected Result: Categories are listed. ---//
                test.validateElementIsPresent(driver, Elements.Education_Webcasts_BrowseBySpecifiedCategory_ProductCategories_Dd_Menu);

                if (util.CheckElement(driver, Elements.Education_Webcasts_BrowseBySpecifiedCategory_ProductCategories_Dd_Menu, 1))
                {
                    //--- Action: Hover over category list to view subcategory ---//
                    action.IMouseOverTo(driver, Elements.Education_Webcasts_BrowseBySpecifiedCategory_ProductCategories_Dd_Menu_Val_Lbls);

                    //--- Expected Result: The subcategories should be displayed ---//
                    test.validateElementIsPresent(driver, Elements.Education_Webcasts_BrowseBySpecifiedCategory_ProductCategories_Subcategories_Dd_Menu);
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.Education_Webcasts_BrowseBySpecifiedCategory_ProductCategories_Dd);
            }
        }
    }
}