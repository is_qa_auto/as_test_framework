﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.Education
{
    [TestFixture]
    public class Education_EducationLibrary : BaseSetUp
    {
        public Education_EducationLibrary() : base() { }

        //--- URLs ---//
        string educationLibrary_page_url = "/education/education-library.html";
        string search_page_url = "/search.html";

        [Test, Category("Design Center"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Left Pane is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Left Pane is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Left Pane is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Left Pane is Present and Working as Expected in RU Locale")]
        public void Education_EducationLibrary_VerifyLeftPane(string Locale)
        {
            //--- Action: Go to Education Library page ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + educationLibrary_page_url);

            //----- Education TestPlan > Desktop Tab > R2 > T2: Verify the contents in the Education Library landing page (AL-7230) -----//

            //--- Expected Result: Listed items in the Education Library Section are displayed ---//
            test.validateElementIsPresent(driver, Elements.Education_EducationLibrary_LeftPane_Subsec_Links);

            //----- Education TestPlan > Desktop Tab > R2 > T13: Verify search section should not be available any more in the left pane of Education Library page (IQ-4339/AL-11339) -----//

            if (util.CheckElement(driver, Elements.Education_EducationLibrary_LeftPane_TechnicalArticles_Link, 2))
            {
                //--- Action: In the left pane under Education Library, click on "Technical Articles" ---//
                action.IOpenLinkInNewTab(driver, Elements.Education_EducationLibrary_LeftPane_TechnicalArticles_Link);
                Thread.Sleep(2000);

                //--- Expected Result: The page should redirect to global search page. ---//
                test.validateStringInstance(driver, driver.Url, search_page_url);

                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles.First());
            }

            if (util.CheckElement(driver, Elements.Education_EducationLibrary_LeftPane_Faqs_Link, 2))
            {
                //--- Action: In the left pane under Education Library, click on "FAQs" ---//
                action.IOpenLinkInNewTab(driver, Elements.Education_EducationLibrary_LeftPane_Faqs_Link);

                //--- Expected Result: The page should redirect to global search page. ---//
                test.validateStringInstance(driver, driver.Url, Locale + search_page_url);

                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles.First());
            }

            if (util.CheckElement(driver, Elements.Education_EducationLibrary_LeftPane_TechnicalBooks_Link, 2))
            {
                //--- Action: In the left pane under Education Library, click on "Technical Books" ---//
                action.IOpenLinkInNewTab(driver, Elements.Education_EducationLibrary_LeftPane_TechnicalBooks_Link);
                Thread.Sleep(1000);

                //--- Expected Result: The page should redirect to global search page. ---//
                test.validateStringInstance(driver, driver.Url, search_page_url);

                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles.First());
            }

            if (util.CheckElement(driver, Elements.Education_EducationLibrary_LeftPane_TrainingAndTutorials_Link, 2))
            {
                //--- Action: In the left pane under Education Library, click on "Training and Tutorials" ---//
                action.IOpenLinkInNewTab(driver, Elements.Education_EducationLibrary_LeftPane_TrainingAndTutorials_Link);
                Thread.Sleep(1000);

                //--- Expected Result: The page should redirect to global search page. ---//
                test.validateStringInstance(driver, driver.Url, search_page_url);

                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles.First());
            }

            if (util.CheckElement(driver, Elements.Education_EducationLibrary_LeftPane_WhitePapersAndCaseStudies_Link, 2))
            {
                //--- Action: In the left pane under Education Library, click on "White Papers and Case Studies" ---//
                action.IOpenLinkInNewTab(driver, Elements.Education_EducationLibrary_LeftPane_WhitePapersAndCaseStudies_Link);
                Thread.Sleep(1000);

                //--- Expected Result: The page should redirect to global search page. ---//
                test.validateStringInstance(driver, driver.Url, search_page_url);
            }
        }

        [Test, Category("Education"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Education Library Page is Working as Expected in EN Locale", Category = "Smoke_ADIWeb")]
        [TestCase("cn", TestName = "Verify that the Education Library Page is Working as Expected in CN Locale", Category = "Smoke_ADIWeb")]
        [TestCase("jp", TestName = "Verify that the Education Library Page is Working as Expected in JP Locale", Category = "Smoke_ADIWeb")]
        [TestCase("ru", TestName = "Verify that the Education Library Page is Working as Expected in RU Locale")]
        public void Education_VerifyEducationLibrary(string Locale)
        {
            string initial_steps = "1. Go to " + Configuration.Env_Url + Locale + educationLibrary_page_url;
            string scenario = "";
            //--- Action: Go to Education Library page ---//
            action.INavigate(driver, Configuration.Env_Url + Locale + educationLibrary_page_url);

            //----- Education TestPlan > Desktop Tab > R2 > T2: Verify the contents in the Education Library landing page (AL-7230) -----//

            //--- Expected Result: Page header is displayed ---//
            scenario = "Verify that Education library page title is present";
            test.validateElementIsPresentv2(driver, Elements.Education_EducationLibrary_Page_Title_Txt,scenario, initial_steps);

            //--- Expected Result: Hero Image is displayed. ---//
            scenario = "Verify that hero image is present in Education library page";
            test.validateElementIsPresentv2(driver, Elements.Education_EducationLibrary_Hero_Img,scenario, initial_steps);
        }
    }
}