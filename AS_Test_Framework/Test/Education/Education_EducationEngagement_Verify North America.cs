﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.Education
{
    [TestFixture]
    public class Education_EducationEngagement_NorthAmerica : BaseSetUp
    {
        public Education_EducationEngagement_NorthAmerica() : base() { }

        //--- URLs ---//
        string northAmerica_page_url = "/education/university-engagement/north-america.html";
        string localStrategicAdvocates_page_url = "/local-strategic-advocates.html";
        string onCampusRecruitment_page_url_en = "analog.com/new-college-grads";
        string onCampusRecruitment_page_url = "/about-adi/careers/students-and-graduates/on-campus-recruitment.html";
        string contactUs_page_url = "/contact-us.html";
        string newsAndTrends_page_url = "/news-and-trends.html";

        [Test, Category("Education"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Nav List is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Nav List is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Nav List is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Nav List is Present and Working as Expected in RU Locale")]
        public void Education_EducationEngagement_VerifyLeftRail(string Locale)
        {
            //--- Action: Go to the North America Page ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + northAmerica_page_url);

            //----- Education TestPlan > Desktop Tab > R2 > T2: Verify that North America landing page is available and accessible -----//

            //--- Expected Result: List of links are displayed ---//
            test.validateElementIsPresent(driver, Elements.Education_NorthAmerica_Nav_List);

            if (util.CheckElement(driver, Elements.Education_NorthAmerica_Nav_List, 1))
            {
                //--- LOCAL STRATEGIC ADVOCATES - LINK ---//
                if (util.CheckElement(driver, Elements.Education_NorthAmerica_Nav_List_LocalStrategicAdvocates_Link, 1))
                {
                    //--- Action: Click "Local Strategic Advocates" link ---//
                    action.IOpenLinkInNewTab(driver, Elements.Education_NorthAmerica_Nav_List_LocalStrategicAdvocates_Link);
                    Thread.Sleep(3000);

                    //--- Expected Result: Local Strategic Advocates landing page will be displayed ---//
                    test.validateStringInstance(driver, driver.Url, localStrategicAdvocates_page_url);

                    if (driver.Url.Contains(localStrategicAdvocates_page_url))
                    {
                        //--- Expected Result: list of articles for the advocates council ---//
                        test.validateElementIsPresent(driver, Elements.Education_LocalStrategicAdvocates_Articles);
                    }

                    driver.Close();
                    driver.SwitchTo().Window(driver.WindowHandles.First());
                }

                //--- CAMPUS RECRUITMENT - LINK ---//
                if (util.CheckElement(driver, Elements.Education_NorthAmerica_Nav_List_CampusRecruitment_Link, 1))
                {
                    //--- Action: Click "Campus Recruitment" link ---//
                    action.IOpenLinkInNewTab(driver, Elements.Education_NorthAmerica_Nav_List_CampusRecruitment_Link);
                    Thread.Sleep(2000);

                    if (Locale.Equals("en"))
                    {
                        //--- Expected Result: On Campus Recruitment landing page will be displayed ---//
                        test.validateStringInstance(driver, driver.Url, onCampusRecruitment_page_url_en);
                    }
                    else {
                        test.validateStringInstance(driver, driver.Url, onCampusRecruitment_page_url);

                        if (driver.Url.Contains(onCampusRecruitment_page_url))
                        {
                            //--- Expected Result: link to view North America Recruitment ---//
                            test.validateElementIsPresent(driver, Elements.Education_OnCampusRecruitment_NorthAmerica_Link);

                            //--- Expected Result: link to view Europe Recruitment ---//
                            test.validateElementIsPresent(driver, Elements.Education_OnCampusRecruitment_Ireland_Link);
                        }
                    }
                    driver.Close();
                    driver.SwitchTo().Window(driver.WindowHandles.First());
                }

                //--- CONTACT US - LINK ---//
                if (util.CheckElement(driver, Elements.Education_NorthAmerica_Nav_List_ContactUs_Link, 1))
                {
                    //--- Action: Click "Contact Us" link ---//
                    action.IOpenLinkInNewTab(driver, Elements.Education_NorthAmerica_Nav_List_ContactUs_Link);
                    Thread.Sleep(1000);

                    //--- Expected Result: Contact Us landing page will be displayed ---//
                    test.validateStringInstance(driver, driver.Url, contactUs_page_url);

                    if (driver.Url.Contains(contactUs_page_url))
                    {
                        //--- Expected Result: Analog Devices link to open a New Email Message ---//
                        test.validateElementIsPresent(driver, Elements.Education_ContactUs_AnalogDevices_Link);
                    }

                    driver.Close();
                    driver.SwitchTo().Window(driver.WindowHandles.First());
                }

                //--- NEWS AND TRENDS - LINK ---//
                if (util.CheckElement(driver, Elements.Education_NorthAmerica_Nav_List_NewsAndTrends_Link, 1))
                {
                    //--- Action: Click "News and Trends" link ---//
                    action.IOpenLinkInNewTab(driver, Elements.Education_NorthAmerica_Nav_List_NewsAndTrends_Link);

                    //--- Expected Result: News and Trends landing page will be displayed ---//
                    test.validateStringInstance(driver, driver.Url, newsAndTrends_page_url);
                    Thread.Sleep(1000);

                    if (driver.Url.Contains(newsAndTrends_page_url))
                    {
                        //--- Expected Result: list of titles and links of news and trends ---//
                        test.validateElementIsPresent(driver, Elements.Education_NewsAndTrends_Link_List);
                    }
                }
            }
        }
    }
}