﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;

namespace AS_Test_Framework.Education
{
    [TestFixture]
    public class Education_EducationLibrary_Webcasts_WebcastPage : BaseSetUp
    {
        public Education_EducationLibrary_Webcasts_WebcastPage() : base() { }

        [Test, Category("Education"), Category("Core"), Retry(2)]
        [TestCase("en", "/education/education-library/webcasts/solving-telecommunication-multiband-needs-with-wideband-amplifiers.html", TestName = "Verify that the Author Summary Section is Present and Working as Expected in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Author Summary Section is Present and Working as Expected in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Author Summary Section is Present and Working as Expected in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Author Summary Section is Present and Working as Expected in RU Locale")]
        public void Education_EducationLibrary_Webcasts_WebcastPage_VerifyAuthorSummarySection(string Locale, string WebcastPage_Url)
        {
            //--- Action: Go to a Webcast Page ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + WebcastPage_Url);

            //----- Webcasts TestPlan > Desktop Tab > R3 > T1: Verify Author Summary -----//

            //--- Expected Result: Author information should be displayed including the following attributes: Image ---//
            test.validateElementIsPresent(driver, Elements.Education_WebcastPage_AuthorSummary_Img);

            //--- Expected Result: Author information should be displayed including the following attributes: Name, Title(e.g.Applications engineer), Contact Info ---//
            test.validateElementIsPresent(driver, Elements.Education_WebcastPage_AuthorSummary_Details_Txt);
        }

        [Test, Category("Education"), Category("Core"), Retry(2)]
        [TestCase("en", "/education/education-library/webcasts/solving-telecommunication-multiband-needs-with-wideband-amplifiers.html", TestName = "Verify that the 3rd Column Section is Present and Working as Expected in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the 3rd Column Section is Present and Working as Expected in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the 3rd Column Section is Present and Working as Expected in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the 3rd Column Section is Present and Working as Expected in RU Locale")]
        public void Education_EducationLibrary_Webcasts_WebcastPage_Verify3rdColumnSection(string Locale, string WebcastPage_Url)
        {
            //--- Action: Go to a Webcast Page ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + WebcastPage_Url);

            //----- Webcasts TestPlan > Desktop Tab > R3 > T2: Verify Webcasts elements in the third column -----//

            //--- Expected Result: Webcasts title should be displayed ---//
            test.validateElementIsPresent(driver, Elements.Education_WebcastPage_3rdCol_Webcast_Title_Lbl);

            //--- Expected Result: Webcasts description should be displayed ---//
            test.validateElementIsPresent(driver, Elements.Education_WebcastPage_3rdCol_Webcast_Desc_Txt);

            //----- Webcasts TestPlan > Desktop Tab > R4 > T3: Verify Webcast Video -----//

            //--- Expected Result: The player should have the new player (HJgoScClQ) (IQ-5254/AL-12760) ---//
            if (util.CheckElement(driver, By.CssSelector("div[id*='vjs_video_']"), 2))
            {
                test.validateStringInstance(driver, "HJgoScClQ", util.ReturnAttribute(driver, By.CssSelector("div[id='PlayerContainer']>div"), "data-player"));
            }

            //----- Webcasts TestPlan > Desktop Tab > R3 > T2: Verify Webcasts elements in the third column -----//

            //--- Expected Result: The Author's Information should bedisplayed, including the following attributes: Image ---//
            test.validateElementIsPresent(driver, Elements.Education_WebcastPage_3rdCol_AuthorInfo_Img);

            //--- Expected Result: The Author's Information should bedisplayed, including the following attributes: Name, Title(e.g.Applications engineer), Contact Info ---//
            test.validateElementIsPresent(driver, Elements.Education_WebcastPage_3rdCol_AuthorInfo_Details_Txt);

            //--- Expected Result: The Author's Information should bedisplayed, including the following attributes: Bio ---//
            test.validateElementIsPresent(driver, Elements.Education_WebcastPage_3rdCol_AuthorInfo_Bio_Txt);
        }
    }
}