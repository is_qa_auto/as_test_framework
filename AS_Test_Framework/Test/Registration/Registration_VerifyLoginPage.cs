﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System;
using System.Threading;
using OpenQA.Selenium;
using System.Text.RegularExpressions;
using System.Linq;

namespace AS_Test_Framework.Registration
{

    [TestFixture]
    public class Registration_VerifyLoginPage : BaseSetUp
    {
        public Registration_VerifyLoginPage() : base() { }

        /****Remove due to al-18380*****/
        //[Test, Category("Registration"), Category("NonCore"), Retry(2)]
        //[TestCase("en", TestName = "Verify login button from myAnalog tab for EN Locale")]
        //[TestCase("cn", TestName = "Verify login button from myAnalog tab for CN Locale")]
        //[TestCase("jp", TestName = "Verify login button from myAnalog tab for JP Locale")]
        //public void Registration_VerifyLoginViaMyAnalogTab(string Locale) {

        //    action.Navigate(driver, Configuration.Env_Url + Locale + "/index.html");
        //    action.IClick(driver, Elements.MainNavigation_MyAnalogTab);
        //    test.validateElementIsPresent(driver, Elements.MyAnalogTab_LogInToMyAnalog_Btn);
        //}


        [Test, Category("Registration"), Category("NonCore")]
        [TestCase("en", "Log in or register with :", TestName = "Verify login page elements for EN Locale")]
        [TestCase("zh", "登录或使用以下方式注册：", TestName = "Verify login page elements for CN Locale")]
        [TestCase("jp", "既存のアカウントでログインまたは登録", TestName = "Verify login page elements for JP Locale")]
        public void Registration_VerifyLoginlements(string Locale, string SocialReg_Header)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/index.html");
            action.IClick(driver, Elements.MainNavigation_MyAnalogTab);
            //action.IClick(driver, Elements.MyAnalogTab_LogInToMyAnalog_Btn);
            Assert.Multiple(() =>
            {
                test.validateElementIsPresent(driver, Elements.UserTextField);
                test.validateElementIsPresent(driver, Elements.PwdTextField);
                test.validateElementIsPresent(driver, Elements.LoginBtn);
                test.validateElementIsPresent(driver, Elements.LoginPage_Register_Link);
                test.validateElementIsPresent(driver, Elements.Login_ForgotPassLink);
                test.validateElementIsPresent(driver, Elements.Login_RememberMe_Btn);
            });

            //Social Registration
            Assert.Multiple(() =>
            {
                test.validateStringIsCorrect(driver, Elements.Social_Header, SocialReg_Header);
                if (Locale.Equals("zh"))
                {
                    test.validateElementIsPresent(driver, Elements.Social_LindkenIn_Cn_Btn);
                    test.validateElementIsNotPresent(driver, Elements.Social_GoogleEx_Btn);
                    test.validateElementIsNotPresent(driver, Elements.Social_Facebook_Btn);
                }
                else
                { 
                    test.validateElementIsPresent(driver, Elements.Social_LinkedIn_Btn);
                    test.validateElementIsPresent(driver, Elements.Social_GoogleEx_Btn);
                    test.validateElementIsPresent(driver, Elements.Social_Facebook_Btn);
                }
            });

        }

        [Test, Category("Registration"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify Forgot Password redirection for EN Locale")]
        [TestCase("zh", TestName = "Verify Forgot Password redirection for CN Locale")]
        [TestCase("jp", TestName = "Verify Forgot Password redirection for JP Locale")]
        public void Registration_VerifyForgotPasswordRedirection(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/index.html");
            action.IClick(driver, Elements.MainNavigation_MyAnalogTab);
            //action.IClick(driver, Elements.MyAnalogTab_LogInToMyAnalog_Btn);
            action.IClick(driver, Elements.Login_ForgotPassLink);
            test.validateStringInstance(driver, driver.Url, "b2c_1a_adi_passwordreset");
        }

        [Test, Category("Registration"), Category("NonCore"), Retry(2)]
        [TestCase("en", "Please enter your email", "Please enter a valid email address", "A user with the specified credential could not be found.", TestName = "Verify field validation in Email Address for EN Locale")]
        [TestCase("zh", "请输入电子邮件地址", "请输入有效的电子邮件地址", "找不到具有指定证书的用户。", TestName = "Verify field validation in Email Address for CN Locale")]
        [TestCase("jp", "メール アドレスを入力してください", "有効な電子メール アドレスを入力してください", "指定の資格情報を持つユーザは見つかりません。", TestName = "Verify field validation in Email Address for JP Locale")]
        public void Registration_VerifyErrorMessageForEmailAdd(string Locale, string ErrorMessage, string ErrorMessage2, string ErrorMessage3)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/index.html");
            action.IClick(driver, Elements.MainNavigation_MyAnalogTab);
            //action.IClick(driver, Elements.MyAnalogTab_LogInToMyAnalog_Btn);
            action.IClick(driver, Elements.LoginBtn);
            test.validateString(driver, ErrorMessage, util.GetText(driver, By.CssSelector("div[class^='error itemLevel']")));
            action.IType(driver, Elements.UserTextField, "aries.sorosoro");
            action.IClick(driver, Elements.LoginBtn);
            test.validateString(driver, ErrorMessage2, util.GetText(driver, By.CssSelector("div[class^='error itemLevel']>p")));
            action.IDeleteValueOnFields(driver, Elements.UserTextField);
            action.IType(driver, Elements.UserTextField, "wild_guess@mailinator.com");
            action.IType(driver, Elements.PwdTextField, "Test_1234");
            action.IClick(driver, Elements.LoginBtn);
            test.validateString(driver, ErrorMessage3, util.GetText(driver, By.CssSelector("div[class^='error pageLevel']>p")));
        }

        [Test, Category("Registration"), Category("NonCore"), Retry(2)]
        [TestCase("en", "Please enter your password", "Invalid email or password", TestName = "Verify Error Message when user entered invalid password for EN Locale")]
        [TestCase("zh", "请输入密码", "无效电子邮箱或密码", TestName = "Verify Error Message when user entered invalid password for CN Locale")]
        [TestCase("jp", "パスワードを入力してください", "入力したEメールアドレスまたはパスワードが無効です", TestName = "Verify Error Message when user entered invalid password for JP Locale")]
        public void Registration_VerifyErrorMessageForInvalidPassword(string Locale, string ErrorMessage, string ErrorMessage2)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/index.html");
            action.IClick(driver, Elements.MainNavigation_MyAnalogTab);
            //action.IClick(driver, Elements.MyAnalogTab_LogInToMyAnalog_Btn);
            action.IType(driver, Elements.UserTextField, "aries.sorosoro@analog.com");
            action.IClick(driver, Elements.LoginBtn);
            test.validateString(driver, ErrorMessage, util.GetText(driver, By.CssSelector("div[class='entry-item']:nth-child(2)>div[class^='error itemLevel']>p")));
            action.IType(driver, Elements.PwdTextField, "Test_12345");
            action.IClick(driver, Elements.LoginBtn);
            test.validateString(driver, ErrorMessage2, util.GetText(driver, By.CssSelector("div[class^='error pageLevel']>p")));
        }
    }
}
    