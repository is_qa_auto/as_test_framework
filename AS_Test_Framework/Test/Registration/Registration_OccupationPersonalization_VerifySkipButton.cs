﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System;
using System.Threading;
using OpenQA.Selenium;

namespace AS_Test_Framework.Registration
{

    [TestFixture]
    public class Registration_OccupationPersonalization_VerifySkipButton : BaseSetUp
    {
        public Registration_OccupationPersonalization_VerifySkipButton() : base() { }
        [Test, Category("Registration"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify Skip button when No selected Occupation for EN Locale")]
        [TestCase("zh", TestName = "Verify Skip button when No selected Occupation for CN Locale")]
        [TestCase("jp", TestName = "Verify Skip button when No selected Occupation for JP Locale")]
        [TestCase("ru", TestName = "Verify Skip button when No selected Occupation for RU Locale")]
        public void Registration_VerifyNextBtnWhenNoOccupationCatSelected(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/app/registration");
            Thread.Sleep(1000);
            action.IClick(driver, Elements.Registration_Next_Btn);
            action.IClick(driver, Elements.Registration_Next_Btn);
            action.IClick(driver, Elements.Registration_Skip_Btn);
            test.validateElementIsPresent(driver, By.CssSelector("div[class='verify email']"));
        }
    }
}
