﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System;
using System.Threading;
using OpenQA.Selenium;
using System.Text.RegularExpressions;

namespace AS_Test_Framework.Registration
{

    [TestFixture]
    public class Registration_MarketCatPersonalization_VerifyComponent : BaseSetUp
    {
        public Registration_MarketCatPersonalization_VerifyComponent() : base() { }

        [Test, Category("Registration"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify Registration's Market Categories Personalization Page component is complete for EN Locale")]
        [TestCase("zh", TestName = "Verify Registration's Market Categories Personalization Page component is complete for CN Locale")]
        [TestCase("jp", TestName = "Verify Registration's Market Categories Personalization Page component is complete for JP Locale")]
        [TestCase("ru", TestName = "Verify Registration's Market Categories Personalization Page component is complete for RU Locale")]
        public void Registration_ProductCategoriesPersonalizationPage_VerifyComponent(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url.Replace("www", "my").Replace("cldnet","corpnt") + Locale + "/app/registration");
            action.IClick(driver, Elements.Registration_Next_Btn);
            test.validateElementIsPresent(driver, By.CssSelector("section[class='registration'] * section[class='markets']>div[class='section header']>h2>span"));
            
        }

        [Test, Category("Registration"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that Market Categories will change its color when selecting or de-selecting for EN Locale")]
        [TestCase("zh", TestName = "Verify that Market Categories will change its color when selecting or de-selecting for CN Locale")]
        [TestCase("jp", TestName = "Verify that Market Categories will change its color when selecting or de-selecting for JP Locale")]
        [TestCase("ru", TestName = "Verify that Market Categories will change its color when selecting or de-selecting for RU Locale")]
        public void Registration_ProductCategoriesPersonalizationPage_VerifySelectingOfProdCat(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/app/registration");
            Thread.Sleep(1000);
            action.IClick(driver, Elements.Registration_Next_Btn);
            action.IClick(driver, By.CssSelector("section[class='registration'] * section[class='markets']>div[class='options grid']>div[class='option']>label"));
            action.IClick(driver, By.CssSelector("section[class='registration'] * section[class='markets']>div[class='options grid']>div[class='option']:nth-child(2)>label"));
            test.validateString(driver, "rgba(0, 159, 189, 1)", util.GetCssValue(driver, By.CssSelector("section[class='registration'] * section[class='markets']>div[class='options grid']>div[class='option']>label"), "background-color"));
            test.validateString(driver, "rgba(0, 159, 189, 1)", util.GetCssValue(driver, By.CssSelector("section[class='registration'] * section[class='markets']>div[class='options grid']>div[class='option']:nth-child(2)>label"), "background-color"));
            action.IClick(driver, By.CssSelector("section[class='registration'] * section[class='markets']>div[class='options grid']>div[class='option']>label"));
            test.validateString(driver, "rgba(255, 255, 255, 1)", util.GetCssValue(driver, By.CssSelector("section[class='registration'] * section[class='markets']>div[class='options grid']>div[class='option']>label"), "background-color"));
        }
    }
}
