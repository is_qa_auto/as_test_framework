﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System;
using System.Threading;
using OpenQA.Selenium;
using System.Text.RegularExpressions;
using System.Linq;

namespace AS_Test_Framework.Registration
{

    [TestFixture]
    public class Registration_VerifyGDPR : BaseSetUp
    {
        public Registration_VerifyGDPR() : base() { }

        [Test, Category("Registration"), Category("NonCore")]
        [TestCase("en", TestName = "Verify GDPR From PDP")]
        public void Registration_VerifyGDPRFromPDP(string Locale)
        { 
            string Email_Address = util.Generate_EmailAddress();

            action.Navigate(driver, Configuration.Env_Url + Locale + "/AD7960");
            Thread.Sleep(1000);
            action.IClick(driver, Elements.PDP_Country_Dropdown);
            action.IClick(driver, By.CssSelector("div[class='search-with-filter pcnpdn-country-filter'] * ul[class='dropdown-menu'] * a[data-id='BE']"));
            action.IClick(driver, Elements.PDP_CheckInventory_Btn);
            action.IClick(driver, By.CssSelector("div[class='products-packaging-grid sb-grid-model table-responsive grid-clone view-inventory'] * tr>td[data-id='AnalogDevices']>a"));
            test.validateStringInstance(driver, util.GetText(driver, Elements.Shopping_Cart_Country_Dropdown).Trim(), "BELGIUM");
            action.IClick(driver, Elements.SC_LoginLink);
            action.IClick(driver, Elements.LoginPage_Register_Link);
            action.IVerifyEmailVerificationCode(driver, Email_Address);
            test.validateElementIsPresent(driver, Elements.Registration_TermsOfUse_ChxBox);
            test.validateElementIsPresent(driver, Elements.Registration_Consent_Comm);

        }

        [Test, Category("Registration"), Category("NonCore")]
        [TestCase("en", TestName = "Verify GDPR From DSP")]
        public void Registration_VerifyGDPRFromDSP(string Locale)
        {
            string Email_Address = util.Generate_EmailAddress();

            action.Navigate(driver, Configuration.Env_Url + Locale + "/ADSWT-CCES");
            Thread.Sleep(1000);
            action.IClick(driver, Elements.PDP_Country_Dropdown);
            action.IClick(driver, By.CssSelector("div[class='search-with-filter pcnpdn-country-filter'] * ul[class='dropdown-menu'] * a[data-id='CA']"));
            action.IClick(driver, Elements.DSP_CheckInventory_Btn);
            action.IClick(driver, By.CssSelector("div[class='products-packaging-grid sb-grid-evaluation table-responsive sample-table grid-clone view-inventory'] * input[class='check-add-to-cart']"));
            action.IClick(driver, Elements.DSP_AddToCart_Btn);
            test.validateStringInstance(driver, util.GetText(driver, Elements.Shopping_Cart_Country_Dropdown).Trim(), "CANADA");
            action.IClick(driver, Elements.SC_LoginLink);
            action.IClick(driver, Elements.LoginPage_Register_Link);
            action.IVerifyEmailVerificationCode(driver, Email_Address);
            test.validateElementIsPresent(driver, Elements.Registration_TermsOfUse_ChxBox);
            test.validateElementIsPresent(driver, Elements.Registration_Consent_Comm);

        }

        [Test, Category("Registration"), Category("NonCore")]
        [TestCase("en", TestName = "Verify GDPR From EvalBoard")]
        public void Registration_VerifyGDPRFromEvalBoard(string Locale)
        {
            string Email_Address = util.Generate_EmailAddress();

            action.Navigate(driver, Configuration.Env_Url + Locale + "/EVAL-AD5313R");
            Thread.Sleep(1000);
            action.IClick(driver, Elements.PDP_Country_Dropdown);
            action.IClick(driver, By.CssSelector("div[class='search-with-filter pcnpdn-country-filter'] * ul[class='dropdown-menu'] * a[data-id='PH']"));
            action.IClick(driver, Elements.DSP_CheckInventory_Btn);
            action.IClick(driver, By.CssSelector("div[class='products-packaging-grid sb-grid-evaluation table-responsive sample-table grid-clone view-inventory'] * input[class='check-add-to-cart']"));
            action.IClick(driver, Elements.DSP_AddToCart_Btn);
            test.validateStringInstance(driver, util.GetText(driver, Elements.Shopping_Cart_Country_Dropdown).Trim(), "PHILIPPINES");
            action.IClick(driver, Elements.SC_LoginLink);
            action.IClick(driver, Elements.LoginPage_Register_Link);
            action.IVerifyEmailVerificationCode(driver, Email_Address);
            test.validateElementIsPresent(driver, Elements.Registration_TermsOfUse_ChxBox);
            test.validateElementIsPresent(driver, Elements.Registration_Consent_Comm);


        }

        [Test, Category("Registration"), Category("NonCore")]
        [TestCase("en", TestName = "Verify GDPR From CFTL")]
        public void Registration_VerifyGDPRFromCFTL(string Locale)
        {
            string Email_Address = util.Generate_EmailAddress();

            action.Navigate(driver, Configuration.Env_Url + Locale + "/CN0288");
            Thread.Sleep(1000);
            action.IClick(driver, Elements.PDP_Country_Dropdown);
            action.IClick(driver, By.CssSelector("div[class='search-with-filter pcnpdn-country-filter'] * ul[class='dropdown-menu'] * a[data-id='EE']"));
            action.IClick(driver, Elements.DSP_CheckInventory_Btn);
            action.IClick(driver, By.CssSelector("div[class='products-packaging-grid sb-grid-evaluation table-responsive sample-table grid-clone view-inventory'] * input[class='check-add-to-cart']"));
            action.IClick(driver, Elements.DSP_AddToCart_Btn);
            test.validateStringInstance(driver, util.GetText(driver, Elements.Shopping_Cart_Country_Dropdown).Trim(), "ESTONIA");
            action.IClick(driver, Elements.SC_LoginLink);
            action.IClick(driver, Elements.LoginPage_Register_Link);
            action.IVerifyEmailVerificationCode(driver, Email_Address);
            test.validateElementIsPresent(driver, Elements.Registration_TermsOfUse_ChxBox);
            test.validateElementIsPresent(driver, Elements.Registration_Consent_Comm);
        }
    }
}
    