﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System;
using System.Threading;
using OpenQA.Selenium;
using System.Text.RegularExpressions;
using System.Linq;

namespace AS_Test_Framework.Registration
{

    [TestFixture]
    public class Registration_VerifyLoginViaSocialFed : BaseSetUp
    {
        public Registration_VerifyLoginViaSocialFed() : base() { }

        //[Test, Category("Registration"), Category("NonCore"), Retry(2)]
        //[TestCase("en", TestName = "Verify user can login via LinkedIn for EN Locale")]
        //[TestCase("cn", TestName = "Verify user can login via LinkedIn for CN Locale")]
        //[TestCase("jp", TestName = "Verify user can login via LinkedIn for JP Locale")]
        //public void Registration_VerifyLoginViaLinkedIn(string Locale) {

        //    action.Navigate(driver, Configuration.Env_Url + Locale + "/index.html");
        //    action.IClick(driver, Elements.MainNavigation_MyAnalogTab);
        //    action.IClick(driver, Elements.MyAnalogTab_LogInToMyAnalog_Btn);
        //    if (!Locale.Equals("cn"))
        //    {
        //        action.IClick(driver, Elements.Social_LinkedIn_Btn);
        //    }
        //    else {
        //        action.IClick(driver, Elements.Social_LindkenIn_Cn_Btn);
        //    }
        //    action.ILoginVia_LinkedIn(driver, "ISSQA.Auto0413@gmail.com", "Test_1234");
        //    Thread.Sleep(2000);
        //    test.validateElementIsPresent(driver, By.CssSelector("a[data-qa-element='link_myAnalog'] * img"));
        //    if (Locale.Equals("cn"))
        //    {
        //        test.validateScreenByUrl(driver, Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + "zh/app");
        //    }
        //    else
        //    { 
        //        test.validateScreenByUrl(driver, Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/app");
        //    }
        //}

        //[Test, Category("Registration"), Category("NonCore"), Retry(2)]
        //[TestCase("en", TestName = "Verify user can login via Gmail for EN Locale")]
        //[TestCase("jp", TestName = "Verify user can login via Gmail for JP Locale")]
        //public void Registration_VerifyLoginViaGmail(string Locale)
        //{

        //    action.Navigate(driver, Configuration.Env_Url + Locale + "/index.html");
        //    action.IClick(driver, Elements.MainNavigation_MyAnalogTab);
        //    action.IClick(driver, Elements.MyAnalogTab_LogInToMyAnalog_Btn);
        //    action.IClick(driver, Elements.Social_GoogleEx_Btn);
        //    action.ILoginVia_Google(driver, "ISSQA.Auto0413@gmail.com", "Test_1234");
        //    Thread.Sleep(2000);
        //    test.validateElementIsPresent(driver, By.CssSelector("a[data-qa-element='link_myAnalog'] * img"));
        //    test.validateScreenByUrl(driver, Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/app");
        //}

        //[Test, Category("Registration"), Category("NonCore"), Retry(2)]
        //[TestCase("en", TestName = "Verify user can login via FB for EN Locale")]
        //[TestCase("jp", TestName = "Verify user can login via FB for JP Locale")]
        //public void Registration_VerifyLoginViaFB(string Locale)
        //{

        //    action.Navigate(driver, Configuration.Env_Url + Locale + "/index.html");
        //    action.IClick(driver, Elements.MainNavigation_MyAnalogTab);
        //    action.IClick(driver, Elements.MyAnalogTab_LogInToMyAnalog_Btn);
        //    action.IClick(driver, Elements.Social_Facebook_Btn);
        //    action.ILoginVia_FB(driver, "ISSQA.Auto0413@gmail.com", "Test_1234");
        //    Thread.Sleep(2000);
        //    test.validateElementIsPresent(driver, By.CssSelector("a[data-qa-element='link_myAnalog'] * img"));
        //    test.validateScreenByUrl(driver, Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/app");
        //}
    }
}
    