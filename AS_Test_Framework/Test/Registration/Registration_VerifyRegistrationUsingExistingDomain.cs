﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System;
using System.Threading;
using OpenQA.Selenium;
using System.Text.RegularExpressions;

namespace AS_Test_Framework.Registration
{

    [TestFixture]
    public class Registration_VerifyRegistrationUsingExistingDomain : BaseSetUp
    {
        public Registration_VerifyRegistrationUsingExistingDomain() : base() { }

        [Test, Category("Registration"), Category("NonCore"), Retry(2)]
        [TestCase("en", "The username/email you selected is already registered. Please choose a different one.×", TestName = "Verify Registration using Existing adi account for EN Locale")]
        [TestCase("zh", "您所选的用户名/电子邮件已经注册。请选择其他用户名/电子邮件。×", TestName = "Verify Registration using Existing adi account for CN Locale")]
        [TestCase("jp", "The username/email you selected is already registered. Please choose a different one.×", TestName = "Verify Registration using Existing adi account for JP Locale")]
        [TestCase("ru", "Имя пользователя/email, которое вы указали, уже зарегистрировано. Пожалуйста, выберите другое.×", TestName = "Verify Registration using Existing adi account for RU Locale")]
        public void Registration_VerifyErrorMessage(string Locale, string expectedMessage)
        {
            string Email = "aries.sorosoro@analog.com";
            action.Navigate(driver, Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/app/registration");
            Thread.Sleep(1000);
            action.IClick(driver, Elements.Registration_Skip_Btn);
            action.IType(driver, Elements.Registration_VerifyEmail_TxtField, Email);
            action.IClick(driver, Elements.Registration_SendVerificationCode_Btn);
            test.validateStringInstance (driver, util.GetText(driver, Elements.Registration_RegisterExisting_Message), expectedMessage);
        }

    }
}
