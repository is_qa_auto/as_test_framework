﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System;
using System.Threading;
using OpenQA.Selenium;

namespace AS_Test_Framework.Registration
{

    [TestFixture]
    public class Registration_ProdCatPersonalization_VerifyLanguageDropdown : BaseSetUp
    {
        public Registration_ProdCatPersonalization_VerifyLanguageDropdown() : base() { }

        [Test, Category("Registration"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify Language Dropdown in Registration's Product Categories Personalization Page")]
        public void Registration_ProductCategoriesPersonalizationPage_VerifyLanguageDropdown(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/app/registration");
            Assert.Multiple(() =>
            {
                test.validateElementIsPresent(driver, Elements.Registration_Language_Dropdown);
                test.validateStringIsCorrect(driver, By.CssSelector("div[class='change language'] * button>span"), "English");
            });
        }

        [Test, Category("Registration"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify user can change language to CN using the language dropdown in Registration Page")]
        public void Registration_ProductCategoriesPersonalizationPage_VerifyUserCanSelectCNLanguage(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/app/registration");
            action.IClick(driver, Elements.Registration_Language_Dropdown);
            action.IClick(driver, By.CssSelector("div[class='change language']>div>div[class='options open']>div[data-label='简体中文']"));
            Assert.Multiple(() =>
            {
                test.validateScreenByUrl(driver, Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + "zh/app/registration");
                test.validateStringIsCorrect(driver, By.CssSelector("div[class='change language'] * button>span"), "简体中文");
            });
        }

        [Test, Category("Registration"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify user can change language to JP using the language dropdown in Registration Page")]
        public void Registration_ProductCategoriesPersonalizationPage_VerifyUserCanSelectJPLanguage(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/app/registration");
            action.IClick(driver, Elements.Registration_Language_Dropdown);
            action.IClick(driver, By.CssSelector("div[class='change language']>div>div[class='options open']>div[data-label='日本語']"));
            Assert.Multiple(() =>
            {
                test.validateScreenByUrl(driver, Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + "jp/app/registration");
                test.validateStringIsCorrect(driver, By.CssSelector("div[class='change language'] * button>span"), "日本語");
            });
        }

        [Test, Category("Registration"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify user can change language to RU using the language dropdown in Registration Page")]
        public void Registration_ProductCategoriesPersonalizationPage_VerifyUserCanSelectRULanguage(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/app/registration");
            action.IClick(driver, Elements.Registration_Language_Dropdown);
            action.IClick(driver, By.CssSelector("div[class='change language']>div>div[class='options open']>div[data-label='Руccкий']"));
            Assert.Multiple(() =>
            {
                test.validateScreenByUrl(driver, Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + "ru/app/registration");
                test.validateStringIsCorrect(driver, By.CssSelector("div[class='change language'] * button>span"), "Руccкий");
            });
        }
    }
}
