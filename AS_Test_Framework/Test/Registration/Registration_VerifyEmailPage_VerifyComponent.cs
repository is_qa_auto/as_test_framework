﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System;
using System.Threading;
using OpenQA.Selenium;
using System.Text.RegularExpressions;

namespace AS_Test_Framework.Registration
{

    [TestFixture]
    public class Registration_VerifyEmailPage_VerifyComponent : BaseSetUp
    {
        public Registration_VerifyEmailPage_VerifyComponent() : base() { }

        [Test, Category("Registration"), Category("NonCore"), Retry(2)]
        [TestCase("en", "Please verify your email", "You will receive a verification code email within a minute", TestName = "Verify \"Verify Email\" page component for EN Locale")]
        [TestCase("zh", "请确认您的邮箱", "您将在一分钟之内收到确认代码", TestName = "Verify \"Verify Email\" page component for CN Locale")]
        [TestCase("jp", "Eメールアドレスを入力してください。", "認証用コードが記載されたメールが、数分のうちに届きます。", TestName = "Verify \"Verify Email\" page component for JP Locale")]
        [TestCase("ru", "Пожалуйста, подтвердите ваш адрес электронной почты", "Вы получите код подтверждения на ящик электронной почты в течение минуты", TestName = "Verify \"Verify Email\" page component for RU Locale")]
        public void Registration_ProductCategoriesPersonalizationPage_VerifyComponent(string Locale, string HeaderLabel, string Description)
        {
            action.Navigate(driver, Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/app/registration");
            Thread.Sleep(1000);
            action.IClick(driver, Elements.Registration_Skip_Btn);
            Assert.Multiple(() =>
            {
                test.validateStringIsCorrect(driver, By.CssSelector("div[class='verify email'] * h2>span"), HeaderLabel);
                test.validateStringIsCorrect(driver, By.CssSelector("div[class='verify email'] * h2>small"), Description);
                test.validateElementIsPresent(driver, Elements.Registration_VerifyEmail_TxtField);
                test.validateElementIsPresent(driver, By.CssSelector("div[class='verify email']>button[class='ma btn btn-primary disabled']"));
                test.validateElementIsPresent(driver, Elements.Registration_VerifyEmail_Login_Link);
            });
            //Social Registration
            Assert.Multiple(() =>
            {
                test.validateStringIsCorrect(driver, Elements.Social_Reg_Header, "Or register with:");
                if (Locale.Equals("zh"))
                {
                    test.validateElementIsPresent(driver, Elements.Social_Reg_LinkedIn_Btn);
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.Social_Reg_LinkedIn_Btn);
                    test.validateElementIsPresent(driver, Elements.Social_Reg_GoogleEx_Btn);
                    test.validateElementIsPresent(driver, Elements.Social_Reg_Facebook_Btn);
                }
            });
        }

        [Test, Category("Registration"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify field validation when entering valid and invalid email address in verification page for EN Locale")]
        [TestCase("zh", TestName = "Verify field validation when entering valid and invalid email address in verification page for CN Locale")]
        [TestCase("jp", TestName = "Verify field validation when entering valid and invalid email address in verification page for JP Locale")]
        [TestCase("ru", TestName = "Verify field validation when entering valid and invalid email address in verification page for RU Locale")]
        public void Registration_VerifyFieldValidation(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/app/registration");
            Thread.Sleep(1000);
            action.IClick(driver, Elements.Registration_Skip_Btn);
            action.IType(driver, Elements.Registration_VerifyEmail_TxtField, "aries.sorosoro@analog.com");
            test.validateElementIsPresent(driver, Elements.Registration_SendVerificationCode_Btn);
            driver.Navigate().Refresh();
            action.IClick(driver, Elements.Registration_Skip_Btn);
            action.IType(driver, Elements.Registration_VerifyEmail_TxtField, "aries.sorosoro");
            test.validateElementIsPresent(driver, By.CssSelector("div[class='email input has value error']>input[name='verify-email']"));
        }


        [Test, Category("Registration"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify login link from validation page for EN Locale")]
        [TestCase("zh", TestName = "Verify login link from validation page for CN Locale")]
        [TestCase("jp", TestName = "Verify login link from validation page for JP Locale")]
        [TestCase("ru", TestName = "Verify login link from validation page for RU Locale")]
        public void Registration_VerifyLogInLink(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/app/registration");
            Thread.Sleep(1000);
            action.IClick(driver, Elements.Registration_Skip_Btn);
            action.IClick(driver, Elements.Registration_VerifyEmail_Login_Link);
            test.validateStringInstance(driver, driver.Url, "b2clogin.com");
        }
    }
}
