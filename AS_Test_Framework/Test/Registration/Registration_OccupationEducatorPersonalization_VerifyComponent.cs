﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System;
using System.Threading;
using OpenQA.Selenium;
using System.Text.RegularExpressions;

namespace AS_Test_Framework.Registration
{

    [TestFixture]
    public class Registration_OccupationEducatorPersonalization_VerifyComponent : BaseSetUp
    {
        public Registration_OccupationEducatorPersonalization_VerifyComponent() : base() { }
        [Test, Category("Registration"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify when clicking Educator options occupation box for EN Locale")]
        [TestCase("zh", TestName = "Verify when clicking Educator options occupation box for CN Locale")]
        [TestCase("jp", TestName = "Verify when clicking Educator options occupation box for JP Locale")]
        [TestCase("ru", TestName = "Verify when clicking Educator options occupation box for RU Locale")]
        public void Registration_OccupationPersonalizationPage_VerifyNextBtnWhenOccupationSelectedIsStudent(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/app/registration");
            Thread.Sleep(1000);
            action.IClick(driver, Elements.Registration_Next_Btn);
            action.IClick(driver, Elements.Registration_Next_Btn);
            action.IClick(driver, By.CssSelector("section[class='registration'] section[class='occupations']>div[class='large options grid']>div[class='option']:nth-child(3)>label"));

            Random r = new Random();
            int Selected_Occupation = r.Next(1, util.GetCount(driver, By.CssSelector("section[class='registration'] div[class='step flies in from right']>section[class='occupations']>div[class='large options grid']>div[class='option']")));
            string Get_Occupation_Label = util.GetText(driver, By.CssSelector("section[class='registration'] div[class='step flies in from right']>section[class='occupations']>div[class='large options grid']>div[class='option']:nth-child(" + Selected_Occupation  + ")>label>span" ));
            action.IClick(driver, By.CssSelector("section[class='registration'] div[class='step flies in from right']>section[class='occupations']>div[class='large options grid']>div[class='option']:nth-child(" + Selected_Occupation + ")>label"));
            test.validateElementIsPresent(driver, By.CssSelector("div[class='verify email']"));

        }
    }
}
