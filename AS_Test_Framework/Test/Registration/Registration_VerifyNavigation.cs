﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System;
using System.Threading;

namespace AS_Test_Framework.Registration
{

    [TestFixture]
    public class Registration_VerifyNavigation : BaseSetUp
    {
        public Registration_VerifyNavigation() : base() { }

        /****Remove Due to AL-18380****/
        //[Test, Category("Registration"), Category("NonCore"), Retry(2)]
        //[TestCase("en", TestName = "Verify Registration when Navigating via myAnalog Masthead for EN Locale")]
        //[TestCase("ru", TestName = "Verify Registration when Navigating via myAnalog Masthead for RU Locale")]
        //public void Registration_VerifyRegistrationViaMyAnalogTab(string Locale)
        //{
        //    action.Navigate(driver, Configuration.Env_Url + Locale + "/index.html");
        //    action.IClick(driver, Elements.MainNavigation_MyAnalogTab);
        //    //action.IClick(driver, Elements.MyAnalogTab_GetStarted_Btn);
        //    Assert.Multiple(() =>
        //    {
        //        test.validateScreenByUrl(driver, Configuration.Env_Url.Replace("www", "my").Replace("cldnet","corpnt") + Locale + "/app/registration");
        //        test.validateElementIsPresent(driver, Elements.Registration_ProdCat_Section);
        //    });

        //    action.Navigate(driver, Configuration.Env_Url + Locale + "/index.html");
        //    action.IClick(driver, Elements.MainNavigation_MyAnalogTab);
        //    action.IClick(driver, Elements.MyAnalogTab_RegisterNow_Link);
        //    Assert.Multiple(() =>
        //    {
        //        test.validateScreenByUrl(driver, Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/app/registration");
        //        test.validateElementIsPresent(driver, Elements.Registration_ProdCat_Section);
        //    });
        //}

        /****Remove Due to AL-18380****/
        //[Test, Category("Registration"), Category("NonCore"), Retry(2)]
        //[TestCase("en", TestName = "Verify Registration when Navigating via myAnalog widget")]
        //public void Registration_VerifyRegistrationViaMyAnalogWidget(string Locale)
        //{
        //    action.Navigate(driver, Configuration.Env_Url + Locale + "/ad7960");
        //    action.IClick(driver, Elements.PDP_MyAnalogWidget_Btn);
        //    action.IClick(driver, Elements.MyAnalogWidget_RegisterNow_Link);
        //    Assert.Multiple(() =>
        //    {
        //        Thread.Sleep(2000);
        //        test.validateScreenByUrl(driver, Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/app/registration");
        //        test.validateElementIsPresent(driver, Elements.Registration_ProdCat_Section);
        //    });

        //}

        [Test, Category("Registration"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify Registration when Navigating via Log-in page")]
        public void Registration_VerifyRegistrationWhenNavigatingViaLoginPage(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/index.html");
            action.IClick(driver, Elements.MainNavigation_MyAnalogTab);
            //action.IClick(driver, Elements.MyAnalogTab_LogInToMyAnalog_Btn);
            action.IClick(driver, Elements.LoginPage_Register_Link);
            Thread.Sleep(1000);
            Assert.Multiple(() =>
            {
                test.validateStringInstance(driver, driver.Url, Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/app/registration");
                test.validateElementIsPresent(driver, Elements.Registration_ProdCat_Section);
            });

        }

        [Test, Category("Registration"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify behaviour when logging in via Shopping cart page")]
        public void Registration_VerifyBehaviourUponLogginWhenLoggingIntheShoppingCartPage(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx");
            Thread.Sleep(2000);
            action.IClick(driver, Elements.SC_LoginLink);
            action.ILogin(driver, "aries.sorosoro@analog.com", "Test_1234");
            Thread.Sleep(2000);
            test.validateScreenByUrl(driver, Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx?locale=en");
        }
    }
}
