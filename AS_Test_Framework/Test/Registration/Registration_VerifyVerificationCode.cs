﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System;
using System.Threading;
using OpenQA.Selenium;
using System.Text.RegularExpressions;
using System.Linq;

namespace AS_Test_Framework.Registration
{
    /***test onl;y***/
    [TestFixture]
    public class Registration_VerifyVerificationCode : BaseSetUp
    {
        public Registration_VerifyVerificationCode() : base() { }

        [Test, Category("Registration"), Category("NonCore")]
        [TestCase("en", "Authorization code is invalid×", TestName = "Verify when entering invalid verification code for EN Locale")]
        [TestCase("zh", "授权码无效×", TestName = "Verify when entering invalid verification code for CN Locale")]
        [TestCase("jp", "入力いただいた認証用コードは無効です。×", TestName = "Verify when entering invalid verification code for JP Locale")]
        [TestCase("ru", "Неверный код авторизации×", TestName = "Verify when entering invalid verification code for RU Locale")]
        public void Registration_VerifyCodeValidation(string Locale, string ErrorMessage)
        {
            string Email_Address = util.Generate_EmailAddress();

            action.Navigate(driver, Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/app/registration");
            Thread.Sleep(1000);
            action.IClick(driver, Elements.Registration_Skip_Btn);
            action.IType(driver, Elements.Registration_VerifyEmail_TxtField, Email_Address);
            action.IClick(driver, Elements.Registration_SendVerificationCode_Btn);
            Thread.Sleep(3000);
            action.IClick(driver, Elements.Registration_SendVerificationCode_Btn);
            Assert.Multiple(() =>
            {
                test.validateElementIsNotPresent(driver, Elements.Registration_RegisterExisting_Message);
                action.IType(driver, Elements.Registration_EnterCode_TextField, "123456");
                action.IClick(driver, Elements.Registration_SendVerificationCode_Btn);
                test.validateStringInstance(driver, util.GetText(driver, Elements.Registration_RegisterExisting_Message), ErrorMessage);
                action.IDeleteValueOnFields(driver, Elements.Registration_EnterCode_TextField);
                action.IClick(driver, Elements.Registration_VerifyEmail_SendNewCode_Link);
                ((IJavaScriptExecutor)driver).ExecuteScript("window.open()");
                Thread.Sleep(1000);
                driver.SwitchTo().Window(driver.WindowHandles.Last());
                action.ICheckMailinatorEmail(driver, Email_Address);
                test.validateString(driver, "2", util.GetCount(driver, By.CssSelector("table[class='table-striped jambo_table']>tbody>tr")).ToString());
                action.IClick(driver, By.CssSelector("table[class='table-striped jambo_table'] * tr>td:nth-child(2)"));
                driver.SwitchTo().Frame(driver.FindElement(By.CssSelector("iframe[id='html_msg_body']")));
                string VerificationCode = util.GetText(driver, By.CssSelector("td>strong")).Substring(util.GetText(driver, By.CssSelector("td>strong")).Length - 6).Trim();
                driver.SwitchTo().Window(driver.WindowHandles.First());
                action.IType(driver, Elements.Registration_EnterCode_TextField, VerificationCode);
                action.IClick(driver, Elements.Registration_SendVerificationCode_Btn);
                Thread.Sleep(3000);
                test.validateElementIsPresent(driver, By.CssSelector("section[class='register']"));
                test.validateString(driver, Email_Address, util.ReturnAttribute(driver, Elements.Registration_EmailAddress_TxtField, "value"));
                test.validateElementIsNotPresent(driver, Elements.Registration_RegisterExisting_Message);
            });
        }
    }
}
    