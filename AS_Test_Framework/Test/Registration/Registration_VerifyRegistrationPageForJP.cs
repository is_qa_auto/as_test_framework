﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System;
using System.Threading;
using OpenQA.Selenium;
using System.Text.RegularExpressions;
using System.Linq;

namespace AS_Test_Framework.Registration
{

    [TestFixture]
    public class Registration_VerifyRegistrationPageForJP : BaseSetUp
    {
        public Registration_VerifyRegistrationPageForJP() : base() { }

        [Test, Category("Registration"), Category("NonCore")]
        [TestCase("jp", TestName = "Verify Registration in JP Locale")]
        public void Registration_VerifyRegistrationPageTitle(string Locale)
        {
            string MoreThan255Char = "This is just a test This is just a test This is just a test This is just a test This is just a test This is just a test This is just a test This is just a test This is just a test This is just a test This is just a test This is just a test This is just a test";
            string ValidInput = "Auto123!";
            string Email_Address = util.Generate_EmailAddress();
            string ForeignChar = "立即";
            string password_input = "Test123testtesttest";

            action.Navigate(driver, Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/app/registration");
            Thread.Sleep(1000);
            action.IVerifyEmailVerificationCode(driver, Email_Address);


            Assert.Multiple(() =>
            {
                test.validateElementIsPresent(driver, By.CssSelector("button[class='ma btn btn-primary disabled']"));
                test.validateString(driver, "登録して始めよう！", util.GetText(driver, By.CssSelector("section[class='register'] * h2>span")).Replace("'", ""));
                test.validateString(driver, "以下の項目をすべて入力してください。アカウントを有効にするためのメールが、数分で届きます。", util.GetText(driver, By.CssSelector("section[class='register'] * h2>small")));

                /****First Name****/
                test.validateElementIsPresent(driver, Elements.Registration_FirstName_TxtField);
                test.validateString(driver, "名", util.ReturnAttribute(driver, Elements.Registration_FirstName_TxtField, "placeholder"));
                action.IType(driver, Elements.Registration_FirstName_TxtField, ForeignChar);
                test.validateElementIsNotPresent(driver, By.CssSelector("div[class='text input error required']>input[name='first']"));
                action.IDeleteValueOnFieldUsingBackSpace(driver, Elements.Registration_FirstName_TxtField, ForeignChar);
                test.validateElementIsPresent(driver, By.CssSelector("div[class='text input error required']>input[name='first']"));
                action.IDeleteValueOnFields(driver, Elements.Registration_FirstName_TxtField);
                action.IType(driver, Elements.Registration_FirstName_TxtField, MoreThan255Char);
                test.validateString(driver, "255", util.ReturnAttribute(driver, Elements.Registration_FirstName_TxtField, "value").Length.ToString());
                action.IDeleteValueOnFields(driver, Elements.Registration_FirstName_TxtField);
                action.IType(driver, Elements.Registration_FirstName_TxtField, ValidInput);
                test.validateElementIsNotPresent(driver, By.CssSelector("div[class='text input error required']>input[name='first']"));


                ///****First Name Pronounce****/
                //test.validateElementIsPresent(driver, By.CssSelector("input[name='firstPronounce']"));
                //test.validateString(driver, "メイ", util.ReturnAttribute(driver, By.CssSelector("input[name='firstPronounce']"), "placeholder"));
                //action.IType(driver, By.CssSelector("input[name='firstPronounce']"), ForeignChar);
                //test.validateElementIsNotPresent(driver, By.CssSelector("div[class='text input error required']>input[name='firstPronounce']"));
                //action.IDeleteValueOnFieldUsingBackSpace(driver, By.CssSelector("input[name='firstPronounce']"), ForeignChar);
                //test.validateElementIsPresent(driver, By.CssSelector("div[class='text input error required']>input[name='firstPronounce']"));
                //action.IDeleteValueOnFields(driver, By.CssSelector("input[name='firstPronounce']"));
                //action.IType(driver, By.CssSelector("input[name='firstPronounce']"), MoreThan255Char);
                //test.validateString(driver, "255", util.ReturnAttribute(driver, By.CssSelector("input[name='firstPronounce']"), "value").Length.ToString());
                //action.IDeleteValueOnFields(driver, By.CssSelector("input[name='firstPronounce']"));
                //action.IType(driver, By.CssSelector("input[name='firstPronounce']"), ValidInput);
                //test.validateElementIsNotPresent(driver, By.CssSelector("div[class='text input error required']>input[name='firstPronounce']"));


                /****Last Name****/
                test.validateElementIsPresent(driver, Elements.Registration_LastName_TxtField);
                test.validateString(driver, "姓", util.ReturnAttribute(driver, Elements.Registration_LastName_TxtField, "placeholder"));
                action.IType(driver, Elements.Registration_LastName_TxtField, ForeignChar);
                action.IType(driver, Elements.Registration_LastName_TxtField, Keys.Tab);
                test.validateElementIsNotPresent(driver, By.CssSelector("div[class='text input error required']>input[name='last']"));
                action.IDeleteValueOnFieldUsingBackSpace(driver, Elements.Registration_LastName_TxtField, ForeignChar);
                action.IType(driver, Elements.Registration_LastName_TxtField, Keys.Tab);
                test.validateElementIsPresent(driver, By.CssSelector("div[class='text input error required']>input[name='last']"));
                action.IDeleteValueOnFields(driver, Elements.Registration_LastName_TxtField);
                action.IType(driver, Elements.Registration_LastName_TxtField, MoreThan255Char);
                test.validateString(driver, "255", util.ReturnAttribute(driver, Elements.Registration_LastName_TxtField, "value").Length.ToString());
                action.IDeleteValueOnFields(driver, Elements.Registration_LastName_TxtField);
                action.IType(driver, Elements.Registration_LastName_TxtField, ValidInput);
                action.IType(driver, Elements.Registration_LastName_TxtField, Keys.Tab);
                test.validateElementIsNotPresent(driver, By.CssSelector("div[class='text input error required']>input[name='last']"));

                /****Last Name Pronounce***
                test.validateElementIsPresent(driver, By.CssSelector("input[name='lastPronounce']"));
                test.validateString(driver, "セイ", util.ReturnAttribute(driver, By.CssSelector("input[name='lastPronounce']"), "placeholder"));
                action.IType(driver, By.CssSelector("input[name='lastPronounce']"), ForeignChar);
                action.IType(driver, By.CssSelector("input[name='lastPronounce']"), Keys.Tab);
                test.validateElementIsNotPresent(driver, By.CssSelector("div[class='text input error required']>input[name='lastPronounce']"));
                action.IDeleteValueOnFieldUsingBackSpace(driver, By.CssSelector("input[name='lastPronounce']"), ForeignChar);
                action.IType(driver, By.CssSelector("input[name='lastPronounce']"), Keys.Tab);
                test.validateElementIsPresent(driver, By.CssSelector("div[class='text input error required']>input[name='lastPronounce']"));
                action.IDeleteValueOnFields(driver, By.CssSelector("input[name='lastPronounce']"));
                action.IType(driver, By.CssSelector("input[name='lastPronounce']"), MoreThan255Char);
                test.validateString(driver, "255", util.ReturnAttribute(driver, By.CssSelector("input[name='lastPronounce']"), "value").Length.ToString());
                action.IDeleteValueOnFields(driver, By.CssSelector("input[name='lastPronounce']"));
                action.IType(driver, By.CssSelector("input[name='lastPronounce']"), ValidInput);
                action.IType(driver, By.CssSelector("input[name='lastPronounce']"), Keys.Tab);
                test.validateElementIsNotPresent(driver, By.CssSelector("div[class='text input error required']>input[name='lastPronounce']"));
                */
                /***Email Field***/
                test.validateElementIsNotEnabled(driver, Elements.Registration_EmailAddress_TxtField);

                /****Password****/
                test.validateElementIsPresent(driver, Elements.Registration_Password_TxtField);
                test.validateString(driver, "パスワード", util.ReturnAttribute(driver, Elements.Registration_Password_TxtField, "placeholder"));
                action.IClick(driver, Elements.Registration_Password_TxtField);
                test.validateElementIsPresent(driver, By.CssSelector("div[class='requirements focused']>span"));
                action.IType(driver, Elements.Registration_Password_TxtField, password_input);
                test.validateString(driver, "16", util.ReturnAttribute(driver, Elements.Registration_Password_TxtField, "value").Length.ToString());
                action.IDeleteValueOnFieldUsingBackSpace(driver, Elements.Registration_Password_TxtField, password_input);
                test.validateElementIsPresent(driver, By.CssSelector("div[class='password input error required invalid icon']>input[name='password']"));
                action.IType(driver, Elements.Registration_Password_TxtField, "aa");
                test.validateElementIsPresent(driver, By.CssSelector("div[class='password input has value error required invalid icon']>input[name='password']"));
                action.IDeleteValueOnFields(driver, Elements.Registration_Password_TxtField);
                action.IType(driver, Elements.Registration_Password_TxtField, "testtest");
                test.validateElementIsPresent(driver, By.CssSelector("div[class='password input has value required invalid icon']>input[name='password']"));
                action.IDeleteValueOnFields(driver, Elements.Registration_Password_TxtField);
                action.IType(driver, Elements.Registration_Password_TxtField, "TESTTEST");
                test.validateElementIsPresent(driver, By.CssSelector("div[class='password input has value required invalid icon']>input[name='password']"));
                action.IDeleteValueOnFields(driver, Elements.Registration_Password_TxtField);
                action.IType(driver, Elements.Registration_Password_TxtField, "Test_1234");
            
                /****Confirm Password****/
                test.validateElementIsPresent(driver, Elements.Registration_ConfirmPassword_TxtField);
                test.validateString(driver, "パスワード（確認）", util.ReturnAttribute(driver, Elements.Registration_ConfirmPassword_TxtField, "placeholder"));

                action.IType(driver, Elements.Registration_ConfirmPassword_TxtField, "aa");
                test.validateElementIsPresent(driver, By.CssSelector("div[class='password input has value error required invalid icon']>input[name='confirmPassword']"));

                action.IDeleteValueOnFieldUsingBackSpace(driver, Elements.Registration_ConfirmPassword_TxtField, password_input);
                test.validateElementIsPresent(driver, By.CssSelector("div[class='password input error required']>input[name='confirmPassword']"));

                action.IDeleteValueOnFields(driver, Elements.Registration_ConfirmPassword_TxtField);
                action.IType(driver, Elements.Registration_ConfirmPassword_TxtField, "Test_1234");
            
                /****Country Dropdown****/
                test.validateElementIsPresent(driver, Elements.Registration_Country_Dropdown);
                test.validateString(driver, "JAPAN", util.GetText(driver, By.CssSelector("div[class='select required has value']>button>span")));
                action.IClick(driver, Elements.Registration_Country_Dropdown);
                test.validateElementIsPresent(driver, By.CssSelector("div[class='select required has value']>div[class='options open']"));
                string[] country_list = new string[10];
                for (int x = 2, y = 0; y <= 9; x++, y++)
                {
                    country_list[y] = util.GetText(driver, By.CssSelector("div[class='select required has value']>div[class='options open']>div:nth-child(" + x + ")"));
                }
                util.IsAscendingOrder(country_list);
                test.validateStringIfInAscenading(country_list);
                action.IClick(driver, By.CssSelector("div[class='select required has value']>div[class='options open']>div[data-label='JAPAN']"));

                /****Telephone****/
                test.validateElementIsPresent(driver, Elements.Telephone_TextField);
                test.validateString(driver, "電話番号", util.ReturnAttribute(driver, Elements.Telephone_TextField, "placeholder"));
                action.IType(driver, Elements.Telephone_TextField, ForeignChar);
                test.validateElementIsNotPresent(driver, By.CssSelector("div[class='text input error required']>input[name='phoneNumber']"));
                action.IDeleteValueOnFieldUsingBackSpace(driver, Elements.Telephone_TextField, ForeignChar);
                test.validateElementIsPresent(driver, By.CssSelector("div[class='text input error required']>input[name='phoneNumber']"));
                action.IType(driver, Elements.Telephone_TextField, "12345679");
                test.validateElementIsNotPresent(driver, By.CssSelector("div[class='text input error required']>input[name='phoneNumber']"));

                /****Company/School Name****/
                test.validateElementIsPresent(driver, Elements.Registration_Company_RadioButton);
                test.validateElementIsPresent(driver, Elements.Registration_School_RadioButton);

                test.validateElementIsPresent(driver, Elements.Registration_CompName_TxtField);
                test.validateString(driver, "会社", util.ReturnAttribute(driver, Elements.Registration_CompName_TxtField, "placeholder"));

                action.IType(driver, Elements.Registration_CompName_TxtField, "Analog");
                action.IDeleteValueOnFieldUsingBackSpace(driver, Elements.Registration_CompName_TxtField, util.ReturnAttribute(driver, Elements.Registration_CompName_TxtField, "value"));
                Thread.Sleep(1500);
                test.validateElementIsPresent(driver, By.CssSelector("div[class='text input error required']>input[name='companyName']"));

                action.IClick(driver, Elements.Registration_School_RadioButton);
                test.validateElementIsPresent(driver, Elements.Registration_School_TextField);
                test.validateString(driver, "学校", util.ReturnAttribute(driver, Elements.Registration_School_TextField, "placeholder"));

                action.IClick(driver, Elements.Registration_Company_RadioButton);
                action.IType(driver, Elements.Registration_CompName_TxtField, "Analog");

                /****Company Name Pronounce***
                test.validateElementIsPresent(driver, By.CssSelector("input[name='companyNamePronounce']"));
                test.validateString(driver, "カイシャ・ダンタイメイ", util.ReturnAttribute(driver, By.CssSelector("input[name='companyNamePronounce']"), "placeholder"));
                action.IType(driver, By.CssSelector("input[name='companyNamePronounce']"), ForeignChar);
                test.validateElementIsNotPresent(driver, By.CssSelector("div[class='text input error required']>input[name='companyNamePronounce']"));
                action.IDeleteValueOnFieldUsingBackSpace(driver, By.CssSelector("input[name='companyNamePronounce']"), ForeignChar);
                test.validateElementIsPresent(driver, By.CssSelector("div[class='text input error required']>input[name='companyNamePronounce']"));
                action.IDeleteValueOnFields(driver, By.CssSelector("input[name='companyNamePronounce']"));
                action.IType(driver, By.CssSelector("input[name='companyNamePronounce']"), ValidInput);
                test.validateElementIsNotPresent(driver, By.CssSelector("div[class='text input error required']>input[name='companyNamePronounce']"));
                */                
                /****Division****/
                test.validateElementIsPresent(driver, By.CssSelector("input[name='division']"));
                test.validateString(driver, "部署名", util.ReturnAttribute(driver, By.CssSelector("input[name='division']"), "placeholder"));
                action.IType(driver, By.CssSelector("input[name='division']"), ForeignChar);
                test.validateElementIsNotPresent(driver, By.CssSelector("div[class='text input error required']>input[name='division']"));
                action.IDeleteValueOnFieldUsingBackSpace(driver, By.CssSelector("input[name='division']"), ForeignChar);
                test.validateElementIsPresent(driver, By.CssSelector("div[class='text input error required']>input[name='division']"));
                action.IDeleteValueOnFields(driver, By.CssSelector("input[name='division']"));
                action.IType(driver, By.CssSelector("input[name='division']"), ValidInput);
                test.validateElementIsNotPresent(driver, By.CssSelector("div[class='text input error required']>input[name='division']"));

                /****GDPR Section*****/
                test.validateElementIsPresent(driver, Elements.Registration_TermsOfUse_ChxBox);
                test.validateElementIsPresent(driver, Elements.Registration_Consent_Comm);
                test.validateStringIsCorrect(driver, By.CssSelector("div[class='consents']>div[class='checkbox']>label>span"), "アナログ・デバイセズまたはその正規販売代理店から、製品やそのサポートに関する連絡が来る場合があることを承諾する。");
                test.validateStringIsCorrect(driver, By.CssSelector("div[class='consents']>div[class='checkbox']>label>span>a:nth-child(1)"), "正規販売代理店");
                test.validateStringIsCorrect(driver, By.CssSelector("div[class='consents']>p:nth-of-type(2)>a:nth-child(2)"), "その代理店");
                test.validateStringIsCorrect(driver, By.CssSelector("div[class='consents']>p:nth-of-type(2)>a:nth-child(1)"), "個人情報保護とセキュリティに関する方針");
                action.IClick(driver, Elements.Registration_EmailConsent_ChxBox);
                test.validateElementIsPresent(driver, By.CssSelector("button[class='ma btn btn-primary disabled']"));
                action.IClick(driver, Elements.Registration_EmailConsent_ChxBox);
                action.IClick(driver, Elements.Registration_Consent_Comm);
                test.validateElementIsPresent(driver, By.CssSelector("button[class='ma btn btn-primary disabled']"));
                action.IClick(driver, Elements.Registration_Consent_Comm);
                action.IClick(driver, Elements.Registration_TermsOfUse_ChxBox);
                action.IType(driver, Elements.Registration_ConfirmPassword_TxtField, "Test");
                test.validateElementIsPresent(driver, By.CssSelector("button[class='ma btn btn-primary disabled']"));
                action.IDeleteValueOnFields(driver, Elements.Registration_ConfirmPassword_TxtField);
                action.IType(driver, Elements.Registration_ConfirmPassword_TxtField, "Test_1234");

                action.IClick(driver, Elements.Registration_Register_Btn);

                Thread.Sleep(10000);
                test.validateStringInstance(driver, driver.Url, "b2clogin.com");
                action.ILogin(driver, Email_Address, "Test_1234");
                Thread.Sleep(5000);
                test.validateStringInstance(driver, driver.Url, "jp/app");
            });
        }

        [Test, Category("Registration"), Category("NonCore")]
        [TestCase("jp", TestName = "Verify registration using already registered social account in JP Locale")]
        public void Registration_VerifyRegistrationUsingSocialAccount(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/app/registration");
            action.IClick(driver, Elements.Registration_Skip_Btn);
            action.IType(driver, Elements.Registration_VerifyEmail_TxtField, "ISSQA.Auto0413@gmail.com");
            action.IClick(driver, Elements.Registration_SendVerificationCode_Btn);
            test.validateStringIsCorrect(driver, By.CssSelector("span[class='ma alert fade in alert-danger'] p"), "This email is already registered with myAnalog and connected to a social network.");
            string SocialApp = "";
            for (int x = 1; util.CheckElement(driver, By.CssSelector("span[class='ma alert fade in alert-danger']>span>a:nth-of-type(" + x + ")"), 1); x++)
            {
                SocialApp = util.GetText(driver, By.CssSelector("span[class='ma alert fade in alert-danger']>span>a:nth-of-type(" + x + ")"));

                action.IOpenLinkInNewTab(driver, By.CssSelector("span[class='ma alert fade in alert-danger']>span>a:nth-of-type(" + x + ")"));
                if (SocialApp.Equals("Google"))
                {
                    test.validateStringInstance(driver, driver.Url, "google.com");
                }
                else if (SocialApp.Equals("LinkedIn"))
                {
                    test.validateStringInstance(driver, driver.Url, "linkedin.com");
                }
                else if (SocialApp.Equals("Facebook"))
                {
                    test.validateStringInstance(driver, driver.Url, "facebook.com");
                }

                driver.SwitchTo().Window(driver.WindowHandles.First());
            }
        }
    }
}
    