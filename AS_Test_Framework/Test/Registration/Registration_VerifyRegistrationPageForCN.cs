﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System;
using System.Threading;
using OpenQA.Selenium;
using System.Text.RegularExpressions;
using System.Linq;

namespace AS_Test_Framework.Registration
{

    [TestFixture]
    public class Registration_VerifyRegistrationPageForChinese : BaseSetUp
    {
        public Registration_VerifyRegistrationPageForChinese() : base() { }

        [Test, Category("Registration"), Category("NonCore")]
        [TestCase("zh", TestName = "Verify Registration in Chinese Locale")]
        public void Registration_VerifyRegistrationPageTitle(string Locale)
        {
            string MoreThan255Char = "This is just a test This is just a test This is just a test This is just a test This is just a test This is just a test This is just a test This is just a test This is just a test This is just a test This is just a test This is just a test This is just a test";
            string ValidInput = "Auto123!";
            string Email_Address = util.Generate_EmailAddress();
            string ForeignChar = "立即";
            string password_input = "Test123testtesttest";

            action.Navigate(driver, Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/app/registration");
            Thread.Sleep(1000);
            action.IVerifyEmailVerificationCode(driver, Email_Address);

            Assert.Multiple(() =>
            {

                test.validateElementIsPresent(driver, By.CssSelector("button[class='ma btn btn-primary disabled']"));

                test.validateStringIsCorrect(driver, By.CssSelector("section[class='register']>button"), "繁体中文");
                test.validateString(driver, "CHINA", util.GetText(driver, By.CssSelector("div[class='select required has value']>button>span")));
                action.IClick(driver, By.CssSelector("section[class='register']>button"));
                test.validateStringIsCorrect(driver, By.CssSelector("section[class='register']>button"), "简体中文");
                test.validateString(driver, "TAIWAN", util.GetText(driver, By.CssSelector("div[class='select required has value']>button>span")));
                action.IClick(driver, By.CssSelector("section[class='register']>button"));

                /****Family Name****/
                test.validateElementIsPresent(driver, Elements.Registration_LastName_TxtField);
                test.validateString(driver, "名", util.ReturnAttribute(driver, Elements.Registration_LastName_TxtField, "placeholder"));
                action.IType(driver, Elements.Registration_LastName_TxtField, ForeignChar);
                action.IType(driver, Elements.Registration_LastName_TxtField, Keys.Tab);
                test.validateElementIsNotPresent(driver, By.CssSelector("div[class='text input error required']>input[name='last']"));
                action.IDeleteValueOnFieldUsingBackSpace(driver, Elements.Registration_LastName_TxtField, ForeignChar);
                action.IType(driver, Elements.Registration_LastName_TxtField, Keys.Tab);
                test.validateElementIsPresent(driver, By.CssSelector("div[class='text input error required']>input[name='last']"));
                action.IDeleteValueOnFields(driver, Elements.Registration_LastName_TxtField);
                action.IType(driver, Elements.Registration_LastName_TxtField, MoreThan255Char);
                test.validateString(driver, "255", util.ReturnAttribute(driver, Elements.Registration_LastName_TxtField, "value").Length.ToString());
                action.IDeleteValueOnFields(driver, Elements.Registration_LastName_TxtField);
                action.IType(driver, Elements.Registration_LastName_TxtField, ValidInput);
                action.IType(driver, Elements.Registration_LastName_TxtField, Keys.Tab);
                test.validateElementIsNotPresent(driver, By.CssSelector("div[class='text input error required']>input[name='last']"));

                /****First Name****/
                test.validateElementIsPresent(driver, Elements.Registration_FirstName_TxtField);
                test.validateString(driver, "姓", util.ReturnAttribute(driver, Elements.Registration_FirstName_TxtField, "placeholder"));
                action.IType(driver, Elements.Registration_FirstName_TxtField, ForeignChar);
                test.validateElementIsNotPresent(driver, By.CssSelector("div[class='text input error required']>input[name='first']"));
                action.IDeleteValueOnFieldUsingBackSpace(driver, Elements.Registration_FirstName_TxtField, ForeignChar);
                test.validateElementIsPresent(driver, By.CssSelector("div[class='text input error required']>input[name='first']"));
                action.IDeleteValueOnFields(driver, Elements.Registration_FirstName_TxtField);
                action.IType(driver, Elements.Registration_FirstName_TxtField, MoreThan255Char);
                test.validateString(driver, "255", util.ReturnAttribute(driver, Elements.Registration_FirstName_TxtField, "value").Length.ToString());
                action.IDeleteValueOnFields(driver, Elements.Registration_FirstName_TxtField);
                action.IType(driver, Elements.Registration_FirstName_TxtField, ValidInput);
                test.validateElementIsNotPresent(driver, By.CssSelector("div[class='text input error required']>input[name='first']"));

                /***Email Field***/
                test.validateElementIsNotEnabled(driver, Elements.Registration_EmailAddress_TxtField);

                /****Password****/
                test.validateElementIsPresent(driver, Elements.Registration_Password_TxtField);
                test.validateString(driver, "密码", util.ReturnAttribute(driver, Elements.Registration_Password_TxtField, "placeholder"));
                action.IClick(driver, Elements.Registration_Password_TxtField);
                test.validateElementIsPresent(driver, By.CssSelector("div[class='requirements focused']>span"));
                action.IType(driver, Elements.Registration_Password_TxtField, password_input);
                test.validateString(driver, "16", util.ReturnAttribute(driver, Elements.Registration_Password_TxtField, "value").Length.ToString());
                action.IDeleteValueOnFieldUsingBackSpace(driver, Elements.Registration_Password_TxtField, password_input);
                test.validateElementIsPresent(driver, By.CssSelector("div[class='password input error required invalid icon']>input[name='password']"));
                action.IType(driver, Elements.Registration_Password_TxtField, "aa");
                test.validateElementIsPresent(driver, By.CssSelector("div[class='password input has value error required invalid icon']>input[name='password']"));
                action.IDeleteValueOnFields(driver, Elements.Registration_Password_TxtField);
                action.IType(driver, Elements.Registration_Password_TxtField, "testtest");
                test.validateElementIsPresent(driver, By.CssSelector("div[class='password input has value required invalid icon']>input[name='password']"));
                action.IDeleteValueOnFields(driver, Elements.Registration_Password_TxtField);
                action.IType(driver, Elements.Registration_Password_TxtField, "TESTTEST");
                test.validateElementIsPresent(driver, By.CssSelector("div[class='password input has value required invalid icon']>input[name='password']"));
                action.IDeleteValueOnFields(driver, Elements.Registration_Password_TxtField);
                action.IType(driver, Elements.Registration_Password_TxtField, "Test_1234");

                /****Confirm Password****/
                test.validateElementIsPresent(driver, Elements.Registration_ConfirmPassword_TxtField);
                test.validateString(driver, "再次确认", util.ReturnAttribute(driver, Elements.Registration_ConfirmPassword_TxtField, "placeholder"));

                action.IType(driver, Elements.Registration_ConfirmPassword_TxtField, "aa");
                test.validateElementIsPresent(driver, By.CssSelector("div[class='password input has value error required invalid icon']>input[name='confirmPassword']"));

                action.IDeleteValueOnFieldUsingBackSpace(driver, Elements.Registration_ConfirmPassword_TxtField, password_input);
                test.validateElementIsPresent(driver, By.CssSelector("div[class='password input error required']>input[name='confirmPassword']"));

                action.IDeleteValueOnFields(driver, Elements.Registration_ConfirmPassword_TxtField);
                action.IType(driver, Elements.Registration_ConfirmPassword_TxtField, "Test_1234");

                /****Country Dropdown****/
                test.validateElementIsPresent(driver, Elements.Registration_Country_Dropdown);
                action.IClick(driver, Elements.Registration_Country_Dropdown);
                test.validateElementIsPresent(driver, By.CssSelector("div[class='select required has value']>div[class='options open']"));
                string[] country_list = new string[10];
                for (int x = 2, y = 0; y <= 9; x++, y++)
                {
                    country_list[y] = util.GetText(driver, By.CssSelector("div[class='select required has value']>div[class='options open']>div:nth-child(" + x + ")"));
                }
                util.IsAscendingOrder(country_list);
                test.validateStringIfInAscenading(country_list);
                action.IClick(driver, By.CssSelector("div[class='select required has value']>div[class='options open']>div[data-label='CHINA']"));

                /****Province Dropdown****/
                test.validateElementIsPresent(driver, By.CssSelector("div[class='select required']>button"));
                test.validateStringIsCorrect(driver, By.CssSelector("div[class='select required']>button>span"), "选择省市");
                action.IClick(driver, By.CssSelector("div[class='select required']>button"));
                test.validateElementIsPresent(driver, By.CssSelector("div[class='select required']>div[class='options open']"));
                string[] province_list = new string[10];
                for (int x = 2, y = 0; y <= 9; x++, y++)
                {
                    province_list[y] = util.GetText(driver, By.CssSelector("div[class='select required']>div[class='options open']>div:nth-child(" + x + ")"));
                }
                test.validateStringIfInAscenading(province_list);

                action.IClick(driver, By.CssSelector("div[class='select required has value']>button"));
                action.IClick(driver, By.CssSelector("div[class='select required has value']>div[class='options open']>div[data-label='UNITED STATES']"));
                test.validateElementIsPresent(driver, By.CssSelector("div[class='select required']>button"));

                action.IClick(driver, By.CssSelector("div[class='select required has value']>button"));
                action.IClick(driver, By.CssSelector("div[class='select required has value']>div[class='options open']>div[data-label='CHINA']"));
                action.IClick(driver, By.CssSelector("div[class='select required']>button"));
                action.IClick(driver, By.CssSelector("div[class='select required']>div[class='options open']>div[data-label='Anhui']"));
                action.IClick(driver, By.CssSelector("section[class='register']>button"));
                test.validateElementIsPresent(driver, By.CssSelector("div[class='select required']>button"));
                action.IClick(driver, By.CssSelector("div[class='select required']>button"));
                action.IClick(driver, By.CssSelector("div[class='select required']>div[class='options open']>div:nth-child(1)"));
                action.IClick(driver, By.CssSelector("section[class='register']>button"));
                test.validateElementIsPresent(driver, By.CssSelector("div[class='select required']>button"));
                action.IClick(driver, By.CssSelector("div[class='select required']>button"));
                action.IClick(driver, By.CssSelector("div[class='select required']>div[class='options open']>div[data-label='Anhui']"));

                /****City****/
                test.validateElementIsPresent(driver, Elements.Registration_City_TxtField);
                test.validateString(driver, "城市", util.ReturnAttribute(driver, Elements.Registration_City_TxtField, "placeholder"));
                action.IType(driver, Elements.Registration_City_TxtField, "TEST CITY");
                test.validateString(driver, "TEST CITY", util.ReturnAttribute(driver, Elements.Registration_City_TxtField, "value"));
                action.IDeleteValueOnFieldUsingBackSpace(driver, Elements.Registration_City_TxtField, "TEST CITY");
                test.validateElementIsPresent(driver, By.CssSelector("div[class='text input error required'] input[name='city']"));
                action.IType(driver, Elements.Registration_City_TxtField, "TEST CITY");

                /****Company/School Name****/
                test.validateElementIsPresent(driver, Elements.Registration_Company_RadioButton);
                test.validateElementIsPresent(driver, Elements.Registration_School_RadioButton);

                test.validateElementIsPresent(driver, Elements.Registration_CompName_TxtField);
                test.validateString(driver, "公司名称", util.ReturnAttribute(driver, Elements.Registration_CompName_TxtField, "placeholder"));

                action.IType(driver, Elements.Registration_CompName_TxtField, "Analog");
                action.IDeleteValueOnFieldUsingBackSpace(driver, Elements.Registration_CompName_TxtField, util.ReturnAttribute(driver, Elements.Registration_CompName_TxtField, "value"));
                Thread.Sleep(1500);
                test.validateElementIsPresent(driver, By.CssSelector("div[class='text input error required']>input[name='companyName']"));

                action.IClick(driver, Elements.Registration_School_RadioButton);
                test.validateElementIsPresent(driver, Elements.Registration_School_TextField);
                test.validateString(driver, "学校名称", util.ReturnAttribute(driver, Elements.Registration_School_TextField, "placeholder"));

                action.IClick(driver, Elements.Registration_Company_RadioButton);
                action.IType(driver, Elements.Registration_CompName_TxtField, "Analog");

                /****Telephone****/
                test.validateElementIsPresent(driver, Elements.Telephone_TextField);
                test.validateString(driver, "电话", util.ReturnAttribute(driver, Elements.Telephone_TextField, "placeholder"));
                action.IType(driver, Elements.Telephone_TextField, ForeignChar);
                test.validateElementIsNotPresent(driver, By.CssSelector("div[class='text input error required']>input[name='phoneNumber']"));
                action.IDeleteValueOnFieldUsingBackSpace(driver, Elements.Telephone_TextField, ForeignChar);
                test.validateElementIsPresent(driver, By.CssSelector("div[class='text input error required']>input[name='phoneNumber']"));
                action.IType(driver, Elements.Telephone_TextField, "12345679");
                test.validateElementIsNotPresent(driver, By.CssSelector("div[class='text input error required']>input[name='phoneNumber']"));

                /****GDPR Section*****/
                test.validateElementIsPresent(driver, Elements.Registration_TermsOfUse_ChxBox);
                test.validateElementIsPresent(driver, Elements.Registration_Consent_Comm);
                test.validateStringIsCorrect(driver, By.CssSelector("div[class='consents']>div[class='checkbox']>label>span"), "是的，我愿意接收来自Analog Devices 以及授权合作伙伴提供的相关 ADI 产品服务信息。");
                test.validateStringIsCorrect(driver, By.CssSelector("div[class='consents']>div[class='checkbox']>label>span>a:nth-child(1)"), "授权合作伙伴");
                test.validateStringIsCorrect(driver, By.CssSelector("div[class='consents']>p:nth-of-type(2)>a:nth-child(2)"), "授权合作伙伴");
                test.validateStringIsCorrect(driver, By.CssSelector("div[class='consents']>p:nth-of-type(2)>a:nth-child(1)"), "隐私和安全声明");
                action.IClick(driver, Elements.Registration_EmailConsent_ChxBox);
                test.validateElementIsPresent(driver, By.CssSelector("button[class='ma btn btn-primary disabled']"));
                action.IClick(driver, Elements.Registration_EmailConsent_ChxBox);
                action.IClick(driver, Elements.Registration_Consent_Comm);
                test.validateElementIsPresent(driver, By.CssSelector("button[class='ma btn btn-primary disabled']"));
                action.IClick(driver, Elements.Registration_Consent_Comm);
                action.IClick(driver, Elements.Registration_TermsOfUse_ChxBox);
                action.IType(driver, Elements.Registration_ConfirmPassword_TxtField, "Test");
                test.validateElementIsPresent(driver, By.CssSelector("button[class='ma btn btn-primary disabled']"));
                action.IDeleteValueOnFields(driver, Elements.Registration_ConfirmPassword_TxtField);
                action.IType(driver, Elements.Registration_ConfirmPassword_TxtField, "Test_1234");
            
                action.IClick(driver, Elements.Registration_Register_Btn);

                Thread.Sleep(10000);
                test.validateStringInstance(driver, driver.Url, "b2clogin.com");
                action.ILogin(driver, Email_Address, "Test_1234");
                Thread.Sleep(5000);
                test.validateStringInstance(driver, driver.Url, "zh/app");

            });   
        }

        [Test, Category("Registration"), Category("NonCore")]
        [TestCase("zh", TestName = "Verify registration using already registered social account in CN Locale")]
        public void Registration_VerifyRegistrationUsingSocialAccount(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/app/registration");
            action.IClick(driver, Elements.Registration_Skip_Btn);
            action.IType(driver, Elements.Registration_VerifyEmail_TxtField, "ISSQA.Auto0413@gmail.com");
            action.IClick(driver, Elements.Registration_SendVerificationCode_Btn);
            test.validateStringIsCorrect(driver, By.CssSelector("span[class='ma alert fade in alert-danger'] p"), "This email is already registered with myAnalog and connected to a social network.");
            string SocialApp = "";
            for (int x = 1; util.CheckElement(driver, By.CssSelector("span[class='ma alert fade in alert-danger']>span>a:nth-of-type(" + x + ")"), 1); x++) {
                SocialApp = util.GetText(driver, By.CssSelector("span[class='ma alert fade in alert-danger']>span>a:nth-of-type(" + x + ")"));

                action.IOpenLinkInNewTab(driver, By.CssSelector("span[class='ma alert fade in alert-danger']>span>a:nth-of-type(" + x +")"));
                if (SocialApp.Equals("Google"))
                {
                    test.validateStringInstance(driver, driver.Url, "analogb2c");
                }
                else if (SocialApp.Equals("LinkedIn")){
                    test.validateStringInstance(driver, driver.Url, "linkedin.com");
                }
                else if (SocialApp.Equals("Facebook"))
                {
                    test.validateStringInstance(driver, driver.Url, "facebook.com");
                }
                driver.SwitchTo().Window(driver.WindowHandles.First());
            }
           
        }
        [Test, Category("Registration"), Category("NonCore")]
        [TestCase("zh", TestName = "Verify if the province field is not visible for China on the registration page if the user accessing the registration page in shopping cart page.")]
        public void Registration_VerifyCNRegistration(string Locale)
        {
            string Email_Address = util.Generate_EmailAddress();
            action.Navigate(driver, Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx?locale=zh");
            action.IClick(driver, Elements.SC_LoginLink);
            action.IClick(driver, Elements.LoginPage_Register_Link);
            action.IVerifyEmailVerificationCode(driver, Email_Address);
            test.validateStringIsCorrect(driver, By.CssSelector("div[class='select required']>button>span"), "选择省市");
        }
    }
}
    