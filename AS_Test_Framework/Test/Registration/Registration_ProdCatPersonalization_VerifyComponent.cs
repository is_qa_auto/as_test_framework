﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System;
using System.Threading;
using OpenQA.Selenium;
using System.Text.RegularExpressions;

namespace AS_Test_Framework.Registration
{

    [TestFixture]
    public class Registration_ProdCatPersonalization_VerifyComponent : BaseSetUp
    {
        public Registration_ProdCatPersonalization_VerifyComponent() : base() { }

        [Test, Category("Registration"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify Registration's Product Categories Personalization Page component is complete for EN Locale")]
        [TestCase("zh", TestName = "Verify Registration's Product Categories Personalization Page component is complete for CN Locale")]
        [TestCase("jp", TestName = "Verify Registration's Product Categories Personalization Page component is complete for JP Locale")]
        [TestCase("ru", TestName = "Verify Registration's Product Categories Personalization Page component is complete for RU Locale")]
        public void Registration_ProductCategoriesPersonalizationPage_VerifyComponent(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/app/registration");
            Thread.Sleep(5000);
            Assert.Multiple(() =>
            {
                test.validateElementIsPresent(driver, By.CssSelector("section[class='registration'] * section[class='categories']>div[class='section header']>h2>span"));
                test.validateElementIsPresent(driver, By.CssSelector("section[class='registration'] * section[class='categories']>div[class='section header']>h2>small"));
                test.validateElementIsPresent(driver, By.CssSelector("section[class='registration'] * section[class='categories']>div[class='section header']>h2>div[class='help icon top']"));
                test.validateElementIsPresent(driver, By.CssSelector("section[class='registration'] * section[class='categories']>div[class='options grid']"));
                test.validateElementIsPresent(driver, Elements.Registration_Next_Btn);
                test.validateElementIsPresent(driver, Elements.Registration_Skip_Btn);
            });
        }

        [Test, Category("Registration"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that help icon is displayed the tooltip for EN Locale")]
        [TestCase("zh", TestName = "Verify that help icon is displayed the tooltip for CN Locale")]
        [TestCase("jp", TestName = "Verify that help icon is displayed the tooltip for JP Locale")]
        [TestCase("ru", TestName = "Verify that help icon is displayed the tooltip for RU Locale")]
        public void Registration_ProductCategoriesPersonalizationPage_VerifyHelpIcon(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/app/registration");
            Thread.Sleep(1000);
            action.IMouseOverTo(driver, By.CssSelector("section[class='registration'] * section[class='categories']>div[class='section header']>h2>div[class='help icon top']"));
            if (Locale.Equals("zh"))
            {
                test.validateStringIsCorrect(driver, By.CssSelector("section[class='registration'] * section[class='categories']>div[class='section header']>h2>div[class='help icon top']>p>span"), "您的选择可以随时在账号设置中更新或移除");
            }
            else if (Locale.Equals("ru"))
            {
                test.validateStringIsCorrect(driver, By.CssSelector("section[class='registration'] * section[class='categories']>div[class='section header']>h2>div[class='help icon top']>p>span"), "Ваши варианты выбора могут быть обновлены или удалены в любое время в настройках вашего аккаунта.");
            }
            else
            {
                test.validateStringIsCorrect(driver, By.CssSelector("section[class='registration'] * section[class='categories']>div[class='section header']>h2>div[class='help icon top']>p>span"), "Your selections can be updated or removed anytime in your account settings.");
            }
        }

        [Test, Category("Registration"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that Product Categories will change its color when selecting or de-selecting for EN Locale")]
        [TestCase("zh", TestName = "Verify that Product Categories will change its color when selecting or de-selecting for CN Locale")]
        [TestCase("jp", TestName = "Verify that Product Categories will change its color when selecting or de-selecting for JP Locale")]
        [TestCase("ru", TestName = "Verify that Product Categories will change its color when selecting or de-selecting for RU Locale")]
        public void Registration_ProductCategoriesPersonalizationPage_VerifySelectingOfProdCat(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/app/registration");
            action.IClick(driver, By.CssSelector("div[class='options grid']>div[class='option']>label"));
            action.IClick(driver, By.CssSelector("div[class='options grid']>div[class='option']:nth-child(2)>label"));
            test.validateString(driver, "rgba(0, 159, 189, 1)", util.GetCssValue(driver, By.CssSelector("div[class='options grid']>div[class='option']>label"), "background-color"));
            test.validateString(driver, "rgba(0, 159, 189, 1)", util.GetCssValue(driver, By.CssSelector("div[class='options grid']>div[class='option']:nth-child(2)>label"), "background-color"));
            action.IClick(driver, By.CssSelector("section[class='registration'] * section[class='categories']>div[class='options grid']>div>label"));
            test.validateString(driver, "rgba(255, 255, 255, 1)", util.GetCssValue(driver, By.CssSelector("section[class='registration'] * section[class='categories']>div[class='options grid']>div>label"), "background-color"));
        }

    }
}
