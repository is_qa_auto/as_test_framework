﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System;
using System.Threading;
using OpenQA.Selenium;
using System.Text.RegularExpressions;
using System.Linq;

namespace AS_Test_Framework.Registration
{

    [TestFixture]
    public class Registration_VerifyRegistrationPageForEn : BaseSetUp
    {
        public Registration_VerifyRegistrationPageForEn() : base() { }

        [Test, Category("Registration"), Category("NonCore")]
        [TestCase("en", TestName = "Verify Registration in EN Locale")]
        public void Registration_VerifyRegistrationPageTitle(string Locale)
        {
            string MoreThan255Char = "This is just a test This is just a test This is just a test This is just a test This is just a test This is just a test This is just a test This is just a test This is just a test This is just a test This is just a test This is just a test This is just a test";
            string ValidInput = "Auto123!";
            string Email_Address = util.Generate_EmailAddress();
            string ForeignChar = "立即";
            string password_input = "Test123testtesttest";

            action.Navigate(driver, Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/app/registration");
            Thread.Sleep(1000);
            action.IVerifyEmailVerificationCode(driver, Email_Address);

            Assert.Multiple(() =>
            {
                test.validateElementIsPresent(driver, By.CssSelector("button[class='ma btn btn-primary disabled']"));
                test.validateString(driver, "Register today. Lets get started!", util.GetText(driver, By.CssSelector("section[class='register'] * h2>span")).Replace("'", ""));
                test.validateString(driver, "Please fill in all fields below. You will receive an email within a few minutes to activate your registration.", util.GetText(driver, By.CssSelector("section[class='register'] * h2>small")));

                /****First Name****/
                test.validateElementIsPresent(driver, Elements.Registration_FirstName_TxtField);
                test.validateString(driver, "First name", util.ReturnAttribute(driver, Elements.Registration_FirstName_TxtField, "placeholder"));
                action.IType(driver, Elements.Registration_FirstName_TxtField, ForeignChar);
                test.validateElementIsNotPresent(driver, By.CssSelector("div[class='text input error required']>input[name='first']"));
                action.IDeleteValueOnFieldUsingBackSpace(driver, Elements.Registration_FirstName_TxtField, ForeignChar);
                test.validateElementIsPresent(driver, By.CssSelector("div[class='text input error required']>input[name='first']"));
                action.IDeleteValueOnFields(driver, Elements.Registration_FirstName_TxtField);
                action.IType(driver, Elements.Registration_FirstName_TxtField, MoreThan255Char);
                test.validateString(driver, "255", util.ReturnAttribute(driver, Elements.Registration_FirstName_TxtField, "value").Length.ToString());
                action.IDeleteValueOnFields(driver, Elements.Registration_FirstName_TxtField);
                action.IType(driver, Elements.Registration_FirstName_TxtField, ValidInput);
                test.validateElementIsNotPresent(driver, By.CssSelector("div[class='text input error required']>input[name='first']"));
            });


            /****Last Name****/
            Assert.Multiple(() =>
            {
                test.validateElementIsPresent(driver, Elements.Registration_LastName_TxtField);
                test.validateString(driver, "Last name", util.ReturnAttribute(driver, Elements.Registration_LastName_TxtField, "placeholder"));
                action.IType(driver, Elements.Registration_LastName_TxtField, ForeignChar);
                action.IType(driver, Elements.Registration_LastName_TxtField, Keys.Tab);
                test.validateElementIsNotPresent(driver, By.CssSelector("div[class='text input error required']>input[name='last']"));
                action.IDeleteValueOnFieldUsingBackSpace(driver, Elements.Registration_LastName_TxtField, ForeignChar);
                action.IType(driver, Elements.Registration_LastName_TxtField, Keys.Tab);
                test.validateElementIsPresent(driver, By.CssSelector("div[class='text input error required']>input[name='last']"));
                action.IDeleteValueOnFields(driver, Elements.Registration_LastName_TxtField);
                action.IType(driver, Elements.Registration_LastName_TxtField, MoreThan255Char);
                test.validateString(driver, "255", util.ReturnAttribute(driver, Elements.Registration_LastName_TxtField, "value").Length.ToString());
                action.IDeleteValueOnFields(driver, Elements.Registration_LastName_TxtField);
                action.IType(driver, Elements.Registration_LastName_TxtField, ValidInput);
                action.IType(driver, Elements.Registration_LastName_TxtField, Keys.Tab);
                test.validateElementIsNotPresent(driver, By.CssSelector("div[class='text input error required']>input[name='last']"));
            });
            /***Email Field***/
            test.validateElementIsNotEnabled(driver, Elements.Registration_EmailAddress_TxtField);

            /****Password****/
            Assert.Multiple(() =>
            {
                test.validateElementIsPresent(driver, Elements.Registration_Password_TxtField);
                test.validateString(driver, "Password", util.ReturnAttribute(driver, Elements.Registration_Password_TxtField, "placeholder"));
                action.IClick(driver, Elements.Registration_Password_TxtField);
                test.validateElementIsPresent(driver, By.CssSelector("div[class='requirements focused']>span"));
                action.IType(driver, Elements.Registration_Password_TxtField, password_input);
                test.validateString(driver, "16", util.ReturnAttribute(driver, Elements.Registration_Password_TxtField, "value").Length.ToString());
                action.IDeleteValueOnFieldUsingBackSpace(driver, Elements.Registration_Password_TxtField, password_input);
                test.validateElementIsPresent(driver, By.CssSelector("div[class='password input error required invalid icon']>input[name='password']"));
                action.IType(driver, Elements.Registration_Password_TxtField, "aa");
                test.validateElementIsPresent(driver, By.CssSelector("div[class='password input has value error required invalid icon']>input[name='password']"));
                action.IDeleteValueOnFields(driver, Elements.Registration_Password_TxtField);
                action.IType(driver, Elements.Registration_Password_TxtField, "testtest");
                test.validateElementIsPresent(driver, By.CssSelector("div[class='password input has value required invalid icon']>input[name='password']"));
                action.IDeleteValueOnFields(driver, Elements.Registration_Password_TxtField);
                action.IType(driver, Elements.Registration_Password_TxtField, "TESTTEST");
                test.validateElementIsPresent(driver, By.CssSelector("div[class='password input has value required invalid icon']>input[name='password']"));
                action.IDeleteValueOnFields(driver, Elements.Registration_Password_TxtField);
                action.IType(driver, Elements.Registration_Password_TxtField, "Test_1234");
            });

            /****Confirm Password****/
            Assert.Multiple(() =>
            {
                test.validateElementIsPresent(driver, Elements.Registration_ConfirmPassword_TxtField);
                test.validateString(driver, "Confirm Password", util.ReturnAttribute(driver, Elements.Registration_ConfirmPassword_TxtField, "placeholder"));

                action.IType(driver, Elements.Registration_ConfirmPassword_TxtField, "aa");
                test.validateElementIsPresent(driver, By.CssSelector("div[class='password input has value error required invalid icon']>input[name='confirmPassword']"));

                action.IDeleteValueOnFieldUsingBackSpace(driver, Elements.Registration_ConfirmPassword_TxtField, password_input);
                test.validateElementIsPresent(driver, By.CssSelector("div[class='password input error required']>input[name='confirmPassword']"));

                action.IDeleteValueOnFields(driver, Elements.Registration_ConfirmPassword_TxtField);
                action.IType(driver, Elements.Registration_ConfirmPassword_TxtField, "Test_1234");
            });

            /****Country Dropdown****/
            Assert.Multiple(() =>
            {
                test.validateElementIsPresent(driver, Elements.Registration_Country_Dropdown);
                test.validateString(driver, "Select your country/region", util.GetText(driver, By.CssSelector("div[class='select required']>button>span")));
                action.IClick(driver, Elements.Registration_Country_Dropdown);
                test.validateElementIsPresent(driver, By.CssSelector("div[class='select required']>div[class='options open']"));
                string[] country_list = new string[10];
                for (int x = 2, y = 0; y <= 9; x++, y++)
                {
                    country_list[y] = util.GetText(driver, By.CssSelector("div[class='select required']>div[class='options open']>div:nth-child(" + x + ")"));
                }
                util.IsAscendingOrder(country_list);
                test.validateStringIfInAscenading(country_list);
                action.IClick(driver, By.CssSelector("div[class='select required']>div[class='options open']>div[data-label='CHINA']"));
                Thread.Sleep(2000);
                test.validateElementIsPresent(driver, By.CssSelector("div[class='select required']>button"));
                action.IClick(driver, By.CssSelector("div[class='select required has value']>button>span"));
                action.IClick(driver, By.CssSelector("div[class='select required has value']>div[class='options open']>div[data-label='UNITED STATES']"));
                //test.validateElementIsNotPresent(driver, By.CssSelector("div[class='select required']>button"));
            });

            /****State Dropdown****/
            Assert.Multiple(() =>
            {
                test.validateElementIsPresent(driver, By.CssSelector("div[class='select required']>button"));
                test.validateString(driver, "State", util.GetText(driver, By.CssSelector("div[class='select required']>button>span")));
                action.IClick(driver, By.CssSelector("div[class='select required']>button"));
                test.validateElementIsPresent(driver, By.CssSelector("div[class='select required']>div[class='options open']"));
                string[] state_list = new string[10];
                for (int x = 2, y = 0; y <= 9; x++, y++)
                {
                    state_list[y] = util.GetText(driver, By.CssSelector("div[class='select required']>div[class='options open']>div:nth-child(" + x + ")"));
                }
                util.IsAscendingOrder(state_list);
                test.validateStringIfInAscenading(state_list);
                action.IClick(driver, By.CssSelector("div[class='select required']>div[class='options open']>div[data-label='Alabama']"));
                test.validateElementIsNotPresent(driver, By.CssSelector("div[class='select required']>button"));
            });

            /****City Dropdown****/
            Assert.Multiple(() =>
            {
                test.validateElementIsPresent(driver, Elements.Registration_City_TxtField);
                test.validateString(driver, "City", util.ReturnAttribute(driver, Elements.Registration_City_TxtField, "placeholder"));
                action.IType(driver, Elements.Registration_City_TxtField, "TEST CITY");
                test.validateString(driver, "TEST CITY", util.ReturnAttribute(driver, Elements.Registration_City_TxtField, "value"));
                action.IDeleteValueOnFieldUsingBackSpace(driver, Elements.Registration_City_TxtField, "TEST CITY");
                test.validateElementIsPresent(driver, By.CssSelector("div[class='text input error required'] input[name='city']"));
                action.IType(driver, Elements.Registration_City_TxtField, "TEST CITY");

            });

            /****Zipcode Dropdown****/
            Assert.Multiple(() =>
            {
                test.validateElementIsPresent(driver, Elements.Registration_ZipCode_TxtField);
                test.validateString(driver, "Zip code", util.ReturnAttribute(driver, Elements.Registration_ZipCode_TxtField, "placeholder"));
                action.IType(driver, Elements.Registration_ZipCode_TxtField, "1234567");
                test.validateString(driver, "1234567", util.ReturnAttribute(driver, Elements.Registration_ZipCode_TxtField, "value"));
                action.IDeleteValueOnFields(driver, Elements.Registration_ZipCode_TxtField);
            });

            /****Company/School Name****/
            Assert.Multiple(() =>
            {
                test.validateElementIsPresent(driver, Elements.Registration_Company_RadioButton);
                test.validateElementIsPresent(driver, Elements.Registration_School_RadioButton);

                test.validateElementIsPresent(driver, Elements.Registration_CompName_TxtField);
                test.validateString(driver, "Company name", util.ReturnAttribute(driver, Elements.Registration_CompName_TxtField, "placeholder"));

                action.IType(driver, Elements.Registration_CompName_TxtField, "Analog");
                action.IDeleteValueOnFieldUsingBackSpace(driver, Elements.Registration_CompName_TxtField, util.ReturnAttribute(driver, Elements.Registration_CompName_TxtField, "value"));
                Thread.Sleep(1500);
                test.validateElementIsPresent(driver, By.CssSelector("div[class='text input error required']>input[name='companyName']"));

                action.IClick(driver, Elements.Registration_School_RadioButton);
                test.validateElementIsPresent(driver, Elements.Registration_School_TextField);
                test.validateString(driver, "School name", util.ReturnAttribute(driver, Elements.Registration_School_TextField, "placeholder"));

                action.IClick(driver, Elements.Registration_Company_RadioButton);
                action.IType(driver, Elements.Registration_CompName_TxtField, "Analog");
            });

            /****GDPR Section*****/
            Assert.Multiple(() =>
            {
                test.validateElementIsPresent(driver, Elements.Registration_TermsOfUse_ChxBox);
                test.validateElementIsPresent(driver, Elements.Registration_Consent_Comm);
                test.validateStringIsCorrect(driver, By.CssSelector("div[class='consents']>div[class='checkbox']>label>span"), "Yes, I’d like to receive communications from Analog Devices and authorized partners related to ADI’s products and services.");
                test.validateStringIsCorrect(driver, By.CssSelector("div[class='consents']>div[class='checkbox']>label>span>a:nth-child(1)"), "authorized partners");
                test.validateStringIsCorrect(driver, By.CssSelector("div[class='consents']>p:nth-of-type(2)>a:nth-child(2)"), "Authorized Partners");
                test.validateStringIsCorrect(driver, By.CssSelector("div[class='consents']>p:nth-of-type(2)>a:nth-child(1)"), "Privacy & Security Statement");
                action.IClick(driver, Elements.Registration_EmailConsent_ChxBox);
                test.validateElementIsPresent(driver, By.CssSelector("button[class='ma btn btn-primary disabled']"));
                action.IClick(driver, Elements.Registration_EmailConsent_ChxBox);
                action.IClick(driver, Elements.Registration_Consent_Comm);
                test.validateElementIsPresent(driver, By.CssSelector("button[class='ma btn btn-primary disabled']"));
                action.IClick(driver, Elements.Registration_Consent_Comm);
                action.IClick(driver, Elements.Registration_TermsOfUse_ChxBox);
                action.IType(driver, Elements.Registration_ConfirmPassword_TxtField, "Test");
                test.validateElementIsPresent(driver, By.CssSelector("button[class='ma btn btn-primary disabled']"));
                action.IDeleteValueOnFields(driver, Elements.Registration_ConfirmPassword_TxtField);
                action.IType(driver, Elements.Registration_ConfirmPassword_TxtField, "Test_1234");
            });

            action.IClick(driver, Elements.Registration_Register_Btn);

            Assert.Multiple(() =>
            {
                Thread.Sleep(10000);
                test.validateStringInstance(driver, driver.Url, "b2clogin.com");
                action.ILogin(driver, Email_Address, "Test_1234");
                Thread.Sleep(5000);
                test.validateStringInstance(driver, driver.Url, "en/app");
            });

        }

        [Test, Category("Registration"), Category("NonCore")]
        [TestCase("en", TestName = "Verify registration using already registered social account in EN Locale")]
        public void Registration_VerifyRegistrationUsingSocialAccount(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/app/registration");
            action.IClick(driver, Elements.Registration_Skip_Btn);
            action.IType(driver, Elements.Registration_VerifyEmail_TxtField, "ISSQA.Auto0413@gmail.com");
            action.IClick(driver, Elements.Registration_SendVerificationCode_Btn);
            test.validateStringIsCorrect(driver, By.CssSelector("span[class='ma alert fade in alert-danger'] p"), "This email is already registered with myAnalog and connected to a social network.");
            string SocialApp = "";
            for (int x = 1; util.CheckElement(driver, By.CssSelector("span[class='ma alert fade in alert-danger']>span>a:nth-of-type(" + x + ")"), 1); x++)
            {
                SocialApp = util.GetText(driver, By.CssSelector("span[class='ma alert fade in alert-danger']>span>a:nth-of-type(" + x + ")"));

                action.IOpenLinkInNewTab(driver, By.CssSelector("span[class='ma alert fade in alert-danger']>span>a:nth-of-type(" + x + ")"));
                if (SocialApp.Equals("Google"))
                {
                    test.validateStringInstance(driver, driver.Url, "google.com");
                }
                else if (SocialApp.Equals("LinkedIn"))
                {
                    test.validateStringInstance(driver, driver.Url, "linkedin.com");
                }
                else if (SocialApp.Equals("Facebook"))
                {
                    test.validateStringInstance(driver, driver.Url, "facebook.com");
                }
                driver.SwitchTo().Window(driver.WindowHandles.First());
            }
        }


        //[Test, Category("Registration"), Category("NonCore")]
        //[TestCase("en", TestName = "Verify registration and login using linkedin account in EN Locale")]
        //public void Registration_VerifyRegistrationUsingSocialAccount1(string Locale)
        //{
        //    action.DeleteAccountInWebcontent(driver, "ISSQA.Auto0413@gmail.com");
        //    action.Navigate(driver, Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/app/registration");
        //    action.IClick(driver, Elements.Registration_Skip_Btn);
        //    action.IClick(driver, Elements.Social_Reg_LinkedIn_Btn);
        //    action.ILoginVia_LinkedIn(driver, "ISSQA.Auto0413@gmail.com","Test_1234");
            
        //    /***validate that Firstname and Lastname is pre-populated***/
        //    test.validateString(driver, "IS QA", util.ReturnAttribute(driver, Elements.Social_Reg_Fname, "value"));
        //    test.validateString(driver, "Corp", util.ReturnAttribute(driver, Elements.Social_Reg_Lname, "value"));

        //    /***Field Validation***/
        //    test.validateString(driver, "Please provide the following details", util.GetText(driver, By.CssSelector("section[class='register'] * h2>span")));
        //}
    }
}
    