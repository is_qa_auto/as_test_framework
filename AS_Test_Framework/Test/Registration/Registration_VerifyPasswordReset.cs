﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Threading;
using System.Linq;
using System;

namespace AS_Test_Framework.Registration
{

    [TestFixture]
    public class Registration_VerifyPasswordReset : BaseSetUp
    {
        public Registration_VerifyPasswordReset() : base() { }

        [Test, Category("Registration"), Category("NonCore")]
        [TestCase("en", "This information is required.", "Please enter a valid email address.", TestName = "Verify Forget Password Step 1 - Email Verification elements and error validation for EN Locale")]
        [TestCase("cn", "此信息为必填项。", "请输入有效的电子邮件地址。", TestName = "Verify Forget Password Step 1 - Email Verification elements for CN Locale")]
        [TestCase("jp", "この情報は必須です。", "有効なメールアドレスを入力してください。", TestName = "Verify Forget Password Step 1 - Email Verification elements for JP Locale")]
        public void Registration_VerifyForgotPasswordElementValidation(string Locale, string NoEmailMessage, string InvEmailErrorMessage) {

            action.Navigate(driver, Configuration.Env_Url + Locale + "/index.html");
            action.IClick(driver, Elements.MainNavigation_MyAnalogTab);
            action.IClick(driver, Elements.MyAnalogTab_LogInToMyAnalog_Btn);
            action.IClick(driver, Elements.Login_ForgotPassLink);
           
            test.validateElementIsPresent(driver, Elements.ForgotPass_Email_TxtField);
            test.validateElementIsPresent(driver, Elements.ForgotPass_SendVerificationCode_Btn);
            action.IClick(driver, Elements.ForgotPass_SendVerificationCode_Btn);
            test.validateString(driver, NoEmailMessage, util.GetText(driver, By.CssSelector("div[class='error itemLevel show']")));
            driver.Navigate().Refresh();
            action.IType(driver, Elements.ForgotPass_Email_TxtField, "aries.sorosoro");
            action.IClick(driver, Elements.ForgotPass_SendVerificationCode_Btn);
            test.validateString(driver, InvEmailErrorMessage, util.GetText(driver, By.CssSelector("div[class='error itemLevel show']")));
           
        }


        [Test, Category("Registration"), Category("NonCore"), Retry(2)]
        [TestCase("en", "An account could not be found for the provided user ID.", "account email verification code", TestName = "Verify Error validation for Non Existing account for EN Locale")]
        [TestCase("cn", "An account could not be found for the provided user ID.", "帳戶電子郵��驗證碼", TestName = "Verify Error validation for Non Existing account for CN Locale")]
        [TestCase("jp", "入力いただいたユーザIDでのアカウント登録が確認できません。", "アカウント��電子メール確認コード", TestName = "Verify Error validation for Non Existing account for JP Locale")]
        public void Registration_VerifyForgotPassswordForNonExistingAccount(string Locale, string ErrorMessage1, string ErrorMessage2)
        {
            const string number = "1234567890";
            var random3 = new Random();
            string rand3 = new string(Enumerable.Repeat(number, 2)
              .Select(s => s[random3.Next(s.Length)]).ToArray());

            action.Navigate(driver, Configuration.Env_Url + Locale + "/index.html");
            action.IClick(driver, Elements.MainNavigation_MyAnalogTab);
            action.IClick(driver, Elements.MyAnalogTab_LogInToMyAnalog_Btn);
            action.IClick(driver, Elements.Login_ForgotPassLink);
            action.IType(driver, Elements.ForgotPass_Email_TxtField, "non_registered_" + Locale + "_" + rand3 + "@mailinator.com");
            action.IClick(driver, Elements.ForgotPass_SendVerificationCode_Btn);
            action.IOpenNewTab(driver);
            
            action.ICheckMailinatorEmail(driver, "non_registered_" + Locale + "_" + rand3 + "@mailinator.com");
            if (Configuration.Environment.Equals("qa"))
            {
                test.validateStringInstance(driver, util.GetText(driver, By.CssSelector("table[class='table-striped jambo_table'] * tr>td:nth-child(3)")), "Analog Devices B2C QA");
            }
            else if (Configuration.Environment.Equals("dev"))
            {
                test.validateStringInstance(driver, util.GetText(driver, By.CssSelector("table[class='table-striped jambo_table'] * tr>td:nth-child(3)")), "Analog Devices B2C Dev");
            }
            else
            {
                test.validateStringInstance(driver, util.GetText(driver, By.CssSelector("table[class='table-striped jambo_table'] * tr>td:nth-child(3)")), "Analog Devices B2C ");
            }
            action.IClick(driver, By.CssSelector("table[class='table-striped jambo_table'] * tr>td:nth-child(2)"));
            driver.SwitchTo().Frame(driver.FindElement(By.CssSelector("iframe[id='html_msg_body']")));
            string VerificationCode = util.GetText(driver, By.CssSelector("span[id='BodyPlaceholder_UserVerificationEmailBodySentence2']")).Substring(util.GetText(driver, By.CssSelector("span[id='BodyPlaceholder_UserVerificationEmailBodySentence2']")).Length - 6).Trim();

            driver.SwitchTo().Window(driver.WindowHandles.First());
            action.IType(driver, Elements.ForgotPass_VerifyEmail_TxtField, VerificationCode);
            action.IClick(driver, Elements.ForgotPass_VerifyCode_Btn);
            Thread.Sleep(5000);
            action.IClick(driver, Elements.ForgotPass_Continue_Btn);
            Thread.Sleep(5000);
            test.validateString(driver, ErrorMessage1, util.GetText(driver, By.CssSelector("div[id='claimVerificationServerError']")).Replace("'", "").Trim());
        }
        
        [Test, Category("Registration"), Category("NonCore"), Retry(1)]
        [TestCase("en", TestName = "Verify Elements and Field validation in Reset Password page")]
        public void Registration_VerifyFieldValidationInResetPasswordPage(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/index.html");
            action.IClick(driver, Elements.MainNavigation_MyAnalogTab);
            action.IClick(driver, Elements.MyAnalogTab_LogInToMyAnalog_Btn);
            Thread.Sleep(3000);
            action.IClick(driver, Elements.Login_ForgotPassLink);
            action.IType(driver, Elements.ForgotPass_Email_TxtField, util.GetDataValue(Configuration.Environment, "Email_Address"));
            action.IClick(driver, Elements.ForgotPass_SendVerificationCode_Btn);
            ((IJavaScriptExecutor)driver).ExecuteScript("window.open()");
            driver.SwitchTo().Window(driver.WindowHandles.Last());
            action.ICheckMailinatorEmail(driver, util.GetDataValue(Configuration.Environment, "Email_Address"));
            action.IClick(driver, By.CssSelector("table[class='table-striped jambo_table'] * tr>td:nth-child(2)"));
            driver.SwitchTo().Frame(driver.FindElement(By.CssSelector("iframe[id='html_msg_body']")));
            string VerificationCode = util.GetText(driver, By.CssSelector("span[id='BodyPlaceholder_UserVerificationEmailBodySentence2']")).Substring(util.GetText(driver, By.CssSelector("span[id='BodyPlaceholder_UserVerificationEmailBodySentence2']")).Length - 6).Trim();
            driver.SwitchTo().Window(driver.WindowHandles.First());
            action.IType(driver, Elements.ForgotPass_VerifyEmail_TxtField, VerificationCode);
            action.IClick(driver, Elements.ForgotPass_VerifyCode_Btn);
            Thread.Sleep(5000);
            action.IClick(driver, Elements.ForgotPass_Continue_Btn);
            Thread.Sleep(5000);

            test.validateStringInstance(driver, driver.Url.ToLower(), "adi_passwordreset");
            test.validateElementIsPresent(driver, Elements.ResetPass_NewPass_TextField);
            test.validateElementIsPresent(driver, Elements.ResetPass_ConfirmNewPass_TextField);
            //remove due to changes in Al-17845
            //test.validateElementIsPresent(driver, By.CssSelector("div[class='error itemLevel']>p"));
            test.validateElementIsPresent(driver, Elements.ForgotPass_Continue_Btn);
            test.validateElementIsPresent(driver, Elements.ForgotPass_Cancel_Btn);
            action.IClick(driver, Elements.ForgotPass_Continue_Btn);
            Thread.Sleep(5000);
            test.validateString(driver, "A required field is missing. Please fill out all required fields and try again.", util.GetText(driver, By.CssSelector("div[id='requiredFieldMissing']")));
            action.IType(driver, Elements.ResetPass_NewPass_TextField, "Test_1234");
            action.IClick(driver, Elements.ForgotPass_Continue_Btn);
            test.validateString(driver, "The password entry fields do not match. Please enter the same password in both fields and try again.", util.GetText(driver, By.CssSelector("div[id='passwordEntryMismatch']")));
            action.IType(driver, Elements.ResetPass_ConfirmNewPass_TextField, "Test");
            Thread.Sleep(2000);
            test.validateElementIsPresent(driver, By.CssSelector("div[class='error itemLevel show']"));
            action.IDeleteValueOnFields(driver, Elements.ResetPass_ConfirmNewPass_TextField);
            action.IType(driver, Elements.ResetPass_ConfirmNewPass_TextField, "1234");
            Thread.Sleep(2000);
            test.validateElementIsPresent(driver, By.CssSelector("div[class='error itemLevel show']"));
            action.IDeleteValueOnFields(driver, Elements.ResetPass_ConfirmNewPass_TextField);
            action.IType(driver, Elements.ResetPass_ConfirmNewPass_TextField, "testtest");
            Thread.Sleep(2000);
            test.validateElementIsPresent(driver, By.CssSelector("div[class='error itemLevel show']"));
            action.IDeleteValueOnFields(driver, Elements.ResetPass_ConfirmNewPass_TextField);
            action.IType(driver, Elements.ResetPass_ConfirmNewPass_TextField, "TESTTEST");
            Thread.Sleep(2000);
            test.validateElementIsPresent(driver, By.CssSelector("div[class='error itemLevel show']"));
            action.IDeleteValueOnFields(driver, Elements.ResetPass_ConfirmNewPass_TextField);
            action.IType(driver, Elements.ResetPass_ConfirmNewPass_TextField, "Test_1234!!!!!!!!!!");
            Thread.Sleep(2000);
            test.validateElementIsPresent(driver, By.CssSelector("div[class='error itemLevel show']"));

        }

        [Test, Category("Registration"), Category("NonCore"), Retry(1)]
        [TestCase("en", TestName = "Verify user can reset his password successfully")]
        public void Registration_VerifyResetPasswordPage(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/index.html");
            action.IClick(driver, Elements.MainNavigation_MyAnalogTab);
            action.IClick(driver, Elements.MyAnalogTab_LogInToMyAnalog_Btn);
            Thread.Sleep(3000);
            action.IClick(driver, Elements.Login_ForgotPassLink);
            action.IType(driver, Elements.ForgotPass_Email_TxtField, util.GetDataValue(Configuration.Environment, "Email_Address"));
            action.IClick(driver, Elements.ForgotPass_SendVerificationCode_Btn);
            ((IJavaScriptExecutor)driver).ExecuteScript("window.open()");
            driver.SwitchTo().Window(driver.WindowHandles.Last());
            action.ICheckMailinatorEmail(driver, util.GetDataValue(Configuration.Environment, "Email_Address"));
            action.IClick(driver, By.CssSelector("table[class='table-striped jambo_table'] * tr>td:nth-child(2)"));
            driver.SwitchTo().Frame(driver.FindElement(By.CssSelector("iframe[id='html_msg_body']")));
            string VerificationCode = util.GetText(driver, By.CssSelector("span[id='BodyPlaceholder_UserVerificationEmailBodySentence2']")).Substring(util.GetText(driver, By.CssSelector("span[id='BodyPlaceholder_UserVerificationEmailBodySentence2']")).Length - 6).Trim();
            driver.SwitchTo().Window(driver.WindowHandles.First());
            action.IType(driver, Elements.ForgotPass_VerifyEmail_TxtField, VerificationCode);
            action.IClick(driver, Elements.ForgotPass_VerifyCode_Btn);
            Thread.Sleep(5000);
            action.IClick(driver, Elements.ForgotPass_Continue_Btn);
            Thread.Sleep(5000);
            
            string NewPassFromForgotPassword = util.RandomString();

            action.IType(driver, Elements.ResetPass_NewPass_TextField, NewPassFromForgotPassword);
            action.IType(driver, Elements.ResetPass_ConfirmNewPass_TextField, NewPassFromForgotPassword);
            action.IClick(driver, Elements.ForgotPass_Continue_Btn);
            Thread.Sleep(5000);
            test.validateStringInstance(driver, driver.Url, "analog.com/en/app");

            util.UpdateDBData("Old_Password", util.GetDataValue(Configuration.Environment, "Password"), Configuration.Environment);
            util.UpdateDBData("Password", NewPassFromForgotPassword, Configuration.Environment);

            action.IClick(driver, By.CssSelector("a[data-qa-element='link_myAnalog']"));
            Thread.Sleep(3000);
            action.IClick(driver, By.CssSelector("a[class='logout']"));

            action.Navigate(driver, Configuration.Env_Url + Locale + "/index.html");
            action.ILoginViaMyAnalogMegaMenu(driver, util.GetDataValue(Configuration.Environment, "Email_Address"), util.GetDataValue(Configuration.Environment, "Old_Password"));
            test.validateString(driver, "Invalid email or password", util.GetText(driver, By.CssSelector("div[class^='error pageLevel']>p")));

            string username2 = util.GetDataValue(Configuration.Environment, "Email_Address");
            string password2 = util.GetDataValue(Configuration.Environment, "Password");
            action.IDeleteValueOnFields(driver, Elements.UserTextField);
            action.IDeleteValueOnFields(driver, Elements.PwdTextField);

            action.ILogin(driver, username2, password2);
            if (util.CheckElement(driver, By.CssSelector("section[class='ccpa']"), 10))
            {
                action.IClick(driver, By.CssSelector("input[name='consent']"));
                action.IClick(driver, By.CssSelector("button[class='ma btn btn-primary']"));
                Thread.Sleep(5000);
            }
            test.validateElementIsPresent(driver, By.CssSelector("a[data-qa-element='link_myAnalog'] * img"));
            Thread.Sleep(2000);
            test.validateScreenByUrl(driver, Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/app");

        }

    }
}
    