﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System;
using System.Threading;
using OpenQA.Selenium;
using System.Text.RegularExpressions;

namespace AS_Test_Framework.Registration
{

    [TestFixture]
    public class Registration_OccupationPersonalization_VerifyComponent : BaseSetUp
    {
        public Registration_OccupationPersonalization_VerifyComponent() : base() { }

        [Test, Category("Registration"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify Registration's What's Your Occupation Personalization Page component is complete for EN Locale")]
        [TestCase("zh", TestName = "Verify Registration's What's Your Occupation Personalization Page component is complete for CN Locale")]
        [TestCase("jp", TestName = "Verify Registration's What's Your Occupation Personalization Page component is complete for JP Locale")]
        [TestCase("ru", TestName = "Verify Registration's What's Your Occupation Personalization Page component is complete for RU Locale")]
        public void Registration_ProductCategoriesPersonalizationPage_VerifyComponent(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/app/registration");
            Thread.Sleep(1000);
            action.IClick(driver, Elements.Registration_Next_Btn);
            action.IClick(driver, Elements.Registration_Next_Btn);
            Assert.Multiple(() =>
            {
                test.validateElementIsPresent(driver, By.CssSelector("section[class='registration'] * section[class='occupations']>div[class='section header']>h2>span"));
                test.validateElementIsPresent(driver, By.CssSelector("section[class='registration'] * section[class='occupations']>div[class='section header']>h2>div[class='help icon bottom']>i"));
                test.validateElementIsPresent(driver, By.CssSelector("section[class='registration'] * section[class='occupations']>div[class='large options grid']"));
                test.validateElementIsPresent(driver, Elements.Registration_Skip_Btn);
            });
        }

        [Test, Category("Registration"), Category("NonCore"), Retry(2)]
        [TestCase("en", "What type of ", TestName = "Verify Next button when there are selected Occupation for EN Locale")]
        [TestCase("zh", "哪类 ", TestName = "Verify Next button when there are selected Occupation for CN Locale")]
        [TestCase("jp", "職種の詳細を教えてください。", TestName = "Verify Next button when there are selected Occupation for JP Locale")]
        [TestCase("ru", "What type of ", TestName = "Verify Next button when there are selected Occupation for RU Locale")]
        public void Registration_OccupationPersonalizationPage_VerifyNextBtnWhenOccupationSelected(string Locale, string WhatTypeOfOccupation)
        {
            action.Navigate(driver, Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/app/registration");
            Thread.Sleep(1000);
            action.IClick(driver, Elements.Registration_Next_Btn);
            action.IClick(driver, Elements.Registration_Next_Btn);

            Random r = new Random();
            int Selected_Occupation = r.Next(1, util.GetCount(driver, By.CssSelector("section[class='registration'] section[class='occupations']>div[class='large options grid']>div[class='option']")));
            string Get_Occupation_Label = util.GetText(driver, By.CssSelector("section[class='registration'] section[class='occupations']>div[class='large options grid']>div[class='option']:nth-child(" + Selected_Occupation  + ")>label>span" ));

            action.IClick(driver, By.CssSelector("section[class='registration'] section[class='occupations']>div[class='large options grid']>div[class='option']:nth-child(" + Selected_Occupation + ")>label"));


            if (Selected_Occupation == 1 || Selected_Occupation == 2 || Selected_Occupation == 3)
            {
                if (Locale.Equals("zh") || Locale.Equals("jp"))
                {
                    test.validateStringIsCorrect(driver, By.CssSelector("section[class='registration'] * div[class='step flies in from right']>section[class='occupations']>div>h2>span"), WhatTypeOfOccupation + Get_Occupation_Label);
                }
                else
                {
                    test.validateStringIsCorrect(driver, By.CssSelector("section[class='registration'] * div[class='step flies in from right']>section[class='occupations']>div>h2>span"), WhatTypeOfOccupation + Get_Occupation_Label + "?");
                }
            }
            else {
                test.validateElementIsPresent(driver, By.CssSelector("div[class='verify email']"));
            }

            action.IClick(driver, Elements.Registration_Back_Btn);
            test.validateString(driver, "rgba(0, 159, 189, 1)", util.GetCssValue(driver, By.CssSelector("section[class='registration'] * div[class='step flies in from left']>section[class='occupations']>div[class='large options grid']>div[class='option']:nth-child(" + Selected_Occupation + ")>label"), "background-color"));

        }
    }
}
