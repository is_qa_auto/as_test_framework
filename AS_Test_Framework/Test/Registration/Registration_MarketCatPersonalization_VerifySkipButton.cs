﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System;
using System.Threading;
using OpenQA.Selenium;

namespace AS_Test_Framework.Registration
{

    [TestFixture]
    public class Registration_MarketCatPersonalization_VerifySkipButton : BaseSetUp
    {
        public Registration_MarketCatPersonalization_VerifySkipButton() : base() { }
        [Test, Category("Registration"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify Skip button when No selected Market Categories for EN Locale")]
        [TestCase("zh", TestName = "Verify Skip button when No selected Market Categories for CN Locale")]
        [TestCase("jp", TestName = "Verify Skip button when No selected Market Categories for JP Locale")]
        [TestCase("ru", TestName = "Verify Skip button when No selected Market Categories for RU Locale")]
        public void Registration_MarketCategoriesPersonalizationPage_VerifyNextBtnWhenNoMarketCatSelected(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/app/registration");
            Thread.Sleep(1000);
            action.IClick(driver, Elements.Registration_Next_Btn);
            action.IClick(driver, Elements.Registration_Skip_Btn);
            test.validateElementIsPresent(driver, By.CssSelector("div[class='verify email']"));
        }

        [Test, Category("Registration"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify Skip button when there are selected Market Categories for EN Locale")]
        [TestCase("zh", TestName = "Verify Skip button when there are selected Market Categories for CN Locale")]
        [TestCase("jp", TestName = "Verify Skip button when there are selected Market Categories for JP Locale")]
        [TestCase("ru", TestName = "Verify Skip button when there are selected Market Categories for RU Locale")]
        public void Registration_MarketCategoriesPersonalizationPage_VerifyNextBtnWhenMarketCatSelected(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/app/registration");
            Thread.Sleep(1000);
            action.IClick(driver, Elements.Registration_Next_Btn);
            action.IClick(driver, By.CssSelector("section[class='registration'] * section[class='markets']>div[class='options grid']>div[class='option']>label"));
            action.IClick(driver, Elements.Registration_Skip_Btn);
            test.validateElementIsPresent(driver, By.CssSelector("div[class='verify email']"));
            action.IClick(driver, Elements.Registration_Back_Btn);
            action.IClick(driver, Elements.Registration_Back_Btn);
            test.validateString(driver, "rgba(0, 159, 189, 1)", util.GetCssValue(driver, By.CssSelector("section[class='registration'] * section[class='markets']>div[class='options grid']>div[class='option']>label"), "background-color"));
        }

    }
}
