﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System;
using System.Threading;
using OpenQA.Selenium;
using System.Text.RegularExpressions;

namespace AS_Test_Framework.Registration
{

    [TestFixture]
    public class Registration_OccupationStudentPersonalization_VerifyComponent : BaseSetUp
    {
        public Registration_OccupationStudentPersonalization_VerifyComponent() : base() { }
        [Test, Category("Registration"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify when clicking Student options Occupation box for EN Locale")]
        [TestCase("zh", TestName = "Verify when clicking Student options Occupation box for CN Locale")]
        [TestCase("jp", TestName = "Verify when clicking Student options Occupation box for JP Locale")]
        [TestCase("ru", TestName = "Verify when clicking Student options Occupation box for RU Locale")]
        public void Registration_OccupationPersonalizationPage_VerifyNextBtnWhenOccupationSelectedIsStudent(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/app/registration");
            Thread.Sleep(1000);
            action.IClick(driver, Elements.Registration_Next_Btn);
            action.IClick(driver, Elements.Registration_Next_Btn);
            action.IClick(driver, By.CssSelector("section[class='registration'] section[class='occupations']>div[class='large options grid']>div[class='option']:nth-child(2)>label"));

            Random r = new Random();
            int Selected_Occupation = r.Next(1, util.GetCount(driver, By.CssSelector("section[class='registration'] div[class='step flies in from right']>section[class='occupations']>div[class='large options grid']>div[class='option']")));
            string Get_Occupation_Label = util.GetText(driver, By.CssSelector("section[class='registration'] div[class='step flies in from right']>section[class='occupations']>div[class='large options grid']>div[class='option']:nth-child(" + Selected_Occupation  + ")>label>span" ));

            action.IClick(driver, By.CssSelector("section[class='registration'] div[class='step flies in from right']>section[class='occupations']>div[class='large options grid']>div[class='option']:nth-child(" + Selected_Occupation + ")>label"));

            Assert.Multiple(() =>
            {

                test.validateElementIsPresent(driver, By.CssSelector("section[class='graduation year']"));
                test.validateStringIsCorrect(driver, By.CssSelector("section[class='graduation year']>div[class='select']>button>span"), "select");

                action.IClick(driver, By.CssSelector("section[class='graduation year']>div[class='select']>button>span"));

                string[] graduate_year = new string[util.GetCount(driver, By.CssSelector("section[class='graduation year']>div[class='select']>div[class='options open']>div"))];
                for (int x = 1, y = 0; util.CheckElement(driver, By.CssSelector("section[class='graduation year']>div[class='select']>div[class='options open']>div:nth-child(" + x + ")"),5); x++, y++)
                {
                    graduate_year[y] = util.GetText(driver, By.CssSelector("section[class='graduation year']>div[class='select']>div[class='options open']>div:nth-child(" + x + ")"));
                }

                test.validateStringIfInAscenading(graduate_year);
                string selected_year = util.GetText(driver, By.CssSelector("section[class='graduation year']>div[class='select']>div[class='options open']>div:nth-child(7)"));
                action.IClick(driver, By.CssSelector("section[class='graduation year']>div[class='select']>div[class='options open']>div:nth-child(7)"));
                test.validateStringIsCorrect(driver, By.CssSelector("section[class='graduation year']>div[class='select has value']>button>span"), selected_year);
                action.IClick(driver, Elements.Registration_Next_Btn);
                test.validateElementIsPresent(driver, By.CssSelector("div[class='verify email']"));
            });
        }
    }
}
