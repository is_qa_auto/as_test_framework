﻿/*********Test**************/
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System;
using System.Threading;
using OpenQA.Selenium;
using System.Text.RegularExpressions;

namespace AS_Test_Framework.Registration
{

    [TestFixture]
    public class Registration_VerifyBlockDomain : BaseSetUp
    {
        public Registration_VerifyBlockDomain() : base() { }

        [Test, Category("Registration"), Category("NonCore"), Retry(2)]
        [TestCase("en", "Test_Account@godaddy.ir", TestName = "Verify registration attempts from Iran domain")]
        [TestCase("en", "Test_Account@godaddy.sy", TestName = "Verify registration attempts from Syria domain")]
        [TestCase("en", "Test_Account@godaddy.cu", TestName = "Verify registration attempts from Cuba domain")]
        [TestCase("en", "Test_Account@godaddy.sd", TestName = "Verify registration attempts from Sudan domain")]
        [TestCase("en", "Test_Account@huawei.com", TestName = "Verify registration attempts from huawei domain")]
        [TestCase("en", "Test_Account@linshiyouxiang.net", TestName = "Verify registration attempts from linshiyou domain")]
        public void Registration_VerifyErrorMessage(string Locale, string Email)
        {
            string expectedMessage = "This email domain is blocked. Please use different domain×";
            action.Navigate(driver, Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/app/registration");
            Thread.Sleep(1000);
            action.IClick(driver, Elements.Registration_Skip_Btn);
            action.IType(driver, Elements.Registration_VerifyEmail_TxtField, Email);
            action.IType(driver, Elements.Registration_VerifyEmail_TxtField, Keys.Tab);
            test.validateStringIsCorrect(driver, Elements.Registration_BlockDomain_Message, expectedMessage);
        }

        [Test, Category("Registration"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that blocked country is available in Registration's country dropdown")]
        public void Registration_VerifyBlockCountry(string Locale)
        {
            string Email_Address = util.Generate_EmailAddress();

            string[] block_country = { "IRAN", "CUBA", "SYRIA", "SUDAN" };
            action.Navigate(driver, Configuration.Env_Url + Locale + "/gated/alliances/members-lounge.html");
            Thread.Sleep(1000);
            action.IClick(driver, Elements.LoginPage_Register_Link);
            action.GivenIAcceptCookies(driver);
            action.IVerifyEmailVerificationCode(driver, Email_Address);
            action.IClick(driver, Elements.Registration_Country_Dropdown);
            Assert.Multiple(() =>
            {
                test.validateElementIsPresent(driver, By.CssSelector("div[class='options open']>div[data-label='IRAN']"));
                test.validateElementIsPresent(driver, By.CssSelector("div[class='options open']>div[data-label='CUBA']"));
                test.validateElementIsPresent(driver, By.CssSelector("div[class='options open']>div[data-label='SYRIA']"));
                test.validateElementIsPresent(driver, By.CssSelector("div[class='options open']>div[data-label='SUDAN']"));
            });

            Random r = new Random();
            int Selected_Country = r.Next(0, block_country.Length-1);
            action.IClick(driver, By.CssSelector("div[class='options open']>div[data-label='" + block_country[Selected_Country] + "']"));

            /*****Select Province*****/
            action.IClick(driver, By.CssSelector("div[class='select required']>button"));
            action.IClick(driver, By.CssSelector("div[class='select required']>div[class='options open']>div:nth-child(1)"));

            action.IType(driver, Elements.Registration_FirstName_TxtField, "Test");
            action.IType(driver, Elements.Registration_LastName_TxtField, "Auto");
            action.IType(driver, Elements.Registration_Password_TxtField, "Test_1234");
            action.IType(driver, Elements.Registration_ConfirmPassword_TxtField, "Test_1234");
            action.IType(driver, Elements.Registration_City_TxtField, "Test_City");
            action.IType(driver, Elements.Registration_ZipCode_TxtField, "123456");
            action.IType(driver, Elements.Registration_CompName_TxtField, "Analog");
            action.IClick(driver, Elements.Registration_TermsOfUse_ChxBox);
            action.IClick(driver, Elements.Registration_Register_Btn);
            util.CheckElement(driver, Elements.LoginBtn, 15);
            test.validateStringInstance(driver, driver.Url, "b2clogin.com");

            action.ILogin(driver, Email_Address, "Test_1234");
            Thread.Sleep(2000);
            test.validateStringInstance(driver, driver.Url, "gated/alliances/members-lounge.html");

            action.IClick(driver, Elements.Cart_Icon);
            if (util.CheckElement(driver, Elements.SC_LoginLink, 5)) {
                action.IClick(driver, Elements.SC_LoginLink);
            }
            test.validateStringIsCorrect(driver, Elements.SC_ErrorMessage_Label, "Online purchasing and samples are not available for the country selected or your account. View Sales and Distributor map for other options.");
        }

    }
}
