﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;

namespace AS_Test_Framework.HomePage
{
    [TestFixture]
    public class HomePage_AnalogSignalsSection : BaseSetUp
    {
        public HomePage_AnalogSignalsSection() : base() { }

        //--- URLs ---//
        string home_page_url = "/index.html";

        [Test, Category("Home Page"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Analog Signals Section is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Analog Signals Section is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Analog Signals Section is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Analog Signals Section is Present and Working as Expected in RU Locale")]
        public void HomePage_VerifyAnalogSignalsSections(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + home_page_url);

            //----- R1 > T9: Verify Analog Signals section -----//

            //--- Expected Result: The Analog Signals section should appear properly below hero area. ---//
            test.validateElementIsPresent(driver, Elements.HomePage_AnalogSignals_Sec);

            if (util.CheckElement(driver, Elements.HomePage_AnalogSignals_Sec, 2))
            {
                //--- Expected Result: The card should only have max of 3 spots displayed in a form of carousel. ---//
                test.validateCountIsLessOrEqual(driver, util.GetCount(driver, Elements.HomePage_AnalogSignals_CardsSpot), 3);

                //--- Expected Result: Carousel item (cards) should contain the following: Type of material, Date, Title ---//
                test.validateElementIsPresent(driver, Elements.HomePage_AnalogSignals_Header_Txt);
                test.validateElementIsPresent(driver, Elements.HomePage_AnalogSignals_Date);
                test.validateElementIsPresent(driver, Elements.HomePage_AnalogSignals_Title);
            }
        }
    }
}