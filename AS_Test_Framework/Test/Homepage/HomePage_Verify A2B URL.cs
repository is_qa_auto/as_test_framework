﻿using AS_Test_Framework.Util;
using NUnit.Framework;
using System.Threading;

namespace AS_Test_Framework.HomePage
{
    [TestFixture]
    public class HomePage_A2BUrl : BaseSetUp
    {
        public HomePage_A2BUrl() : base() { }

        //--- URLs ---//
        string a2b_url = "/Gated/a2b/a2b-technology.html";
        string login_page_url = "b2clogin";

        [Test, Category("Home Page"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the A2B URL redirects to the Correct Page in EN Locale")]
        [TestCase("cn", TestName = "Verify that the A2B URL redirects to the Correct Page in CN Locale")]
        [TestCase("jp", TestName = "Verify that the A2B URL redirects to the Correct Page in JP Locale")]
        [TestCase("ru", TestName = "Verify that the A2B URL redirects to the Correct Page in RU Locale")]
        public void HomePage_VerifyA2BUrl(string Locale)
        {
            //----- R18 > T1: Verify that the page is redirected  to the myAnalog login page upon accessing A2B page when not logged in-----//

            //--- Action: Access A2B page ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + a2b_url);
            Thread.Sleep(500);

            //--- Expected Result: The page is redirected  to the myAnalog login page---//
            test.validateStringInstance(driver, driver.Url, login_page_url);
        }
    }
}