﻿using AS_Test_Framework.Util;
using NUnit.Framework;

namespace AS_Test_Framework.HomePage
{
    [TestFixture]
    public class HomePage_MyAnalogUrl : BaseSetUp
    {
        public HomePage_MyAnalogUrl() : base() { }

        //--- URLs ---//
        string myAnalog_url = "/myAnalog.html";
        string home_page_url = "/index.html";

        [Test, Category("Home Page"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the myAnalog URL redirects to the Correct Page in EN Locale")]
        [TestCase("cn", TestName = "Verify that the myAnalog URL redirects to the Correct Page in CN Locale")]
        [TestCase("jp", TestName = "Verify that the myAnalog URL redirects to the Correct Page in JP Locale")]
        [TestCase("ru", TestName = "Verify that the myAnalog URL redirects to the Correct Page in RU Locale")]
        public void HomePage_VerifyMyAnalogUrl(string Locale)
        {
            //----- R17: Homepage through accessing url www.analog.com/en/myAnalog.html -----//
            action.Navigate(driver, Configuration.Env_Url + Locale + myAnalog_url);

            //--- Expected Result: Page is redirected to Analog Home Page ---//
            test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + home_page_url);
        }
    }
}