﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.HomePage
{
    [TestFixture]
    public class HomePage_NewProductsSection : BaseSetUp
    {
        public HomePage_NewProductsSection() : base() { }

        //--- URLs ---//
        string home_page_url = "/index.html";
        string newProductsListing_page_url = "/products/landing-pages/new-products-listing.html";

        [Test, Category("Home Page"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the New Products Section is Present and Working as Expected in EN Locale", Category = "Smoke_ADIWeb")]
        [TestCase("cn", TestName = "Verify that the New Products Section is Present and Working as Expected in CN Locale", Category = "Smoke_ADIWeb")]
        [TestCase("jp", TestName = "Verify that the New Products Section is Present and Working as Expected in JP Locale", Category = "Smoke_ADIWeb")]
        [TestCase("ru", TestName = "Verify that the New Products Section is Present and Working as Expected in RU Locale")]
        public void HomePage_VerifyNewProductsSections(string Locale)
        {
            string initial_steps = "1. Go to " + Configuration.Env_Url + Locale + home_page_url;
            string scenario = "";
            action.INavigate(driver, Configuration.Env_Url + Locale + home_page_url);

            //----- R1 > T14: Verify the "NEW PRODUCTS" embedded code under hero carousel of analog homepage - A/B testing -----//

            //--- Expected Result: The New Products section should be present ---//
            scenario = "Verify that the new product section is present in homepage";
            test.validateElementIsPresentv2(driver, Elements.HomePage_NewProducts_Sec, scenario, initial_steps);

            scenario = "Verify that the new product section displays a maximum of 6 products (excluding the featured products)";
            test.validateCountIsEqualv2(driver, 6, util.GetCount(driver, By.CssSelector("div[class='new-products-component container'] li>a")),scenario, initial_steps);

            if (util.CheckElement(driver, Elements.HomePage_NewProducts_Sec, 2))
            {
                if (util.CheckElement(driver, By.CssSelector("div[class^='new-products']>ul>li"), 2))
                {
                    List<int> NewProducts_List = new List<int>();
                    Random rand = new Random();

                    int ctr = rand.Next(1, util.GetCount(driver, By.CssSelector("div[class^='new-products']>ul:nth-of-type(1)>li")));

                    string productNo = util.GetText(driver, By.CssSelector("div[class^='new-products']>ul:nth-of-type(1)>li:nth-child(" + ctr + ")>a>span"));
                    //Console.WriteLine("productNo = " + productNo);

                    //--- Expected Result: The product name in the URL displayed on the lower left part of the screen is in lower case ----//
                    //test.validateStringInstance(driver, util.ReturnAttribute(driver, By.CssSelector("div[class^='new-products']>ul:nth-of-type(1)>li:nth-child(" + ctr + ")>a"), "href"), "/" + productNo.ToLower() + ".html");

                    //--- Action: Click all 6 new products (one at a time). ---//
                    action.IClick(driver, By.CssSelector("div[class^='new-products']>ul:nth-of-type(1)>li:nth-child(" + ctr + ")>a"));
                    Thread.Sleep(3000);

                    //--- Expected Result: Corresponding product detail page of each products should be displayed. ----//
                    //--- Expected Result: The product name in the URL is in lower case ----//
                    scenario = "Verify that user will be redirected to correct product page when clicking \"" + productNo + "\" link in new product section";
                    test.validateScreenByUrlv2(driver, Configuration.Env_Url + Locale + "/products/" + productNo.ToLower() + ".html", scenario, initial_steps + "<br>2. Click \""+ productNo + "\" link in new product section");

                    //driver.Close();
                    //driver.SwitchTo().Window(driver.WindowHandles.First());
                    driver.Navigate().Back();

                }

                ////--- Expected Result: The "VIEW ALL NEW PRODUCTS" button is displayed ---//
                //test.validateElementIsPresent(driver, Elements.HomePage_ViewAllNewProducts_Btn);

                if (util.CheckElement(driver, Elements.HomePage_ViewAllNewProducts_Btn, 2))
                {
                    //--- Action: Click "VIEW ALL NEW PRODUCTS" button ---//
                    action.IClick(driver, Elements.HomePage_ViewAllNewProducts_Btn);

                    //--- Expected Result: "New Products Listing" page should be displayed. ---//
                    scenario = "Verify that user will be redirected to correct new product listing page when clicking \"View All New Products\" button in new product section";
                    test.validateScreenByUrlv2(driver, Configuration.Env_Url + Locale + newProductsListing_page_url, scenario, initial_steps + "<br>2. Click \"View All New Products\" button in new product section");

                    //driver.Close();
                    //driver.SwitchTo().Window(driver.WindowHandles.First());
                }
            }
        }
    }
}