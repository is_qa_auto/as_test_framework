﻿using AS_Test_Framework.Util;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace AS_Test_Framework.HomePage
{
    [TestFixture]
    public class HomePage_MapRoutes : BaseSetUp
    {
        public HomePage_MapRoutes() : base() { }

        //--- Test Data ---//
        string[] url_prefix = { "shoppingcart", "form" };
        string[] url_suffix = { "abc/ShoppingCartPage.aspx", "abc/form_pages/support/integrated/techsupport.aspx" };
        List<int> listNumbers = new List<int>();
        int ctr = 0;
        Random rand = new Random();

        [Test, Category("Home Page"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Non Core Page - Verify that the URL with Any Characters as Map Routes redirects to the Correct Page in EN Locale")]
        public void HomePage_VerifyMapRoutes(string Locale)
        {
            //----- R16 > T1: Verify that entering an invalid locale in URL would display a 404 page -----//

            ctr = rand.Next(0, url_prefix.Length);
            string url = Configuration.Env_Url.Replace("www", url_prefix[ctr]).Replace("cldnet", "corpnt") + url_suffix[ctr];
            //Console.WriteLine("URL = " + url);

            //--- Action: Access any non-core page and append any characters as map routes ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + url);

            //--- Expected Result: Error page for Error 404 should be displayed ---//
            test.validateStringInstance(driver, driver.Title, "Error Page");
        }
    }
}