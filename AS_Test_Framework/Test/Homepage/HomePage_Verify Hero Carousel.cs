﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Threading;

namespace AS_Test_Framework.HomePage
{
    [TestFixture]
    public class HomePage_HeroCarousel : BaseSetUp
    {
        public HomePage_HeroCarousel() : base() { }

        //--- URLs ---//
        string home_page_url = "/index.html";

        [Test, Category("Home Page"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Hero Carousel is Present and Working as Expected in EN Locale", Category = "Smoke_ADIWeb")]
        [TestCase("cn", TestName = "Verify that the Hero Carousel is Present and Working as Expected in CN Locale", Category = "Smoke_ADIWeb")]
        [TestCase("jp", TestName = "Verify that the Hero Carousel is Present and Working as Expected in JP Locale", Category = "Smoke_ADIWeb")]
        [TestCase("ru", TestName = "Verify that the Hero Carousel is Present and Working as Expected in RU Locale")]
        public void HomePage_VerifyHeroCarousel(string Locale)
        {
            action.INavigate(driver, Configuration.Env_Url + Locale + home_page_url);
            string initial_steps = "1. Go to " + Configuration.Env_Url + Locale + home_page_url;
            string scenario = "";

            //----- R1 > T8_1: Verify Hero content -----//

            //--- Expected Result: The hero images should display properly on the page in a form a rotating carousel. ---//
            scenario = "Verify that hero carousel is present in homepage";
            test.validateElementIsPresentv2(driver, Elements.HomePage_HeroCarousel, scenario, initial_steps);

            if (util.CheckElement(driver, Elements.HomePage_HeroCarousel, 2))
            {
                //----- R1 > T8_2: Verify Image carousel (IQ-4287/AL-11756) -----//

                //--- Expected Result: Each spot of carousel should have an image with textbox  that has the following contents: --//

                //--- Header ---//
                scenario = "Verify that hero carousel title is present";
                test.validateElementIsPresentv2(driver, Elements.HomePage_HeroCarousel_Header_Txt, scenario, initial_steps);
                
                /*****This is a content related*****/
                //--- Description ---//
                //if (!util.CheckElement(driver, Elements.HomePage_HeroCarousel_Description_Txt, 2))
                //{
                //    scenario = "Verify that hero carousel description is present";
                //    test.validateElementIsPresentv2(driver, By.CssSelector("div[id='carousel-main-home'] * div[class$='active'] * div[class='description rte visible-desktop']"), scenario, initial_steps);
                //}
                //else
                //{
                //    scenario = "Verify that hero carousel description is present";
                //    test.validateElementIsPresentv2(driver, Elements.HomePage_HeroCarousel_Description_Txt, scenario, initial_steps);
                //}

                //--- Learn More / Read Story link ---//
                scenario = "Verify that \"Learn More\" button is present in hero carousel";
                test.validateElementIsPresentv2(driver, Elements.HomePage_HeroCarousel_LearnMore_Link,scenario, initial_steps);

                /***this is content related****/
                //if (util.CheckElement(driver, Elements.HomePage_HeroCarousel_LearnMore_Link, 2))
                //{
                //    action.IClick(driver, Elements.HomePage_HeroCarousel_LearnMore_Link);
                    
                //    //--- Expected Result: All links should work across all locales and falls back to its repective locale. ----//
                //    test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + "/");
                //}
            }
        }
    }
}