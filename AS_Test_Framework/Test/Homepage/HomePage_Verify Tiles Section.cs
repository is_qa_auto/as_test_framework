﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;

namespace AS_Test_Framework.HomePage
{
    [TestFixture]
    public class HomePage_TilesSection : BaseSetUp
    {
        public HomePage_TilesSection() : base() { }

        //--- URLs ---//
        string home_page_url = "/index.html";

        [Test, Category("Home Page"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Tiles Section is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Tiles Section is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Tiles Section is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Tiles Section is Present and Working as Expected in RU Locale")]
        public void HomePage_VerifyTilesSections(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + home_page_url);

            //----- R1 > T37: Verify "Power by Linear" or "Download LTspice®" or "May 2019 Analog Dialogue: Analysis of Input Current Noise" (IQ-6981/ALA-11988) -----//

            if (util.CheckElement(driver, Elements.HomePage_Tiles_Sec, 2))
            {
                int article_count = util.GetCount(driver, By.CssSelector("section[class^='articles row']>article"));

                for (int ctr = 1; ctr <= article_count; ctr++)
                {
                    //--- Expected Result: Image is displayed ---//
                    test.validateElementIsPresent(driver, By.CssSelector("section[class^='articles row']>article:nth-of-type(" + ctr + ") * img[class='banner']"));
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.HomePage_Tiles_Sec);
            }
        }
    }
}