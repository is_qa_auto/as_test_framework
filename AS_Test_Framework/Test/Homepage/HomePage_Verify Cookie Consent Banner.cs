﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;

namespace AS_Test_Framework.HomePage
{
    [TestFixture]
    public class HomePage_CookieConsentBanner : BaseSetUp
    {
        public HomePage_CookieConsentBanner() : base() { }

        //--- URLs ---//
        string home_page_url = "/index.html";

        [Test, Category("Home Page"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Cookie Consent Banner is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Cookie Consent Banner is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Cookie Consent Banner is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Cookie Consent Banner is Present and Working as Expected in RU Locale")]
        public void HomePage_VerifyCookieConsentBanners(string Locale)
        {
            //----- R26 > T1: Verify if the myHistory tracking is disabled if the user decline the cookies. (IQ-8934/AL-16011) -----//

            driver.Manage().Cookies.DeleteAllCookies();
            driver.Manage().Window.Maximize();
            if (Configuration.Environment.Equals("production"))
            {
                driver.Navigate().GoToUrl("https://analogb2c.b2clogin.com/analogb2c.onmicrosoft.com/oauth2/v2.0/logout?p=B2C_1A_ADI_SignUpOrSignInWithKmsi&post_logout_redirect_uri=" + Configuration.Env_Url);
            }
            else
            {
                driver.Navigate().GoToUrl("https://analogb2c" + Configuration.Environment + ".b2clogin.com/analogb2c" + Configuration.Environment + ".onmicrosoft.com/oauth2/v2.0/logout?p=B2C_1A_ADI_SignUpOrSignInWithKmsi&post_logout_redirect_uri=" + Configuration.Env_Url);
            }

            driver.Navigate().GoToUrl(Configuration.Env_Url + Locale + home_page_url);

            if (util.CheckElement(driver, Elements.CookieConsent_Banner, 2))
            {
                //--- Action: Click cookies detail link in the banner ---//
                action.IClick(driver, Elements.GDPR_Cookie_Details_Link);

                //--- Expected Result: The cookie banner expand. ---//
                test.validateElementIsPresent(driver, Elements.CookieConsent_Banner_Expanded);

                //--- Action: Click Decline cookies ---//
                action.IClick(driver, Elements.GDPR_Privacy_Decline_Link);

                //--- Expected Result: Cookie banner closed ---//
                test.validateElementIsNotPresent(driver, Elements.CookieConsent_Banner);

                //--- Action: Click myHistory tab. ---//
                action.IClick(driver, Elements.MainNavigation_MyHistoryTab);

                //--- Expected Result: Product view section empty ---//
                test.validateElementIsNotPresent(driver, By.CssSelector("div[class='myhistory menu content expanded'] * div[class='row']>div:nth-of-type(1) * a"));
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.CookieConsent_Banner);
            }
        }
    }
}