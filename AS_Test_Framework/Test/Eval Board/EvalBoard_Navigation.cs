﻿using AS_Test_Framework.Util;
using NUnit.Framework;

namespace AS_Test_Framework.EvalBoard
{
    [TestFixture]
    public class EvalBoard_Navigation : BaseSetUp
    {
        public EvalBoard_Navigation() : base() { }

        //--- URLs ---//
        string evalBoard_short_url = "/EVAL-ADIS";
        string evalBoard_url_format = "/design-center/evaluation-hardware-and-software/evaluation-boards-kits";

        [Test, Category("EvalBoard"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Short URL is Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Short URL is Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Short URL is Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Short URL is Working as Expected in RU Locale")]
        public void EvalBoard_Navigation_VerifyShortUrl(string Locale)
        {
            //----- R1 > T11: Verify Short URL behavior for EVAL Board/LTC eval (IQ-4398/AL-11135) -----//

            //--- Action: Go eval board that is part of the short URL ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + evalBoard_short_url);

            //--- Expected Result: The following should redirect to eval board/ Eval board LTC page (long URL) ---//
            test.validateStringInstance(driver, driver.Url, "/" + Locale + evalBoard_url_format + evalBoard_short_url + ".html");
        }

        [Test, Category("EvalBoard"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the URL without html is Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the URL without html is Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the URL without html is Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the URL without html is Working as Expected in RU Locale")]
        public void EvalBoard_Navigation_VerifyUrlWithoutHtml(string Locale)
        {
            //----- R1 > T15: Verify access to page using URL without ".html" (IQ-8406/AUAT-1202) -----//

            //--- Action: Enter the URL in the address bar ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + evalBoard_url_format + evalBoard_short_url);

            //--- Expected Result: Page redirects toeval board page (long URL) ---//
            test.validateStringInstance(driver, driver.Url, "/" + Locale + evalBoard_url_format + evalBoard_short_url + ".html");
        }
    }
}