﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;

namespace AS_Test_Framework.EvalBoard
{
    [TestFixture]
    public class EvalBoard_Overview_Tabs : BaseSetUp
    {
        public EvalBoard_Overview_Tabs() : base() { }

        //--- URLs ---//
        string evalBoard_url = "/EVAL-ADIS";

        //--- Labels ---//
        string overview_txt = "Overview";

        [Test, Category("EvalBoard"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Features and Benefits is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Features and Benefits is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Features and Benefits is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Features and Benefits is Present and Working as Expected in RU Locale")]
        public void EvalBoard_Overview_VerifyFeaturesAndBenefits(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + evalBoard_url);

            //----- R6 > T1: Verify Overview header title (AL-10991) -----//

            //--- Expected Result: The header title "Overview" must be displayed ---//
            if (Locale.Equals("en"))
            {
                test.validateStringInstance(driver, overview_txt, util.GetText(driver, Elements.EvalBoard_Overview_Section_Lbl));
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.EvalBoard_Overview_Section_Lbl);
            }

            if (util.CheckElement(driver, Elements.EvalBoard_Overview_FeaturesAndBenefits_Link, 2))
            {
                //----- R6 > T8: Verify the behavior of tabs under overview section (AL-10537) -----//

                //--- Expected Result: Features and benefits is selected ---//
                test.validateElementIsPresent(driver, Elements.EvalBoard_Overview_Selected_FeaturesAndBenefits_Tab);

                if (!util.CheckElement(driver, Elements.EvalBoard_Overview_Selected_FeaturesAndBenefits_Tab, 2))
                {
                    action.IClick(driver, Elements.EvalBoard_Overview_FeaturesAndBenefits_Link);
                }

                //----- R6 > T8: Verify the behavior of tabs under overview section (AL-10537) -----//

                //--- Expected Result: Features and benefits is  with active color #1e1e1e. ---//
                test.validateColorIsCorrect(driver, util.GetCssValue(driver, Elements.EvalBoard_Overview_Selected_FeaturesAndBenefits_Tab, "text-decoration-color"), "#1E1E1E");

                //--- Expected Result: Details/information under "Features & Benefits" should be displayed ---//
                test.validateElementIsPresent(driver, Elements.EvalBoard_Overview_FeaturesAndBenefits_Content);
            }

            //----- R6 > T3: Verify Product Details -----//

            //--- Expected Result: "Product Details" should be clickable. ---//
            test.validateElementIsEnabled(driver, Elements.EvalBoard_Overview_ProductDetails_Link);
        }

        [Test, Category("EvalBoard"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Product Details is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Product Details is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Product Details is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Product Details is Present and Working as Expected in RU Locale")]
        public void EvalBoard_Overview_VerifyProductDetails(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + evalBoard_url);

            if (util.CheckElement(driver, Elements.EvalBoard_Overview_ProductDetails_Link, 2))
            {
                //----- R6 > T8: Verify the behavior of tabs under overview section (AL-10537) -----//

                //--- Action: Hover on Product details tab beside Featured and benefits ---//
                action.IMouseOverTo(driver, Elements.EvalBoard_Overview_ProductDetails_Link);

                //--- Expected Result: Product details tab should be in hover state color #009fbd ---//
                test.validateColorIsCorrect(driver, util.GetCssValue(driver, Elements.EvalBoard_Overview_ProductDetails_Link, "text-decoration-color"), "#009FBD");

                if (!util.CheckElement(driver, By.CssSelector("li[class='active']>a[href$='product-details']"), 2))
                {
                    action.IClick(driver, Elements.EvalBoard_Overview_ProductDetails_Link);
                }

                //----- R6 > T3: Verify Product Details -----//

                //--- Expected Result: Details/information under "Product Details" should be displayed ---//
                if (!util.CheckElement(driver, Elements.EvalBoard_Overview_ProductDetails_Link, 2))
                {
                    test.validateElementIsPresent(driver, Elements.EvalBoard_Overview_ProductDetails_Content);
                }
                else
                {
                    test.validateElementIsPresent(driver, By.CssSelector("div[id$='product-details']"));
                }
            }

            //----- R6 > T2: Verify Features & Benefits -----//

            //--- Expected Result: "Features & Benefits" should be clickable.. ---//
            test.validateElementIsEnabled(driver, Elements.EvalBoard_Overview_FeaturesAndBenefits_Link);

        }
    }
}