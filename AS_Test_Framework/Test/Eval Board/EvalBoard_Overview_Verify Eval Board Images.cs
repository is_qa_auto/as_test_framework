﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Threading;

namespace AS_Test_Framework.EvalBoard
{
    [TestFixture]
    public class EvalBoard_Overview_EvalBoardImages : BaseSetUp
    {
        public EvalBoard_Overview_EvalBoardImages() : base() { }

        //--- URLs ---//
        string evalBoard_url = "/EVAL-ADuM5411";

        [Test, Category("EvalBoard"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Eval Board Images are Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Eval Board Images are Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Eval Board Images are Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Eval Board Images are Present and Working as Expected in RU Locale")]
        public void EvalBoard_Overview_VerifyEvalBoardImages(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + evalBoard_url);

            //----- R5 > T1: Verify Eval board images -----//

            //--- Expected Result: Eval board images should be displayed. ---//
            test.validateElementIsPresent(driver, Elements.EvalBoard_Overview_Image);
            test.validateElementIsPresent(driver, Elements.EvalBoard_Overview_Thumbnail_Image);

            if (util.CheckElement(driver, Elements.EvalBoard_Overview_Thumbnail_Image, 2))
            {
                //----- R5 > T3: Verify user can select any thumbnail images -----//

                //--- Action: Click on any thumbnail images ---//
                action.IClick(driver, By.CssSelector("div[id='previewProductsThumnails']>div:nth-last-of-type(1) * img"));
                Thread.Sleep(2000);

                //--- Expected Result: A larger size of selected thumbnail image should be displayed ---//
                test.validateElementIsPresent(driver, Elements.EvalBoard_Overview_Image);
            }

            if (util.CheckElement(driver, Elements.EvalBoard_Overview_Image, 2))
            {
                //----- R5 > T4: Verify Zoom In icon on selected thumbnail image -----//

                //--- Expected Result: The zoom in icon should be displayed for the selected image ---//
                test.validateElementIsPresent(driver, Elements.EvalBoard_Overview_ZoomIn_Icon);

                //--- Action: Click Zoom In icon ---//
                action.IClick(driver, Elements.EvalBoard_Overview_ZoomIn_Icon);
                Thread.Sleep(1000);

                //--- Expected Result: A pop up screen should be shown ---//
                test.validateElementIsPresent(driver, Elements.EvalBoard_Overview_PopUp);

                if (util.CheckElement(driver, Elements.EvalBoard_Overview_PopUp, 2))
                {
                    //--- Expected Result: A pop up screen should be shown displaying the selected image ---//
                    test.validateElementIsPresent(driver, Elements.EvalBoard_Overview_PopUp_Img);

                    //----- R5 > T5: Verify Download Icon on the selected image -----//

                    //--- Expected Result: Download Icon must be displayed in the pop up image ---//
                    test.validateElementIsPresent(driver, Elements.EvalBoard_Overview_PopUp_Download_Icon);

                    //----- R5 > T6: Verify Print Icon on the selected image -----//

                    //--- Expected Result: Print Icon must be displayed in the pop up image ---//
                    test.validateElementIsPresent(driver, Elements.EvalBoard_Overview_PopUp_Print_Icon);

                    if (util.CheckElement(driver, Elements.EvalBoard_Overview_PopUp_Pagination, 2))
                    {
                        //----- R5 > T8: Verify the carousel to use OWL carousel. (IQ-7141/AL-13685) -----//

                        //--- Action: Click on the slider button (arrow mark) or the dots exists in the pop up ---//
                        action.IClick(driver, By.CssSelector("div[id^='product-modal'] * div[class='modal-content'] * div[class='owl-dots']>button:nth-last-of-type(1)"));

                        //--- Expected Result: image change accordingly ---//
                        test.validateElementIsPresent(driver, By.CssSelector("div[id^='product-modal'] * div[class='modal-content'] * div[class='owl-dots']>button:nth-last-of-type(1)[class$='active']"));
                    }

                    //----- R5 > T7: Verify Close button on the selected image -----//

                    //--- Expected Result: Close button must be displayed in the pop up image ---//
                    test.validateElementIsPresent(driver, Elements.EvalBoard_Overview_PopUp_X_Icon);

                    if (util.CheckElement(driver, Elements.EvalBoard_Overview_PopUp_X_Icon, 2))
                    {
                        //--- Action: Click on Close button (X) ---//
                        action.IClick(driver, Elements.EvalBoard_Overview_PopUp_X_Icon);

                        //--- Expected Result: Pop up image must be closed ---//
                        test.validateElementIsNotPresent(driver, Elements.EvalBoard_Overview_PopUp);
                    }
                }
            }
        }
    }
}