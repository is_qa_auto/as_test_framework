﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;

namespace AS_Test_Framework.EvalBoard
{
    [TestFixture]
    public class EvalBoard_SystemRequirements_HeaderTitle : BaseSetUp
    {
        public EvalBoard_SystemRequirements_HeaderTitle() : base() { }

        //--- URLs ---//
        string evalBoard_url = "/EVAL-ADIS";

        //--- Labels ---//
        string systemRequirements_txt = "System Requirements";

        [Test, Category("EvalBoard"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the System Requirements Header Title is Present and Correct in EN Locale")]
        [TestCase("cn", TestName = "Verify that the System Requirements Header Title is Present in CN Locale")]
        [TestCase("jp", TestName = "Verify that the System Requirements Header Title is Present in JP Locale")]
        [TestCase("ru", TestName = "Verify that the System Requirements Header Title is Present in RU Locale")]
        public void EvalBoard_SystemRequirements_VerifyHeaderTitle(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + evalBoard_url);

            //----- R11 > T1: Verify System Requirements Title -----//

            //--- Expected Result: The header title "Getting Started" must be displayed ---//
            if (Locale.Equals("en"))
            {
                test.validateStringInstance(driver, util.GetText(driver, Elements.EvalBoard_SystemRequirements_Sec_Lbl), systemRequirements_txt);
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.EvalBoard_SystemRequirements_Sec_Lbl);
            }

            //----- R11 > T2: Verify System Requirements Conten -----//

            //--- Expected Result: System Requirements text/contents should be present ---//
            test.validateElementIsPresent(driver, Elements.EvalBoard_SystemRequirements_Sec_Content);
        }
    }
}