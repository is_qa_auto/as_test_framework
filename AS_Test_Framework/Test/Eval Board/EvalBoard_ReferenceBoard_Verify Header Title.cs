﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.EvalBoard
{
    [TestFixture]
    public class EvalBoard_ReferenceBoard_Section : BaseSetUp
    {
        public EvalBoard_ReferenceBoard_Section() : base() { }

        //--- URLs ---//
        string evalBoard_url = "/EVAL-AD-FMCOMMS6-EBZ";
        string pdp_url_format = "/products/";
        string productCategory_page_url_format = "/products/";

        //--- Labels ---//
        string referenceBoard_txt = "Reference Board";

        [Test, Category("EvalBoard"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Reference Board Section is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Reference Board Section is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Reference Board Section is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Reference Board Section is Present and Working as Expected in RU Locale")]
        public void EvalBoard_ReferenceBoard_VerifySection(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + evalBoard_url);

            //----- R13 > T1: Verify Reference board Title (ACM-2776) -----//

            //--- Expected Result: The header title "Reference Board" must be displayed ---//
            if (Locale.Equals("en"))
            {
                test.validateStringInstance(driver, util.GetText(driver, Elements.EvalBoard_ReferenceBoard_Sec_Lbl), referenceBoard_txt);
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.EvalBoard_ReferenceBoard_Sec_Lbl);
            }

            //----- R13 > T2: Verify Reference board Content -----//

            //--- Expected Result: Reference Board Title should be displayed. ---//
            test.validateElementIsPresent(driver, Elements.EvalBoard_ReferenceBoard_Title_Txt);

            //--- Expected Result: Product Categories should be displayed. ---//
            test.validateElementIsPresent(driver, Elements.EvalBoard_ReferenceBoard_ProductCategories);

            //--- Expected Result: Reference Board Image should be displayed. ---//
            test.validateElementIsPresent(driver, Elements.EvalBoard_ReferenceBoard_Img);

            if (util.CheckElement(driver, Elements.EvalBoard_ReferenceBoard_Pins, 2))
            {
                //--- Action: Hover over the colored pins ---//
                action.IMouseOverTo(driver, Elements.EvalBoard_ReferenceBoard_Pins);
                Thread.Sleep(1000);

                //--- Expected Result: Product Category link should be displayed. ---//
                test.validateElementIsPresent(driver, Elements.EvalBoard_ReferenceBoard_Pin_ToolTip_ProductCategory_Link);

                //--- Expected Result: Part Number link should be displayed. ---//
                test.validateElementIsPresent(driver, Elements.EvalBoard_ReferenceBoard_Pin_ToolTip_PartNo_Link);

                //--- Expected Result: Short description should be displayed. ---//
                test.validateElementIsPresent(driver, Elements.EvalBoard_ReferenceBoard_Pin_ToolTip_ShortDesc_Txt);

                if (util.CheckElement(driver, Elements.EvalBoard_ReferenceBoard_Pin_ToolTip_ProductCategory_Link, 2))
                {
                    //--- Action: Click the product category link on the tool tip ---//
                    action.IOpenLinkInNewTab(driver, Elements.EvalBoard_ReferenceBoard_Pin_ToolTip_ProductCategory_Link);
                    Thread.Sleep(4000);

                    //--- Expected Result: The product category page should be displayed ---//
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + productCategory_page_url_format);

                    driver.Close();
                    driver.SwitchTo().Window(driver.WindowHandles.First());
                }

                if (util.CheckElement(driver, Elements.EvalBoard_ReferenceBoard_Pin_ToolTip_PartNo_Link, 2))
                {
                    //--- Action: Click the part number link on the tool tip ---//
                    action.IOpenLinkInNewTab(driver, Elements.EvalBoard_ReferenceBoard_Pin_ToolTip_PartNo_Link);
                    Thread.Sleep(4000);

                    //--- Expected Result: The Part Number detail page should be displayed ---//
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + pdp_url_format);
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.EvalBoard_ReferenceBoard_Pins);
            }
        }
    }
}