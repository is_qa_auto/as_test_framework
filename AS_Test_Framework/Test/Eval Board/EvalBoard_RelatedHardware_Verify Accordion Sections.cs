﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
//using System.Linq;
using System.Text.RegularExpressions;
//using System.Threading;

namespace AS_Test_Framework.EvalBoard
{
    [TestFixture]
    public class EvalBoard_RelatedHardware_AccordionSections : BaseSetUp
    {
        public EvalBoard_RelatedHardware_AccordionSections() : base() { }

        //--- URLs ---//
        string evalBoard_url = "/sdp-b";
        //string pdp_url_format = "/products/";

        [Test, Category("EvalBoard"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Accordion Section is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Accordion Section is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Accordion Section is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Accordion Section is Present and Working as Expected in RU Locale")]
        public void EvalBoard_RelatedHardware_VerifyAccordionSection(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + evalBoard_url);

            if (util.CheckElement(driver, Elements.EvalBoard_RelatedHardware_Accordion_Sec, 2))
            {
                //----- R9 > T3: Verify the Accordion Sections under Related Hardware -----//

                //--- Expected Result: The Accordions should all be Collapsed by Default. ---//
                test.validateElementIsPresent(driver, Elements.EvalBoard_RelatedHardware_Collpased_Accordion);

                if (util.CheckElement(driver, Elements.EvalBoard_RelatedHardware_Collpased_Accordion, 2))
                {
                    //--- Expected Result: The Header of the Accordion is Present. ---//
                    test.validateElementIsPresent(driver, Elements.EvalBoard_RelatedHardware_Collpased_Accordion_Header_Txt);

                    if (util.CheckElement(driver, Elements.EvalBoard_RelatedHardware_Collpased_Accordion_Header_Txt, 2))
                    {
                        //--- Expected Result: There's a Counter displayed beside Accordion Header enclosed by parenthesis showing the Total Number of available Related Hardwares under the Section. ---//
                        test.validateStringInstance(driver, util.GetCount(driver, Elements.EvalBoard_RelatedHardware_Collpased_Accordion_Product_Links).ToString(), Regex.Match(util.GetText(driver, Elements.EvalBoard_RelatedHardware_Collpased_Accordion_Header_Txt), @"\d+").Value);
                    }

                    //--- Expected Result: The Accordion Button is Present and is Facing Down. ---//
                    test.validateElementIsPresent(driver, Elements.EvalBoard_RelatedHardware_Expand_Accordion_Btn);

                    if (util.CheckElement(driver, Elements.EvalBoard_RelatedHardware_Expand_Accordion_Btn, 2))
                    {
                        //--- Action: Click on the Accordion Button ---//
                        action.IClick(driver, Elements.EvalBoard_RelatedHardware_Expand_Accordion_Btn);

                        //--- Expected Result: The Accordion will Expand. ---//
                        test.validateElementIsPresent(driver, Elements.EvalBoard_RelatedHardware_Expanded_Accordion);

                        if (util.CheckElement(driver, Elements.EvalBoard_RelatedHardware_Expanded_Accordion, 2))
                        {
                            //--- Expected Result: The Accordion Button will Face Up. ---//
                            test.validateElementIsPresent(driver, Elements.EvalBoard_RelatedHardware_Collapse_Accordion_Btn);

                            //--- Expected Result: Each will have a Link. ---//
                            test.validateElementIsPresent(driver, Elements.EvalBoard_RelatedHardware_Expanded_Accordion_Links);

                            //--- Expected Result: Each will have a Description. ---//
                            test.validateElementIsPresent(driver, Elements.EvalBoard_RelatedHardware_Expanded_Accordion_Desc_Txt);

                            //if (util.CheckElement(driver, Elements.EvalBoard_RelatedHardware_Expanded_Accordion_Links, 2))
                            //{
                            //    //--- Action: Click on Each Product Link ---//
                            //    action.IOpenLinkInNewTab(driver, Elements.EvalBoard_RelatedHardware_Expanded_Accordion_Links);
                
                            //    //--- Expected Result: The Page will redirect to the Correct Pages. ---//
                            //    test.validateStringInstance(driver, driver.Url, pdp_url_format);

                            //    driver.Close();
                            //    driver.SwitchTo().Window(driver.WindowHandles.First());
                            //}

                            if (util.CheckElement(driver, Elements.EvalBoard_RelatedHardware_Collapse_Accordion_Btn, 2))
                            {
                                //--- Action: Click on the Accordion Button again ---//
                                action.IClick(driver, Elements.EvalBoard_RelatedHardware_Collapse_Accordion_Btn);

                                //--- Expected Result: The Accordion will Collapse ---//
                                test.validateElementIsPresent(driver, Elements.EvalBoard_RelatedHardware_Collpased_Accordion);

                                //--- Expected Result: The Accordion Button will Face Down. ---//
                                test.validateElementIsPresent(driver, Elements.EvalBoard_RelatedHardware_Expand_Accordion_Btn);
                            }
                        }
                    }
                }
            }
        }
    }
}