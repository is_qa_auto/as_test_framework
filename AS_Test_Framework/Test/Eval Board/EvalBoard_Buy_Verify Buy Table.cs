﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Threading;

namespace AS_Test_Framework.EvalBoard
{
    [TestFixture]
    public class EvalBoard_Buy_BuyTable : BaseSetUp
    {
        public EvalBoard_Buy_BuyTable() : base() { }

        //--- Login Credentials ---//
        string username = "myanalog_t3st_333@mailinator.com";
        string password = "Test_1234";

        //--- URLs ---//
        string evalBoard_url = "/EVAL-ADuM5411";

        //--- Labels ---//
        string buy_txt = "Buy";
        string[] buy_col_headers = { "Model", "Description", "Price", "RoHS" };
        string country_selected = "United States";
        string selectACountry_dropdown_default_value = "Select a country";

        [Test, Category("EvalBoard"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Buy Table is Present and Working as Expected when the User is Logged Out in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Buy Table is Present and Working as Expected when the User is Logged Out in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Buy Table is Present and Working as Expected when the User is Logged Out in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Buy Table is Present and Working as Expected when the User is Logged Out in RU Locale")]
        public void EvalBoard_Buy_VerifyBuyTableWhenUserIsLoggedOut(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + evalBoard_url);

            //----- R14 > T1: Verify the Buy title -----//

            //--- Expected Result: The header title "Buy" must be displayed ---//
            if (Locale.Equals("en"))
            {
                test.validateStringInstance(driver, util.GetText(driver, Elements.EvalBoard_Buy_Section_Lbl), buy_txt);
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.EvalBoard_Buy_Section_Lbl);
            }

            //----- R14 > T2: Verify Buy Online Table on Buy Section  -----//

            //--- Expected Result: The message must be shown. ---//
            test.validateElementIsPresent(driver, Elements.EvalBoard_Buy_Msg_Txt);

            if (util.CheckElement(driver, Elements.EvalBoard_Buy_Tbl, 2))
            {
                for (int locator_ctr = 1, label_ctr = 0; locator_ctr <= buy_col_headers.Length; locator_ctr++, label_ctr++)
                {
                    //----- R14 > T2: Verify Buy Online Table on Buy Section  -----//

                    //--- Expected Result: The Buy online table should have the following content: Model | Description | Price | RoHS | ---//
                    if (Locale.Equals("en"))
                    {
                        test.validateStringIsCorrect(driver, By.CssSelector("section[id$='buy'] * div[class$='evaluation table-responsive sample-table']>table>thead>tr>th:nth-of-type(" + locator_ctr + ")"), buy_col_headers[label_ctr]);
                    }
                    else
                    {
                        test.validateElementIsPresent(driver, By.CssSelector("section[id$='buy'] * div[class$='evaluation table-responsive sample-table']>table>thead>tr>th:nth-of-type(" + locator_ctr + ")"));
                    }

                    //--- Expected Result: Value in RoHS column must either be "Yes" or "No" (IQ-8342/AL-15602) ---//
                    if (Locale.Equals("en"))
                    {
                        if (util.GetText(driver, Elements.EvalBoard_Buy_Tbl_Rohs_Col_Val).Equals("Yes"))
                        {
                            test.validateStringIsCorrect(driver, Elements.EvalBoard_Buy_Tbl_Rohs_Col_Val, "Yes");
                        }
                        else
                        {
                            test.validateStringIsCorrect(driver, Elements.EvalBoard_Buy_Tbl_Rohs_Col_Val, "No");
                        }
                    }
                    else
                    {
                        test.validateElementIsPresent(driver, Elements.EvalBoard_Buy_Tbl_Rohs_Col_Val);
                    }
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.EvalBoard_Buy_Tbl);
            }

            //----- R14 > T3: Verify Back, Select a Country and Check Inventory button behavior on Buy Section -----//

            //--- Expected Result: The Back button should be disabled on page load. ---//
            test.validateElementIsPresent(driver, Elements.EvalBoard_Buy_Disabled_Back_Btn);

            //--- Expected Result: The  Check Inventory button should be disabled on page load. ---//
            test.validateElementIsPresent(driver, Elements.EvalBoard_Buy_Disabled_CheckInventory_Btn);

            //--- Expected Result: 'Select a Country' should be a default selection in Select a Country dropdown on page load. ---//
            if (Locale.Equals("en"))
            {
                test.validateString(driver, util.GetText(driver, Elements.EvalBoard_Buy_SelectACountry_Dd_Selected_Val), selectACountry_dropdown_default_value);
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.EvalBoard_Buy_SelectACountry_Dd);
            }

            if (util.CheckElement(driver, Elements.EvalBoard_Buy_SelectACountry_Dd, 2))
            {
                //--- Action: Click Select a Country Dropdown ---//
                action.IClick(driver, Elements.EvalBoard_Buy_SelectACountry_Dd_Btn);

                //--- Expected Result: Should be able to see the lists of all countries. ---//
                test.validateElementIsPresent(driver, Elements.EvalBoard_Buy_SelectACountry_Dd_Open);

                //--- Action: Select a country from Select a Country dropdown ---//
                string country_txt = util.GetText(driver, By.CssSelector("section[id$='buy'] * div>div:nth-of-type(1) * div[class$='country-filter'] * a[data-id='VN']"));
                action.IClick(driver, By.CssSelector("section[id$='buy'] * div>div:nth-of-type(1) * div[class$='country-filter'] * a[data-id='VN']"));

                //--- Expected Result: Country should be selected ---//
                test.validateStringIsCorrect(driver, Elements.EvalBoard_Buy_SelectACountry_Dd_Selected_Val, country_txt);
            }
        }

        [Test, Category("EvalBoard"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Buy Table is Present and Working as Expected when the User is Logged In in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Buy Table is Present and Working as Expected when the User is Logged In in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Buy Table is Present and Working as Expected when the User is Logged In in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Buy Table is Present and Working as Expected when the User is Logged In in RU Locale")]
        public void EvalBoard_Buy_VerifyBuyTableWhenUserIsLoggedIn(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + evalBoard_url);

            if (util.CheckElement(driver, Elements.MyAnalog_Widget_Btn, 2))
            {
                action.ILogInViaMyAnalogWidget(driver, username, password);

                //----- R14 > T4.2: Verify the country selected if user is Logged-in (AL-9209) -----//

                //--- Expected Result: The country selected should be the same as the country registered in the user's account ---//
                test.validateStringIsCorrect(driver, Elements.EvalBoard_Buy_SelectACountry_Dd_Selected_Val, country_selected);

                if (!util.GetText(driver, Elements.EvalBoard_Buy_SelectACountry_Dd_Selected_Val).Equals(selectACountry_dropdown_default_value))
                {
                    if (util.CheckElement(driver, Elements.EvalBoard_Buy_CheckInventory_Btn, 2))
                    {
                        //--- Action: Click on Check Inventory button ---//
                        action.IClick(driver, Elements.EvalBoard_Buy_CheckInventory_Btn);
                        Thread.Sleep(1000);

                        //--- Expected Result: The Buy Table will change into distributor table ---//
                        test.validateElementIsPresent(driver, Elements.EvalBoard_Buy_ViewInventory_Tbl);
                    }
                    else
                    {
                        test.validateElementIsPresent(driver, Elements.EvalBoard_Buy_CheckInventory_Btn);
                    }
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.MyAnalog_Widget_Btn);
            }
        }
    }
}