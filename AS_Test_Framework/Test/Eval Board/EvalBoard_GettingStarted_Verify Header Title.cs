﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;

namespace AS_Test_Framework.EvalBoard
{
    [TestFixture]
    public class EvalBoard_GettingStarted_HeaderTitle : BaseSetUp
    {
        public EvalBoard_GettingStarted_HeaderTitle() : base() { }

        //--- URLs ---//
        string evalBoard_url = "/EVAL-ADuCM330";

        //--- Labels ---//
        string gettingStarted_txt = "Getting Started";

        [Test, Category("EvalBoard"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Getting Started Header Title is Present and Correct in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Getting Started Header Title is Present in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Getting Started Header Title is Present in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Getting Started Header Title is Present in RU Locale")]
        public void EvalBoard_GettingStarted_VerifyHeaderTitle(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + evalBoard_url);

            //----- R12 > T1: Verify Getting Started Title -----//

            //--- Expected Result: The header title "Getting Started" must be displayed ---//
            if (Locale.Equals("en"))
            {
                test.validateStringInstance(driver, util.GetText(driver, Elements.EvalBoard_GettingStarted_Sec_Lbl), gettingStarted_txt);
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.EvalBoard_GettingStarted_Sec_Lbl);
            }           
        }
    }
}