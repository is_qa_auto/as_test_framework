﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Threading;
using System.Text.RegularExpressions;
using System;
using System.Linq;

namespace AS_Test_Framework.EvalBoard
{
    [TestFixture]
    public class EvalBoard_Overview_RadioVerseSection : BaseSetUp
    {
        public EvalBoard_Overview_RadioVerseSection() : base() { }

        //--- URLs ---//
        string evalBoard_withMarketsAndTechnology_url = "/EVAL-ADRV9371";
        string evalBoard_withApplicableParts_url = "/PulsarPMODs";
        string evalBoard_withPackageContents_url = "EVAL-ADuCM330";
        string applications_page_url_format = "/applications/";
        string pdp_url_format = "/products/";

        //--- Labels ---//
        string marketsAndTechnology_txt = "Markets & Technology";
        string applicableParts_txt = "Applicable Parts";
        string packageContents_txt = "Package Contents";

        [Test, Category("EvalBoard"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Markets and Technology Section is Present and Working as Expected in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Markets & Technology Section is Present and Working as Expected in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Markets & Technology Section is Present and Working as Expected in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Markets & Technology Section is Present and Working as Expected in RU Locale")]
        public void EvalBoard_Overview_VerifyMarketsAndTechnologySection(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + evalBoard_withMarketsAndTechnology_url);

            int sectionHeader_count = util.GetCount(driver, By.CssSelector("div[class^='radioverse']>h2"));

            for (int sectionHeader_ctr = 1; sectionHeader_ctr <= sectionHeader_count; sectionHeader_ctr++)
            {
                if (util.GetText(driver, By.CssSelector("div[class^='radioverse']>h2:nth-of-type(" + sectionHeader_ctr + ")")).Equals(marketsAndTechnology_txt))
                {
                    //----- R6 > T5: Verify Markets & Technology -----//

                    /*****this is for AL-16901*****/
                    test.validateElementIsPresent(driver, By.CssSelector("div[class^='radioverse'] ul>li[class='accordionGroup']"));
                    test.validateElementIsPresent(driver, By.CssSelector("div[class^='radioverse'] ul>li[class='accordionGroup']:nth-child(1)>div[class='accordionGroup__toggle collapsed']"));
                    string SubItemCount = Regex.Match(util.GetText(driver, By.CssSelector("div[class^='radioverse'] ul>li[class='accordionGroup']>div>a>span")), @"\d+").Value;
                    /***expand the accordion***/
                    action.IClick(driver, By.CssSelector("div[class^='radioverse'] ul>li[class='accordionGroup']:nth-child(1)>div[class='accordionGroup__toggle collapsed'] span[class='accordionGroup__toggle__icon']"));
                    test.validateElementIsPresent(driver, By.CssSelector("div[class^='radioverse'] ul>li[class='accordionGroup']:nth-child(1)>div[class='accordionGroup__toggle']"));
                    test.validateElementIsPresent(driver, By.CssSelector("div[class^='radioverse'] ul>li[class='accordionGroup']:nth-child(1) div[class='accordionGroup__body__inner']"));
                    test.validateCountIsEqual(driver, Int32.Parse(SubItemCount), util.GetCount(driver, By.CssSelector("div[class^='radioverse'] ul>li[class='accordionGroup']:nth-child(1) div[class='accordionGroup__body__inner']>ul>li")));


                    //--- Expected Result: Markets & Technology links must be displayed showing the list of markets & technology applications. ---//
                    test.validateElementIsPresent(driver, By.CssSelector("div[class^='radioverse']>ul:nth-of-type(" + sectionHeader_ctr + ") * a"));

                    if (util.CheckElement(driver, By.CssSelector("div[class^='radioverse']>ul:nth-of-type(" + sectionHeader_ctr + ") * a"), 2))
                    {
                        //--- Action: Click on any markets & technology links ---//
                        action.IOpenLinkInNewTab(driver, By.CssSelector("div[class^='radioverse']>ul:nth-of-type(" + sectionHeader_ctr + ") * a"));
                        Thread.Sleep(2000);

                        //--- Expected Result: Link should redirect to its respective markets & technology application landing pages ---//
                        test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + applications_page_url_format);
                    }
                    driver.SwitchTo().Window(driver.WindowHandles.First());
                    action.IOpenLinkInNewTab(driver, Elements.PDP_MarketsAndTechnology_Link);
                    Thread.Sleep(2000);

                    //--- Expected Result: Clicking the Markets link should go correspnding Application pages ---//
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + applications_page_url_format);

                    break;
                }
            }
        }

        [Test, Category("EvalBoard"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Applicable Parts Section is Present and Working as Expected in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Applicable Parts Section is Present and Working as Expected in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Applicable Parts Section is Present and Working as Expected in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Applicable Parts Section is Present and Working as Expected in RU Locale")]
        public void EvalBoard_Overview_VerifyApplicablePartsSection(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + evalBoard_withApplicableParts_url);

            int sectionHeader_count = util.GetCount(driver, By.CssSelector("div[class^='radioverse']>h2"));

            for (int sectionHeader_ctr = 1; sectionHeader_ctr <= sectionHeader_count; sectionHeader_ctr++)
            {
                if (util.GetText(driver, By.CssSelector("div[class^='radioverse']>h2:nth-of-type(" + sectionHeader_ctr + ")")).Equals(applicableParts_txt))
                {
                    //----- R6 > T6: Verify Applicable Parts. (AL-10843) -----//

                    //--- Expected Result: Product links must be displayed under Applicable parts. ---//
                    test.validateElementIsPresent(driver, Elements.EvalBoard_Overview_All_ApplicableParts_Links);

                    if (util.CheckElement(driver, Elements.EvalBoard_Overview_All_ApplicableParts_Links, 2))
                    {
                        //--- Action: Click Any links under Applicable parts ---//
                        action.IOpenLinkInNewTab(driver, Elements.EvalBoard_Overview_All_ApplicableParts_Links);
                        Thread.Sleep(2000);

                        //--- Expected Result: The locale in the formed URL should be correct ---//
                        test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + pdp_url_format);

                        int applicableParts_count = util.GetCount(driver, Elements.EvalBoard_Overview_All_ApplicableParts_Links);

                        if (applicableParts_count <= 10)
                        {
                            //--- Expected Result: View all and view less link should not displayed ---//
                            test.validateElementIsNotPresent(driver, Elements.EvalBoard_Overview_ApplicableParts_ViewAll_ViewLess_Link);
                        }
                        else
                        {
                            //--- Expected Result: Default applicable parts is 10. ---//
                            test.validateCountIsEqual(driver, 10, util.GetCount(driver, Elements.EvalBoard_Overview_Displayed_ApplicableParts_Links));

                            if (util.CheckElement(driver, Elements.EvalBoard_Overview_ApplicableParts_ViewAll_ViewLess_Link, 2))
                            {
                                //--- Action: Click View All link ---//
                                action.IClick(driver, Elements.EvalBoard_Overview_ApplicableParts_ViewAll_ViewLess_Link);

                                //--- Expected Result: The remaining applicable should expand. ---//
                                test.validateCountIsGreaterOrEqual(driver, util.GetCount(driver, Elements.EvalBoard_Overview_Displayed_ApplicableParts_Links), 11);

                                //--- Action: Click View less link ---//
                                action.IClick(driver, Elements.EvalBoard_Overview_ApplicableParts_ViewAll_ViewLess_Link);

                                //--- Expected Result: The content should collapsed back to 10 applicable products ---//
                                test.validateCountIsEqual(driver, 10, util.GetCount(driver, Elements.EvalBoard_Overview_Displayed_ApplicableParts_Links));
                            }
                        }
                    }

                    break;
                }
            }
        }

        [Test, Category("EvalBoard"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Package Contents Section is Present and Working as Expected in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Package Contents Section is Present and Working as Expected in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Package Contents Section is Present and Working as Expected in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Package Contents Section is Present and Working as Expected in RU Locale")]
        public void EvalBoard_Overview_VerifyPackageContentsSection(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + evalBoard_withPackageContents_url);

            int sectionHeader_count = util.GetCount(driver, By.CssSelector("div[class^='radioverse']>h2"));

            for (int sectionHeader_ctr = 1; sectionHeader_ctr <= sectionHeader_count; sectionHeader_ctr++)
            {
                if (util.GetText(driver, By.CssSelector("div[class^='radioverse']>h2:nth-of-type(" + sectionHeader_ctr + ")")).Equals(packageContents_txt))
                {
                    //----- R6 > T7: Verify Package Contents. (AL-10523) -----//

                    //--- Expected Result: Details/information under "Package contents" should be displayed ---//
                    test.validateElementIsPresent(driver, By.CssSelector("div[class^='radioverse']>ul:nth-of-type(" + (sectionHeader_ctr - 1) + ")>li"));

                    break;
                }
            }
        }
    }
}