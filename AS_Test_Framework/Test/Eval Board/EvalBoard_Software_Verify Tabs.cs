﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Text.RegularExpressions;
using System.Threading;

namespace AS_Test_Framework.EvalBoard
{
    [TestFixture]
    public class EvalBoard_Software_Tabs : BaseSetUp
    {
        public EvalBoard_Software_Tabs() : base() { }

        //--- URLs ---//
        string evalBoard_withEvaluationSoftware_url = "/EVAL-ADIS";
        string evalBoard_withAceSoftware_url = "/EVAL-ADA4571";
        string aceSoftware_page_url = "/ace-software.html";

        //--- Labels ---//
        string software_txt = "Software";
        string viewAll_txt = "View All";
        string evaluationSoftware_txt = "Evaluation Software";
        string aceSoftware_txt = "ACE Software";

        [Test, Category("EvalBoard"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the View All Tab is Present and Working as Expected in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the View All Tab is Present and Working as Expected in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the View All Tab is Present and Working as Expected in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the View All Tab is Present and Working as Expected in RU Locale")]
        public void EvalBoard_Software_VerifyViewAllTab(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + evalBoard_withEvaluationSoftware_url);

            //----- R8 > T1: Verify Software header title (AL-10019) -----//

            //--- Expected Result: The header title "Software" must be displayed ---//
            if (Locale.Equals("en"))
            {
                test.validateStringInstance(driver, software_txt, util.GetText(driver, Elements.EvalBoard_Software_Sec_Lbl));
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.EvalBoard_Software_Sec_Lbl);
            }

            if (util.CheckElement(driver, Elements.EvalBoard_Software_Tabs, 2))
            {
                int softwareTabs_count = util.GetCount(driver, Elements.EvalBoard_Software_Tabs);

                for (int ctr = 1; ctr <= softwareTabs_count; ctr++)
                {
                    if (util.GetText(driver, By.CssSelector("section[id$='software'] * div[class$='tabs']>ul>li:nth-of-type(" + ctr + ")>a")).Contains(viewAll_txt))
                    {
                        //--- Locators ---//
                        string softwareTab_locator = "section[id$='software'] * div[class$='tabs']>ul>li:nth-of-type(" + ctr + ")>a";
                        string selectedSoftwareTab_locator = "section[id$='software'] * div[class$='tabs']>ul>li[class$='active']:nth-of-type(" + ctr + ")>a";

                        //----- R8 > T2: Verify "View All" tab. -----//

                        //--- Expected Result: "View All" tab is present ---//
                        test.validateElementIsPresent(driver, By.CssSelector(softwareTab_locator));

                        //----- R8 > T5: Verify the behavior of tabs under Software Section (AL-10573) -----//

                        //--- Expected Result: "View all tab" should be selected in default ---//
                        test.validateElementIsPresent(driver, By.CssSelector(selectedSoftwareTab_locator));

                        if (!util.CheckElement(driver, By.CssSelector(selectedSoftwareTab_locator), 2))
                        {
                            action.IClick(driver, By.CssSelector(softwareTab_locator));
                        }

                        //--- Expected Result: There should be a counter displayed beside View All tab enclosed by parenthesis showing the number of available pdf documents. ---//
                        test.validateStringInstance(driver, util.GetCount(driver, Elements.EvalBoard_Software_Tab_Links).ToString(), Regex.Match(util.GetText(driver, By.CssSelector(softwareTab_locator)), @"\d+").Value);

                        //----- R8 > T5: Verify the behavior of tabs under Software Section (AL-10573) -----//

                        //--- Expected Result: "View all tab" should be selected with active color #1e4056. ---//
                        test.validateColorIsCorrect(driver, util.GetCssValue(driver, By.CssSelector(softwareTab_locator), "text-decoration-color"), "#1E4056");

                        break;
                    }

                    if (ctr == softwareTabs_count)
                    {
                        test.validateElementIsPresent(driver, By.CssSelector("section[id$='software'] * div[class$='tabs']>ul>li:nth-of-type(" + (ctr + 1) + ")>a"));
                    }
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.EvalBoard_Software_Tabs);
            }
        }

        [Test, Category("EvalBoard"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Evaluation Software Tab is Present and Working as Expected in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Evaluation Software Tab is Present and Working as Expected in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Evaluation Software Tab is Present and Working as Expected in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Evaluation Software Tab is Present and Working as Expected in RU Locale")]
        public void EvalBoard_Software_VerifyEvaluationSoftwareTab(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + evalBoard_withEvaluationSoftware_url);

            if (util.CheckElement(driver, Elements.EvalBoard_Software_Tabs, 2))
            {
                int softwareTabs_count = util.GetCount(driver, Elements.EvalBoard_Software_Tabs);

                for (int ctr = 1; ctr <= softwareTabs_count; ctr++)
                {
                    if (util.GetText(driver, By.CssSelector("section[id$='software'] * div[class$='tabs']>ul>li:nth-of-type(" + ctr + ")>a")).Contains(evaluationSoftware_txt))
                    {
                        //--- Locators ---//
                        string softwareTab_locator = "section[id$='software'] * div[class$='tabs']>ul>li:nth-of-type(" + ctr + ")>a";

                        //----- R8 > T5: Verify the behavior of tabs under Software Section (AL-10573) -----//

                        //--- Action: Hover on the next tab besides the "View all" tab ---//
                        action.IMouseOverTo(driver, By.CssSelector(softwareTab_locator));

                        //--- Expected Result: "Evaluation Software" tab should be in hover state color #009fbd ---//
                        test.validateColorIsCorrect(driver, util.GetCssValue(driver, By.CssSelector(softwareTab_locator), "text-decoration-color"), "#009FBD");

                        //----- R8 > T3: Verify Evaluation Software tab. -----//

                        if (!util.CheckElement(driver, By.CssSelector("section[id$='software'] * div[class$='tabs']>ul>li[class$='active']:nth-of-type(" + ctr + ")>a"), 2))
                        {
                            action.IClick(driver, By.CssSelector(softwareTab_locator));
                        }

                        //--- Expected Result: There should be a counter displayed beside Evaluation Documentation tab enclosed by parenthesis showing the number of available ZIP files or links. ---//
                        test.validateStringInstance(driver, util.GetCount(driver, Elements.EvalBoard_Software_Tab_Links).ToString(), Regex.Match(util.GetText(driver, By.CssSelector(softwareTab_locator)), @"\d+").Value);

                        //--- Expected Result: There should be "Evaluation Documentation" title displayed ---//
                        if (Locale.Equals("en"))
                        {
                            test.validateStringInstance(driver, evaluationSoftware_txt, util.GetText(driver, Elements.EvalBoard_Software_Tab_Title));
                        }
                        else
                        {
                            test.validateElementIsPresent(driver, Elements.EvalBoard_Software_Tab_Title);
                        }

                        //--- Expected Result: There should be ZIP file/link(s) displayed. ---//
                        test.validateElementIsPresent(driver, Elements.EvalBoard_Software_Tab_FileType_Icons);

                        break;
                    }
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.EvalBoard_Software_Tabs);
            }
        }

        [Test, Category("EvalBoard"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the ACE Software Tab is Present and Working as Expected in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the ACE Software Tab is Present and Working as Expected in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the ACE Software Tab is Present and Working as Expected in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the ACE Software Tab is Present and Working as Expected in RU Locale")]
        public void EvalBoard_Software_VerifyAceSoftwareTab(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + evalBoard_withAceSoftware_url);

            if (util.CheckElement(driver, Elements.EvalBoard_Software_Tabs, 2))
            {
                int softwareTabs_count = util.GetCount(driver, Elements.EvalBoard_Software_Tabs);

                for (int ctr = 1; ctr <= softwareTabs_count; ctr++)
                {
                    if (util.GetText(driver, By.CssSelector("section[id$='software'] * div[class$='tabs']>ul>li:nth-of-type(" + ctr + ")>a")).Contains(aceSoftware_txt))
                    {
                        //--- Locators ---//
                        string softwareTab_locator = "section[id$='software'] * div[class$='tabs']>ul>li:nth-of-type(" + ctr + ")>a";

                        //----- R8 > T4: Verify ACE Software tab. (AL-10354) -----//

                        if (!util.CheckElement(driver, By.CssSelector("section[id$='software'] * div[class$='tabs']>ul>li[class$='active']:nth-of-type(" + ctr + ")>a"), 2))
                        {
                            action.IClick(driver, By.CssSelector(softwareTab_locator));
                        }

                        //--- Expected Result: There should be a counter displayed beside ACE Software tab enclosed by parenthesis showing the number of available ZIP files or links. ---//
                        test.validateStringInstance(driver, (util.GetCount(driver, Elements.EvalBoard_Software_Tab_Exe_Links) + util.GetCount(driver, Elements.EvalBoard_Software_Tab_Zip_Links)).ToString(), Regex.Match(util.GetText(driver, By.CssSelector(softwareTab_locator)), @"\d+").Value);

                        //--- Expected Result: There should be "ACE Software" title displayed under  View all tab. ---//
                        if (Locale.Equals("en"))
                        {
                            test.validateStringInstance(driver, aceSoftware_txt, util.GetText(driver, Elements.EvalBoard_Software_Tab_Title));
                        }
                        else
                        {
                            test.validateElementIsPresent(driver, Elements.EvalBoard_Software_Tab_Title);
                        }

                        //--- Expected Result: There should be ZIP file/EXE file link(s) displayed. ---//
                        test.validateElementIsPresent(driver, Elements.EvalBoard_Software_Tab_Links);

                        //--- Expected Result: "ACE software page >" link under ZIP file/EXE file link must be present. ---//
                        test.validateElementIsPresent(driver, Elements.EvalBoard_Software_Tab_AceSoftwarePage_Link);

                        if (util.CheckElement(driver, Elements.EvalBoard_Software_Tab_AceSoftwarePage_Link, 2))
                        {
                            //--- Action: Click on "ACE software page >" link ---//
                            action.IOpenLinkInNewTab(driver, Elements.EvalBoard_Software_Tab_AceSoftwarePage_Link);
                            Thread.Sleep(1000);

                            //--- Expected Result: Page should redirect to Analysis | Control | Evaluation (ACE) Software page. ---//
                            test.validateStringInstance(driver, driver.Url, aceSoftware_page_url);
                        }

                        break;
                    }
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.EvalBoard_Software_Tabs);
            }
        }
    }
}