﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;

namespace AS_Test_Framework.EvalBoard
{
    [TestFixture]
    public class EvalBoard_Discussions_HeaderTitle : BaseSetUp
    {
        public EvalBoard_Discussions_HeaderTitle() : base() { }

        //--- URLs ---//
        string evalBoard_url = "/EVAL-ADV7612";

        //--- Labels ---//
        string discussions_txt = "Discussions";

        [Test, Category("EvalBoard"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Discussions Header Title is Present and Correct in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Discussions Header Title is Present in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Discussions Header Title is Present in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Discussions Header Title is Present in RU Locale")]
        public void EvalBoard_Discussions_VerifyHeaderTitle(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + evalBoard_url);

            //----- R10 > T1: Verify Discussion Title -----//

            //--- Expected Result: The header title "Discussion" must be displayed ---//
            if (Locale.Equals("en"))
            {
                test.validateStringInstance(driver, util.GetText(driver, Elements.EvalBoard_Discussions_Sec_Lbl), discussions_txt);
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.EvalBoard_Discussions_Sec_Lbl);
            }           
        }
    }
}