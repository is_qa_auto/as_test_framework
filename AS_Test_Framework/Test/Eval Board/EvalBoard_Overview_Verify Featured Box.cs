﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;

namespace AS_Test_Framework.EvalBoard
{
    [TestFixture]
    public class EvalBoard_Overview_FeaturedBoxes : BaseSetUp
    {
        public EvalBoard_Overview_FeaturedBoxes() : base() { }

        //--- URLs ---//
        string evalBoard_url = "/EVAL-ADIS";
        string evalBoard_documentation_url = "#eb-documentation";
        string evalBoard_software_url = "#eb-relatedsoftware";

        //--- Labels ---//
        string userGuides_txt = "User Guides";
        string evaluationSoftware_txt = "Evaluation Software";

        [Test, Category("EvalBoard"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the User Guides Featured Box is Present and Working as Expected in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the User Guides Featured Box is Present and Working as Expected in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the User Guides Featured Box is Present and Working as Expected in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the User Guides Featured Box is Present and Working as Expected in RU Locale")]
        public void EvalBoard_Overview_VerifyUserGuidesFeaturedBox(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + evalBoard_url);

            if (util.CheckElement(driver, Elements.EvalBoard_UserGuides_FeaturedBox, 2))
            {
                int featuredBoxes_count = util.GetCount(driver, By.CssSelector("section[id$='overview'] * div[class*='datasheets']>div"));

                for (int featuredBox_ctr = 1; featuredBox_ctr <= featuredBoxes_count; featuredBox_ctr++)
                {
                    if (util.GetText(driver, By.CssSelector("section[id$='overview'] * div[class*='datasheets']>div:nth-of-type(" + featuredBox_ctr + ") * div[class='title'] span:nth-of-type(1)")).Equals(userGuides_txt))
                    {
                        //----- R4 > T2: Verify Users Guide featured box -----//

                        //--- Expected Result: Counter icon is displayed ---//
                        test.validateElementIsPresent(driver, By.CssSelector("section[id$='overview'] * div[class*='datasheets']>div:nth-of-type(" + featuredBox_ctr + ") * span[class='count']"));

                        if (util.CheckElement(driver, By.CssSelector("section[id$='overview'] * div[class*='datasheets']>div:nth-of-type(" + featuredBox_ctr + ") * span[class='count']"), 2))
                        {
                            string userGuides_count = util.GetText(driver, By.CssSelector("section[id$='overview'] * div[class*='datasheets']>div:nth-of-type(" + featuredBox_ctr + ") * span[class='count']"));

                            //--- Action: Click the counter icon in the User Guide card. ---//
                            action.IClick(driver, By.CssSelector("section[id$='overview'] * div[class*='datasheets']>div:nth-of-type(" + featuredBox_ctr + ") * span[class='count']"));

                            //--- Expected Result: The page should anchor down to "Documentation" section. ---//
                            test.validateStringInstance(driver, driver.Url, evalBoard_documentation_url);

                            if (util.CheckElement(driver, Elements.EvalBoard_Documentation_Tabs, 2))
                            {
                                int documentationTabs_count = util.GetCount(driver, Elements.EvalBoard_Documentation_Tabs);

                                for (int documentationTabs_ctr = 1; documentationTabs_ctr <= documentationTabs_count; documentationTabs_ctr++)
                                {
                                    if (util.GetText(driver, By.CssSelector("section[id$='documentation'] * div[class$='tabs']>ul>li:nth-of-type(" + documentationTabs_ctr + ")>a")).Contains(userGuides_txt))
                                    {
                                        //--- Expected Result: The number of items in the featured box should match in the User guides displayed under documentation ---//
                                        test.validateStringInstance(driver, userGuides_count, util.GetCount(driver, Elements.EvalBoard_Documentation_UserGuides_Links).ToString());

                                        break;
                                    }
                                }
                            }
                        }

                        break;
                    }
                }
            }
        }

        [Test, Category("EvalBoard"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Evaluation Software Featured Box is Present and Working as Expected in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Evaluation Software Featured Box is Present and Working as Expected in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Evaluation Software Featured Box is Present and Working as Expected in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Evaluation Software Featured Box is Present and Working as Expected in RU Locale")]
        public void EvalBoard_Overview_VerifyEvaluationSoftwareFeaturedBox(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + evalBoard_url);

            if (util.CheckElement(driver, Elements.EvalBoard_EvaluationSoftware_FeaturedBox, 2))
            {
                int featuredBoxes_count = util.GetCount(driver, By.CssSelector("section[id$='overview'] * div[class*='datasheets']>div"));

                for (int featuredBox_ctr = 1; featuredBox_ctr <= featuredBoxes_count; featuredBox_ctr++)
                {
                    if (util.GetText(driver, By.CssSelector("section[id$='overview'] * div[class*='datasheets']>div:nth-of-type(" + featuredBox_ctr + ") * div[class='title'] span:nth-of-type(1)")).Equals(evaluationSoftware_txt))
                    {
                        //----- R4 > T3: Verify Evaluation Software featured box -----//

                        //--- Expected Result: Counter icon is displayed ---//
                        test.validateElementIsPresent(driver, By.CssSelector("section[id$='overview'] * div[class*='datasheets']>div:nth-of-type(" + featuredBox_ctr + ") * span[class='count']"));

                        if (util.CheckElement(driver, By.CssSelector("section[id$='overview'] * div[class*='datasheets']>div:nth-of-type(" + featuredBox_ctr + ") * span[class='count']"), 2))
                        {
                            string evaluationSoftware_count = util.GetText(driver, By.CssSelector("section[id$='overview'] * div[class*='datasheets']>div:nth-of-type(" + featuredBox_ctr + ") * span[class='count']"));

                            if (!Configuration.Browser.Equals("Chrome_HL"))
                            {
                                //--- Action: Click the counter icon in the Evaluation Software card. ---//
                                action.IClick(driver, By.CssSelector("section[id$='overview'] * div[class*='datasheets']>div:nth-of-type(" + featuredBox_ctr + ") * span[class='count']"));

                                //--- Expected Result: The page should anchor down to "Software" section.---//
                                test.validateStringInstance(driver, driver.Url, evalBoard_software_url);
                            }

                            if (util.CheckElement(driver, Elements.EvalBoard_Software_Tabs, 2))
                            {
                                int softwareTabs_count = util.GetCount(driver, Elements.EvalBoard_Software_Tabs);

                                for (int softwareTabs_ctr = 1; softwareTabs_ctr <= softwareTabs_count; softwareTabs_ctr++)
                                {
                                    if (util.GetText(driver, By.CssSelector("section[id$='software'] * div[class$='tabs']>ul>li:nth-of-type(" + softwareTabs_ctr + ")>a")).Contains(evaluationSoftware_txt))
                                    {
                                        //--- Expected Result: The number of items in the featured box should match in the Evaluation Software displayed under software  ---//
                                        test.validateStringInstance(driver, evaluationSoftware_count, util.GetCount(driver, Elements.EvalBoard_Software_EvaluationSoftware_Links).ToString());

                                        break;
                                    }
                                }
                            }
                        }

                        break;
                    }
                }
            }
        }
    }
}