﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Text.RegularExpressions;

namespace AS_Test_Framework.EvalBoard
{
    [TestFixture]
    public class EvalBoard_Documentation_Tabs : BaseSetUp
    {
        public EvalBoard_Documentation_Tabs() : base() { }

        //--- URLs ---//
        string evalBoard_url = "/EVAL-ADIS";

        //--- Labels ---//
        string documentation_txt = "Documentation";
        string viewAll_txt = "View All";
        string userGuides_txt = "User Guides";
        string evaluationDocumentation_txt = "Evaluation Documentation";

        [Test, Category("EvalBoard"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the View All Tab is Present and Working as Expected in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the View All Tab is Present and Working as Expected in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the View All Tab is Present and Working as Expected in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the View All Tab is Present and Working as Expected in RU Locale")]
        public void EvalBoard_Documentation_VerifyViewAllTab(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + evalBoard_url);

            //----- R7 > T1: Verify Documentation header title -----//

            //--- Expected Result: The header title "Documentation" must be displayed ---//
            if (Locale.Equals("en"))
            {
                test.validateStringInstance(driver, documentation_txt, util.GetText(driver, Elements.EvalBoard_Documentation_Sec_Lbl));
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.EvalBoard_Documentation_Sec_Lbl);
            }

            if (util.CheckElement(driver, Elements.EvalBoard_Documentation_Tabs, 2))
            {
                int documentationTabs_count = util.GetCount(driver, Elements.EvalBoard_Documentation_Tabs);

                for (int ctr = 1; ctr <= documentationTabs_count; ctr++)
                {
                    if (util.GetText(driver, By.CssSelector("section[id$='documentation'] * div[class$='tabs']>ul>li:nth-of-type(" + ctr + ")>a")).Contains(viewAll_txt))
                    {
                        //--- Locators ---//
                        string documentationTab_locator = "section[id$='documentation'] * div[class$='tabs']>ul>li:nth-of-type(" + ctr + ")>a";
                        string selectedDocumentationTab_locator = "section[id$='documentation'] * div[class$='tabs']>ul>li[class$='active']:nth-of-type(" + ctr + ")>a";

                        //----- R7 > T2: Verify "View All" tab. -----//

                        //--- Expected Result: "View All" tab is present ---//
                        test.validateElementIsPresent(driver, By.CssSelector(documentationTab_locator));

                        //----- R7 > T6: Verify if  "View all tab" is selected in default (AL-10573) -----//

                        //--- Expected Result: "View all tab" should be selected in default ---//
                        test.validateElementIsPresent(driver, By.CssSelector(selectedDocumentationTab_locator));

                        if (!util.CheckElement(driver, By.CssSelector(selectedDocumentationTab_locator), 2))
                        {
                            //--- Action: Click View All Tab ---//
                            action.IClick(driver, By.CssSelector(documentationTab_locator));
                        }

                        //--- Expected Result: Number of Documents shown beside to "View All" should be same as the total number of documents. ---//
                        test.validateStringInstance(driver, util.GetCount(driver, Elements.EvalBoard_Documentation_Tab_Links).ToString(), Regex.Match(util.GetText(driver, By.CssSelector(documentationTab_locator)), @"\d+").Value);

                        //----- R7 > T6: Verify if  "View all tab" is selected in default (AL-10573) -----//

                        //--- Expected Result: "View all tab" should be with active color #1e4056. ---//
                        test.validateColorIsCorrect(driver, util.GetCssValue(driver, By.CssSelector(documentationTab_locator), "text-decoration-color"), "#1E4056");

                        break;
                    }

                    if (ctr == documentationTabs_count)
                    {
                        test.validateElementIsPresent(driver, By.CssSelector("section[id$='documentation'] * div[class$='tabs']>ul>li:nth-of-type(" + (ctr + 1) + ")>a"));
                    }
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.EvalBoard_Documentation_Tabs);
            }
        }

        [Test, Category("EvalBoard"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the User Guides Tab is Present and Working as Expected in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the User Guides Tab is Present and Working as Expected in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the User Guides Tab is Present and Working as Expected in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the User Guides Tab is Present and Working as Expected in RU Locale")]
        public void EvalBoard_Documentation_VerifyUserGuidesTab(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + evalBoard_url);

            if (util.CheckElement(driver, Elements.EvalBoard_Documentation_Tabs, 2))
            {
                int documentationTabs_count = util.GetCount(driver, Elements.EvalBoard_Documentation_Tabs);

                for (int ctr = 1; ctr <= documentationTabs_count; ctr++)
                {
                    if (util.GetText(driver, By.CssSelector("section[id$='documentation'] * div[class$='tabs']>ul>li:nth-of-type(" + ctr + ")>a")).Contains(userGuides_txt))
                    {
                        //--- Locators ---//
                        string documentationTab_locator = "section[id$='documentation'] * div[class$='tabs']>ul>li:nth-of-type(" + ctr + ")>a";

                        //----- R7 > T5: Verify the behavior of tabs under Software Section (AL-10573) -----//

                        //--- Action: Hover on the next tab besides the "View all" tab ---//
                        action.IMouseOverTo(driver, By.CssSelector(documentationTab_locator));

                        //--- Expected Result: "Evaluation Software" tab should be in hover state color #009fbd ---//
                        test.validateColorIsCorrect(driver, util.GetCssValue(driver, By.CssSelector(documentationTab_locator), "text-decoration-color"), "#009FBD");

                        //----- R7 > T3: Verify User Guides tab. -----//

                        if (!util.CheckElement(driver, By.CssSelector("section[id$='documentation'] * div[class$='tabs']>ul>li[class$='active']:nth-of-type(" + ctr + ")>a"), 2))
                        {
                            action.IClick(driver, By.CssSelector(documentationTab_locator));
                        }

                        //--- Expected Result: There should be a counter displayed beside User Guides tab enclosed by parenthesis showing the number of available pdf data sheet documents. ---//
                        test.validateStringInstance(driver, util.GetCount(driver, Elements.EvalBoard_Documentation_Tab_Links).ToString(), Regex.Match(util.GetText(driver, By.CssSelector(documentationTab_locator)), @"\d+").Value);

                        //--- Expected Result: There should be "User Guides" title displayed under User Guides tab. ---//
                        if (Locale.Equals("en"))
                        {
                            test.validateStringInstance(driver, userGuides_txt, util.GetText(driver, Elements.EvalBoard_Documentation_Tab_Title));
                        }
                        else
                        {
                            test.validateElementIsPresent(driver, Elements.EvalBoard_Documentation_Tab_Title);
                        }

                        break;
                    }
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.EvalBoard_Documentation_Tabs);
            }
        }

        [Test, Category("EvalBoard"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Evaluation Documentation Tab is Present and Working as Expected in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Evaluation Documentation Tab is Present and Working as Expected in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Evaluation Documentation Tab is Present and Working as Expected in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Evaluation Documentation Tab is Present and Working as Expected in RU Locale")]
        public void EvalBoard_Documentation_VerifyEvaluationDocumentationTab(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + evalBoard_url);

            if (util.CheckElement(driver, Elements.EvalBoard_Documentation_Tabs, 2))
            {
                int documentationTabs_count = util.GetCount(driver, Elements.EvalBoard_Documentation_Tabs);

                for (int ctr = 1; ctr <= documentationTabs_count; ctr++)
                {
                    if (util.GetText(driver, By.CssSelector("section[id$='documentation'] * div[class$='tabs']>ul>li:nth-of-type(" + ctr + ")>a")).Contains(evaluationDocumentation_txt))
                    {
                        //----- R7 > T5: Verify Evaluation Documentation tab. -----//

                        if (!util.CheckElement(driver, By.CssSelector("section[id$='documentation'] * div[class$='tabs']>ul>li[class$='active']:nth-of-type(" + ctr + ")>a"), 2))
                        {
                            action.IClick(driver, By.CssSelector("section[id$='documentation'] * div[class$='tabs']>ul>li:nth-of-type(" + ctr + ")>a"));
                        }

                        //--- Expected Result: There should be a counter displayed beside Evaluation Documentation tab enclosed by parenthesis showing the number of available pdf data sheet documents. ---//
                        test.validateStringInstance(driver, util.GetCount(driver, Elements.EvalBoard_Documentation_Tab_Links).ToString(), Regex.Match(util.GetText(driver, By.CssSelector("section[id$='documentation'] * div[class$='tabs']>ul>li:nth-of-type(" + ctr + ")>a")), @"\d+").Value);

                        //--- Expected Result: There should be "Evaluation Documentation" title displayed under User Guides tab. ---//
                        if (Locale.Equals("en"))
                        {
                            test.validateStringInstance(driver, evaluationDocumentation_txt, util.GetText(driver, Elements.EvalBoard_Documentation_Tab_Title));
                        }
                        else
                        {
                            test.validateElementIsPresent(driver, Elements.EvalBoard_Documentation_Tab_Title);
                        }

                        break;
                    }
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.EvalBoard_Documentation_Tabs);
            }
        }
    }
}