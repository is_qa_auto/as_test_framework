﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Threading;

namespace AS_Test_Framework.EvalBoard
{
    [TestFixture]
    public class EvalBoard_NavigationalMenu : BaseSetUp
    {
        public EvalBoard_NavigationalMenu() : base() { }

        //--- URLs ---//
        string evalBoard_withLongDesc_url = "/bf-extender";
        string evalBoard_url = "/EVAL-ADIS";
        string evalBoard_overview_url = "#eb-overview";
        string evalBoard_documentation_url = "#eb-documentation";
        string evalBoard_software_url = "#eb-relatedsoftware";
        string evalBoard_relatedHardware_url = "#eb-relatedhardware";
        string evalBoard_discussions_url = "#eb-discussions";
        string evalBoard_systemsRequirements_url = "#eb-systemrequirements";
        string evalBoard_gettingStarted_url = "#eb-gettingstarted";
        string evalBoard_referenceBoard_url = "referenceboard";
        string evalBoard_buy_url = "#eb-relatedhardware";

        [Test, Category("EvalBoard"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Title Section is Present and Working as Expected in EN Locale", Category = "Smoke_ADIWeb")]
        [TestCase("cn", TestName = "Verify that the Title Section is Present and Working as Expected in CN Locale", Category = "Smoke_ADIWeb")]
        [TestCase("jp", TestName = "Verify that the Title Section is Present and Working as Expected in JP Locale", Category = "Smoke_ADIWeb")]
        [TestCase("ru", TestName = "Verify that the Title Section is Present and Working as Expected in RU Locale")]
        public void EvalBoard_VerifyTitleSection(string Locale)
        {
            string initial_steps = "1. Go to " + Configuration.Env_Url + Locale + evalBoard_withLongDesc_url;
            string scenario = "";
            action.INavigate(driver, Configuration.Env_Url + Locale + evalBoard_withLongDesc_url);

            if (util.CheckElement(driver, Elements.EvalBoard_Title_Sec, 2))
            {
                //----- R2 > T1: Verify the Eval board name and description -----//

                //--- Expected Result: Eval board name should display---//
                scenario = "Verify that Evaluation Board title is present";
                test.validateElementIsPresentv2(driver, Elements.EvalBoard_Title_Sec_Name_Txt, scenario, initial_steps);

                //--- Expected Result: Eval board SHORT description and/or specs should be display ---//
                scenario = "Verify that Evaluation Board description is present";
                test.validateElementIsPresentv2(driver, Elements.EvalBoard_Title_Sec_Desc_Txt, scenario, initial_steps);

                if (util.CheckElement(driver, Elements.EvalBoard_Title_Sec_ShowMore_ShowLess_Link, 2))
                {
                    //----- R2 > T2: Verify Show more and Show less link for Eval board description more that 750 -----//

                    //--- Action: Cilck Show more link  ---//
                    action.IClick(driver, Elements.EvalBoard_Title_Sec_ShowMore_ShowLess_Link);

                    //--- Expected Result: Description should expand. ---//
                    scenario = "Verify that expanded description will be displayed when user clicks on \"show more\" link";
                    test.validateElementIsPresentv2(driver, Elements.EvalBoard_Title_Sec_Expanded_Desc_Txt, scenario, initial_steps + "<br>2. Click \"show more\" Link");

                    //--- Action: Click on show less link ---//
                    action.IClick(driver, Elements.EvalBoard_Title_Sec_ShowMore_ShowLess_Link);

                    //--- Expected Result: Description should collapse. ---//
                    scenario = "Verify that short description will be displayed when user clicks on \"show less\" link";
                    test.validateElementIsNotPresentv2(driver, Elements.EvalBoard_Title_Sec_Expanded_Desc_Txt, scenario, initial_steps);
                }

                action.IMouseOverTo(driver, Elements.Footer);

                //--- Expected Result: Eval Board name and Description should dock along with the sticky nav upon clicking specific component or upon scrolling. ---//
                scenario = "Verify that Title and Description will dock along with the sticky nav upon scrolling the page down. ";
                test.validateElementIsPresentv2(driver, Elements.EvalBoard_Title_Sec, scenario, initial_steps + "<br>2. Scroll down up to the footer section");
            }
            else
            {
                scenario = "Verify that Evaluation Board title section is present";
                test.validateElementIsPresentv2(driver, Elements.EvalBoard_Title_Sec, scenario, initial_steps);
            }
        }

        [Test, Category("EvalBoard"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Navigational Menu is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Navigational Menu is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Navigational Menu is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Navigational Menu is Present and Working as Expected in RU Locale")]
        public void EvalBoard_VerifyNavigationalMenu(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + evalBoard_url);

            //----- R3 > T1: Verify navigational menus (AL-10132 & AL-10016) -----//

            //--- Expected Result: Analog product part number must be shown. ---//
            test.validateElementIsPresent(driver, Elements.EvalBoard_NavigationalMenu_Sec);

            if (util.CheckElement(driver, Elements.EvalBoard_NavigationalMenu_Sec, 2))
            {
                //----- R3 > T1: Verify navigational menus are clickable -----//

                if (util.CheckElement(driver, Elements.EvalBoard_NavigationalMenu_Overview, 2))
                {
                    //--- Action: Click "Overview" on navigational menus ---//
                    action.IClick(driver, Elements.EvalBoard_NavigationalMenu_Overview);

                    //--- Expected Result: Each navigational buttons must anchor on its respective sections within the page upon clicking. ---//
                    test.validateStringInstance(driver, driver.Url, evalBoard_overview_url);
                }

                if (util.CheckElement(driver, Elements.EvalBoard_NavigationalMenu_Documentation, 2))
                {
                    //--- Action: Click "Documentation" on navigational menus ---//
                    action.IClick(driver, Elements.EvalBoard_NavigationalMenu_Documentation);

                    //--- Expected Result: Each navigational buttons must anchor on its respective sections within the page upon clicking. ---//
                    test.validateStringInstance(driver, driver.Url, evalBoard_documentation_url);
                }

                if (util.CheckElement(driver, Elements.EvalBoard_NavigationalMenu_Software, 2))
                {
                    //--- Action: Click "Software" on navigational menus ---//
                    action.IClick(driver, Elements.EvalBoard_NavigationalMenu_Software);
                    Thread.Sleep(1000);

                    //--- Expected Result: Each navigational buttons must anchor on its respective sections within the page upon clicking. ---//
                    test.validateStringInstance(driver, driver.Url, evalBoard_software_url);
                }

                if (util.CheckElement(driver, Elements.EvalBoard_NavigationalMenu_RelatedHardware, 2))
                {
                    //--- Action: Click "Related Hardware" on navigational menus ---//
                    action.IClick(driver, Elements.EvalBoard_NavigationalMenu_RelatedHardware);

                    //--- Expected Result: Each navigational buttons must anchor on its respective sections within the page upon clicking. ---//
                    test.validateStringInstance(driver, driver.Url, evalBoard_relatedHardware_url);
                }

                if (util.CheckElement(driver, Elements.EvalBoard_NavigationalMenu_Discussions, 2))
                {
                    //--- Action: Click "Discussions" on navigational menus ---//
                    action.IClick(driver, Elements.EvalBoard_NavigationalMenu_Discussions);

                    //--- Expected Result: Each navigational buttons must anchor on its respective sections within the page upon clicking. ---//
                    test.validateStringInstance(driver, driver.Url, evalBoard_discussions_url);
                }

                if (util.CheckElement(driver, Elements.EvalBoard_NavigationalMenu_SystemRequirements, 2))
                {
                    //--- Action: Click "System Requirements" on navigational menus ---//
                    action.IClick(driver, Elements.EvalBoard_NavigationalMenu_SystemRequirements);

                    //--- Expected Result: Each navigational buttons must anchor on its respective sections within the page upon clicking. ---//
                    test.validateStringInstance(driver, driver.Url, evalBoard_systemsRequirements_url);
                }

                if (util.CheckElement(driver, Elements.EvalBoard_NavigationalMenu_GettingStarted, 2))
                {
                    //--- Action: Click "Getting Started" on navigational menus ---//
                    action.IClick(driver, Elements.EvalBoard_NavigationalMenu_GettingStarted);

                    //--- Expected Result: Each navigational buttons must anchor on its respective sections within the page upon clicking. ---//
                    test.validateStringInstance(driver, driver.Url, evalBoard_gettingStarted_url);
                }

                if (util.CheckElement(driver, Elements.EvalBoard_NavigationalMenu_ReferenceBoard, 2))
                {
                    //--- Action: Click "Reference Board" on navigational menus ---//
                    action.IClick(driver, Elements.EvalBoard_NavigationalMenu_ReferenceBoard);

                    //--- Expected Result: Each navigational buttons must anchor on its respective sections within the page upon clicking. ---//
                    test.validateStringInstance(driver, driver.Url, evalBoard_referenceBoard_url);
                }

                if (util.CheckElement(driver, Elements.EvalBoard_NavigationalMenu_Buy, 2))
                {
                    //--- Action: Click "Buy" on navigational menus ---//
                    action.IClick(driver, Elements.EvalBoard_NavigationalMenu_Buy);

                    //--- Expected Result: Each navigational buttons must anchor on its respective sections within the page upon clicking. ---//
                    test.validateStringInstance(driver, driver.Url, evalBoard_buy_url);
                }

                //----- R3 > T4: Verify navigational menus should dock to the header when clicking any menu sections. -----//

                //--- Expected Result: Navigational menus should dock to the header upon clicking or selecting any of the menu sections. ---//
                test.validateElementIsPresent(driver, Elements.EvalBoard_NavigationalMenu_Sec);

                //----- R3 > T6: Verify Back to Top button when mouse scroll -----//

                //--- Expected Result: Back to top button must be shown ---//
                test.validateElementIsPresent(driver, Elements.EvalBoard_NavigationalMenu_Top_Btn);

                if (util.CheckElement(driver, Elements.EvalBoard_NavigationalMenu_Top_Btn, 2))
                {
                    //--- Action: Click "TOP" button. ---//
                    action.IClick(driver, Elements.EvalBoard_NavigationalMenu_Top_Btn);

                    //--- Expected Result: The page should anchor back to the top page. ---//
                    test.validateStringInstance(driver, driver.Url, evalBoard_overview_url);
                }
            }
        }
    }
}