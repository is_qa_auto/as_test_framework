﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_Products_Elements : BaseSetUp
    {
        //--- myAnalog Products URL: ---//
        //--- DEV: https://my-dev.corpnt.analog.com/en/app/products ---//
        //--- QA: https://my-qa.corpnt.analog.com/en/app/products ---//
        //--- PROD: https://my.analog.com/en/app/products ---//

        public MyAnalog_Products_Elements() : base() { }

        //--- Login Credentials ---//
        string username = "myanalog_t3st_pr0ducts_555@mailinator.com";
        string password = "Test_1234";

        //-- Login Credentials for Account with no Saved Products --//
        string username_new = "myanalog_t3st_new_555@mailinator.com";
        string password_new = "Test_1234";

        //--- URLs ---//
        string myAnalog_products_url = "app/products";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Elements are Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Elements are Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Elements are Present and Working as Expected in JP Locale")]
        public void MyAnalog_Products_VerifyElements(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Products URL ---//
            string myAnalog_url = $"{Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt")}{Locale}/{myAnalog_products_url}";

            //--- Action: Open myAnalog Products using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);

            //----- Product Test Plan > MAR>Dashboard>Products Tab > R2 > T1: Verify the page title "Products" -----//

            //--- Expected Result: "Products" should be displayed ---//
            if (Locale.Equals("en"))
            {
                test.validateStringIsCorrect(driver, Elements.MyAnalog_Products_Label, "Products");
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.MyAnalog_Products_Label);
            }

            //----- Product Test Plan > MAR>Dashboard>Products Tab > R2 > T2: Verify the display message under the page title "Products" -----//

            //--- Expected Result: The following message should be shown under the page title "Products": ---//
            //--- See updates to your saved products, add products to your list, and share or remove a product using the options on the right. ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Products_Description_Text);

            //----- Hardware Evalboard Registration > Test Case Title: Verify if the Evaluation boards tab display in myAnalog Products page. (IQ-10429/AL-17522) -----//

            //--- Expected Result: Evaluation boards tab display ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Products_EvaluationBoards_Tab);
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Elements are Present and Working as Expected when No Product is Saved in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Elements are Present and Working as Expected when No Product is Saved in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Elements are Present and Working as Expected when No Product is Saved in JP Locale")]
        public void MyAnalog_Products_VerifyElementsWhenNoSavedProduct(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Products URL ---//
            string myAnalog_url = $"{Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt")}{Locale}/{myAnalog_products_url}";

            //--- Action: Open myAnalog Products using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username_new, password_new);

            //----- Product Test Plan > MAR>Dashboard>Products Tab > R6 > T1: Verify myAnalog Dashboard Products widget when there is no saved products -----//

            //--- Expected Result: The following message should be shown: "Add products to myAnalog to receive important updates." ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Products_NoSavedProduct_Description_Text);
        }
    }
}