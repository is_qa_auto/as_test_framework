﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System;
using OpenQA.Selenium;
using System.Threading;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_Masthead_Markets_SavingMarketCategories : BaseSetUp
    {
        public MyAnalog_Masthead_Markets_SavingMarketCategories() : base() { }

        //--- Login Credentials ---//
        string username = "alliance_qa_auto@mailinator.com";
        string password = "Test_1234";

        [Test, Category("MyAnalog"), Category("NonCore")]
        [TestCase("en", TestName = "Verify user can saved and delete market categories in myAnalog Masthead is working in EN Locale")]
        [TestCase("zh", TestName = "Verify user can saved and delete market categories in myAnalog Masthead is working in CN Locale")]
        [TestCase("jp", TestName = "Verify user can saved and delete market categories in myAnalog Masthead is working in JP Locale")]
        public void MyAnalog_Masthead_VerifySavingOfMarketCategory(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Resources URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/app";

            //--- Action: Open MyAnalog App direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);

            /*****Test Cancel Button****/
            action.IClick(driver, Elements.MyAnalog_Masthead_Market_DD);
            action.IClick(driver, Elements.MyAnalog_Masthead_Category_Cancel_Button);
            test.validateElementIsNotPresent(driver, Elements.MyAnalog_Masthead_Category_Menu_List);

            /*****Test Saving of Market Categories*****/
            action.IClick(driver, Elements.MyAnalog_Masthead_Market_DD);
            string SelectedCategory = util.GetText(driver, By.CssSelector("section[class='interests__menu__items__options'] ul>li:nth-child(1) span"));

            test.validateElementIsNotSelected(driver, By.CssSelector("section[class='interests__menu__items__options'] ul>li:nth-child(1) div[class='switch'] input"));
            action.IClick(driver, By.CssSelector("section[class='interests__menu__items__options'] ul>li:nth-child(1) div[class='switch'] label"));
            test.validateElementIsSelected(driver, By.CssSelector("section[class='interests__menu__items__options'] ul>li:nth-child(1) div[class='switch'] input"));
            Thread.Sleep(2000);

            test.validateElementIsEnabled(driver, Elements.MyAnalog_Masthead_Category_Saved_Button);
            action.IClick(driver, Elements.MyAnalog_Masthead_Category_Saved_Button);
            test.validateElementIsNotPresent(driver, Elements.MyAnalog_Masthead_Category_Menu_List);

            /*****Test IQ-7457 ****/
            string No_of_saved_category = util.GetText(driver, Elements.MyAnalog_Masthead_Market_Count);
            action.IClick(driver, Elements.MyAnalog_Masthead_Market_DD);
            test.validateCountIsEqual(driver, Int32.Parse(No_of_saved_category), util.GetCount(driver, By.CssSelector("section[class='interests__menu__items__saved'] ul[class='interests__menu__items__list']>li")));

            bool isCategorySaved = false;

            for (int x = 1; util.CheckElement(driver, By.CssSelector("section[class='interests__menu__items__saved'] ul[class='interests__menu__items__list']>li:nth-child(" + x + ")"), 5); x++)
            {
                if (util.GetText(driver, By.CssSelector("section[class='interests__menu__items__saved'] ul[class='interests__menu__items__list']>li:nth-child(" + x + ") span")).Equals(SelectedCategory))
                {
                    isCategorySaved = true;
                    break;
                }
            }

            Assert.True(isCategorySaved);

            for (int x = 1; util.CheckElement(driver, By.CssSelector("section[class='interests__menu__items__saved'] ul[class='interests__menu__items__list']>li:nth-child(" + x + ")"), 5); x++)
            {
                if (util.GetText(driver, By.CssSelector("section[class='interests__menu__items__saved'] ul[class='interests__menu__items__list']>li:nth-child(" + x + ") span")).Equals(SelectedCategory))
                {
                    action.IClick(driver, By.CssSelector("section[class='interests__menu__items__saved'] ul>li:nth-child(" + x + ") div[class='switch'] label"));
                    action.IClick(driver, Elements.MyAnalog_Masthead_Category_Saved_Button);
                    break;
                }
            }
            test.validateElementIsNotPresent(driver, Elements.MyAnalog_Masthead_Category_Menu_List);
            action.IClick(driver, Elements.MyAnalog_Masthead_Category_DD);

            bool isCategoryDeleted = false;

            for (int x = 1; util.CheckElement(driver, By.CssSelector("section[class='interests__menu__items__saved'] ul[class='interests__menu__items__list']>li:nth-child(" + x + ")"), 5); x++)
            {
                if (util.GetText(driver, By.CssSelector("section[class='interests__menu__items__saved'] ul[class='interests__menu__items__list']>li:nth-child(" + x + ") span")).Equals(SelectedCategory))
                {
                    isCategorySaved = true;
                    break;
                }
            }
            Assert.False(isCategoryDeleted);
        }

        [Test, Category("MyAnalog"), Category("NonCore")]
        [TestCase("en", TestName = "Verify display of sub-market in Markets widget in EN Locale")]
        [TestCase("zh", TestName = "Verify display of sub-market in Markets widget in CN Locale")]
        [TestCase("jp", TestName = "Verify display of sub-market in Markets widget in JP Locale")]
        public void MyAnalog_Masthead_VerifySavingofSubMarket(string Locale)
        {
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/app";

            //--- Action: Open MyAnalog App direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);
            action.IClick(driver, Elements.MyAnalog_Masthead_Market_DD);
            action.IClick(driver, By.CssSelector("section[class='interests__menu__items__options'] ul>li:nth-child(1) button"));
            string SelectedCategory = util.GetText(driver, By.CssSelector("section[class='interests__menu__items__options'] ul>li:nth-child(1) ul>li:nth-child(1) span"));
            action.IClick(driver, By.CssSelector("section[class='interests__menu__items__options'] ul>li:nth-child(1) ul>li:nth-child(1) div[class='switch'] label"));
            action.IClick(driver, Elements.MyAnalog_Masthead_Category_Saved_Button);
            action.IClick(driver, Elements.MainNavigation_MyAnalogTab);
            test.validateStringIsCorrect(driver, By.CssSelector("div[class='myAnalog menu content expanded'] div[class='col-md-3 widget applications'] ul>li:nth-child(1)>a"), SelectedCategory);

            action.IRemoveMarketInMyAnalog(driver, Locale, username, password, SelectedCategory);
        }
    }
}