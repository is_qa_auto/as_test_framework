﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System;
using System.Threading;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_Projects_CreateANewProjectModal : BaseSetUp
    {
        //--- myAnalog Projects URL: ---//
        //--- DEV: https://my-dev.corpnt.analog.com/en/app/projects ---//
        //--- QA: https://my-qa.corpnt.analog.com/en/app/projects ---//
        //--- PROD: https://my.analog.com/en/app/projects ---//

        public MyAnalog_Projects_CreateANewProjectModal() : base() { }

        //----- Note: Please don't edit/delete any details on the Accounts below -----//

        //--- Login Credentials ---//
        string username = "my_t3st_pr0j3cts_444@mailinator.com";
        string password = "Test_1234";

        //--- URLs ---//
        string myAnalog_projects_url = "app/projects";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Create a new project Modal is Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Create a new project Modal is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Create a new project Modal is Present and Working as Expected in JP Locale")]
        public void MyAnalog_Projects_VerifyCreateANewProjectModal(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Projects URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_projects_url;

            //--- Action: Open myAnalog Projects using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify previously selected quick message is clickable/selectable (on the previous failed attempt of creating new project) -----//

            //--- Action: Click Create a New Project button ---//
            action.IClick(driver, Elements.MyAnalog_Projects_CreateANewProject_Button);
            Thread.Sleep(2000);

            //--- Expected Result: Modal popped up ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Projects_CreateANewProject_Modal);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify the title -----//
            if (Locale.Equals("en"))
            {
                //--- Expected Result: The title "Create new project" should be present ---//
                test.validateStringIsCorrect(driver, Elements.MyAnalog_Projects_CreateANewProject_Modal_Header_Label, "Create a new project");
            }
            else
            {
                //--- Expected Result: The title should be present ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_Projects_CreateANewProject_Modal_Header_Label);
            }

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify Project Description default ghost text -----//

            //--- Expected Result: The ghost text should display ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Projects_CreateANewProject_Modal_ProjectDescription_GhostText_Label);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify Invitees field default ghost text -----//

            //--- Expected Result: The ghost text should display as: "Add invitees' emails (separated by commas) you want to invite to this project" ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Projects_CreateANewProject_Modal_Invitees_GhostText_Label);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify Message field default ghost text -----//

            //--- Expected Result: The ghost text should display as: "You can write a message" ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Projects_CreateANewProject_Modal_Message_GhostText_Label);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify the Quick Message -----//

            //--- Expected Result: The quick message should be present ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Projects_CreateANewProject_Modal_QuickMessage_Buttons);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify adding quick message when there is already a message present on Message field -----//

            //--- Action: Input value on Message field ---//
            string message_input = "Message_" + util.RandomString();
            action.IDeleteValueOnFields(driver, Elements.MyAnalog_Projects_CreateANewProject_Modal_Message_InputBox);
            action.IType(driver, Elements.MyAnalog_Projects_CreateANewProject_Modal_Message_InputBox, message_input);

            //--- Action: Click on any of the quick message ---//
            action.IClick(driver, Elements.MyAnalog_Projects_CreateANewProject_Modal_QuickMessage_Buttons);

            //--- Expected Result: The message should be replaced by the selected quick message ---//
            test.validateStringInstance(driver, util.GetText(driver, Elements.MyAnalog_Projects_CreateANewProject_Modal_QuickMessage_Buttons), util.GetInputBoxValue(driver, Elements.MyAnalog_Projects_CreateANewProject_Modal_Message_InputBox));
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the X Button is Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the X Button is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the X Button is Present and Working as Expected in JP Locale")]
        public void MyAnalog_Projects_CreateANewProjectModal_VerifyXButton(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Projects URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_projects_url;

            //--- Action: Open myAnalog Projects using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);

            //--- Action: Click Create A New Project to open the pop-up ---//
            action.IClick(driver, Elements.MyAnalog_Projects_CreateANewProject_Button);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Close (x) icon -----//

            //--- Action: Click close (x) icon ---//
            action.IClick(driver, Elements.MyAnalog_Projects_CreateANewProject_Modal_X_Button);

            //--- Expected Result: The pop/up should close ---//
            test.validateElementIsNotPresent(driver, Elements.MyAnalog_Projects_CreateANewProject_Modal);
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Cancel Button is Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Cancel Button is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Cancel Button is Present and Working as Expected in JP Locale")]
        public void MyAnalog_Projects_CreateANewProjectModal_VerifyCancelButton(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Projects URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_projects_url;

            //--- Action: Open myAnalog Projects using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);

            //--- Action: Click Create A New Project to open the pop-up ---//
            action.IClick(driver, Elements.MyAnalog_Projects_CreateANewProject_Button);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify Cancel -----//

            //--- Action: Click Cancel ---//
            action.IClick(driver, Elements.MyAnalog_Projects_CreateANewProject_Modal_Cancel_Button);

            //--- Expected Result: The pop/up should close ---//
            test.validateElementIsNotPresent(driver, Elements.MyAnalog_Projects_CreateANewProject_Modal);
        }
    }
}