﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System;
using System.Threading;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_Dashboard_EngineerZoneSection : BaseSetUp
    {
        public MyAnalog_Dashboard_EngineerZoneSection() : base() { }

        //-- Login Credentials --//
        string withEz_username = "test_05032019@mailinator.com";
        string withEz_password = "Test_1234";
        string noEz_username = "myanalog_t3st_n0_ez_us3rn4me@mailinator.com";
        string noEz_password = "Test_1234";

        //--- URLs ---//
        string myAnalog_dashboard_url = "app";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the EngineerZone Section is Present and Working as Expected for Users that have an EngineerZone Username in EN Locale")]
        [TestCase("zh", TestName = "Verify that the EngineerZone Section is Present and Working as Expected for Users that have an EngineerZone Username in CN Locale")]
        [TestCase("jp", TestName = "Verify that the EngineerZone Section is Present and Working as Expected for Users that have an EngineerZone Username in JP Locale")]
        public void MyAnalog_Dashboard_EngineerZoneSection_VerifyForUsersThatHaveAnEngineerzoneUsername(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Dashboard URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_dashboard_url;

            //--- Action: Go to myAnalog dashboard ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid user account in the myAnalog Login page ---//
            action.ILogin(driver, withEz_username, withEz_password);

            //----- YOUR ENGINEERZONE USERNAME -----//

            //----- Dashboard Test Plan > Desktop Tab > R10 > T2: Verify the EZ username reflect in the profile (IQ-9776/ AL-14662) -----//

            //--- Expected Result: engineer zone name displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_EngineerZone_Username_Value);

            //----- RELATED SUPPORT FORUMS -----//

            //----- Dashboard Test Plan > Desktop Tab > R11 > T1: Verify Related Support Forums (IQ-9781/AL-16480) -----//

            //--- Expected Result: Related Support Forums section is displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_RelatedSupportForums_Section);

            if (util.CheckElement(driver, Elements.MyAnalog_Dashboard_RelatedSupportForums_Section_RelatedForums, 2))
            {
                //----- Dashboard Test Plan > Desktop Tab > R11 > T1.2: Verify the counter available for every related forums -----//

                //--- Expected Result: The counter appear ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_RelatedSupportForums_Section_RelatedForums_Counter);
            }
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the EngineerZone Section is Present and Working as Expected for Users that have No EngineerZone Username in EN Locale")]
        [TestCase("zh", TestName = "Verify that the EngineerZone Section is Present and Working as Expected for Users that have No EngineerZone Username in CN Locale")]
        [TestCase("jp", TestName = "Verify that the EngineerZone Section is Present and Working as Expected for Users that have No EngineerZone Username in JP Locale")]
        public void MyAnalog_Dashboard_EngineerZoneSection_VerifyForUsersThatHaveNoEngineerzoneUsername(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Dashboard URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_dashboard_url;

            //--- Action: Go to myAnalog dashboard ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid user account in the myAnalog Login page ---//
            action.ILogin(driver, noEz_username, noEz_password);

            //----- Dashboard Test Plan > Desktop Tab > R10 > T3: Verify the Prevent merging accounts in EZ: myAnalog and profiles changes (IQ-10589/AL-17396) -----//
            test.validateElementIsNotPresent(driver, Elements.MyAnalog_Dashboard_EngineerZone_Username_Value);

            //--- Action: Type a ezUsername that already exists. ---//
            action.IClick(driver, Elements.MyAnalog_Dashboard_EngineerZone_Username_InputBox);
            string engineerZoneUsername_input = null;
            if (Util.Configuration.Environment.Equals("dev"))
            {
                engineerZoneUsername_input = "dummy_user";
            }
            else if (Util.Configuration.Environment.Equals("production"))
            {
                engineerZoneUsername_input = "aries0803";
            }
            else
            {
                engineerZoneUsername_input = "legacytest222";
            }

            action.IDeleteValueOnFields(driver, Elements.MyAnalog_Dashboard_EngineerZone_Username_InputBox);
            action.IType(driver, Elements.MyAnalog_Dashboard_EngineerZone_Username_InputBox, engineerZoneUsername_input);

            //--- Action: click on check availability button ---//
            action.IClick(driver, Elements.MyAnalog_Dashboard_CheckAvailability_Button);
            Thread.Sleep(1000);

            //--- Expected Result: ezUsername section Displayed "X" which means this ezUserName is not available ---//
            test.validateImageIsNotBeBroken(driver, Elements.MyAnalog_Dashboard_Availability_X_Icon);
        }
    }
}