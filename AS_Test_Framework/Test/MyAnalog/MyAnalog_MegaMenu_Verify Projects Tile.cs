﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_MegaMenu_ProjectsTile : BaseSetUp
    {
        public MyAnalog_MegaMenu_ProjectsTile() : base() { }

        //--- Login Credentials ---//
        string username = "my_t3st_pr0j3cts_555@mailinator.com";
        string password = "Test_1234";

        //--- URLs ---//
        string myAnalog_projects_url = "app/projects";
        string myAnalog_projectDetail_page_url_format = "app/projects/";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Projects Tile is Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Projects Tile is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Projects Tile is Present and Working as Expected in JP Locale")]
        public void MyAnalog_MegaMenu_VerifyProjectsTile(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Projects URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_projects_url;

            action.Navigate(driver, myAnalog_url);

            //--- Action: Log in valid account in login page ---//
            action.ILogin(driver, username, password);

            //--- Action: Get the Name of the Latest Project ---//
            string latest_created_project_name = util.GetMyAnalogFirstProjectName(driver);

            //--- Action: Get the Count for the Created Projects ---//
            int actual_createdProjects_count = util.GetMyAnalogCreatedProjectsCount(driver);

            if (!driver.Url.Equals(myAnalog_url) && !driver.Url.Equals(myAnalog_url + "#"))
            {
                //--- Action: Go to myAnalog Projects ---//
                driver.Navigate().GoToUrl(myAnalog_url);
            }

            //--- Action: click myanalog tab ---//
            action.IExpandMyAnalogMegaMenu(driver);
            Thread.Sleep(1000);

            //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R2 > T2: Verify myPage Components -----//

            //--- Expected Result: The Project tiles is displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_MegaMenu_Projects_Tile);

            //--- Expected Result: The Create Project button component is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_MegaMenu_CreateAProject_Button);

            //----- PROJECTS TITLE -----//

            //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R3 > T1: Verify Project Tile components -----//

            //--- Expected Result: The Project link component is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_MegaMenu_Projects_Link);

            if (util.CheckElement(driver, Elements.MyAnalog_MegaMenu_Projects_Link, 2))
            {
                string projects_link = util.GetText(driver, Elements.MyAnalog_MegaMenu_Projects_Link);

                //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R3 > T2: Verify when clicking Project Link -----//

                //--- Expected Result: Project counter is correctly displayed ---//
                test.validateStringInstance(driver, util.ExtractNumber(driver, Elements.MyAnalog_MegaMenu_Projects_Link), actual_createdProjects_count.ToString());

                //--- Action: Click Project link ---//
                action.IOpenLinkInNewTab(driver, Elements.MyAnalog_MegaMenu_Projects_Link);

                //--- Expected Result: Page will redirect to project detail page ---//
                test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + "/" + myAnalog_projects_url);
            }

            //----- SEE ALL YOUR PROJECTS -----//

            //--- Action: Click myAnalog tab ---//
            action.IExpandMyAnalogMegaMenu(driver);
            Thread.Sleep(1000);

            //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R3 > T1: Verify Project Tile components -----//

            //--- Expected Result: The See all your Projects link component is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_MegaMenu_SeeAllYourProjects_Link);

            if (util.CheckElement(driver, Elements.MyAnalog_MegaMenu_SeeAllYourProjects_Link, 2))
            {
                //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R3 > T7: Verify See all your project link -----//
                string current_url = driver.Url;

                //--- Action: Click "See all your Projects" link ---//
                action.IClick(driver, Elements.MyAnalog_MegaMenu_SeeAllYourProjects_Link);

                if (driver.Url.Contains(current_url))
                {
                    driver.SwitchTo().Window(driver.WindowHandles.Last());
                }

                //--- Expected Result: Project detail page is displayed ---//      
                test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + "/" + myAnalog_projects_url);
            }

            //----- CREATED PROJECT -----//

            //--- Action: Click myAnalog tab ---//
            action.IExpandMyAnalogMegaMenu(driver);
            Thread.Sleep(1000);

            if (Locale.Equals("en"))
            {
                //----- Project & Project Detail Page Test Plan > Projects Page Tab > R8 > T1: Verify "Modified On" should be changed to "Modified" (IQ-9372/AL-16076) -----//

                //--- Expected Result: Changed to Modified ---//
                test.validateStringInstance(driver, util.GetText(driver, Elements.MyAnalog_MegaMenu_Created_Project_ModifiedDate_Label), "Modified");
            }
            else
            {
                //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R3 > T1: Verify Project Tile components -----//

                //--- Expected Result: The Modified Date component is available ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_MegaMenu_Created_Project_ModifiedDate_Label);
            }

            //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R3 > T1: Verify Project Tile components -----//

            //--- Expected Result: The Project Contents component is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_MegaMenu_Created_Project_Contents);

            //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R3 > T4: Verify Project content -----//

            //--- Expected Result: The latest project displays in the tile ---//
            test.validateStringIsCorrect(driver, Elements.MyAnalog_MegaMenu_Created_Project_Name_Link, latest_created_project_name);

            if (util.CheckElement(driver, Elements.MyAnalog_MegaMenu_Created_Project_Name_Link, 2))
            {
                string projectName_link = util.GetText(driver, Elements.MyAnalog_MegaMenu_Created_Project_Name_Link);

                //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R3 > T3: Verify Project Name link -----//
                string current_url = driver.Url;

                //--- Action: Click Project title link ---//
                action.IClick(driver, Elements.MyAnalog_MegaMenu_Created_Project_Name_Link);
                Thread.Sleep(1000);

                if (driver.Url.Contains(current_url))
                {
                    driver.SwitchTo().Window(driver.WindowHandles.Last());
                }

                //--- Expected Result: Page will redirect to project detail page ---//
                test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + "/" + myAnalog_projectDetail_page_url_format);
            }
        }
    }
}