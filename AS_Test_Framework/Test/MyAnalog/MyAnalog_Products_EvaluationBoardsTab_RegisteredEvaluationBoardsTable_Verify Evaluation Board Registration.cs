﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System;
using System.Drawing;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_Products_EvaluationBoardsTab_RegisteredEvaluationBoardsTable_EvaluationBoardRegistration : BaseSetUp
    {
        //--- myAnalog Products URL: ---//
        //--- DEV: https://my-dev.corpnt.analog.com/en/app/products ---//
        //--- QA: https://my-qa.corpnt.analog.com/en/app/products ---//
        //--- PROD: https://my.analog.com/en/app/products ---//

        public MyAnalog_Products_EvaluationBoardsTab_RegisteredEvaluationBoardsTable_EvaluationBoardRegistration() : base() { }

        //--- Login Credentials ---//
        string username = "myanalog_ev4l_b04rd_r3g_test@mailinator.com";
        string password = "Test_1234";

        //--- URLs ---//
        string myAnalog_products_url = "app/products";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Evaluation Board Registration is Working as Expected in EN Locale")]
        public void MyAnalog_Products_EvaluationBoardsTab_RegisteredEvaluationBoardsTable_VerifyEvaluationBoardRegistration(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- Action: Set the Window to a Larger Size since the Ellipsis Buttons are being cut in Small Screens ---//
            driver.Manage().Window.Size = new Size(1920, 1080);

            //--- myAnalog Products URL ---//
            string myAnalog_url = $"{Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt")}{Locale}/{myAnalog_products_url}";

            //--- Action: Open myAnalog Products using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);

            //--- Action: Select the Evaluation Boards Tab ---//
            action.IClick(driver, Elements.MyAnalog_Products_EvaluationBoards_Tab);

            //----- Hardware Evalboard Registration > Test Case Title: Verify if the "Register an evaluation board" button display in Evaluation boards tab. -----//

            //--- Expected Result: "Register an evaluation board" button displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Products_RegisterAnEvaluationBoard_Button);

            if (Locale.Equals("en"))
            {
                //--- Action: Remove All Registered Evaluation Boards in myAnalog if there's any ---//
                if (util.CheckElement(driver, Elements.MyAnalog_Products_RegisteredEvaluationBoards, 2))
                {
                    action.IRemoveAllRegisteredEvalBoardsInMyAnalog(driver, Locale);
                }

                //----- Hardware Evalboard Registration > Test Case Title: Verify if the page keep loading if the user register an already exists product (IQ-11310/AL-17943) -----//

                //--- Action: Click the "Register an evaluation board" button ---//
                //--- Action: Enter Product number in Product number field (e.g. AD603-EVALZ) ---//
                //--- Action: Click the "Register your evaluation board" button ---//
                string productNumber_input = "AD603-EVALZ";
                action.IRegisterAnEvalBoardInMyAnalog(driver, Locale, productNumber_input);

                //--- Expected Result: Evaluation board registered. ---//
                test.validateStringIsCorrect(driver, Elements.MyAnalog_Products_ModelNumber_Links, productNumber_input);
            }
        }
    }
}