﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System;
using System.Linq;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_Products_PartsTab_SavedProductsTable : BaseSetUp
    {
        //--- myAnalog Products URL: ---//
        //--- DEV: https://my-dev.corpnt.analog.com/en/app/products ---//
        //--- QA: https://my-qa.corpnt.analog.com/en/app/products ---//
        //--- PROD: https://my.analog.com/en/app/products ---//

        public MyAnalog_Products_PartsTab_SavedProductsTable() : base() { }

        //--- Login Credentials ---//
        string username = "myanalog_t3st_pr0ducts_555@mailinator.com";
        string password = "Test_1234";

        //--- URLs ---//
        string myAnalog_products_url = "app/products";
        string pdp_url_format = "/products/";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Saved Products are Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Saved Products are Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Saved Products are Present and Working as Expected in JP Locale")]
        public void MyAnalog_Products_PartsTab_SavedProductsTable_VerifySavedProducts(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Products URL ---//
            string myAnalog_url = $"{Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt")}{ Locale }/{myAnalog_products_url}";

            //--- Action: Open myAnalog Products using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);

            //--- Action: Select the Parts Tab ---//
            if (!util.CheckElement(driver, Elements.MyAnalog_Products_Selected_Parts_Tab, 2))
            {
                action.IClick(driver, Elements.MyAnalog_Products_Parts_Tab);
            }

            //----- Product Test Plan > MAR>Dashboard>Products Tab > R7 > T3: Verify saved products are separated and displayed in a dark blue table header. -----//

            //--- Expected Result: Each saved product uses/displays the Short Description ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Products_Saved_Products_ShortDescription);

            //--- Expected Result: Each saved product uses/displays the Status ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Products_Saved_Products_Status);

            //--- Expected Result: Each saved product uses/displays the Generic’s name (product number) ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Products_Saved_Products_GenericName_Link);

            if (util.CheckElement(driver, Elements.MyAnalog_Products_Saved_Products_GenericName_Link, 1))
            {
                //----- Product Test Plan > MAR>Dashboard>Products Tab > R7 > T3.1.2: Verify the product page displayed upon clicking the generic name (product number) link of the saved product. -----//

                //--- Action: Click the generic name (product number).---//
                action.IOpenLinkInNewTab(driver, Elements.MyAnalog_Products_Saved_Products_GenericName_Link);

                //--- Expected Result: Associated product page should be opened ---//
                if (Locale.Equals("cn") || Locale.Equals("zh"))
                {
                    test.validateStringInstance(driver, driver.Url, "analog.com/cn" + pdp_url_format);
                }
                else
                {
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + pdp_url_format);
                }

                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles.First());
            }

            //----- Product Test Plan > MAR>Dashboard>Products Tab > R5 > T1.1: Verify Product detail page icon (IQ-6667 / ALA-11943) -----//

            //--- Expected Result: Arrow is available after pruduct number ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Products_Saved_Products_GenericName_Arrow_Icon);

            if (util.CheckElement(driver, Elements.MyAnalog_Products_Saved_Products_GenericName_Arrow_Icon, 1))
            {
                //--- Action: Click arrow ---//
                action.IOpenLinkInNewTab(driver, Elements.MyAnalog_Products_Saved_Products_GenericName_Arrow_Icon);

                //--- Expected Result: product detail pages is opened ---//
                if (Locale.Equals("cn") || Locale.Equals("zh"))
                {
                    test.validateStringInstance(driver, driver.Url, "analog.com/cn" + pdp_url_format);
                }
                else
                {
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + pdp_url_format);
                }
            }
        }
    }
}