﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System;
using System.Threading;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_Projects_ProjectListing_Pagination : BaseSetUp
    {
        //--- myAnalog Projects URL: ---//
        //--- DEV: https://my-dev.corpnt.analog.com/en/app/projects ---//
        //--- QA: https://my-qa.corpnt.analog.com/en/app/projects ---//
        //--- PROD: https://my.analog.com/en/app/projects ---//

        public MyAnalog_Projects_ProjectListing_Pagination() : base() { }

        //----- Note: Please don't edit/delete any details on the Accounts below -----//

        //--- Login Credentials (Account with More than 10 Projects) ---//
        string username_projectWithMoreThan10Projects = "my_t3st_pr0j3ct_p4g1n4t10n_555@mailinator.com";
        string password_projectWithMoreThan10Projects = "Test_1234";

        //--- URLs ---//
        string myAnalog_projects_url = "app/projects";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Pagination is Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Pagination is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Pagination is Present and Working as Expected in JP Locale")]
        public void MyAnalog_Projects_ProjectListing_VerifyPagination(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Projects URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_projects_url;

            //--- Action: Open myAnalog Projects using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username_projectWithMoreThan10Projects, password_projectWithMoreThan10Projects);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify the maximum number of Project to display in Projects Dashboard/Landing Page -----//

            //--- Expected Result: Project to display should have a limit of 10 items ---//
            test.validateCountIsLessOrEqual(driver, util.GetCount(driver, Elements.MyAnalog_Projects_Created_Projects), 10);

            //--- Expected Result: Pagination button "Previous" should be displayed. ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Projects_Previous_Button);

            //--- Expected Result: Pagination button "Next" should be displayed. ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Projects_Next_Button);

            string lastPage_label = util.GetText(driver, Elements.MyAnalog_Projects_Last_Page_Button);

            //--- Action: Click any page number in the pagnation ---//
            action.IClick(driver, Elements.MyAnalog_Projects_Last_Page_Button);

            //--- Expected Result: Verify that the pagination is working correctly ---//
            test.validateStringIsCorrect(driver, Elements.MyAnalog_Projects_Current_Page_Selected_Button, lastPage_label);
        }
    }
}