﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_Projects_InviteModal_ProjectInvite : BaseSetUp
    {
        //--- myAnalog Projects URL: ---//
        //--- DEV: https://my-dev.corpnt.analog.com/en/app/projects ---//
        //--- QA: https://my-qa.corpnt.analog.com/en/app/projects ---//
        //--- PROD: https://my.analog.com/en/app/projects ---//

        public MyAnalog_Projects_InviteModal_ProjectInvite() : base() { }

        //----- Note: Please don't edit/delete any details on the Accounts below -----//

        //--- Login Credentials (Used for Project Invite) ---//
        string username_projectInvite = "my_t3st_pr0j3ct_1nv1t3_555@mailinator.com";
        string password_projectInvite = "Test_1234";

        //--- URLs ---//
        string myAnalog_projects_url = "app/projects";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Project Invite is Working as Expected in EN Locale")]
        //[TestCase("zh", TestName = "Verify that the Project Invite is Working as Expected in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Project Invite is Working as Expected in JP Locale")]
        public void MyAnalog_Projects_InviteModal_VerifyProjectInvite(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Projects URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_projects_url;

            //--- Action: Open myAnalog Projects using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username_projectInvite, password_projectInvite);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify Message should be an optional field -----//

            //--- Action: Click the Invite button ---//
            action.IClick(driver, Elements.MyAnalog_Projects_Invite_Button);

            //--- Action: Do not input value on Message field ---//
            action.IDeleteValueOnFields(driver, Elements.MyAnalog_Projects_Invite_Modal_Message_InputBox);

            //--- Action: Input valid values on other fields ---//
            string invitees_input = "t3st_" + Util.Configuration.Environment.ToLower() + "_ProjectInvitee_" + util.GenerateRandomNumber() + "@mailinator.com";
            action.IDeleteValueOnFields(driver, Elements.MyAnalog_Projects_Invite_Modal_Invitees_InputBox);
            action.IType(driver, Elements.MyAnalog_Projects_Invite_Modal_Invitees_InputBox, invitees_input);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify the Email field with valid single email address (IQ-7416/ALA-11964) -----//

            //--- Action: Click Invite button ---//
            action.IClick(driver, Elements.MyAnalog_Projects_Invite_Modal_Invite_Button);

            //--- Expected Result: The System displays 'Invite' popup box ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Projects_Invite_Modal);

            if (Locale.Equals("en"))
            {
                //--- Expected Result: With below Confirmation message: Your invitation successfully sent to <Invitee Email Address> ---//
                test.validateStringIsCorrect(driver, Elements.MyAnalog_Projects_CreatedNewInvite_Modal_ConfirmationMessage_Text, "Your invitation successfully sent to " + invitees_input);
            }
            else
            {
                test.validateStringInstance(driver, util.GetText(driver, Elements.MyAnalog_Projects_CreatedNewInvite_Modal_ConfirmationMessage_Text), invitees_input);
            }

            //--- Action: Go to the Mailbox of the Invitee ---//
            action.ICheckMailinatorEmail(driver, invitees_input);

            //--- Expected Result: A Project invite email is received ---//   
            test.validateElementIsPresent(driver, Elements.Mailinator_LatestEmail);
        }
    }
}