﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System;
using System.Threading;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_Projects_ProjectInvite : BaseSetUp
    {
        //--- myAnalog Projects URL: ---//
        //--- DEV: https://my-dev.corpnt.analog.com/en/app/projects ---//
        //--- QA: https://my-qa.corpnt.analog.com/en/app/projects ---//
        //--- PROD: https://my.analog.com/en/app/projects ---//

        public MyAnalog_Projects_ProjectInvite() : base() { }

        //----- Note: Please don't edit/delete any details on the Accounts below -----//

        //--- Login Credentials (Project Invitee that doesn't have a Project) ---// 
        string username_new_invitee = "my_t3st_pr0ject_n3w_1nv1t33_555@mailinator.com";
        string password_new_invitee = "Test_1234";

        //--- Login Credentials (Project Invitee that has a Project) ---// 
        string username_invitee = "my_t3st_pr0ject_1nv1t33_555@mailinator.com";
        string password_invitee = "Test_1234";

        //--- Login Credentials (Used for Accepting Project Invites) ---// 
        string username_acceptProjectInvite = "my_t3st_acc3pt_pr0ject_1nv1t3_555@mailinator.com";
        string password_acceptProjectInvite = "Test_1234";

        //--- Login Credentials (Used for Declining Project Invites) ---// 
        string username_declineProjectInvite = "my_t3st_d3cl1n3_pr0ject_1nv1t3_555@mailinator.com";
        string password_declineProjectInvite = "Test_1234";

        //--- Login Credentials (Project Owner) ---// 
        string username_owner = "my_t3st_pr0j3ct_0wn3r_555@mailinator.com";
        string password_owner = "Test_1234";

        //--- URLs ---//
        string myAnalog_projects_url = "app/projects";
        string myAnalog_logout_url = "app/logout";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the New Invites are Present and Working as Expected for New Users in EN Locale")]
        [TestCase("zh", TestName = "Verify that the New Invites are Present and Working as Expected for New Users in CN Locale")]
        [TestCase("jp", TestName = "Verify that the New Invites are Present and Working as Expected for New Users in JP Locale")]
        public void MyAnalog_Projects_ProjectInvite_VerifyNewInvitesForNewUsers(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Projects URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_projects_url;

            //--- Action: Open myAnalog Projects using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials (Project Participant) and click login ---//
            action.ILogin(driver, username_new_invitee, password_new_invitee);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify new user display when invited in a project -----//

            //--- Expected Result: The You are invited message is available ---//
            //--- NOTE: AL-18533 has been logged for the Issue where Project Invites are Not Displaying for Users that doesn't have Projects. ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Projects_NewInvites_YouAreInvited_Label);

            //--- Expected Result: The Poject Name is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Projects_NewInvites_Project_Name_Label);

            //--- Expected Result: The Accept button is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Projects_NewInvites_Accept_Button);

            //--- Expected Result: The Decline link is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Projects_NewInvites_Decline_Button);
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the New Invites are Present and Working as Expected for Existing Users in EN Locale")]
        [TestCase("zh", TestName = "Verify that the New Invites are Present and Working as Expected for Existing Users in CN Locale")]
        [TestCase("jp", TestName = "Verify that the New Invites are Present and Working as Expected for Existing Users in JP Locale")]
        public void MyAnalog_Projects_ProjectInvite_VerifyNewInvitesForExistingUsers(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Projects URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_projects_url;

            //--- Action: Open myAnalog Projects using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials (Project Participant) and click login ---//
            action.ILogin(driver, username_invitee, password_invitee);

            //--- Action: Create a Project Invite in myAnalog if there's none ---//
            if (!util.CheckElement(driver, Elements.MyAnalog_Projects_NewInvites, 2))
            {
                Console.WriteLine("Invite the Project Participant to a Project:");

                //--- Action: Sign Out of myAnalog ---//
                string logout_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_logout_url;
                driver.Navigate().GoToUrl(logout_url);
                Console.WriteLine("\tGo to Log Out URL: " + logout_url);

                //--- Action: Open myAnalog Projects using direct link ---//
                Console.Write("\t");
                action.Navigate(driver, myAnalog_url);

                //--- Action: Login valid credentials (Project Owner) and click login ---//
                action.ILogin(driver, username_owner, password_owner);

                //--- Action: Create a Project Invite in myAnalog ---//
                Console.Write("\t");
                action.ICreateAProjectInviteInMyAnalog(driver, Locale, username_invitee);

                //--- Action: Sign Out of myAnalog ---//
                driver.Navigate().GoToUrl(logout_url);
                Console.WriteLine("\tGo to Log Out URL: " + logout_url);

                //--- Action: Open myAnalog Projects using direct link ---//
                Console.Write("\t");
                action.Navigate(driver, myAnalog_url);

                //--- Action: Login valid credentials (Project Participant) and click login ---//
                action.ILogin(driver, username_invitee, password_invitee);
            }

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify existing user display when invited in a project -----//

            //--- Expected Result: The You are invited button is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Projects_NewInvites_YouAreInvited_Label);

            //--- Expected Result: The Poject Name is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Projects_NewInvites_Project_Name_Label);

            //--- Expected Result: The Accept button is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Projects_NewInvites_Accept_Button);

            //--- Expected Result: The Decline link is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Projects_NewInvites_Decline_Button);
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Project Listing are Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Project Listing are Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Project Listing are Present and Working as Expected in JP Locale")]
        public void MyAnalog_Projects_ProjectInvite_VerifyProjectListing(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Projects URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_projects_url;

            //--- Action: Open myAnalog Projects using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials (Project Participant) and click login ---//
            action.ILogin(driver, username_invitee, password_invitee);

            //--- Action: Create a Project Invite in myAnalog if there's none ---//
            if (!util.CheckElement(driver, Elements.MyAnalog_Projects_NewInvites, 2))
            {
                Console.WriteLine("Invite the Project Participant to a Project:");

                //--- Action: Sign Out of myAnalog ---//
                string logout_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_logout_url;
                driver.Navigate().GoToUrl(logout_url);
                Console.WriteLine("\tGo to Log Out URL: " + logout_url);

                //--- Action: Open myAnalog Projects using direct link ---//
                Console.Write("\t");
                action.Navigate(driver, myAnalog_url);

                //--- Action: Login valid credentials (Project Owner) and click login ---//
                action.ILogin(driver, username_owner, password_owner);

                //--- Action: Create a Project Invite in myAnalog ---//
                Console.Write("\t");
                action.ICreateAProjectInviteInMyAnalog(driver, Locale, username_invitee);

                //--- Action: Sign Out of myAnalog ---//
                driver.Navigate().GoToUrl(logout_url);
                Console.WriteLine("\tGo to Log Out URL: " + logout_url);

                //--- Action: Open myAnalog Projects using direct link ---//
                Console.Write("\t");
                action.Navigate(driver, myAnalog_url);

                //--- Action: Login valid credentials (Project Participant) and click login ---//
                action.ILogin(driver, username_invitee, password_invitee);
            }

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify Details of accepted Project -----//

            //--- Expected Result: The Project Name is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Projects_Project_Name_Link);

            //--- Expected Result: The Created by is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Projects_CreatedBy_Name_Label);

            //--- Expected Result: The Modified by is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Projects_ModifiedBy_Name_Label);

            //--- Expected Result: The list of project components are available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Projects_Components);

            //--- Expected Result: The Invite button is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Projects_Invite_Button);

            //--- Expected Result: The Project details button is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Projects_ProjectDetails_Buttons);
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that Accepting a Project Invite is Working as Expected in EN Locale")]
        public void MyAnalog_Projects_ProjectInvite_VerifyAcceptingProjectInvite(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Projects URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_projects_url;

            //--- Action: Open myAnalog Projects using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials (Project Participant) and click login ---//
            action.ILogin(driver, username_acceptProjectInvite, password_acceptProjectInvite);

            //--- Action: Create a Project Invite in myAnalog if there's none ---//
            if (!util.CheckElement(driver, Elements.MyAnalog_Projects_NewInvites, 2))
            {
                Console.WriteLine("Invite the Project Participant to a Project:");

                //--- Action: Sign Out of myAnalog ---//
                string logout_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_logout_url;
                driver.Navigate().GoToUrl(logout_url);
                Console.WriteLine("\tGo to Log Out URL: " + logout_url);

                //--- Action: Open myAnalog Projects using direct link ---//
                Console.Write("\t");
                action.Navigate(driver, myAnalog_url);

                //--- Action: Login valid credentials (Project Owner) and click login ---//
                action.ILogin(driver, username_owner, password_owner);

                //--- Action: Create a Project in myAnalog ---//
                Console.Write("\t");
                action.ICreateAProjectInMyAnalog(driver, Locale);

                //--- Action: Create a Project Invite in myAnalog ---//
                Console.Write("\t");
                action.ICreateAProjectInviteInMyAnalog(driver, Locale, username_acceptProjectInvite);

                //--- Action: Sign Out of myAnalog ---//
                driver.Navigate().GoToUrl(logout_url);
                Console.WriteLine("\tGo to Log Out URL: " + logout_url);

                //--- Action: Open myAnalog Projects using direct link ---//
                Console.Write("\t");
                action.Navigate(driver, myAnalog_url);

                //--- Action: Login valid credentials (Project Participant) and click login ---//
                action.ILogin(driver, username_acceptProjectInvite, password_acceptProjectInvite);
            }

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify Details of accepted Project -----//
            string newInvites_projectName_label = util.GetText(driver, Elements.MyAnalog_Projects_NewInvites_Project_Name_Label);

            //--- Action: Click Accept button ---//
            action.IClick(driver, Elements.MyAnalog_Projects_NewInvites_Accept_Button);

            driver.Navigate().Refresh();
            Thread.Sleep(1000);
            Console.WriteLine("\tRefresh the Page.");

            //--- Expected Result: Project successfully saved ---//
            string first_projectName_label = util.GetMyAnalogFirstProjectName(driver);
            test.validateStringInstance(driver, newInvites_projectName_label, first_projectName_label);

            //--- Action: Verify email received ---//
            action.ICheckMailinatorEmail(driver, username_owner);

            //--- Expected Result: Project owner will receive an email (IQ-9089/AL-15870) ---//
            test.validateElementIsPresent(driver, Elements.Mailinator_LatestEmail);
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that Declining a Project Invite is Working as Expected in EN Locale")]
        public void MyAnalog_Projects_ProjectInvite_VerifyDecliningProjectInvite(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Projects URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_projects_url;

            //--- Action: Open myAnalog Projects using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials (Project Participant) and click login ---//
            action.ILogin(driver, username_declineProjectInvite, password_declineProjectInvite);

            //--- Action: Create a Project Invite in myAnalog if there's none ---//
            if (!util.CheckElement(driver, Elements.MyAnalog_Projects_NewInvites, 2))
            {
                Console.WriteLine("Invite the Project Participant to a Project:");

                //--- Action: Sign Out of myAnalog ---//
                string logout_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_logout_url;
                driver.Navigate().GoToUrl(logout_url);
                Console.WriteLine("\tGo to Log Out URL: " + logout_url);

                //--- Action: Open myAnalog Projects using direct link ---//
                Console.Write("\t");
                action.Navigate(driver, myAnalog_url);

                //--- Action: Login valid credentials (Project Owner) and click login ---//
                action.ILogin(driver, username_owner, password_owner);

                //--- Action: Create a Project Invite in myAnalog ---//
                Console.Write("\t");
                action.ICreateAProjectInviteInMyAnalog(driver, Locale, username_declineProjectInvite);

                //--- Action: Sign Out of myAnalog ---//
                driver.Navigate().GoToUrl(logout_url);
                Console.WriteLine("\tGo to Log Out URL: " + logout_url);

                //--- Action: Open myAnalog Projects using direct link ---//
                Console.Write("\t");
                action.Navigate(driver, myAnalog_url);

                //--- Action: Login valid credentials (Project Participant) and click login ---//
                action.ILogin(driver, username_declineProjectInvite, password_declineProjectInvite);
            }

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify Clicking decline link (IQ-9089/AL-15870) -----//
            int newInvites_count = util.GetCount(driver, Elements.MyAnalog_Projects_NewInvites);

            //--- Action: Click decline link ---//
            action.IClick(driver, Elements.MyAnalog_Projects_NewInvites_Decline_Button);
            Thread.Sleep(2000);

            //--- Expected Result: Project disappear ---//
            if (newInvites_count < 2)
            {
                test.validateElementIsNotPresent(driver, Elements.MyAnalog_Projects_NewInvites);
            }
            else
            {
                test.validateCountIsLess(driver, util.GetCount(driver, Elements.MyAnalog_Projects_NewInvites), newInvites_count);
            }
            //--- Action: Verify email received ---//
            action.ICheckMailinatorEmail(driver, username_owner);

            //--- Expected Result: Project owner will receive an email (IQ-9089/AL-15870) ---//
            test.validateElementIsPresent(driver, Elements.Mailinator_LatestEmail);
        }
    }
}