﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_Projects_ProjectDetailPage_ProjectNotes : BaseSetUp
    {
        //--- myAnalog Projects URL: ---//
        //--- DEV: https://my-dev.corpnt.analog.com/en/app/projects ---//
        //--- QA: https://my-qa.corpnt.analog.com/en/app/projects ---//
        //--- PROD: https://my.analog.com/en/app/projects ---//

        public MyAnalog_Projects_ProjectDetailPage_ProjectNotes() : base() { }

        //----- Note: Please don't edit/delete any details on the Accounts below -----//

        //--- Login Credentials ---// 
        string username = "my_t3st_pr0j3ct_d3t41ls_555@mailinator.com";
        string password = "Test_1234";

        //--- Login Credentials (Used for Adding Notes) ---// 
        string username_addNotes = "my_t3st_3d1t_pr0j3ct_d3t41ls_555@mailinator.com";
        string password_addNotes = "Test_1234";

        //--- URLs ---//
        string myAnalog_projects_url = "app/projects";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that Project Notes is Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that Project Notes is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that Project Notes is Present and Working as Expected in JP Locale")]
        public void MyAnalog_Projects_ProjectDetailPage_VerifyProjectNotes(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Projects URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_projects_url;

            //--- Action: Open myAnalog Projects using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);

            //--- Action: Click Project Detail button ---//
            action.IClick(driver, Elements.MyAnalog_Projects_ProjectDetails_Buttons);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Navigate to Project Details Page -----//

            //--- Expected Result: The Project Note Section component is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_ProjectDetailPage_ProjectNotes_Section);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify Projet Notes -----//

            //--- Expected Result: The Edit option is displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_ProjectDetailPage_ProjectNotes_Edit_Button);

            //--- Expected Result: The Comment section textbox is displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_ProjectDetailPage_ProjectNotes_Description_InputBox);

            //--- Expected Result: The Add note button is displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_ProjectDetailPage_ProjectNotes_AddNote_Button);
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that Adding Notes is Working as Expected in EN Locale")]
        public void MyAnalog_Projects_ProjectDetailPage_ProjectNotes_VerifyAddingNotes(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Projects URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_projects_url;

            //--- Action: Open myAnalog Projects using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username_addNotes, password_addNotes);

            //--- Action: Click Project Detail button ---//
            action.IClick(driver, Elements.MyAnalog_Projects_ProjectDetails_Buttons);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify Projet Notes -----//

            //--- Action: Enter comment on textbox ---//
            string description_input = "Note_" + util.GenerateRandomString();
            action.IDeleteValueOnFields(driver, Elements.MyAnalog_ProjectDetailPage_ProjectNotes_Description_InputBox);
            action.IType(driver, Elements.MyAnalog_ProjectDetailPage_ProjectNotes_Description_InputBox, description_input);

            //--- Action: Click Add Note button ---//
            action.IClick(driver, Elements.MyAnalog_ProjectDetailPage_ProjectNotes_AddNote_Button);

            //--- Expected Result: Contributor name is displayed ---//
            test.validateStringIsCorrect(driver, Elements.MyAnalog_ProjectDetailPage_Last_SavedNote_Contributor_Label, util.GetText(driver, Elements.MyAnalog_Profile_Username_Label));

            //--- Expected Result: Comments is successfully added ---//
            test.validateStringIsCorrect(driver, Elements.MyAnalog_ProjectDetailPage_Last_SavedNote_Comment_Label, description_input);
        }
    }
}