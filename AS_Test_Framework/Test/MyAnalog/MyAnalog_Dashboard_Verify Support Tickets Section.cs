﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System;
using System.Linq;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_Dashboard_SupportTicketsSection : BaseSetUp
    {
        public MyAnalog_Dashboard_SupportTicketsSection() : base() { }

        //-- Login Credentials --//
        string username = "myanalog_t3st_555@mailinator.com";
        string password = "Test_1234";

        //--- URLs ---//
        string myAnalog_dashboard_url = "app";
        string myAnalog_supportTickets_url = "app/support";
        string myAnalog_supportTicketDetail_page_url_format = "app/support/";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Support Tickets Label is Present and Working as Expected for Accounts with Created Support Tickets in EN Locale")]
        public void MyAnalog_Dashboard_SupportTicketsSection_AccountsWithCreatedSupportTickets_VerifySupportTicketsLabel(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Support Tickets URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_supportTickets_url;

            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid user account in the myAnalog Login page ---//
            action.ILogin(driver, username, password);

            //--- Action: Get the Count for the Created Support Tickets ---//
            int actual_createdSupportTickets_count = util.GetMyAnalogCreatedSupportTicketsCount(driver);

            //--- myAnalog Dashboard URL ---//
            myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_dashboard_url;

            //--- Action: Go to myAnalog dashboard ---//
            driver.Navigate().GoToUrl(myAnalog_url);
            Console.WriteLine("Go to: " + myAnalog_url);

            //----- Tech Support Test Plan > Test Case Title: Verify Support Ticket Title display -----//

            //--- Expected Result: Support Ticket Title is displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_SupportTickets_Label);

            if (util.CheckElement(driver, Elements.MyAnalog_Dashboard_SupportTickets_Label, 2))
            {
                //----- Tech Support Test Plan > Test Case Title: Verify Support Ticket counter -----//

                //--- Expected Result: The number of Support Ticket is reflect in the Support Ticket counter ---//
                test.validateStringInstance(driver, util.ExtractNumber(driver, Elements.MyAnalog_Dashboard_SupportTickets_Label), actual_createdSupportTickets_count.ToString());
            }
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the See all your support tickets Link is Present and Working as Expected for Accounts with Created Support Tickets in EN Locale")]
        public void MyAnalog_Dashboard_SupportTicketsSection_AccountsWithCreatedSupportTickets_VerifySeeAllYourSupportTicketsLink(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Dashboard URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_dashboard_url;

            //--- Action: Go to myAnalog dashboard ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid user account in the myAnalog Login page ---//
            action.ILogin(driver, username, password);

            //----- Tech Support Test Plan > Test Case Title: Verify Support Tickets Section when account has transaction already -----//

            //--- Expected Result: Support Tickets section contains the See all your support tickets link ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_SeeAllYourSupportTickets_Link);

            if (util.CheckElement(driver, Elements.MyAnalog_Dashboard_SeeAllYourSupportTickets_Link, 2))
            {
                //----- Tech Support Test Plan > Test Case Title: Verift when navigation when clicking "See all your support tickets" link -----//

                //--- Action: Click "See all your support tickets" link  ---//
                action.IOpenLinkInNewTab(driver, Elements.MyAnalog_Dashboard_SeeAllYourSupportTickets_Link);

                //--- Expected Result: goes to “Support Tickets” overview page ---//
                test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + "/" + myAnalog_supportTickets_url);
            }
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Support Ticket Details are Present and Working as Expected for Accounts with Created Support Tickets in EN Locale")]
        public void MyAnalog_Dashboard_SupportTicketsSection_AccountsWithCreatedSupportTickets_VerifySupportTicketDetails(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Support Tickets URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_supportTickets_url;

            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid user account in the myAnalog Login page ---//
            action.ILogin(driver, username, password);

            //--- Action: Get the Case Title of the Latest Support Ticket ---//
            string latest_created_supportTicket_caseTitle = util.GetMyAnalogFirstCreatedSupportTicketCaseTitle(driver);

            //--- myAnalog Dashboard URL ---//
            myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_dashboard_url;

            //--- Action: Go to myAnalog dashboard ---//
            driver.Navigate().GoToUrl(myAnalog_url);
            Console.WriteLine("Go to: " + myAnalog_url);

            //----- SUPPORT TICKET CASE TITLE LINK -----//

            //----- Tech Support Test Plan > Test Case Title: Verify Date modified -----//

            //--- Expected Result: Date modified is displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_SupportTicket_ModifiedDate);

            //----- Tech Support Test Plan > Test Case Title: Verify Support Ticket details and counter -----//

            //--- Expected Result: Support Ticket title is correct ---//      
            test.validateStringIsCorrect(driver, Elements.MyAnalog_Dashboard_SupportTicket_CaseTitle_Link, latest_created_supportTicket_caseTitle);

            if (util.CheckElement(driver, Elements.MyAnalog_Dashboard_SupportTicket_CaseTitle_Link, 2))
            {
                //----- Tech Support Test Plan > Test Case Title: Verify when clicking Support Ticket title -----//

                //--- Action: Click Support Ticket title link ---//
                action.IOpenLinkInNewTab(driver, Elements.MyAnalog_Dashboard_SupportTicket_CaseTitle_Link);

                //--- Expected Result: Page will redirect to Support Ticket Detail Page ---//
                test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + "/" + myAnalog_supportTicketDetail_page_url_format);

                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles.Last());
            }

            //----- SUPPORT TICKET STATISTICS -----//

            //----- Tech Support Test Plan > Test Case Title: Verify all sections as flags on Support Tickets widget -----//

            //--- Expected Result: Case number is displayed. ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_CaseNumber_Statistic);

            //--- Expected Result: Primary Product is displayed. ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_PrimaryProduct_Statistic);

            //--- Expected Result: Status is displayed. ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_Status_Statistic);
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Left Arrow and Right Arrow are Present and Working as Expected for Accounts with Created Support Tickets in EN Locale")]
        public void MyAnalog_Dashboard_SupportTicketsSection_AccountsWithCreatedSupportTickets_VerifyLeftArrowAndRightArrow(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Dashboard URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_dashboard_url;

            //--- Action: Go to myAnalog dashboard ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid user account in the myAnalog Login page ---//
            action.ILogin(driver, username, password);

            //----- LEFT ARROW -----//

            //----- Tech Support Test Plan > Test Case Title: Verify display when clicking previous arrow -----//
            string previous_selected_supportTicketCaseTitle = util.GetText(driver, Elements.MyAnalog_Dashboard_SupportTicket_CaseTitle_Link);

            //--- Action: Click previos arrow ---//
            action.IClick(driver, Elements.MyAnalog_Dashboard_SupportTickets_Left_Arrow_Button);

            //--- Expected Result: will go back to previosly Support Ticket ---//
            test.validateStringChanged(driver, previous_selected_supportTicketCaseTitle, util.GetText(driver, Elements.MyAnalog_Dashboard_SupportTicket_CaseTitle_Link));

            //--- Action: Go to myAnalog dashboard ---//
            driver.Navigate().Refresh();
            Console.WriteLine("Refresh the Page.");

            //----- RIGHT ARROW -----//

            //----- Tech Support Test Plan > Test Case Title: Verify display when clicking Next arrow -----//
            previous_selected_supportTicketCaseTitle = util.GetText(driver, Elements.MyAnalog_Dashboard_SupportTicket_CaseTitle_Link);

            //--- Action: Click next arrow ---//
            action.IClick(driver, Elements.MyAnalog_Dashboard_SupportTickets_Right_Arrow_Button);

            //--- Expected Result: Next Support Ticket will be displayed ---//
            test.validateStringChanged(driver, previous_selected_supportTicketCaseTitle, util.GetText(driver, Elements.MyAnalog_Dashboard_SupportTicket_CaseTitle_Link));
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Create a New Support Ticket Link is Present and Working as Expected for Accounts with Created Support Tickets in EN Locale")]
        public void MyAnalog_Dashboard_SupportTicketsSection_AccountsWithCreatedSupportTickets_VerifyCreateANewSupportTicketLink(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Dashboard URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_dashboard_url;

            //--- Action: Go to myAnalog dashboard ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid user account in the myAnalog Login page ---//
            action.ILogin(driver, username, password);

            //----- Tech Support Test Plan > Test Case Title: Verify Support Tickets Section when account has transaction already -----//

            //--- Expected Result: Support Tickets section contains the Create a New Support Ticket ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_CreateANewSupportTicket_Link);

            //if (util.CheckElement(driver, Elements.MyAnalog_Dashboard_CreateANewSupportTicket_Link, 2))
            //{
            //    //----- Tech Support Test Plan > Test Case Title: Verify when creating new support ticket -----//

            //    //--- Action: Click Create a New Support Ticket link ---//
            //    action.IClick(driver, Elements.MyAnalog_Dashboard_CreateANewSupportTicket_Link);

            //    //--- Expected Result: Page will redirect to Technical Support Form ---//
            //    //
            //}
        }
    }
}