﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System;
using System.Threading;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_Resources_SavedTab_Pagination : BaseSetUp
    {
        //--- myAnalog Resources URL: ---//
        //--- DEV: https://my-dev.corpnt.analog.com/en/app/resources ---//
        //--- QA: https://my-qa.corpnt.analog.com/en/app/resources ---//
        //--- PROD: https://my.analog.com/en/app/resources ---//

        public MyAnalog_Resources_SavedTab_Pagination() : base() { }

        //--- Login Credentials ---//
        string username = "myanalog_t3st_r3s0urc3s_555@mailinator.com";
        string password = "Test_1234";

        //--- URLs ---//
        string myAnalog_resources_url = "app/resources";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Previous Button and Next Button are Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Previous Button and Next Button are Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Previous Button and Next Button are Present and Working as Expected in JP Locale")]
        public void MyAnalog_Resources_SavedTab_Pagination_VerifyPreviousButtonAndNextButton(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Resources URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_resources_url;

            //--- Action: Open resources direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);

            //----- Resources Test Plan > Desktop Tab > R4 > T1: Verify Pagination in Saved Resourses tab (IQ-6365/AL-13329) -----//

            //--- Expected Result: The maximum display of items displayed should be 10 resources ---//
            test.validateCountIsEqual(driver, 10, util.GetCount(driver, Elements.MyAnalog_Resources_Saved_Resources_Rows));

            //----- NEXT BUTTON -----//

            //----- Resources Test Plan > Desktop Tab > R4 > T1: Verify Pagination in Saved Resourses tab (IQ-6365/AL-13329) -----//

            //--- Expected Result: There's a button for Next Page for more than 10 resources ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Resources_Next_Button);

            //--- Action: Get the Current Page Selected ---//
            int currentPage_selected = Int32.Parse(util.GetText(driver, Elements.MyAnalog_Resources_Current_Page_Selected));

            //--- Action: Click the Next button ---//
            action.IClick(driver, Elements.MyAnalog_Resources_Next_Button);

            //--- Expected Result: The system will show the next page upon clicking the Next button ---//
            test.validateStringIsCorrect(driver, Elements.MyAnalog_Resources_Current_Page_Selected, (currentPage_selected + 1).ToString());

            //----- PREVIOUS BUTTON -----//

            //--- Expected Result: There's a button for Previous Page for more than 10 resources ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Resources_Previous_Button);

            //--- Action: Get the Current Page Selected ---//
            currentPage_selected = Int32.Parse(util.GetText(driver, Elements.MyAnalog_Resources_Current_Page_Selected));

            //--- Action: Click the Previous button ---//
            action.IClick(driver, Elements.MyAnalog_Resources_Previous_Button);

            //--- Expected Result: The system will show the previous page upon clicking the Previous button ---//
            test.validateStringIsCorrect(driver, Elements.MyAnalog_Resources_Current_Page_Selected, (currentPage_selected - 1).ToString());
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Last Page Button is Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Last Page Button is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Last Page Button is Present and Working as Expected in JP Locale")]
        public void MyAnalog_Resources_SavedTab_Pagination_VerifyLastPageButton(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Resources URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_resources_url;

            //--- Action: Open resources direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);

            //----- Resources Test Plan > Desktop Tab > R4 > T1: Verify Pagination in Saved Resourses tab (IQ-6365/AL-13329) -----//

            //--- Expected Result: The last page link is shown ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Resources_Last_Page_Button);

            string lastPage_text = util.GetText(driver, Elements.MyAnalog_Resources_Last_Page_Button);

            //--- Action: Click last page link ---//
            action.IClick(driver, Elements.MyAnalog_Resources_Last_Page_Button);

            //--- Expected Result: The system will show the last page upon clicking last page link ---//
            test.validateStringIsCorrect(driver, Elements.MyAnalog_Resources_Current_Page_Selected, lastPage_text);
        }
    }
}