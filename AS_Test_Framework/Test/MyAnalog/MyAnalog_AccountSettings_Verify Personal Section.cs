﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System;
using System.Threading;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_AccountSettings_PersonalSection : BaseSetUp
    {
        //--- myAnalog Account Settings URL: ---//
        //--- DEV: https://my-dev.corpnt.analog.com/en/app/account ---//
        //--- QA: https://my-qa.corpnt.analog.com/en/app/account ---//
        //--- PROD: https://my.analog.com/en/app/account ---//

        public MyAnalog_AccountSettings_PersonalSection() : base() { }

        //--- Login Credentials ---//
        string username = null;
        string password = null;

        //--- URLs ---//
        string myAnalog_accountSettings_url = "app/account";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Elements are Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Elements are Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Elements are Present and Working as Expected in JP Locale")]
        public void MyAnalog_AccountSettings_VerifyPersonalSection(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- Login Credentials ---//
            if (Locale.Equals("zh") || Locale.Equals("cn"))
            {
                username = "myanalog_t3st_cn_777@mailinator.com";
            }
            else if (Locale.Equals("jp") || Locale.Equals("cn"))
            {
                username = "myanalog_t3st_jp_777@mailinator.com";
            }
            else
            {
                username = "myanalog_t3st_777@mailinator.com";
            }
            password = "Test_1234";

            //--- myAnalog Account Settings URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_accountSettings_url;

            //--- Action: Open account settings page ---//
            action.Navigate(driver, myAnalog_url);

            action.ILogin(driver, username, password);
            Thread.Sleep(4000);

            //----- Account Settings Test Plan > myAnalog Dashboard Tab > R2 > T3: Verify when clicking Edit Name section -----//

            //--- Expected Result: Name section is displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_AccountSettings_Personal_Section);

            //----- Account Settings Test Plan > myAnalog Dashboard Tab > R5 > T1/T3/T5: for English/Chinese/Japanese language -----//

            //--- Expected Result: The the First Name field is available and is required ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_AccountSettings_Personal_Section_Required_FirstName_InputBox);

            //--- Expected Result: The the Last Name field is available and is required ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_AccountSettings_Personal_Section_Required_LastName_InputBox);

            //--- Expected Result: The the Company Name field is available and is required ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_AccountSettings_Personal_Section_Required_CompanyName_InputBox);

            if (Locale.Equals("jp"))
            {
                //----- Account Settings Test Plan > myAnalog Dashboard Tab > R5 > T5: Required fields for Japanese language -----//

                /*****is already removed in 21.3.1*****/
                ////--- Expected Result: The Pronunciation of First Name field is available and required---//
                //test.validateElementIsPresent(driver, Elements.MyAnalog_AccountSettings_Personal_Section_Required_FirstPronounce_InputBox);

                ////--- Expected Result: The Pronunciation of Last Name field is available and required---//
                //test.validateElementIsPresent(driver, Elements.MyAnalog_AccountSettings_Personal_Section_Required_LastPronounce_InputBox);

                //--- Expected Result: The Telephone field is available and required ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_AccountSettings_Personal_Section_Required_Telephone_InputBox);

                //--- Expected Result: The Divisions field is available and required ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_AccountSettings_Personal_Section_Required_Division_InputBox);
            }

            if (Locale.Equals("en"))
            {
                //----- Account Settings Test Plan > myAnalog Dashboard Tab > R5 > T2: Optional fields for English language (IQ-8917/AL-15745) -----//

                //--- Expected Result: The the Telephone field is available and is optional ---//
                test.validateElementIsNotPresent(driver, Elements.MyAnalog_AccountSettings_Personal_Section_Required_Telephone_InputBox);
            }

            //----- Account Settings Test Plan > myAnalog Dashboard Tab > R2 > T14: Verify if the Edit button is available in all section except change password section -----//

            //--- Expected Result: Edit button available in all section except change password section ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_AccountSettings_Personal_Section_Edit_Button);

            //----- Account Settings Test Plan > myAnalog Dashboard Tab > R2 > T3: Verify when clicking Edit Name section -----//

            //--- Action: Click Pencil icon top-right corner of  the section ---//
            action.IClick(driver, Elements.MyAnalog_AccountSettings_Personal_Section_Edit_Button);

            //--- Expected Result: Name  field section enables editing mode ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_AccountSettings_Personal_Section_Editing_Mode);

            //--- Expected Result: the pencil icon is replace with “save” and “cancel” options. ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_AccountSettings_Personal_Section_Save_Button);
            test.validateElementIsPresent(driver, Elements.MyAnalog_AccountSettings_Personal_Section_Cancel_Button);

            //----- Account Settings Test Plan > myAnalog Dashboard Tab > R2 > T3: Verify when clicking Edit Name section -----//

            //--- Expected Result: The First Name field is editable ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_AccountSettings_Personal_Section_FirstName_InputBox);

            //----- Account Settings Test Plan > myAnalog Dashboard Tab > R2 > T3.1: Verify Clear icon must only appear when the input has a value (IQ-6547/AL-13367) -----//

            //--- Expected Result: Clear Icon must apear when the input has a value ---//
            if (!driver.FindElement(Elements.MyAnalog_AccountSettings_Personal_Section_FirstName_InputBox).GetAttribute("value").Equals(""))
            {
                test.validateElementIsPresent(driver, Elements.MyAnalog_AccountSettings_Personal_Section_FirstName_InputBox_X_Button);
            }

            //----- Account Settings Test Plan > myAnalog Dashboard Tab > R2 > T3: Verify when clicking Edit Name section -----//

            //--- Expected Result: The Last Name field is editable ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_AccountSettings_Personal_Section_LastName_InputBox);

            //----- Account Settings Test Plan > myAnalog Dashboard Tab > R2 > T3.1: Verify Clear icon must only appear when the input has a value (IQ-6547/AL-13367) -----//
            if (!driver.FindElement(Elements.MyAnalog_AccountSettings_Personal_Section_LastName_InputBox).GetAttribute("value").Equals(""))
            {
                test.validateElementIsPresent(driver, Elements.MyAnalog_AccountSettings_Personal_Section_LastName_InputBox_X_Button);
            }

            //----- Account Settings Test Plan > myAnalog Dashboard Tab > R2 > T3: Verify when clicking Edit Name section -----//

            //--- Expected Result: The Telephone field is editable ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_AccountSettings_Personal_Section_Telephone_InputBox);

            //----- Account Settings Test Plan > myAnalog Dashboard Tab > R2 > T3.1: Verify Clear icon must only appear when the input has a value (IQ-6547/AL-13367) -----//
            if (!driver.FindElement(Elements.MyAnalog_AccountSettings_Personal_Section_Telephone_InputBox).GetAttribute("value").Equals(""))
            {
                test.validateElementIsPresent(driver, Elements.MyAnalog_AccountSettings_Personal_Section_Telephone_InputBox_X_Button);
            }

            //----- Account Settings Test Plan > myAnalog Dashboard Tab > R2 > T3: Verify when clicking Edit Name section -----//

            //--- Expected Result: The Company Name field is editable ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_AccountSettings_Personal_Section_CompanyName_InputBox);

            //----- Account Settings Test Plan > myAnalog Dashboard Tab > R2 > T3.1: Verify Clear icon must only appear when the input has a value (IQ-6547/AL-13367) -----//
            if (!driver.FindElement(Elements.MyAnalog_AccountSettings_Personal_Section_CompanyName_InputBox).GetAttribute("value").Equals(""))
            {
                test.validateElementIsPresent(driver, Elements.MyAnalog_AccountSettings_Personal_Section_CompanyName_InputBox_X_Button);
            }

            //----- Account Settings Test Plan > myAnalog Dashboard Tab > R2 > T2: Verify if Mobile Phone field label is display in JP local -----//

            //--- Expected Result: Mobile Phome field displayed in JP local ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_AccountSettings_Personal_Section_MobilePhone_Label);

            //----- Account Settings Test Plan > myAnalog Dashboard Tab > R2 > T1: Verify if Mobile phone number is available. -----//

            //--- Expected Result: Mobile phone number is display. ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_AccountSettings_Personal_Section_MobilePhone_InputBox);

            //----- Account Settings Test Plan > myAnalog Dashboard Tab > R2 > T3.1: Verify Clear icon must only appear when the input has a value (IQ-6547/AL-13367) -----//
            if (!driver.FindElement(Elements.MyAnalog_AccountSettings_Personal_Section_MobilePhone_InputBox).GetAttribute("value").Equals(""))
            {
                test.validateElementIsPresent(driver, Elements.MyAnalog_AccountSettings_Personal_Section_MobilePhone_InputBox_X_Button);
            }
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Save Button is Present and Working for Valid Inputs as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Save Button is Present and Working for Valid Inputs as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Save Button is Present and Working for Valid Inputs as Expected in JP Locale")]
        public void MyAnalog_AccountSettings_PersonalSection_VerifySaveButtonForValidInputs(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- Login Credentials ---//
            if (Locale.Equals("zh") || Locale.Equals("cn"))
            {
                username = "myanalog_t3st_upd4t3_p3s0n4l_f13lds_cn_555@mailinator.com";
            }
            else if (Locale.Equals("jp"))
            {
                username = "myanalog_t3st_upd4t3_p3s0n4l_f13lds_jp_555@mailinator.com";
            }
            else
            {
                username = "myanalog_t3st_upd4t3_p3s0n4l_f13lds_en_555@mailinator.com";
            }
            password = "Test_1234";

            //--- myAnalog Account Settings URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_accountSettings_url;

            //--- Action: Open account settings page ---//
            action.Navigate(driver, myAnalog_url);

            action.ILogin(driver, username, password);
            Thread.Sleep(5000);

            //----- Account Settings Test Plan > myAnalog Dashboard Tab > R4 > T3: Verify that the user will be able to update his/her account when all required and optional fields have been populated -----//
            //----- Precondition: Order has not been made yet) -----//

            //--- Action: Click the pencil icon to edit first section ---//
            action.IClick(driver, Elements.MyAnalog_AccountSettings_Personal_Section_Edit_Button);

            //--- Action: Edit all fields with valid values ---//
            bool firstName_inputBox = false;
            string firstName_input = "firstName_" + util.GenerateRandomString();
            if (util.CheckElement(driver, Elements.MyAnalog_AccountSettings_Personal_Section_FirstName_InputBox, 2))
            {
                firstName_inputBox = true;

                action.IDeleteValueOnFields(driver, Elements.MyAnalog_AccountSettings_Personal_Section_FirstName_InputBox);
                action.IType(driver, Elements.MyAnalog_AccountSettings_Personal_Section_FirstName_InputBox, firstName_input);
            }

            bool firstPronounce_inputBox = false;
            string firstPronounce_input = firstName_input;
            if (util.CheckElement(driver, Elements.MyAnalog_AccountSettings_Personal_Section_FirstPronounce_InputBox, 2))
            {
                firstPronounce_inputBox = true;

                action.IDeleteValueOnFields(driver, Elements.MyAnalog_AccountSettings_Personal_Section_FirstPronounce_InputBox);
                action.IType(driver, Elements.MyAnalog_AccountSettings_Personal_Section_FirstPronounce_InputBox, firstPronounce_input);
            }

            bool lastName_inputBox = false;
            string lastName_input = "lastName_" + util.GenerateRandomString();
            if (util.CheckElement(driver, Elements.MyAnalog_AccountSettings_Personal_Section_LastName_InputBox, 2))
            {
                lastName_inputBox = true;

                action.IDeleteValueOnFields(driver, Elements.MyAnalog_AccountSettings_Personal_Section_LastName_InputBox);
                action.IType(driver, Elements.MyAnalog_AccountSettings_Personal_Section_LastName_InputBox, lastName_input);
            }

            bool lastPronounce_inputBox = false;
            string lastPronounce_input = lastName_input;
            if (util.CheckElement(driver, Elements.MyAnalog_AccountSettings_Personal_Section_LastPronounce_InputBox, 2))
            {
                lastPronounce_inputBox = true;

                action.IDeleteValueOnFields(driver, Elements.MyAnalog_AccountSettings_Personal_Section_LastPronounce_InputBox);
                action.IType(driver, Elements.MyAnalog_AccountSettings_Personal_Section_LastPronounce_InputBox, lastPronounce_input);
            }

            bool telephone_inputBox = false;
            string telephone_input = null;
            if (util.CheckElement(driver, Elements.MyAnalog_AccountSettings_Personal_Section_Telephone_InputBox, 2))
            {
                telephone_inputBox = true;

                telephone_input = util.GenerateRandomNumber().ToString() + util.GenerateRandomNumber().ToString() + util.GenerateRandomNumber().ToString();
                action.IDeleteValueOnFields(driver, Elements.MyAnalog_AccountSettings_Personal_Section_Telephone_InputBox);
                action.IType(driver, Elements.MyAnalog_AccountSettings_Personal_Section_Telephone_InputBox, telephone_input);
            }

            bool companyName_inputBox = false;
            string companyName_input = "companyName_" + util.GenerateRandomString();
            if (util.CheckElement(driver, Elements.MyAnalog_AccountSettings_Personal_Section_CompanyName_InputBox, 2))
            {
                companyName_inputBox = true;

                action.IDeleteValueOnFields(driver, Elements.MyAnalog_AccountSettings_Personal_Section_CompanyName_InputBox);
                action.IType(driver, Elements.MyAnalog_AccountSettings_Personal_Section_CompanyName_InputBox, companyName_input);
            }

            bool companyNamePronounce_inputBox = false;
            string companyNamePronounce_input = companyName_input;
            if (util.CheckElement(driver, Elements.MyAnalog_AccountSettings_Personal_Section_CompanyNamePronounce_InputBox, 2))
            {
                companyNamePronounce_inputBox = true;

                action.IDeleteValueOnFields(driver, Elements.MyAnalog_AccountSettings_Personal_Section_CompanyNamePronounce_InputBox);
                action.IType(driver, Elements.MyAnalog_AccountSettings_Personal_Section_CompanyNamePronounce_InputBox, companyNamePronounce_input);
            }

            bool mobilePhone_inputBox = false;
            string mobilePhone_input = null;
            if (util.CheckElement(driver, Elements.MyAnalog_AccountSettings_Personal_Section_MobilePhone_InputBox, 2))
            {
                mobilePhone_inputBox = true;

                mobilePhone_input = util.GenerateRandomNumber().ToString() + util.GenerateRandomNumber().ToString() + util.GenerateRandomNumber().ToString();
                action.IDeleteValueOnFields(driver, Elements.MyAnalog_AccountSettings_Personal_Section_MobilePhone_InputBox);
                action.IType(driver, Elements.MyAnalog_AccountSettings_Personal_Section_MobilePhone_InputBox, mobilePhone_input);
            }

            bool division_inputBox = false;
            string division_input = "division_" + util.GenerateRandomString();
            if (util.CheckElement(driver, Elements.MyAnalog_AccountSettings_Personal_Section_Division_InputBox, 2))
            {
                division_inputBox = true;

                action.IDeleteValueOnFields(driver, Elements.MyAnalog_AccountSettings_Personal_Section_Division_InputBox);
                action.IType(driver, Elements.MyAnalog_AccountSettings_Personal_Section_Division_InputBox, division_input);
            }

            //--- Action: Click Save option ---//
            action.IClick(driver, Elements.MyAnalog_AccountSettings_Personal_Section_Save_Button);

            //--- Expected Result: System successfullly saved changes made ---//
            if (firstName_inputBox.Equals(true))
            {
                test.validateStringInstance(driver, firstName_input, util.GetInputBoxValue(driver, Elements.MyAnalog_AccountSettings_Personal_Section_FirstName_InputBox));
            }

            if (firstPronounce_inputBox.Equals(true))
            {
                test.validateStringInstance(driver, firstPronounce_input, util.GetInputBoxValue(driver, Elements.MyAnalog_AccountSettings_Personal_Section_FirstPronounce_InputBox));
            }

            if (lastName_inputBox.Equals(true))
            {
                test.validateStringInstance(driver, lastPronounce_input, util.GetInputBoxValue(driver, Elements.MyAnalog_AccountSettings_Personal_Section_LastName_InputBox));
            }

            if (lastPronounce_inputBox.Equals(true))
            {
                test.validateStringInstance(driver, lastName_input, util.GetInputBoxValue(driver, Elements.MyAnalog_AccountSettings_Personal_Section_LastPronounce_InputBox));
            }

            if (telephone_inputBox.Equals(true))
            {
                test.validateStringInstance(driver, telephone_input, util.GetInputBoxValue(driver, Elements.MyAnalog_AccountSettings_Personal_Section_Telephone_InputBox));
            }

            if (companyName_inputBox.Equals(true))
            {
                test.validateStringInstance(driver, companyName_input, util.GetInputBoxValue(driver, Elements.MyAnalog_AccountSettings_Personal_Section_CompanyName_InputBox));
            }

            if (companyNamePronounce_inputBox.Equals(true))
            {
                test.validateStringInstance(driver, companyName_input, util.GetInputBoxValue(driver, Elements.MyAnalog_AccountSettings_Personal_Section_CompanyNamePronounce_InputBox));
            }

            if (mobilePhone_inputBox.Equals(true))
            {
                test.validateStringInstance(driver, mobilePhone_input, util.GetInputBoxValue(driver, Elements.MyAnalog_AccountSettings_Personal_Section_MobilePhone_InputBox));
            }

            if (division_inputBox.Equals(true))
            {
                test.validateStringInstance(driver, division_input, util.GetInputBoxValue(driver, Elements.MyAnalog_AccountSettings_Personal_Section_Division_InputBox));
            }
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Save Button is Present and Working for Blank Inputs as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Save Button is Present and Working for Blank Inputs as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Save Button is Present and Working for Blank Inputs as Expected in JP Locale")]
        public void MyAnalog_AccountSettings_PersonalSection_VerifySaveButtonForBlankInputs(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- Login Credentials ---//
            if (Locale.Equals("zh") || Locale.Equals("cn"))
            {
                username = "myanalog_t3st_upd4t3_p3s0n4l_f13lds_cn_555@mailinator.com";
            }
            else if (Locale.Equals("jp"))
            {
                username = "myanalog_t3st_upd4t3_p3s0n4l_f13lds_jp_555@mailinator.com";
            }
            else
            {
                username = "myanalog_t3st_upd4t3_p3s0n4l_f13lds_en_555@mailinator.com";
            }
            password = "Test_1234";

            //--- myAnalog Account Settings URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_accountSettings_url;

            //--- Action: Open account settings page ---//
            action.Navigate(driver, myAnalog_url);

            action.ILogin(driver, username, password);
            Thread.Sleep(5000);

            //----- Account Settings Test Plan > myAnalog Dashboard Tab > R4 > T19: Verify if red lines are removed in the 1st section of Account Settings upon clicking Cancel button (IQ-7684/AL-14022) -----//

            //--- Action: Click the pencil icon on the 1st section of the page(firstname, etc.)  ---//
            action.IClick(driver, Elements.MyAnalog_AccountSettings_Personal_Section_Edit_Button);

            //--- Action: Empty all fields ---//
            bool firstName_inputBox = false;
            if (util.CheckElement(driver, Elements.MyAnalog_AccountSettings_Personal_Section_FirstName_InputBox, 2))
            {
                firstName_inputBox = true;

                action.IDeleteValueOnFields(driver, Elements.MyAnalog_AccountSettings_Personal_Section_FirstName_InputBox);
                string element_placeholder = util.ReturnAttribute(driver, Elements.MyAnalog_AccountSettings_Personal_Section_FirstName_InputBox, "placeholder");
                if (!string.IsNullOrEmpty(element_placeholder))
                {
                    Console.WriteLine("Delete Value in " + element_placeholder.Trim() + " Text Field.");
                }
                else
                {
                    Console.WriteLine("Delete Value in First name Text Field.");
                }
            }

            if (util.CheckElement(driver, Elements.MyAnalog_AccountSettings_Personal_Section_FirstPronounce_InputBox, 2))
            {
                action.IDeleteValueOnFields(driver, Elements.MyAnalog_AccountSettings_Personal_Section_FirstPronounce_InputBox);
                string element_placeholder = util.ReturnAttribute(driver, Elements.MyAnalog_AccountSettings_Personal_Section_FirstPronounce_InputBox, "placeholder");
                if (!string.IsNullOrEmpty(element_placeholder))
                {
                    Console.WriteLine("Delete Value in " + element_placeholder.Trim() + " Text Field.");
                }
                else
                {
                    Console.WriteLine("Delete Value in First Pronounce Text Field.");
                }
            }

            bool lastName_inputBox = false;
            if (util.CheckElement(driver, Elements.MyAnalog_AccountSettings_Personal_Section_LastName_InputBox, 2))
            {
                lastName_inputBox = true;

                action.IDeleteValueOnFields(driver, Elements.MyAnalog_AccountSettings_Personal_Section_LastName_InputBox);
                string element_placeholder = util.ReturnAttribute(driver, Elements.MyAnalog_AccountSettings_Personal_Section_LastName_InputBox, "placeholder");
                if (!string.IsNullOrEmpty(element_placeholder))
                {
                    Console.WriteLine("Delete Value in " + element_placeholder.Trim() + " Text Field.");
                }
                else
                {
                    Console.WriteLine("Delete Value in Last name Text Field.");
                }
            }

            if (util.CheckElement(driver, Elements.MyAnalog_AccountSettings_Personal_Section_LastPronounce_InputBox, 2))
            {
                action.IDeleteValueOnFields(driver, Elements.MyAnalog_AccountSettings_Personal_Section_LastPronounce_InputBox);
                string element_placeholder = util.ReturnAttribute(driver, Elements.MyAnalog_AccountSettings_Personal_Section_LastPronounce_InputBox, "placeholder");
                if (!string.IsNullOrEmpty(element_placeholder))
                {
                    Console.WriteLine("Delete Value in " + element_placeholder.Trim() + " Text Field.");
                }
                else
                {
                    Console.WriteLine("Delete Value in Last Pronounce. Text Field");
                }
            }

            if (util.CheckElement(driver, Elements.MyAnalog_AccountSettings_Personal_Section_Telephone_InputBox, 2))
            {
                action.IDeleteValueOnFields(driver, Elements.MyAnalog_AccountSettings_Personal_Section_Telephone_InputBox);
                string element_placeholder = util.ReturnAttribute(driver, Elements.MyAnalog_AccountSettings_Personal_Section_Telephone_InputBox, "placeholder");
                if (!string.IsNullOrEmpty(element_placeholder))
                {
                    Console.WriteLine("Delete Value in " + element_placeholder.Trim() + " Text Field.");
                }
                else
                {
                    Console.WriteLine("Delete Value in Telephone Text Field.");
                }
            }

            bool companyName_inputBox = false;
            if (util.CheckElement(driver, Elements.MyAnalog_AccountSettings_Personal_Section_CompanyName_InputBox, 2))
            {
                companyName_inputBox = true;

                action.IDeleteValueOnFields(driver, Elements.MyAnalog_AccountSettings_Personal_Section_CompanyName_InputBox);
                string element_placeholder = util.ReturnAttribute(driver, Elements.MyAnalog_AccountSettings_Personal_Section_CompanyName_InputBox, "placeholder");
                if (!string.IsNullOrEmpty(element_placeholder))
                {
                    Console.WriteLine("Delete Value in " + element_placeholder.Trim() + " Text Field.");
                }
                else
                {
                    Console.WriteLine("Delete Value in Company name Text Field.");
                }
            }

            if (util.CheckElement(driver, Elements.MyAnalog_AccountSettings_Personal_Section_CompanyNamePronounce_InputBox, 2))
            {
                action.IDeleteValueOnFields(driver, Elements.MyAnalog_AccountSettings_Personal_Section_CompanyNamePronounce_InputBox);
                string element_placeholder = util.ReturnAttribute(driver, Elements.MyAnalog_AccountSettings_Personal_Section_CompanyNamePronounce_InputBox, "placeholder");
                if (!string.IsNullOrEmpty(element_placeholder))
                {
                    Console.WriteLine("Delete Value in " + element_placeholder.Trim() + " Text Field.");
                }
                else
                {
                    Console.WriteLine("Delete Value in Company name Pronounce Text Field.");
                }
            }

            if (util.CheckElement(driver, Elements.MyAnalog_AccountSettings_Personal_Section_MobilePhone_InputBox, 2))
            {
                action.IDeleteValueOnFields(driver, Elements.MyAnalog_AccountSettings_Personal_Section_MobilePhone_InputBox);
                string element_placeholder = util.ReturnAttribute(driver, Elements.MyAnalog_AccountSettings_Personal_Section_MobilePhone_InputBox, "placeholder");
                if (!string.IsNullOrEmpty(element_placeholder))
                {
                    Console.WriteLine("Delete Value in " + element_placeholder.Trim() + " Text Field.");
                }
                else
                {
                    Console.WriteLine("Delete Value in Mobile Phone Text Field.");
                }
            }

            if (util.CheckElement(driver, Elements.MyAnalog_AccountSettings_Personal_Section_Division_InputBox, 2))
            {
                action.IDeleteValueOnFields(driver, Elements.MyAnalog_AccountSettings_Personal_Section_Division_InputBox);
                string element_placeholder = util.ReturnAttribute(driver, Elements.MyAnalog_AccountSettings_Personal_Section_Division_InputBox, "placeholder");
                if (!string.IsNullOrEmpty(element_placeholder))
                {
                    Console.WriteLine("Delete Value in " + element_placeholder.Trim() + " Text Field.");
                }
                else
                {
                    Console.WriteLine("Delete Value in Division Text Field.");
                }
            }

            //--- Action: Click Save button ---//
            action.IClick(driver, Elements.MyAnalog_AccountSettings_Personal_Section_Save_Button);

            if (firstName_inputBox.Equals(true))
            {
                //--- Expected Result: Red line will be displayed on First name field indicating it is not valid ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_AccountSettings_Personal_Section_FirstName_InputBox_Error);
            }

            if (lastName_inputBox.Equals(true))
            {
                //--- Expected Result: Red line will be displayed on Last name field indicating it is not valid ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_AccountSettings_Personal_Section_LastName_InputBox_Error);
            }

            if (companyName_inputBox.Equals(true))
            {
                //--- Expected Result: Red line will be displayed on Company name field indicating it is not valid ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_AccountSettings_Personal_Section_CompanyName_InputBox_Error);
            }

            //--- Action: Click Cancel button ---//
            action.IClick(driver, Elements.MyAnalog_AccountSettings_Personal_Section_Cancel_Button);

            if (firstName_inputBox.Equals(true))
            {
                //--- Expected Result: Red line or invalid input indicator are no longer visible on First name field ---//
                test.validateElementIsNotPresent(driver, Elements.MyAnalog_AccountSettings_Personal_Section_FirstName_InputBox_Error);
            }

            if (lastName_inputBox.Equals(true))
            {
                //--- Expected Result: Red line or invalid input indicator are no longer visible on Last name field ---//
                test.validateElementIsNotPresent(driver, Elements.MyAnalog_AccountSettings_Personal_Section_LastName_InputBox_Error);
            }

            if (companyName_inputBox.Equals(true))
            {
                //--- Expected Result: Red line or invalid input indicator are no longer visible on Company name field ---//
                test.validateElementIsNotPresent(driver, Elements.MyAnalog_AccountSettings_Personal_Section_CompanyName_InputBox_Error);
            }
        }
    }
}