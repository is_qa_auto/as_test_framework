﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System;
using System.Threading;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_Projects_ProjectDetailPage_SectionHeader : BaseSetUp
    {
        //--- myAnalog Projects URL: ---//
        //--- DEV: https://my-dev.corpnt.analog.com/en/app/projects ---//
        //--- QA: https://my-qa.corpnt.analog.com/en/app/projects ---//
        //--- PROD: https://my.analog.com/en/app/projects ---//

        public MyAnalog_Projects_ProjectDetailPage_SectionHeader() : base() { }

        //----- Note: Please don't edit/delete any details on the Accounts below -----//

        //--- Login Credentials (Project Owner) ---// 
        string username_owner = "my_t3st_pr0j3cts_555@mailinator.com";
        string password_owner = "Test_1234";

        //--- Login Credentials (Project Participant) ---// 
        string username_participant = "my_t3st_pr0ject_1nv1t33_555@mailinator.com";
        string password_participant = "Test_1234";

        //--- Login Credentials (Used for Deleting a Project) ---// 
        string username_projectDeletion = "my_t3st_cr34t3_pr0j3cts_555@mailinator.com";
        string password_projectDeletion = "Test_1234";

        //--- URLs ---//
        string myAnalog_projects_url = "app/projects";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Back Arrow Icon is Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Back Arrow Icon is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Back Arrow Icon is Present and Working as Expected in JP Locale")]
        public void MyAnalog_Projects_ProjectDetailPage_SectionHeader_VerifyBackArrowIcon(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Projects URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_projects_url;

            //--- Action: Open myAnalog Projects using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username_owner, password_owner);

            //--- Action: Click Project Detail button ---//
            action.IClick(driver, Elements.MyAnalog_Projects_ProjectDetails_Buttons);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Navigate to Project Details Page -----//

            //--- Expected Result: The Header arrow (back icon) component is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_ProjectDetailPage_BackArrow_Icon);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify if title were changed with the selected title (IQ-6440/AL-13525) -----//

            //--- Action: Click Back button ---//
            action.IClick(driver, Elements.MyAnalog_ProjectDetailPage_BackArrow_Icon);

            //--- Expected Result: Page is redirected to Project Listing page ---// 
            test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + "/" + myAnalog_projects_url);
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that Edit Project Name is Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that Edit Project Name is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that Edit Project Name is Present and Working as Expected in JP Locale")]
        public void MyAnalog_Projects_ProjectDetailPage_SectionHeader_VerifyEditProjectName(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Projects URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_projects_url;

            //--- Action: Open myAnalog Projects using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials (Project Owner) and click login ---//
            action.ILogin(driver, username_owner, password_owner);

            //--- Action: Click Project Detail button ---//
            action.IClick(driver, Elements.MyAnalog_Projects_ProjectDetails_Buttons);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Navigate to Project Details Page -----//

            //--- Expected Result: The project title component is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_ProjectDetailPage_ProjectName_Label);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify when the owner Edit the project -----//

            //--- Action: Click the Edit/Remove option  on the Project as an owner ---//
            action.IClick(driver, Elements.MyAnalog_ProjectDetailPage_Ellipsis_Button);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify the Delete and Edit option -----//

            //--- Expected Result: The option to edit should appear ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_ProjectDetailPage_EditProjectName_Option);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify when the owner Edit the project -----//

            //--- Action: Click Edit ---//
            action.IClick(driver, Elements.MyAnalog_ProjectDetailPage_EditProjectName_Option);

            //--- Action: Edit the Project Name ---//
            string projectName_input = "Edit_ProjectName_" + util.RandomString();
            action.IDeleteValueOnFields(driver, Elements.MyAnalog_ProjectDetailPage_EditProjectName_InputBox);
            action.IType(driver, Elements.MyAnalog_ProjectDetailPage_EditProjectName_InputBox, projectName_input);

            //--- Action: Click OK ---//
            action.IClick(driver, Elements.MyAnalog_ProjectDetailPage_EditProjectName_Save_Button);
            Thread.Sleep(1000);

            //--- Expected Result: The project name should be updated ---//
            //--- NOTE: AL-18532 has been logged for the Issue where Edit Project Name in Project Detail Page doesn't work. ---//
            test.validateStringIsCorrect(driver, Elements.MyAnalog_ProjectDetailPage_ProjectName_Label, projectName_input);
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that Delete Project is Present and Working as Expected in EN Locale")]
        public void MyAnalog_Projects_ProjectDetailPage_SectionHeader_VerifyDeleteProject(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Projects URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_projects_url;

            //--- Action: Open myAnalog Projects using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials (Project Owner) and click login ---//
            action.ILogin(driver, username_projectDeletion, password_projectDeletion);

            //--- Action: Create a Project in myAnalog if there's none ---//
            if (!util.CheckElement(driver, Elements.MyAnalog_Projects_Created_Projects, 2))
            {
                action.ICreateAProjectInMyAnalog(driver, Locale);
            }

            //--- Action: Click Project Detail button ---//
            action.IClick(driver, Elements.MyAnalog_Projects_ProjectDetails_Buttons);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify when the owner delete the project -----//
            string projectName = util.GetText(driver, Elements.MyAnalog_ProjectDetailPage_ProjectName_Label);

            //--- Action: Click the Edit/Remove option  on the Project as an owner ---//
            action.IClick(driver, Elements.MyAnalog_ProjectDetailPage_Ellipsis_Button);

            //--- Action: Click Delete Project link ---//
            action.IClick(driver, Elements.MyAnalog_ProjectDetailPage_DeleteProject_Option);

            //--- Expected Result: Warning message appear ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_ProjectDetailPage_DeleteProject_Modal_WarningMessage);

            //--- Action: Click OK in confirmation message ---//
            action.IClick(driver, Elements.MyAnalog_ProjectDetailPage_DeleteProject_Modal_Remove_Button);
            Thread.Sleep(1000);

            driver.Navigate().Refresh();
            Console.WriteLine("\tRefresh the Page.");

            //--- Expected Result: The project should be removed ---//
            test.validateStringChanged(driver, util.GetText(driver, Elements.MyAnalog_Projects_First_Project_Name_Link), projectName);
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that Cancel Delete Project is Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that Cancel Delete Project is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that Cancel Delete Project is Present and Working as Expected in JP Locale")]
        public void MyAnalog_Projects_ProjectDetailPage_SectionHeader_VerifyCancelDeleteProject(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Projects URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_projects_url;

            //--- Action: Open myAnalog Projects using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials (Project Owner) and click login ---//
            action.ILogin(driver, username_projectDeletion, password_projectDeletion);

            //--- Action: Create a Project in myAnalog if there's none ---//
            if (!util.CheckElement(driver, Elements.MyAnalog_Projects_Created_Projects, 2))
            {
                action.ICreateAProjectInMyAnalog(driver, Locale);
            }

            //--- Action: Click Project Detail button ---//
            action.IClick(driver, Elements.MyAnalog_Projects_ProjectDetails_Buttons);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify the Delete and Edit option -----//

            //--- Action: Click on the Edit/Remove option (3 bullet points) ---//
            action.IClick(driver, Elements.MyAnalog_ProjectDetailPage_Ellipsis_Button);

            //--- Expected Result: The option to remove should appear ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_ProjectDetailPage_DeleteProject_Option);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify Edit description is displayed -----//
            string projectDetailsPage_url = driver.Url;

            //--- Action: Click Remove ---//
            action.IClick(driver, Elements.MyAnalog_ProjectDetailPage_DeleteProject_Option);

            //--- Action: Click Cancel ---//
            action.IClick(driver, Elements.MyAnalog_ProjectDetailPage_DeleteProject_Modal_Cancel_Button);

            //--- Expected Result: Clicking Cancel should not delete the Project ---//
            test.validateScreenByUrl(driver, projectDetailsPage_url);
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that Edit Project is Working as Expected for Project Participants in EN Locale")]
        [TestCase("zh", TestName = "Verify that Edit Project is Working as Expected for Project Participants in CN Locale")]
        [TestCase("jp", TestName = "Verify that Edit Project is Working as Expected for Project Participants in JP Locale")]
        public void MyAnalog_Projects_ProjectDetailPage_SectionHeader_VerifyEditProjectForForProjectParticipants(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Projects URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_projects_url;

            //--- Action: Open myAnalog Projects using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials (Project Participants) and click login ---//
            action.ILogin(driver, username_participant, password_participant);

            //--- Action: Click Project Detail button ---//
            action.IClick(driver, Elements.MyAnalog_Projects_ProjectDetails_Buttons);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify when the participant Edit the project(non-owner) -----//

            //--- Action: Click the Edit/Remove option  on the Project as an owner ---//
            action.IClick(driver, Elements.MyAnalog_ProjectDetailPage_Ellipsis_Button);

            //--- Expected Result: Option to edit is not available ---//
            test.validateElementIsNotPresent(driver, Elements.MyAnalog_ProjectDetailPage_EditProjectName_Option);
            test.validateElementIsNotPresent(driver, Elements.MyAnalog_ProjectDetailPage_EditDescription_Option);

            //--- Expected Result: Option to remove is not available ---//
            test.validateElementIsNotPresent(driver, Elements.MyAnalog_ProjectDetailPage_DeleteProject_Option);
        }
    }
}