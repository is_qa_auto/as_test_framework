﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_MegaMenu_ProfileSection : BaseSetUp
    {
        public MyAnalog_MegaMenu_ProfileSection() : base() { }

        //--- Login Credentials ---//
        string username = "myanalog_t3st_555@mailinator.com";
        string password = "Test_1234";

        //--- myAnalog Dashboard URL: ---//
        //--- DEV: https://my-dev.corpnt.analog.com/en/app/ ---//
        //--- QA: https://my-qa.corpnt.analog.com/en/app/ ---//
        //--- PROD: https://my.analog.com/en/app/ ---//

        //--- URLs ---//
        string myAnalog_dashboard_url = "app";
        string findSalesOfficeAndDistributors_url = "/find-sale-office-distributor.html";
        string salesAndDistributors_url = "/sales-and-distributors.html";
        string jp_salesAndDistribution_jp_url = "/jp-sales-and-disti.html";
        string partnerApplicationsPortal_url = "/accessrequestform.aspx";
        string core_page_url = "/applications/markets/aerospace-and-defense-pavilion-home.html";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Profile Section is Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Profile Section is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Profile Section is Present and Working as Expected in JP Locale")]
        public void MyAnalog_MegaMenu_VerifyProfileSection(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Dashboard URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_dashboard_url;

            action.Navigate(driver, myAnalog_url);

            //--- Action: Log in valid account in login page ---//
            action.ILogin(driver, username, password);

            //--- Action: click myanalog tab ---//
            action.IExpandMyAnalogMegaMenu(driver);

            //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R2 > T2: Verify myPage Components -----//

            //--- Expected Result: The Profile section is displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_MegaMenu_Profile_Section);

            //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R2 > T3: Verify myANalog profile -----//

            //--- Expected Result: Avatar initials (IQ-6349/AL-13515): ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_MegaMenu_Initials_Text);

            //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R2 > T2: Verify myPage Components -----//

            //--- Expected Result: The MyAnalog Dashboard button is displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_MegaMenu_MyAnalogDashboard_Button);

            if (util.CheckElement(driver, Elements.MyAnalog_MegaMenu_MyAnalogDashboard_Button, 2))
            {
                //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R2 > T4: Verify when clicking myAnalog Dashboard button -----//

                //--- Action: Click myAnalog Dashboard button ---//
                action.IOpenLinkInNewTab(driver, Elements.MyAnalog_MegaMenu_MyAnalogDashboard_Button);
                Thread.Sleep(1000);

                //--- Expected Result: myAnalog dashboard page is displayed ---//
                test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + "/" + myAnalog_dashboard_url);

                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles.Last());
            }

            //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R2 > T2: Verify myPage Components -----//

            //--- Expected Result: The Find your local sales contact link is displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_MegaMenu_FindYourLocalSalesContact_Link);

            if (util.CheckElement(driver, Elements.MyAnalog_MegaMenu_FindYourLocalSalesContact_Link, 2))
            {
                //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R2 > T5: Verify Find your local sales contact link -----//

                //--- Action: Click local sales contact link ---//
                action.IOpenLinkInNewTab(driver, Elements.MyAnalog_MegaMenu_FindYourLocalSalesContact_Link);
                Thread.Sleep(1000);

                //--- Expected Result: Page will redirect to local sales contact page ---//
                if (Locale.Equals("cn") || Locale.Equals("zh"))
                {
                    test.validateStringInstance(driver, driver.Url, salesAndDistributors_url);
                }
                else if (Locale.Equals("jp"))
                {
                    test.validateStringInstance(driver, driver.Url, jp_salesAndDistribution_jp_url);
                }
                else
                {
                    test.validateStringInstance(driver, driver.Url, findSalesOfficeAndDistributors_url);
                }

                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles.Last());
            }

            //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R2 > T2: Verify myPage Components -----//

            //--- Expected Result: The Distributor Portal is displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_MegaMenu_DistributorPortal_Link);

            if (util.CheckElement(driver, Elements.MyAnalog_MegaMenu_DistributorPortal_Link, 2))
            {
                //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R2 > T6: Verify when clicking Distributor Portal hyperlink (IQ-7675/AL-14929) -----//

                //--- Action: Click Distributor Portal hyperlink ---//
                action.IOpenLinkInNewTab(driver, Elements.MyAnalog_MegaMenu_DistributorPortal_Link);
                Thread.Sleep(2000);

                //--- Expected Result: The System displays the Initiate Request for Access to Partner Applications Portal page ---//
                test.validateStringInstance(driver, driver.Url, partnerApplicationsPortal_url);

                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles.Last());
            }
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the sign out Link is Present and Working as Expected in Non-core Pages EN Locale")]
        [TestCase("zh", TestName = "Verify that the sign out Link is Present and Working as Expected in Non-core Pages CN Locale")]
        [TestCase("jp", TestName = "Verify that the sign out Link is Present and Working as Expected in Non-core Pages JP Locale")]
        public void MyAnalog_MegaMenu_ProfileSection_VerifySignOutLinkInNonCorePages(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Dashboard URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_dashboard_url;

            action.Navigate(driver, myAnalog_url);

            //--- Action: Log in valid account in login page ---//
            action.ILogin(driver, username, password);

            //--- Action: Go to a Non-core Page ---//
            string page_url = Configuration.Env_Url.Replace("www", "shoppingcart").Replace("cldnet", "corpnt") + "ShoppingCartPage.aspx";
            if (Locale.Equals("jp") || Locale.Equals("ru"))
            {
                page_url = page_url + "?locale=" + Locale;
            }
            else if (Locale.Equals("cn") || Locale.Equals("zh"))
            {
                page_url = page_url + "?locale=zh";
            }
            driver.Navigate().GoToUrl(page_url);
            Console.WriteLine("Go to a Non-core Page: " + page_url);

            if (util.CheckElement(driver, Elements.SC_LoginLink, 2))
            {
                action.IClick(driver, Elements.SC_LoginLink);
            }

            //--- Action: click myanalog tab ---//
            action.IExpandMyAnalogMegaMenu(driver);

            //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R11 > T1: Verify Sign out link -----//

            //--- Expected Result: The sign out link is displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_MegaMenu_SignOut_Link);

            //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R12 > T2: Verify myAnalog sign-out for shopping cart page (IQ-7835/AL-15131) -----//

            //--- Action: CLick sign out ---//
            action.ISignOut(driver);

            //--- Expected Result: Account has been successfully signed out and redirected to Shopping Cart page ---//
            test.validateStringInstance(driver, driver.Url, page_url);
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the sign out Link is Present and Working as Expected in Core Pages EN Locale")]
        [TestCase("zh", TestName = "Verify that the sign out Link is Present and Working as Expected in Core Pages CN Locale")]
        [TestCase("jp", TestName = "Verify that the sign out Link is Present and Working as Expected in Core Pages JP Locale")]
        public void MyAnalog_MegaMenu_ProfileSection_VerifySignOutLinkInCorePages(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Dashboard URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_dashboard_url;

            action.Navigate(driver, myAnalog_url);

            //--- Action: Log in valid account in login page ---//
            action.ILogin(driver, username, password);

            //--- Action: Go to a Core Page ---//
            string page_url = null;
            if (Locale.Equals("cn") || Locale.Equals("zh"))
            {
                page_url = Configuration.Env_Url + "cn" + core_page_url;
            }
            else
            {
                page_url = Configuration.Env_Url + Locale + core_page_url;
            }
            driver.Navigate().GoToUrl(page_url);
            Console.WriteLine("Go to a Core Page: " + page_url);

            //--- Action: Go to analog tab ---//
            action.IExpandMyAnalogMegaMenu(driver);

            //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R11 > T1: Verify Sign out link -----//

            //--- Expected Result: The sign out link is displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_MegaMenu_SignOut_Link);

            //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R12 > T3: Verify myAnalog sign-out for application pages (IQ-7740/AL-14059) -----//

            //--- Action: log out ---//
            action.ISignOut(driver);

            //--- Expected Result: account has been successfully signed out and redirected to application page ---//
            test.validateStringInstance(driver, driver.Url, page_url);
        }
    }
}