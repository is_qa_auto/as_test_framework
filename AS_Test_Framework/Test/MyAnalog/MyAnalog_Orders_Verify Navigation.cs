﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System;
using System.Threading;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_Orders_Navigation : BaseSetUp
    {
        //--- myAnalog Orders URL: ---//
        //--- DEV: https://my-dev.corpnt.analog.com/en/app/orders ---//
        //--- QA: https://my-qa.corpnt.analog.com/en/app/orders ---//
        //--- PROD: https://my.analog.com/en/app/orders ---//

        public MyAnalog_Orders_Navigation() : base() { }

        //--- Login Credentials ---//
        string username = "marvin.bebe@analog.com";
        string password = "Test_1234";

        //--- URLs ---//
        string myAnalog_orders_url = "app/orders";
        string myAnalog_orderNumber_url_format = "app/orders/";
        string login_page_url = "b2clogin";
        string myAnalog_logout_url = "app/logout";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Navigation is Working as Expected when the User is Logged Out in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Navigation is Working as Expected when the User is Logged Out in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Navigation is Working as Expected when the User is Logged Out in JP Locale")]
        public void MyAnalog_Orders_Navigation_VerifyWhenUserIsLoggedOut(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Orders URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_orders_url;

            //----- ORDERS -----//

            //----- Orders Test Plan > Test Case Title: Verify that the user will be able to navigate to Orders page via direct link when account is not logged in -----//

            //--- Action: Open order page direct link to browser ---//
            action.Navigate(driver, myAnalog_url);

            //--- Expected Result: Page will redirect to logged in page ---//
            test.validateStringInstance(driver, driver.Url, login_page_url);

            //----- ORDER DETAILS -----//

            //--- Action: Enter valid username and password and click login button ---//
            action.ILogin(driver, username, password);

            //--- myAnalog Orders URL ---//
            string orderNumber_link = util.GetText(driver, Elements.MyAnalog_Orders_OrderNumber_Row_Link);
            string myAnalog_orderNumber_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_orders_url + "/" + orderNumber_link;

            //----- Orders Test Plan > Test Case Title: Verify that the user will be able to navigate to Orders page via direct link when account is not logged in -----//
            string logout_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_logout_url;
            driver.Navigate().GoToUrl(logout_url);

            //--- Action: Open order detail page direct link to browser ---//
            action.Navigate(driver, myAnalog_orderNumber_url);

            //--- Expected Result: Page will redirect to logged in page ---//
            test.validateStringInstance(driver, driver.Url, login_page_url);
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Navigation is Working as Expected when the User is Logged In in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Navigation is Working as Expected when the User is Logged In in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Navigation is Working as Expected when the User is Logged In in JP Locale")]
        public void MyAnalog_Orders_Navigation_VerifyWhenUserIsLoggedIn(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Orders URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_orders_url;

            //----- ORDERS -----//

            //----- Orders Test Plan > Test Case Title: Verify  user  navigation when opening direct link and the user is logged in -----//

            //--- Action: Open order page direct link to browser ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Enter valid username and password and click login button ---//
            action.ILogin(driver, username, password);
            Thread.Sleep(1000);

            //--- Expected Result: Page will redirect to order page ---//
            test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + "/" + myAnalog_orders_url);

            //----- ORDER DETAILS -----//

            //--- myAnalog Orders URL ---//
            string orderNumber_link = util.GetText(driver, Elements.MyAnalog_Orders_OrderNumber_Row_Link);
            string myAnalog_orderNumber_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_orders_url + "/" + orderNumber_link;

            //----- Orders Test Plan > Test Case Title: Verify  user  navigation when opening direct link and the user is logged in -----//

            //--- Action: Open order detail page direct link to browser ---//
            driver.Navigate().GoToUrl(myAnalog_orderNumber_url);
            Console.WriteLine("Go to: " + myAnalog_orderNumber_url);

            //--- Action: Enter valid username and password and click login button ---//
            //--- Action: Click login ---//
            action.ILogin(driver, username, password);
            Thread.Sleep(1000);

            //--- Expected Result: Page will redirect to order detail page ---//
            test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + "/" + myAnalog_orderNumber_url_format);
        }
    }
}