﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System;
using System.Threading;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_Projects_CreateANewProjectModal_InputValidations : BaseSetUp
    {
        //--- myAnalog Projects URL: ---//
        //--- DEV: https://my-dev.corpnt.analog.com/en/app/projects ---//
        //--- QA: https://my-qa.corpnt.analog.com/en/app/projects ---//
        //--- PROD: https://my.analog.com/en/app/projects ---//

        public MyAnalog_Projects_CreateANewProjectModal_InputValidations() : base() { }

        //----- Note: Please don't edit/delete any details on the Accounts below -----//

        //--- Login Credentials ---//
        string username = "my_t3st_pr0j3cts_444@mailinator.com";
        string password = "Test_1234";

        //--- URLs ---//
        string myAnalog_projects_url = "app/projects";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Input Validations for the Invitees are Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Input Validations for the Invitees are Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Input Validations for the Invitees are Working as Expected in JP Locale")]
        public void MyAnalog_Projects_CreateANewProjectModal_InputValidations_VerifyInvitees(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Projects URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_projects_url;

            //--- Action: Open myAnalog Projects using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);

            //--- Action: Click Create A New Project to open the pop-up ---//
            action.IClick(driver, Elements.MyAnalog_Projects_CreateANewProject_Button);
            Thread.Sleep(1000);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify if the users allow to invite themselve (IQ-9371/AL-16017) -----//

            //--- Action: Try to invite yourself to the project  ---//
            string invitees_input = username;
            action.IDeleteValueOnFields(driver, Elements.MyAnalog_Projects_CreateANewProject_Modal_Invitees_InputBox);
            action.IType(driver, Elements.MyAnalog_Projects_CreateANewProject_Modal_Invitees_InputBox, invitees_input);

            //--- Action: Create a project ---//
            string projectName_input = "ProjectName_" + util.RandomString();
            action.IDeleteValueOnFields(driver, Elements.MyAnalog_Projects_CreateANewProject_Modal_ProjectName_InputBox);
            action.IType(driver, Elements.MyAnalog_Projects_CreateANewProject_Modal_ProjectName_InputBox, projectName_input);

            action.IClick(driver, Elements.MyAnalog_Projects_CreateANewProject_Modal_Done_Button);
            Thread.Sleep(1000);

            //--- Expected Result: Error message should be displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Projects_CreateANewProject_Modal_Error_Message);
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Input Validations are Working as Expected when Project Name is Empty in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Input Validations are Working as Expected when Project Name is Empty in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Input Validations are Working as Expected when Project Name is Empty in JP Locale")]
        public void MyAnalog_Projects_CreateANewProjectModal_InputValidations_VerifyWhenProjectNameIsEmpty(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Projects URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_projects_url;

            //--- Action: Open myAnalog Projects using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);

            //--- Action: Click Create A New Project to open the pop-up ---//
            action.IClick(driver, Elements.MyAnalog_Projects_CreateANewProject_Button);
            Thread.Sleep(1000);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify when the Project name is empty -----//

            //--- Expected Result: Invite button is disabled by default ---//
            test.validateElementIsNotEnabled(driver, Elements.MyAnalog_Projects_CreateANewProject_Modal_Done_Button);

            //--- Action: Leave Project name empty ---//
            action.IDeleteValueOnFields(driver, Elements.MyAnalog_Projects_CreateANewProject_Modal_ProjectName_InputBox);

            //--- Action: Enter description ---//
            string projectDescription_input = "ProjectDescription_" + util.RandomString();
            action.IDeleteValueOnFields(driver, Elements.MyAnalog_Projects_CreateANewProject_Modal_ProjectDescription_InputBox);
            action.IType(driver, Elements.MyAnalog_Projects_CreateANewProject_Modal_ProjectDescription_InputBox, projectDescription_input);

            //--- Action: Enter Invitees details ---//
            string invitees_input = "t3st_" + Util.Configuration.Environment.ToLower() + "_CreateANewProject_Invitee_" + util.GenerateRandomNumber() + "@mailinator.com";
            action.IDeleteValueOnFields(driver, Elements.MyAnalog_Projects_CreateANewProject_Modal_Invitees_InputBox);
            action.IType(driver, Elements.MyAnalog_Projects_CreateANewProject_Modal_Invitees_InputBox, invitees_input);

            //--- Expected Result: Invite button is still disabled ---//
            test.validateElementIsNotEnabled(driver, Elements.MyAnalog_Projects_CreateANewProject_Modal_Done_Button);
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Input Validations are Working as Expected when Project Name is Blank in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Input Validations are Working as Expected when Project Name is Blank in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Input Validations are Working as Expected when Project Name is Blank in JP Locale")]
        public void MyAnalog_Projects_CreateANewProjectModal_InputValidations_VerifyWhenProjectNameIsBlank(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Projects URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_projects_url;

            //--- Action: Open myAnalog Projects using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify the Error message in Create project Modal (IQ-6960/ALA-12249) -----//

            //--- Action: Click Create a New Project button ---//
            action.IClick(driver, Elements.MyAnalog_Projects_CreateANewProject_Button);

            //--- Action: Input blank spaces on project name field ---//
            string projectName_input = " ";
            action.IDeleteValueOnFields(driver, Elements.MyAnalog_Projects_CreateANewProject_Modal_ProjectName_InputBox);
            action.IType(driver, Elements.MyAnalog_Projects_CreateANewProject_Modal_ProjectName_InputBox, projectName_input);
            Thread.Sleep(1000);

            //--- Action: Click Done button ---//
            action.IClick(driver, Elements.MyAnalog_Projects_CreateANewProject_Modal_Done_Button);

            //--- Expected Result: Project name highlighted with red line. ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Projects_CreateANewProject_Modal_ProjectName_InputBox_Error);
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Input Validations are Working as Expected when Project Name is Already Existing in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Input Validations are Working as Expected when Project Name is Already Existing in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Input Validations are Working as Expected when Project Name is Already Existing in JP Locale")]
        public void MyAnalog_Projects_CreateANewProjectModal_InputValidations_VerifyWhenProjectNameIsAlreadyExisting(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Projects URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_projects_url;

            //--- Action: Open myAnalog Projects using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify if error message/prompt appear upon creating with existing Project name (IQ-7628/AL-13949) -----//

            //--- Action: Get the Name of the First Project in the Project Listing ---//
            string first_projectName_label = util.GetText(driver, Elements.MyAnalog_Projects_First_Project_Name_Link);

            //--- Action: Click Create a New Project button ---//
            action.IClick(driver, Elements.MyAnalog_Projects_CreateANewProject_Button);
            Thread.Sleep(1000);

            //--- Action: Enter duplicate/existing project name in Project Name field ---//
            string projectName_input = first_projectName_label;
            action.IDeleteValueOnFields(driver, Elements.MyAnalog_Projects_CreateANewProject_Modal_ProjectName_InputBox);
            action.IType(driver, Elements.MyAnalog_Projects_CreateANewProject_Modal_ProjectName_InputBox, projectName_input);

            //--- Action: Click Done button ---//
            action.IClick(driver, Elements.MyAnalog_Projects_CreateANewProject_Modal_Done_Button);

            //--- Action: Error message/prompt is displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Projects_CreateANewProject_Modal_Error_Message);
        }
    }
}