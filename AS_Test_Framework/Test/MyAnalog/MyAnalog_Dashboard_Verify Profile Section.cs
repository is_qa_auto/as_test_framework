﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using OpenQA.Selenium;
using System;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_Dashboard_ProfileSection : BaseSetUp
    {
        public MyAnalog_Dashboard_ProfileSection() : base() { }

        //-- Login Credentials --//
        string username = "marvin.bebe@analog.com";
        string password = "Test_1234";

        //--- URLs ---//
        string myAnalog_dashboard_url = "app";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Profile Section is Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Profile Section is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Profile Section is Present and Working as Expected in JP Locale")]
        public void MyAnalog_Dashboard_VerifyProfileSection(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Dashboard URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_dashboard_url;

            //--- Action: Go to myAnalog dashboard ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid user account in the myAnalog Login page ---//
            action.ILogin(driver, username, password);

            //----- Masthead > Test Case Title: Verify Percentage beside the Avatar is displayed (IQ-6377/AL-13458) -----//

            //--- Expected Result: Percentage displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Profile_Percentage);

            //----- Dashboard Test Plan > Desktop Tab > R2 > T1: Verify myAnalog Profile section is present in left rail -----//

            //--- Expected Result: myAnalog Profile section should be present ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Profile_Section);

            //----- Dashboard Test Plan > Desktop Tab > R2 > T18: Verify that the Avatar section is displayed correctly in IE11 -----//

            //--- Expected Result: Initials are displayed---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Profile_Initials_Value);

            //--- Expected Result: Edit link is displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Profile_Edit_Button);
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Edit Profile Menu is Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Edit Profile Menu is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Edit Profile Menu is Present and Working as Expected in JP Locale")]
        public void MyAnalog_Dashboard_ProfileSection_VerifyEditProfileMenu(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Dashboard URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_dashboard_url;

            //--- Action: Go to myAnalog dashboard ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid user account in the myAnalog Login page ---//
            action.ILogin(driver, username, password);

            //----- Dashboard Test Plan > Desktop Tab > R2 > T1.1: Verify Edit Profile -----//

            //--- Action: Click Edit ---//
            action.IClick(driver, Elements.MyAnalog_Profile_Edit_Button);

            //--- Expected Result: Edit Profile is dislayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Profile_EditProfile_Menu);

            if (util.CheckElement(driver, Elements.MyAnalog_Profile_EditProfile_Menu, 2))
            {
                //--- Expected Result: Percentage of profile is also displayed ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_Profile_EditProfile_Menu_ProfileCompletion_Percentage);

                //--- Expected Result: Initials are displayed ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_Profile_EditProfile_Menu_Initials_Value);

                //--- Expected Result: Profice score checklist are displaying ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_Profile_EditProfile_Menu_ProfileScore_Checklist);

                //--- Expected Result: 10 color selections are available ---//
                test.validateCountIsEqual(driver, 10, util.GetCount(driver, Elements.MyAnalog_Profile_EditProfile_Menu_Color_Selection));
            }
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that Change Profile Color is Working as Expected in EN Locale")]
        public void MyAnalog_Dashboard_ProfileSection_VerifyChangeProfileColor(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Dashboard URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_dashboard_url;

            //--- Action: Go to myAnalog dashboard ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid user account in the myAnalog Login page ---//
            action.ILogin(driver, username, password);

            //----- Dashboard Test Plan > Desktop Tab > R2 > T1.1: Verify Edit Profile -----//

            //--- Action: Click Edit ---//
            action.IClick(driver, Elements.MyAnalog_Profile_Edit_Button);

            //--- Action: Change color ---//
            int colorSelections_count = util.GetCount(driver, Elements.MyAnalog_Profile_EditProfile_Menu_Color_Selection);
            Random rand = new Random();
            int random_number = rand.Next(1, colorSelections_count);
            string colorToBeSelected_value = util.ReturnAttribute(driver, By.CssSelector("div[class='palette']>button:nth-of-type(" + random_number + ")"), "class").Split(' ')[0];

            action.IClick(driver, By.CssSelector("div[class='palette']>button:nth-of-type(" + random_number + ")"));

            //--- Expected Result: Profile image color changes ---//
            test.validateStringInstance(driver, colorToBeSelected_value, util.ReturnAttribute(driver, Elements.MyAnalog_Profile_EditProfile_Menu_Profile_Color, "class").Split(' ')[0]);

            //--- Action: Click Save ---//
            action.IClick(driver, Elements.MyAnalog_Profile_EditProfile_Menu_SaveAndClose_Button);

            //--- Expected Result: Changes saves successfully ---//
            test.validateStringInstance(driver, colorToBeSelected_value, util.ReturnAttribute(driver, Elements.MyAnalog_Profile_Color, "class").Split(' ')[0]);
        }
    }
}