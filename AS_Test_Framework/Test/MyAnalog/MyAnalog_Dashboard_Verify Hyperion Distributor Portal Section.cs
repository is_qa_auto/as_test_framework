﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_Dashboard_HyperionDistributorPortalSection : BaseSetUp
    {
        public MyAnalog_Dashboard_HyperionDistributorPortalSection() : base() { }

        //-- Login Credentials --//
        string username = "marvin.bebe@analog.com";
        string password = "Test_1234";

        //--- URLs ---//
        string myAnalog_dashboard_url = "app";
        string accessRequest_form_url = "/accessrequestform.aspx";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Hyperion Distributor Portal Section is Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Hyperion Distributor Portal Section is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Hyperion Distributor Portal Section is Present and Working as Expected in JP Locale")]
        public void MyAnalog_Dashboard_VerifyHyperionDistributorPortalSection(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Dashboard URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_dashboard_url;

            //--- Action: Go to myAnalog dashboard ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid user account in the myAnalog Login page ---//
            action.ILogin(driver, username, password);

            //----- Dashboard Test Plan > Desktop Tab > R2 > T11.1: Verify when clicking Distributor Portal hyperlink -----//

            //--- Expected Result: Hyperion logo is displayed ---//
            test.validateImageIsNotBeBroken(driver, Elements.MyAnalog_Hyperion_Logo);

            //--- Expected Result: Distributor Portal link is displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_DistributorPortal_Link);

            if (util.CheckElement(driver, Elements.MyAnalog_DistributorPortal_Link, 2))
            {
                //--- Action: Click Distributor Portal hyperlink ---//
                action.IOpenLinkInNewTab(driver, Elements.MyAnalog_DistributorPortal_Link);

                //--- Expected Result: The System displays the Initiate Request for Access to Partner Applications Portal page (IQ-6367/AL-13594, IQ-7675/AL-14929) ---//
                test.validateStringInstance(driver, driver.Url, accessRequest_form_url);
            }
        }
    }
}