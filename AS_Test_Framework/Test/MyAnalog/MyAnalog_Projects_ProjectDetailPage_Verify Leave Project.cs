﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using OpenQA.Selenium;
using System;
using System.Threading;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_Projects_ProjectDetailPage_LeaveProject : BaseSetUp
    {
        //--- myAnalog Projects URL: ---//
        //--- DEV: https://my-dev.corpnt.analog.com/en/app/projects ---//
        //--- QA: https://my-qa.corpnt.analog.com/en/app/projects ---//
        //--- PROD: https://my.analog.com/en/app/projects ---//

        public MyAnalog_Projects_ProjectDetailPage_LeaveProject() : base() { }

        //----- Note: Please don't edit/delete any details on the Accounts below -----//

        //--- Login Credentials (Project Participant) ---// 
        string username_participant = "my_t3st_acc3pt_pr0ject_1nv1t3_555@mailinator.com";
        string password_participant = "Test_1234";

        //--- Login Credentials (Project Owner) ---// 
        string username_owner = "my_t3st_pr0j3ct_0wn3r_555@mailinator.com";
        string password_owner = "Test_1234";

        //--- URLs ---//
        string myAnalog_projects_url = "app/projects";
        string myAnalog_logout_url = "app/logout";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that Leaving a Project is Working as Expected in EN Locale")]
        public void MyAnalog_Projects_ProjectDetailPage_VerifyLeaveProject(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Projects URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_projects_url;

            //--- Log Out URL ---//
            string logout_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_logout_url;

            //--- Action: Open myAnalog Projects using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Enter Email Address of the invited User in the 'Email' field ---//
            //--- Action: Enter Password of the invited User in the 'Password' field ---//
            //--- Action: Click the Login button ---//
            action.ILogin(driver, username_participant, password_participant);

            //--- Action: Join a Project in myAnalog if there's none ---//
            string first_projectName_label = null;
            if (util.GetCount(driver, Elements.MyAnalog_Projects_First_Project_Contributor_Avatars) < 2)
            {
                Console.WriteLine("Join a Project:");

                //--- Action: Sign Out of myAnalog ---//
                driver.Navigate().GoToUrl(logout_url);
                Console.WriteLine("\tGo to Log Out URL: " + logout_url);

                //--- Action: Open myAnalog Projects using direct link ---//
                Console.Write("\t");
                action.Navigate(driver, myAnalog_url);

                //--- Action: Login valid credentials (Project Owner) and click login ---//
                action.ILogin(driver, username_owner, password_owner);

                //--- Action: Get the Name of the First Project in the Project Listing ---//
                first_projectName_label = util.GetText(driver, Elements.MyAnalog_Projects_First_Project_Name_Link);

                //--- Action: Create a Project Invite in myAnalog ---//
                Console.Write("\t");
                action.ICreateAProjectInviteInMyAnalog(driver, Locale, username_participant);

                //--- Action: Sign Out of myAnalog ---//
                driver.Navigate().GoToUrl(logout_url);
                Console.WriteLine("\tGo to Log Out URL: " + logout_url);

                //--- Action: Open myAnalog Projects using direct link ---//
                Console.Write("\t");
                action.Navigate(driver, myAnalog_url);

                //--- Action: Login valid credentials (Project Participant) and click login ---//
                action.ILogin(driver, username_participant, password_participant);

                //--- Action: Click the Accept Button ---//
                Console.Write("\t");
                action.IClick(driver, Elements.MyAnalog_Projects_NewInvites_Accept_Button);

                driver.Navigate().Refresh();
                Thread.Sleep(2000);
                Console.WriteLine("\tRefresh the Page.");
            }

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify ability to Leave Project User doesn't own (IQ-7269/ALA-11874) -----//

            //--- Action: Click the Project Details button of the Project ---//
            action.IClick(driver, Elements.MyAnalog_Projects_ProjectDetails_Buttons);

            //--- Action: Get the Project Name ---//
            string projectName_label = util.GetText(driver, Elements.MyAnalog_ProjectDetailPage_ProjectName_Label);
            if (string.IsNullOrEmpty(first_projectName_label))
            {
                first_projectName_label = projectName_label;
            }

            //--- Action: Click the ellipsis beside the Project Name ---//
            action.IClick(driver, Elements.MyAnalog_ProjectDetailPage_Ellipsis_Button);

            //--- Action: Click the Leave Project button in the popup ---//
            action.IClick(driver, By.CssSelector("menu[class='options dropdown']>button"));

            //--- Expected Result: The System displays 'Are you sure you want to leave project "{project name}"?' in the popup box (IQ-9088/AL-15977) ---//
            if (Locale.Equals("en"))
            {
                test.validateStringIsCorrect(driver, Elements.MyAnalog_ProjectDetailPage_LeaveProject_Modal_Message, "Are you sure you want to leave project \"" + projectName_label + "\"?");
            }
            else
            {
                test.validateStringInstance(driver, util.GetText(driver, Elements.MyAnalog_ProjectDetailPage_LeaveProject_Modal_Message), projectName_label);
            }

            //--- Action: Click the Yes! Leave button ---//
            action.IClick(driver, Elements.MyAnalog_ProjectDetailPage_LeaveProject_YesLeave_Button);
            Thread.Sleep(2000);

            //--- Expected Result: The System displays the Projects page ---//
            test.validateScreenByUrl(driver, myAnalog_url);

            driver.Navigate().Refresh();
            Console.WriteLine("\tRefresh the Page.");

            //--- Expected Result: The Project is removed from the list after leaving ---//
            test.validateStringChanged(driver, util.GetText(driver, Elements.MyAnalog_Projects_First_Project_Name_Link), projectName_label);

            //--- Action: Go to the Mailbox of the Project Owner ---//
            action.ICheckMailinatorEmail(driver, username_owner);

            //--- Expected Result: Project owner will receive an email about leaving project (IQ-9089/AL-15870) ---//
            test.validateElementIsPresent(driver, Elements.Mailinator_LatestEmail);

            //--- Action: Sign Out of myAnalog ---//
            driver.Navigate().GoToUrl(logout_url);
            Console.WriteLine("Go to Log Out URL: " + logout_url);

            //--- Action: Open myAnalog Projects using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Enter Email Address of the Project Owner in the 'Email' field ---//
            //--- Action: Enter Password of the Project Owner in the 'Password' field  ---//
            //--- Action: Click the Login button ---//
            action.ILogin(driver, username_owner, password_owner);

            //--- Expected Result: The Project has 1 Contributor (Owner of the Project) ---//
            test.validateCountIsEqual(driver, 1, util.GetCount(driver, Elements.MyAnalog_Projects_First_Project_Contributor_Avatars));

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify when the participant delete the project (non-owner) -----//

            //--- Expected Result: The project should not be deleted to owner ---//
            test.validateStringIsCorrect(driver, Elements.MyAnalog_Projects_First_Project_Name_Link, first_projectName_label);

        }
    }
}