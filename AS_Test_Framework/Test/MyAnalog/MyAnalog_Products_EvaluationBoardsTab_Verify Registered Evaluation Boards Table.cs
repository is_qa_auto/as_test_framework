﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System;
using System.Drawing;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_Products_EvaluationBoardsTab_RegisteredEvaluationBoardsTable : BaseSetUp
    {
        //--- myAnalog Products URL: ---//
        //--- DEV: https://my-dev.corpnt.analog.com/en/app/products ---//
        //--- QA: https://my-qa.corpnt.analog.com/en/app/products ---//
        //--- PROD: https://my.analog.com/en/app/products ---//

        public MyAnalog_Products_EvaluationBoardsTab_RegisteredEvaluationBoardsTable() : base() { }

        //--- Login Credentials ---//
        string username = "myanalog_ev4l_b04rds_test@mailinator.com";
        string password = "Test_1234";

        //--- URLs ---//
        string myAnalog_products_url = "app/products";
        string searchResults_page_url = "/search.html";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Registered Evaluation Boards are Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Registered Evaluation Boards are Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Registered Evaluation Boards are Present and Working as Expected in JP Locale")]
        public void MyAnalog_Products_EvaluationBoardsTab_SavedProductsTable_VerifySavedProducts(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- Action: Set the Window to a Larger Size since the Ellipsis Buttons are being cut in Small Screens ---//
            driver.Manage().Window.Size = new Size(1920, 1080);

            //--- myAnalog Products URL ---//
            string myAnalog_url = $"{Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt")}{Locale}/{myAnalog_products_url}";

            //--- Action: Open myAnalog Products using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);

            //--- Action: Select the Evaluation Boards Tab ---//
            action.IClick(driver, Elements.MyAnalog_Products_EvaluationBoards_Tab);

            //----- Hardware Evalboard Registration > Test Case Title: Verify if the "Registered Evaluation Boards" section display in the page. -----//

            //--- Expected Result: "Registered Evaluation Boards" section displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Products_RegisteredEvaluationBoards_Section);

            //----- Hardware Evalboard Registration > Test Case Title: Verify if the "Model Number" display in "Saved Evaluation Boards" section. (IQ-11468/AL-18234) -----//

            //--- Expected Result: The "Model Number" display ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Products_ModelNumber_Column_Header);

            //----- Hardware Evalboard Registration > Test Case Title: Verify if the "Serial Number" display in "Registered Evaluation Boards" section -----//

            //--- Expected Result: "Serial Number" displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Products_SerialNumber_Column_Header);

            //----- Hardware Evalboard Registration > Test Case Title: Verify if the "Product Version" display in "Registered Evaluation Boards" section -----//

            //--- Expected Result: "Product Version" displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Products_ProductVersion_Column_Header);

            //----- Hardware Evalboard Registration > Test Case Title: Verify if the "Notify me" Toggle display in "Registered Evaluation Boards" section -----//

            //--- Expected Result: "Notify me" Toggle displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Products_NotifyMe_Column_Header);
            test.validateElementIsPresent(driver, Elements.MyAnalog_Products_NotifyMe_Switches);

            //----- Hardware Evalboard Registration > Test Case Title: Verify if the "Options dropdown" display in "Registered Evaluation Boards" section -----//

            //--- Expected Result: "Options dropdown" displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Products_RegisteredEvaluationBoards_Ellipsis_Button);

            //----- Hardware Evalboard Registration > Test Case Title: Verify if the Model number is hyperlinked to search page -----//

            //--- Action: Click any Model number link ---//
            action.IOpenLinkInNewTab(driver, Elements.MyAnalog_Products_ModelNumber_Links);

            //--- Expected Result: The page redirected to search page ---//
            test.validateStringInstance(driver, driver.Url, searchResults_page_url);
        }
    }
}