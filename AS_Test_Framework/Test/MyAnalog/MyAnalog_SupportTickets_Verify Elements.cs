﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_SupportTickets_Elements : BaseSetUp
    {
        public MyAnalog_SupportTickets_Elements() : base() { }

        //--- Login Credentials ---//
        string username = "myanalog_t3st_555@mailinator.com";
        string password = "Test_1234";

        //--- URLs ---//
        string myAnalog_supportTickets_url = "app/support";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Elements are Present and Working as Expected in EN Locale")]
        public void MyAnalog_SupportTickets_VerifyElements(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Support Tickets URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_supportTickets_url;

            //--- Action: Open Support Tickets page direct link to browser ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Enter valid username and password and click login button ---//
            action.ILogin(driver, username, password);

            //----- SUPPORT TICKETS -----//

            //----- Tech Support Test Plan > Test Case Title: Verify myAnalog Support Tickets Landing Page -----//

            //--- Expected Result: The Support Tickets - Label is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_SupportTickets_Label);

            //----- CREATE A NEW SUPPORT TICKET -----//

            //----- Tech Support Test Plan > Test Case Title: Verify myAnalog Support Tickets Landing Page -----//

            //--- Expected Result: The Create a New Support Ticket - Button is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_SupportTickets_CreateANewSupportTicket_Button);

            //----- SUPPOR TTICKETS TABLE -----//

            //----- Tech Support Test Plan > Test Case Title: Verify myAnalog Support Tickets Landing Page -----//

            //--- Expected Result: The Case Title - Column Header is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_SupportTickets_CaseTitle_Column_Header);

            //--- Expected Result: The Case Number - Column Header is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_SupportTickets_CaseNumber_Column_Header);

            //--- Expected Result: The Created On - Column Header is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_SupportTickets_CreatedOn_Column_Header);

            //--- Expected Result: The Primary Product - Column Header is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_SupportTickets_PrimaryProduct_Column_Header);

            //--- Expected Result: The Status - Column Header is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_SupportTickets_Status_Column_Header);
        }
    }
}