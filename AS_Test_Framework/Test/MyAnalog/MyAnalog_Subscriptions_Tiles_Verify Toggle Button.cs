﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using OpenQA.Selenium;
using System;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_Subscriptions_Tiles_ToggleButton : BaseSetUp
    {
        //--- myAnalog Subscriptions URL: ---//
        //--- DEV: https://my-dev.corpnt.analog.com/en/app/subscriptions ---//
        //--- QA: https://my-qa.corpnt.analog.com/en/app/subscriptions ---//
        //--- PROD: https://my.analog.com/en/app/subscriptions ---//

        public MyAnalog_Subscriptions_Tiles_ToggleButton() : base() { }

        //--- Login Credentials ---//
        string username = "myanalog_t3st_subscripti0ns_555@mailinator.com";
        string password = "Test_1234";

        //--- URLs ---//
        string myAnalog_subscriptions_url = "app/subscriptions";
        string myAnalog_dashboard_url = "app/";

        //--- Labels ---//
        string subscriptionsAlert_disclaimer_text = "You didn't save the changes you made with your subscriptions";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Toggle Button is Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Toggle Button is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Toggle Button is Present and Working as Expected in JP Locale")]
        public void MyAnalog_Subscriptions_Tiles_VerifyToggleButton(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Subscriptions URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_subscriptions_url;
            myAnalog_dashboard_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_dashboard_url;

            //--- Action: Open subscription page ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);

            //----- Subscription - Dashboard > Desktop Tab > R4 > T11: Verify when unsubsribing to all available tiles -----//

            //--- Action: Click toggle button ---//
            action.IClick(driver, Elements.MyAnalog_Subscriptions_Tiles_Toggle_Button);

            //--- Action: Click save changes ---//
            action.IClick(driver, Elements.MyAnalog_Subscriptions_SaveChanges_Button);
            Thread.Sleep(5000);

            bool toggle_selected = driver.FindElement(Elements.MyAnalog_Subscriptions_Tiles_Toggle_Button).Selected;

            //--- Action: Go to the Mailbox of the User ---//
            ((IJavaScriptExecutor)driver).ExecuteScript("window.open()");
            driver.SwitchTo().Window(driver.WindowHandles.Last());
            action.ICheckMailinatorEmail(driver, username);

            //--- Expected Result: Email Should be received when you unsubscribed ---//
            test.validateElementIsPresent(driver, Elements.Mailinator_LatestEmail);

            //----- Subscription - Dashboard > Desktop Tab > R4 > T12: Verify when subsribing to myAnalog but later go back and not subscribed (IQ-6324/AL-13566) -----//

            //--- Action: Leave the page ---//
            driver.Navigate().GoToUrl(Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_dashboard_url);
            Console.WriteLine("Leave the page, go to: " + Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_dashboard_url);

            //--- Action: Return to subscription page ---//
            driver.Navigate().GoToUrl(myAnalog_url);
            Console.WriteLine("Go back to myAnalog Subscriptions: " + myAnalog_url);

            //--- Expected Result: The changes in the Subscription is still saved ---//
            test.validateSelectedRadioButtonIsCorrect(driver, Elements.MyAnalog_Subscriptions_Tiles_Toggle_Button, toggle_selected);
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Toggle Button is Present and Working as Expected after Clicking Leave Page in Subscriptions Alert in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Toggle Button is Present and Working as Expected after Clicking Leave Page in Subscriptions Alert in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Toggle Button is Present and Working as Expected after Clicking Leave Page in Subscriptions Alert in JP Locale")]
        public void MyAnalog_Subscriptions_Tiles_ToggleButton_VerifyAfterClickingLeavePageInSubscriptionsAlert(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Subscriptions URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_subscriptions_url;

            //--- Action: Open subscription page ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);

            //----- Subscription - Dashboard > Desktop Tab > R2 > T8: Verify the consent disclaimer upon leaving the page without clicking save changes button. (IQ-8794/ ALA-13754) -----//

            //--- Action: Click toggle button ---//
            action.IClick(driver, Elements.MyAnalog_Subscriptions_Tiles_Toggle_Button);

            //--- Action: Click myAnalog menu on the left panel without saving the changes ---//
            action.IClick(driver, Elements.MyAnalog_Navigation_MyAnalog_Link);

            //--- Expected Result: The System displays a popup box with Confirmation Message: You didn't save the changes you made with your subscriptions ---//
            if (Locale.Equals("en"))
            {
                test.validateStringIsCorrect(driver, Elements.MyAnalog_Subscriptions_Alert_Disclaimer_Text, subscriptionsAlert_disclaimer_text);
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.MyAnalog_Subscriptions_Alert_Disclaimer_Text);
            }

            //----- Subscription - Dashboard > Desktop Tab > R6 > T1: Verify Unsaved Subscription Changes (IQ-7343/ALA-11977) -----//
            bool toggle_selected = driver.FindElement(Elements.MyAnalog_Subscriptions_Tiles_Toggle_Button).Selected;

            //--- Action: Click Leave Page button ---//
            action.IClick(driver, Elements.MyAnalog_Subscriptions_Alert_LeavePage_Button);

            //--- Action: Open subscription page ---//
            driver.Navigate().GoToUrl(myAnalog_url);
            Console.WriteLine("Go back to myAnalog Subscriptions: " + myAnalog_url);

            //--- Expected Result: The change in the Subscription has NOT been saved ---//
            test.validateSelectedRadioButtonIsCorrect(driver, Elements.MyAnalog_Subscriptions_Tiles_Toggle_Button, toggle_selected);
        }
    }
}