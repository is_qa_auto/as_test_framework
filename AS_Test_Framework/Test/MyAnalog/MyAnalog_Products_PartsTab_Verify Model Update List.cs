﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_Products_PartsTab_ModelUpdateList : BaseSetUp
    {
        //--- myAnalog Products URL: ---//
        //--- DEV: https://my-dev.corpnt.analog.com/en/app/products ---//
        //--- QA: https://my-qa.corpnt.analog.com/en/app/products ---//
        //--- PROD: https://my.analog.com/en/app/products ---//

        public MyAnalog_Products_PartsTab_ModelUpdateList() : base() { }

        //--- Login Credentials ---//
        string username = "myanalog_t3st_pr0ducts_555@mailinator.com";
        string password = "Test_1234";

        //--- URLs ---//
        string myAnalog_products_url = "app/products";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Model Update List is Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Model Update List is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Model Update List is Present and Working as Expected in JP Locale")]
        public void MyAnalog_Products_PartsTab_VerifyModelUpdateList(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Products URL ---//
            string myAnalog_url = $"{Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt")}{ Locale }/{myAnalog_products_url}";

            //--- Action: Open myAnalog Products using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);

            //--- Action: Select the Parts Tab ---//
            if (!util.CheckElement(driver, Elements.MyAnalog_Products_Selected_Parts_Tab, 2))
            {
                action.IClick(driver, Elements.MyAnalog_Products_Parts_Tab);
            }

            //--- Action: Click anywhere in dark blue table header ---//
            action.IClick(driver, Elements.MyAnalog_Products_Saved_Products_ManagePcnPdn);

            if (util.CheckElement(driver, Elements.MyAnalog_Products_ProductDetails_Updates_Row_PcnPdn_Button, 1))
            {
                //--- Action: click pcn/pdn updates ---//
                action.IClick(driver, Elements.MyAnalog_Products_ProductDetails_Updates_Row_PcnPdn_Button);

                //----- Product Test Plan > MAR>Dashboard>Products Tab > R7 > T4.3.1.1: Verify the modal or pop up box displayed upon clicking the Update value PCN link. -----//

                //--- Expected Result: type is displayed ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_Products_ModelUpdateList_Type_Row_Value);

                //--- Expected Result: Notification # is displayed ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_Products_ModelUpdateList_NotificationNumber_Row_Value);

                //--- Expected Result: description is displayed ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_Products_ModelUpdateList_Description_Row_Value);

                //--- Expected Result: Publishing date is displayed ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_Products_ModelUpdateList_PublicationDate_Row_Value);

                //--- Expected Result: documents are displayed ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_Products_ModelUpdateList_Documents_Row_Value);
            }
        }
    }
}