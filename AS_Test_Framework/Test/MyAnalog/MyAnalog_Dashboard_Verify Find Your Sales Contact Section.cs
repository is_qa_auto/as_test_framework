﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System;
using System.Threading;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_Dashboard_FindYourSalesContactSection : BaseSetUp
    {
        public MyAnalog_Dashboard_FindYourSalesContactSection() : base() { }

        //-- Login Credentials --//
        string username = "marvin.bebe@analog.com";
        string password = "Test_1234";

        //--- URLs ---//
        string myAnalog_dashboard_url = "app";
        string findSaleOfficeAndDistributor_page_url = "support/customer-service-resources/sales/find-sale-office-distributor.html";
        string salesAndDistribution_cn_page_url = "about-adi/landing-pages/002/sales-and-distributors.html";
        string salesAndDistribution_jp_page_url = "about-adi/landing-pages/003/jp-sales-and-disti.html";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Find Your Sales Contact Section is Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Find Your Sales Contact Section is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Find Your Sales Contact Section is Present and Working as Expected in JP Locale")]
        public void MyAnalog_Dashboard_VerifyFindYourSalesContactSection(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Dashboard URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_dashboard_url;

            //--- Action: Go to myAnalog dashboard ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid user account in the myAnalog Login page ---//
            action.ILogin(driver, username, password);

            //----- Dashboard Test Plan > Desktop Tab > R2 > T10.2: Verify local sales contract logo -----//

            //--- Expected Result: Logo is displaying ---//
            test.validateImageIsNotBeBroken(driver, Elements.MyAnalog_FindYourSalesContact_Logo);

            //----- Dashboard Test Plan > Desktop Tab > R2 > T10.1: Verify local sales contact -----//

            //--- Expected Result: Find your locale Sales Contact is a link ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_FindYourSalesContact_Link);

            if (util.CheckElement(driver, Elements.MyAnalog_FindYourSalesContact_Link, 2))
            {
                //----- Dashboard Test Plan > Desktop Tab > R2 > T10.3: Verify when clicking Find Your Sales Contact link -----//

                //--- Action: Click Find Your Sales Contact link ---//
                action.IOpenLinkInNewTab(driver, Elements.MyAnalog_FindYourSalesContact_Link);
                Thread.Sleep(2000);

                //--- Expected Result: The System displays the Find Sale Office and Distributor page ---//
                if (Locale.Equals("cn") || Locale.Equals("zh"))
                {
                    test.validateStringInstance(driver, driver.Url, "analog.com/cn/" + salesAndDistribution_cn_page_url);
                }
                else if (Locale.Equals("jp"))
                {
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale.ToLower() + "/" + salesAndDistribution_jp_page_url);
                }
                else
                {
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale.ToLower() + "/" + findSaleOfficeAndDistributor_page_url);
                }
            }
        }
    }
}