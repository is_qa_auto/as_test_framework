﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System;
using System.Threading;
using System.Linq;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_MegaMenu : BaseSetUp
    {
        public MyAnalog_MegaMenu() : base() { }

        //--- Login Credentials ---//
        string username = "myanalog_t3st_555@mailinator.com";
        string password = "Test_1234";

        //--- URLs ---//
        string newMyAnalogExperience_page_url = "/new-myanalog-experience.html";
        string login_page_url = "b2clogin";
        string registration_page_url = "/app/registration";
        //string applications_page_url = "/applications/markets/aerospace-and-defense-pavilion-home.html";
        //string education_page_url = "/education/education-library/videos.html";
        //string evalBoard_page_url = "/EVAL-HMC742ALP5";
        string form_page_url = "Form_Pages/support/customerService.aspx";
        string pdp_url = "/ad4112";
        //string pst_url = "/parametricsearch/11502";
        //string sitemap_page_url = "/sitemap.html";

        //[Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        //[TestCase("en", TestName = "Verify that the MYANALOG Mega Menu Content are Present in a PST in EN Locale")]
        //[TestCase("zh", TestName = "Verify that the MYANALOG Mega Menu Content are Present in a PST in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the MYANALOG Mega Menu Content are Present in a PST in JP Locale")]
        //public void MyAnalog_MegaMenu_LoggedInState_VerifyInPst(string Locale)
        //{
        //    Console.WriteLine("Steps to Test:");

        //    //--- myAnalog Dashboard URL ---//
        //    string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_dashboard_url;

        //    action.Navigate(driver, myAnalog_url);

        //    //--- Action: Log in valid account in login page ---//
        //    action.ILogin(driver, username, password);

        //    //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R1 > T6: Verify Navigation to myPage from Parametric Searches -----//

        //    //--- Action: Open parametric search ---//
        //    string page_url = page_url = Configuration.Env_Url + Locale + pst_url;
        //    driver.Navigate().GoToUrl(page_url);
        //    Thread.Sleep(1000);
        //    Console.WriteLine("Go to a PST: " + page_url);

        //    //--- Action: Click again myAnalog tab ---//
        //    action.IExpandMyAnalogMegaMenu(driver);
        //    Thread.Sleep(1000);

        //    //--- Expected Result: mypage is displayed ---//
        //    test.validateElementIsPresent(driver, Elements.MyAnalog_MegaMenu_Tab_Content);
        //}

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Core Page - Verify that the MYANALOG Mega Menu is Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Core Page - Verify that the MYANALOG Mega Menu is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Core Page - Verify that the MYANALOG Mega Menu is Present and Working as Expected in JP Locale")]
        public void MyAnalog_MegaMenu_VerifyInProductsPage(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- Product Detail Page URL ---//
            string page_url = null;
            if (Locale.Equals("cn") || Locale.Equals("zh"))
            {
                page_url = Configuration.Env_Url + "cn" + pdp_url;
            }
            else
            {
                page_url = Configuration.Env_Url + Locale + pdp_url;
            }

            //--- Action: Open Product Detail Page ---//
            action.Navigate(driver, page_url);

            //---------- LOGGED OUT ----------//

            //----- MYANALOG TAB -----//

            //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R1 > T1: Navigate to myAnalog menu when account is not logged in (IQ-6133/AL-13651) -----//

            //--- Expected Result: myAnalog tab should be a constant option ---//
            test.validateElementIsPresent(driver, Elements.MainNavigation_MyAnalogTab);

            //--- Expected Result: myanalog logo (wave) is not displayed in myAnalog tab label when account is not login ---//
            test.validateElementIsNotPresent(driver, Elements.MyAnalog_MegaMenu_Tab_Wave_Icon);

            /****Removing this item due to changes for AL-18380*****/
            //--- Action: Click myAnalog tab ---//
            action.IExpandMyAnalogMegaMenu(driver);

            ////----- REGISTER NOW -----//

            ////----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R1 > T2: Verify display when clicking myAnalog menu when account is not logged in -----//

            ////--- Expected Result: The “Register now” header is available ---//
            //test.validateElementIsPresent(driver, Elements.MyAnalog_MegaMenu_RegisterNow_Header_Text);

            ////--- Expected Result: The Welcome copy is available ---//
            //test.validateElementIsPresent(driver, Elements.MyAnalog_MegaMenu_RegisterNow_Description_Text);

            ////--- Expected Result: The Get Started button is available ---//
            //test.validateElementIsPresent(driver, Elements.MyAnalog_MegaMenu_GetStarted_Button);

            ////--- Expected Result: The Video is available ---//
            //test.validateElementIsPresent(driver, Elements.MyAnalog_MegaMenu_Video);

            //if (util.CheckElement(driver, Elements.MyAnalog_MegaMenu_ExploreMyAnalogFeaturesNow_Link, 2))
            //{
            //    //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R1 > T2.1: Verify SSO mega menu (AL-14069/IQ-6648) -----//

            //    //--- Action: Click Explore myAnalog features now link ---//
            //    action.IOpenLinkInNewTab(driver, Elements.MyAnalog_MegaMenu_ExploreMyAnalogFeaturesNow_Link);
            //    Thread.Sleep(2000);

            //    //--- Expected Result: Page will redirect to: New CTA to go to the myAnalog Features page ---//
            //    test.validateStringInstance(driver, driver.Url, newMyAnalogExperience_page_url);

            //    driver.Close();
            //    driver.SwitchTo().Window(driver.WindowHandles.Last());
            //}

            //----- LOGIN -----//

            //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R1 > T2: Verify display when clicking myAnalog menu when account is not logged in -----//

            //--- Expected Result: The Login” header is available ---//
            //test.validateElementIsPresent(driver, Elements.MyAnalog_MegaMenu_Login_Header_Text);

            //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R1 > T2.1: Verify SSO mega menu (AL-14069/IQ-6648) -----//

            //--- Expected Result: The login description is available ---//
            //test.validateElementIsPresent(driver, Elements.MyAnalog_MegaMenu_Login_Description_Text);

            //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R1 > T2: Verify display when clicking myAnalog menu when account is not logged in -----//

            //--- Expected Result: Line: “Don’t have an account?" is available ---//
            //test.validateElementIsPresent(driver, Elements.MyAnalog_MegaMenu_DontHaveAnAccount_Text);

            //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R1 > T2.1: Verify SSO mega menu (AL-14069/IQ-6648) -----//

            //--- Expected Result: The Register now link is available ---//
            //test.validateElementIsPresent(driver, Elements.MyAnalog_MegaMenu_RegisterNow_Link);

            //if (util.CheckElement(driver, Elements.MyAnalog_MegaMenu_RegisterNow_Link, 2))
            //{
            //    //--- Action: Click Register now link ---//
            //    action.IOpenLinkInNewTab(driver, Elements.MyAnalog_MegaMenu_RegisterNow_Link);
            //    Thread.Sleep(1000);

            //    //--- Expected Result: Page will redirect to Registering is easy. Start by selecting your product categories page ---//
            //    test.validateStringInstance(driver, driver.Url, registration_page_url);

            //    driver.Close();
            //    driver.SwitchTo().Window(driver.WindowHandles.Last());
            //}

            ////--- Expected Result: The Login to myanalog button is available ---//
            //test.validateElementIsPresent(driver, Elements.MyAnalog_MegaMenu_LogInToMyAnalog_Button);

            //if (util.CheckElement(driver, Elements.MyAnalog_MegaMenu_LogInToMyAnalog_Button, 2))
            //{
            //    //--- Action: Click Login to myanalog button ---//
            //    action.IClick(driver, Elements.MyAnalog_MegaMenu_LogInToMyAnalog_Button);

            //    //--- Expected Result: Page will redirect to Login Page ---//
            //    test.validateStringInstance(driver, driver.Url, login_page_url);
            //}

            //---------- LOGGED IN ----------//

            //--- Action: Log in valid account in login page ---//
            action.ILogin(driver, username, password);

            //----- MYANALOG TAB -----//

            //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R2 > T1: Verify myPage Landing Page -----//

            //--- Expected Result: myAnalog logo (wave) is displayed in myAnalog tab label ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_MegaMenu_Tab_Wave_Icon);

            //----- MYANALOG MEGA MENU -----//

            //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R1 > T8: Verify Navigation to myPage from Product Detail Page -----//

            //--- Action: Click again myAnalog tab ---//
            //action.IExpandMyAnalogMegaMenu(driver);

            //--- Expected Result: mypage is displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_MegaMenu_Tab_Content);
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Non Core Page - Verify that the MYANALOG Mega Menu is Present and Working as Expected in EN Locale")]
        public void MyAnalog_MegaMenu_VerifyInFormPage(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- Forms page URL ---//
            string page_url = Configuration.Env_Url.Replace("www", "form").Replace("cldnet", "corpnt") + form_page_url;

            //--- Action: Open Forms page ---//
            action.Navigate(driver, page_url);

            //---------- LOGGED OUT ----------//

            //----- MYANALOG TAB -----//

            //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R1 > T1: Navigate to myAnalog menu when account is not logged in (IQ-6133/AL-13651) -----//

            //--- Expected Result: myAnalog tab should be a constant option ---//
            test.validateElementIsPresent(driver, Elements.MainNavigation_MyAnalogTab);

            //--- Expected Result: myanalog logo (wave) is not displayed in myAnalog tab label when account is not login ---//
            test.validateElementIsNotPresent(driver, Elements.MyAnalog_MegaMenu_Tab_Wave_Icon);

            //--- Action: Click myAnalog tab ---//
            action.IExpandMyAnalogMegaMenu(driver);

            ////----- REGISTER NOW -----//

            ////----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R1 > T2: Verify display when clicking myAnalog menu when account is not logged in -----//

            ////--- Expected Result: The “Register now” header is available ---//
            //test.validateElementIsPresent(driver, Elements.MyAnalog_MegaMenu_RegisterNow_Header_Text);

            ////--- Expected Result: The Welcome copy is available ---//
            //test.validateElementIsPresent(driver, Elements.MyAnalog_MegaMenu_RegisterNow_Description_Text);

            ////--- Expected Result: The Get Started button is available ---//
            //test.validateElementIsPresent(driver, Elements.MyAnalog_MegaMenu_GetStarted_Button);

            ////--- Expected Result: The Video is available ---//
            //test.validateElementIsPresent(driver, Elements.MyAnalog_MegaMenu_Video);

            //if (util.CheckElement(driver, Elements.MyAnalog_MegaMenu_ExploreMyAnalogFeaturesNow_Link, 2))
            //{
            //    //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R1 > T2.1: Verify SSO mega menu (AL-14069/IQ-6648) -----//

            //    //--- Action: Click Explore myAnalog features now link ---//
            //    action.IOpenLinkInNewTab(driver, Elements.MyAnalog_MegaMenu_ExploreMyAnalogFeaturesNow_Link);
            //    Thread.Sleep(2000);

            //    //--- Expected Result: Page will redirect to: New CTA to go to the myAnalog Features page ---//
            //    test.validateStringInstance(driver, driver.Url, newMyAnalogExperience_page_url);

            //    driver.Close();
            //    driver.SwitchTo().Window(driver.WindowHandles.Last());
            //}

            ////----- LOGIN -----//

            ////----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R1 > T2: Verify display when clicking myAnalog menu when account is not logged in -----//

            ////--- Expected Result: The Login” header is available ---//
            //test.validateElementIsPresent(driver, Elements.MyAnalog_MegaMenu_Login_Header_Text);

            ////----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R1 > T2.1: Verify SSO mega menu (AL-14069/IQ-6648) -----//

            ////--- Expected Result: The login description is available ---//
            //test.validateElementIsPresent(driver, Elements.MyAnalog_MegaMenu_Login_Description_Text);

            ////----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R1 > T2: Verify display when clicking myAnalog menu when account is not logged in -----//

            ////--- Expected Result: Line: “Don’t have an account?" is available ---//
            //test.validateElementIsPresent(driver, Elements.MyAnalog_MegaMenu_DontHaveAnAccount_Text);

            ////----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R1 > T2.1: Verify SSO mega menu (AL-14069/IQ-6648) -----//

            ////--- Expected Result: The Register now link is available ---//
            //test.validateElementIsPresent(driver, Elements.MyAnalog_MegaMenu_RegisterNow_Link);

            //if (util.CheckElement(driver, Elements.MyAnalog_MegaMenu_RegisterNow_Link, 2))
            //{
            //    //--- Action: Click Register now link ---//
            //    action.IOpenLinkInNewTab(driver, Elements.MyAnalog_MegaMenu_RegisterNow_Link);
            //    Thread.Sleep(1000);

            //    //--- Expected Result: Page will redirect to Registering is easy. Start by selecting your product categories page ---//
            //    test.validateStringInstance(driver, driver.Url, registration_page_url);

            //    driver.Close();
            //    driver.SwitchTo().Window(driver.WindowHandles.Last());
            //}

            ////--- Expected Result: The Login to myanalog button is available ---//
            //test.validateElementIsPresent(driver, Elements.MyAnalog_MegaMenu_LogInToMyAnalog_Button);

            //if (util.CheckElement(driver, Elements.MyAnalog_MegaMenu_LogInToMyAnalog_Button, 2))
            //{
            //    //--- Action: Click Login to myanalog button ---//
            //    action.IClick(driver, Elements.MyAnalog_MegaMenu_LogInToMyAnalog_Button);

            //    //--- Expected Result: Page will redirect to Login Page ---//
            //    test.validateStringInstance(driver, driver.Url, login_page_url);
            //}

            //---------- LOGGED IN ----------//

            //--- Action: Log in valid account in login page ---//
            action.ILogin(driver, username, password);

            //----- MYANALOG TAB -----//

            //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R2 > T1: Verify myPage Landing Page -----//

            //--- Expected Result: myAnalog logo (wave) is displayed in myAnalog tab label ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_MegaMenu_Tab_Wave_Icon);

            //----- MYANALOG MEGA MENU -----//

            //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R1 > T10: Verify Navigation to myPage from Forms page -----//

            //--- Action: Click again myAnalog tab ---//
            //action.IExpandMyAnalogMegaMenu(driver);

            //--- Expected Result: mypage is displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_MegaMenu_Tab_Content);
        }

        //[Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        //[TestCase("en", TestName = "Verify that the MYANALOG Mega Menu Content are Present in an Applications Page in EN Locale")]
        //[TestCase("zh", TestName = "Verify that the MYANALOG Mega Menu Content are Present in an Applications Page in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the MYANALOG Mega Menu Content are Present in an Applications Page in JP Locale")]
        //public void MyAnalog_MegaMenu_LoggedInState_VerifyInApplicationsPage(string Locale)
        //{
        //    Console.WriteLine("Steps to Test:");

        //    //--- myAnalog Dashboard URL ---//
        //    string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_dashboard_url;

        //    action.Navigate(driver, myAnalog_url);

        //    //--- Action: Log in valid account in login page ---//
        //    action.ILogin(driver, username, password);

        //    //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R1 > T9: Verify Navigation to myPage from Application Page -----//

        //    //--- Action: Open Application Page Page ---//
        //    string page_url = null;
        //    if (Locale.Equals("cn") || Locale.Equals("zh"))
        //    {
        //        page_url = Configuration.Env_Url + "cn" + applications_page_url;
        //    }
        //    else
        //    {
        //        page_url = Configuration.Env_Url + Locale + applications_page_url;
        //    }
        //    driver.Navigate().GoToUrl(page_url);
        //    Thread.Sleep(1000);
        //    Console.WriteLine("Go to an Applications Page: " + page_url);

        //    //--- Action: Click again myAnalog tab ---//
        //    action.IExpandMyAnalogMegaMenu(driver);

        //    //--- Expected Result: mypage is displayed ---//
        //    test.validateElementIsPresent(driver, Elements.MyAnalog_MegaMenu_Tab_Content);
        //}

        //[Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        //[TestCase("en", TestName = "Verify that the MYANALOG Mega Menu Content are Present in a Design Center Page in EN Locale")]
        //[TestCase("zh", TestName = "Verify that the MYANALOG Mega Menu Content are Present in a Design Center Page in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the MYANALOG Mega Menu Content are Present in a Design Center Page in JP Locale")]
        //public void MyAnalog_MegaMenu_LoggedInState_VerifyInDesignCenterPage(string Locale)
        //{
        //    Console.WriteLine("Steps to Test:");

        //    //--- myAnalog Dashboard URL ---//
        //    string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_dashboard_url;

        //    action.Navigate(driver, myAnalog_url);

        //    //--- Action: Log in valid account in login page ---//
        //    action.ILogin(driver, username, password);

        //    //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R1 > T11: Verify Navigation to myPage from Eval pages -----//

        //    //--- Action: Open EVAL page ---//
        //    string page_url = null;
        //    if (Locale.Equals("cn") || Locale.Equals("zh"))
        //    {
        //        page_url = Configuration.Env_Url + "cn" + evalBoard_page_url;
        //    }
        //    else
        //    {
        //        page_url = Configuration.Env_Url + Locale + evalBoard_page_url;
        //    }
        //    driver.Navigate().GoToUrl(page_url);
        //    Thread.Sleep(1000);
        //    Console.WriteLine("Go to a Design Center Page: " + page_url);

        //    //--- Action: Click again myAnalog tab ---//
        //    action.IExpandMyAnalogMegaMenu(driver);
        //    Thread.Sleep(1000);

        //    //--- Expected Result: mypage is displayed ---//
        //    test.validateElementIsPresent(driver, Elements.MyAnalog_MegaMenu_Tab_Content);
        //}

        //[Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        //[TestCase("en", TestName = "Verify that the MYANALOG Mega Menu Content are Present in an Education Page in EN Locale")]
        //[TestCase("zh", TestName = "Verify that the MYANALOG Mega Menu Content are Present in an Education Page in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the MYANALOG Mega Menu Content are Present in an Education Page in JP Locale")]
        //public void MyAnalog_MegaMenu_LoggedInState_VerifyInEducationPage(string Locale)
        //{
        //    Console.WriteLine("Steps to Test:");

        //    //--- myAnalog Dashboard URL ---//
        //    string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_dashboard_url;

        //    action.Navigate(driver, myAnalog_url);

        //    //--- Action: Log in valid account in login page ---//
        //    action.ILogin(driver, username, password);

        //    //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R1 > T16: Verify Navigation to myPage from Education Page -----//

        //    //--- Action: Open education Page ---//
        //    string page_url = null;
        //    if (Locale.Equals("cn") || Locale.Equals("zh"))
        //    {
        //        page_url = Configuration.Env_Url + "cn" + education_page_url;
        //    }
        //    else
        //    {
        //        page_url = Configuration.Env_Url + Locale + education_page_url;
        //    }
        //    driver.Navigate().GoToUrl(page_url);
        //    Thread.Sleep(1000);
        //    Console.WriteLine("Go to an Education Page: " + page_url);

        //    //--- Action: Click again myAnalog tab ---//
        //    action.IExpandMyAnalogMegaMenu(driver);

        //    //--- Expected Result: mypage is displayed ---//
        //    test.validateElementIsPresent(driver, Elements.MyAnalog_MegaMenu_Tab_Content);
        //}

        //[Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        //[TestCase("en", TestName = "Verify that the MYANALOG Mega Menu Content are Present in a Sitemap Page in EN Locale")]
        //[TestCase("zh", TestName = "Verify that the MYANALOG Mega Menu Content are Present in a Sitemap Page in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the MYANALOG Mega Menu Content are Present in a Sitemap Page in JP Locale")]
        //public void MyAnalog_MegaMenu_LoggedInState_VerifyInSitemapPage(string Locale)
        //{
        //    Console.WriteLine("Steps to Test:");

        //    //--- myAnalog Dashboard URL ---//
        //    string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_dashboard_url;

        //    action.Navigate(driver, myAnalog_url);

        //    //--- Action: Log in valid account in login page ---//
        //    action.ILogin(driver, username, password);

        //    //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R1 > T18: Verify Navigation to myPage from Sitemap Page -----//

        //    //--- Action: Open sitemap ---//
        //    string page_url = null;
        //    if (Locale.Equals("cn") || Locale.Equals("zh"))
        //    {
        //        page_url = Configuration.Env_Url + "cn" + sitemap_page_url;
        //    }
        //    else
        //    {
        //        page_url = Configuration.Env_Url + Locale + sitemap_page_url;
        //    }
        //    driver.Navigate().GoToUrl(page_url);
        //    Console.WriteLine("Go to a Sitemap Page: " + page_url);

        //    //--- Action: Click again myAnalog tab ---//
        //    action.IExpandMyAnalogMegaMenu(driver);
        //    Thread.Sleep(1000);

        //    //--- Expected Result: mypage is displayed ---//
        //    test.validateElementIsPresent(driver, Elements.MyAnalog_MegaMenu_Tab_Content);
        //}
    }
}