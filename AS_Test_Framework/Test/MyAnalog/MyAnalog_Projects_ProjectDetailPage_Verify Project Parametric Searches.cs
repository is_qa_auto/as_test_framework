﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System;
using System.Threading;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_Projects_ProjectDetailPage_ProjectParametricSearches : BaseSetUp
    {
        //--- myAnalog Projects URL: ---//
        //--- DEV: https://my-dev.corpnt.analog.com/en/app/projects ---//
        //--- QA: https://my-qa.corpnt.analog.com/en/app/projects ---//
        //--- PROD: https://my.analog.com/en/app/projects ---//

        public MyAnalog_Projects_ProjectDetailPage_ProjectParametricSearches() : base() { }

        //----- Note: Please don't edit/delete any details on the Accounts below -----//

        //--- Login Credentials (This Account must have more than 10 Saved Project Parametric Searches) ---// 
        string username = "my_t3st_pr0j3ct_d3t41ls_555@mailinator.com";
        string password = "Test_1234";

        //--- Login Credentials (Used for Editing Saved Parametric Searches) ---// 
        string username_editSavedParametricSearches = "my_t3st_3d1t_pr0j3ct_d3t41ls_555@mailinator.com";
        string password_editSavedParametricSearches = "Test_1234";

        //--- URLs ---//
        string myAnalog_projects_url = "app/projects";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that Project Parametric Searches is Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that Project Parametric Searches is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that Project Parametric Searches is Present and Working as Expected in JP Locale")]
        public void MyAnalog_Projects_ProjectDetailPage_VerifyProjectParametricSearches(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Projects URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_projects_url;

            //--- Action: Open myAnalog Projects using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);

            //--- Action: Click Project Detail button ---//
            action.IClick(driver, Elements.MyAnalog_Projects_ProjectDetails_Buttons);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Navigate to Project Details Page -----//

            //--- Expected Result: The Project Parametric Search section component is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_ProjectDetailPage_ProjectParametricSearches_Section);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify Project Parametric searches (IQ-6325/AL-13534) -----//

            //--- Expected Result: The Project Parametric title is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_ProjectDetailPage_ProjectParametricSearches_Label);

            //--- Expected Result: The Edit option is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_ProjectDetailPage_ProjectParametricSearches_Edit_Button);
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that Delete Saved Parametric Searches is Working as Expected in EN Locale")]
        public void MyAnalog_Projects_ProjectDetailPage_ProjectParametricSearches_VerifyDeleteSavedParametricSearches(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Projects URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_projects_url;

            //--- Action: Open myAnalog Projects using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username_editSavedParametricSearches, password_editSavedParametricSearches);

            //--- Action: Click Project Detail button ---//
            action.IClick(driver, Elements.MyAnalog_Projects_ProjectDetails_Buttons);

            //--- Action: Save Project Parametric Searches in myAnalog if there's none ---//
            if (!util.CheckElement(driver, Elements.MyAnalog_ProjectDetailPage_SavedParametricSearches, 2))
            {
                string projectDetailPage_url = driver.Url;

                action.ISaveProjectParametricSearchesInMyAnalog(driver, Locale, "/parametricsearch/11470");

                //--- Action: Go Back to the Project Detail Page ---//
                driver.Navigate().GoToUrl(projectDetailPage_url);
                Console.WriteLine("\tGo back to: " + projectDetailPage_url);
            }

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify when editing Parametric Searches -----//
            string first_savedParametricSearches_label = util.GetText(driver, Elements.MyAnalog_ProjectDetailPage_First_SavedParametricSearch_Link);

            //--- Action: Click edit option ---//
            action.IClick(driver, Elements.MyAnalog_ProjectDetailPage_ProjectParametricSearches_Edit_Button);

            //--- Expected Result: File is editable ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_ProjectDetailPage_ProjectParametricSearches_Section_EditMode);

            //--- Expected Result: Save is displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_ProjectDetailPage_ProjectParametricSearches_Save_Button);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify if the "X" close button over lap with save button when editing Parametric Searches (IQ-6957/ALA-12321) -----//

            //--- Expected Result: The "x" close button is Present ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_ProjectDetailPage_SavedParametricSearches_X_Icon);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify when editing Parametric Searches -----//

            //--- Action: Edit file ---//
            action.IClick(driver, Elements.MyAnalog_ProjectDetailPage_SavedParametricSearches_X_Icon);

            //--- Action: Click Save ---//
            action.IClick(driver, Elements.MyAnalog_ProjectDetailPage_ProjectParametricSearches_Save_Button);
            Thread.Sleep(3000);

            //--- Expected Result: Changes will save successfully ---//
            test.validateStringChanged(driver, util.GetText(driver, Elements.MyAnalog_ProjectDetailPage_First_SavedParametricSearch_Link), first_savedParametricSearches_label);
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Cancel Changes is Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Cancel Changes is Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Cancel Changes is Working as Expected in JP Locale")]
        public void MyAnalog_Projects_ProjectDetailPage_ProjectParametricSearches_VerifyCancelChanges(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Projects URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_projects_url;

            //--- Action: Open myAnalog Projects using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);

            //--- Action: Click Project Detail button ---//
            action.IClick(driver, Elements.MyAnalog_Projects_ProjectDetails_Buttons);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify when editing Parametric Searches -----//

            //--- Action: Click edit option ---//
            action.IClick(driver, Elements.MyAnalog_ProjectDetailPage_ProjectParametricSearches_Edit_Button);

            //--- Expected Result: Cancel is displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_ProjectDetailPage_ProjectParametricSearches_Cancel_Button);

            //--- Action: Click cancel ---//
            action.IClick(driver, Elements.MyAnalog_ProjectDetailPage_ProjectParametricSearches_Cancel_Button);

            //--- Expected Result: Changes will be cancelled ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_ProjectDetailPage_ProjectParametricSearches_Section);
        }
    }
}