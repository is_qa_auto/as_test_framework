﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_MegaMenu_ParametricSearchesTile : BaseSetUp
    {
        public MyAnalog_MegaMenu_ParametricSearchesTile() : base() { }

        //-- Login Credentials --//
        string username = "myanalog_t3st_pst_555@mailinator.com";
        string password = "Test_1234";

        //--- myAnalog Parametric Searches URL: ---//
        //--- DEV: https://my-dev.corpnt.analog.com/en/app/parametric-searches ---//
        //--- QA: https://my-qa.corpnt.analog.com/en/app/parametric-searches ---//
        //--- PROD: https://my.analog.com/en/app/parametric-searches ---//

        //--- URLs ---//
        string myAnalog_parametricSearches_url = "app/parametric-searches";
        string pst_url_format = "/parametricsearch/";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Parametric Searches Tile is Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Parametric Searches Tile is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Parametric Searches Tile is Present and Working as Expected in JP Locale")]
        public void MyAnalog_MegaMenu_VerifyParametricSearchesTile(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Parametric Searches URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_parametricSearches_url;

            action.Navigate(driver, myAnalog_url);

            //--- Action: Log in valid account in login page ---//
            action.ILogin(driver, username, password);

            //--- Action: click myanalog tab ---//
            action.IExpandMyAnalogMegaMenu(driver);
            Thread.Sleep(1000);

            //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R2 > T2: Verify myPage Components -----//

            //--- Expected Result: The Parametric Search tile is displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_MegaMenu_ParametricSearches_Tile);

            //----- PARAMETRIC SEARCHES TITLE -----//

            //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R6 > T1: Verify Parametric Searches component -----//

            //--- Expected Result: The Parametric Searches title component is displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_MegaMenu_ParametricSearches_Link);

            if (util.CheckElement(driver, Elements.MyAnalog_MegaMenu_ParametricSearches_Link, 2))
            {
                string parametricSearches_link = util.GetText(driver, Elements.MyAnalog_MegaMenu_ParametricSearches_Link);

                //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R6 > T2: Verify when clicking Parametric Searches title link -----//
                string current_url = driver.Url;

                //--- Action: Click Parametric search link ---//
                action.IClick(driver, Elements.MyAnalog_MegaMenu_ParametricSearches_Link);

                if (driver.Url.Contains(current_url))
                {
                    driver.SwitchTo().Window(driver.WindowHandles.Last());
                }

                //--- Expected Result: Link will redirect to dashboard parametric seach page ---//
                test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + "/" + myAnalog_parametricSearches_url);
            }

            //----- SEE ALL YOUR PARAMETRIC SEARCHES -----//

            //--- Action: Click myAnalog tab ---//
            action.IExpandMyAnalogMegaMenu(driver);

            //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R6 > T1: Verify Parametric Searches component -----//

            //--- Expected Result: The See all your parametric searches link component is displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_MegaMenu_SeeAllYourParametricSearches_Link);

            if (util.CheckElement(driver, Elements.MyAnalog_MegaMenu_SeeAllYourParametricSearches_Link, 2))
            {
                //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R6 > T4: Verify when clicking See all your Parametric Searches link -----//
                string current_url = driver.Url;

                //--- Action: Click See all your parametric search link ---//
                action.IClick(driver, Elements.MyAnalog_MegaMenu_SeeAllYourParametricSearches_Link);

                if (driver.Url.Contains(current_url))
                {
                    driver.SwitchTo().Window(driver.WindowHandles.Last());
                }

                //--- Expected Result: Link will redirect to dashboard parametric seach page ---//
                test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + "/" + myAnalog_parametricSearches_url);
            }

            //----- SAVED PSTS -----//

            //--- Action: Click myAnalog tab ---//
            action.IExpandMyAnalogMegaMenu(driver);

            //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R6 > T1: Verify Parametric Searches component -----//

            //--- Expected Result: The Parametric Searches links component is displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_MegaMenu_Saved_Pst_Title_Links);

            if (util.CheckElement(driver, Elements.MyAnalog_MegaMenu_Saved_Pst_Title_Links, 2))
            {
                //--- Expected Result: 10 most recent items ---//
                test.validateCountIsLessOrEqual(driver, 10, util.GetCount(driver, Elements.MyAnalog_MegaMenu_Saved_Pst_Title_Links));

                //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R6 > T3: Verify when clicking Parametric Searches  link -----//
                string current_url = driver.Url;

                //--- Action: Click saved parametric search link ---//
                action.IClick(driver, Elements.MyAnalog_MegaMenu_Saved_Pst_Title_Links);
                Thread.Sleep(1000);

                if (driver.Url.Contains(current_url))
                {
                    driver.SwitchTo().Window(driver.WindowHandles.Last());
                }

                //--- Expected Result: Link will redirect to parametric search page ---//
                test.validateStringInstance(driver, driver.Url, pst_url_format);
            }
        }
    }
}