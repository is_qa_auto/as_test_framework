﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_Projects_ProjectDetailPage_ProjectDetails : BaseSetUp
    {
        //--- myAnalog Projects URL: ---//
        //--- DEV: https://my-dev.corpnt.analog.com/en/app/projects ---//
        //--- QA: https://my-qa.corpnt.analog.com/en/app/projects ---//
        //--- PROD: https://my.analog.com/en/app/projects ---//

        public MyAnalog_Projects_ProjectDetailPage_ProjectDetails() : base() { }

        //----- Note: Please don't edit/delete any details on the Accounts below -----//

        //--- Login Credentials (Account with Projects that has More than 5 Contributors) ---//
        string username = "my_t3st_pr0j3ct_1nv1t3_555@mailinator.com";
        string password = "Test_1234";

        //--- URLs ---//
        string myAnalog_projects_url = "app/projects";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Project Details are Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Project Details are Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Project Details are Present and Working as Expected in JP Locale")]
        public void MyAnalog_Projects_ProjectDetailPage_VerifyProjectDetails(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Projects URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_projects_url;

            //--- Action: Open myAnalog Projects using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);

            //--- Action: Click Project Detail button ---//
            action.IClick(driver, Elements.MyAnalog_Projects_ProjectDetails_Buttons);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify project title section -----//

            //--- Expected Result: The Name who created the Project should be available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_ProjectDetailPage_AddedBy_Name_Label);

            //--- Expected Result: The Date and time of Project was created should be available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_ProjectDetailPage_AddedBy_DateAndTime_Label);

            if (Locale.Equals("en"))
            {
                //----- Project & Project Detail Page Test Plan > Test Case Title: Verify "Modified On" should be changed to "Modified" (IQ-9372/AL-16076) -----//

                //--- Expected Result: Changed to Modified ---//
                test.validateStringInstance(driver, util.GetText(driver, Elements.MyAnalog_ProjectDetailPage_ModifiedBy_Name_Label), "Modified");
            }
            else
            {
                //----- Project & Project Detail Page Test Plan > Test Case Title: Verify project title section -----//

                //--- Expected Result: The Name who modify the Project  should be available ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_ProjectDetailPage_ModifiedBy_Name_Label);
            }

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify project title section -----//

            //--- Expected Result: The Date and time of Project was modified should be available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_ProjectDetailPage_ModifiedBy_DateAndTime_Label);
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Contributors are Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Contributors are Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Contributors are Present and Working as Expected in JP Locale")]
        public void MyAnalog_Projects_ProjectDetailPage_ProjectDetails_VerifyContributor(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Projects URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_projects_url;

            //--- Action: Open myAnalog Projects using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);

            //--- Action: Click Project Detail button ---//
            action.IClick(driver, Elements.MyAnalog_Projects_ProjectDetails_Buttons);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify list of contributors -----//

            //--- Expected Result: List of contributors are found ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_ProjectDetailPage_Participants_Dropdown);

            //--- Action: Get the Participants Count ---//
            int participants_count = Int32.Parse(util.ExtractNumber(driver, Elements.MyAnalog_ProjectDetailPage_Participants_Count_Label));

            //--- Action: Click the dropdown ---//
            action.IClick(driver, Elements.MyAnalog_ProjectDetailPage_Participants_Dropdown);

            //--- Expected Result: Names of contributors are availale in the list ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_ProjectDetailPage_Participants_Dropdown_Options);

            ////--- Action: Get the Participants Dropdown Options Count ---//
            //int participantsDropdownOptions_count = util.GetCount(driver, Elements.MyAnalog_ProjectDetailPage_Participants_Dropdown_Options);
            //if (util.CheckElement(driver, Elements.MyAnalog_ProjectDetailPage_Participants_Dropdown_Owner_Label, 1))
            //{
            //    participantsDropdownOptions_count--;
            //}

            ////--- Expected Result: Count of contributors are correct to the number of available contributors in the list ---// -> PENDING: When there's more than 5 Contributors, the Count doesn't Match.. Manual Testing Team to confirm if this is still valid.
            //test.validateCountIsEqual(driver, participants_count, participantsDropdownOptions_count);
        }
    }
}