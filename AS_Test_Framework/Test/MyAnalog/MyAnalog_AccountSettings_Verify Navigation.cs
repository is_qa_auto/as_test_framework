﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using System;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_AccountSettings_Navigation : BaseSetUp
    {
        //--- myAnalog Account Settings URL: ---//
        //--- DEV: https://my-dev.corpnt.analog.com/en/app/account ---//
        //--- QA: https://my-qa.corpnt.analog.com/en/app/account ---//
        //--- PROD: https://my.analog.com/en/app/account ---//

        public MyAnalog_AccountSettings_Navigation() : base() { }

        //--- Login Credentials ---//
        string username = null;
        string password = null;

        //--- URLs ---//
        string myAnalog_accountSettings_url = "app/account";
        string login_page_url = "b2clogin";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Navigation is Working as Expected when the User is Logged Out in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Navigation is Working as Expected when the User is Logged Out in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Navigation is Working as Expected when the User is Logged Out in JP Locale")]
        public void MyAnalog_AccountSettings_Navigation_VerifyWhenUserIsLoggedOut(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Account Settings URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_accountSettings_url;

            //----- Account Settings Test Plan > myAnalog Dashboard Tab > R1 > T2: Navigate to Account Settings page using direct link when account is not logged in -----//

            //--- Action: Open URL direct to browser ---//
            action.Navigate(driver, myAnalog_url);

            //--- Expected Result: Page will redirect to login page ---//
            test.validateStringInstance(driver, driver.Url, login_page_url);
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Navigation is Working as Expected when the User is Logged In in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Navigation is Working as Expected when the User is Logged In in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Navigation is Working as Expected when the User is Logged In in JP Locale")]
        public void MyAnalog_AccountSettings_Navigation_VerifyWhenUserIsLoggedIn(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- Login Credentials ---//       
            if (Util.Configuration.Environment.Equals("dev"))
            {
                username = "myanalog_t3st_555@mailinator.com";
            }
            else
            {
                username = "marvin.bebe@analog.com";
            }
            password = "Test_1234";

            //--- myAnalog Account Settings URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_accountSettings_url;

            //----- Account Settings Test Plan > myAnalog Dashboard Tab > R1 > T3: Navigate to Account Settings page using direct link when account is logged in -----//

            //--- Action: Open URL direct to browser ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid username and password ---//
            action.ILogin(driver, username, password);

            //--- Expected Result: Account setting page is displayed ---//
            test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + "/" + myAnalog_accountSettings_url);
        }
    }
}