﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_ParametricSearches_Pagination : BaseSetUp
    {
        //--- myAnalog Parametric Searches URL: ---//
        //--- DEV: https://my-dev.corpnt.analog.com/en/app/parametric-searches ---//
        //--- QA: https://my-qa.corpnt.analog.com/en/app/parametric-searches ---//
        //--- PROD: https://my.analog.com/en/app/parametric-searches ---//

        public MyAnalog_ParametricSearches_Pagination() : base() { }

        //--- Login Credentials ---//
        string username = "myanalog_t3st_pst_555@mailinator.com";
        string password = "Test_1234";

        //--- URLs ---//
        string myAnalog_parametricSearches_url = "app/parametric-searches";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Previous Button and Next Button are Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Previous Button and Next Button are Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Previous Button and Next Button are Present and Working as Expected in JP Locale")]
        public void MyAnalog_ParametricSearches_Pagination_VerifyPreviousButtonAndNextButton(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Parametric Searches URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_parametricSearches_url;

            //--- Action: Open parametric search using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);

            //----- Parametric Searches Test Plan > myAnalog Dashboard Tab > R3 > T1: Pagination (IQ-6365/AL-13329) -----//

            //--- Expected Result: The first few(10) page links are shown ---//
            test.validateCountIsEqual(driver, 10, util.GetCount(driver, Elements.MyAnalog_ParametricSearches_Saved_ParametricSearches_Rows));

            //----- NEXT BUTTON -----//

            //----- Parametric Searches Test Plan > myAnalog Dashboard Tab > R3 > T1: Pagination (IQ-6365/AL-13329) -----//

            //--- Expected Result: There's a button for Next page ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_ParametricSearches_Next_Button);

            //--- Action: Get the Current Page Selected ---//
            int currentPage_selected = Int32.Parse(util.GetText(driver, Elements.MyAnalog_ParametricSearches_Current_Page_Selected));

            //--- Action: Click the Next button ---//
            action.IClick(driver, Elements.MyAnalog_ParametricSearches_Next_Button);

            //--- Expected Result: The system will show the next page upon clicking the Next button ---//
            test.validateStringIsCorrect(driver, Elements.MyAnalog_ParametricSearches_Current_Page_Selected, (currentPage_selected + 1).ToString());

            //----- PREVIOUS BUTTON -----//

            //--- Expected Result: There's a button for Previous page ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_ParametricSearches_Previous_Button);

            //--- Action: Get the Current Page Selected ---//
            currentPage_selected = Int32.Parse(util.GetText(driver, Elements.MyAnalog_ParametricSearches_Current_Page_Selected));

            //--- Action: Click the Previous button ---//
            action.IClick(driver, Elements.MyAnalog_ParametricSearches_Previous_Button);

            //--- Expected Result: The system will show the previous page upon clicking the Previous button ---//
            test.validateStringIsCorrect(driver, Elements.MyAnalog_ParametricSearches_Current_Page_Selected, (currentPage_selected - 1).ToString());
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Last Page Button is Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Last Page Button is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Last Page Button is Present and Working as Expected in JP Locale")]
        public void MyAnalog_ParametricSearches_Pagination_VerifyLastPageButton(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Parametric Searches URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_parametricSearches_url;

            //--- Action: Open parametric search using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);

            //----- Parametric Searches Test Plan > myAnalog Dashboard Tab > R3 > T1: Pagination (IQ-6365/AL-13329) -----//

            //--- Expected Result: The last page link is shown ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_ParametricSearches_Last_Page_Button);

            string lastPage_text = util.GetText(driver, Elements.MyAnalog_ParametricSearches_Last_Page_Button);

            //--- Action: Click last page link ---//
            action.IClick(driver, Elements.MyAnalog_ParametricSearches_Last_Page_Button);

            //--- Expected Result: The system will show the last page upon clicking last page (IQ-7386/ALA-12142) ---//
            test.validateStringIsCorrect(driver, Elements.MyAnalog_ParametricSearches_Current_Page_Selected, lastPage_text);
        }
    }
}