﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System;
using System.Threading;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_ParametricSearches_Elements : BaseSetUp
    {
        //--- myAnalog Parametric Searches URL: ---//
        //--- DEV: https://my-dev.corpnt.analog.com/en/app/parametric-searches ---//
        //--- QA: https://my-qa.corpnt.analog.com/en/app/parametric-searches ---//
        //--- PROD: https://my.analog.com/en/app/parametric-searches ---//

        public MyAnalog_ParametricSearches_Elements() : base() { }

        //--- Login Credentials ---//
        string username = "myanalog_t3st_pst_555@mailinator.com";
        string password = "Test_1234";

        //--- URLs ---//
        string myAnalog_parametricSearches_url = "app/parametric-searches";
        string pst_url_format = "/parametricsearch/";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Elements are Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Elements are Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Elements are Present and Working as Expected in JP Locale")]
        public void MyAnalog_ParametricSearches_VerifyElements(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Parametric Searches URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_parametricSearches_url;

            //--- Action: Open parametric search using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);
            Thread.Sleep(1000);

            //----- Parametric Searches Test Plan > myAnalog Dashboard Tab > R2 > T1: Verify landing page display of Parametric Searches -----//

            //--- Expected Result: The Parametric Search title is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_ParametricSearchess_Label);

            //--- Expected Result: The Description is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_ParametricSearches_Description_Text);

            //----- Parametric Searches Test Plan > myAnalog Dashboard Tab > R1 > T4: Verify Parametric Searches Display -----//

            //--- Expected Result: The Date added column is present ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_ParametricSearches_Saved_ParametricSearches_DateAdded_Value);

            //--- Expected Result: The Ellipsis button column is present ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_ParametricSearches_Saved_ParametricSearches_Ellipsis_Button);

            //--- Expected Result: The Title column is present ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_ParametricSearches_Saved_ParametricSearches_Title_Value);

            if (util.CheckElement(driver, Elements.MyAnalog_ParametricSearches_Saved_ParametricSearches_Title_Value, 1))
            {
                //----- Parametric Searches Test Plan > myAnalog Dashboard Tab > R4 > T1: Verify redirection of link when parametric search is saved WITH edited title -----//
                string pst_title_link = util.GetText(driver, Elements.MyAnalog_ParametricSearches_Saved_ParametricSearches_Title_Value);

                //--- Action: Click parametric search link ---//
                action.IOpenLinkInNewTab(driver, Elements.MyAnalog_ParametricSearches_Saved_ParametricSearches_Title_Value);
                Thread.Sleep(2000);

                //--- Expected Result: Page redirect to PST page ---//
                test.validateStringInstance(driver, driver.Url, pst_url_format);
            }
        }
    }
}