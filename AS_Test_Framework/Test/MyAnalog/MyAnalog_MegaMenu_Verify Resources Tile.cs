﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_MegaMenu_ResourcesTile : BaseSetUp
    {
        public MyAnalog_MegaMenu_ResourcesTile() : base() { }

        //-- Login Credentials --//
        string username = "myanalog_t3st_r3s0urc3s_555@mailinator.com";
        string password = "Test_1234";

        //--- myAnalog Resources URL: ---//
        //--- DEV: https://my-dev.corpnt.analog.com/en/app/resources ---//
        //--- QA: https://my-qa.corpnt.analog.com/en/app/resources ---//
        //--- PROD: https://my.analog.com/en/app/resources ---//

        //--- URLs ---//
        string myAnalog_resources_url = "app/resources";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Resources Tile is Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Resources Tile is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Resources Tile is Present and Working as Expected in JP Locale")]
        public void MyAnalog_MegaMenu_VerifyResourcesTile(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Resources URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_resources_url;

            action.Navigate(driver, myAnalog_url);

            //--- Action: Log in valid account in login page ---//
            action.ILogin(driver, username, password);

            //--- Action: click myanalog tab ---//
            action.IExpandMyAnalogMegaMenu(driver);
            Thread.Sleep(1000);

            //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R2 > T2: Verify myPage Components -----//

            //--- Expected Result: The Resouces tile is displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_MegaMenu_Resources_Tile);

            //----- RESOURCES TITLE -----//

            //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R5 > T1: Verify Resources component -----//

            //--- Expected Result: The Resource title is displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_MegaMenu_Resources_Link);

            if (util.CheckElement(driver, Elements.MyAnalog_MegaMenu_Resources_Link, 2))
            {
                string resources_link = util.GetText(driver, Elements.MyAnalog_MegaMenu_Resources_Link);

                //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R5 > T2: Verify when clicking Resources title link -----//
                string current_url = driver.Url;

                //--- Action: Click Resources title link ---//
                action.IClick(driver, Elements.MyAnalog_MegaMenu_Resources_Link);

                if (driver.Url.Contains(current_url))
                {
                    driver.SwitchTo().Window(driver.WindowHandles.Last());
                }

                //--- Expected Result: Page will redirect to Resources page ---//
                test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + "/" + myAnalog_resources_url);
            }

            //----- SEE ALL YOUR RESOURCES -----//

            //--- Action: Click myAnalog tab ---//
            action.IExpandMyAnalogMegaMenu(driver);

            //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R5 > T1: Verify Resources component -----//

            //--- Expected Result: The See all your resources link is displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_MegaMenu_SeeAllYourResources_Link);

            if (util.CheckElement(driver, Elements.MyAnalog_MegaMenu_SeeAllYourResources_Link, 2))
            {
                //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R5 > T4: Verify when clicking See all your resources link -----//
                string current_url = driver.Url;

                //--- Action: Click see all your resources link ---//
                action.IClick(driver, Elements.MyAnalog_MegaMenu_SeeAllYourResources_Link);

                if (driver.Url.Contains(current_url))
                {
                    driver.SwitchTo().Window(driver.WindowHandles.Last());
                }

                //--- Expected Result: Page will redirect to Resources page ---//
                test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + "/" + myAnalog_resources_url);
            }

            //----- SAVED RESOURCES -----//

            //--- Action: Click myAnalog tab ---//
            action.IExpandMyAnalogMegaMenu(driver);

            //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R5 > T1: Verify Resources component -----//

            //--- Expected Result: The Resources links is displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_MegaMenu_Saved_Resources_Title_Links);

            if (util.CheckElement(driver, Elements.MyAnalog_MegaMenu_Saved_Resources_Title_Links, 2))
            {
                //--- Expected Result: 10 most recent items ---//
                test.validateCountIsLessOrEqual(driver, 10, util.GetCount(driver, Elements.MyAnalog_MegaMenu_Saved_Resources_Title_Links));
            }
        }
    }
}