﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System;
using System.Threading;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_MegaMenu_EngineerZoneTile : BaseSetUp
    {
        public MyAnalog_MegaMenu_EngineerZoneTile() : base() { }

        //-- Login Credentials --//
        string username = "myanalog_t3st_555@mailinator.com";
        string password = "Test_1234";

        //--- myAnalog Dashboard URL: ---//
        //--- DEV: https://my-dev.corpnt.analog.com/en/app ---//
        //--- QA: https://my-qa.corpnt.analog.com/en/app ---//
        //--- PROD: https://my.analog.com/en/app ---//

        //--- URLs ---//
        string myAnalog_dashboard_url = "app";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the EngineerZone Tile is Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the EngineerZone Tile is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the EngineerZone Tile is Present and Working as Expected in JP Locale")]
        public void MyAnalog_MegaMenu_VerifyEngineerZoneTile(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog EngineerZone URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_dashboard_url;

            action.Navigate(driver, myAnalog_url);

            //--- Action: Log in valid account in login page ---//
            action.ILogin(driver, username, password);

            //--- Action: click myanalog tab ---//
            action.IExpandMyAnalogMegaMenu(driver);
            Thread.Sleep(1000);

            //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R2 > T2: Verify myPage Components -----//

            //--- Expected Result: The Engineerzone tile is displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_MegaMenu_EngineerZone_Tile);
        }
    }
}