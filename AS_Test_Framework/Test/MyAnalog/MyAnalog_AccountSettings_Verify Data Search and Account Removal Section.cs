﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System;
using System.Threading;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_AccountSettings_DataSearchAndAccountRemovalSection : BaseSetUp
    {
        //--- myAnalog Account Settings URL: ---//
        //--- DEV: https://my-dev.corpnt.analog.com/en/app/account ---//
        //--- QA: https://my-qa.corpnt.analog.com/en/app/account ---//
        //--- PROD: https://my.analog.com/en/app/account ---//

        public MyAnalog_AccountSettings_DataSearchAndAccountRemovalSection() : base() { }

        //--- Login Credentials ---//
        string username = null;
        string password = null;

        //--- URLs ---//
        string myAnalog_accountSettings_url = "app/account";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Elements are Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Elements are Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Elements are Present and Working as Expected in JP Locale")]
        public void MyAnalog_AccountSettings_VerifyDataSearchAndAccountRemovalSection(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- Login Credentials ---//
            if (Locale.Equals("en"))
            {
                username = "myanalog_t3st_999@mailinator.com";
            }
            else if (Locale.Equals("zh") || Locale.Equals("cn"))
            {
                username = "myanalog_t3st_cn_777@mailinator.com";
            }
            else
            {
                if (Util.Configuration.Environment.Equals("dev"))
                {
                    username = "test_jp_04252019@mailinator.com";
                }
                else
                {
                    username = "myanalog_t3st_jp_777@mailinator.com";
                }
            }
            password = "Test_1234";

            //--- myAnalog Account Settings URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_accountSettings_url;

            //--- Action: Open account settings page ---//
            action.Navigate(driver, myAnalog_url);

            action.ILogin(driver, username, password);
            Thread.Sleep(2000);

            //----- Account Settings Test Plan > myAnalog Dashboard Tab > R6 > T3: Verify click Here functionality -----//

            //--- Expected Result: "Data Search and Account Removal" should be displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_AccountSettings_DataSearchAndAccountRemovalSection_Section_Label);

            //--- Expected Result: "To find out what information has been collected in connection with your registration for myAnalog, first visit our Data Search page to have an email with this information sent to your email. If, subject to applicable law, you would like to have this information removed from our database and your myAnalog Account deleted, send an email to our Web Support team" should be displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_AccountSettings_DataSearchAndAccountRemovalSection_Section_Description_Text);
        }
    }
}