﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using System;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_Resources_Navigation : BaseSetUp
    {
        //--- myAnalog Resources URL: ---//
        //--- DEV: https://my-dev.corpnt.analog.com/en/app/resources ---//
        //--- QA: https://my-qa.corpnt.analog.com/en/app/resources ---//
        //--- PROD: https://my.analog.com/en/app/resources ---//

        public MyAnalog_Resources_Navigation() : base() { }

        //--- Login Credentials ---//
        string username = "myanalog_t3st_r3s0urc3s_555@mailinator.com";
        string password = "Test_1234";

        //--- URLs ---//
        string myAnalog_resources_url = "app/resources";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Navigation is Working as Expected when the User is Logged In in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Navigation is Working as Expected when the User is Logged In in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Navigation is Working as Expected when the User is Logged In in JP Locale")]
        public void MyAnalog_Resources_Navigation_VerifyWhenUserIsLoggedIn(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Resources URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_resources_url;

            //----- Resources Test Plan > Desktop Tab > R1 > T3: Verify navigation using direct link -----//

            //--- Action: Open resources direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);

            //--- Expected Result: Resources page is open ---//
            test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + "/" + myAnalog_resources_url);
        }
    }
}