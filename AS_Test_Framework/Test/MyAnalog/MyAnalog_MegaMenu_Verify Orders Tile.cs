﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_MegaMenu_OrdersTile : BaseSetUp
    {
        public MyAnalog_MegaMenu_OrdersTile() : base() { }

        //-- Login Credentials --//
        string username = "aries.sorosoro@analog.com";
        string password = "Test_1234";

        //--- myAnalog Orders URL: ---//
        //--- DEV: https://my-dev.corpnt.analog.com/en/app/orders ---//
        //--- QA: https://my-qa.corpnt.analog.com/en/app/orders ---//
        //--- PROD: https://my.analog.com/en/app/orders ---//

        //--- URLs ---//
        string myAnalog_orders_url = "app/orders";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Orders Tile is Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Orders Tile is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Orders Tile is Present and Working as Expected in JP Locale")]
        public void MyAnalog_MegaMenu_VerifyOrdersTile(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Orders URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_orders_url;

            action.Navigate(driver, myAnalog_url);

            //--- Action: Log in valid account in login page ---//
            action.ILogin(driver, username, password);

            //--- Action: Get the Number of the Latest Order ---//
            if (!driver.Url.Equals(myAnalog_url) && !driver.Url.Equals(myAnalog_url + "#"))
            {
                driver.Navigate().GoToUrl(myAnalog_url);
            }

            string latest_orderNumber = util.GetText(driver, Elements.MyAnalog_Orders_OrderNumber_Row_Link);

            //--- Action: click myanalog tab ---//
            action.IExpandMyAnalogMegaMenu(driver);
            Thread.Sleep(1000);

            //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R2 > T2: Verify myPage Components -----//

            //--- Expected Result: The Order tile is displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_MegaMenu_Orders_Tile);

            //----- ORDERS TITLE -----//

            //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R7 > T1: Verify Orders component -----//

            //--- Expected Result: The Oder title is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_MegaMenu_Orders_Link);

            if (util.CheckElement(driver, Elements.MyAnalog_MegaMenu_Orders_Link, 2))
            {
                //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R7 > T2: Verify when clicking Orders link -----//
                string current_url = driver.Url;

                //--- Action: Click Orders link ---//
                action.IClick(driver, Elements.MyAnalog_MegaMenu_Orders_Link);

                if (driver.Url.Contains(current_url))
                {
                    driver.SwitchTo().Window(driver.WindowHandles.Last());
                }

                //--- Expected Result: Order dashboard page will displayed ---//
                test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + "/" + myAnalog_orders_url);
            }

            //----- PLACED ORDERS -----//

            //--- Action: Click myAnalog tab ---//
            action.IExpandMyAnalogMegaMenu(driver);

            //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R7 > T2: Verify when clicking Orders link -----//

            //--- Expected Result: Order date is displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_MegaMenu_PlacedOrder_Dates);

            //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R7 > T4: Verify that the most recently created Order is shown first in the Orders tile (IQ-7706/AL-14030) -----//

            //--- Expected Result: The most recently created Order is shown first in the Orders tile ---//
            test.validateStringInstance(driver, util.GetText(driver, Elements.MyAnalog_MegaMenu_PlacedOrder_Number_Links), latest_orderNumber);

            if (util.CheckElement(driver, Elements.MyAnalog_MegaMenu_PlacedOrder_Number_Links, 2))
            {
                //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R7 > T1: Verify Orders component -----//

                //--- Expected Result: 10 most recent items ---//
                test.validateCountIsLessOrEqual(driver, 10, util.GetCount(driver, Elements.MyAnalog_MegaMenu_PlacedOrder_Number_Links));
            }

            //----- SEE ALL YOUR ORDERS -----//

            //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R7 > T1: Verify Orders component -----//

            //--- Expected Result: The  See all your orders link is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_MegaMenu_SeeAllYourOrders_Link);

            if (util.CheckElement(driver, Elements.MyAnalog_MegaMenu_SeeAllYourOrders_Link, 2))
            {
                //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R7 > T3: Verify when clicking See all your Orders link -----//

                //--- Action: Click See all your Orders link ---//
                action.IOpenLinkInNewTab(driver, Elements.MyAnalog_MegaMenu_SeeAllYourOrders_Link);

                //--- Expected Result: Page will redirect to Order page in myanalog dashboard ---//
                test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + "/" + myAnalog_orders_url);
            }
        }
    }
}