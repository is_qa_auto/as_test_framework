﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_SupportTickets_SupportTicketDetails : BaseSetUp
    {
        public MyAnalog_SupportTickets_SupportTicketDetails() : base() { }

        //--- Login Credentials ---//
        string username = "myanalog_t3st_555@mailinator.com";
        string password = "Test_1234";

        //--- URLs ---//
        string myAnalog_supportTickets_url = "app/support";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Elements are Present and Working as Expected in EN Locale")]
        public void MyAnalog_SupportTickets_VerifySupportTicketDetails(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Support Tickets URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_supportTickets_url;

            //--- Action: Open Support Tickets page direct link to browser ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Enter valid username and password and click login button ---//
            action.ILogin(driver, username, password);

            if (util.CheckElement(driver, Elements.MyAnalog_SupportTickets_First_Created_SupportTickets_CaseTitle, 1))
            {
                //--- Action: Click on a Support Ticket Case Title ---//
                action.IOpenLinkInNewTab(driver, Elements.MyAnalog_SupportTickets_First_Created_SupportTickets_CaseTitle);

                //----- TICKET META -----//

                //--- Expected Result: The Created On - Label is Present ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_SupportTicketsDetails_CreatedOn_Label);

                //--- Expected Result: The Created On - Value is Present ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_SupportTicketsDetails_CreatedOn_Value);

                //--- Expected Result: The Case number - Label is Present ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_SupportTicketsDetails_CaseNumber_Label);

                //--- Expected Result: The Case number - Value is Present ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_SupportTicketsDetails_CaseNumber_Value);

                //--- Expected Result: The Primary Product - Label is Present ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_SupportTicketsDetails_PrimaryProduct_Label);

                //--- Expected Result: The Primary Product - Value is Present ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_SupportTicketsDetails_PrimaryProduct_Value);

                //--- Expected Result: The Status - Label is Present ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_SupportTicketsDetails_Status_Label);

                //--- Expected Result: The Status - Value is Present ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_SupportTicketsDetails_Status_Value);

                //----- TICKET DETAILS -----//

                //--- Expected Result: The Ticket Details - Label is Present ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_SupportTicketsDetails_TicketDetails_Label);

                //----- SECTION HEADER -----//

                //--- Action: Click on a Support Ticket Case Title ---//
                action.IOpenLinkInNewTab(driver, Elements.MyAnalog_SupportTickets_First_Created_SupportTickets_CaseTitle);

                //--- Expected Result: The Case Title - Label is Present ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_SupportTicketsDetails_CaseTitle_Label);

                //--- Expected Result: The Back Arrow - Button is Present ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_SupportTicketsDetails_BackArrow_Icon);

                if (util.CheckElement(driver, Elements.MyAnalog_SupportTicketsDetails_BackArrow_Icon, 1))
                {
                    action.IClick(driver, Elements.MyAnalog_SupportTicketsDetails_BackArrow_Icon);

                    test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale.ToLower() + "/" + myAnalog_supportTickets_url);
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.MyAnalog_SupportTickets_First_Created_SupportTickets_CaseTitle);
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.MyAnalog_SupportTickets_First_Created_SupportTickets_CaseTitle);
            }
        }
    }
}