﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using OpenQA.Selenium;
using System;
using System.Threading;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_Dashboard_RelatedContentSection_Part1 : BaseSetUp
    {
        public MyAnalog_Dashboard_RelatedContentSection_Part1() : base() { }

        //-- Login Credentials --//
        string en_username = "myanalog_t3st_888@mailinator.com";
        string en_password = "Test_1234";
        string cn_username = "test_zh_04252019@mailinator.com";
        string cn_password = "Test_1234";
        string jp_username = "test_jp_04252019@mailinator.com";
        string jp_password = "Test_1234";

        //--- URLs ---//
        string myAnalog_dashboard_url = "app";
        string pdp_url_format = "/products/";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", "All", TestName = "Verify that the All Tab is Present and Working as Expected in EN Locale")]
        [TestCase("zh", "全部", TestName = "Verify that the All Tab is Present and Working as Expected in CN Locale")]
        [TestCase("jp", "すべてを表示", TestName = "Verify that the All Tab is Present and Working as Expected in JP Locale")]
        public void MyAnalog_Dashboard_RelatedContentSection_VerifyAllTab(string Locale, string All_Text)
        {
            Console.WriteLine("Steps to Test:");

            //--- Login Credentials ---//
            string username = null;
            string password = null;
            if (Locale.Equals("zh") || Locale.Equals("cn"))
            {
                username = cn_username;
                password = cn_password;
            }
            else if (Locale.Equals("jp"))
            {
                username = jp_username;
                password = jp_password;
            }
            else
            {
                username = en_username;
                password = en_password;
            }

            //--- myAnalog Dashboard URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_dashboard_url;

            //--- Action: Go to myAnalog dashboard ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid user account in the myAnalog Login page ---//
            action.ILogin(driver, username, password);
            Thread.Sleep(4000);

            //--- Expected Result: Related Content Tabs must be Present ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_RelatedContent_Tabs);

            if (!util.GetText(driver, Elements.MyAnalog_Dashboard_RelatedContent_Selected_Tab).Equals(All_Text) && !util.GetText(driver, Elements.MyAnalog_Dashboard_RelatedContent_Selected_Tab).Equals("All"))
            {
                //--- This looks for the All Tab ---//
                int relatedContent_tabs_count = util.GetCount(driver, Elements.MyAnalog_Dashboard_RelatedContent_Tabs);

                for (int counter = 1; counter <= relatedContent_tabs_count; counter++)
                {
                    if (util.GetText(driver, By.CssSelector("ul[class='nav nav-tabs']>li:nth-of-type(" + counter + ")>a")).Equals(All_Text) || util.GetText(driver, By.CssSelector("ul[class='nav nav-tabs']>li:nth-of-type(" + counter + ")>a")).Equals("All"))
                    {
                        //--- Locators ---//
                        string all_tab_locator = "ul[class='nav nav-tabs']>li:nth-of-type(" + counter + ")>a";

                        //--- Action: Click All tab ---//
                        action.IClick(driver, By.CssSelector(all_tab_locator));
                        Thread.Sleep(2000);

                        break;
                    }

                    if (counter == relatedContent_tabs_count)
                    {
                        //--- Expected Result: All Tab must be Present ---//
                        test.validateElementIsPresent(driver, By.CssSelector("ul[class='nav nav-tabs']>li:nth-of-type(" + (counter + 1) + ")>a"));
                    }
                }
            }

            if (util.CheckElement(driver, Elements.MyAnalog_Dashboard_All_Tab_Tiles, 5))
            {
                //----- Dashboard Test Plan > Desktop Tab > R7 > T1. 1: Verify the maximun number of display of Related Content -----//

                //--- Expected Result: Show 12 tiles at a time ---//
                test.validateCountIsLessOrEqual(driver, util.GetCount(driver, Elements.MyAnalog_Dashboard_All_Tab_Tiles), 12);

                if (util.CheckElement(driver, Elements.MyAnalog_Dashboard_ShowMore_Button, 5))
                {
                    //----- Dashboard Test Plan > Desktop Tab > R7 > T2.1: Verify when clicking show more button -----//

                    //--- Action: Click Show more button ---//
                    action.IClick(driver, Elements.MyAnalog_Dashboard_ShowMore_Button);
                    Thread.Sleep(1000);

                    //--- Expected Result: related content tiles should display ---//
                    test.validateCountIsGreater(driver, util.GetCount(driver, Elements.MyAnalog_Dashboard_All_Tab_Tiles), 12);
                }
            }
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", "New Products", TestName = "Verify that the New Products Tab is Present and Working as Expected in EN Locale")]
        [TestCase("zh", "新产品", TestName = "Verify that the New Products Tab is Present and Working as Expected in CN Locale")]
        [TestCase("jp", "新製品", TestName = "Verify that the New Products Tab is Present and Working as Expected in JP Locale")]
        public void MyAnalog_Dashboard_RelatedContentSection_VerifyNewProductsTab(string Locale, string NewProducts_Text)
        {
            Console.WriteLine("Steps to Test:");

            //--- Login Credentials ---//
            string username = null;
            string password = null;
            if (Locale.Equals("zh") || Locale.Equals("cn"))
            {
                username = cn_username;
                password = cn_password;
            }
            else if (Locale.Equals("jp"))
            {
                username = jp_username;
                password = jp_password;
            }
            else
            {
                username = en_username;
                password = en_password;
            }

            //--- myAnalog Dashboard URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_dashboard_url;

            //--- Action: Go to myAnalog dashboard ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid user account in the myAnalog Login page ---//
            action.ILogin(driver, username, password);
            Thread.Sleep(4000);

            //--- Expected Result: Related Content Tabs must be Present ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_RelatedContent_Tabs);

            if (!util.GetText(driver, Elements.MyAnalog_Dashboard_RelatedContent_Selected_Tab).Equals(NewProducts_Text) && !util.GetText(driver, Elements.MyAnalog_Dashboard_RelatedContent_Selected_Tab).Equals("New Products"))
            {
                //--- This looks for the New Products Tab ---//
                int relatedContent_tabs_count = util.GetCount(driver, Elements.MyAnalog_Dashboard_RelatedContent_Tabs);

                for (int counter = 1; counter <= relatedContent_tabs_count; counter++)
                {
                    if (util.GetText(driver, By.CssSelector("ul[class='nav nav-tabs']>li:nth-of-type(" + counter + ")>a")).Equals(NewProducts_Text) || util.GetText(driver, By.CssSelector("ul[class='nav nav-tabs']>li:nth-of-type(" + counter + ")>a")).Equals("New Products"))
                    {
                        //--- Locators ---//
                        string newProducts_tab_locator = "ul[class='nav nav-tabs']>li:nth-of-type(" + counter + ")>a";

                        //--- Action: Click New Product tab ---//
                        action.IClick(driver, By.CssSelector(newProducts_tab_locator));
                        Thread.Sleep(2000);

                        break;
                    }

                    if (counter == relatedContent_tabs_count)
                    {
                        //--- Expected Result: New Products Tab must be Present ---//
                        test.validateElementIsPresent(driver, By.CssSelector("ul[class='nav nav-tabs']>li:nth-of-type(" + (counter + 1) + ")>a"));
                    }
                }
            }

            if (util.CheckElement(driver, Elements.MyAnalog_Dashboard_NewProducts_Tiles, 5))
            {
                //----- Dashboard Test Plan > Desktop Tab > R7 > T3.1: Verify the component of New Product tab -----//

                //--- Expected Result: New Product Label are Present ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_NewProducts_Tiles_NewProducts_Labels);

                //--- Expected Result: Other option icon are Present ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_NewProducts_Tiles_OtherOption_Icons);

                //--- Expected Result: New Product image are Present ---//
                test.validateImageIsNotBeBroken(driver, Elements.MyAnalog_Dashboard_NewProducts_Tiles_Thumbnail_Images);

                //--- Expected Result: List of applications / List of tags are Present ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_1st_NewProducts_Tile_Tag_Links);

                if (util.CheckElement(driver, Elements.MyAnalog_Dashboard_1st_NewProducts_Tile_Tag_Links, 1))
                {
                    //----- Dashboard Test Plan > Desktop Tab > R7 > T3.4.1: Verify the maximum number of tags should display (IQ-6374/AL-13502) -----//

                    //--- Expected Result: Should show no more than 5 in collapsed view ---//
                    test.validateCountIsLessOrEqual(driver, util.GetCount(driver, Elements.MyAnalog_Dashboard_1st_NewProducts_Tile_Tag_Links), 5);
                }

                //----- Dashboard Test Plan > Desktop Tab > R7 > T3.1: Verify the component of New Product tab -----//

                //--- Expected Result: Product number are Present ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_NewProducts_Tiles_ProductNumber_Links);

                if (util.CheckElement(driver, Elements.MyAnalog_Dashboard_NewProducts_Tiles_ProductNumber_Links, 1))
                {
                    //----- Dashboard Test Plan > Desktop Tab > R7 > T3.3: Verify navigation when clicking Product number -----//

                    //--- Action: Click Product Number ---//
                    action.IOpenLinkInNewTab(driver, Elements.MyAnalog_Dashboard_NewProducts_Tiles_ProductNumber_Links);
                    Thread.Sleep(1000);

                    //--- Expected Result: Page will redirect to Product detail page ---//
                    if (Locale.Equals("zh") || Locale.Equals("cn"))
                    {
                        test.validateStringInstance(driver, driver.Url, "analog.com/cn" + pdp_url_format);
                    }
                    else
                    {
                        test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + pdp_url_format);
                    }
                }
            }
        }
    }
}