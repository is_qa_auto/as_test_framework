﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System;
using System.Linq;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_MegaMenu_ProductsTile : BaseSetUp
    {
        public MyAnalog_MegaMenu_ProductsTile() : base() { }

        //-- Login Credentials --//
        string username = "myanalog_t3st_pr0ducts_555@mailinator.com";
        string password = "Test_1234";

        //-- Login Credentials for New Account --//
        string username_new = "myanalog_t3st_new_555@mailinator.com";
        string password_new = "Test_1234";

        //--- URLs ---//
        string myAnalog_products_url = "app/products";
        string pdp_url_format = "/products/";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Products Tile is Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Products Tile is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Products Tile is Present and Working as Expected in JP Locale")]
        public void MyAnalog_MegaMenu_VerifyProductsTile(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Products URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_products_url;

            action.Navigate(driver, myAnalog_url);

            //--- Action: Log in valid account in login page ---//
            action.ILogin(driver, username, password);

            //--- Action: Get the Count for the Saved Products ---//
            int actual_savedProducts_count = util.GetMyAnalogSavedProductsCount(driver);

            if (!driver.Url.Equals(myAnalog_url) && !driver.Url.Equals(myAnalog_url + "#"))
            {
                //--- Action: Go to myAnalog Projects ---//
                driver.Navigate().GoToUrl(myAnalog_url);
            }

            //--- Action: click myanalog tab ---//
            action.IExpandMyAnalogMegaMenu(driver);

            //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R2 > T2: Verify myPage Components -----//

            //--- Expected Result: The Products tile is displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_MegaMenu_Products_Tile);

            //----- PRODUCTS TITLE -----//

            //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R4 > T1: Verify Product Tile component -----//

            //--- Expected Result: The Product  link component is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_MegaMenu_Products_Link);

            if (util.CheckElement(driver, Elements.MyAnalog_MegaMenu_Products_Link, 2))
            {
                //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R4 > T2: Verify when clicking Product link -----//

                //--- Expected Result: Product counter is dispayed ---//
                test.validateStringInstance(driver, util.ExtractNumber(driver, Elements.MyAnalog_MegaMenu_Products_Link), actual_savedProducts_count.ToString());
            }

            //----- SEE ALL YOUR PRODUCTS -----//

            //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R4 > T1: Verify Product Tile component -----//

            //--- Expected Result: The See all your products link component is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_MegaMenu_SeeAllYourProducts_Link);

            if (util.CheckElement(driver, Elements.MyAnalog_MegaMenu_SeeAllYourProducts_Link, 2))
            {
                //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R4 > T6: Verify when clicking See all your products -----//
                string current_url = driver.Url;

                //--- Action: Click See all your products link ---//
                action.IClick(driver, Elements.MyAnalog_MegaMenu_SeeAllYourProducts_Link);

                if (driver.Url.Contains(current_url))
                {
                    driver.SwitchTo().Window(driver.WindowHandles.Last());
                }

                //--- Expected Result: Products detail page is displayed ---//
                test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + "/" + myAnalog_products_url);
            }

            //----- SAVED PRODUCTS -----//

            //--- Action: Click myAnalog tab ---//
            action.IExpandMyAnalogMegaMenu(driver);

            //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R2 > T2: Verify myPage Components -----//

            //--- Expected Result: 10 most recent items ---//
            test.validateCountIsLessOrEqual(driver, 10, util.GetCount(driver, Elements.MyAnalog_MegaMenu_Saved_Products));

            //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R4 > T1: Verify Product Tile component -----//

            //--- Expected Result: The Datasheet link component are available---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_MegaMenu_Saved_Products_Datasheet_Links);

            //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R4 > T1: Verify Product Tile component -----//

            //--- Expected Result: The Product link component are available---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_MegaMenu_Saved_Products_Name_Links);

            if (util.CheckElement(driver, Elements.MyAnalog_MegaMenu_Saved_Products_Name_Links, 2))
            {
                string productName_link = util.GetText(driver, Elements.MyAnalog_MegaMenu_Saved_Products_Name_Links);
                string current_url = driver.Url;

                //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R4 > T4: Verify when clicking Product link -----//

                //--- Action: Click Product link ---//
                action.IClick(driver, Elements.MyAnalog_MegaMenu_Saved_Products_Name_Links);

                if (driver.Url.Contains(current_url))
                {
                    driver.SwitchTo().Window(driver.WindowHandles.Last());
                }

                //--- Expected Result: Page will redirect to product detail page ---//
                test.validateStringInstance(driver, driver.Url, pdp_url_format);

                //--- Expected Result: Product number in product detail page is same as displayed in product tile ---//
                test.validateStringIsCorrect(driver, Elements.PDP_ProductNo_Txt, productName_link.ToUpper());
            }
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Products Tile is Present and Working as Expected when No Product is Saved in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Products Tile is Present and Working as Expected when No Product is Saved in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Products Tile is Present and Working as Expected when No Product is Saved in JP Locale")]
        public void MyAnalog_MegaMenu_ProductsTile_VerifyWhenNoProductIsSaved(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Products URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_products_url;

            action.Navigate(driver, myAnalog_url);

            //--- Action: Log in valid account in login page ---//
            action.ILogin(driver, username_new, password_new);

            //--- Action: Click myAnalog menu panel ---//
            action.IExpandMyAnalogMegaMenu(driver);

            //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R1 > T3.1: Verify Products section if there are no saved products (IQ-6351/AL-13441) -----//

            //--- Expected Result: there should be message in product tile "you don’t have any products saved" ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_MegaMenu_NoSavedProduct_Message);
        }
    }
}