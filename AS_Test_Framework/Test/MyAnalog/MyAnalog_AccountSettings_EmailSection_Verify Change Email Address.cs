﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using OpenQA.Selenium;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_AccountSettings_EmailSection_ChangeEmailAddress : BaseSetUp
    {
        //--- myAnalog Account Settings URL: ---//
        //--- DEV: https://my-dev.corpnt.analog.com/en/app/account ---//
        //--- QA: https://my-qa.corpnt.analog.com/en/app/account ---//
        //--- PROD: https://my.analog.com/en/app/account ---//

        public MyAnalog_AccountSettings_EmailSection_ChangeEmailAddress() : base() { }

        public static DataTable dtElements;

        //--- Login Credentials ---//
        string username = null;
        string password = null;

        //--- URLs ---//
        string myAnalog_accountSettings_url = "app/account";
        string login_page_url = "b2clogin";

        //--- Labels ---//
        string myAnalog_verificationCode_emailSubject = "myAnalog: Verification Code";
        string oldAccount_errorMessage = "A user with the specified credential could not be found.";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that Change Email Address is Working as Expected in EN Locale")]
        //[TestCase("zh", TestName = "Verify that Change Email Address is Working as Expected in CN Locale")]
        //[TestCase("jp", TestName = "Verify that Change Email Address is Working as Expected in JP Locale")]
        public void MyAnalog_AccountSettings_EmailSection_VerifyChangeEmailAddress(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- Login Credentials ---//
            username = GetEmailValue(Configuration.Environment, "Email_Address");
            password = "Test_1234";

            //--- myAnalog Account Settings URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_accountSettings_url;

            //--- Action: Open account settings page ---//
            action.Navigate(driver, myAnalog_url);

            action.ILogin(driver, username, password);
            Thread.Sleep(1000);

            //----- Account Settings Test Plan > myAnalog Dashboard Tab > R6 > T8: Verify time limit up until the verification code is still valid (IQ-7832/AL-14819) -----//

            //--- Action: Click the Edit button / hyperlink in the Email section ---//
            action.IClick(driver, Elements.MyAnalog_AccountSettings_Email_Section_Edit_Button);

            //--- Action: Enter new Email Address in the 'Enter New Email' field ---//
            string enterNewEmail_input = "myanalog_t3st_ch4ng3_em41l_" + util.GenerateRandomNumber() + "@mailinator.com";
            action.IDeleteValueOnFields(driver, Elements.MyAnalog_AccountSettings_Email_Section_EnterNewEmail_InputBox);
            action.IType(driver, Elements.MyAnalog_AccountSettings_Email_Section_EnterNewEmail_InputBox, enterNewEmail_input);

            //--- Action: Enter the same Email Address in the 'Confirm New Email' field ---//
            string confirmNewEmail_input = enterNewEmail_input;
            action.IDeleteValueOnFields(driver, Elements.MyAnalog_AccountSettings_Email_Section_ConfirmNewEmail_InputBox);
            action.IType(driver, Elements.MyAnalog_AccountSettings_Email_Section_ConfirmNewEmail_InputBox, confirmNewEmail_input);

            //--- Action: Click the Send verification code button ---//
            action.IClick(driver, Elements.MyAnalog_AccountSettings_Email_Section_SendVerificationCode_Button);
            Thread.Sleep(1250);

            ((IJavaScriptExecutor)driver).ExecuteScript("window.open()");
            driver.SwitchTo().Window(driver.WindowHandles.Last());
            action.ICheckMailinatorEmail(driver, enterNewEmail_input);

            //--- Expected Result: User received 'myAnalog: Verification Code' email ---//
            if (Locale.Equals("en"))
            {
                test.validateStringInstance(driver, util.GetText(driver, Elements.Mailinator_LatestEmail_Subj), myAnalog_verificationCode_emailSubject);
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.Mailinator_LatestEmail_Subj);
            }

            action.IClick(driver, Elements.Mailinator_LatestEmail_Sender);
            Thread.Sleep(1000);
            driver.SwitchTo().Frame(driver.FindElement(By.CssSelector("iframe[id='html_msg_body']")));
            string verification_code = util.GetText(driver, Elements.Mailinator_Email_Verification_Code).Substring(util.GetText(driver, Elements.Mailinator_Email_Verification_Code).Length - 6).Trim();
            driver.Close();
            driver.SwitchTo().Window(driver.WindowHandles.Last());

            //--- Action: enter Verification Code indicated in the email in the 'Enter Verification Code' field ---//
            string enterVerificationCode_input = verification_code;
            action.IDeleteValueOnFields(driver, Elements.MyAnalog_AccountSettings_Email_Section_EnterVerificationCode_InputBox);
            action.IType(driver, Elements.MyAnalog_AccountSettings_Email_Section_EnterVerificationCode_InputBox, enterVerificationCode_input);

            //--- Action: Click the Submit button ---//
            action.IClick(driver, Elements.MyAnalog_AccountSettings_Email_Section_Submit_Button);
            Thread.Sleep(7000);

            UpdateEmailValue("Old_Email", GetEmailValue(Configuration.Environment, "Email_Address"), username, Configuration.Environment);
            UpdateEmailValue("Email_Address", enterNewEmail_input, username, Configuration.Environment);

            //--- Expected Result: The System displays the B2C Login page (System automatically logged-out) ---//
            test.validateStringInstance(driver, driver.Url, login_page_url);

            //--- Action: Enter the old Email Address in the Email field ---//
            //--- Action: Enter the Password of the old account in the Password field ---//
            //--- Action: Click the Login button ---//
            action.ILogin(driver, username, password);
            Thread.Sleep(1000);

            //--- Expected Result: The System displays an Error Message 'A user with the specified credential could not be found' ---//
            if (Locale.Equals("en"))
            {
                test.validateStringIsCorrect(driver, Elements.LogIn_Error_Message, oldAccount_errorMessage);
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.LogIn_Error_Message);
            }

            action.IDeleteValueOnFields(driver, Elements.UserTextField);
            action.IDeleteValueOnFields(driver, Elements.PwdTextField);

            //--- Action: Enter the Email Address of the newly registered account in the Email field ---//   
            //--- Action: Enter the Password of the newly registered account in the Password field ---//
            //--- Action: Click the Login button ---//
            action.ILogin(driver, enterNewEmail_input, password);

            //--- Action: The User is successfully logged in to myAnalog ---//
            test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + "/" + myAnalog_accountSettings_url);

            //--- Expected Result: The new Email is displayed ---//
            test.validateStringInstance(driver, util.GetInputBoxValue(driver, Elements.MyAnalog_AccountSettings_Email_Section_CurrentEmail_InputBox), enterNewEmail_input);
        }

        public string GetEmailValue(string EnvironmentValue, string ColumnToSelect)
        {
            //--- CAV-OUTSR-D01 DB ---//
            string cn = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

            ////--- LOCAL DB ---//
            //string cn = System.Configuration.ConfigurationManager.ConnectionStrings["LocalConnection"].ConnectionString;

            SqlConnection myConnection = new SqlConnection(cn);
            myConnection.Open();
            SqlCommand myCommand = new SqlCommand("SELECT * FROM ebiz_user_account", myConnection);

            dtElements = new DataTable();
            dtElements.Load(myCommand.ExecuteReader());

            myConnection.Close();

            return dtElements.Select(string.Format("Environment = '{0}' AND Email_Address LIKE '{1}%'", EnvironmentValue, "myanalog_t3st_ch4ng3_em41l_"))[0][ColumnToSelect].ToString();
        }

        public void UpdateEmailValue(string ColumnToUpdate, string NewValue, string Username, string EnvironmentValue)
        {
            //--- CAV-OUTSR-D01 DB ---//
            string cn = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

            ////--- LOCAL DB ---//
            //string cn = System.Configuration.ConfigurationManager.ConnectionStrings["LocalConnection"].ConnectionString;

            SqlConnection myConnection = new SqlConnection(cn);
            myConnection.Open();
            SqlCommand myCommand = new SqlCommand("UPDATE ebiz_user_account SET " + ColumnToUpdate + " = '" + NewValue + "' WHERE Email_Address = '" + Username + "' AND Environment = '" + EnvironmentValue + "'", myConnection);
            myCommand.ExecuteNonQuery();
            myConnection = null;
        }
    }
}