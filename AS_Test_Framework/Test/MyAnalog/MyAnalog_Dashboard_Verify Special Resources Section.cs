﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_Dashboard_SpecialResourcesSection : BaseSetUp
    {
        public MyAnalog_Dashboard_SpecialResourcesSection() : base() { }

        //-- Login Credentials --//
        string username = "myanalog_t3st_pr0ducts_555@mailinator.com";
        string password = "Test_1234";

        //--- URLs ---//
        string myAnalog_dashboard_url = "app";
        string myAnalog_resources_url = "app/resources";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Special Resources Section is Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Special Resources Section is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Special Resources Section is Present and Working as Expected in JP Locale")]
        public void MyAnalog_Dashboard_SpecialResourcesSection_VerifyForUsersThatHaveAnEngineerzoneUsername(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Dashboard URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_dashboard_url;

            //--- Action: Go to myAnalog dashboard ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid user account in the myAnalog Login page ---//
            action.ILogin(driver, username, password);

            //----- Dashboard Test Plan > Desktop Tab > R6 > T1: Verify Insider Resources -----//

            //--- Expected Result: The Special Resources title is displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_SpecialResources_Label);

            if (util.CheckElement(driver, Elements.MyAnalog_Dashboard_SpecialResources_Links, 2))
            {
                //----- Dashboard Test Plan > Desktop Tab > R6 > T1.3: Verify the number of content display -----//

                //--- Expected Result: 10 pieces of content is displayed ---//
                test.validateCountIsLessOrEqual(driver, util.GetCount(driver, Elements.MyAnalog_Dashboard_SpecialResources_Links), 10);
            }

            //----- Dashboard Test Plan > Desktop Tab > R6 > T1: Verify Insider Resources -----//

            //--- Expected Result: The See all your Special Resources link is displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_SeeAllYourSpecialResources_Link);

            if (util.CheckElement(driver, Elements.MyAnalog_Dashboard_SeeAllYourSpecialResources_Link, 1))
            {
                //----- Dashboard Test Plan > Desktop Tab > R8 > T1: Verify number of content display -----//

                //--- Action: Click See all resources ---//
                action.IOpenLinkInNewTab(driver, Elements.MyAnalog_Dashboard_SeeAllYourSpecialResources_Link);

                //--- Expected Result: takes user to Resources page ---//
                test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale.ToLower() + "/" + myAnalog_resources_url);
            }
        }
    }
}