﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System;
using System.Linq;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_Dashboard_LeftNavigationSection : BaseSetUp
    {
        public MyAnalog_Dashboard_LeftNavigationSection() : base() { }

        //-- Login Credentials --//
        string username = "marvin.bebe@analog.com";
        string password = "Test_1234";

        //--- URLs ---//
        string myAnalog_dashboard_url = "app";
        string myAnalog_accountSettings_url = "app/account";
        string myAnalog_productss_url = "app/products";
        string myAnalog_orders_url = "app/orders";
        string myAnalog_projects_url = "app/projects";
        string myAnalog_resources_url = "app/resources";
        string myAnalog_parametricSearches_url = "app/parametric-searches";
        string myAnalog_subscriptions_url = "app/subscriptions";
        string myAnalog_manageContacts_url = "app/manage-contacts";

        //--- Labels ---//
        string myAnalog_text = "myAnalog";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Left Navigation Section is Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Left Navigation Section is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Left Navigation Section is Present and Working as Expected in JP Locale")]
        public void MyAnalog_Dashboard_VerifyLeftNavigationSection(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Dashboard URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_dashboard_url;

            //--- Action: Go to myAnalog dashboard ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid user account in the myAnalog Login page ---//
            action.ILogin(driver, username, password);

            //----- Dashboard Test Plan > Desktop Tab > R2 > T2: Verify myAnalog button in the left rail navigation -----//

            //--- Expected Result: myAnalog section should be present at the left pane ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Navigation_MyAnalog_Link);

            if (util.CheckElement(driver, Elements.MyAnalog_Navigation_MyAnalog_Link, 2))
            {
                //--- Expected Result: MyAnalog is selected by default ---//
                test.validateStringIsCorrect(driver, Elements.MyAnalog_Navigation_Selected_Link, myAnalog_text);
            }

            //----- Dashboard Test Plan > Desktop Tab > R2 > T3: Verify Account Settings in the left navigation -----//

            //--- Expected Result: Account Settings should be present ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Navigation_AccountSettings_Link);

            if (util.CheckElement(driver, Elements.MyAnalog_Navigation_AccountSettings_Link, 2))
            {
                //--- Action: Click Account Settings ---//
                action.IOpenLinkInNewTab(driver, Elements.MyAnalog_Navigation_AccountSettings_Link);

                //--- Expected Result: The page should display the Account settings section ---//
                test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale.ToLower() + "/" + myAnalog_accountSettings_url);

                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles.Last());
            }

            //----- Dashboard Test Plan > Desktop Tab > R2 > T4: Verify Products in the left navigation -----//

            //--- Expected Result: Products should be present ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Navigation_Products_Link);

            if (util.CheckElement(driver, Elements.MyAnalog_Navigation_Products_Link, 2))
            {
                //--- Action: Click Products ---//
                action.IOpenLinkInNewTab(driver, Elements.MyAnalog_Navigation_Products_Link);

                //--- Expected Result: The page should display the Products section ---//
                test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale.ToLower() + "/" + myAnalog_productss_url);

                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles.Last());
            }

            //----- Dashboard Test Plan > Desktop Tab > R2 > T5: Verify Orders in the left navigation -----//

            //--- Expected Result: Orders should be present ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Navigation_Orders_Link);

            if (util.CheckElement(driver, Elements.MyAnalog_Navigation_Orders_Link, 2))
            {
                //--- Action: Click Orders ---//
                action.IOpenLinkInNewTab(driver, Elements.MyAnalog_Navigation_Orders_Link);

                //--- Expected Result: The page should display the Orders detail section ---//
                test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale.ToLower() + "/" + myAnalog_orders_url);

                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles.Last());
            }

            //----- Dashboard Test Plan > Desktop Tab > R2 > T6: Verify Projects in the left navigation -----//

            //--- Expected Result: Projects should be present ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Navigation_Projects_Link);

            if (util.CheckElement(driver, Elements.MyAnalog_Navigation_Projects_Link, 2))
            {
                //--- Action: Click Projects ---//
                action.IOpenLinkInNewTab(driver, Elements.MyAnalog_Navigation_Projects_Link);

                //--- Expected Result: The page should display the Projects section ---//
                test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale.ToLower() + "/" + myAnalog_projects_url);

                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles.Last());
            }

            //----- Dashboard Test Plan > Desktop Tab > R2 > T7: Verify Resources in the left navigation -----//

            //--- Expected Result: Resources should be present ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Navigation_Resources_Link);

            if (util.CheckElement(driver, Elements.MyAnalog_Navigation_Resources_Link, 2))
            {
                //--- Action: Click Resources ---//
                action.IOpenLinkInNewTab(driver, Elements.MyAnalog_Navigation_Resources_Link);

                //--- Expected Result: The page should display the Resources section ---//
                test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale.ToLower() + "/" + myAnalog_resources_url);

                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles.Last());
            }

            //----- Dashboard Test Plan > Desktop Tab > R2 > T8: Verify Parametric Searches in the left navigation -----//

            //--- Expected Result: Parametric Searches should be present ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Navigation_ParametricSearches_Link);

            if (util.CheckElement(driver, Elements.MyAnalog_Navigation_ParametricSearches_Link, 2))
            {
                //--- Action: Click Parametric Searches ---//
                action.IOpenLinkInNewTab(driver, Elements.MyAnalog_Navigation_ParametricSearches_Link);

                //--- Expected Result: The page should display the Parametric Searches section ---//
                test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale.ToLower() + "/" + myAnalog_parametricSearches_url);

                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles.Last());
            }

            //----- Dashboard Test Plan > Desktop Tab > R2 > T9: Verify Subscriptions in the left navigation -----//

            //--- Expected Result: Subscription should be present ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Navigation_Subscriptions_Link);

            if (util.CheckElement(driver, Elements.MyAnalog_Navigation_Subscriptions_Link, 2))
            {
                //--- Action: Click Subscription Searches ---//
                action.IOpenLinkInNewTab(driver, Elements.MyAnalog_Navigation_Subscriptions_Link);

                //--- Expected Result: The page should display the Subscription section ---//
                test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale.ToLower() + "/" + myAnalog_subscriptions_url);

                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles.Last());
            }

            if (util.CheckElement(driver, Elements.MyAnalog_Navigation_ManageContacts_Link, 2))
            {
                //----- Dashboard Test Plan > Desktop Tab > R2 > T17: Verify if myAnalog does not occur an error. (IQ-7647/ AL-14331) -----//

                //--- Action: Click Manage contact  In left menu trail ---//
                action.IOpenLinkInNewTab(driver, Elements.MyAnalog_Navigation_ManageContacts_Link);

                //--- Expected Result: Manage contact display ---//
                test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale.ToLower() + "/" + myAnalog_manageContacts_url);
            }
        }
    }
}