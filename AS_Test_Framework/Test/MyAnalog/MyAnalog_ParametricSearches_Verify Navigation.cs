﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using System;
using System.Threading;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_ParametricSearches_Navigation : BaseSetUp
    {
        //--- myAnalog Parametric Searches URL: ---//
        //--- DEV: https://my-dev.corpnt.analog.com/en/app/parametric-searches ---//
        //--- QA: https://my-qa.corpnt.analog.com/en/app/parametric-searches ---//
        //--- PROD: https://my.analog.com/en/app/parametric-searches ---//

        public MyAnalog_ParametricSearches_Navigation() : base() { }

        //--- Login Credentials ---//
        string username = "myanalog_t3st_pst_555@mailinator.com";
        string password = "Test_1234";

        //--- URLs ---//
        string myAnalog_parametricSearches_url = "app/parametric-searches";
        string login_page_url = "b2clogin";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Navigation is Working as Expected when the User is Logged Out in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Navigation is Working as Expected when the User is Logged Out in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Navigation is Working as Expected when the User is Logged Out in JP Locale")]
        public void MyAnalog_ParametricSearches_Navigation_VerifyWhenUserIsLoggedOut(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Parametric Searches URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_parametricSearches_url;

            //----- Parametric Searches Test Plan > myAnalog Dashboard Tab > R1 > T3: Verify navigation to Parametric search using direct link (when account is not login) -----//

            //--- Action: Open parametric search using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Expected Result: page will redirect to login page ---//
            test.validateStringInstance(driver, driver.Url, login_page_url);
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Navigation is Working as Expected when the User is Logged In in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Navigation is Working as Expected when the User is Logged In in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Navigation is Working as Expected when the User is Logged In in JP Locale")]
        public void MyAnalog_ParametricSearches_Navigation_VerifyWhenUserIsLoggedIn(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Parametric Searches URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_parametricSearches_url;

            //----- Parametric Searches Test Plan > myAnalog Dashboard Tab > R1 > T4: Verify navigation to Parametric search using direct link (when account is not login) -----//

            //--- Action: Open parametric search using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);
            Thread.Sleep(1000);

            //--- Expected Result: After logging in page redirect to parametric searches ---//
            test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + "/" + myAnalog_parametricSearches_url);
        }
    }
}