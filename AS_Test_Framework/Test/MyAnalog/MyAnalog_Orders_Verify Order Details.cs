﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using OpenQA.Selenium;
using System;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_Orders_OrderDetails : BaseSetUp
    {
        //--- myAnalog Orders URL: ---//
        //--- DEV: https://my-dev.corpnt.analog.com/en/app/orders ---//
        //--- QA: https://my-qa.corpnt.analog.com/en/app/orders ---//
        //--- PROD: https://my.analog.com/en/app/orders ---//

        public MyAnalog_Orders_OrderDetails() : base() { }

        //--- Login Credentials ---//
        string username = "marvin.bebe@analog.com";
        string password = "Test_1234";

        //--- URLs ---//
        string myAnalog_orders_url = "app/orders";
        string customerServiceCenter_url = "/customer-service-resources.html";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Elements are Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Elements are Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Elements are Present and Working as Expected in JP Locale")]
        public void MyAnalog_Orders_VerifyOrderDetails(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Orders URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_orders_url;

            //--- Action: Open order page direct link to browser ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Enter valid username and password and click login button ---//
            action.ILogin(driver, username, password);
            Thread.Sleep(3000);

            string orderNumber_link = util.GetText(driver, Elements.MyAnalog_Orders_OrderNumber_Row_Link);
            action.IClick(driver, Elements.MyAnalog_Orders_OrderNumber_Row_Link);

            do
            {
                driver.Navigate().Refresh();
            }
            while (util.CheckElement(driver, By.CssSelector("section[class='order details']>p"), 5));

            //----- MYANALOG > ORDER NUMBER -----//

            //----- Order Information -----//

            //----- Orders and Order Details Test Plan > myAnalog Dashboard > R4 > T1: Verify Order Detail landing page -----//

            //--- Expected Result: The Order Status is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Orders_OrderNumber_OrderStatus_Label);

            //--- Expected Result: The Order Placed is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Orders_OrderNumber_OrderPlaced_Label);

            //--- Expected Result: The PO Number is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Orders_OrderNumber_PoNumber_Label);

            //--- Expected Result: The Delivery Method is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Orders_OrderNumber_DeliveryMethod_Label);

            //--- Expected Result: The Shipping Address is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Orders_OrderNumber_ShippingAddress_Label);

            //--- Expected Result: The Billing Address is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Orders_OrderNumber_BillingAddress_Label);

            //--- Expected Result: The Email is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Orders_OrderNumber_Email_Label);

            //----- Order Details -----//

            //----- Orders and Order Details Test Plan > myAnalog Dashboard > R4 > T14: Check available columns in Order Detail Page -----//

            //--- Expected Result: The Model column is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Orders_OrderNumber_Model_Label);

            //--- Expected Result: The Status column is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Orders_OrderNumber_Status_Label);

            //--- Expected Result: The Delivery Number column is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Orders_OrderNumber_DeliveryNumber_Label);

            //--- Expected Result: The Est. Delivery Date column is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Orders_OrderNumber_EstDeliveryDate_Label);

            //--- Expected Result: The Quantity column is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Orders_OrderNumber_Quantity_Label);

            //--- Expected Result: The Unit Price column is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Orders_OrderNumber_UnitPrice_Label);

            //--- Expected Result: The Total Price column is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Orders_OrderNumber_TotalPrice_Label);

            //----- Orders and Order Details Test Plan > myAnalog Dashboard > R4 > T25: Message at the bottom of the Order Details page (IQ-6125) -----//

            //--- Expected Result: message is displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Orders_OrderNumber_Disclaimer_Text);

            //----- Section Header -----//

            //----- Orders and Order Details Test Plan > myAnalog Dashboard > R4 > T1: Verify Order Detail landing page -----//

            //--- Expected Result: The Order# is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Orders_OrderNumber_OrderNumber_Label);

            //----- Orders and Order Details Test Plan > myAnalog Dashboard > R4 > T5: Customer service link -----//
            if (util.CheckElement(driver, Elements.MyAnalog_Orders_OrderNumber_CustomerService_Link, 1))
            {
                //--- Action: Click the Customer Service link ---//
                action.IOpenLinkInNewTab(driver, Elements.MyAnalog_Orders_OrderNumber_CustomerService_Link);

                //--- Expected Result: System will redirect user to Contact Customer Service page ---//
                test.validateStringInstance(driver, driver.Url, customerServiceCenter_url);

                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles.Last());
            }

            //----- Orders and Order Details Test Plan > myAnalog Dashboard > R4 > T4: Verify that clicking the back arrow redirects user back to Orders page -----//
            Assert.Multiple(() =>
            {
                //--- Expected Result: a back arrow is present ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_Orders_OrderNumber_BackArrow_Icon);

                //--- Action: Click the back arrow ---//
                action.IClick(driver, Elements.MyAnalog_Orders_OrderNumber_BackArrow_Icon);

                //--- Expected Result: System will redirect user to the Orders page ---//
                test.validateStringInstance(driver, driver.Url, myAnalog_orders_url);
            });
        }
    }
}