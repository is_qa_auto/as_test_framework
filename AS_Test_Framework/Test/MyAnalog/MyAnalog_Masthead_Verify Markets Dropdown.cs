﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System;
using OpenQA.Selenium;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_Masthead_Markets : BaseSetUp
    {
        public MyAnalog_Masthead_Markets() : base() { }

        //--- Login Credentials ---//
        string username = "aries.sorosoro@analog.com";
        string password = "Test_1234";

        [Test, Category("MyAnalog"), Category("NonCore")]
        [TestCase("en", TestName = "Verify Market Component in myAnalog Masthead is present and complete in EN Locale")]
        [TestCase("zh", TestName = "Verify Market Component in myAnalog Masthead is present and complete in CN Locale")]
        [TestCase("jp", TestName = "Verify Market Component in myAnalog Masthead is present and complete in JP Locale")]
        public void MyAnalog_Masthead_VerifyCategories(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Resources URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/app";

            //--- Action: Open MyAnalog App direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);

            test.validateElementIsPresent(driver, Elements.MyAnalog_Masthead_Category_Count);

            action.IClick(driver, Elements.MyAnalog_Masthead_Market_DD);

            test.validateElementIsPresent(driver, Elements.MyAnalog_Masthead_Category_Menu_List);
            test.validateElementIsPresent(driver, Elements.MyAnalog_Masthead_Category_Saved);
            test.validateElementIsPresent(driver, Elements.MyAnalog_Masthead_Category_Search);
            test.validateElementIsPresent(driver, Elements.MyAnalog_Masthead_Category_Saved_Button);
            test.validateElementIsPresent(driver, Elements.MyAnalog_Masthead_Category_Cancel_Button);

            //Validate Sub Category will be displayed
            action.IClick(driver, By.CssSelector("section[class='interests__menu__items__options'] ul>li:nth-child(1) button"));

            test.validateElementIsPresent(driver, By.CssSelector("section[class='interests__menu__items__options'] ul>li:nth-child(1) ul"));

            //Validate Sub - Child Category will be displayed
            action.IClick(driver, By.CssSelector("section[class='interests__menu__items__options'] ul>li:nth-child(1) ul>li:nth-child(1) button"));

            test.validateElementIsPresent(driver, By.CssSelector("section[class='interests__menu__items__options'] ul>li:nth-child(1) ul>li:nth-child(1) ul"));
        }
    }
}