﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_Projects_ProjectListing_NoProjects : BaseSetUp
    {
        //--- myAnalog Projects URL: ---//
        //--- DEV: https://my-dev.corpnt.analog.com/en/app/projects ---//
        //--- QA: https://my-qa.corpnt.analog.com/en/app/projects ---//
        //--- PROD: https://my.analog.com/en/app/projects ---//

        public MyAnalog_Projects_ProjectListing_NoProjects() : base() { }

        //----- Note: Please don't edit/delete any details on the Accounts below -----//

        //-- Login Credentials (Account that doesn't have Projects) --//
        string username = "myanalog_t3st_new_555@mailinator.com";
        string password = "Test_1234";

        //--- URLs ---//
        string myAnalog_projects_url = "app/projects";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Project Listing is Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Project Listing is Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Project Listing is Working as Expected in JP Locale")]
        public void MyAnalog_Projects_ProjectListing_VerifyNoProjects(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Projects URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_projects_url;

            //--- Action: Open myAnalog Projects using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify Project landing when there is NO existing project (IQ-6371/AL-13538) -----//

            //--- Expected Result: The Project label is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Projects_Label);

            //--- Expected Result: There should be no project displayed in the page ---//
            test.validateElementIsNotPresent(driver, Elements.MyAnalog_Projects_Created_Projects);

            //--- Expected Result: Create a new project is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Projects_CreateANewProject_Button);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Display Projects benefits video when myAnalog user has no projects (IQ-10998/AL-17671) -----//

            //--- Expected Result: Video that shows the benefits of the "Projects" section is displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Projects_CreatingANewProject_Video);
        }
    }
}