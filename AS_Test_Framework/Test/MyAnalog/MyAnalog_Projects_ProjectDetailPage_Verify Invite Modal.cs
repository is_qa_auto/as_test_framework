﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using OpenQA.Selenium;
using System;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_Projects_ProjectDetailPage_InviteModal : BaseSetUp
    {
        //--- myAnalog Projects URL: ---//
        //--- DEV: https://my-dev.corpnt.analog.com/en/app/projects ---//
        //--- QA: https://my-qa.corpnt.analog.com/en/app/projects ---//
        //--- PROD: https://my.analog.com/en/app/projects ---//

        public MyAnalog_Projects_ProjectDetailPage_InviteModal() : base() { }

        //----- Note: Please don't edit/delete any details on the Accounts below -----//

        //--- Login Credentials (Project Owner) ---// 
        string username = "my_t3st_pr0j3ct_1nv1t3_555@mailinator.com";
        string password = "Test_1234";

        //--- URLs ---//
        string myAnalog_projects_url = "app/projects";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Invite Modal is Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Invite Modal is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Invite Modal is Present and Working as Expected in JP Locale")]
        public void MyAnalog_Projects_ProjectDetailPage_VerifyInviteModal(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Projects URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_projects_url;

            //--- Action: Open myAnalog Projects using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);

            //--- Action: Click Project Detail button ---//
            action.IClick(driver, Elements.MyAnalog_Projects_ProjectDetails_Buttons);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Navigate to Project Details Page -----//

            //--- Expected Result: The invite option component is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_ProjectDetailPage_Invite_Button);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify when sending an invitation -----//

            //--- Action: Click Invite icon ---//
            action.IClick(driver, Elements.MyAnalog_ProjectDetailPage_Invite_Button);

            //--- Expected Result: Project Invite pop-up will display ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Projects_Invite_Modal);

            //--- Expected Result: The Project title is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Projects_Invite_Modal_Project_Name_Label);

            //--- Expected Result: The Add recepient's email is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Projects_Invite_Modal_Invitees_InputBox);

            //--- Expected Result: The message textbox is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Projects_Invite_Modal_Message_InputBox);

            //--- Expected Result: The Quick message  is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Projects_Invite_Modal_QuickMessage_Buttons);

            //--- Expected Result: The Cancel button is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Projects_Invite_Modal_Cancel_Button);

            //--- Expected Result: The Invite button is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Projects_Invite_Modal_Invite_Button);

            //--- Action: Select quick message ---//
            action.IClick(driver, Elements.MyAnalog_Projects_Invite_Modal_QuickMessage_Buttons);

            //--- Expected Result: Selected quick message is displayed in the text field ---//       
            test.validateStringInstance(driver, util.GetText(driver, Elements.MyAnalog_Projects_Invite_Modal_QuickMessage_Buttons), util.GetInputBoxValue(driver, Elements.MyAnalog_Projects_Invite_Modal_Message_InputBox));
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the X Button is Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the X Button is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the X Button is Present and Working as Expected in JP Locale")]
        public void MyAnalog_Projects_ProjectDetailPage_VerifyXButton(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Projects URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_projects_url;

            //--- Action: Open myAnalog Projects using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);

            //--- Action: Click Project Detail button ---//
            action.IClick(driver, Elements.MyAnalog_Projects_ProjectDetails_Buttons);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify when sending an invitation -----//

            //--- Action: Click Invite icon ---//
            action.IClick(driver, Elements.MyAnalog_ProjectDetailPage_Invite_Button);

            //--- Expected Result: The Close icon (x) is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Projects_Invite_Modal_X_Button);

            //--- Action: Click (x) icon---//
            action.IClick(driver, Elements.MyAnalog_Projects_Invite_Modal_X_Button);

            //--- Expected Result: The Invite modal will close ---//
            test.validateElementIsNotPresent(driver, Elements.MyAnalog_Projects_Invite_Modal);
        }
    }
}