﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using OpenQA.Selenium;
using System;
using System.Threading;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_AccountSettings_LocationSection : BaseSetUp
    {
        //--- myAnalog Account Settings URL: ---//
        //--- DEV: https://my-dev.corpnt.analog.com/en/app/account ---//
        //--- QA: https://my-qa.corpnt.analog.com/en/app/account ---//
        //--- PROD: https://my.analog.com/en/app/account ---//

        public MyAnalog_AccountSettings_LocationSection() : base() { }

        //--- Login Credentials ---//
        string username = null;
        string password = null;

        //--- URLs ---//
        string myAnalog_accountSettings_url = "app/account";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Elements are Present and Working as Expected for a User that has a Placed Order in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Elements are Present and Working as Expected for a User that has a Placed Order in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Elements are Present and Working as Expected for a User that has a Placed Order in JP Locale")]
        public void MyAnalog_AccountSettings_LocationSection_VerifyForUserThatHasPlacedOrder(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- Login Credentials ---//
            if (Locale.Equals("zh") || Locale.Equals("cn"))
            {
                username = "m3test.cn@gmail.com";
                password = "Test_1234";
            }
            else if (Locale.Equals("jp"))
            {
                if (Util.Configuration.Environment.Equals("production"))
                {
                    username = "marvin.bebe@analog.com";
                    password = "Test_1234";
                }
                else
                {
                    username = "myanalog_t3st_l0c4t10n_w1th_0rder_jp_555@mailinator.com";
                    password = "Test_1234";
                }
            }
            else
            {
                username = "marvin.bebe@analog.com";
                password = "Test_1234";
            }

            //--- myAnalog Account Settings URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_accountSettings_url;

            //--- Action: Open account settings page ---//
            action.Navigate(driver, myAnalog_url);

            action.ILogin(driver, username, password);
            Thread.Sleep(6000);

            //----- Account Settings Test Plan > myAnalog Dashboard Tab > R2 > T6: Verify when clicking Edit Address section (IQ-6555) -----//

            //--- Expected Result: Address section is displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_AccountSettings_Location_Section);

            //----- Account Settings Test Plan > myAnalog Dashboard Tab > R5 > T1/T3/T5: Required fields for English/Chinese/Japanese language -----//

            //--- Expected Result: The the Country/Region field is available and is required ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_AccountSettings_Location_Section_Required_CountryRegion_Dropdown);

            if (Locale.Equals("en"))
            {
                //----- Account Settings Test Plan > myAnalog Dashboard Tab > R5 > T2: Optional fields for English language (IQ-8917/AL-15745) -----//

                //--- Expected Result: The the Address Line 1 field is available and is optional---//
                test.validateElementIsNotPresent(driver, Elements.MyAnalog_AccountSettings_Location_Section_Required_AddressLine1_InputBox);

                //--- Expected Result: The the State field is available and is optional ---//
                test.validateElementIsNotPresent(driver, Elements.MyAnalog_AccountSettings_Location_Section_Required_State_Dropdown);

                //--- Expected Result: The the City field is available and is optional ---//
                test.validateElementIsNotPresent(driver, Elements.MyAnalog_AccountSettings_Location_Section_Required_City_InputBox);

                //--- Expected Result: The the Zip field is available and is optional ---//
                test.validateElementIsNotPresent(driver, Elements.MyAnalog_AccountSettings_Location_Section_Required_Zip_InputBox);
            }

            //----- Account Settings Test Plan > myAnalog Dashboard Tab > R5 > T2/T4/T6: Optional fields for English/Chinese/Japanese language (IQ-8917/AL-15745) -----//

            //--- Expected Result: The the Address Line 2 field is available and is optional ---//
            test.validateElementIsNotPresent(driver, Elements.MyAnalog_AccountSettings_Location_Section_Required_AddressLine2_InputBox);

            //----- Account Settings Test Plan > myAnalog Dashboard Tab > R2 > T5: Verify when clicking Edit Address section -----//

            //--- Action: Click Pencil icon top-right corner of  the section ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_AccountSettings_Location_Section_Edit_Button);

            //----- Account Settings Test Plan > myAnalog Dashboard Tab > R2 > T6: Verify when clicking Edit Address section (IQ-6555) -----//

            //--- Action: Click Pencil icon top-right corner of  the section ---//
            action.IClick(driver, Elements.MyAnalog_AccountSettings_Location_Section_Edit_Button);

            //--- Expected Result: Address ection enables editing mode ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_AccountSettings_Location_Section_Editing_Mode);

            //--- Expected Result: the pencil icon is replace with “save” and “cancel” options. ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_AccountSettings_Location_Section_Save_Button);
            test.validateElementIsPresent(driver, Elements.MyAnalog_AccountSettings_Location_Section_Cancel_Button);

            //----- Account Settings Test Plan > myAnalog Dashboard Tab > R2 > T6: Verify when clicking Edit Address section (IQ-6555) -----//

            //--- Expected Result: The Address Line 1 field is editable ---//
            test.validateElementIsEnabled(driver, Elements.MyAnalog_AccountSettings_Location_Section_AddressLine1_InputBox);

            //----- Account Settings Test Plan > myAnalog Dashboard Tab > R2 > T6.1: Verify Clear icon must only appear when the input has a value (IQ-6547/ AL-13367) -----//

            //--- Expected Result: Clear Icon must apear when the input has a value ---//
            if (!driver.FindElement(Elements.MyAnalog_AccountSettings_Location_Section_AddressLine1_InputBox).GetAttribute("value").Equals(""))
            {
                test.validateElementIsPresent(driver, Elements.MyAnalog_AccountSettings_Location_Section_AddressLine1_InputBox_X_Button);
            }

            //----- Account Settings Test Plan > myAnalog Dashboard Tab > R2 > T6: Verify when clicking Edit Address section (IQ-6555) -----//

            //--- Expected Result: The Address Line 2 field is editable ---//
            test.validateElementIsEnabled(driver, Elements.MyAnalog_AccountSettings_Location_Section_AddressLine2_InputBox);

            //----- Account Settings Test Plan > myAnalog Dashboard Tab > R2 > T6.1: Verify Clear icon must only appear when the input has a value (IQ-6547/ AL-13367) -----//

            //--- Expected Result: Clear Icon must apear when the input has a value ---//
            if (!driver.FindElement(Elements.MyAnalog_AccountSettings_Location_Section_AddressLine2_InputBox).GetAttribute("value").Equals(""))
            {
                test.validateElementIsPresent(driver, Elements.MyAnalog_AccountSettings_Location_Section_AddressLine2_InputBox_X_Button);
            }

            //----- Account Settings Test Plan > myAnalog Dashboard Tab > R2 > T6: Verify when clicking Edit Address section (IQ-6555) -----//

            //--- Expected Result: The City field is editable ---//
            test.validateElementIsEnabled(driver, Elements.MyAnalog_AccountSettings_Location_Section_City_InputBox);

            //----- Account Settings Test Plan > myAnalog Dashboard Tab > R2 > T6.1: Verify Clear icon must only appear when the input has a value (IQ-6547/ AL-13367) -----//

            //--- Expected Result: Clear Icon must apear when the input has a value ---//
            if (!driver.FindElement(Elements.MyAnalog_AccountSettings_Location_Section_City_InputBox).GetAttribute("value").Equals(""))
            {
                test.validateElementIsPresent(driver, Elements.MyAnalog_AccountSettings_Location_Section_City_InputBox_X_Button);
            }

            //----- Account Settings Test Plan > myAnalog Dashboard Tab > R2 > T6: Verify when clicking Edit Address section (IQ-6555) -----//

            //--- Expected Result: The Zip field is editable ---//
            test.validateElementIsEnabled(driver, Elements.MyAnalog_AccountSettings_Location_Section_Zip_InputBox);

            //----- Account Settings Test Plan > myAnalog Dashboard Tab > R2 > T6.1: Verify Clear icon must only appear when the input has a value (IQ-6547/ AL-13367) -----//

            //--- Expected Result: Clear Icon must apear when the input has a value ---//
            if (!driver.FindElement(Elements.MyAnalog_AccountSettings_Location_Section_Zip_InputBox).GetAttribute("value").Equals(""))
            {
                test.validateElementIsPresent(driver, Elements.MyAnalog_AccountSettings_Location_Section_Zip_InputBox_X_Button);
            }

            //----- Account Settings Test Plan > myAnalog Dashboard Tab > R2 > T6: Verify when clicking Edit Address section (IQ-6555) -----//

            //--- Expected Result: The State field is editable ---//
            test.validateElementIsEnabled(driver, Elements.MyAnalog_AccountSettings_Location_Section_State_Dropdown);

            //--- Expected Result: The Country/Region field is editable (note: user can edit this if no order transaction happened in the account used) ---//
            test.validateElementIsNotEnabled(driver, Elements.MyAnalog_AccountSettings_Location_Section_CountryRegion_Dropdown);
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Save Button is Present and Working for Valid Inputs as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Save Button is Present and Working for Valid Inputs as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Save Button is Present and Working for Valid Inputs as Expected in JP Locale")]
        public void MyAnalog_AccountSettings_LocationSection_VerifySaveButtonForValidInputs(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- Login Credentials ---//
            username = "myanalog_t3st_ch4ng3_l0c4t10n_555@mailinator.com";
            password = "Test_1234";

            //--- myAnalog Account Settings URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_accountSettings_url;

            //--- Action: Open account settings page ---//
            action.Navigate(driver, myAnalog_url);

            action.ILogin(driver, username, password);
            Thread.Sleep(4000);

            //----- Account Settings Test Plan > myAnalog Dashboard Tab > R4 > T3: Verify that the user will be able to update his/her account when all required and optional fields have been populated -----//
            //----- Precondition: Order has not been made yet) -----//

            //--- Action: Click edit icon to edit fifth section (Billing Address section) ---//
            action.IClick(driver, Elements.MyAnalog_AccountSettings_Location_Section_Edit_Button);

            //--- Action: Edit all fields with valid values ---//
            bool addressLine1_inputBox = false;
            string addressLine1_input = null;
            if (util.CheckElement(driver, Elements.MyAnalog_AccountSettings_Location_Section_AddressLine1_InputBox, 2))
            {
                addressLine1_inputBox = true;

                addressLine1_input = "addressLine1_" + util.GenerateRandomString();
                action.IDeleteValueOnFields(driver, Elements.MyAnalog_AccountSettings_Location_Section_AddressLine1_InputBox);
                action.IType(driver, Elements.MyAnalog_AccountSettings_Location_Section_AddressLine1_InputBox, addressLine1_input);
            }

            bool addressLine2_inputBox = false;
            string addressLine2_input = null;
            if (util.CheckElement(driver, Elements.MyAnalog_AccountSettings_Location_Section_AddressLine1_InputBox, 2))
            {
                addressLine2_inputBox = true;

                addressLine2_input = "addressLine2_" + util.GenerateRandomString();
                action.IDeleteValueOnFields(driver, Elements.MyAnalog_AccountSettings_Location_Section_AddressLine2_InputBox);
                action.IType(driver, Elements.MyAnalog_AccountSettings_Location_Section_AddressLine2_InputBox, addressLine2_input);
            }

            //bool countryRegion_dropdown = false;
            //string countryRegion_input = null;
            //if (util.CheckElement(driver, Elements.MyAnalog_AccountSettings_Location_Section_CountryRegion_Dropdown, 1) && driver.FindElement(Elements.MyAnalog_AccountSettings_Location_Section_CountryRegion_Dropdown).Enabled)
            //{
            //    countryRegion_dropdown = true;

            //    countryRegion_input = "UNITED STATES";
            //    action.ISelectFromDropdown(driver, Elements.Forms_CountryRegion_Dropdown, countryRegion_input);
            //}

            bool state_dropdown = false;
            string state_input = null;
            if (util.CheckElement(driver, Elements.MyAnalog_AccountSettings_Location_Section_State_Dropdown, 1) && driver.FindElement(Elements.MyAnalog_AccountSettings_Location_Section_State_Dropdown).Enabled)
            {
                state_dropdown = true;

                action.IClick(driver, Elements.MyAnalog_AccountSettings_Location_Section_State_Dropdown);

                int state_dropdown_value_count = util.GetCount(driver, Elements.MyAnalog_AccountSettings_Location_Section_State_Dropdown_Values);
                Random rand = new Random();
                int random_number = rand.Next(1, state_dropdown_value_count);

                state_input = util.GetText(driver, By.CssSelector("div[class='location fields']>div[class^='select']:nth-of-type(4)>div[class^='options']>div:nth-of-type(" + random_number + ")"));
                action.IClick(driver, By.CssSelector("div[class='location fields']>div[class^='select']:nth-of-type(4)>div[class^='options']>div:nth-of-type(" + random_number + ")"));
            }

            bool city_inputBox = false;
            string city_input = null;
            if (util.CheckElement(driver, Elements.MyAnalog_AccountSettings_Location_Section_City_InputBox, 2))
            {
                city_inputBox = true;

                city_input = "city_" + util.GenerateRandomString();
                action.IDeleteValueOnFields(driver, Elements.MyAnalog_AccountSettings_Location_Section_City_InputBox);
                action.IType(driver, Elements.MyAnalog_AccountSettings_Location_Section_City_InputBox, city_input);
            }

            bool zip_inputBox = false;
            string zip_input = null;
            if (util.CheckElement(driver, Elements.MyAnalog_AccountSettings_Location_Section_Zip_InputBox, 2))
            {
                zip_inputBox = true;

                zip_input = "12" + util.GenerateRandomNumber().ToString();
                action.IDeleteValueOnFields(driver, Elements.MyAnalog_AccountSettings_Location_Section_Zip_InputBox);
                action.IType(driver, Elements.MyAnalog_AccountSettings_Location_Section_Zip_InputBox, zip_input);
            }

            //--- Action: Click Save option ---//
            action.IClick(driver, Elements.MyAnalog_AccountSettings_Location_Section_Save_Button);

            //--- Expected Result: System successfully saved changes made ---//
            if (addressLine1_inputBox.Equals(true))
            {
                test.validateStringInstance(driver, addressLine1_input, util.GetInputBoxValue(driver, Elements.MyAnalog_AccountSettings_Location_Section_AddressLine1_InputBox));
            }

            if (addressLine2_inputBox.Equals(true))
            {
                test.validateStringInstance(driver, addressLine2_input, util.GetInputBoxValue(driver, Elements.MyAnalog_AccountSettings_Location_Section_AddressLine2_InputBox));
            }

            //if (countryRegion_dropdown.Equals(true))
            //{
            //    test.validateStringIsCorrect(driver, Elements.MyAnalog_AccountSettings_Location_Section_CountryRegion_Dropdown, countryRegion_input);
            //}

            if (state_dropdown.Equals(true))
            {
                test.validateStringIsCorrect(driver, Elements.MyAnalog_AccountSettings_Location_Section_State_Dropdown, state_input);
            }

            if (city_inputBox.Equals(true))
            {
                test.validateStringInstance(driver, city_input, util.GetInputBoxValue(driver, Elements.MyAnalog_AccountSettings_Location_Section_City_InputBox));
            }

            if (zip_inputBox.Equals(true))
            {
                test.validateStringInstance(driver, zip_input, util.GetInputBoxValue(driver, Elements.MyAnalog_AccountSettings_Location_Section_Zip_InputBox));
            }
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Cancel Button is Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Cancel Button is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Cancel Button is Present and Working as Expected in JP Locale")]
        public void MyAnalog_AccountSettings_LocationSection_VerifyCancelButton(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- Login Credentials ---//
            if (Locale.Equals("zh") || Locale.Equals("cn"))
            {
                username = "m3test.cn@gmail.com";
                password = "Test_1234";
            }
            else if (Locale.Equals("jp"))
            {
                if (Util.Configuration.Environment.Equals("production"))
                {
                    username = "marvin.bebe@analog.com";
                    password = "Test_1234";
                }
                else
                {
                    username = "myanalog_t3st_l0c4t10n_w1th_0rder_jp_555@mailinator.com";
                    password = "Test_1234";
                }
            }
            else
            {
                username = "marvin.bebe@analog.com";
                password = "Test_1234";
            }

            //--- myAnalog Account Settings URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_accountSettings_url;

            //--- Action: Open account settings page ---//
            action.Navigate(driver, myAnalog_url);

            action.ILogin(driver, username, password);
            Thread.Sleep(13000);

            //----- Account Settings Test Plan > myAnalog Dashboard Tab > R4 > T13: Verify that cancelling changes made will revert changes -----//

            //--- Action: Click the pencil icon to edit the sections for address ---//
            action.IClick(driver, Elements.MyAnalog_AccountSettings_Location_Section_Edit_Button);

            //--- Action: Edit all fields with valid values ---//
            bool addressLine1_inputBox = false;
            string addressLine1_default_value = null;
            string addressLine1_input = null;
            if (util.CheckElement(driver, Elements.MyAnalog_AccountSettings_Location_Section_AddressLine1_InputBox, 2))
            {
                addressLine1_inputBox = true;

                addressLine1_default_value = util.GetInputBoxValue(driver, Elements.MyAnalog_AccountSettings_Location_Section_AddressLine1_InputBox);

                addressLine1_input = "addressLine1_" + util.GenerateRandomString();
                action.IDeleteValueOnFields(driver, Elements.MyAnalog_AccountSettings_Location_Section_AddressLine1_InputBox);
                action.IType(driver, Elements.MyAnalog_AccountSettings_Location_Section_AddressLine1_InputBox, addressLine1_input);
            }

            bool addressLine2_inputBox = false;
            string addressLine2_default_value = null;
            string addressLine2_input = null;
            if (util.CheckElement(driver, Elements.MyAnalog_AccountSettings_Location_Section_AddressLine1_InputBox, 2))
            {
                addressLine2_inputBox = true;

                addressLine2_default_value = util.GetInputBoxValue(driver, Elements.MyAnalog_AccountSettings_Location_Section_AddressLine2_InputBox);

                addressLine2_input = "addressLine2_" + util.GenerateRandomString();
                action.IDeleteValueOnFields(driver, Elements.MyAnalog_AccountSettings_Location_Section_AddressLine2_InputBox);
                action.IType(driver, Elements.MyAnalog_AccountSettings_Location_Section_AddressLine2_InputBox, addressLine2_input);
            }

            bool countryRegion_dropdown = false;
            string countryRegion_default_value = null;
            string countryRegion_input = null;
            if (util.CheckElement(driver, Elements.MyAnalog_AccountSettings_Location_Section_CountryRegion_Dropdown, 1) && driver.FindElement(Elements.MyAnalog_AccountSettings_Location_Section_CountryRegion_Dropdown).Enabled)
            {
                countryRegion_dropdown = true;

                countryRegion_default_value = util.GetText(driver, Elements.MyAnalog_AccountSettings_Location_Section_CountryRegion_Dropdown);

                countryRegion_input = "UNITED STATES";
                action.ISelectFromDropdown(driver, Elements.Forms_CountryRegion_Dropdown, countryRegion_input);
            }

            bool state_dropdown = false;
            string state_default_value = null;
            string state_input = null;
            if (util.CheckElement(driver, Elements.MyAnalog_AccountSettings_Location_Section_State_Dropdown, 1) && driver.FindElement(Elements.MyAnalog_AccountSettings_Location_Section_State_Dropdown).Enabled)
            {
                state_dropdown = true;

                state_default_value = util.GetText(driver, Elements.MyAnalog_AccountSettings_Location_Section_State_Dropdown);

                action.IClick(driver, Elements.MyAnalog_AccountSettings_Location_Section_State_Dropdown);

                int state_dropdown_value_count = util.GetCount(driver, Elements.MyAnalog_AccountSettings_Location_Section_State_Dropdown_Values);
                Random rand = new Random();
                int random_number = rand.Next(1, state_dropdown_value_count);

                state_input = util.GetText(driver, By.CssSelector("div[class='location fields']>div[class^='select']:nth-of-type(4)>div[class^='options']>div:nth-of-type(" + random_number + ")"));
                action.IClick(driver, By.CssSelector("div[class='location fields']>div[class^='select']:nth-of-type(4)>div[class^='options']>div:nth-of-type(" + random_number + ")"));
            }

            bool city_inputBox = false;
            string city_default_value = null;
            string city_input = null;
            if (util.CheckElement(driver, Elements.MyAnalog_AccountSettings_Location_Section_City_InputBox, 2))
            {
                city_inputBox = true;

                city_default_value = util.GetInputBoxValue(driver, Elements.MyAnalog_AccountSettings_Location_Section_City_InputBox);

                city_input = "city_" + util.GenerateRandomString();
                action.IDeleteValueOnFields(driver, Elements.MyAnalog_AccountSettings_Location_Section_City_InputBox);
                action.IType(driver, Elements.MyAnalog_AccountSettings_Location_Section_City_InputBox, city_input);
            }

            bool zip_inputBox = false;
            string zip_default_value = null;
            string zip_input = null;
            if (util.CheckElement(driver, Elements.MyAnalog_AccountSettings_Location_Section_Zip_InputBox, 2))
            {
                zip_inputBox = true;

                zip_default_value = util.GetInputBoxValue(driver, Elements.MyAnalog_AccountSettings_Location_Section_Zip_InputBox);

                zip_input = "12" + util.GenerateRandomNumber().ToString();
                action.IDeleteValueOnFields(driver, Elements.MyAnalog_AccountSettings_Location_Section_Zip_InputBox);
                action.IType(driver, Elements.MyAnalog_AccountSettings_Location_Section_Zip_InputBox, zip_input);
            }

            //--- Action: Click Cancel option ---//
            action.IClick(driver, Elements.MyAnalog_AccountSettings_Location_Section_Cancel_Button);
            Thread.Sleep(2000);

            //--- Expected Result: System reverted all fields back to its original alues ---//
            if (addressLine1_inputBox.Equals(true))
            {
                test.validateStringInstance(driver, addressLine1_default_value, util.GetInputBoxValue(driver, Elements.MyAnalog_AccountSettings_Location_Section_AddressLine1_InputBox));
            }

            if (addressLine2_inputBox.Equals(true))
            {
                test.validateStringInstance(driver, addressLine2_default_value, util.GetInputBoxValue(driver, Elements.MyAnalog_AccountSettings_Location_Section_AddressLine2_InputBox));
            }

            if (countryRegion_dropdown.Equals(true))
            {
                test.validateStringIsCorrect(driver, Elements.MyAnalog_AccountSettings_Location_Section_CountryRegion_Dropdown, countryRegion_default_value);
            }

            if (state_dropdown.Equals(true))
            {
                test.validateStringIsCorrect(driver, Elements.MyAnalog_AccountSettings_Location_Section_State_Dropdown, state_default_value);
            }

            if (city_inputBox.Equals(true))
            {
                test.validateStringInstance(driver, city_default_value, util.GetInputBoxValue(driver, Elements.MyAnalog_AccountSettings_Location_Section_City_InputBox));
            }

            if (zip_inputBox.Equals(true))
            {
                test.validateStringInstance(driver, zip_default_value, util.GetInputBoxValue(driver, Elements.MyAnalog_AccountSettings_Location_Section_Zip_InputBox));
            }
        }
    }
}