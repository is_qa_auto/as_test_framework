﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System;
using System.Drawing;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_Products_EvaluationBoardsTab_RegisteredEvaluationBoardsTable_EllipsisButton : BaseSetUp
    {
        //--- myAnalog Products URL: ---//
        //--- DEV: https://my-dev.corpnt.analog.com/en/app/products ---//
        //--- QA: https://my-qa.corpnt.analog.com/en/app/products ---//
        //--- PROD: https://my.analog.com/en/app/products ---//

        public MyAnalog_Products_EvaluationBoardsTab_RegisteredEvaluationBoardsTable_EllipsisButton() : base() { }

        //--- Login Credentials ---//
        string username = "myanalog_ev4l_b04rds_test@mailinator.com";
        string password = "Test_1234";

        //--- URLs ---//
        string myAnalog_products_url = "app/products";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Edit serial number Option is Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Edit serial number Option is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Edit serial number Option is Present and Working as Expected in JP Locale")]
        public void MyAnalog_Products_EvaluationBoardsTab_RegisteredEvaluationBoardsTable_EllipsisButton_VerifyEditSerialNumberOption(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- Action: Set the Window to a Larger Size since the Ellipsis Buttons are being cut in Small Screens ---//
            driver.Manage().Window.Size = new Size(1920, 1080);

            //--- myAnalog Products URL ---//
            string myAnalog_url = $"{Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt")}{Locale}/{myAnalog_products_url}";

            //--- Action: Open myAnalog Products using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);

            //--- Action: Select the Evaluation Boards Tab ---//
            action.IClick(driver, Elements.MyAnalog_Products_EvaluationBoards_Tab);

            //----- Hardware Evalboard Registration > Test Case Title: Verify if the user can Edit serial number of registered boards. -----//

            //--- Action: Click the ellipsis in any listed boards. ---//
            action.IClick(driver, Elements.MyAnalog_Products_RegisteredEvaluationBoards_Ellipsis_Button);

            //--- Expected Result: Dropdown menu display ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Products_RegisteredEvaluationBoards_Options_Dropdown_Menu);

            //----- Hardware Evalboard Registration > Test Case Title: Verify the Option menu in Option dropdown. -----//

            //--- Expected Result: Edit serial number link displayed. ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Products_RegisteredEvaluationBoards_EditSerialNumber_Option);

            //----- Hardware Evalboard Registration > Test Case Title: Verify if the user can Edit serial number of registered boards. -----//

            //--- Action: Click Edit serial number link. ---//
            action.IClick(driver, Elements.MyAnalog_Products_RegisteredEvaluationBoards_EditSerialNumber_Option);

            //--- Expected Result: Serial number field of eval board become editable. ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Products_SerialNumber_InputBox);

            //--- Action: Change the serial number ---//
            string serialNumber_input = $"{"T3st_"}{Locale.ToUpper()}{"_SerialNumber_Input_"}{DateTime.Now.ToString("MMddyyyy")}{"_"}{util.RandomString()}";
            action.IDeleteValueOnFields(driver, Elements.MyAnalog_Products_SerialNumber_InputBox);
            action.IType(driver, Elements.MyAnalog_Products_SerialNumber_InputBox, serialNumber_input);

            //--- Action: Click Save button ---//
            action.IClick(driver, Elements.MyAnalog_Products_RegisteredEvaluationBoards_Save_Button);

            //--- Expected Result: Changed saved. ---//
            test.validateStringIsCorrect(driver, Elements.MyAnalog_Products_SerialNumber_Value_Labels, serialNumber_input);
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Edit product version Option is Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Edit product version Option is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Edit product version Option is Present and Working as Expected in JP Locale")]
        public void MyAnalog_Products_EvaluationBoardsTab_RegisteredEvaluationBoardsTable_EllipsisButton_VerifyEditProductVersionOption(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- Action: Set the Window to a Larger Size since the Ellipsis Buttons are being cut in Small Screens ---//
            driver.Manage().Window.Size = new Size(1920, 1080);

            //--- myAnalog Products URL ---//
            string myAnalog_url = $"{Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt")}{Locale}/{myAnalog_products_url}";

            //--- Action: Open myAnalog Products using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);

            //--- Action: Select the Evaluation Boards Tab ---//
            action.IClick(driver, Elements.MyAnalog_Products_EvaluationBoards_Tab);

            //----- Hardware Evalboard Registration > Test Case Title: Verify if the user can Edit product version of registered boards. -----//

            //--- Action: Click the ellipsis in any listed boards. ---//
            action.IClick(driver, Elements.MyAnalog_Products_RegisteredEvaluationBoards_Ellipsis_Button);

            //--- Expected Result: Dropdown menu display ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Products_RegisteredEvaluationBoards_Options_Dropdown_Menu);

            //----- Hardware Evalboard Registration > Test Case Title: Verify the Option menu in Option dropdown. -----//

            //--- Expected Result: Edit product version displayed. ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Products_RegisteredEvaluationBoards_EditProductVersion_Option);

            //----- Hardware Evalboard Registration > Test Case Title: Verify if the user can Edit product version of registered boards. -----//

            //--- Action: Click Edit product version link. ---//
            action.IClick(driver, Elements.MyAnalog_Products_RegisteredEvaluationBoards_EditProductVersion_Option);

            //--- Expected Result: Product version field of eval board become editable. ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Products_ProductVersion_InputBox);

            //--- Action: Change the product version ---//
            string productVersion_input = $"{"T3st_"}{Locale.ToUpper()}{"_ProductVersion_Input_"}{DateTime.Now.ToString("MMddyyyy")}{"_"}{util.RandomString()}";
            action.IDeleteValueOnFields(driver, Elements.MyAnalog_Products_ProductVersion_InputBox);
            action.IType(driver, Elements.MyAnalog_Products_ProductVersion_InputBox, productVersion_input);

            //--- Action: Click Save button ---//
            action.IClick(driver, Elements.MyAnalog_Products_RegisteredEvaluationBoards_Save_Button);

            //--- Expected Result: Changed saved. ---//
            test.validateStringIsCorrect(driver, Elements.MyAnalog_Products_ProductVersion_Value_Labels, productVersion_input);
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Copy Link Option is Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Copy Link Option is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Copy Link Option is Present and Working as Expected in JP Locale")]
        public void MyAnalog_Products_EvaluationBoardsTab_RegisteredEvaluationBoardsTable_EllipsisButton_VerifyCopyLinkOption(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- Action: Set the Window to a Larger Size since the Ellipsis Buttons are being cut in Small Screens ---//
            driver.Manage().Window.Size = new Size(1920, 1080);

            //--- myAnalog Products URL ---//
            string myAnalog_url = $"{Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt")}{Locale}/{myAnalog_products_url}";

            //--- Action: Open myAnalog Products using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);

            //--- Action: Select the Evaluation Boards Tab ---//
            action.IClick(driver, Elements.MyAnalog_Products_EvaluationBoards_Tab);

            //----- Hardware Evalboard Registration > Test Case Title: Verify if the user can copy the link by Clicking the "Copy link" link. -----//

            //--- Action: Get the Model Number Link ---//
            string modelNumber_link = util.ReturnAttribute(driver, Elements.MyAnalog_Products_ModelNumber_Links, "href");

            //--- Action: Click the ellipsis in any listed boards. ---//
            action.IClick(driver, Elements.MyAnalog_Products_RegisteredEvaluationBoards_Ellipsis_Button);

            //--- Expected Result: Dropdown menu display ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Products_RegisteredEvaluationBoards_Options_Dropdown_Menu);

            //----- Hardware Evalboard Registration > Test Case Title: Verify the Option menu in Option dropdown. -----//

            //--- Expected Result: Copy link link displayed. ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Products_RegisteredEvaluationBoards_CopyLink_Option);

            if (Locale.Equals("en"))
            {
                //----- Hardware Evalboard Registration > Test Case Title: Verify if the user can copy the link by Clicking the "Copy link" link. -----//

                //--- Action: Click "Copy link" ---//
                action.IClick(driver, Elements.MyAnalog_Products_RegisteredEvaluationBoards_CopyLink_Option);

                //--- Expected Result: The text changed to "Link copied" ---//
                test.validateStringIsCorrect(driver, Elements.MyAnalog_Products_RegisteredEvaluationBoards_CopyLink_Option, "Link Copied!");
            }
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Remove Option is Present and Working as Expected in EN Locale")]
        public void MyAnalog_Products_EvaluationBoardsTab_RegisteredEvaluationBoardsTable_EllipsisButton_VerifyRemoveOption(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- Action: Set the Window to a Larger Size since the Ellipsis Buttons are being cut in Small Screens ---//
            driver.Manage().Window.Size = new Size(1920, 1080);

            //--- Login Credentials ---//
            username = "myanalog_r3m0v3_ev4l_b04rds_test@mailinator.com";
            password = "Test_1234";

            //--- myAnalog Products URL ---//
            string myAnalog_url = $"{Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt")}{Locale}/{myAnalog_products_url}";

            //--- Action: Open myAnalog Products using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);

            //--- Action: Select the Evaluation Boards Tab ---//
            action.IClick(driver, Elements.MyAnalog_Products_EvaluationBoards_Tab);

            //--- Action: Save Evaluation Boards in myAnalog if there's none ---//
            if (!util.CheckElement(driver, Elements.MyAnalog_Products_RegisteredEvaluationBoards, 2))
            {
                string productNumber_input = "AD603-EVALZ";
                action.IRegisterAnEvalBoardInMyAnalog(driver, Locale, productNumber_input);
            }

            //----- Hardware Evalboard Registration > Test Case Title: Verify if the user can removed a registered boards. -----//

            //--- Action: Click the ellipsis in any listed boards. ---//
            action.IClick(driver, Elements.MyAnalog_Products_RegisteredEvaluationBoards_Ellipsis_Button);

            //--- Expected Result: Dropdown menu display ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Products_RegisteredEvaluationBoards_Options_Dropdown_Menu);

            //----- Hardware Evalboard Registration > Test Case Title: Verify the Option menu in Option dropdown. -----//

            //--- Expected Result: Remove link displayed. ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Products_RegisteredEvaluationBoards_Remove_Option);

            if (Locale.Equals("en"))
            {
                //----- Hardware Evalboard Registration > Test Case Title: Verify if the user can removed a registered boards. -----//

                //--- Action: Click Remove in dropdown menu ---//
                action.IClick(driver, Elements.MyAnalog_Products_RegisteredEvaluationBoards_Remove_Option);
                action.IClick(driver, Elements.MyAnalog_Products_RegisteredEvaluationBoards_Remove_Confirmation_Modal_Remove_Button);

                //--- Expected Result: Registered board removed. ---//
                test.validateElementIsNotPresent(driver, Elements.MyAnalog_Products_RegisteredEvaluationBoards);
            }
        }
    }
}