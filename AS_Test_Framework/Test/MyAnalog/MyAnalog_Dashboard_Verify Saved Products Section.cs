﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_Dashboard_SavedProductsSection : BaseSetUp
    {
        public MyAnalog_Dashboard_SavedProductsSection() : base() { }

        //-- Login Credentials --//
        string username = "myanalog_t3st_pr0ducts_555@mailinator.com";
        string password = "Test_1234";

        //--- URLs ---//
        string myAnalog_dashboard_url = "app";
        string myAnalog_products_url = "app/products";
        string pdp_url_format = "/products/";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Saved Products Label is Present and Working as Expected for Accounts with Saved Products in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Saved Products Label is Present and Working as Expected for Accounts with Saved Products in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Saved Products Label is Present and Working as Expected for Accounts with Saved Products in JP Locale")]
        public void MyAnalog_Dashboard_SavedProductsSection_AccountsWithSavedProducts_VerifySavedProductsLabel(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Products URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_products_url;

            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid user account in the myAnalog Login page ---//
            action.ILogin(driver, username, password);

            //--- Action: Get the Count for the Saved Products ---//
            int actual_savedProducts_count = util.GetMyAnalogSavedProductsCount(driver);

            //--- myAnalog Dashboard URL ---//
            myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_dashboard_url;

            //--- Action: Go to myAnalog dashboard ---//
            driver.Navigate().GoToUrl(myAnalog_url);
            Thread.Sleep(1000);
            Console.WriteLine("Go to: " + myAnalog_url);

            //----- Dashboard Test Plan > Desktop Tab > R4 > T1: Verify Your Saved Products display -----//

            //--- Expected Result: The Saved Products label is available in Saved Products section ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_SavedProducts_Label);

            if (util.CheckElement(driver, Elements.MyAnalog_Dashboard_SavedProducts_Label, 2))
            {
                //--- Expected Result: The Saved Products counter is available in Saved Products section ---//
                test.validateStringInstance(driver, util.ExtractNumber(driver, Elements.MyAnalog_Dashboard_SavedProducts_Label), actual_savedProducts_count.ToString());
            }
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the See all your products Link is Present and Working as Expected for Accounts with Saved Products in EN Locale")]
        [TestCase("zh", TestName = "Verify that the See all your products Link is Present and Working as Expected for Accounts with Saved Products in CN Locale")]
        [TestCase("jp", TestName = "Verify that the See all your products Link is Present and Working as Expected for Accounts with Saved Products in JP Locale")]
        public void MyAnalog_Dashboard_SavedProductsSection_AccountsWithSavedProducts_VerifySeeAllYourProductsLink(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Dashboard URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_dashboard_url;

            //--- Action: Go to myAnalog dashboard ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid user account in the myAnalog Login page ---//
            action.ILogin(driver, username, password);

            //----- Dashboard Test Plan > Desktop Tab > R4 > T1: Verify Your Saved Products display -----//

            //--- Expected Result: The See all your products link is available in Saved Products section ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_SeeAllYourProducts_Link);

            if (util.CheckElement(driver, Elements.MyAnalog_Dashboard_SeeAllYourProducts_Link, 2))
            {
                //----- Dashboard Test Plan > Desktop Tab > R4 > T4: Verify "See all your Products" link -----//

                //--- Action: Click "See all your Products" link ---//
                action.IOpenLinkInNewTab(driver, Elements.MyAnalog_Dashboard_SeeAllYourProducts_Link);
                Thread.Sleep(1000);

                //--- Expected Result: Page will redirect to myAnalog Products page ---//
                test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + "/" + myAnalog_products_url);
            }
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Product Tiles are Present and Working as Expected for Accounts with Saved Products in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Product Tiles are Present and Working as Expected for Accounts with Saved Products in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Product Tiles are Present and Working as Expected for Accounts with Saved Products in JP Locale")]
        public void MyAnalog_Dashboard_SavedProductsSection_AccountsWithSavedProducts_VerifyProductTiles(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Dashboard URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_dashboard_url;

            //--- Action: Go to myAnalog dashboard ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid user account in the myAnalog Login page ---//
            action.ILogin(driver, username, password);

            //----- Dashboard Test Plan > Desktop Tab > R4 > T1: Verify Your Saved Products display -----//

            //--- Expected Result: The Products tiles are available in Saved Products section ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_Product_Tiles);

            //----- Dashboard Test Plan > Desktop Tab > R4 > T5: Verify the maximum display of Product Tile -----//

            //--- Expected Result: 4 Project tile is displayed ---//
            test.validateCountIsLessOrEqual(driver, util.GetCount(driver, Elements.MyAnalog_Dashboard_Product_Tiles), 4);

            //----- Dashboard Test Plan > Desktop Tab > R4 > T2: Verify Product section Component -----//

            //--- Expected Result: zoom in and zoom out is displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_Product_Tiles_Zoom_Button);

            //--- Expected Result: image is displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_Product_Tiles_Image);

            //--- Expected Result: product status is displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_Product_Tiles_Status);

            //--- Expected Result: Product description is displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_Product_Tiles_Description_Text);

            //----- Dashboard Test Plan > Desktop Tab > R4 > T2: Verify Product section Component -----//

            //--- Expected Result: Product name is displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_Product_Tiles_Name_Link);

            if (util.CheckElement(driver, Elements.MyAnalog_Dashboard_Product_Tiles_Name_Link, 2))
            {
                //----- Dashboard Test Plan > Desktop Tab > R4 > T2.5: Verify when clicking Product link inside Product tile -----//

                //--- Action: Click Product link ---//
                action.IOpenLinkInNewTab(driver, Elements.MyAnalog_Dashboard_Product_Tiles_Name_Link);
                Thread.Sleep(5000);

                //--- Expected Result: Link will redirect to Product detail page ---//
                test.validateStringInstance(driver, driver.Url, pdp_url_format);

                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles.Last());
            }

            //----- ALTERNATIVE PARTS -----//

            if (util.CheckElement(driver, Elements.MyAnalog_Dashboard_SeeAlternativeParts_Links, 2))
            {
                //----- Dashboard Test Plan > Desktop Tab > R4 > T2.7: Verify See Alternative Parts inside Product Tile -----//
                string seeAlternativeParts_counter = util.ExtractNumber(driver, Elements.MyAnalog_Dashboard_SeeAlternativeParts_Links);

                //--- Action: Click See Alternative Parts inside Product Tile ---//
                action.IClick(driver, Elements.MyAnalog_Dashboard_SeeAlternativeParts_Links);

                //--- Expected Result: List of other products will expand ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_Expanded_AlternativeParts_Section);

                //----- Dashboard Test Plan > Desktop Tab > R4 > T2.7.1: Verify See Alternative Parts counter -----//

                //--- Expected Result: Counter should match to the number of Product available under Alternative Parts ---//
                string alternativeParts_count = util.GetCount(driver, Elements.MyAnalog_Dashboard_Expanded_AlternativeParts_Section_Part_Links).ToString();
                test.validateStringInstance(driver, seeAlternativeParts_counter, alternativeParts_count);

                if (util.CheckElement(driver, Elements.MyAnalog_Dashboard_Expanded_AlternativeParts_Section_Part_Links, 2))
                {
                    //----- Dashboard Test Plan > Desktop Tab > R4 > T2.7.4: Verify when clicking Product link inside Alternative Parts -----//

                    //--- Action: Click Product link ---//
                    action.IOpenLinkInNewTab(driver, Elements.MyAnalog_Dashboard_Expanded_AlternativeParts_Section_Part_Links);
                    Thread.Sleep(2000);

                    //--- Expected Result: Page will redirect to Product detail page ---//
                    test.validateStringInstance(driver, driver.Url, pdp_url_format);

                    driver.Close();
                    driver.SwitchTo().Window(driver.WindowHandles.Last());
                }

                //----- Dashboard Test Plan > Desktop Tab > R4 > T2.7.5: Verify when clicking arrow up -----//

                //--- Action: Click Arrow up ---//
                action.IClick(driver, Elements.MyAnalog_Dashboard_Collapse_AlternativeParts_Section_Button);

                //--- Expected Result: List of product is hidden ---//
                test.validateElementIsNotPresent(driver, Elements.MyAnalog_Dashboard_Expanded_AlternativeParts_Section);
            }
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Product Image Pop-up is Present and Working as Expected for Accounts with Saved Products in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Product Image Pop-up is Present and Working as Expected for Accounts with Saved Products in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Product Image Pop-up is Present and Working as Expected for Accounts with Saved Products in JP Locale")]
        public void MyAnalog_Dashboard_SavedProductsSection_AccountsWithSavedProducts_VerifyProductImagePopUp(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Dashboard URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_dashboard_url;

            //--- Action: Go to myAnalog dashboard ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid user account in the myAnalog Login page ---//
            action.ILogin(driver, username, password);

            //----- Dashboard Test Plan > Desktop Tab > R4 > T2.1: Verify the zoom in and zoom out functionality -----//

            //--- Action: Click zoom in zoom out ---//
            action.IClick(driver, Elements.MyAnalog_Dashboard_Product_Tiles_Zoom_Button);

            //--- Expected Result: Product images pop-up will displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_ProductImage_PopUp);

            //----- Dashboard Test Plan > Desktop Tab > R4 > T2.2: Verify Image pop-up -----//

            //--- Expected Result: The Image title is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_ProductImage_PopUp_Title);

            //--- Expected Result: The Download functionality is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_ProductImage_PopUp_Download_Button);

            //--- Expected Result: The Print Functionality is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_ProductImage_PopUp_Print_Button);

            //--- Expected Result: The Close icon is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_ProductImage_PopUp_X_Button);

            if (util.CheckElement(driver, Elements.MyAnalog_Dashboard_ProductImage_PopUp_X_Button, 2))
            {
                //----- Dashboard Test Plan > Desktop Tab > R4 > T2.2.3: Verify when clicking close icon on image pop-up -----//

                //--- Action: Click close icon ---//
                action.IClick(driver, Elements.MyAnalog_Dashboard_ProductImage_PopUp_X_Button);

                //--- Expected Result: Image pop-up is set to close ---//
                test.validateElementIsNotPresent(driver, Elements.MyAnalog_Dashboard_ProductImage_PopUp);
            }
        }
    }
}