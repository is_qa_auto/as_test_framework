﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System;
using System.Threading;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_Subscriptions_SaveChangesButton : BaseSetUp
    {
        //--- myAnalog Subscriptions URL: ---//
        //--- DEV: https://my-dev.corpnt.analog.com/en/app/subscriptions ---//
        //--- QA: https://my-qa.corpnt.analog.com/en/app/subscriptions ---//
        //--- PROD: https://my.analog.com/en/app/subscriptions ---//

        public MyAnalog_Subscriptions_SaveChangesButton() : base() { }

        //--- Login Credentials ---//
        string username = "myanalog_t3st_subscripti0ns_555@mailinator.com";
        string password = "Test_1234";

        //--- URLs ---//
        string myAnalog_subscriptions_url = "app/subscriptions";

        //--- Labels ---//
        string email_subject = "myAnalog: Analog Devices eNewsletter Subscription Status";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Save Changes Button is Present and Working as Expected after Selecting HTML in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Save Changes Button is Present and Working as Expected after Selecting HTML in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Save Changes Button is Present and Working as Expected after Selecting HTML in JP Locale")]
        public void MyAnalog_Subscriptions_SaveChangesButton_VerifyAfterSelectingHtml(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Subscriptions URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_subscriptions_url;

            //--- Action: Open subscription page ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);

            //----- Subscription - Dashboard > Desktop Tab > R3 > T1.1: Verify myAnalog Updates Tile when user country account is not China/Taiwan or Japan (eg. US) (IQ-6363/AL-13521) -----//

            //--- Action: Select english ---//
            string language_dropdown_input = null;
            if (Locale.Equals("zh") || Locale.Equals("cn"))
            {
                language_dropdown_input = "英语";
            }
            else
            {
                language_dropdown_input = "English";
            }
            action.IClick(driver, Elements.MyAnalog_Subscriptions_Tiles_Locale_Dropdown);
            action.IClick(driver, Elements.MyAnalog_Subscriptions_Tiles_Locale_Dropdown_English_Value);

            //--- Action: Select html radio button ---//
            action.IClick(driver, Elements.MyAnalog_Subscriptions_Tiles_Html_RadioButton);

            //--- Action: Click save changes button ---//
            action.IClick(driver, Elements.MyAnalog_Subscriptions_SaveChanges_Button);
            Thread.Sleep(5000);

            //--- Expected Result: subscription to myAnalog Updates Tile saves successfully ---//
            test.validateStringIsCorrect(driver, Elements.MyAnalog_Subscriptions_Tiles_Locale_Dropdown, language_dropdown_input);
            test.validateSelectedRadioButtonIsCorrect(driver, Elements.MyAnalog_Subscriptions_Tiles_Html_RadioButton, true);

            //----- Subscription - Dashboard > Desktop Tab > R4 > T1: Verify subscription email when subscribed to myAnalog Updates -----//

            //--- Action: Go to the Mailbox of the User ---//
            action.ICheckMailinatorEmail(driver, username);

            //--- Expected Result: Email is received by the end user (IQ-6114, IQ-7745/AL-10307): ---//
            //--- Subject: 'myAnalog: Analog Devices eNewsletter Subscription Status' ---//
            test.validateStringInstance(driver, util.GetText(driver, Elements.Mailinator_LatestEmail_Subj), email_subject);
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Save Changes Button is Present and Working as Expected after Selecting Text in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Save Changes Button is Present and Working as Expected after Selecting Text in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Save Changes Button is Present and Working as Expected after Selecting Text in JP Locale")]
        public void MyAnalog_Subscriptions_SaveChangesButton_VerifyAfterSelectingText(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Subscriptions URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_subscriptions_url;

            //--- Action: Open subscription page ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);

            //----- Subscription - Dashboard > Desktop Tab > R3 > T1.1: Verify myAnalog Updates Tile when user country account is not China/Taiwan or Japan (eg. US) (IQ-6363/AL-13521) -----//

            //--- Action: Select english ---//
            string language_dropdown_input = null;
            if (Locale.Equals("zh") || Locale.Equals("cn"))
            {
                language_dropdown_input = "英语";
            }
            else
            {
                language_dropdown_input = "English";
            }
            action.IClick(driver, Elements.MyAnalog_Subscriptions_Tiles_Locale_Dropdown);
            action.IClick(driver, Elements.MyAnalog_Subscriptions_Tiles_Locale_Dropdown_English_Value);

            //--- Action: Select text radio button ---//
            action.IClick(driver, Elements.MyAnalog_Subscriptions_Tiles_Text_RadioButton);

            //--- Action: Click save changes button ---//
            action.IClick(driver, Elements.MyAnalog_Subscriptions_SaveChanges_Button);
            Thread.Sleep(5000);

            //--- Expected Result: subscription to myAnalog Updates Tile saves successfully ---//
            test.validateStringIsCorrect(driver, Elements.MyAnalog_Subscriptions_Tiles_Locale_Dropdown, language_dropdown_input);
            test.validateSelectedRadioButtonIsCorrect(driver, Elements.MyAnalog_Subscriptions_Tiles_Text_RadioButton, true);

            //----- Subscription - Dashboard > Desktop Tab > R4 > T15: Verify email notification received for text -----//

            //--- Action: Go to the Mailbox of the User ---//
            action.ICheckMailinatorEmail(driver, username);

            //--- Expected Result: Eemail notification received ---//
            test.validateElementIsPresent(driver, Elements.Mailinator_LatestEmail);
        }
    }
}