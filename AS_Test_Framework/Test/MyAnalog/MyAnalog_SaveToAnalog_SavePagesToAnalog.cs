﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using OpenQA.Selenium;
using System;
using System.Threading;
using System.Linq;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_SaveToAnalog_SavePagesToAnalog : BaseSetUp
    {
        public MyAnalog_SaveToAnalog_SavePagesToAnalog() : base() { }

        private string URL = "/landing-pages/001/ahead-of-whats-possible.html";
        private string Username = "ISSQA_Automated_Test@analog.com";
        private string password = "Test_1234";

        [Test, Category("MyAnalog"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that user can save AWP Page in Save To Analog for EN Locale")]
        public void MyAnalog_SaveToAnalog_VerifySaveToAnalogInAWP(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            action.Navigate(driver, Configuration.Env_Url + Locale + URL);
            action.IClick(driver, Elements.MyAnalog_Widget_Btn);
            //action.IClick(driver, Elements.MyAnalog_Widget_LogInToMyAnalog_Btn);
            action.ILogin(driver, Username, password);
            //action.IClick(driver, Elements.MyAnalog_Widget_Btn);
            action.IClick(driver, Elements.MyAnalog_Widget_CreateNewProject_Tab);
            string ProjectName = "Test_Project_" + util.GenerateRandomNumber();
            action.IType(driver, Elements.MyAnalog_Widget_NewProjectTitle_TextField, ProjectName);
            action.IClick(driver, Elements.MyAnalog_Widget_AddDescription_Button);
            action.IType(driver, Elements.MyAnalog_Widget_AddDescription_TextArea, "Test_Project_Description_" + util.GenerateRandomNumber());
            action.IClick(driver, Elements.MyAnalog_Widget_Save_Button);
            test.validateStringIsCorrect(driver, Elements.MyAnalog_Widget_SaveToAnalog_Success, "Resource saved to \"" + ProjectName + "\"");

            /*****validate IQ-8911****/
            driver.Navigate().Refresh();
            //action.IClick(driver, Elements.MyAnalog_Widget_Btn);
            action.IClick(driver, Elements.MyAnalog_Widget_CreateNewProject_Tab);
            action.IType(driver, Elements.MyAnalog_Widget_NewProjectTitle_TextField, ProjectName);
            action.IClick(driver, Elements.MyAnalog_Widget_Save_Button);
            test.validateStringIsCorrect(driver, Elements.MyAnalog_Widget_DuplicateProject_Validate_Message, "Please enter a different project name.");
        }

        [Test, Category("MyAnalog"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that user can save About ADI Page in Save To Analog for EN Locale")]
        public void MyAnalog_SaveToAnalog_VerifySaveToAnalogInAboutADI(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            action.Navigate(driver, Configuration.Env_Url + Locale + "/about-adi.html");
            action.IClick(driver, Elements.MyAnalog_Widget_Btn);
            //action.IClick(driver, Elements.MyAnalog_Widget_LogInToMyAnalog_Btn);
            action.ILogin(driver, Username, password);
            //action.IClick(driver, Elements.MyAnalog_Widget_Btn);
            action.IClick(driver, Elements.MyAnalog_Widget_Save_Button);
            action.IClick(driver, Elements.MyAnalog_Widget_ViewSavedLink);
            driver.SwitchTo().Window(driver.WindowHandles.Last());
            test.validateStringIsCorrect(driver, By.CssSelector("table[class='editable table'] tr:nth-child(1)>td:nth-child(2)>span"), "About ADI");

            /****Remove Save AboutADI***/
            action.IClick(driver, By.CssSelector("table[class='editable table'] tr:nth-child(1)>td:nth-child(4) button"));
            action.IClick(driver, By.CssSelector("menu[class='options dropdown']>button:nth-child(4)"));
            action.IClick(driver, By.CssSelector("div[class='modal fade in'] button[class='ma btn btn-primary']"));
        }

        [Test, Category("MyAnalog"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that user can save Alliance Page in Save To Analog for EN Locale")]
        public void MyAnalog_SaveToAnalog_VerifySaveToAnalogInAlliance(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            action.Navigate(driver, Configuration.Env_Url + Locale + "/about-adi/alliances.html");
            action.IClick(driver, Elements.MyAnalog_Widget_Btn);
            //action.IClick(driver, Elements.MyAnalog_Widget_LogInToMyAnalog_Btn);
            action.ILogin(driver, Username, password);
            //action.IClick(driver, Elements.MyAnalog_Widget_Btn);
            action.IClick(driver, Elements.MyAnalog_Widget_Save_Button);
            test.validateStringIsCorrect(driver, Elements.MyAnalog_Widget_SaveToAnalog_Success, "Resource saved to \"myAnalog\"");

            /****Remove Save Alliance***/
            action.IClick(driver, Elements.MyAnalog_Widget_ViewSavedLink);
            driver.SwitchTo().Window(driver.WindowHandles.Last());
            action.IClick(driver, By.CssSelector("table[class='editable table'] tr:nth-child(1)>td:nth-child(4) button"));
            action.IClick(driver, By.CssSelector("menu[class='options dropdown']>button:nth-child(4)"));
            action.IClick(driver, By.CssSelector("div[class='modal fade in'] button[class='ma btn btn-primary']"));
        }

        [Test, Category("MyAnalog"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that user can save Terms and Condition Page in Save To Analog for EN Locale")]
        public void MyAnalog_SaveToAnalog_VerifySaveToAnalogInSupport(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            action.Navigate(driver, Configuration.Env_Url + Locale + "/support/customer-service-resources/sales/terms-and-conditions.html");
            action.IClick(driver, Elements.MyAnalog_Widget_Btn);
            //action.IClick(driver, Elements.MyAnalog_Widget_LogInToMyAnalog_Btn);
            action.ILogin(driver, Username, password);
            //action.IClick(driver, Elements.MyAnalog_Widget_Btn);
            test.validateStringIsCorrect(driver, Elements.MyAnalog_Widget_Save_Button, "Save");
            test.validateStringIsCorrect(driver, Elements.MyAnalog_Widget_SaveToProject_Label, "* Save To");
            action.IClick(driver, Elements.MyAnalog_Widget_Save_Button);
            test.validateStringIsCorrect(driver, Elements.MyAnalog_Widget_SaveToAnalog_Success, "Saved to \"myAnalog\"");
            action.IClick(driver, Elements.MyAnalog_Widget_ViewSavedLink);
            driver.SwitchTo().Window(driver.WindowHandles.Last());
            test.validateStringIsCorrect(driver, By.CssSelector("table[class='editable table'] tr:nth-child(1)>td:nth-child(2)>span"), "Support");

            /****Remove Save AboutADI***/
            action.IClick(driver, By.CssSelector("table[class='editable table'] tr:nth-child(1)>td:nth-child(4) button"));
            action.IClick(driver, By.CssSelector("menu[class='options dropdown']>button:nth-child(4)"));
            action.IClick(driver, By.CssSelector("div[class='modal fade in'] button[class='ma btn btn-primary']"));
        }

        [Test, Category("MyAnalog"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that user can save Design Center Page in Save To Analog for EN Locale")]
        public void MyAnalog_SaveToAnalog_VerifySaveToAnalogInDesignCenter(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            action.Navigate(driver, Configuration.Env_Url + Locale + "/design-center/reference-designs/drivers-reference-code.html");
            action.IClick(driver, Elements.MyAnalog_Widget_Btn);
            //action.IClick(driver, Elements.MyAnalog_Widget_LogInToMyAnalog_Btn);
            action.ILogin(driver, Username, password);
            //action.IClick(driver, Elements.MyAnalog_Widget_Btn);
            action.IClick(driver, Elements.MyAnalog_Widget_Save_Button);
            test.validateStringIsCorrect(driver, Elements.MyAnalog_Widget_SaveToAnalog_Success, "Saved to \"myAnalog\"");
            test.validateElementIsPresent(driver, Elements.MyAnalog_Widget_ViewSavedLink);
            /****Remove Save Alliance***/
            action.IClick(driver, Elements.MyAnalog_Widget_ViewSavedLink);
            driver.SwitchTo().Window(driver.WindowHandles.Last());
            action.IClick(driver, By.CssSelector("table[class='editable table'] tr:nth-child(1)>td:nth-child(4) button"));
            action.IClick(driver, By.CssSelector("menu[class='options dropdown']>button:nth-child(4)"));
            action.IClick(driver, By.CssSelector("div[class='modal fade in'] button[class='ma btn btn-primary']"));
        }

        [Test, Category("MyAnalog"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that user can save Evaluation Board Page in Save To Analog for EN Locale")]
        public void MyAnalog_SaveToAnalog_VerifySaveToAnalogInEvalBoard(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            action.Navigate(driver, Configuration.Env_Url + Locale + "/design-center/evaluation-hardware-and-software/evaluation-boards-kits/EVAL-HMC897LP4E.html");
            action.IClick(driver, Elements.MyAnalog_Widget_Btn);
            //action.IClick(driver, Elements.MyAnalog_Widget_LogInToMyAnalog_Btn);
            action.ILogin(driver, Username, password);
            //action.IClick(driver, Elements.MyAnalog_Widget_Btn);
            action.IClick(driver, Elements.MyAnalog_Widget_Save_Button);
            test.validateStringIsCorrect(driver, Elements.MyAnalog_Widget_SaveToAnalog_Success, "Saved to \"myAnalog\"");
            test.validateElementIsPresent(driver, Elements.MyAnalog_Widget_ViewSavedLink);
            /****Remove Save Alliance***/
            action.IClick(driver, Elements.MyAnalog_Widget_ViewSavedLink);
            driver.SwitchTo().Window(driver.WindowHandles.Last());
            action.IClick(driver, By.CssSelector("table[class='editable table'] tr:nth-child(1)>td:nth-child(4) button"));
            action.IClick(driver, By.CssSelector("menu[class='options dropdown']>button:nth-child(4)"));
            action.IClick(driver, By.CssSelector("div[class='modal fade in'] button[class='ma btn btn-primary']"));
        }

        [Test, Category("MyAnalog"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that user can save Education Page in Save To Analog for EN Locale")]
        public void MyAnalog_SaveToAnalog_VerifySaveToAnalogInEducation(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            action.Navigate(driver, Configuration.Env_Url + Locale + "/education/education-library/tutorials.html");
            action.IClick(driver, Elements.MyAnalog_Widget_Btn);
            //action.IClick(driver, Elements.MyAnalog_Widget_LogInToMyAnalog_Btn);
            action.ILogin(driver, Username, password);
            //action.IClick(driver, Elements.MyAnalog_Widget_Btn);
            action.IClick(driver, Elements.MyAnalog_Widget_Save_Button);
            test.validateStringIsCorrect(driver, Elements.MyAnalog_Widget_SaveToAnalog_Success, "Resource saved to \"myAnalog\"");
            test.validateElementIsPresent(driver, Elements.MyAnalog_Widget_ViewSavedLink);
            /****Remove Save Alliance***/
            action.IClick(driver, Elements.MyAnalog_Widget_ViewSavedLink);
            driver.SwitchTo().Window(driver.WindowHandles.Last());
            action.IClick(driver, By.CssSelector("table[class='editable table'] tr:nth-child(1)>td:nth-child(4) button"));
            action.IClick(driver, By.CssSelector("menu[class='options dropdown']>button:nth-child(4)"));
            action.IClick(driver, By.CssSelector("div[class='modal fade in'] button[class='ma btn btn-primary']"));
        }

        [Test, Category("MyAnalog"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that user can save PST Page in Save To Analog for EN Locale")]
        public void MyAnalog_SaveToAnalog_VerifySaveToAnalogInPST(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            action.Navigate(driver, Configuration.Env_Url + Locale + "/parametricsearch/11106");

            action.ILoginViaMyAnalogMegaMenu(driver, Username, password);

            action.IClick(driver, Elements.MainNavigation_MyAnalogTab);
            //--- Action: Click the Save to myAnalog Button ---//
            action.IClick(driver, Elements.PST_SaveToMyAnalog_Button);

            test.validateStringIsCorrect(driver, Elements.MyAnalog_Widget_SaveToProject_Label, "* Save All Models to:");
            test.validateElementIsPresent(driver, Elements.MyAnalog_Widget_SaveToProject_Dropdown);

            //--- Action: Expand the Save All Models to Dropdown ---//
            action.IClick(driver, Elements.MyAnalog_Widget_SaveToProject_Dropdown);
            test.validateElementIsPresent(driver, By.CssSelector("div[class='select-product dropdown open']>ul"));

            //--- Action: Collapse the Save All Models to Dropdown ---//
            action.IClick(driver, By.CssSelector("div[id='saveToWidget']>div[class='select-product dropdown open']>div"));

            test.validateStringIsCorrect(driver, Elements.MyAnalog_Widget_TableName_Label, "* Table Name");
            test.validateElementIsPresent(driver, Elements.MyAnalog_Widget_TableName_TextField);
            test.validateElementIsPresent(driver, Elements.MyAnalog_Widget_AddNote_Button);

            //--- Action: Click the Add Note Button ---//
            action.IClick(driver, Elements.MyAnalog_Widget_AddNote_Button);
            test.validateElementIsPresent(driver, Elements.MyAnalog_Widget_AddNote_Textarea);
            test.validateElementIsPresent(driver, Elements.MyAnalog_Widget_Save_Button);
            test.validateStringIsCorrect(driver, Elements.MyAnalog_Widget_Save_Button, "Save PST");

            //--- Action: Get the Value in Table Name ---//
            string tableName_value = util.GetInputBoxValue(driver, Elements.MyAnalog_Widget_TableName_TextField);

            //--- Action: Delete the Value in Table Name ---//
            action.IDeleteValueOnFieldUsingBackSpace(driver, Elements.MyAnalog_Widget_TableName_TextField, util.ReturnAttribute(driver, Elements.MyAnalog_Widget_TableName_TextField, "value"));
            Console.WriteLine("Delete the value in Table Name.");

            test.validateElementIsPresent(driver, By.CssSelector("div[class='widget-footer single btn'] button[class='btn btn-primary pst disabled']"));

            //--- Action: Return the Value in Table Name ---//
            action.IDeleteValueOnFields(driver, Elements.MyAnalog_Widget_TableName_TextField);
            action.IType(driver, Elements.MyAnalog_Widget_TableName_TextField, tableName_value);

            //--- Action: Click the Save PST Button ---//
            action.IClick(driver, Elements.MyAnalog_Widget_Save_Button);
            Thread.Sleep(2000);

            test.validateStringIsCorrect(driver, Elements.MyAnalog_Widget_SaveToAnalog_Success, "Table Saved to \"myAnalog\"");
            test.validateElementIsPresent(driver, Elements.MyAnalog_Widget_ViewSavedLink);

            /****Remove Saved PST***/

            //--- Action: Click the View my saved PST Link ---//
            action.IClick(driver, Elements.MyAnalog_Widget_ViewSavedLink);
            driver.SwitchTo().Window(driver.WindowHandles.Last());

            //--- Action: Click the Ellipsis ---//
            action.IClick(driver, Elements.MyAnalog_ParametricSearches_Saved_ParametricSearches_Ellipsis_Button);

            //--- Action: Click the Remove Button ---//
            action.IClick(driver, Elements.MyAnalog_ParametricSearches_Saved_ParametricSearches_Remove_Option);

            //--- Action: Click the Remove Button ---//
            action.IClick(driver, Elements.MyAnalog_ParametricSearches_Remove_Confirmation_Modal_Remove_Button);
        }

        [Test, Category("MyAnalog"), Category("NonCore")]
        [TestCase("en", TestName = "Verify that user can save PST Page by creating new myAnalog project for EN Locale")]
        public void MyAnalog_SaveToAnalog_VerifyCreateNewProjectInPST(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            action.Navigate(driver, Configuration.Env_Url + Locale + "/parametricsearch/11106");

            action.ILoginViaMyAnalogMegaMenu(driver, Username, password);

            action.IClick(driver, Elements.MainNavigation_MyAnalogTab);
            //--- Action: Click the Save to myAnalog Button ---//
            action.IClick(driver, Elements.PST_SaveToMyAnalog_Button);

            //--- Action: Click the New Project Tab ---//       
            action.IClick(driver, Elements.MyAnalog_Widget_CreateNewProject_Tab);

            test.validateStringIsCorrect(driver, Elements.MyAnalog_Widget_CreateNewProject_Label, "* Create a New Project");
            test.validateElementIsPresent(driver, Elements.MyAnalog_Widget_NewProjectTitle_TextField);
            test.validateElementIsPresent(driver, Elements.MyAnalog_Widget_AddDescription_Button);

            //--- Action: Click the Add Description Button ---//
            action.IClick(driver, Elements.MyAnalog_Widget_AddDescription_Button);
            test.validateElementIsPresent(driver, Elements.MyAnalog_Widget_AddDescription_TextArea);

            test.validateStringIsCorrect(driver, Elements.MyAnalog_Widget_TableName_Label, "* Table Name");
            test.validateElementIsPresent(driver, Elements.MyAnalog_Widget_TableName_TextField);
            test.validateElementIsPresent(driver, Elements.MyAnalog_Widget_AddNote_Button);

            //--- Action: Click the Add Note Button ---//
            action.IClick(driver, Elements.MyAnalog_Widget_AddNote_Button);
            test.validateElementIsPresent(driver, Elements.MyAnalog_Widget_AddNote_Textarea);

            test.validateElementIsPresent(driver, By.CssSelector("div[class='widget-footer single btn'] button[class='btn btn-primary pst disabled']"));

            //--- Action: Enter a Project Name ---//
            string ProjectName = "Test_Project_" + util.GenerateRandomNumber();
            action.IType(driver, Elements.MyAnalog_Widget_NewProjectTitle_TextField, ProjectName);

            test.validateElementIsPresent(driver, Elements.MyAnalog_Widget_Save_Button);

            //--- Action: Get the Value in Table Name ---//
            string tableName_value = util.GetInputBoxValue(driver, Elements.MyAnalog_Widget_TableName_TextField);

            //--- Action: Delete the Value in Table Name ---//
            action.IDeleteValueOnFieldUsingBackSpace(driver, Elements.MyAnalog_Widget_TableName_TextField, util.ReturnAttribute(driver, Elements.MyAnalog_Widget_TableName_TextField, "value"));
            Console.WriteLine("Delete the value in Table Name.");

            test.validateElementIsPresent(driver, By.CssSelector("div[class='widget-footer single btn'] button[class='btn btn-primary pst disabled']"));

            //--- Action: Return the Value in Table Name ---//
            action.IDeleteValueOnFields(driver, Elements.MyAnalog_Widget_TableName_TextField);
            action.IType(driver, Elements.MyAnalog_Widget_TableName_TextField, tableName_value);

            //--- Action: Click the Save PST Button ---//
            action.IClick(driver, Elements.MyAnalog_Widget_Save_Button);
            Thread.Sleep(2000);

            test.validateStringIsCorrect(driver, Elements.MyAnalog_Widget_SaveToAnalog_Success, "Table Saved to \"" + ProjectName + "\"");
        }

        [Test, Category("MyAnalog"), Category("NonCore")]
        [TestCase("en", TestName = "Verify if PCN/PDN notification should not be available on save to a new or existing project in PDP page.")]
        public void MyAnalog_SaveToAnalog_VerifyCreateNewProjectInPDP(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            action.Navigate(driver, Configuration.Env_Url + Locale + "/AD7960");

            action.ILoginViaMyAnalogMegaMenu(driver, Username, password);
            action.IClick(driver, Elements.MainNavigation_MyAnalogTab);
            action.IClick(driver, Elements.MyAnalog_Widget_Btn);

            action.IClick(driver, Elements.MyAnalog_Widget_CreateNewProject_Tab);
            string ProjectName = "Test_Project_" + util.GenerateRandomNumber();

            action.IType(driver, Elements.MyAnalog_Widget_NewProjectTitle_TextField, ProjectName);

            action.IClick(driver, Elements.MyAnalog_Widget_Save_Button);
            Thread.Sleep(2000);
            test.validateElementIsNotPresent(driver, Elements.MyAnalog_Widget_PCNPDN_Notification);

            /*****for saving in existing project****/
            driver.Navigate().GoToUrl(Configuration.Env_Url + Locale + "/ADL5580");
            Console.WriteLine("Go to: " + Configuration.Env_Url + Locale + "/ADL5580");

            action.IClick(driver, Elements.MyAnalog_Widget_Btn);
            action.IClick(driver, Elements.MyAnalog_Widget_SaveToProject_Dropdown);
            action.IClick(driver, By.CssSelector("div[class='select-product dropdown open']>ul>li:nth-child(2)"));
            action.IClick(driver, Elements.MyAnalog_Widget_Save_Button);
            Thread.Sleep(2000);
            test.validateElementIsNotPresent(driver, Elements.MyAnalog_Widget_PCNPDN_Notification);
        }
    }
}