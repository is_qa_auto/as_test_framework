﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using OpenQA.Selenium;
using System;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_Projects_ProjectDetailPage_InputValidations : BaseSetUp
    {
        //--- myAnalog Projects URL: ---//
        //--- DEV: https://my-dev.corpnt.analog.com/en/app/projects ---//
        //--- QA: https://my-qa.corpnt.analog.com/en/app/projects ---//
        //--- PROD: https://my.analog.com/en/app/projects ---//

        public MyAnalog_Projects_ProjectDetailPage_InputValidations() : base() { }

        //----- Note: Please don't edit/delete any details on the Accounts below -----//

        //--- Login Credentials (Project Owner) ---// 
        string username = "my_t3st_pr0j3ct_1nv1t3_555@mailinator.com";
        string password = "Test_1234";

        //--- URLs ---//
        string myAnalog_projects_url = "app/projects";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Input Validation for the Email Address of Current User is Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Input Validation for the Email Address of Current User is Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Input Validation for the Email Address of Current User is Working as Expected in JP Locale")]
        public void MyAnalog_Projects_ProjectDetailPage_InputValidations_VerifyForTheEmailAddressOfCurrentUser(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Projects URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_projects_url;

            //--- Action: Open myAnalog Projects using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);

            //--- Action: Click Project Detail button ---//
            action.IClick(driver, Elements.MyAnalog_Projects_ProjectDetails_Buttons);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify sending invite to current user's email addresss (IQ-8723/AL-14182) -----//

            //--- Action: Click Invite button ---//
            action.IClick(driver, Elements.MyAnalog_ProjectDetailPage_Invite_Button);

            //--- Action: Enter current user's email address ---//
            string invitees_input = username;
            action.IDeleteValueOnFields(driver, Elements.MyAnalog_Projects_Invite_Modal_Invitees_InputBox);
            action.IType(driver, Elements.MyAnalog_Projects_Invite_Modal_Invitees_InputBox, invitees_input);
            action.IType(driver, Elements.MyAnalog_Projects_Invite_Modal_Invitees_InputBox, Keys.Tab);

            //--- Expected Result: Error message is displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Projects_Invite_Modal_Invitees_Error_Message);
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Input Validation for More Than 10 Invitees is Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Input Validation for More Than 10 Invitees is Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Input Validation for More Than 10 Invitees is Working as Expected in JP Locale")]
        public void MyAnalog_Projects_ProjectDetailPage_InputValidations_VerifyForMoreThan10Invitees(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Projects URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_projects_url;

            //--- Action: Open myAnalog Projects using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);

            //--- Action: Click Project Detail button ---//
            action.IClick(driver, Elements.MyAnalog_Projects_ProjectDetails_Buttons);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify Limit of invites on Project (IQ-6795/AL-14306) -----//

            //--- Action: Click Invite icon ---//
            action.IClick(driver, Elements.MyAnalog_ProjectDetailPage_Invite_Button);

            //--- Action: Enter more that 10 recepients email separated by comma ---//
            int invitees_input_counter = 0;
            string invitees_input = null;
            do
            {
                if (invitees_input_counter == 0)
                {
                    invitees_input = "t3st_" + Util.Configuration.Environment.ToLower() + "_ProjectInvitee_" + util.GenerateRandomNumber() + invitees_input_counter + "@mailinator.com";
                }
                else
                {
                    invitees_input = invitees_input + ", t3st_" + Util.Configuration.Environment.ToLower() + "_ProjectInvitee_" + util.GenerateRandomNumber() + invitees_input_counter + "@mailinator.com";
                }

                invitees_input_counter++;
            }
            while (invitees_input_counter < 11);

            action.IType(driver, Elements.MyAnalog_Projects_Invite_Modal_Invitees_InputBox, invitees_input.TrimEnd(','));

            //--- Action: Click Invite ---//
            action.IClick(driver, Elements.MyAnalog_Projects_Invite_Modal_Invite_Button);

            //--- Expected Result: An error message will display: Bad Request: {You can only invite maximum of 10 emails per project. ---//          
            test.validateElementIsPresent(driver, Elements.MyAnalog_Projects_Invite_Modal_Error_Message);
        }
    }
}