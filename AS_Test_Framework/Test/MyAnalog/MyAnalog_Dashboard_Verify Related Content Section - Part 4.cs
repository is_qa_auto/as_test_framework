﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using OpenQA.Selenium;
using System;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_Dashboard_RelatedContentSection_Part4 : BaseSetUp
    {
        public MyAnalog_Dashboard_RelatedContentSection_Part4() : base() { }

        //-- Login Credentials --//
        string en_username = "myanalog_t3st_888@mailinator.com";
        string en_password = "Test_1234";
        string cn_username = "test_zh_04252019@mailinator.com";
        string cn_password = "Test_1234";
        string jp_username = "test_jp_04252019@mailinator.com";
        string jp_password = "Test_1234";

        //--- URLs ---//
        string myAnalog_dashboard_url = "app";
        string pressReleases_url_format = "/press-releases/";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", "Press Releases", TestName = "Verify that the Press Releases Tab is Present and Working as Expected in EN Locale")]
        [TestCase("zh", "新闻发布", TestName = "Verify that the Press Releases Tab is Present and Working as Expected in CN Locale")]
        [TestCase("jp", "プレス・リリース", TestName = "Verify that the Press Releases Tab is Present and Working as Expected in JP Locale")]
        public void MyAnalog_Dashboard_RelatedContentSection_VerifyPressReleasesTab(string Locale, string PressReleases_Text)
        {
            Console.WriteLine("Steps to Test:");

            //--- Login Credentials ---//
            string username = null;
            string password = null;
            if (Locale.Equals("zh") || Locale.Equals("cn"))
            {
                username = cn_username;
                password = cn_password;
            }
            else if (Locale.Equals("jp"))
            {
                username = jp_username;
                password = jp_password;
            }
            else
            {
                username = en_username;
                password = en_password;
            }

            //--- myAnalog Dashboard URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_dashboard_url;

            //--- Action: Go to myAnalog dashboard ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid user account in the myAnalog Login page ---//
            action.ILogin(driver, username, password);
            Thread.Sleep(4000);

            //--- Expected Result: Related Content Tabs must be Present ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_RelatedContent_Tabs);

            if (!util.GetText(driver, Elements.MyAnalog_Dashboard_RelatedContent_Selected_Tab).Equals(PressReleases_Text) && !util.GetText(driver, Elements.MyAnalog_Dashboard_RelatedContent_Selected_Tab).Equals("Press Releases"))
            {
                //--- This looks for the Press Releases Tab ---//
                int relatedContent_tabs_count = util.GetCount(driver, Elements.MyAnalog_Dashboard_RelatedContent_Tabs);

                for (int counter = 1; counter <= relatedContent_tabs_count; counter++)
                {
                    if (util.GetText(driver, By.CssSelector("ul[class='nav nav-tabs']>li:nth-last-of-type(" + counter + ")>a")).Equals(PressReleases_Text) || util.GetText(driver, By.CssSelector("ul[class='nav nav-tabs']>li:nth-last-of-type(" + counter + ")>a")).Equals("Press Releases"))
                    {
                        //--- Locators ---//
                        string pressReleases_tab_locator = "ul[class='nav nav-tabs']>li:nth-last-of-type(" + counter + ")>a";

                        //--- Action: Click Press Release tab ---//
                        action.IClick(driver, By.CssSelector(pressReleases_tab_locator));
                        Thread.Sleep(2000);

                        break;
                    }

                    if (counter == relatedContent_tabs_count)
                    {
                        //--- Expected Result: Press Releases Tab must be Present ---//
                        test.validateElementIsPresent(driver, By.CssSelector("ul[class='nav nav-tabs']>li:nth-last-of-type(" + (counter + 1) + ")>a"));
                    }
                }
            }

            if (util.CheckElement(driver, Elements.MyAnalog_Dashboard_PressReleases_Tiles, 5))
            {
                //----- Dashboard Test Plan > Desktop Tab > R7 > T8.7: Verify the maximum number of display allowable for Press Releases Tile -----//

                //--- Expected Result: 12 is the max dispay ---//
                test.validateCountIsLessOrEqual(driver, util.GetCount(driver, Elements.MyAnalog_Dashboard_PressReleases_Tiles), 12);

                //----- Dashboard Test Plan > Desktop Tab > R7 > T8.4: Verify navigation when clicking Press release title link -----//

                //--- Expected Result: Press Release title are Present ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_PressReleases_Tiles_Title_Links);

                if (util.CheckElement(driver, Elements.MyAnalog_Dashboard_PressReleases_Tiles_Title_Links, 1))
                {
                    //--- Action: Click Press Release title link ---//
                    action.IOpenLinkInNewTab(driver, Elements.MyAnalog_Dashboard_PressReleases_Tiles_Title_Links);
                    Thread.Sleep(1000);

                    //--- Expected Result: Link redirect to Press Release page ---//
                    test.validateStringInstance(driver, driver.Url, pressReleases_url_format);

                    driver.Close();
                    driver.SwitchTo().Window(driver.WindowHandles.Last());
                }

                //----- Dashboard Test Plan > Desktop Tab > R7 > T8.5: Verify tags available-----//

                //--- Expected Result: Tags are displayed ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_1st_PressReleasesTile_Tag_Links);

                if (util.CheckElement(driver, Elements.MyAnalog_Dashboard_1st_PressReleasesTile_Tag_Links, 1))
                {
                    //----- Dashboard Test Plan > Desktop Tab > R7 > T8.5.1: Verify the maximum number of tags should display (IQ-6374/AL-13502) -----//

                    //--- Expected Result: Should show no more than 5 in collapsed view ---//
                    test.validateCountIsLessOrEqual(driver, util.GetCount(driver, Elements.MyAnalog_Dashboard_1st_PressReleasesTile_Tag_Links), 5);
                }

                if (util.CheckElement(driver, Elements.MyAnalog_Dashboard_1st_PressReleases_Tile_ArrowDown_Button, 1))
                {
                    //----- Dashboard Test Plan > Desktop Tab > R7 > T8.4.3: Verify what will happen if the maximum number of allowable tags is reach -----//

                    //--- Action: Click arrow down ---//
                    action.IClick(driver, Elements.MyAnalog_Dashboard_1st_PressReleases_Tile_ArrowDown_Button);

                    //--- Expected Result: List of other tags are displayed ---//
                    test.validateCountIsGreater(driver, util.GetCount(driver, Elements.MyAnalog_Dashboard_1st_PressReleasesTile_Tag_Links), 5);

                    //--- Action: Click arrow up ---//
                    action.IClick(driver, Elements.MyAnalog_Dashboard_1st_PressReleases_Tile_ArrowUp_Button);

                    //--- Expected Result: List set to collapse ---//
                    test.validateCountIsLessOrEqual(driver, util.GetCount(driver, Elements.MyAnalog_Dashboard_1st_PressReleasesTile_Tag_Links), 5);
                }

                //----- Dashboard Test Plan > Desktop Tab > R7 > T8.5: Verify when clicking ellipsis Webcast tile -----//

                //--- Action: Click ellipsis in webcast tile ---//
                action.IClick(driver, Elements.MyAnalog_Dashboard_PressReleases_Tiles_OtherOption_Icons);

                //--- Expected Result: Option to save is displayed ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_PressReleases_Tiles_Save_Buttons);

                //--- Expected Result: Option to Copy Link is displayed ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_PressReleases_Tiles_CopyLink_Buttons);
            }
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", "Customer Case Studies", TestName = "Verify that the Customer Case Studies Tab is Present and Working as Expected in EN Locale")]
        [TestCase("zh", "客户案例研究", TestName = "Verify that the Customer Case Studies Tab is Present and Working as Expected in CN Locale")]
        [TestCase("jp", "採用事例", TestName = "Verify that the Customer Case Studies Tab is Present and Working as Expected in JP Locale")]
        public void MyAnalog_Dashboard_RelatedContentSection_VerifyCustomerCaseStudiesTab(string Locale, string CustomerCaseStudies_Text)
        {
            Console.WriteLine("Steps to Test:");

            //--- Login Credentials ---//
            string username = null;
            string password = null;
            if (Locale.Equals("zh") || Locale.Equals("cn"))
            {
                username = cn_username;
                password = cn_password;
            }
            else if (Locale.Equals("jp"))
            {
                username = jp_username;
                password = jp_password;
            }
            else
            {
                username = en_username;
                password = en_password;
            }

            //--- myAnalog Dashboard URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_dashboard_url;

            //--- Action: Go to myAnalog dashboard ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid user account in the myAnalog Login page ---//
            action.ILogin(driver, username, password);
            Thread.Sleep(4000);

            //--- Expected Result: Related Content Tabs must be Present ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_RelatedContent_Tabs);

            if (!util.GetText(driver, Elements.MyAnalog_Dashboard_RelatedContent_Selected_Tab).Equals(CustomerCaseStudies_Text) && !util.GetText(driver, Elements.MyAnalog_Dashboard_RelatedContent_Selected_Tab).Equals("Customer Case Studies"))
            {
                //--- This looks for the Customer Case Studies Tab ---//
                int relatedContent_tabs_count = util.GetCount(driver, Elements.MyAnalog_Dashboard_RelatedContent_Tabs);

                for (int counter = 1; counter <= relatedContent_tabs_count; counter++)
                {
                    if (util.GetText(driver, By.CssSelector("ul[class='nav nav-tabs']>li:nth-last-of-type(" + counter + ")>a")).Equals(CustomerCaseStudies_Text) || util.GetText(driver, By.CssSelector("ul[class='nav nav-tabs']>li:nth-last-of-type(" + counter + ")>a")).Equals("Customer Case Studies"))
                    {
                        //--- Locators ---//
                        string customerCaseStudies_tab_locator = "ul[class='nav nav-tabs']>li:nth-last-of-type(" + counter + ")>a";

                        //--- Action: Click Customer Case Studies tab ---//
                        action.IClick(driver, By.CssSelector(customerCaseStudies_tab_locator));
                        Thread.Sleep(2000);

                        break;
                    }

                    if (counter == relatedContent_tabs_count)
                    {
                        //--- Expected Result: Customer Case Studies Tab must be Present ---//
                        test.validateElementIsPresent(driver, By.CssSelector("ul[class='nav nav-tabs']>li:nth-last-of-type(" + (counter + 1) + ")>a"));
                    }
                }
            }

            if (util.CheckElement(driver, Elements.MyAnalog_Dashboard_CustomerCaseStudies_Tiles, 5))
            {
                //----- Dashboard Test Plan > Desktop Tab > R7 > T11.1: Verify Customer Case Studies component -----//

                //--- Expected Result: Customer Case Studies Label are Present ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_CustomerCaseStudies_Tiles_CustomerCaseStudies_Labels);

                //--- Expected Result: Customer Case Studies title are Present ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_CustomerCaseStudies_Tiles_Title_Links);

                //--- Expected Result: Tag Labels are Present ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_1st_CustomerCaseStudies_Tile_Tag_Links);

                if (util.CheckElement(driver, Elements.MyAnalog_Dashboard_1st_CustomerCaseStudies_Tile_Tag_Links, 1))
                {
                    //----- Dashboard Test Plan > Desktop Tab > R7 > T11.4.1: Verify the maximum number of tags should display (IQ-6374/AL-13502) -----//

                    //--- Expected Result: Should show no more than 5 in collapsed view ---//
                    test.validateCountIsLessOrEqual(driver, util.GetCount(driver, Elements.MyAnalog_Dashboard_1st_CustomerCaseStudies_Tile_Tag_Links), 5);
                }

                //----- Dashboard Test Plan > Desktop Tab > R7 > T11.1: Verify Customer Case Studies component -----//

                //--- Action: Click ellipsis in Customer Case Studies tile ---//
                action.IClick(driver, Elements.MyAnalog_Dashboard_CustomerCaseStudies_Tiles_OtherOption_Icons);

                //--- Expected Result: Save option are Present ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_CustomerCaseStudies_Tiles_Save_Buttons);

                //--- Expected Result: Copy Link option are Present ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_CustomerCaseStudies_Tiles_CopyLink_Buttons);
            }
        }
    }
}