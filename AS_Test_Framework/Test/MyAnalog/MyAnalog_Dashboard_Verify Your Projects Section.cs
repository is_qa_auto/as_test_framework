﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_Dashboard_YourProjectsSection : BaseSetUp
    {
        public MyAnalog_Dashboard_YourProjectsSection() : base() { }

        //-- Login Credentials --//
        string username = "myanalog_t3st_555@mailinator.com";
        string password = "Test_1234";

        //--- URLs ---//
        string myAnalog_dashboard_url = "app";
        string myAnalog_projects_url = "app/projects";
        string myAnalog_projectDetail_page_url_format = "app/projects/";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Your Projects Label is Present and Working as Expected for Accounts with Created Projects in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Your Projects Label is Present and Working as Expected for Accounts with Created Projects in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Your Projects Label is Present and Working as Expected for Accounts with Created Projects in JP Locale")]
        public void MyAnalog_Dashboard_YourProjectsSection_AccountsWithCreatedProjects_VerifyYourProjectsLabel(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Projects URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_projects_url;

            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid user account in the myAnalog Login page ---//
            action.ILogin(driver, username, password);

            //--- Action: Get the Count for the Created Projects ---//
            int actual_createdProjects_count = util.GetMyAnalogCreatedProjectsCount(driver);

            //--- myAnalog Dashboard URL ---//
            myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_dashboard_url;

            //--- Action: Go to myAnalog dashboard ---//
            driver.Navigate().GoToUrl(myAnalog_url);
            Console.WriteLine("Go to: " + myAnalog_url);

            //----- Dashboard Test Plan > Desktop Tab > R3 > T2: Verify Project Title display -----//

            //--- Expected Result: Project Title is displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_YourProjects_Label);

            if (util.CheckElement(driver, Elements.MyAnalog_Dashboard_YourProjects_Label, 2))
            {
                //----- Dashboard Test Plan > Desktop Tab > R3 > T2.1: Verify Project counter -----//

                //--- Expected Result: The number of project is reflect in the Project counter ---//
                test.validateStringInstance(driver, util.ExtractNumber(driver, Elements.MyAnalog_Dashboard_YourProjects_Label), actual_createdProjects_count.ToString());
            }
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the See all your projects Link is Present and Working as Expected for Accounts with Created Projects in EN Locale")]
        [TestCase("zh", TestName = "Verify that the See all your projects Link is Present and Working as Expected for Accounts with Created Projects in CN Locale")]
        [TestCase("jp", TestName = "Verify that the See all your projects Link is Present and Working as Expected for Accounts with Created Projects in JP Locale")]
        public void MyAnalog_Dashboard_YourProjectsSection_AccountsWithCreatedProjects_VerifySeeAllYourProjectsLink(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Dashboard URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_dashboard_url;

            //--- Action: Go to myAnalog dashboard ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid user account in the myAnalog Login page ---//
            action.ILogin(driver, username, password);

            //----- Dashboard Test Plan > Desktop Tab > R3 > T.1: Verify Your Project Section when account has transaction already -----//

            //--- Expected Result: Your project section contains the See all Projects link ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_SeeAllYourProjects_Link);

            if (util.CheckElement(driver, Elements.MyAnalog_Dashboard_SeeAllYourProjects_Link, 2))
            {
                //----- Dashboard Test Plan > Desktop Tab > R3 > T5: Verift when navigation when clicking "See all your Projects" link -----//

                //--- Action: Click "See all your Projects" link  ---//
                action.IOpenLinkInNewTab(driver, Elements.MyAnalog_Dashboard_SeeAllYourProjects_Link);
                Thread.Sleep(1000);

                //--- Expected Result: goes to “Projects” overview page ---//
                test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + "/" + myAnalog_projects_url);
            }
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Project Details are Present and Working as Expected for Accounts with Created Projects in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Project Details are Present and Working as Expected for Accounts with Created Projects in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Project Details are Present and Working as Expected for Accounts with Created Projects in JP Locale")]
        public void MyAnalog_Dashboard_YourProjectsSection_AccountsWithCreatedProjects_VerifyProjectDetails(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Projects URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_projects_url;

            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid user account in the myAnalog Login page ---//
            action.ILogin(driver, username, password);
            Thread.Sleep(1000);

            //--- Action: Get the Name of the Latest Project ---//
            string latest_created_project_name = util.GetMyAnalogFirstProjectName(driver);

            //--- myAnalog Dashboard URL ---//
            myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_dashboard_url;

            //--- Action: Go to myAnalog dashboard ---//
            driver.Navigate().GoToUrl(myAnalog_url);
            Console.WriteLine("Go to: " + myAnalog_url);

            //----- PROJECT NAME LINK -----//

            //----- Dashboard Test Plan > Desktop Tab > R3 > T2.4: Verify Date modified format (IQ-6378/AL-13457, IQ-9091/Al-15109) -----//

            //--- Expected Result: Date modified is displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_Project_ModifiedDate);

            //----- Dashboard Test Plan > Desktop Tab > R3 > T2.8: Verify Project details and counter -----//

            //--- Expected Result: Project title is correct ---//      
            test.validateStringIsCorrect(driver, Elements.MyAnalog_Dashboard_ProjectName_Link, latest_created_project_name);

            if (util.CheckElement(driver, Elements.MyAnalog_Dashboard_ProjectName_Link, 2))
            {
                //----- Dashboard Test Plan > Desktop Tab > R3 > T2.2: Verify when clicking Project title -----//

                //--- Action: Click Project title link ---//
                action.IOpenLinkInNewTab(driver, Elements.MyAnalog_Dashboard_ProjectName_Link);
                Thread.Sleep(1000);

                //--- Expected Result: Page will redirect to Project Detail Page ---//
                test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + "/" + myAnalog_projectDetail_page_url_format);

                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles.Last());
            }

            //----- PROJECT STATISTICS -----//

            //----- Dashboard Test Plan > Desktop Tab > R3 > T10: Verify all sections as flags on Projects widget (IQ-8694/ALA-13514) -----//

            //--- Expected Result: Contributors is displayed. ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_Contributors_Statistic);

            //--- Expected Result: Products is displayed. ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_Products_Statistic);

            //----- Dashboard Test Plan > Desktop Tab > R3 > T2.15: Verify Products counter (IQ-9029/AL-16075) -----//

            //--- Expected Result: Products counter is displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_Products_Statistic_Count);

            //----- Dashboard Test Plan > Desktop Tab > R3 > T10: Verify all sections as flags on Projects widget (IQ-8694/ALA-13514) -----//

            //--- Expected Result: Resources is displayed. ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_Resources_Statistic);

            //--- Expected Result: PSTs is displayed. ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_Psts_Statistic);

            //----- Dashboard Test Plan > Desktop Tab > R3 > T2.16: Verify PSTs counter (IQ-9029/AL-16075) -----//

            //--- Expected Result: PSTs counter is displayed ---//        
            test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_Psts_Statistic_Count);

            //----- Dashboard Test Plan > Desktop Tab > R3 > T10: Verify all sections as flags on Projects widget (IQ-8694/ALA-13514) -----//

            //--- Expected Result: Notes is displayed. ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_Notes_Statistic);
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Left Arrow and Right Arrow are Present and Working as Expected for Accounts with Created Projects in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Left Arrow and Right Arrow are Present and Working as Expected for Accounts with Created Projects in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Left Arrow and Right Arrow are Present and Working as Expected for Accounts with Created Projects in JP Locale")]
        public void MyAnalog_Dashboard_YourProjectsSection_AccountsWithCreatedProjects_VerifyLeftArrowAndRightArrow(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Dashboard URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_dashboard_url;

            //--- Action: Go to myAnalog dashboard ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid user account in the myAnalog Login page ---//
            action.ILogin(driver, username, password);

            //----- LEFT ARROW -----//

            //----- Dashboard Test Plan > Desktop Tab > R3 > T4: Verify the Previous and Next funtionality -----//

            //--- Expected Result: Previous icon is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_YourProjects_Left_Arrow_Button);

            if (util.CheckElement(driver, Elements.MyAnalog_Dashboard_YourProjects_Left_Arrow_Button, 2))
            {
                //----- Dashboard Test Plan > Desktop Tab > R3 > T4.1: Verify display when clicking previous arrow -----//
                string previous_selected_projectName = util.GetText(driver, Elements.MyAnalog_Dashboard_ProjectName_Link);

                //--- Action: Click previos arrow ---//
                action.IClick(driver, Elements.MyAnalog_Dashboard_YourProjects_Left_Arrow_Button);

                //--- Expected Result: will go back to previosly project ---//
                test.validateStringChanged(driver, previous_selected_projectName, util.GetText(driver, Elements.MyAnalog_Dashboard_ProjectName_Link));
            }

            //--- Action: Go to myAnalog dashboard ---//
            driver.Navigate().Refresh();
            Console.WriteLine("Refresh the Page.");

            //----- RIGHT ARROW -----//

            //----- Dashboard Test Plan > Desktop Tab > R3 > T4: Verify the Previous and Next funtionality -----//

            //--- Expected Result: Next icon is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_YourProjects_Right_Arrow_Button);

            if (util.CheckElement(driver, Elements.MyAnalog_Dashboard_YourProjects_Right_Arrow_Button, 2))
            {
                //----- Dashboard Test Plan > Desktop Tab > R3 > T4.2: Verify display when clicking Next arrow -----//
                string previous_selected_projectName = util.GetText(driver, Elements.MyAnalog_Dashboard_ProjectName_Link);

                //--- Action: Click next arrow ---//
                action.IClick(driver, Elements.MyAnalog_Dashboard_YourProjects_Right_Arrow_Button);

                //--- Expected Result: Next Project will be displayed ---//
                test.validateStringChanged(driver, previous_selected_projectName, util.GetText(driver, Elements.MyAnalog_Dashboard_ProjectName_Link));
            }
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Create a new project Link is Present and Working as Expected for Accounts with Created Projects in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Create a new project Link is Present and Working as Expected for Accounts with Created Projects in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Create a new project Link is Present and Working as Expected for Accounts with Created Projects in JP Locale")]
        public void MyAnalog_Dashboard_YourProjectsSection_AccountsWithCreatedProjects_VerifyCreateANewProjectLink(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Dashboard URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_dashboard_url;

            //--- Action: Go to myAnalog dashboard ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid user account in the myAnalog Login page ---//
            action.ILogin(driver, username, password);

            //----- Dashboard Test Plan > Desktop Tab > R3 > T1.1: Verify Your Project Section when account has transaction already -----//

            //--- Expected Result: Your project section contains the Create a New Project ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_CreateANewProject_Link);

            if (util.CheckElement(driver, Elements.MyAnalog_Dashboard_CreateANewProject_Link, 2))
            {
                //----- Dashboard Test Plan > Desktop Tab > R3 > T7: Verify when creating new Project -----//

                //--- Action: Click create new project link ---//
                action.IClick(driver, Elements.MyAnalog_Dashboard_CreateANewProject_Link);

                //--- Expected Result: Page will redirect to create project page ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_CreateANewProject_Dialog);
            }
        }
    }
}