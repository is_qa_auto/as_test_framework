﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_Projects_ProjectDetailPage_ProjectResources : BaseSetUp
    {
        //--- myAnalog Projects URL: ---//
        //--- DEV: https://my-dev.corpnt.analog.com/en/app/projects ---//
        //--- QA: https://my-qa.corpnt.analog.com/en/app/projects ---//
        //--- PROD: https://my.analog.com/en/app/projects ---//

        public MyAnalog_Projects_ProjectDetailPage_ProjectResources() : base() { }

        //----- Note: Please don't edit/delete any details on the Accounts below -----//

        //--- Login Credentials (This Account must have more than 10 Saved Project Resources) ---// 
        string username = "my_t3st_pr0j3ct_d3t41ls_555@mailinator.com";
        string password = "Test_1234";

        //--- Login Credentials (Used for Editing Saved Resources) ---// 
        string username_editSavedResources = "my_t3st_3d1t_pr0j3ct_d3t41ls_555@mailinator.com";
        string password_editSavedResources = "Test_1234";

        //--- URLs ---//
        string myAnalog_projects_url = "app/projects";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that Project Resources is Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that Project Resources is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that Project Resources is Present and Working as Expected in JP Locale")]
        public void MyAnalog_Projects_ProjectDetailPage_VerifyProjectResources(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Projects URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_projects_url;

            //--- Action: Open myAnalog Projects using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);

            //--- Action: Click Project Detail button ---//
            action.IClick(driver, Elements.MyAnalog_Projects_ProjectDetails_Buttons);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Navigate to Project Details Page -----//

            //--- Expected Result: The Project Resources section component is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_ProjectDetailPage_ProjectResources_Section);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify Projet Resources (IQ-6227/AL-13533) -----//

            //--- Expected Result: The Project Resource label is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_ProjectDetailPage_ProjectResources_Label);

            //--- Expected Result: The Resource link are available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_ProjectDetailPage_SavedResources);

            //--- Expected Result: The Resource textbox is available ---//

            //--- Expected Result: The Add link button is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_ProjectDetailPage_ProjectResources_AddLink_Button);

            //--- Expected Result: The Edit option is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_ProjectDetailPage_ProjectResources_Edit_Button);

            //--- Expected Result: Pagination button "Previous" should be displayed. (IQ-6365/AL-13329) ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_ProjectDetailPage_ProjectResources_Previous_Button);

            //--- Expected Result: Pagination button "Next" should be displayed. (IQ-6365/AL-13329) ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_ProjectDetailPage_ProjectResources_Next_Button);
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that Adding Resources is Working as Expected in EN Locale")]
        public void MyAnalog_Projects_ProjectDetailPage_ProjectResources_VerifyAddingResources(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Projects URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_projects_url;

            //--- Action: Open myAnalog Projects using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username_editSavedResources, password_editSavedResources);

            //--- Action: Click Project Detail button ---//
            action.IClick(driver, Elements.MyAnalog_Projects_ProjectDetails_Buttons);

            //--- Action: Delete All Saved Resources in myAnalog if there's some ---//
            if (util.CheckElement(driver, Elements.MyAnalog_ProjectDetailPage_SavedResources, 2))
            {
                action.DeleteAllSavedProjectResourcesInMyAnalog(driver, Locale);
            }

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify if the resource link accepts a bad link. (IQ-6549/AL-13243) -----//

            //--- Action: Enter correct formatted URL. ---//
            string resource_input = null;
            if (Locale.Equals("zh"))
            {
                resource_input = Configuration.Env_Url + "cn/design-center/design-tools-and-calculators.html";
            }
            else
            {
                resource_input = Configuration.Env_Url + Locale + "/design-center/design-tools-and-calculators.html";
            }
            action.IDeleteValueOnFields(driver, Elements.MyAnalog_ProjectDetailPage_ProjectResources_Resource_InputBox);
            action.IType(driver, Elements.MyAnalog_ProjectDetailPage_ProjectResources_Resource_InputBox, resource_input);

            //--- Expected Result: Add link button enabled. ---//
            test.validateElementIsEnabled(driver, Elements.MyAnalog_ProjectDetailPage_ProjectResources_AddLink_Button);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify when adding resurce link (IQ-6566/AL-13394) -----//

            //--- Action: Click Add link button ---//
            action.IClick(driver, Elements.MyAnalog_ProjectDetailPage_ProjectResources_AddLink_Button);

            //--- Expected Result: Copied link appeared in project resource section ---//
            test.validateStringIsCorrect(driver, Elements.MyAnalog_ProjectDetailPage_Last_SavedResource_Link, resource_input);
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that Validation for Adding Invalid Resources is Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that Validation for Adding Invalid Resources is Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that Validation for Adding Invalid Resources is Working as Expected in JP Locale")]
        public void MyAnalog_Projects_ProjectDetailPage_ProjectResources_VerifyAddingInvalidResources(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Projects URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_projects_url;

            //--- Action: Open myAnalog Projects using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);

            //--- Action: Click Project Detail button ---//
            action.IClick(driver, Elements.MyAnalog_Projects_ProjectDetails_Buttons);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify if the resource link accepts a bad link. (IQ-6549/AL-13243) -----//

            //--- Action: Enter an erroneously formatted URL ---//
            string resource_input = "www.example.com/main[" + util.GenerateRandomString() + "[.html";
            action.IDeleteValueOnFields(driver, Elements.MyAnalog_ProjectDetailPage_ProjectResources_Resource_InputBox);
            action.IType(driver, Elements.MyAnalog_ProjectDetailPage_ProjectResources_Resource_InputBox, resource_input);

            //--- Expected Result: Add link button disabled. ---//
            test.validateElementIsNotEnabled(driver, Elements.MyAnalog_ProjectDetailPage_ProjectResources_AddLink_Button);
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Cancel Changes is Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Cancel Changes is Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Cancel Changes is Working as Expected in JP Locale")]
        public void MyAnalog_Projects_ProjectDetailPage_ProjectResources_VerifyCancelChanges(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Projects URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_projects_url;

            //--- Action: Open myAnalog Projects using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);

            //--- Action: Click Project Detail button ---//
            action.IClick(driver, Elements.MyAnalog_Projects_ProjectDetails_Buttons);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify when adding resurce link (IQ-6566/AL-13394) -----//

            //--- Action: Click edit option ---//
            action.IClick(driver, Elements.MyAnalog_ProjectDetailPage_ProjectResources_Edit_Button);

            //--- Expected Result: Save is displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_ProjectDetailPage_ProjectResources_Save_Button);

            //--- Expected Result: Cancel is displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_ProjectDetailPage_ProjectResources_Cancel_Button);
        }
    }
}