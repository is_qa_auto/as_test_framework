﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_Subscriptions_Elements : BaseSetUp
    {
        //--- myAnalog Subscriptions URL: ---//
        //--- DEV: https://my-dev.corpnt.analog.com/en/app/subscriptions ---//
        //--- QA: https://my-qa.corpnt.analog.com/en/app/subscriptions ---//
        //--- PROD: https://my.analog.com/en/app/subscriptions ---//

        public MyAnalog_Subscriptions_Elements() : base() { }

        //--- Login Credentials ---//
        string username = "myanalog_t3st_subscripti0ns_555@mailinator.com";
        string password = "Test_1234";

        //--- URLs ---//
        string myAnalog_subscriptions_url = "app/subscriptions";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Elements are Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Elements are Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Elements are Present and Working as Expected in JP Locale")]
        public void MyAnalog_Subscriptions_VerifyElements(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Subscriptions URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_subscriptions_url;

            //--- Action: Open subscription page ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);

            //----- Subscription - Dashboard > Desktop Tab > R2 > T1: Verify Subscription Landing Page -----//

            //--- Expected Result: The Subscription Title is displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Subscriptions_Label);

            //--- Expected Result: The Subscription description is displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Subscriptions_Description_Text);

            //--- Expected Result: The Save changes button is displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Subscriptions_SaveChanges_Button);

            //----- Subscription - Dashboard > Desktop Tab > R2 > T7: Verify the consent disclaimer (IQ-8794/ALA-13754) -----//

            //--- Expected Result: Consent Disclaimer displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Subscriptions_Disclaimer_Text);
        }
    }
}