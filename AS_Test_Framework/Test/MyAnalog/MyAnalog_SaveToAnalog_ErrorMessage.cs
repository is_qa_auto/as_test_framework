﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_SaveToAnalog_ErrorMessage : BaseSetUp
    {
        public MyAnalog_SaveToAnalog_ErrorMessage() : base() { }

        private string Username = "ISSQA_Automated_Test@analog.com";
        private string password = "Test_1234";

        [Test, Category("MyAnalog"), Category("NonCore")]
        [TestCase("en", TestName = "Verify Error Message if an already existing Product is saved to a Project")]
        public void MyAnalog_SaveToAnalog_VerifyErrorForExistingProduct(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            action.IRemoveAllSavedProductsInMyAnalog(driver, Username, password);

            action.Navigate(driver, Configuration.Env_Url + Locale + "/ad7960");

            action.ILogInViaMyAnalogWidget(driver, Username, password);

            action.IClick(driver, Elements.MyAnalog_Widget_Save_Button);

            driver.Navigate().Refresh();
            Console.WriteLine("Refresh the Page.");

            //action.IClick(driver, Elements.MyAnalog_Widget_Btn);
            action.IClick(driver, Elements.MyAnalog_Widget_Save_Button);
            test.validateStringIsCorrect(driver, Elements.MyAnalog_Widget_DuplicateCat_Validation_Message, "This page is already saved to myAnalog");
        }
    }
}