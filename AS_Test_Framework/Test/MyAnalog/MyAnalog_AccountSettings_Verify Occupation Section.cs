﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using OpenQA.Selenium;
using System;
using System.Threading;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_AccountSettings_OccupationSection : BaseSetUp
    {
        //--- myAnalog Account Settings URL: ---//
        //--- DEV: https://my-dev.corpnt.analog.com/en/app/account ---//
        //--- QA: https://my-qa.corpnt.analog.com/en/app/account ---//
        //--- PROD: https://my.analog.com/en/app/account ---//

        public MyAnalog_AccountSettings_OccupationSection() : base() { }

        //--- Login Credentials ---//
        string username = null;
        string password = null;

        //--- URLs ---//
        string myAnalog_accountSettings_url = "app/account";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Elements are Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Elements are Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Elements are Present and Working as Expected in JP Locale")]
        public void MyAnalog_AccountSettings_VerifyOccupationSection(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- Login Credentials ---//
            if (Locale.Equals("zh") || Locale.Equals("cn"))
            {
                username = "myanalog_t3st_cn_777@mailinator.com";
            }
            else if (Locale.Equals("jp"))
            {
                username = "myanalog_t3st_jp_777@mailinator.com";
            }
            else
            {
                username = "myanalog_t3st_777@mailinator.com";
            }
            password = "Test_1234";

            //--- myAnalog Account Settings URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_accountSettings_url;

            //--- Action: Open account settings page ---//
            action.Navigate(driver, myAnalog_url);

            action.ILogin(driver, username, password);
            Thread.Sleep(7000);

            //----- Account Settings Test Plan > myAnalog Dashboard Tab > R2 > T5: Verify when clicking Edit Occupation section -----//

            //--- Expected Result: occupation section is displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_AccountSettings_Occupation_Section);

            //----- Account Settings Test Plan > myAnalog Dashboard Tab > R5 > T2/T4/T6: Optional fields for English/Chinese/Japanese language (IQ-8917/AL-15745)-----//

            //--- Expected Result: The the What's your occupation? field is available and is optional ---//
            test.validateElementIsNotPresent(driver, Elements.MyAnalog_AccountSettings_Occupation_Section_Required_WhatsYourOccupation_Dropdown);

            //--- Expected Result: The the What type of… field is available and is optional ---//
            test.validateElementIsNotPresent(driver, Elements.MyAnalog_AccountSettings_Occupation_Section_Required_WhatTypeOf_Dropdown);

            if (util.CheckElement(driver, Elements.MyAnalog_AccountSettings_Occupation_Section_GraduationYear_Dropdown, 1))
            {
                //--- Expected Result: The the Graduation Year field is available and is optional ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_AccountSettings_Occupation_Section_Required_GraduationYear_Dropdown);
            }

            if (util.CheckElement(driver, Elements.MyAnalog_AccountSettings_Occupation_Section_SchoolName_InputBox, 1))
            {
                //--- Expected Result: The the School Name field is available and is optional ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_AccountSettings_Occupation_Section_Required_SchoolName_InputBox);
            }
            //----- Account Settings Test Plan > myAnalog Dashboard Tab > R2 > T14: Verify if the Edit button is available in all section except change password section -----//

            //--- Expected Result: Edit button available in all section except change password section ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_AccountSettings_Occupation_Section_Edit_Button);

            //----- Account Settings Test Plan > myAnalog Dashboard Tab > R2 > T5: Verify when clicking Edit Occupation section -----//

            //--- Action: Click Pencil icon top-right corner of  the section ---//
            action.IClick(driver, Elements.MyAnalog_AccountSettings_Occupation_Section_Edit_Button);

            //--- Expected Result: Occupation section enables editing mode ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_AccountSettings_Occupation_Section_Editing_Mode);

            //--- Expected Result: the pencil icon is replace with “save” and “cancel” options. ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_AccountSettings_Occupation_Section_Save_Button);
            test.validateElementIsPresent(driver, Elements.MyAnalog_AccountSettings_Occupation_Section_Cancel_Button);

            //--- Expected Result: The Whats Your Occupation ? field is editable ---//
            test.validateElementIsEnabled(driver, Elements.MyAnalog_AccountSettings_Occupation_Section_WhatsYourOccupation_Dropdown);

            ////--- Expected Result: The What Type of… field is editable ---//
            //test.validateElementIsEnabled(driver, Elements.MyAnalog_AccountSettings_Occupation_Section_WhatTypeOf_Dropdown);

            if (util.CheckElement(driver, Elements.MyAnalog_AccountSettings_Occupation_Section_GraduationYear_Dropdown, 1))
            {
                //--- Expected Result: The Graduation Year field is editable ---//
                test.validateElementIsEnabled(driver, Elements.MyAnalog_AccountSettings_Occupation_Section_GraduationYear_Dropdown);
            }

            if (util.CheckElement(driver, Elements.MyAnalog_AccountSettings_Occupation_Section_SchoolName_InputBox, 1))
            {
                //--- Expected Result: The School name field is editable ---//
                test.validateElementIsEnabled(driver, Elements.MyAnalog_AccountSettings_Occupation_Section_SchoolName_InputBox);
            }
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", "STUDENT", TestName = "Verify that the Save Button is Present and Working for Valid Inputs as Expected in EN Locale")]
        [TestCase("zh", "学生", TestName = "Verify that the Save Button is Present and Working for Valid Inputs as Expected in CN Locale")]
        [TestCase("jp", "学生", TestName = "Verify that the Save Button is Present and Working for Valid Inputs as Expected in JP Locale")]
        public void MyAnalog_AccountSettings_OccupationSection_VerifySaveButtonForValidInputs(string Locale, string Student_Text)
        {
            Console.WriteLine("Steps to Test:");

            //--- Login Credentials ---//
            if (Locale.Equals("zh") || Locale.Equals("cn"))
            {
                username = "myanalog_t3st_upd4t3_0cn_f13lds_cn_555@mailinator.com";
            }
            else if (Locale.Equals("jp"))
            {
                username = "myanalog_t3st_upd4t3_0cn_f13lds_jp_555@mailinator.com";
            }
            else
            {
                username = "myanalog_t3st_upd4t3_0cn_f13lds_en_555@mailinator.com";
            }
            password = "Test_1234";

            //--- myAnalog Account Settings URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_accountSettings_url;

            //--- Action: Open account settings page ---//
            action.Navigate(driver, myAnalog_url);

            action.ILogin(driver, username, password);
            Thread.Sleep(6000);

            //----- Account Settings Test Plan > myAnalog Dashboard Tab > R4 > T21: Verify if Graduation Year and School Name are hidden upon selecting occupation except Student -----//

            //--- Action: Click the edit button on the 3rd section of the page (occupation, etc.)  ---//
            action.IClick(driver, Elements.MyAnalog_AccountSettings_Occupation_Section_Edit_Button);

            if (!util.GetText(driver, Elements.MyAnalog_AccountSettings_Occupation_Section_WhatsYourOccupation_Dropdown).Equals(Student_Text))
            {
                //--- Expected Result: Graduation Year is hidden ---//
                test.validateElementIsNotPresent(driver, Elements.MyAnalog_AccountSettings_Occupation_Section_GraduationYear_Dropdown);

                //--- Expected Result: School Name is hidden ---//
                test.validateElementIsNotPresent(driver, Elements.MyAnalog_AccountSettings_Occupation_Section_SchoolName_InputBox);
            }

            //----- Account Settings Test Plan > myAnalog Dashboard Tab > R4 > T: Verify that the user will be able to update his/her account when all required and optional fields have been populated -----//
            //----- Precondition: Order has not been made yet) -----//

            //--- Action: Edit all fields with valid values ---//
            bool whatsYourOccupation_dropdown = false;
            string whatsYourOccupation_input = null;
            if (util.CheckElement(driver, Elements.MyAnalog_AccountSettings_Occupation_Section_WhatsYourOccupation_Dropdown, 1) && driver.FindElement(Elements.MyAnalog_AccountSettings_Occupation_Section_WhatsYourOccupation_Dropdown).Enabled)
            {
                whatsYourOccupation_dropdown = true;

                action.IClick(driver, Elements.MyAnalog_AccountSettings_Occupation_Section_WhatsYourOccupation_Dropdown);

                int whatsYourOccupation_dropdown_value_count = util.GetCount(driver, Elements.MyAnalog_AccountSettings_Occupation_Section_WhatsYourOccupation_Dropdown_Values);
                Random rand = new Random();
                int random_number = rand.Next(1, whatsYourOccupation_dropdown_value_count);

                whatsYourOccupation_input = util.GetText(driver, By.CssSelector("div[class='occupation fields']>div:nth-of-type(1)>div[class^='options']>div:nth-of-type(" + random_number + ")"));
                action.IClick(driver, By.CssSelector("div[class='occupation fields']>div:nth-of-type(1)>div[class^='options']>div:nth-of-type(" + random_number + ")"));
            }

            bool whatTypeOf_dropdown = false;
            string whatTypeOf_input = null;
            if (util.CheckElement(driver, Elements.MyAnalog_AccountSettings_Occupation_Section_WhatTypeOf_Dropdown, 1) && driver.FindElement(Elements.MyAnalog_AccountSettings_Occupation_Section_WhatTypeOf_Dropdown).Enabled)
            {
                whatTypeOf_dropdown = true;

                action.IClick(driver, Elements.MyAnalog_AccountSettings_Occupation_Section_WhatTypeOf_Dropdown);

                int whatTypeOf_dropdown_value_count = util.GetCount(driver, Elements.MyAnalog_AccountSettings_Occupation_Section_WhatTypeOf_Dropdown_Values);
                Random rand = new Random();
                int random_number = rand.Next(1, whatTypeOf_dropdown_value_count);

                whatTypeOf_input = util.GetText(driver, By.CssSelector("div[class='occupation fields']>div:nth-of-type(2)>div[class^='options']>div:nth-of-type(" + random_number + ")"));
                action.IClick(driver, By.CssSelector("div[class='occupation fields']>div:nth-of-type(2)>div[class^='options']>div:nth-of-type(" + random_number + ")"));
            }

            bool graduationYear_dropdown = false;
            string graduationYear_input = null;
            if (util.CheckElement(driver, Elements.MyAnalog_AccountSettings_Occupation_Section_GraduationYear_Dropdown, 1) && driver.FindElement(Elements.MyAnalog_AccountSettings_Occupation_Section_GraduationYear_Dropdown).Enabled)
            {
                graduationYear_dropdown = true;

                action.IClick(driver, Elements.MyAnalog_AccountSettings_Occupation_Section_GraduationYear_Dropdown);

                int graduationYear_dropdown_value_count = util.GetCount(driver, Elements.MyAnalog_AccountSettings_Occupation_Section_GraduationYear_Dropdown_Values);
                Random rand = new Random();
                int random_number = rand.Next(1, graduationYear_dropdown_value_count);

                graduationYear_input = util.GetText(driver, By.CssSelector("div[class='graduation year'] * div[class^='options']>div:nth-of-type(" + random_number + ")"));
                action.IClick(driver, By.CssSelector("div[class='graduation year'] * div[class^='options']>div:nth-of-type(" + random_number + ")"));
            }

            bool schoolName_inputBox = false;
            string schoolName_input = null;
            if (util.CheckElement(driver, Elements.MyAnalog_AccountSettings_Occupation_Section_SchoolName_InputBox, 2))
            {
                schoolName_inputBox = true;

                schoolName_input = "schoolName_" + util.GenerateRandomString();
                action.IDeleteValueOnFields(driver, Elements.MyAnalog_AccountSettings_Occupation_Section_SchoolName_InputBox);
                action.IType(driver, Elements.MyAnalog_AccountSettings_Occupation_Section_SchoolName_InputBox, schoolName_input);
            }

            //--- Action: Click Save option ---//
            action.IClick(driver, Elements.MyAnalog_AccountSettings_Occupation_Section_Save_Button);
            Thread.Sleep(2000);

            //--- Expected Result: System successfully saved changes made ---//
            if (whatsYourOccupation_dropdown.Equals(true))
            {
                test.validateStringIsCorrect(driver, Elements.MyAnalog_AccountSettings_Occupation_Section_WhatsYourOccupation_Dropdown, whatsYourOccupation_input);
            }

            if (whatTypeOf_dropdown.Equals(true))
            {
                test.validateStringIsCorrect(driver, Elements.MyAnalog_AccountSettings_Occupation_Section_WhatTypeOf_Dropdown, whatTypeOf_input);
            }

            if (graduationYear_dropdown.Equals(true))
            {
                test.validateStringIsCorrect(driver, Elements.MyAnalog_AccountSettings_Occupation_Section_GraduationYear_Dropdown, graduationYear_input);
            }

            if (schoolName_inputBox.Equals(true))
            {
                test.validateStringInstance(driver, schoolName_input, util.GetInputBoxValue(driver, Elements.MyAnalog_AccountSettings_Occupation_Section_SchoolName_InputBox));
            }
        }
    }
}