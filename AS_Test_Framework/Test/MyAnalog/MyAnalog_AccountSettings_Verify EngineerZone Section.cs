﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System;
using System.Threading;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_AccountSettings_EngineerZoneSection : BaseSetUp
    {
        //--- myAnalog Account Settings URL: ---//
        //--- DEV: https://my-dev.corpnt.analog.com/en/app/account ---//
        //--- QA: https://my-qa.corpnt.analog.com/en/app/account ---//
        //--- PROD: https://my.analog.com/en/app/account ---//

        public MyAnalog_AccountSettings_EngineerZoneSection() : base() { }

        //--- Login Credentials ---//
        string withEz_username = "test_05032019@mailinator.com";
        string withEz_password = "Test_1234";
        string noEz_username = "myanalog_t3st_n0_ez_us3rn4me@mailinator.com";
        string noEz_password = "Test_1234";

        //--- URLs ---//
        string myAnalog_accountSettings_url = "app/account";

        //--- Label ---//
        string yourEngineerZoneUsername_invalidInput_errorMessage = "{This is a reserved word, Please select a different EngineerZone User Name.}×";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Elements are Present and Working as Expected for a User that has an EngineerZone Username in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Elements are Present and Working as Expected for a User that has an EngineerZone Username in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Elements are Present and Working as Expected for a User that has an EngineerZone Username in JP Locale")]
        public void MyAnalog_AccountSettings_EngineerZoneSection_VerifyForUserThatHasEngineerZoneUsername(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Account Settings URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_accountSettings_url;

            //--- Action: Open account settings page ---//
            action.Navigate(driver, myAnalog_url);

            action.ILogin(driver, withEz_username, withEz_password);
            Thread.Sleep(1000);

            //----- Account Settings Test Plan > myAnalog Dashboard Tab > R2 > T1: Verify Account Settings Landing Page -----//

            //--- Expected Result: The Your Engineer zone account section is displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_AccountSettings_EngineerZone_Section);

            //--- Expected Result: Edit button is no longer available ---//
            test.validateElementIsNotPresent(driver, Elements.MyAnalog_AccountSettings_EngineerZone_Section_Edit_Button);

            //--- Expected Result: 'View Your EngineerZone Profile' hyperlink is present ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_AccountSettings_EngineerZone_Section_ViewYourEngineerZoneProfile_Link);
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Elements are Present and Working as Expected for a User that has No EngineerZone Username in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Elements are Present and Working as Expected for a User that has No EngineerZone Username in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Elements are Present and Working as Expected for a User that has No EngineerZone Username in JP Locale")]
        public void MyAnalog_AccountSettings_EngineerZoneSection_VerifyForUserThatHasNoEngineerZoneUsername(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Account Settings URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_accountSettings_url;

            //--- Action: Open account settings page ---//
            action.Navigate(driver, myAnalog_url);

            action.ILogin(driver, noEz_username, noEz_password);
            Thread.Sleep(2000);

            //----- Account Settings Test Plan > myAnalog Dashboard Tab > R2 > T1: Verify Account Settings Landing Page -----//

            //--- Expected Result: The Your Engineer zone account section is displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_AccountSettings_EngineerZone_Section);

            //----- Account Settings Test Plan > myAnalog Dashboard Tab > R5 > T2/T4/T6: Optional fields for English/Chinese/Japanese language (IQ-8917/AL-15745) -----//

            //--- Expected Result: The the EngineerZone Username field is available and is optional ---//
            test.validateElementIsNotPresent(driver, Elements.MyAnalog_AccountSettings_EngineerZone_Section_Required_YourEngineerZoneUsername_InputBox);

            //----- Account Settings Test Plan > myAnalog Dashboard Tab > R2 > T14: Verify if the Edit button is available in all section except change password section -----//

            //--- Expected Result: Edit button available in all section except change password section ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_AccountSettings_EngineerZone_Section_Edit_Button);

            //----- Account Settings Test Plan > myAnalog Dashboard Tab > R2 > T1: Verify Account Settings Landing Page -----//

            //--- Action: Click the Edit button in the Engineer Zone section (applicable for myAnalog account that no longer have an EngineerZone Username) ---//
            action.IClick(driver, Elements.MyAnalog_AccountSettings_EngineerZone_Section_Edit_Button);

            //--- Expected Result: The Engineer Zone section became editable ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_AccountSettings_EngineerZone_Section_Editing_Mode);
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Save Button is Present and Working as Expected for Invalid Inputs in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Save Button is Present and Working as Expected for Invalid Inputs in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Save Button is Present and Working as Expected for Invalid Inputs in JP Locale")]
        public void MyAnalog_AccountSettings_EngineerZoneSection_VerifySaveButtonForInvalidInputs(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Account Settings URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_accountSettings_url;

            //--- Action: Open account settings page ---//
            action.Navigate(driver, myAnalog_url);

            action.ILogin(driver, noEz_username, noEz_password);
            Thread.Sleep(2000);

            //--- Action: Click Edit button in EZ section ---//
            action.IClick(driver, Elements.MyAnalog_AccountSettings_EngineerZone_Section_Edit_Button);

            //----- Account Settings Test Plan > myAnalog Dashboard Tab > R2 > T1: Verify Account Settings Landing Page -----//

            //--- Action: Enter a reserved word: Ex. "anonymous", "admin", "administrator", "administrators", "analogdevices", "former", "formermember", "cm", "cms", "moderator" in the Your EngineerZone Username field ---//
            string yourEngineerZoneUsername_input = "admin";
            action.IDeleteValueOnFields(driver, Elements.MyAnalog_AccountSettings_EngineerZone_Section_YourEngineerZoneUsername_InputBox);
            action.IType(driver, Elements.MyAnalog_AccountSettings_EngineerZone_Section_YourEngineerZoneUsername_InputBox, yourEngineerZoneUsername_input);
            Thread.Sleep(7000);

            //----- Account Settings Test Plan > myAnalog Dashboard Tab > R2 > T7.1: Verify Clear icon must only appear when the input has a value (IQ-6547/ AL-13367)-----//

            //--- Expected Result: Clear Icon must apear when the input has a value ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_AccountSettings_EngineerZone_Section_YourEngineerZoneUsername_InputBox_X_Button);

            //----- Account Settings Test Plan > myAnalog Dashboard Tab > R2 > T1: Verify Account Settings Landing Page -----//

            //--- Action: Click the Save button ---//
            action.IClick(driver, Elements.MyAnalog_AccountSettings_EngineerZone_Section_Save_Button);

            if (Locale.Equals("EN"))
            {
                //--- Expected Result: The System displayed an Error Message '{This is a reserved word, Please select a different EngineerZone User Name.}×' ---//
                test.validateStringIsCorrect(driver, Elements.MyAnalog_AccountSettings_EngineerZone_Section_YourEngineerZoneUsername_InputBox_InvalidInput_ErrorMessage, yourEngineerZoneUsername_invalidInput_errorMessage);
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.MyAnalog_AccountSettings_EngineerZone_Section_YourEngineerZoneUsername_InputBox_InvalidInput_ErrorMessage);
            }
        }
    }
}