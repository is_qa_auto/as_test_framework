﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_Subscriptions_Tiles : BaseSetUp
    {
        //--- myAnalog Subscriptions URL: ---//
        //--- DEV: https://my-dev.corpnt.analog.com/en/app/subscriptions ---//
        //--- QA: https://my-qa.corpnt.analog.com/en/app/subscriptions ---//
        //--- PROD: https://my.analog.com/en/app/subscriptions ---//

        public MyAnalog_Subscriptions_Tiles() : base() { }

        //--- Login Credentials ---//
        string username = "myanalog_t3st_subscripti0ns_555@mailinator.com";
        string password = "Test_1234";

        //--- URLs ---//
        string myAnalog_subscriptions_url = "app/subscriptions";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Tiles are Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Tiles are Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Tiles are Present and Working as Expected in JP Locale")]
        public void MyAnalog_Subscriptions_VerifyTiles(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Subscriptions URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_subscriptions_url;

            //--- Action: Open subscription page ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);

            //----- Subscription - Dashboard > Desktop Tab > R2 > T1: Verify Subscription Landing Page -----//

            //--- Expected Result: The Subscription Tile is displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Subscriptions_Tiles);

            //----- Subscription - Dashboard > Desktop Tab > R3 > T1: Verify myAnalog Updates Tile -----//

            //--- Expected Result: The title is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Subscriptions_Tiles_Title_Text);

            //--- Expected Result: The toggle button is clickable  ---//
            test.validateElementIsEnabled(driver, Elements.MyAnalog_Subscriptions_Tiles_Toggle_Button);

            //--- Expected Result: The Locale dropdown is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Subscriptions_Tiles_Locale_Dropdown);

            //--- Expected Result: The html radio button is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Subscriptions_Tiles_Html_RadioButton);

            //--- Expected Result: The text radio button is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Subscriptions_Tiles_Text_RadioButton);

            //--- Expected Result: The description is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Subscriptions_Tiles_Description_Text);
        }
    }
}