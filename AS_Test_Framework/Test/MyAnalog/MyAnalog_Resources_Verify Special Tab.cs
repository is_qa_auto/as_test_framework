﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_Resources_SpecialTab : BaseSetUp
    {
        //--- myAnalog Resources URL: ---//
        //--- DEV: https://my-dev.corpnt.analog.com/en/app/resources ---//
        //--- QA: https://my-qa.corpnt.analog.com/en/app/resources ---//
        //--- PROD: https://my.analog.com/en/app/resources ---//

        public MyAnalog_Resources_SpecialTab() : base() { }

        //--- Login Credentials ---//
        string username = "myanalog_t3st_r3s0urc3s_555@mailinator.com";
        string password = "Test_1234";

        //--- URLs ---//
        string myAnalog_resources_url = "app/resources";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Special Tab is Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Special Tab is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Special Tab is Present and Working as Expected in JP Locale")]
        public void MyAnalog_Resources_SpecialTab_VerifyWhenUserIsLoggedIn(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Resources URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_resources_url;

            //--- Action: Open resources direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);

            //----- Resources Test Plan > Desktop Tab > R2 > T1: Verify Display of Resources landing page -----//

            //--- Expected Result: The Special Resources tab is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Resources_Special_Tab);
        }
    }
}