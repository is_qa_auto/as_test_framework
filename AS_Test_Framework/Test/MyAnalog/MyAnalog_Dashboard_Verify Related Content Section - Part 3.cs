﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using OpenQA.Selenium;
using System;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_Dashboard_RelatedContentSection_Part3 : BaseSetUp
    {
        public MyAnalog_Dashboard_RelatedContentSection_Part3() : base() { }

        //-- Login Credentials --//
        string en_username = "myanalog_t3st_888@mailinator.com";
        string en_password = "Test_1234";
        string cn_username = "test_zh_04252019@mailinator.com";
        string cn_password = "Test_1234";
        string jp_username = "test_jp_04252019@mailinator.com";
        string jp_password = "Test_1234";

        //--- URLs ---//
        string myAnalog_dashboard_url = "app";
        string analogdialogue_url_format = "/analog-dialogue/";
        string webcasts_url_format = "/webcasts/";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", "Analog Dialogue", TestName = "Verify that the Analog Dialogue Tab is Present and Working as Expected in EN Locale")]
        [TestCase("zh", "《模拟对话》", TestName = "Verify that the Analog Dialogue Tab is Present and Working as Expected in CN Locale")]
        [TestCase("jp", "アナログ・ダイアログ", TestName = "Verify that the Analog Dialogue Tab is Present and Working as Expected in JP Locale")]
        public void MyAnalog_Dashboard_RelatedContentSection_VerifyAnalogDialogueTab(string Locale, string AnalogDialogue_Text)
        {
            Console.WriteLine("Steps to Test:");

            //--- Login Credentials ---//
            string username = null;
            string password = null;
            if (Locale.Equals("zh") || Locale.Equals("cn"))
            {
                username = cn_username;
                password = cn_password;
            }
            else if (Locale.Equals("jp"))
            {
                username = jp_username;
                password = jp_password;
            }
            else
            {
                username = en_username;
                password = en_password;
            }

            //--- myAnalog Dashboard URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_dashboard_url;

            //--- Action: Go to myAnalog dashboard ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid user account in the myAnalog Login page ---//
            action.ILogin(driver, username, password);
            Thread.Sleep(4000);

            //--- Expected Result: Related Content Tabs must be Present ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_RelatedContent_Tabs);

            if (!util.GetText(driver, Elements.MyAnalog_Dashboard_RelatedContent_Selected_Tab).Equals(AnalogDialogue_Text) && !util.GetText(driver, Elements.MyAnalog_Dashboard_RelatedContent_Selected_Tab).Equals("Analog Dialogue"))
            {
                //--- This looks for the Analog Dialogue Tab ---//
                int relatedContent_tabs_count = util.GetCount(driver, Elements.MyAnalog_Dashboard_RelatedContent_Tabs);

                for (int counter = 1; counter <= relatedContent_tabs_count; counter++)
                {
                    if (util.GetText(driver, By.CssSelector("ul[class='nav nav-tabs']>li:nth-of-type(" + counter + ")>a")).Equals(AnalogDialogue_Text) || util.GetText(driver, By.CssSelector("ul[class='nav nav-tabs']>li:nth-of-type(" + counter + ")>a")).Equals("Analog Dialogue"))
                    {
                        //--- Locators ---//
                        string analogDialogue_tab_locator = "ul[class='nav nav-tabs']>li:nth-of-type(" + counter + ")>a";

                        //--- Action: Click Analog Dialogue tab ---//
                        action.IClick(driver, By.CssSelector(analogDialogue_tab_locator));
                        Thread.Sleep(2000);

                        break;
                    }

                    if (counter == relatedContent_tabs_count)
                    {
                        //--- Expected Result: Analog Dialogue Tab must be Present ---//
                        test.validateElementIsPresent(driver, By.CssSelector("ul[class='nav nav-tabs']>li:nth-of-type(" + (counter + 1) + ")>a"));
                    }
                }
            }

            if (util.CheckElement(driver, Elements.MyAnalog_Dashboard_AnalogDialogue_Tiles, 5))
            {
                //----- Dashboard Test Plan > Desktop Tab > R7 > T6.7: Verify the maximum number of display allowable for Analog Dialogue Tile -----//

                //--- Expected Result: 12 is the max dispay ---//
                test.validateCountIsLessOrEqual(driver, util.GetCount(driver, Elements.MyAnalog_Dashboard_AnalogDialogue_Tiles), 12);

                //----- Dashboard Test Plan > Desktop Tab > R7 > T6.1: Verify Component of Analog Dialogue  -----//

                //--- Expected Result: Analog Dialogue Label are Present ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_AnalogDialogue_Tiles_AnalogDialogue_Labels);

                //--- Expected Result: Image are Present ---//
                test.validateImageIsNotBeBroken(driver, Elements.MyAnalog_Dashboard_AnalogDialogue_Tiles_Thumbnail_Images);

                //--- Expected Result: Analog Dialogue title are Present ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_AnalogDialogue_Tiles_Title_Links);

                if (util.CheckElement(driver, Elements.MyAnalog_Dashboard_AnalogDialogue_Tiles_Title_Links, 1))
                {
                    //----- Dashboard Test Plan > Desktop Tab > R7 > T6.3: Verify navigation when clicking Analog Dialogue title link -----//

                    //--- Action: Click Analog Dialogue title link ---//
                    action.IOpenLinkInNewTab(driver, Elements.MyAnalog_Dashboard_AnalogDialogue_Tiles_Title_Links);

                    //--- Expected Result: Link redirect to Analog Dialogue page ---//
                    if (Locale.Equals("zh") || Locale.Equals("cn"))
                    {
                        test.validateStringInstance(driver, driver.Url, "analog.com/cn" + analogdialogue_url_format);
                    }
                    else
                    {
                        test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + analogdialogue_url_format);
                    }

                    driver.Close();
                    driver.SwitchTo().Window(driver.WindowHandles.Last());
                }

                //--- Expected Result: Tag label are Present ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_1st_AnalogDialogue_Tile_Tag_Links);

                if (util.CheckElement(driver, Elements.MyAnalog_Dashboard_1st_AnalogDialogue_Tile_Tag_Links, 1))
                {
                    //----- Dashboard Test Plan > Desktop Tab > R7 > T6.4.1: Verify the maximum number of tags should display (IQ-6374/AL-13502) -----//

                    //--- Expected Result: Should show no more than 5 in collapsed view ---//
                    test.validateCountIsLessOrEqual(driver, util.GetCount(driver, Elements.MyAnalog_Dashboard_1st_AnalogDialogue_Tile_Tag_Links), 5);
                }

                if (util.CheckElement(driver, Elements.MyAnalog_Dashboard_1st_AnalogDialogue_Tile_ArrowDown_Button, 1))
                {
                    //----- Dashboard Test Plan > Desktop Tab > R7 > T6.4.3: Verify what will happen if the maximum number of allowable tags is reach -----//

                    //--- Action: Click arrow down ---//
                    action.IClick(driver, Elements.MyAnalog_Dashboard_1st_AnalogDialogue_Tile_ArrowDown_Button);

                    //--- Expected Result: List of other tags are displayed ---//
                    test.validateCountIsGreater(driver, util.GetCount(driver, Elements.MyAnalog_Dashboard_1st_AnalogDialogue_Tile_Tag_Links), 5);

                    //--- Action: Click arrow up ---//
                    action.IClick(driver, Elements.MyAnalog_Dashboard_1st_AnalogDialogue_Tile_ArrowUp_Button);

                    //--- Expected Result: List set to collapse ---//
                    test.validateCountIsLessOrEqual(driver, util.GetCount(driver, Elements.MyAnalog_Dashboard_1st_AnalogDialogue_Tile_Tag_Links), 5);
                }

                //----- Dashboard Test Plan > Desktop Tab > R7 > T6.5: Verify when clicking ellipsis Analog Dialogue tile -----//

                //--- Action: Click ellipsis in Analog Dialogue tile ---//
                action.IClick(driver, Elements.MyAnalog_Dashboard_AnalogDialogue_Tiles_OtherOption_Icons);

                //--- Expected Result: Option to save is displayed ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_AnalogDialogue_Tiles_Save_Buttons);

                //--- Expected Result: Option to Copy Link is displayed ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_AnalogDialogue_Tiles_CopyLink_Buttons);
            }
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", "Webcasts", TestName = "Verify that the Webcasts Tab is Present and Working as Expected in EN Locale")]
        [TestCase("zh", "在线研讨会", TestName = "Verify that the Webcasts Tab is Present and Working as Expected in CN Locale")]
        [TestCase("jp", "ウェブキャスト", TestName = "Verify that the Webcasts Tab is Present and Working as Expected in JP Locale")]
        public void MyAnalog_Dashboard_RelatedContentSection_VerifyWebcastsTab(string Locale, string Webcasts_Text)
        {
            Console.WriteLine("Steps to Test:");

            //--- Login Credentials ---//
            string username = null;
            string password = null;
            if (Locale.Equals("zh") || Locale.Equals("cn"))
            {
                username = cn_username;
                password = cn_password;
            }
            else if (Locale.Equals("jp"))
            {
                username = jp_username;
                password = jp_password;
            }
            else
            {
                username = en_username;
                password = en_password;
            }

            //--- myAnalog Dashboard URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_dashboard_url;

            //--- Action: Go to myAnalog dashboard ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid user account in the myAnalog Login page ---//
            action.ILogin(driver, username, password);
            Thread.Sleep(4000);

            //--- Expected Result: Related Content Tabs must be Present ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_RelatedContent_Tabs);

            if (!util.GetText(driver, Elements.MyAnalog_Dashboard_RelatedContent_Selected_Tab).Equals(Webcasts_Text) && !util.GetText(driver, Elements.MyAnalog_Dashboard_RelatedContent_Selected_Tab).Equals("Webcasts"))
            {
                //--- This looks for the Webcasts Tab ---//
                int relatedContent_tabs_count = util.GetCount(driver, Elements.MyAnalog_Dashboard_RelatedContent_Tabs);

                for (int counter = 1; counter <= relatedContent_tabs_count; counter++)
                {
                    if (util.GetText(driver, By.CssSelector("ul[class='nav nav-tabs']>li:nth-last-of-type(" + counter + ")>a")).Equals(Webcasts_Text) || util.GetText(driver, By.CssSelector("ul[class='nav nav-tabs']>li:nth-last-of-type(" + counter + ")>a")).Equals("Webcasts"))
                    {
                        //--- Locators ---//
                        string webcasts_tab_locator = "ul[class='nav nav-tabs']>li:nth-last-of-type(" + counter + ")>a";

                        //--- Action: Click Webcasts tab ---//
                        action.IClick(driver, By.CssSelector(webcasts_tab_locator));
                        Thread.Sleep(2000);

                        break;
                    }

                    if (counter == relatedContent_tabs_count)
                    {
                        //--- Expected Result: Webcasts Tab must be Present ---//
                        test.validateElementIsPresent(driver, By.CssSelector("ul[class='nav nav-tabs']>li:nth-last-of-type(" + (counter + 1) + ")>a"));
                    }
                }
            }

            if (util.CheckElement(driver, Elements.MyAnalog_Dashboard_Webcasts_Tiles, 5))
            {
                //----- Dashboard Test Plan > Desktop Tab > R7 > T7.7: Verify the maximum number of display allowable for Webcasts Tile -----//

                //--- Expected Result: 12 is the max dispay ---//
                test.validateCountIsLessOrEqual(driver, util.GetCount(driver, Elements.MyAnalog_Dashboard_Webcasts_Tiles), 12);

                //----- Dashboard Test Plan > Desktop Tab > R7 > T7.4: Verify navigation when clicking Webcast title link -----//

                //--- Expected Result: Webcast title are Present ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_Webcasts_Tiles_Title_Links);

                if (util.CheckElement(driver, Elements.MyAnalog_Dashboard_Webcasts_Tiles_Title_Links, 1))
                {
                    //--- Action: Click Webcast title link ---//
                    action.IOpenLinkInNewTab(driver, Elements.MyAnalog_Dashboard_Webcasts_Tiles_Title_Links);

                    //--- Expected Result: Link redirect to Webcast page ---//
                    test.validateStringInstance(driver, driver.Url, webcasts_url_format);

                    driver.Close();
                    driver.SwitchTo().Window(driver.WindowHandles.Last());
                }

                //----- Dashboard Test Plan > Desktop Tab > R7 > T7.5: Verify tags available -----//

                //--- Expected Result: Tags are displayed ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_1st_Webcasts_Tile_Tag_Links);

                if (util.CheckElement(driver, Elements.MyAnalog_Dashboard_1st_Webcasts_Tile_Tag_Links, 1))
                {
                    //----- Dashboard Test Plan > Desktop Tab > R7 > T7.5.1: Verify the maximum number of tags should display (IQ-6374/AL-13502) -----//

                    //--- Expected Result: Should show no more than 5 in collapsed view ---//
                    test.validateCountIsLessOrEqual(driver, util.GetCount(driver, Elements.MyAnalog_Dashboard_1st_Webcasts_Tile_Tag_Links), 5);
                }

                if (util.CheckElement(driver, Elements.MyAnalog_Dashboard_1st_Webcasts_Tile_ArrowDown_Button, 1))
                {
                    //----- Dashboard Test Plan > Desktop Tab > R7 > T7.4.3: Verify what will happen if the maximum number of allowable tags is reach -----//

                    //--- Action: Click arrow down ---//
                    action.IClick(driver, Elements.MyAnalog_Dashboard_1st_Webcasts_Tile_ArrowDown_Button);

                    //--- Expected Result: List of other tags are displayed ---//
                    test.validateCountIsGreater(driver, util.GetCount(driver, Elements.MyAnalog_Dashboard_1st_Webcasts_Tile_Tag_Links), 5);

                    //--- Action: Click arrow up ---//
                    action.IClick(driver, Elements.MyAnalog_Dashboard_1st_Webcasts_Tile_ArrowUp_Button);

                    //--- Expected Result: List set to collapse ---//
                    test.validateCountIsLessOrEqual(driver, util.GetCount(driver, Elements.MyAnalog_Dashboard_1st_Webcasts_Tile_Tag_Links), 5);
                }

                //----- Dashboard Test Plan > Desktop Tab > R7 > T7.5: Verify when clicking ellipsis Webcast tile -----//

                //--- Action: Click ellipsis in webcast tile ---//
                action.IClick(driver, Elements.MyAnalog_Dashboard_Webcasts_Tiles_OtherOption_Icons);

                //--- Expected Result: Option to save is displayed ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_Webcasts_Tiles_Save_Buttons);

                //--- Expected Result: Option to Copy Link is displayed ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_Webcasts_Tiles_CopyLink_Buttons);
            }
        }
    }
}