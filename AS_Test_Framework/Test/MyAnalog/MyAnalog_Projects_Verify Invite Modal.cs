﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_Projects_InviteModal : BaseSetUp
    {
        //--- myAnalog Projects URL: ---//
        //--- DEV: https://my-dev.corpnt.analog.com/en/app/projects ---//
        //--- QA: https://my-qa.corpnt.analog.com/en/app/projects ---//
        //--- PROD: https://my.analog.com/en/app/projects ---//

        public MyAnalog_Projects_InviteModal() : base() { }

        //----- Note: Please don't edit/delete any details on the Accounts below -----//

        //--- Login Credentials ---//
        string username = "my_t3st_pr0j3cts_555@mailinator.com";
        string password = "Test_1234";

        //--- URLs ---//
        string myAnalog_projects_url = "app/projects";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Invite Modal is Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Invite Modal is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Invite Modal is Present and Working as Expected in JP Locale")]
        public void MyAnalog_Projects_VerifyInviteModal(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Projects URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_projects_url;

            //--- Action: Open myAnalog Projects using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify the Invite button -----//

            //--- Action: Click the invite button ---//
            action.IClick(driver, Elements.MyAnalog_Projects_Invite_Button);

            //--- Expected Result: The Invite pop up/modal should appear on the screen ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Projects_Invite_Modal);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify the title is present in Invite pop-up -----//

            if (Locale.Equals("en"))
            {
                //--- Expected Result: The title "Invite" should be present on the pop-up ---//
                test.validateStringIsCorrect(driver, Elements.MyAnalog_Projects_Invite_Modal_Header_Label, "Invite");
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.MyAnalog_Projects_Invite_Modal_Header_Label);
            }

            //--- Expected Result: The project name should display ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Projects_Invite_Modal_Project_Name_Label);

            //----- QUICK MESSAGE -----//

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify the Quick Message -----//

            //--- Expected Result: The quick message should be present ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Projects_Invite_Modal_QuickMessage_Buttons);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify adding quick message when there is already a message present on Message field -----//

            //--- Action: Input value on Message field ---//
            string message_input = "Message_" + util.RandomString();
            action.IDeleteValueOnFields(driver, Elements.MyAnalog_Projects_Invite_Modal_Message_InputBox);
            action.IType(driver, Elements.MyAnalog_Projects_Invite_Modal_Message_InputBox, message_input);

            //--- Action: Click on any of the quick message ---//
            action.IClick(driver, Elements.MyAnalog_Projects_Invite_Modal_QuickMessage_Buttons);

            //--- Expected Result: The message should be replaced by the selected quick message ---//            
            test.validateStringInstance(driver, util.GetText(driver, Elements.MyAnalog_Projects_Invite_Modal_QuickMessage_Buttons), util.GetInputBoxValue(driver, Elements.MyAnalog_Projects_Invite_Modal_Message_InputBox));

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify the Invite button in invite modal using ie11 and firefox browsers (IQ-8823/ALA-138433) -----//      
            if (Locale.Equals("en"))
            {
                //--- Expected Result: the label of invite button is "Invite"---//
                test.validateStringIsCorrect(driver, Elements.MyAnalog_Projects_Invite_Modal_Invite_Button, "Invite");
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.MyAnalog_Projects_Invite_Modal_Invite_Button);
            }
        }
    }
}