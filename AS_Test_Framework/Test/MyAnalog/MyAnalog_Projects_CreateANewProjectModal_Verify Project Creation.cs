﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System;
using System.Threading;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_Projects_CreateANewProjectModal_ProjectCreation : BaseSetUp
    {
        //--- myAnalog Projects URL: ---//
        //--- DEV: https://my-dev.corpnt.analog.com/en/app/projects ---//
        //--- QA: https://my-qa.corpnt.analog.com/en/app/projects ---//
        //--- PROD: https://my.analog.com/en/app/projects ---//

        public MyAnalog_Projects_CreateANewProjectModal_ProjectCreation() : base() { }

        //----- Note: Please don't edit/delete any details on the Accounts below -----//

        //--- Login Credentials ---//
        string username = "my_t3st_cr34t3_pr0j3cts_555@mailinator.com";
        string password = "Test_1234";

        //--- Login Credentials (Alternate Account if the Daily Project Creation Limit is Reached) ---//
        string username2 = "my_t3st_cr34t3_pr0j3cts_777@mailinator.com";
        string password2 = "Test_1234";

        //--- URLs ---//
        string myAnalog_projects_url = "app/projects";
        string myAnalog_logout_url = "app/logout";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        //[TestCase("en", TestName = "Verify that the Project Creation with Supplying Only the Required Fields is Working as Expected in EN Locale")] -> Note: This was Commented Out because there's a Daily Project Creation Limit of 5.
        [TestCase("zh", TestName = "Verify that the Project Creation with Supplying Only the Required Fields is Working as Expected in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Project Creation with Supplying Only the Required Fields is Working as Expected in JP Locale")] -> Note: This was Commented Out because there's a Daily Project Creation Limit of 5.
        public void MyAnalog_Projects_CreateANewProjectModal_ProjectCreation_VerifySupplyingOnlyRequiredFields(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Projects URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_projects_url;

            //--- Action: Open myAnalog Projects using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);

            //--- Action: Click Create A New Project to open the pop-up ---//
            action.IClick(driver, Elements.MyAnalog_Projects_CreateANewProject_Button);

            if (util.CheckElement(driver, Elements.MyAnalog_Projects_CannotCreateProject_Modal, 2))
            {
                action.IClick(driver, Elements.MyAnalog_Projects_CannotCreateProject_Modal_Ok_Button);

                string logout_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_logout_url;
                driver.Navigate().GoToUrl(logout_url);
                Console.WriteLine("Go to Log Out URL: " + logout_url);

                //--- Action: Open myAnalog Projects using direct link ---//
                action.Navigate(driver, myAnalog_url);

                //--- Action: Login valid credentials and click login ---//
                action.ILogin(driver, username2, password2);

                //--- Action: Click Create A New Project to open the pop-up ---//
                action.IClick(driver, Elements.MyAnalog_Projects_CreateANewProject_Button);
            }

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify project name should accept alpha numeric characters -----//

            //--- Action: Input alphanumeric value on Project Name field ---//
            string projectName_input = "ProjectName_" + util.RandomString();
            action.IDeleteValueOnFields(driver, Elements.MyAnalog_Projects_CreateANewProject_Modal_ProjectName_InputBox);
            action.IType(driver, Elements.MyAnalog_Projects_CreateANewProject_Modal_ProjectName_InputBox, projectName_input);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify Project Description should be an optional field -----//

            //--- Action: Do not input value on Project Description field ---//
            action.IDeleteValueOnFields(driver, Elements.MyAnalog_Projects_CreateANewProject_Modal_ProjectDescription_InputBox);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify Invitees should be an optional field -----//

            //--- Action: Do not input value on Invitees field ---//
            action.IDeleteValueOnFields(driver, Elements.MyAnalog_Projects_CreateANewProject_Modal_Invitees_InputBox);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify Message should be an optional field -----//

            //--- Action: Do not input value on Message field ---//
            action.IDeleteValueOnFields(driver, Elements.MyAnalog_Projects_CreateANewProject_Modal_Message_InputBox);

            action.IClick(driver, Elements.MyAnalog_Projects_CreateANewProject_Modal_Done_Button);
            Thread.Sleep(1000);

            //--- Expected Result: Should be able to proceed and no error message should display. ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Projects_CreatedNewProject_Modal_Message_Label_Line1);
            test.validateElementIsPresent(driver, Elements.MyAnalog_Projects_CreatedNewProject_Modal_Message_Label_Line2);
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Project Creation with Supplying All Fields is Working as Expected in EN Locale")]
        //[TestCase("zh", TestName = "Verify that the Project Creation with Supplying All Fields is Working as Expected in CN Locale")] -> Note: This was Commented Out because there's a Daily Project Creation Limit of 5.
        //[TestCase("jp", TestName = "Verify that the Project Creation with Supplying All Fields is Working as Expected in JP Locale")] -> Note: This was Commented Out because there's a Daily Project Creation Limit of 5.
        public void MyAnalog_Projects_CreateANewProjectModal_ProjectCreation_VerifySupplyingAllFields(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Projects URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_projects_url;

            //--- Action: Open myAnalog Projects using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);

            //--- Action: Click Create A New Project to open the pop-up ---//
            action.IClick(driver, Elements.MyAnalog_Projects_CreateANewProject_Button);

            if (util.CheckElement(driver, Elements.MyAnalog_Projects_CannotCreateProject_Modal, 2))
            {
                action.IClick(driver, Elements.MyAnalog_Projects_CannotCreateProject_Modal_Ok_Button);

                string logout_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_logout_url;
                driver.Navigate().GoToUrl(logout_url);
                Console.WriteLine("Go to Log Out URL: " + logout_url);

                //--- Action: Open myAnalog Projects using direct link ---//
                action.Navigate(driver, myAnalog_url);

                //--- Action: Login valid credentials and click login ---//
                action.ILogin(driver, username2, password2);

                //--- Action: Click Create A New Project to open the pop-up ---//
                action.IClick(driver, Elements.MyAnalog_Projects_CreateANewProject_Button);
            }

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify  Project Description should accept alpha numeric characters -----//

            //--- Action: Input alphanumeric value on  Project Description field ---//
            string projectDescription_input = "ProjectDescription_" + util.RandomString();
            action.IDeleteValueOnFields(driver, Elements.MyAnalog_Projects_CreateANewProject_Modal_ProjectDescription_InputBox);
            action.IType(driver, Elements.MyAnalog_Projects_CreateANewProject_Modal_ProjectDescription_InputBox, projectDescription_input);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify  Message field should accept alpha numeric characters -----//

            //--- Action: Input alphanumeric value on  Message field ---//
            string message_input = "Message_" + util.RandomString();
            action.IDeleteValueOnFields(driver, Elements.MyAnalog_Projects_CreateANewProject_Modal_Message_InputBox);
            action.IType(driver, Elements.MyAnalog_Projects_CreateANewProject_Modal_Message_InputBox, message_input);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify Invitees field with valid single email address -----//

            //--- Action: Input single valid email address on Invitees field ---//
            string invitees_input = "t3st_" + Util.Configuration.Environment.ToLower() + "_CreateANewProject_Invitee_" + util.GenerateRandomNumber() + "@mailinator.com";
            action.IDeleteValueOnFields(driver, Elements.MyAnalog_Projects_CreateANewProject_Modal_Invitees_InputBox);
            action.IType(driver, Elements.MyAnalog_Projects_CreateANewProject_Modal_Invitees_InputBox, invitees_input);

            //--- Action: Input valid values on other fields ---//
            string projectName_input = "ProjectName_" + util.RandomString();
            action.IDeleteValueOnFields(driver, Elements.MyAnalog_Projects_CreateANewProject_Modal_ProjectName_InputBox);
            action.IType(driver, Elements.MyAnalog_Projects_CreateANewProject_Modal_ProjectName_InputBox, projectName_input);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify successful creation of Project upon deletion of Invitee (IQ-8907/AL-15739) -----//

            //--- Action: Click the Done button ---//
            action.IClick(driver, Elements.MyAnalog_Projects_CreateANewProject_Modal_Done_Button);
            Thread.Sleep(2000);

            //--- Expected Result: The System displays a 'Create a new project' popup box -----//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Projects_CreatedNewProject_Modal);

            if (Locale.Equals("en"))
            {
                //--- Expected Result: The System displays a 'Create a new project' popup box with below message: ---//
                //--- You successfully created a project ---//
                test.validateStringIsCorrect(driver, Elements.MyAnalog_Projects_CreatedNewProject_Modal_Message_Label_Line1, "You successfully created a project");
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.MyAnalog_Projects_CreatedNewProject_Modal_Message_Label_Line1);
            }

            //--- Expected Result: The System displays a 'Create a new project' popup box with below message: ---//
            //--- <Project Name> ---//
            test.validateStringIsCorrect(driver, Elements.MyAnalog_Projects_CreatedNewProject_Modal_Message_Label_Line2, projectName_input);

            //--- Action: Click the 'Ok' button in the 'Create a new project' popup box ---//
            action.IClick(driver, Elements.MyAnalog_Projects_CreatedNewProject_Modal_Ok_Button);

            //--- Expected Result:  The 'Create a new project' popup box will close ---//
            test.validateElementIsNotPresent(driver, Elements.MyAnalog_Projects_CreatedNewProject_Modal);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify newly created project should automatically display in the first position of the list on Project Page without refreshing the page -----//

            //--- Expected Result: The newly create project will automatically display in the first position of the list on Project Page. ---//
            test.validateStringIsCorrect(driver, Elements.MyAnalog_Projects_First_Project_Name_Link, projectName_input);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify Invitees field with valid single email address -----//

            //--- Action: Go to the Mailbox of the Invitee ---//
            action.ICheckMailinatorEmail(driver, invitees_input);

            //--- Expected Result: The email adress should receive email notification ---//
            test.validateElementIsPresent(driver, Elements.Mailinator_LatestEmail);
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        //[TestCase("en", TestName = "Verify that the Validation for Creating 6 Projects is Working as Expected in EN Locale")] -> Note: This was Commented Out because there's a Daily Project Creation Limit of 5.
        //[TestCase("zh", TestName = "Verify that the Validation for Creating 6 Projects is Working as Expected in CN Locale")] -> Note: This was Commented Out because there's a Daily Project Creation Limit of 5.
        [TestCase("jp", TestName = "Verify that the Validation for Creating 6 Projects is Working as Expected in JP Locale")]
        public void MyAnalog_Projects_CreateANewProjectModal_ProjectCreation_VerifyValidtionForCreating6Projects(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Projects URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_projects_url;

            //--- Action: Open myAnalog Projects using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify that the user should be denied in creation of 6th project (IQ-7920/AL-14724) -----//

            //--- Action: Click Create A New Project to open the pop-up ---//
            action.IClick(driver, Elements.MyAnalog_Projects_CreateANewProject_Button);

            while (!util.CheckElement(driver, Elements.MyAnalog_Projects_CannotCreateProject_Modal, 5))
            {
                //--- Action: Attempt to add 6 projects ---//

                //--- Action: Input alphanumeric value on Project Name field ---//
                string projectName_input = "ProjectName_" + util.RandomString();
                action.IDeleteValueOnFields(driver, Elements.MyAnalog_Projects_CreateANewProject_Modal_ProjectName_InputBox);
                action.IType(driver, Elements.MyAnalog_Projects_CreateANewProject_Modal_ProjectName_InputBox, projectName_input);

                //--- Action: Click the Done button ---//
                action.IClick(driver, Elements.MyAnalog_Projects_CreateANewProject_Modal_Done_Button);
                Thread.Sleep(1000);

                //--- Action: Click the 'Ok' button in the 'Create a new project' popup box ---//
                action.IClick(driver, Elements.MyAnalog_Projects_CreatedNewProject_Modal_Ok_Button);

                //--- Action: Click Create A New Project to open the pop-up ---//
                action.IClick(driver, Elements.MyAnalog_Projects_CreateANewProject_Button);
            }

            //--- Expected Result: Error message/prompt is displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Projects_CannotCreateProject_Modal);
        }
    }
}