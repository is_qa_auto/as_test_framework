﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System;
using System.Threading;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_MegaMenu_MarketsTile : BaseSetUp
    {
        public MyAnalog_MegaMenu_MarketsTile() : base() { }

        //-- Login Credentials --//
        string username = "myanalog_t3st_m4rk3ts_555@mailinator.com";
        string password = "Test_1234";

        //--- myAnalog Dashboard URL: ---//
        //--- DEV: https://my-dev.corpnt.analog.com/en/app ---//
        //--- QA: https://my-qa.corpnt.analog.com/en/app ---//
        //--- PROD: https://my.analog.com/en/app ---//

        //--- URLs ---//
        string myAnalog_dashboard_url = "app";
        string applications_markets_page_url_format = "/applications/markets/";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Markets Tile is Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Markets Tile is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Markets Tile is Present and Working as Expected in JP Locale")]
        public void MyAnalog_MegaMenu_VerifyMarketsTile(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Dashboard URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_dashboard_url;

            action.Navigate(driver, myAnalog_url);

            //--- Action: Log in valid account in login page ---//
            action.ILogin(driver, username, password);

            //--- Action: click myanalog tab ---//
            action.IExpandMyAnalogMegaMenu(driver);

            //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R2 > T2: Verify myPage Components -----//

            //--- Expected Result: The Market tileis displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_MegaMenu_Markets_Tile);

            //----- MARKETS TITLE -----//

            //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R10 > T1: Verify Market component -----//

            //--- Expected Result: The Market title is displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_MegaMenu_Markets_Link);

            //----- SAVED MARKETS -----//

            //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R10 > T1: Verify Category component -----//

            //--- Expected Result: The Market link are displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_MegaMenu_Saved_Market_Links);

            if (util.CheckElement(driver, Elements.MyAnalog_MegaMenu_Saved_Market_Links, 2))
            {
                string market_link = util.GetText(driver, Elements.MyAnalog_MegaMenu_Saved_Market_Links);

                //--- Expected Result: 10 most recent items ---//
                test.validateCountIsLessOrEqual(driver, util.GetCount(driver, Elements.MyAnalog_MegaMenu_Saved_Market_Links), 10);

                //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R10 > T2: Verify when clicking Market link -----//

                //--- Action: Verify when clicking Market link ---//
                action.IOpenLinkInNewTab(driver, Elements.MyAnalog_MegaMenu_Saved_Market_Links);
                Thread.Sleep(1000);

                //--- Expected Result: Page will redirect to correct page ---//
                test.validateStringInstance(driver, driver.Url, applications_markets_page_url_format);
            }
        }
    }
}