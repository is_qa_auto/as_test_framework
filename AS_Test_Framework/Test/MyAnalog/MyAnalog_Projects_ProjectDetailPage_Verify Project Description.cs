﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_Projects_ProjectDetailPage_ProjectDescription : BaseSetUp
    {
        //--- myAnalog Projects URL: ---//
        //--- DEV: https://my-dev.corpnt.analog.com/en/app/projects ---//
        //--- QA: https://my-qa.corpnt.analog.com/en/app/projects ---//
        //--- PROD: https://my.analog.com/en/app/projects ---//

        public MyAnalog_Projects_ProjectDetailPage_ProjectDescription() : base() { }

        //----- Note: Please don't edit/delete any details on the Accounts below -----//

        //--- Login Credentials (Project Owner) ---// 
        string username = "my_t3st_pr0j3cts_555@mailinator.com";
        string password = "Test_1234";

        //--- URLs ---//
        string myAnalog_projects_url = "app/projects";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that Edit Description is Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that Edit Description is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that Edit Description is Present and Working as Expected in JP Locale")]
        public void MyAnalog_Projects_ProjectDetailPage_ProjectDescription_VerifyEditDescription(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Projects URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_projects_url;

            //--- Action: Open myAnalog Projects using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);

            //--- Action: Click Project Detail button ---//
            action.IClick(driver, Elements.MyAnalog_Projects_ProjectDetails_Buttons);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify Edit description is displayed -----//

            //--- Action: Click on the Edit/Remove option (3 bullet points) ---//
            action.IClick(driver, Elements.MyAnalog_ProjectDetailPage_Ellipsis_Button);

            //--- Expected Result: The Edit description Label is displayed ---//
            if (Locale.Equals("en"))
            {
                test.validateStringIsCorrect(driver, Elements.MyAnalog_ProjectDetailPage_EditDescription_Option, "Edit Description");
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.MyAnalog_ProjectDetailPage_EditDescription_Option);
            }

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify if the user can save the changes in project description (IQ-6383/AL-13241)-----//

            //--- Action: Click Edit Description ---//
            action.IClick(driver, Elements.MyAnalog_ProjectDetailPage_EditDescription_Option);

            //--- Action: Change the project description ---//
            string projectDescription_input = "Edit_ProjectDescription_" + util.RandomString();
            action.IDeleteValueOnFields(driver, Elements.MyAnalog_ProjectDetailPage_EditDescription_InputBox);
            action.IType(driver, Elements.MyAnalog_ProjectDetailPage_EditDescription_InputBox, projectDescription_input);

            //--- Action: Click Save ---//
            action.IClick(driver, Elements.MyAnalog_ProjectDetailPage_EditDescription_Save_Button);

            //--- Expected Result: The project description is changed. ---//  
            test.validateStringIsCorrect(driver, Elements.MyAnalog_ProjectDetailPage_ProjectDescription_Label, projectDescription_input);
        }
    }
}