﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using OpenQA.Selenium;
using System;
using System.Threading;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_Dashboard_RelatedContentSection_Part5 : BaseSetUp
    {
        public MyAnalog_Dashboard_RelatedContentSection_Part5() : base() { }

        //-- Login Credentials --//
        string en_username = "myanalog_t3st_888@mailinator.com";
        string en_password = "Test_1234";
        string cn_username = "test_zh_04252019@mailinator.com";
        string cn_password = "Test_1234";
        string jp_username = "test_jp_04252019@mailinator.com";
        string jp_password = "Test_1234";

        //--- URLs ---//
        string myAnalog_dashboard_url = "app";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", "Solutions Bulletins & Brochures", TestName = "Verify that the Solutions Bulletins and Brochures Tab is Present and Working as Expected in EN Locale")]
        [TestCase("zh", "解决方案通报和手册", TestName = "Verify that the Solutions Bulletins and Brochures Tab is Present and Working as Expected in CN Locale")]
        [TestCase("jp", "カタログ", TestName = "Verify that the Solutions Bulletins and Brochures Tab is Present and Working as Expected in JP Locale")]
        public void MyAnalog_Dashboard_RelatedContentSection_VerifySolutionsBulletinsAndBrochuresTab(string Locale, string SolutionsBulletinsAndBrochures_Text)
        {
            Console.WriteLine("Steps to Test:");

            //--- Login Credentials ---//
            string username = null;
            string password = null;
            if (Locale.Equals("zh") || Locale.Equals("cn"))
            {
                username = cn_username;
                password = cn_password;
            }
            else if (Locale.Equals("jp"))
            {
                username = jp_username;
                password = jp_password;
            }
            else
            {
                username = en_username;
                password = en_password;
            }

            //--- myAnalog Dashboard URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_dashboard_url;

            //--- Action: Go to myAnalog dashboard ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid user account in the myAnalog Login page ---//
            action.ILogin(driver, username, password);
            Thread.Sleep(4000);

            //--- Expected Result: Related Content Tabs must be Present ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_RelatedContent_Tabs);

            if (!util.GetText(driver, Elements.MyAnalog_Dashboard_RelatedContent_Selected_Tab).Equals(SolutionsBulletinsAndBrochures_Text) && !util.GetText(driver, Elements.MyAnalog_Dashboard_RelatedContent_Selected_Tab).Equals("Solutions Bulletins & Brochures"))
            {
                //--- This looks for the Solutions Bulletins & Brochures Tab ---//
                int relatedContent_tabs_count = util.GetCount(driver, Elements.MyAnalog_Dashboard_RelatedContent_Tabs);

                for (int counter = 1; counter <= relatedContent_tabs_count; counter++)
                {
                    if (util.GetText(driver, By.CssSelector("ul[class='nav nav-tabs']>li:nth-last-of-type(" + counter + ")>a")).Equals(SolutionsBulletinsAndBrochures_Text) || util.GetText(driver, By.CssSelector("ul[class='nav nav-tabs']>li:nth-last-of-type(" + counter + ")>a")).Equals("Solutions Bulletins & Brochures"))
                    {
                        //--- Locators ---//
                        string solutionsBulletinsAndBrochures_tab_locator = "ul[class='nav nav-tabs']>li:nth-last-of-type(" + counter + ")>a";

                        //--- Action: Click Solutions Bulletins & Brochures tab ---//
                        action.IClick(driver, By.CssSelector(solutionsBulletinsAndBrochures_tab_locator));
                        Thread.Sleep(2000);

                        break;
                    }

                    if (counter == relatedContent_tabs_count)
                    {
                        //--- Expected Result: Solutions Bulletins & Brochures Tab must be Present ---//
                        test.validateElementIsPresent(driver, By.CssSelector("ul[class='nav nav-tabs']>li:nth-last-of-type(" + (counter + 1) + ")>a"));
                    }
                }
            }

            if (util.CheckElement(driver, Elements.MyAnalog_Dashboard_SolutionsBulletinsAndBrochures_Tiles, 5))
            {
                //----- Dashboard Test Plan > Desktop Tab > R7 > T9: Verify the component of Solutions Bulletins & Brochure tile -----//

                //--- Expected Result: Solutions Bulletins & Brochure  Label are Present ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_SolutionsBulletinsAndBrochures_Tiles_SolutionsBulletinsAndBrochures_Labels);

                //--- Expected Result: Solutions Bulletins & Brochure   title are Present ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_SolutionsBulletinsAndBrochures_Tiles_Title_Links);

                //--- Expected Result: Tag Labels are Present ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_1st_SolutionsBulletinsAndBrochures_Tile_Tag_Links);

                if (util.CheckElement(driver, Elements.MyAnalog_Dashboard_1st_SolutionsBulletinsAndBrochures_Tile_Tag_Links, 1))
                {
                    //----- Dashboard Test Plan > Desktop Tab > R7 > T9.4.1: Verify the maximum number of tags should display (IQ-6374/AL-13502) -----//

                    //--- Expected Result: Should show no more than 5 in collapsed view ---//
                    test.validateCountIsLessOrEqual(driver, util.GetCount(driver, Elements.MyAnalog_Dashboard_1st_SolutionsBulletinsAndBrochures_Tile_Tag_Links), 5);
                }

                //----- Dashboard Test Plan > Desktop Tab > R7 > T9: Verify the component of Solutions Bulletins & Brochure tile -----//

                //--- Action: Click ellipsis in Solutions Bulletins & Brochure tile ---//
                action.IClick(driver, Elements.MyAnalog_Dashboard_SolutionsBulletinsAndBrochures_Tiles_OtherOption_Icons);

                //--- Expected Result: Save option are Present ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_SolutionsBulletinsAndBrochures_Tiles_Save_Buttons);

                //--- Expected Result: Copy Link option are Present ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_SolutionsBulletinsAndBrochures_Tiles_CopyLink_Buttons);
            }
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", "White Papers", TestName = "Verify that the White Papers Tab is Present and Working as Expected in EN Locale")]
        [TestCase("zh", "白皮书", TestName = "Verify that the White Papers Tab is Present and Working as Expected in CN Locale")]
        [TestCase("jp", "ホワイト・ペーパー", TestName = "Verify that the White Papers Tab is Present and Working as Expected in JP Locale")]
        public void MyAnalog_Dashboard_RelatedContentSection_VerifyWhitePapersTab(string Locale, string WhitePapers_Text)
        {
            Console.WriteLine("Steps to Test:");

            //--- Login Credentials ---//
            string username = null;
            string password = null;
            if (Locale.Equals("zh") || Locale.Equals("cn"))
            {
                username = cn_username;
                password = cn_password;
            }
            else if (Locale.Equals("jp"))
            {
                username = jp_username;
                password = jp_password;
            }
            else
            {
                username = en_username;
                password = en_password;
            }

            //--- myAnalog Dashboard URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_dashboard_url;

            //--- Action: Go to myAnalog dashboard ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid user account in the myAnalog Login page ---//
            action.ILogin(driver, username, password);
            Thread.Sleep(4000);

            //--- Expected Result: Related Content Tabs must be Present ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_RelatedContent_Tabs);

            if (!util.GetText(driver, Elements.MyAnalog_Dashboard_RelatedContent_Selected_Tab).Equals(WhitePapers_Text) && !util.GetText(driver, Elements.MyAnalog_Dashboard_RelatedContent_Selected_Tab).Equals("White Papers"))
            {
                //--- This looks for the White Papers Tab ---//
                int relatedContent_tabs_count = util.GetCount(driver, Elements.MyAnalog_Dashboard_RelatedContent_Tabs);

                for (int counter = 1; counter <= relatedContent_tabs_count; counter++)
                {
                    if (util.GetText(driver, By.CssSelector("ul[class='nav nav-tabs']>li:nth-last-of-type(" + counter + ")>a")).Equals(WhitePapers_Text) || util.GetText(driver, By.CssSelector("ul[class='nav nav-tabs']>li:nth-last-of-type(" + counter + ")>a")).Equals("White Papers"))
                    {
                        //--- Locators ---//
                        string whitePapers_tab_locator = "ul[class='nav nav-tabs']>li:nth-last-of-type(" + counter + ")>a";

                        //--- Action: Click White Papers tab ---//
                        action.IClick(driver, By.CssSelector(whitePapers_tab_locator));
                        Thread.Sleep(2000);

                        break;
                    }

                    if (counter == relatedContent_tabs_count)
                    {
                        //--- Expected Result: White Papers Tab must be Present ---//
                        test.validateElementIsPresent(driver, By.CssSelector("ul[class='nav nav-tabs']>li:nth-last-of-type(" + (counter + 1) + ")>a"));
                    }
                }
            }

            if (util.CheckElement(driver, Elements.MyAnalog_Dashboard_WhitePapers_Tiles, 5))
            {
                //----- Dashboard Test Plan > Desktop Tab > R7 > T10.1: Verify the White Paper component -----//

                //--- Expected Result: White Papers  Label are Present ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_WhitePapers_Tiles_WhitePapers_Labels);

                //--- Expected Result: White Papers  title are Present ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_WhitePapers_Tiles_Title_Links);

                //--- Expected Result: Tag Labels  are Present ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_1st_WhitePapers_Tile_Tag_Links);

                //--- Expected Result: Tag Labels  are Present ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_1st_WhitePapers_Tile_Tag_Links);

                if (util.CheckElement(driver, Elements.MyAnalog_Dashboard_1st_WhitePapers_Tile_Tag_Links, 1))
                {
                    //----- Dashboard Test Plan > Desktop Tab > R7 > T10.5.1: Verify the maximum number of tags should display (IQ-6374/AL-13502) -----//

                    //--- Expected Result: Should show no more than 5 in collapsed view ---//
                    test.validateCountIsLessOrEqual(driver, util.GetCount(driver, Elements.MyAnalog_Dashboard_1st_WhitePapers_Tile_Tag_Links), 5);
                }

                //----- Dashboard Test Plan > Desktop Tab > R7 > T10.1: Verify the White Paper component -----//

                //--- Action: Click ellipsis in webcast tile ---//
                action.IClick(driver, Elements.MyAnalog_Dashboard_WhitePapers_Tiles_OtherOption_Icons);

                //--- Expected Result: Save option are Present ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_WhitePapers_Tiles_Save_Buttons);

                //--- Expected Result: Copy Link option are Present ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_WhitePapers_Tiles_CopyLink_Buttons);
            }
        }
    }
}