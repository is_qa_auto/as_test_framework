﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_Dashboard_SavedProductsSection_NoSavedProducts : BaseSetUp
    {
        public MyAnalog_Dashboard_SavedProductsSection_NoSavedProducts() : base() { }

        //-- Login Credentials for New Account --//
        string username_new = "myanalog_t3st_new_555@mailinator.com";
        string password_new = "Test_1234";

        //--- URLs ---//
        string myAnalog_dashboard_url = "app";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Saved Products Section is Present and Working as Expected for New Accounts in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Saved Products Section is Present and Working as Expected for New Accounts in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Saved Products Section is Present and Working as Expected for New Accounts in JP Locale")]
        public void MyAnalog_Dashboard_SavedProductsSection_NoSavedProducts_VerifyForNewAccounts(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Dashboard URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_dashboard_url;

            //--- Action: Go to myAnalog dashboard ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid user account in the myAnalog Login page ---//
            action.ILogin(driver, username_new, password_new);

            //----- Dashboard Test Plan > Desktop Tab > R4 > T1.1.1: Verify Your Saved Product counter if shows Zero when none are saved. (IQ-6583/AL-13386) -----//

            //--- Expected Result: Saved product counter is shows Zero ---//
            test.validateStringInstance(driver, util.ExtractNumber(driver, Elements.MyAnalog_Dashboard_SavedProducts_Label), "0");

            //----- Dashboard Test Plan > Desktop Tab > R4 > T6: Verify when No Saved Products -----//

            //--- Expected Result: A message display "Add products to myAnalog to receive important updates" ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_SavedProducts_NoData_Text);
        }
    }
}