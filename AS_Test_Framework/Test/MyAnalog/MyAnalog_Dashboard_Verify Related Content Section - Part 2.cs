﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using OpenQA.Selenium;
using System;
using System.Threading;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_Dashboard_RelatedContentSection_Part2 : BaseSetUp
    {
        public MyAnalog_Dashboard_RelatedContentSection_Part2() : base() { }

        //-- Login Credentials --//
        string en_username = "myanalog_t3st_888@mailinator.com";
        string en_password = "Test_1234";
        string cn_username = "test_zh_04252019@mailinator.com";
        string cn_password = "Test_1234";
        string jp_username = "test_jp_04252019@mailinator.com";
        string jp_password = "Test_1234";

        //--- URLs ---//
        string myAnalog_dashboard_url = "app";
        string videos_url_format = "/videos/";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", "Videos", TestName = "Verify that the Videos Tab is Present and Working as Expected in EN Locale")]
        [TestCase("zh", "视频", TestName = "Verify that the Videos Tab is Present and Working as Expected in CN Locale")]
        [TestCase("jp", "ビデオ", TestName = "Verify that the Videos Tab is Present and Working as Expected in JP Locale")]
        public void MyAnalog_Dashboard_RelatedContentSection_VerifyVideosTab(string Locale, string Videos_Text)
        {
            Console.WriteLine("Steps to Test:");

            //--- Login Credentials ---//
            string username = null;
            string password = null;
            if (Locale.Equals("zh") || Locale.Equals("cn"))
            {
                username = cn_username;
                password = cn_password;
            }
            else if (Locale.Equals("jp"))
            {
                username = jp_username;
                password = jp_password;
            }
            else
            {
                username = en_username;
                password = en_password;
            }

            //--- myAnalog Dashboard URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_dashboard_url;

            //--- Action: Go to myAnalog dashboard ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid user account in the myAnalog Login page ---//
            action.ILogin(driver, username, password);
            Thread.Sleep(4000);

            //--- Expected Result: Related Content Tabs must be Present ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_RelatedContent_Tabs);

            if (!util.GetText(driver, Elements.MyAnalog_Dashboard_RelatedContent_Selected_Tab).Equals(Videos_Text) && !util.GetText(driver, Elements.MyAnalog_Dashboard_RelatedContent_Selected_Tab).Equals("Videos"))
            {
                //--- This looks for the Videos Tab ---//
                int relatedContent_tabs_count = util.GetCount(driver, Elements.MyAnalog_Dashboard_RelatedContent_Tabs);

                for (int counter = 1; counter <= relatedContent_tabs_count; counter++)
                {
                    if (util.GetText(driver, By.CssSelector("ul[class='nav nav-tabs']>li:nth-of-type(" + counter + ")>a")).Equals(Videos_Text) || util.GetText(driver, By.CssSelector("ul[class='nav nav-tabs']>li:nth-of-type(" + counter + ")>a")).Equals("Videos"))
                    {
                        //--- Locators ---//
                        string videos_tab_locator = "ul[class='nav nav-tabs']>li:nth-of-type(" + counter + ")>a";

                        //--- Action: Click Videos tab ---//
                        action.IClick(driver, By.CssSelector(videos_tab_locator));
                        Thread.Sleep(3000);

                        break;
                    }

                    if (counter == relatedContent_tabs_count)
                    {
                        //--- Expected Result: Videos Tab must be Present ---//
                        test.validateElementIsPresent(driver, By.CssSelector("ul[class='nav nav-tabs']>li:nth-of-type(" + (counter + 1) + ")>a"));
                    }
                }
            }

            if (util.CheckElement(driver, Elements.MyAnalog_Dashboard_Videos_Tiles, 5))
            {
                //----- Dashboard Test Plan > Desktop Tab > R7 > T4.7: Verify the maximum number of display allowable for Video Tile -----//

                //--- Expected Result: 12 video tiles is displayed ---//
                test.validateCountIsLessOrEqual(driver, util.GetCount(driver, Elements.MyAnalog_Dashboard_Videos_Tiles), 12);

                //----- Dashboard Test Plan > Desktop Tab > R7 > T4.1: Verify Video Tile component -----//

                //--- Expected Result: Videos Label are Present ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_Videos_Tiles_Videos_Labels);

                //--- Expected Result: Videos image are Present ---//
                test.validateImageIsNotBeBroken(driver, Elements.MyAnalog_Dashboard_Videos_Tiles_Thumbnail_Images);

                //--- Expected Result: List of applications are Present ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_1st_Videos_Tile_Tag_Links);

                if (util.CheckElement(driver, Elements.MyAnalog_Dashboard_1st_Videos_Tile_Tag_Links, 1))
                {
                    //----- Dashboard Test Plan > Desktop Tab > R7 > T4.4.1: Verify the maximum number of tags should display (IQ-6374/AL-13502) -----//

                    //--- Expected Result: Should show no more than 5 in collapsed view ---//
                    test.validateCountIsLessOrEqual(driver, util.GetCount(driver, Elements.MyAnalog_Dashboard_1st_Videos_Tile_Tag_Links), 5);
                }

                //----- Dashboard Test Plan > Desktop Tab > R7 > T4.1: Verify Video Tile component -----//

                //--- Expected Result: Video title are Present ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_Videos_Tiles_Title_Links);

                if (util.CheckElement(driver, Elements.MyAnalog_Dashboard_Videos_Tiles_Title_Links, 1))
                {
                    //----- Dashboard Test Plan > Desktop Tab > R7 > T4.3: Verify navigation when clicking Video Title -----//

                    //--- Action: Click video title ---//
                    action.IOpenLinkInNewTab(driver, Elements.MyAnalog_Dashboard_Videos_Tiles_Title_Links);

                    //--- Expected Result: Page will redirect to video page ---//
                    test.validateStringInstance(driver, driver.Url, videos_url_format);
                }
            }
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", "Technical Articles", TestName = "Verify that the Technical Articles Tab is Present and Working as Expected in EN Locale")]
        [TestCase("zh", "技术文章", TestName = "Verify that the Technical Articles Tab is Present and Working as Expected in CN Locale")]
        [TestCase("jp", "技術記事", TestName = "Verify that the Technical Articles Tab is Present and Working as Expected in JP Locale")]
        public void MyAnalog_Dashboard_RelatedContentSection_VerifyTechnicalArticlesTab(string Locale, string TechnicalArticles_Text)
        {
            Console.WriteLine("Steps to Test:");

            //--- Login Credentials ---//
            string username = null;
            string password = null;
            if (Locale.Equals("zh") || Locale.Equals("cn"))
            {
                username = cn_username;
                password = cn_password;
            }
            else if (Locale.Equals("jp"))
            {
                username = jp_username;
                password = jp_password;
            }
            else
            {
                username = en_username;
                password = en_password;
            }

            //--- myAnalog Dashboard URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_dashboard_url;

            //--- Action: Go to myAnalog dashboard ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid user account in the myAnalog Login page ---//
            action.ILogin(driver, username, password);
            Thread.Sleep(4000);

            //--- Expected Result: Related Content Tabs must be Present ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_RelatedContent_Tabs);

            if (!util.GetText(driver, Elements.MyAnalog_Dashboard_RelatedContent_Selected_Tab).Equals(TechnicalArticles_Text) && !util.GetText(driver, Elements.MyAnalog_Dashboard_RelatedContent_Selected_Tab).Equals("Technical Articles"))
            {
                //--- This looks for the Technical Articles Tab ---//
                int relatedContent_tabs_count = util.GetCount(driver, Elements.MyAnalog_Dashboard_RelatedContent_Tabs);

                for (int counter = 1; counter <= relatedContent_tabs_count; counter++)
                {
                    if (util.GetText(driver, By.CssSelector("ul[class='nav nav-tabs']>li:nth-of-type(" + counter + ")>a")).Equals(TechnicalArticles_Text) || util.GetText(driver, By.CssSelector("ul[class='nav nav-tabs']>li:nth-of-type(" + counter + ")>a")).Equals("Technical Articles"))
                    {
                        //--- Locators ---//
                        string technicalArticles_tab_locator = "ul[class='nav nav-tabs']>li:nth-of-type(" + counter + ")>a";

                        //--- Action: Click Technical Article tab ---//
                        action.IClick(driver, By.CssSelector(technicalArticles_tab_locator));
                        Thread.Sleep(2000);

                        break;
                    }

                    if (counter == relatedContent_tabs_count)
                    {
                        //--- Expected Result: Technical Articles Tab must be Present ---//
                        test.validateElementIsPresent(driver, By.CssSelector("ul[class='nav nav-tabs']>li:nth-of-type(" + (counter + 1) + ")>a"));
                    }
                }
            }

            if (util.CheckElement(driver, Elements.MyAnalog_Dashboard_TechnicalArticles_Tiles, 5))
            {
                //----- Dashboard Test Plan > Desktop Tab > R7 > T5.6: Verify the maximum number of display allowable for Technical Article Tile -----//

                //--- Expected Result: 12 is the max dispay ---//
                test.validateCountIsLessOrEqual(driver, util.GetCount(driver, Elements.MyAnalog_Dashboard_TechnicalArticles_Tiles), 12);

                //----- Dashboard Test Plan > Desktop Tab > R7 > T5.1: Verify the component of Technical Article  -----//

                //--- Expected Result: Technical Article Label are Present ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_TechnicalArticles_Tiles_TechnicalArticles_Labels);

                //--- Expected Result: Technical Article title are Present ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_TechnicalArticles_Tiles_Title_Links);

                //--- Expected Result: Tag label are Present ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_1st_TechnicalArticles_Tile_Tag_Links);

                if (util.CheckElement(driver, Elements.MyAnalog_Dashboard_1st_TechnicalArticles_Tile_Tag_Links, 1))
                {
                    //----- Dashboard Test Plan > Desktop Tab > R7 > T5.3.1: Verify the maximum number of tags should display (IQ-6374/AL-13502) -----//

                    //--- Expected Result: Should show no more than 5 in collapsed view ---//
                    test.validateCountIsLessOrEqual(driver, util.GetCount(driver, Elements.MyAnalog_Dashboard_1st_TechnicalArticles_Tile_Tag_Links), 5);
                }

                if (util.CheckElement(driver, Elements.MyAnalog_Dashboard_1st_TechnicalArticles_Tile_ArrowDown_Button, 1))
                {
                    //----- Dashboard Test Plan > Desktop Tab > R7 > T5.3.3: Verify what will happen if the maximum number of allowable tags is reach -----//

                    //--- Action: Click arrow down ---//
                    action.IClick(driver, Elements.MyAnalog_Dashboard_1st_TechnicalArticles_Tile_ArrowDown_Button);

                    //--- Expected Result: Othe tags should displayed ---//
                    test.validateCountIsGreater(driver, util.GetCount(driver, Elements.MyAnalog_Dashboard_1st_TechnicalArticles_Tile_Tag_Links), 5);
                }

                //----- Dashboard Test Plan > Desktop Tab > R7 > T5.4: Verify when clicking elipsis for Technical Article Tile -----//

                //--- Action: Click Edit Technical Article ---//
                action.IClick(driver, Elements.MyAnalog_Dashboard_TechnicalArticles_Tiles_OtherOption_Icons);

                //--- Expected Result: Option for save is available ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_TechnicalArticles_Tiles_Save_Buttons);

                //--- Expected Result: Option for Copy Link is available ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_TechnicalArticles_Tiles_CopyLink_Buttons);
            }
        }
    }
}