﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System;
using System.Threading;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_AccountSettings_PasswordSection : BaseSetUp
    {
        //--- myAnalog Account Settings URL: ---//
        //--- DEV: https://my-dev.corpnt.analog.com/en/app/account ---//
        //--- QA: https://my-qa.corpnt.analog.com/en/app/account ---//
        //--- PROD: https://my.analog.com/en/app/account ---//

        public MyAnalog_AccountSettings_PasswordSection() : base() { }

        //--- Login Credentials ---//
        string username = null;
        string password = null;

        //--- URLs ---//
        string myAnalog_accountSettings_url = "app/account";
        string passwordReset_url = "b2c_1a_adi_passwordreset/";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Elements are Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Elements are Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Elements are Present and Working as Expected in JP Locale")]
        public void MyAnalog_AccountSettings_VerifyPasswordSection(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- Login Credentials ---//
            if (Locale.Equals("zh") || Locale.Equals("cn"))
            {
                username = "myanalog_t3st_cn_777@mailinator.com";
            }
            else if (Locale.Equals("jp"))
            {
                username = "myanalog_t3st_jp_777@mailinator.com";
            }
            else
            {
                username = "myanalog_t3st_555@mailinator.com";
                //username = "myanalog_t3st_999@mailinator.com";
            }
            password = "Test_1234";

            //--- myAnalog Account Settings URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_accountSettings_url;

            //--- Action: Open account settings page ---//
            action.Navigate(driver, myAnalog_url);

            action.ILogin(driver, username, password);
            Thread.Sleep(3000);

            //----- Account Settings Test Plan > myAnalog Dashboard Tab > R2 > T4: Verify when clicking Edit Password section -----//

            //--- Expected Result: Password section is displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_AccountSettings_Password_Section);

            //----- Account Settings Test Plan > myAnalog Dashboard Tab > R2 > T1.2: Verify Password reset from account settings (AL-13597/IQ-6138) -----//

            ////--- Expected Result: The Password field is available in password section ---// -> The Password Field was removed as part of the changes for Restrict email change and password change (AL-18210).
            //test.validateElementIsPresent(driver, Elements.MyAnalog_AccountSettings_Password_Section_Password_InputBox);

            //--- Expected Result: The Reset Password button is available in password section ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_AccountSettings_Password_Section_ResetPassword_Button);

            //--- Action: Click reset Password button ---//
            action.IClick(driver, Elements.MyAnalog_AccountSettings_Password_Section_ResetPassword_Button);

            //--- Expected Result: Page will redirect to reset password page ---//
            test.validateStringInstance(driver, driver.Url, passwordReset_url);
        }
    }
}