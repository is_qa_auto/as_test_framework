﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_Projects_ProjectDetailPage_ProjectProducts : BaseSetUp
    {
        //--- myAnalog Projects URL: ---//
        //--- DEV: https://my-dev.corpnt.analog.com/en/app/projects ---//
        //--- QA: https://my-qa.corpnt.analog.com/en/app/projects ---//
        //--- PROD: https://my.analog.com/en/app/projects ---//

        public MyAnalog_Projects_ProjectDetailPage_ProjectProducts() : base() { }

        //----- Note: Please don't edit/delete any details on the Accounts below -----//

        //--- Login Credentials (This Account must have more than 10 Saved Project Products) ---// 
        string username = "my_t3st_pr0j3ct_d3t41ls_555@mailinator.com";
        string password = "Test_1234";

        //--- Login Credentials (Used for Editing Saved Products) ---// 
        string username_editSavedProducts = "my_t3st_3d1t_pr0j3ct_d3t41ls_555@mailinator.com";
        string password_editSavedProducts = "Test_1234";

        //--- URLs ---//
        string myAnalog_projects_url = "app/projects";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that Project Products is Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that Project Products is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that Project Products is Present and Working as Expected in JP Locale")]
        public void MyAnalog_Projects_ProjectDetailPage_VerifyProjectProducts(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Projects URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_projects_url;

            //--- Action: Open myAnalog Projects using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);

            //--- Action: Click Project Detail button ---//
            action.IClick(driver, Elements.MyAnalog_Projects_ProjectDetails_Buttons);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Navigate to Project Details Page -----//

            //--- Expected Result: The Project Product section component is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_ProjectDetailPage_ProjectProducts_Section);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify Projet Product -----//

            //--- Expected Result: The Add Products to myAnalog is displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_ProjectDetailPage_ProjectProducts_AddProductsToMyAnalog_InputBox);

            //--- Expected Result: The List of products is displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_ProjectDetailPage_SavedProducts);

            //--- Expected Result: The Edit option is displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_ProjectDetailPage_ProjectProducts_Edit_Button);

            //--- Expected Result: Pagination button "Previous" should be displayed. (IQ-6365/AL-13329) ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_ProjectDetailPage_ProjectProducts_Previous_Button);

            //--- Expected Result: Pagination button "Next" should be displayed. (IQ-6365/AL-13329) ---//     
            test.validateElementIsPresent(driver, Elements.MyAnalog_ProjectDetailPage_ProjectProducts_Next_Button);
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that Adding Products is Working as Expected in EN Locale")]
        public void MyAnalog_Projects_ProjectDetailPage_ProjectProducts_VerifyAddingProducts(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Projects URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_projects_url;

            //--- Action: Open myAnalog Projects using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username_editSavedProducts, password_editSavedProducts);

            //--- Action: Click Project Detail button ---//
            action.IClick(driver, Elements.MyAnalog_Projects_ProjectDetails_Buttons);

            //--- Action: Delete All Saved Products in myAnalog if there's some ---//
            if (util.CheckElement(driver, Elements.MyAnalog_ProjectDetailPage_SavedProducts, 2))
            {
                action.DeleteAllSavedProjectProductsInMyAnalog(driver, Locale);
            }

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify Projet Product -----//

            //--- Action: Enter Product (in lower case products) on the add products to myanalog ---//
            string addProductsToMyAnalog_input = "ad7706";
            action.ISaveProjectProductsInMyAnalog(driver, Locale, addProductsToMyAnalog_input.ToLower());

            //--- Action: Click add ---//
            action.IClick(driver, Elements.MyAnalog_ProjectDetailPage_ProjectProducts_AddProductsToMyAnalog_Results);

            //--- Expected Result: Products section should get updated and show the added product in uppercase (IQ-6268/AL-13418) ---// 
            test.validateStringIsCorrect(driver, Elements.MyAnalog_ProjectDetailPage_Last_SavedProduct_Link, addProductsToMyAnalog_input.ToUpper());
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that Validation for Adding Invalid Products is Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that Validation for Adding Invalid Products is Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that Validation for Adding Invalid Products is Working as Expected in JP Locale")]
        public void MyAnalog_Projects_ProjectDetailPage_ProjectProducts_VerifyAddingInvalidProducts(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Projects URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_projects_url;

            //--- Action: Open myAnalog Projects using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);

            //--- Action: Click Project Detail button ---//
            action.IClick(driver, Elements.MyAnalog_Projects_ProjectDetails_Buttons);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify if the page Continuous loading upon adding invalid products. (IQ-6859/AL-14183) -----//

            //--- Action: Enter a invalid product. ---//
            string addProductsToMyAnalog_input = util.GenerateRandomString();
            action.IDeleteValueOnFields(driver, Elements.MyAnalog_ProjectDetailPage_ProjectProducts_AddProductsToMyAnalog_InputBox);
            action.IType(driver, Elements.MyAnalog_ProjectDetailPage_ProjectProducts_AddProductsToMyAnalog_InputBox, addProductsToMyAnalog_input);

            //--- Expected Result: Error message show. "No Result Found. Please modify your Query" ---//
            if (Locale.Equals("en"))
            {
                test.validateStringIsCorrect(driver, Elements.MyAnalog_ProjectDetailPage_ProjectProducts_AddProductsToMyAnalog_Message, "No results found.\r\nPlease modify your query.");
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.MyAnalog_ProjectDetailPage_ProjectProducts_AddProductsToMyAnalog_Message);
            }
        }

        [TestCase("en", TestName = "Verify that Validation for Adding Existing Products is Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that Validation for Adding Existing Products is Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that Validation for Adding Existing Products is Working as Expected in JP Locale")]
        public void MyAnalog_Projects_ProjectDetailPage_ProjectProducts_VerifyAddingExistingProducts(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Projects URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_projects_url;

            //--- Action: Open myAnalog Projects using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);

            //--- Action: Click Project Detail button ---//
            action.IClick(driver, Elements.MyAnalog_Projects_ProjectDetails_Buttons);

            //--- Action: Save Project Products in myAnalog if there's none ---//
            if (!util.CheckElement(driver, Elements.MyAnalog_ProjectDetailPage_SavedProducts, 2))
            {
                action.ISaveProjectProductsInMyAnalog(driver, Locale, "AD7705");
            }

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify when adding same product -----//
            string first_savedProducts_label = util.GetText(driver, Elements.MyAnalog_ProjectDetailPage_First_SavedProduct_Link);

            //--- Action: Add same product ---//
            action.ISaveProjectProductsInMyAnalog(driver, Locale, first_savedProducts_label);

            //--- Expected Result: Proper message appear ---// 
            test.validateElementIsPresent(driver, Elements.MyAnalog_ProjectDetailPage_ProjectProducts_AddProductsToMyAnalog_Error_Message);
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that Delete Saved Products is Working as Expected in EN Locale")]
        public void MyAnalog_Projects_ProjectDetailPage_ProjectProducts_VerifyDeleteSavedProducts(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Projects URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_projects_url;

            //--- Action: Open myAnalog Projects using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username_editSavedProducts, password_editSavedProducts);

            //--- Action: Click Project Detail button ---//
            action.IClick(driver, Elements.MyAnalog_Projects_ProjectDetails_Buttons);

            //--- Action: Save Project Products in myAnalog if there's none ---//
            if (!util.CheckElement(driver, Elements.MyAnalog_ProjectDetailPage_SavedProducts, 2))
            {
                action.ISaveProjectProductsInMyAnalog(driver, Locale, "AD7705");
            }

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify if the Save and Cancel button disappear after Clicking the Save button. (IQ-7634/AL-15026) -----//

            //--- Action: Click Edit icon button in Products ---//
            action.IClick(driver, Elements.MyAnalog_ProjectDetailPage_ProjectProducts_Edit_Button);

            //--- Expected Result: Edit button disappear. ---//
            test.validateElementIsNotPresent(driver, Elements.MyAnalog_ProjectDetailPage_ProjectProducts_Edit_Button);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify when editing Product -----//
            string first_savedProducts_label = util.GetText(driver, Elements.MyAnalog_ProjectDetailPage_First_SavedProduct_Link);

            //--- Expected Result: Save is displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_ProjectDetailPage_ProjectProducts_Save_Button);

            //--- Action: Click close icon (x) inline with the product ---//
            action.IClick(driver, Elements.MyAnalog_ProjectDetailPage_SavedProducts_X_Icon);

            //--- Expected Result: Product disappear ---//
            test.validateStringChanged(driver, util.GetText(driver, Elements.MyAnalog_ProjectDetailPage_First_SavedProduct_Link), first_savedProducts_label);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify if product is removed successfully (IQ-7300/ALA-12223) -----//

            //--- Action: Click Save link ---//
            action.IClick(driver, Elements.MyAnalog_ProjectDetailPage_ProjectProducts_Save_Button);

            //--- Expected Result: Product is removed successfully ---//
            test.validateStringChanged(driver, util.GetText(driver, Elements.MyAnalog_ProjectDetailPage_First_SavedProduct_Link), first_savedProducts_label);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify if the Save and Cancel button disappear after Clicking the Save button. (IQ-7634/AL-15026) -----//

            //--- Expected Result: Edit button Displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_ProjectDetailPage_ProjectProducts_Edit_Button);
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Cancel Changes is Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Cancel Changes is Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Cancel Changes is Working as Expected in JP Locale")]
        public void MyAnalog_Projects_ProjectDetailPage_ProjectProducts_VerifyCancelChanges(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Projects URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_projects_url;

            //--- Action: Open myAnalog Projects using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);

            //--- Action: Click Project Detail button ---//
            action.IClick(driver, Elements.MyAnalog_Projects_ProjectDetails_Buttons);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify when editing Product -----//

            //--- Action: Click Edit option ---//
            action.IClick(driver, Elements.MyAnalog_ProjectDetailPage_ProjectProducts_Edit_Button);

            //--- Expected Result: Cancel is displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_ProjectDetailPage_ProjectProducts_Cancel_Button);

            //--- Action: Click cancel ---//
            action.IClick(driver, Elements.MyAnalog_ProjectDetailPage_ProjectProducts_Cancel_Button);

            //--- Expected Result: will go back to its default display ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_ProjectDetailPage_ProjectProducts_Section);
        }
    }
}