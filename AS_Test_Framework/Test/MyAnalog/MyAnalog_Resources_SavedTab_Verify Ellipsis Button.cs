﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System;
using System.Drawing;
using System.Threading;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_Resources_SavedTab_EllipsisButton : BaseSetUp
    {
        //--- myAnalog Resources URL: ---//
        //--- DEV: https://my-dev.corpnt.analog.com/en/app/resources ---//
        //--- QA: https://my-qa.corpnt.analog.com/en/app/resources ---//
        //--- PROD: https://my.analog.com/en/app/resources ---//

        public MyAnalog_Resources_SavedTab_EllipsisButton() : base() { }

        //--- Login Credentials ---//
        string username = "myanalog_t3st_3d1t_res0urces_555@mailinator.com";
        string password = "Test_1234";

        //--- URLs ---//
        string myAnalog_resources_url = "app/resources";
        string resource_url = "/education/education-library/webcasts.html";

        //--- Labels ---//
        string copyLink_text = "Copy Link";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Save Button in Edit title Option is Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Save Button in Edit title Option is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Save Button in Edit title Option is Present and Working as Expected in JP Locale")]
        public void MyAnalog_Resources_SavedTab_EllipsisButton_EditTitleOption_VerifySaveButton(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- Action: Set the Window to a Larger Size since the Ellipsis Buttons are being cut in Small Screens ---//
            driver.Manage().Window.Size = new Size(1920, 1080);

            //--- myAnalog Resources URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_resources_url;

            //--- Action: Open resources direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);

            //--- Action: Select the Saved Tab ---//
            if (!util.CheckElement(driver, Elements.MyAnalog_Resources_Selected_Saved_Tab, 2))
            {
                action.IClick(driver, Elements.MyAnalog_Resources_Saved_Tab);
            }

            //--- Action: Save Resources in myAnalog if there's none ---//
            if (!util.CheckElement(driver, Elements.MyAnalog_Resources_Saved_Resources_Rows, 2))
            {
                action.ISaveResourcesInMyAnalog(driver, Locale, resource_url);

                driver.Navigate().GoToUrl(myAnalog_url);

                if (!util.CheckElement(driver, Elements.MyAnalog_Resources_Selected_Saved_Tab, 2))
                {
                    action.IClick(driver, Elements.MyAnalog_Resources_Saved_Tab);
                }
            }

            //--- Action: Get the URL Value of the Title of the 1st Row ---//
            string default_titleUrl_value = util.GetCssValue(driver, Elements.MyAnalog_Resources_Saved_Resources_1st_Row_Title_Value, "href");

            //----- Resources Test Plan > Desktop Tab > R5 > T1: Ability to edit the title of a record in saved Resources table -----//

            //--- Action: Click the ellipsis button of the desired record to edit ---//
            action.IClick(driver, Elements.MyAnalog_Resources_Saved_Resources_1st_Row_Ellipsis_Button);

            //--- Expected Result: option to edit title will display ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Resources_Saved_Resources_EditTitle_Option);

            //--- Action: Click the Edit title option ---//
            action.IClick(driver, Elements.MyAnalog_Resources_Saved_Resources_EditTitle_Option);

            //--- Expected Result: Title cell is now editable ---//
            test.validateStringInstance(driver, "editing", util.GetCssValue(driver, Elements.MyAnalog_Resources_Saved_Resources_1st_Row, "class"));

            //--- Expected Result: Save replace the ellipsis (IQ-8155/AL-15309) ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Resources_Saved_Resources_Save_Button);

            //----- Resources Test Plan > Desktop Tab > R5 > T1.1: Verify maximum length of Title field (IQ-7671/AL-14465) -----//

            //--- Action: Enter Resource Title with 400 characters ---//
            string title_input = "T3st_" + Locale.ToUpper() + "_400_Character_Title_Input_" + DateTime.Now.ToString("MMddyyyy") + "_" + util.RandomString()
                + "_zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz";
            action.IDeleteValueOnFields(driver, Elements.MyAnalog_Resources_Saved_Resources_1st_Row_Title_InputBox);
            action.IType(driver, Elements.MyAnalog_Resources_Saved_Resources_1st_Row_Title_InputBox, title_input);

            //--- Action: Click Save button ---//
            action.IClick(driver, Elements.MyAnalog_Resources_Saved_Resources_Save_Button);

            //--- Expected Result: The title has been updated correctly ---//
            test.validateStringIsCorrect(driver, Elements.MyAnalog_Resources_Saved_Resources_1st_Row_Title_Value, title_input);

            //--- Expected Result: URL is still the same ---//
            test.validateStringInstance(driver, default_titleUrl_value, util.GetCssValue(driver, Elements.MyAnalog_Resources_Saved_Resources_1st_Row_Title_Value, "href"));
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Cancel Button in Edit title Option is Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Cancel Button in Edit title Option is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Cancel Button in Edit title Option is Present and Working as Expected in JP Locale")]
        public void MyAnalog_Resources_SavedTab_EllipsisButton_EditTitleOption_VerifCancelButton(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- Action: Set the Window to a Larger Size since the Ellipsis Buttons are being cut in Small Screens ---//
            driver.Manage().Window.Size = new Size(1920, 1080);

            //--- myAnalog Resources URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_resources_url;

            //--- Action: Open resources direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);

            //--- Action: Select the Saved Tab ---//
            if (!util.CheckElement(driver, Elements.MyAnalog_Resources_Selected_Saved_Tab, 2))
            {
                action.IClick(driver, Elements.MyAnalog_Resources_Saved_Tab);
            }

            //--- Action: Save Resources in myAnalog if there's none ---//
            if (!util.CheckElement(driver, Elements.MyAnalog_Resources_Saved_Resources_Rows, 2))
            {
                action.ISaveResourcesInMyAnalog(driver, Locale, resource_url);

                driver.Navigate().GoToUrl(myAnalog_url);

                if (!util.CheckElement(driver, Elements.MyAnalog_Resources_Selected_Saved_Tab, 2))
                {
                    action.IClick(driver, Elements.MyAnalog_Resources_Saved_Tab);
                }
            }

            //--- Action: Get the Title Value of the 1st Row ---//
            string default_title_value = util.GetText(driver, Elements.MyAnalog_Resources_Saved_Resources_1st_Row_Title_Value);

            //--- Action: Click the ellipsis button of the desired record to edit ---//
            action.IClick(driver, Elements.MyAnalog_Resources_Saved_Resources_1st_Row_Ellipsis_Button);

            //--- Action: Click the Edit title option ---//
            action.IClick(driver, Elements.MyAnalog_Resources_Saved_Resources_EditTitle_Option);

            //----- Resources Test Plan > Desktop Tab > R5 > T1: Ability to edit the title of a record in saved Resources table -----//

            //--- Expected Result: Cancel must replace the ellipsis (IQ-8155/AL-15309) ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Resources_Saved_Resources_Cancel_Button);

            //----- Resources Test Plan > Desktop Tab > R5 > T2: Clicking Cancel with not save the changes made -----//

            //--- Action: Enter new value to the title cell ---//
            string title_input = "T3st_" + Locale.ToUpper() + "_Title_Input_" + DateTime.Now.ToString("MMddyyyy") + "_" + util.RandomString();
            action.IDeleteValueOnFields(driver, Elements.MyAnalog_Resources_Saved_Resources_1st_Row_Title_InputBox);
            action.IType(driver, Elements.MyAnalog_Resources_Saved_Resources_1st_Row_Title_InputBox, title_input);

            //--- Action: Click Cancel button ---//
            action.IClick(driver, Elements.MyAnalog_Resources_Saved_Resources_Cancel_Button);

            //--- Expected Result: System will NOT save the changes made and will revert to its original value ---//
            test.validateStringIsCorrect(driver, Elements.MyAnalog_Resources_Saved_Resources_1st_Row_Title_Value, default_title_value);
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Save Button in Edit Note Option is Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Save Button in Edit Note Option is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Save Button in Edit Note Option is Present and Working as Expected in JP Locale")]
        public void MyAnalog_Resources_SavedTab_EllipsisButton_EditNoteOption_VerifySaveButton(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- Action: Set the Window to a Larger Size since the Ellipsis Buttons are being cut in Small Screens ---//
            driver.Manage().Window.Size = new Size(1920, 1080);

            //--- myAnalog Resources URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_resources_url;

            //--- Action: Open resources direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);

            //--- Action: Select the Saved Tab ---//
            if (!util.CheckElement(driver, Elements.MyAnalog_Resources_Selected_Saved_Tab, 2))
            {
                action.IClick(driver, Elements.MyAnalog_Resources_Saved_Tab);
            }

            //--- Action: Save Resources in myAnalog if there's none ---//
            if (!util.CheckElement(driver, Elements.MyAnalog_Resources_Saved_Resources_Rows, 2))
            {
                action.ISaveResourcesInMyAnalog(driver, Locale, resource_url);

                driver.Navigate().GoToUrl(myAnalog_url);

                if (!util.CheckElement(driver, Elements.MyAnalog_Resources_Selected_Saved_Tab, 2))
                {
                    action.IClick(driver, Elements.MyAnalog_Resources_Saved_Tab);
                }
            }

            //----- BLANK INPUT -----//

            //----- Resources Test Plan > Desktop Tab > R6 > T2.1: Verify when no note entered then clicking save -----//

            //--- Action: Click the ellipsis button of the desired record to edit ---//
            action.IClick(driver, Elements.MyAnalog_Resources_Saved_Resources_1st_Row_Ellipsis_Button);

            //--- Action: Click the Add Note option ---//
            action.IClick(driver, Elements.MyAnalog_Resources_Saved_Resources_EditNote_Option);

            //--- Action: Do not Enter notes then click save ---//
            string note_input = " ";
            action.IDeleteValueOnFields(driver, Elements.MyAnalog_Resources_Saved_Resources_1st_Row_Note_InputBox);
            action.IType(driver, Elements.MyAnalog_Resources_Saved_Resources_1st_Row_Note_InputBox, note_input);
            action.IClick(driver, Elements.MyAnalog_Resources_Saved_Resources_Save_Button);

            //--- Expected Result: System able to accept even note field is blank ---//
            test.validateStringIsCorrect(driver, Elements.MyAnalog_Resources_Saved_Resources_1st_Row_Note_Value, "");

            //----- 500 CHARACTERS INPUT -----//

            //----- Resources Test Plan > Desktop Tab > R5 > T1: Ability to edit the title of a record in saved Resources table -----//

            //--- Action: Click the ellipsis button of the desired record to edit ---//
            action.IClick(driver, Elements.MyAnalog_Resources_Saved_Resources_1st_Row_Ellipsis_Button);

            //--- Expected Result: option to Add note will display ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Resources_Saved_Resources_EditNote_Option);

            //----- Resources Test Plan > Desktop Tab > R6 > T1: Ability to add note to  a record in Resources table -----//

            //--- Action: Click the Add Note option ---//
            action.IClick(driver, Elements.MyAnalog_Resources_Saved_Resources_EditNote_Option);

            //--- Expected Result: Note cell is now editable ---//
            test.validateStringInstance(driver, "editing", util.GetCssValue(driver, Elements.MyAnalog_Resources_Saved_Resources_1st_Row, "class"));

            //--- Expected Result: Save must replace the ellipsis (IQ-8155/AL-15309) ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Resources_Saved_Resources_Save_Button);

            //----- Resources Test Plan > Desktop Tab > R6 > T1.1: Verify maximum length of Note field (IQ-7671/AL-14465) -----//

            //--- Action: Enter Resource Note with 500 characters ---//
            note_input = "T3st_" + Locale.ToUpper() + "_500_Character_Note_Input_" + DateTime.Now.ToString("MMddyyyy") + "_" + util.RandomString()
                + "_zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz";
            action.IDeleteValueOnFields(driver, Elements.MyAnalog_Resources_Saved_Resources_1st_Row_Note_InputBox);
            action.IType(driver, Elements.MyAnalog_Resources_Saved_Resources_1st_Row_Note_InputBox, note_input);

            //--- Action: Click Save button ---//
            action.IClick(driver, Elements.MyAnalog_Resources_Saved_Resources_Save_Button);

            //--- Expected Result: System successfully saved the note ---//
            test.validateStringIsCorrect(driver, Elements.MyAnalog_Resources_Saved_Resources_1st_Row_Note_Value, note_input);
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Cancel Button in Edit Note Option is Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Cancel Button in Edit Note Option is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Cancel Button in Edit Note Option is Present and Working as Expected in JP Locale")]
        public void MyAnalog_Resources_SavedTab_EllipsisButton_EditNoteOption_VerifyCancelButton(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- Action: Set the Window to a Larger Size since the Ellipsis Buttons are being cut in Small Screens ---//
            driver.Manage().Window.Size = new Size(1920, 1080);

            //--- myAnalog Resources URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_resources_url;

            //--- Action: Open resources direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);

            //--- Action: Select the Saved Tab ---//
            if (!util.CheckElement(driver, Elements.MyAnalog_Resources_Selected_Saved_Tab, 2))
            {
                action.IClick(driver, Elements.MyAnalog_Resources_Saved_Tab);
            }

            //--- Action: Save Resources in myAnalog if there's none ---//
            if (!util.CheckElement(driver, Elements.MyAnalog_Resources_Saved_Resources_Rows, 2))
            {
                action.ISaveResourcesInMyAnalog(driver, Locale, resource_url);

                driver.Navigate().GoToUrl(myAnalog_url);

                if (!util.CheckElement(driver, Elements.MyAnalog_Resources_Selected_Saved_Tab, 2))
                {
                    action.IClick(driver, Elements.MyAnalog_Resources_Saved_Tab);
                }
            }

            //--- Action: Get the Note Value of the 1st Row ---//
            string default_note_value = util.GetText(driver, Elements.MyAnalog_Resources_Saved_Resources_1st_Row_Note_Value);

            //--- Action: Click the ellipsis button of the desired record to edit ---//
            action.IClick(driver, Elements.MyAnalog_Resources_Saved_Resources_1st_Row_Ellipsis_Button);

            //--- Action: Click the Add Note option ---//
            action.IClick(driver, Elements.MyAnalog_Resources_Saved_Resources_EditNote_Option);

            //----- Resources Test Plan > Desktop Tab > R5 > T1: Ability to edit the title of a record in saved Resources table -----//

            //--- Expected Result: Cancel must replace the ellipsis (IQ-8155/AL-15309) ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Resources_Saved_Resources_Cancel_Button);

            //----- Resources Test Plan > Desktop Tab > R6 > T2: Clicking Cancel with not save the changes made -----//

            //--- Action: Enter new value to the Note cell ---//
            string note_input = "T3st_" + Locale.ToUpper() + "_Note_Input_" + DateTime.Now.ToString("MMddyyyy") + "_" + util.RandomString();
            action.IDeleteValueOnFields(driver, Elements.MyAnalog_Resources_Saved_Resources_1st_Row_Note_InputBox);
            action.IType(driver, Elements.MyAnalog_Resources_Saved_Resources_1st_Row_Note_InputBox, note_input);

            //--- Action: Click Cancel button ---//
            action.IClick(driver, Elements.MyAnalog_Resources_Saved_Resources_Cancel_Button);

            //--- Expected Result: The original note whether there was a value or blank is still the same ---//
            test.validateStringIsCorrect(driver, Elements.MyAnalog_Resources_Saved_Resources_1st_Row_Note_Value, default_note_value);
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Copy Link Option is Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Copy Link Option is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Copy Link Option is Present and Working as Expected in JP Locale")]
        public void MyAnalog_Resources_SavedTab_EllipsisButton_VerifyCopyLinkOption(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- Action: Set the Window to a Larger Size since the Ellipsis Buttons are being cut in Small Screens ---//
            driver.Manage().Window.Size = new Size(1920, 1080);

            //--- myAnalog Resources URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_resources_url;

            //--- Action: Open resources direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);

            //--- Action: Select the Saved Tab ---//
            if (!util.CheckElement(driver, Elements.MyAnalog_Resources_Selected_Saved_Tab, 2))
            {
                action.IClick(driver, Elements.MyAnalog_Resources_Saved_Tab);
            }

            //--- Action: Save Resources in myAnalog if there's none ---//
            if (!util.CheckElement(driver, Elements.MyAnalog_Resources_Saved_Resources_Rows, 2))
            {
                action.ISaveResourcesInMyAnalog(driver, Locale, resource_url);

                driver.Navigate().GoToUrl(myAnalog_url);

                if (!util.CheckElement(driver, Elements.MyAnalog_Resources_Selected_Saved_Tab, 2))
                {
                    action.IClick(driver, Elements.MyAnalog_Resources_Saved_Tab);
                }
            }

            //----- Resources Test Plan > Desktop Tab > R9 > T1: Verify if "share" in options dropdown change to "copy link" (IQ-9362/AL-14820) -----//

            //--- Action: Click menu/ Ellipsis. ---//
            action.IClick(driver, Elements.MyAnalog_Resources_Saved_Resources_1st_Row_Ellipsis_Button);

            //--- Expected Result: Copy link displayed ---//
            if (Locale.Equals("en"))
            {
                test.validateStringIsCorrect(driver, Elements.MyAnalog_Resources_Saved_Resources_CopyLink_Option, copyLink_text);
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.MyAnalog_Resources_Saved_Resources_CopyLink_Option);
            }
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Remove Option is Present and Working as Expected in EN Locale")]
        public void MyAnalog_Resources_SavedTab_EllipsisButton_VerifyRemoveOption(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- Action: Set the Window to a Larger Size since the Ellipsis Buttons are being cut in Small Screens ---//
            driver.Manage().Window.Size = new Size(1920, 1080);

            //--- Login Credentials ---//
            username = "myanalog_t3st_r3m0v3_res0urces_555@mailinator.com";
            password = "Test_1234";

            //--- myAnalog Resources URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_resources_url;

            //--- Action: Open resources direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);

            //--- Action: Select the Saved Tab ---//
            if (!util.CheckElement(driver, Elements.MyAnalog_Resources_Selected_Saved_Tab, 2))
            {
                action.IClick(driver, Elements.MyAnalog_Resources_Saved_Tab);
            }

            //--- Action: Save Resources in myAnalog if there's none ---//
            if (!util.CheckElement(driver, Elements.MyAnalog_Resources_Saved_Resources_Rows, 2))
            {
                action.ISaveResourcesInMyAnalog(driver, Locale, resource_url);

                driver.Navigate().GoToUrl(myAnalog_url);

                if (!util.CheckElement(driver, Elements.MyAnalog_Resources_Selected_Saved_Tab, 2))
                {
                    action.IClick(driver, Elements.MyAnalog_Resources_Saved_Tab);
                }
            }

            //----- Resources Test Plan > Desktop Tab > R5 > T1: Ability to edit the title of a record in saved Resources table -----//

            //--- Action: Click the ellipsis button of the desired record to edit ---//
            action.IClick(driver, Elements.MyAnalog_Resources_Saved_Resources_1st_Row_Ellipsis_Button);

            //--- Expected Result: option to remove will display ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Resources_Saved_Resources_Remove_Option);

            //----- Resources Test Plan > Desktop Tab > R7 > T1: Ability to remove desired record in Resources table -----//

            //--- Action: Click the Remove option ---//
            action.IClick(driver, Elements.MyAnalog_Resources_Saved_Resources_Remove_Option);

            //--- Expected Result: Confirmation Modal Displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Resources_Remove_Confirmation_Modal);

            //--- Action: Click Remove button ---//
            action.IClick(driver, Elements.MyAnalog_Resources_Remove_Confirmation_Modal_Remove_Button);

            //--- Expected Result: Saved Resources Removed ---//
            test.validateElementIsNotPresent(driver, Elements.MyAnalog_Resources_Saved_Resources_Rows);
        }
    }
}