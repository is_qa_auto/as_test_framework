﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_Dashboard_YourProjectsSection_NoCreatedProjects : BaseSetUp
    {
        public MyAnalog_Dashboard_YourProjectsSection_NoCreatedProjects() : base() { }

        //-- Login Credentials for New Account --//
        string username_new = "myanalog_t3st_new_555@mailinator.com";
        string password_new = "Test_1234";

        //--- URLs ---//
        string myAnalog_dashboard_url = "app";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Your Projects Section is Present and Working as Expected for New Accounts in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Your Projects Section is Present and Working as Expected for New Accounts in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Your Projects Section is Present and Working as Expected for New Accounts in JP Locale")]
        public void MyAnalog_Dashboard_YourProjectsSection_NoCreatedProjects_VerifyForNewAccounts(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Dashboard URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_dashboard_url;

            //--- Action: Go to myAnalog dashboard ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid user account in the myAnalog Login page ---//
            action.ILogin(driver, username_new, password_new);

            //----- Dashboard Test Plan > Desktop Tab > R3 > T1: Verify Your Project Section when account is new -----//

            //--- Expected Result: Your project section contains the Your projects label ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_YourProjects_Label);

            //--- Expected Result: Your project section contains the Description ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_YourProjects_Description_Header);
            test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_YourProjects_Description_Text);

            //--- Expected Result: Your project section contains the Video ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_YourProjects_Video);

            //--- Expected Result: Your project section contains the Create new project ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_CreateANewProject_Link);

            //--- Action: Click Create new Project ---//
            action.IClick(driver, Elements.MyAnalog_Dashboard_CreateANewProject_Link);

            if (util.CheckElement(driver, Elements.MyAnalog_Dashboard_CreateANewProject_Link, 2))
            {
                //--- Expected Result: Window for create new project is display ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_CreateANewProject_Dialog);
            }
        }
    }
}