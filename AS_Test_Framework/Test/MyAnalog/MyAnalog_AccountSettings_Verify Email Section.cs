﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using OpenQA.Selenium;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_AccountSettings_EmailSection : BaseSetUp
    {
        //--- myAnalog Account Settings URL: ---//
        //--- DEV: https://my-dev.corpnt.analog.com/en/app/account ---//
        //--- QA: https://my-qa.corpnt.analog.com/en/app/account ---//
        //--- PROD: https://my.analog.com/en/app/account ---//

        public MyAnalog_AccountSettings_EmailSection() : base() { }

        public static DataTable dtElements;

        //--- Login Credentials ---//
        string username = null;
        string password = null;

        //--- URLs ---//
        string myAnalog_accountSettings_url = "app/account";

        //--- Labels ---//
        string alreadyRegistered_errorMessage = "The username/email you selected is already registered. Please choose a different one.×";
        string invalidVerificationCode_errorMessage = "Verification code is invalid. Please try again.×";
        string embargoedCountry_warningMessage = "This email domain is blocked. Please use different domain×";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Elements are Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Elements are Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Elements are Present and Working as Expected in JP Locale")]
        public void MyAnalog_AccountSettings_VerifyEmailSection(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- Login Credentials ---//
            if (Locale.Equals("zh") || Locale.Equals("cn"))
            {
                username = "test_zh_04252019@mailinator.com";
            }
            else if (Locale.Equals("jp"))
            {
                username = "test_jp_04252019@mailinator.com";
            }
            else
            {
                username = "myanalog_t3st_555@mailinator.com";
            }
            password = "Test_1234";

            //--- myAnalog Account Settings URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_accountSettings_url;

            //--- Action: Open account settings page ---//
            action.Navigate(driver, myAnalog_url);

            action.ILogin(driver, username, password);

            //----- Account Settings Test Plan > myAnalog Dashboard Tab > R6 > T1: Verify Change Email Address. (IQ-7093/AL-13200, IQ-7324/ALA-12231, IQ-7390/ALA-11979) -----//

            //--- Action: Click edit icon button. ---//
            action.IClick(driver, Elements.MyAnalog_AccountSettings_Email_Section_Edit_Button);

            //--- Expected Result: The Email section becomes editable ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_AccountSettings_Email_Section_Editing_Mode);

            //----- Account Settings Test Plan > myAnalog Dashboard Tab > R6 > T8: Verify time limit up until the verification code is still valid -----//

            //--- Expected Result: The Edit button / hyperlink in the Email section became Cancel hyperlink (IQ-7832/AL-14819) ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_AccountSettings_Email_Section_Cancel_Button);

            //----- Account Settings Test Plan > myAnalog Dashboard Tab > R6 > T1: Verify Change Email Address. (IQ-7093/AL-13200, IQ-7324/ALA-12231, IQ-7390/ALA-11979) -----//

            //--- Expected Result: read only current Email ---//
            test.validateElementIsNotEnabled(driver, Elements.MyAnalog_AccountSettings_Email_Section_CurrentEmail_InputBox);

            //----- Account Settings Test Plan > myAnalog Dashboard Tab > R6 > T8: Verify time limit up until the verification code is still valid -----//

            //--- Expected Result: Four fields replace “Email” field: Current Email ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_AccountSettings_Email_Section_CurrentEmail_InputBox);

            //--- Expected Result: Four fields replace “Email” field: Enter New Email ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_AccountSettings_Email_Section_EnterNewEmail_InputBox);

            //--- Expected Result: Four fields replace “Email” field: Confirm New Email ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_AccountSettings_Email_Section_ConfirmNewEmail_InputBox);

            //--- Expected Result: Four fields replace “Email” field: Enter Verification Code ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_AccountSettings_Email_Section_EnterVerificationCode_InputBox);
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Input Validations are Working as Expected for Embargoed Country Input in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Input Validations are Working as Expected for Embargoed Country Input in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Input Validations are Working as Expected for Embargoed Country Input in JP Locale")]
        public void MyAnalog_AccountSettings_EmailSection_InputValidations_VerifyEmbargoedCountryInput(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- Login Credentials ---//
            if (Locale.Equals("zh") || Locale.Equals("cn"))
            {
                username = "test_zh_04252019@mailinator.com";
            }
            else if (Locale.Equals("jp"))
            {
                username = "test_jp_04252019@mailinator.com";
            }
            else
            {
                username = "myanalog_t3st_555@mailinator.com";
            }
            password = "Test_1234";

            //--- myAnalog Account Settings URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_accountSettings_url;

            //--- Action: Open account settings page ---//
            action.Navigate(driver, myAnalog_url);

            action.ILogin(driver, username, password);

            //----- Account Settings Test Plan > myAnalog Dashboard Tab > R6 > T2: Verify Change Email Addres using embargoed country (IQ-7135/ALA-12183) -----//

            //--- Action: under email address section click edit ---//
            action.IClick(driver, Elements.MyAnalog_AccountSettings_Email_Section_Edit_Button);

            //--- Action: replace existing email address with email for embargoed country (e.g. arlene.catigan@iran.ir) ---//
            string enterNewEmail_input = "arlene.catigan@iran.ir";
            action.IDeleteValueOnFields(driver, Elements.MyAnalog_AccountSettings_Email_Section_EnterNewEmail_InputBox);
            action.IType(driver, Elements.MyAnalog_AccountSettings_Email_Section_EnterNewEmail_InputBox, enterNewEmail_input);
            action.IType(driver, Elements.MyAnalog_AccountSettings_Email_Section_EnterNewEmail_InputBox, Keys.Tab);
            Thread.Sleep(1000);

            //--- Expected Result: The System displays an Error Message 'This email domain is blocked. Please use different domain' ---//
            if (Locale.Equals("en"))
            {
                test.validateStringIsCorrect(driver, Elements.MyAnalog_AccountSettings_Email_Section_EnterNewEmail_InputBox_Warning_Message, embargoedCountry_warningMessage);
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.MyAnalog_AccountSettings_Email_Section_EnterNewEmail_InputBox_Warning_Message);
            }
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Input Validations are Working as Expected for Already Registered Email Input in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Input Validations are Working as Expected for Already Registered Email Input in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Input Validations are Working as Expected for Already Registered Email Input in JP Locale")]
        public void MyAnalog_AccountSettings_EmailSection_InputValidations_VerifyAlreadyRegisteredEmailInput(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- Login Credentials ---//
            if (Locale.Equals("zh") || Locale.Equals("cn"))
            {
                username = "test_zh_04252019@mailinator.com";
            }
            else if (Locale.Equals("jp"))
            {
                username = "test_jp_04252019@mailinator.com";
            }
            else
            {
                username = "myanalog_t3st_555@mailinator.com";
            }
            password = "Test_1234";

            //--- myAnalog Account Settings URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_accountSettings_url;

            //--- Action: Open account settings page ---//
            action.Navigate(driver, myAnalog_url);

            action.ILogin(driver, username, password);
            Thread.Sleep(1000);

            //----- Account Settings Test Plan > myAnalog Dashboard Tab > R6 > T3: Verify Change Email Address using already registered Email (IQ-7390/ALA-11979) -----//

            //--- Action: Click the Edit icon in the Email section ---//
            action.IClick(driver, Elements.MyAnalog_AccountSettings_Email_Section_Edit_Button);

            //--- Action: Enter an already registered Email in the 'Enter New Email' field ---//
            string enterNewEmail_input = "myanalog_t3st_555@mailinator.com";
            action.IDeleteValueOnFields(driver, Elements.MyAnalog_AccountSettings_Email_Section_EnterNewEmail_InputBox);
            action.IType(driver, Elements.MyAnalog_AccountSettings_Email_Section_EnterNewEmail_InputBox, enterNewEmail_input);

            //--- Action: Enter the Email again in the 'Confirm New Email' field ---//
            string confirmNewEmail_input = "myanalog_t3st_555@mailinator.com";
            action.IDeleteValueOnFields(driver, Elements.MyAnalog_AccountSettings_Email_Section_ConfirmNewEmail_InputBox);
            action.IType(driver, Elements.MyAnalog_AccountSettings_Email_Section_ConfirmNewEmail_InputBox, confirmNewEmail_input);

            //--- Action: Click the 'Send verification code' button ---//
            action.IClick(driver, Elements.MyAnalog_AccountSettings_Email_Section_SendVerificationCode_Button);

            //--- Expected Result: The System displays an Error message 'The username/email you selected is already registered. Please choose a different one.' ---//
            if (Locale.Equals("en"))
            {
                test.validateStringIsCorrect(driver, Elements.MyAnalog_AccountSettings_Email_Section_Error_Message, alreadyRegistered_errorMessage);
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.MyAnalog_AccountSettings_Email_Section_Error_Message);
            }
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Input Validations are Working as Expected for Invalid Validation Code Input in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Input Validations are Working as Expected for Invalid Validation Code Input in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Input Validations are Working as Expected for Invalid Validation Code Input in JP Locale")]
        public void MyAnalog_AccountSettings_EmailSection_InputValidations_VerifyInvalidValidationCodeInput(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- Login Credentials ---//
            if (Locale.Equals("zh") || Locale.Equals("cn"))
            {
                username = "test_zh_04252019@mailinator.com";
            }
            else if (Locale.Equals("jp"))
            {
                username = "test_jp_04252019@mailinator.com";
            }
            else
            {
                username = "myanalog_t3st_555@mailinator.com";
            }
            password = "Test_1234";

            //--- myAnalog Account Settings URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_accountSettings_url;

            //--- Action: Open account settings page ---//
            action.Navigate(driver, myAnalog_url);

            action.ILogin(driver, username, password);
            Thread.Sleep(1000);

            //----- Account Settings Test Plan > myAnalog Dashboard Tab > R6 > T6: Verify the error message when submitting invalid validation code. (IQ-7711/AL-14702) -----//

            //--- Action: Click edit icon button. ---//
            action.IClick(driver, Elements.MyAnalog_AccountSettings_Email_Section_Edit_Button);

            //--- Action: Enter New Email Address in Enter New Email field. ---//
            string enterNewEmail_input = "t3st_" + Util.Configuration.Environment.ToLower() + "_invalidValidationCodeInput_" + util.GenerateRandomNumber() + "@mailinator.com";
            action.IDeleteValueOnFields(driver, Elements.MyAnalog_AccountSettings_Email_Section_EnterNewEmail_InputBox);
            action.IType(driver, Elements.MyAnalog_AccountSettings_Email_Section_EnterNewEmail_InputBox, enterNewEmail_input);

            //--- Action: Enter the email again in Confirm New Email field. ---//
            string confirmNewEmail_input = enterNewEmail_input;
            action.IDeleteValueOnFields(driver, Elements.MyAnalog_AccountSettings_Email_Section_ConfirmNewEmail_InputBox);
            action.IType(driver, Elements.MyAnalog_AccountSettings_Email_Section_ConfirmNewEmail_InputBox, confirmNewEmail_input);

            //--- Action: enter a invalid validation code. ---//
            string enterVerificationCode_input = util.RandomString();
            action.IDeleteValueOnFields(driver, Elements.MyAnalog_AccountSettings_Email_Section_EnterVerificationCode_InputBox);
            action.IType(driver, Elements.MyAnalog_AccountSettings_Email_Section_EnterVerificationCode_InputBox, enterVerificationCode_input);

            //--- Action: Click submit button. ---//
            action.IClick(driver, Elements.MyAnalog_AccountSettings_Email_Section_Submit_Button);

            //--- Expected Result: "Verification code is invalid. Please try again." should display ---//
            if (Locale.Equals("en"))
            {
                test.validateStringIsCorrect(driver, Elements.MyAnalog_AccountSettings_Email_Section_Error_Message, invalidVerificationCode_errorMessage);
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.MyAnalog_AccountSettings_Email_Section_Error_Message);
            }
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Input Validations are Working as Expected for Valid Inputs in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Input Validations are Working as Expected for Valid Inputs in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Input Validations are Working as Expected for Valid Inputs in JP Locale")]
        public void MyAnalog_AccountSettings_EmailSection_InputValidations_VerifyValidInputs(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- Login Credentials ---//
            if (Locale.Equals("zh") || Locale.Equals("cn"))
            {
                username = "test_zh_04252019@mailinator.com";
            }
            else if (Locale.Equals("jp"))
            {
                username = "test_jp_04252019@mailinator.com";
            }
            else
            {
                username = "myanalog_t3st_555@mailinator.com";
            }
            password = "Test_1234";

            //--- myAnalog Account Settings URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_accountSettings_url;

            //--- Action: Open account settings page ---//
            action.Navigate(driver, myAnalog_url);

            action.ILogin(driver, username, password);

            //----- Account Settings Test Plan > myAnalog Dashboard Tab > R6 > T5: Verify that the send verification code is enabled. (IQ-7782/AL-14786) -----//

            //--- Action: Click edit icon button. ---//
            action.IClick(driver, Elements.MyAnalog_AccountSettings_Email_Section_Edit_Button);

            //--- Action: Enter New Email Address in Enter New Email field. ---//
            string enterNewEmail_input = "t3st_" + Util.Configuration.Environment.ToLower() + "_newEmail_" + util.GenerateRandomNumber() + "@mailinator.com";
            action.IDeleteValueOnFields(driver, Elements.MyAnalog_AccountSettings_Email_Section_EnterNewEmail_InputBox);
            action.IType(driver, Elements.MyAnalog_AccountSettings_Email_Section_EnterNewEmail_InputBox, enterNewEmail_input);

            //--- Action: Enter the email again in Confirm New Email field. ---//
            string confirmNewEmail_input = enterNewEmail_input;
            action.IDeleteValueOnFields(driver, Elements.MyAnalog_AccountSettings_Email_Section_ConfirmNewEmail_InputBox);
            action.IType(driver, Elements.MyAnalog_AccountSettings_Email_Section_ConfirmNewEmail_InputBox, confirmNewEmail_input);
            Thread.Sleep(1000);

            //--- Expected Result: Send verification code is enabled. ---//
            test.validateElementIsEnabled(driver, Elements.MyAnalog_AccountSettings_Email_Section_SendVerificationCode_Button);
        }
    }
}