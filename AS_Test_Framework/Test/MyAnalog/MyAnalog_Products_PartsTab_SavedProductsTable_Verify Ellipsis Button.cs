﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System;
using System.Drawing;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_Products_PartsTab_SavedProductsTable_EllipsisButton : BaseSetUp
    {
        //--- myAnalog Products URL: ---//
        //--- DEV: https://my-dev.corpnt.analog.com/en/app/products ---//
        //--- QA: https://my-qa.corpnt.analog.com/en/app/products ---//
        //--- PROD: https://my.analog.com/en/app/products ---//

        public MyAnalog_Products_PartsTab_SavedProductsTable_EllipsisButton() : base() { }

        //--- Login Credentials ---//
        string username = "myanalog_t3st_pr0ducts_555@mailinator.com";
        string password = "Test_1234";

        //--- URLs ---//
        string myAnalog_products_url = "app/products";
        string pdp_url = "/ad7705";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Copy Link Option is Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Copy Link Option is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Copy Link Option is Present and Working as Expected in JP Locale")]
        public void MyAnalog_Products_PartsTab_SavedProductsTable_EllipsisButton_VerifyCopyLinkOption(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Products URL ---//
            string myAnalog_url = $"{Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt")}{ Locale }/{myAnalog_products_url}";

            //--- Action: Open myAnalog Products using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);

            //--- Action: Select the Parts Tab ---//
            if (!util.CheckElement(driver, Elements.MyAnalog_Products_Selected_Parts_Tab, 2))
            {
                action.IClick(driver, Elements.MyAnalog_Products_Parts_Tab);
            }

            //----- Product Test Plan > MAR>Dashboard>Products Tab > R7 > T3: Verify saved products are separated and displayed in a dark blue table header. -----//

            //--- Expected Result: Drop down option ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Products_Saved_Products_Ellipsis_Button);

            //----- Product Test Plan > MAR>Dashboard>Products Tab > R9 > T1: Verify if "share" in options dropdown change to "copy link" (IQ-9362/AL-14820) -----//

            //--- Action: Click menu/ Ellipsis. ---//
            action.IClick(driver, Elements.MyAnalog_Products_Saved_Products_Ellipsis_Button);

            //--- Expected Result: Copy link displayed ---//
            if (Locale.Equals("en"))
            {
                test.validateStringIsCorrect(driver, Elements.MyAnalog_Products_Saved_Products_CopyLink_Option, "Copy Link");
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.MyAnalog_Products_Saved_Products_CopyLink_Option);
            }
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Remove Option is Present and Working as Expected in EN Locale")]
        public void MyAnalog_Products_PartsTab_SavedProductsTable_EllipsisButton_VerifyRemoveOption(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- Action: Set the Window to a Larger Size since the Ellipsis Buttons are being cut in Small Screens ---//
            driver.Manage().Window.Size = new Size(1920, 1080);

            //--- Login Credentials ---//
            username = "myanalog_t3st_r3m0v3_pr0ducts_555@mailinator.com";
            password = "Test_1234";

            //--- myAnalog Products URL ---//
            string myAnalog_url = $"{Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt")}{ Locale }/{myAnalog_products_url}";

            //--- Action: Open myAnalog Products using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);

            //--- Action: Select the Parts Tab ---//
            if (!util.CheckElement(driver, Elements.MyAnalog_Products_Selected_Parts_Tab, 2))
            {
                action.IClick(driver, Elements.MyAnalog_Products_Parts_Tab);
            }

            //--- Action: Save Products in myAnalog if there's none ---//
            if (!util.CheckElement(driver, Elements.MyAnalog_Products_Saved_Products, 2))
            {
                action.ISaveProductsInMyAnalog(driver, Locale, pdp_url);

                driver.Navigate().GoToUrl(myAnalog_url);

                if (!util.CheckElement(driver, Elements.MyAnalog_Products_Selected_Parts_Tab, 2))
                {
                    action.IClick(driver, Elements.MyAnalog_Products_Parts_Tab);
                }
            }

            //----- Product Test Plan > MAR>Dashboard>Products Tab > R7 > T3.5.1: Verify the "Share" option. -----//

            //--- Action: Click on the dropdown option displayed as set of dots (3 dots) in the dark blue table header. ---//
            action.IClick(driver, Elements.MyAnalog_Products_Saved_Products_Ellipsis_Button);

            //--- Expected Result: Remove must be displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Products_Saved_Products_Remove_Option);

            //----- Product Test Plan > MAR>Dashboard>Products Tab > R7 > T3.5.2: Verify the "Remove" option. -----//

            //--- Action: Select/Click on "Remove" ---//
            action.IClick(driver, Elements.MyAnalog_Products_Saved_Products_Remove_Option);

            //--- Expected Result: A confirmation message should be displayed. ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Products_Remove_Confirmation_Modal);

            //--- Action: Click "Remove" ---//
            action.IClick(driver, Elements.MyAnalog_Products_Remove_Confirmation_Modal_Remove_Button);

            //--- Expected Result: Deletes the line item. ---//
            test.validateElementIsNotPresent(driver, Elements.MyAnalog_Products_Saved_Products);
        }
    }
}