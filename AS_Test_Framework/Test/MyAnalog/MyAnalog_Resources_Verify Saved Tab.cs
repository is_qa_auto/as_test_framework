﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System;
using System.Threading;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_Resources_SavedTab : BaseSetUp
    {
        //--- myAnalog Resources URL: ---//
        //--- DEV: https://my-dev.corpnt.analog.com/en/app/resources ---//
        //--- QA: https://my-qa.corpnt.analog.com/en/app/resources ---//
        //--- PROD: https://my.analog.com/en/app/resources ---//

        public MyAnalog_Resources_SavedTab() : base() { }

        //--- Login Credentials ---//
        string username = "myanalog_t3st_r3s0urc3s_555@mailinator.com";
        string password = "Test_1234";

        //--- URLs ---//
        string myAnalog_resources_url = "app/resources";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Saved Tab is Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Saved Tab is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Saved Tab is Present and Working as Expected in JP Locale")]
        public void MyAnalog_Resources_VerifySavedTab(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Resources URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_resources_url;

            //--- Action: Open resources direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);
            Thread.Sleep(1000);

            //----- Resources Test Plan > Desktop Tab > R2 > T1: Verify Display of Resources landing page -----//

            //--- Expected Result: The Saved Resources tab is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Resources_Saved_Tab);

            //----- Resources Test Plan > Desktop Tab > R2 > T4: Verify Saved Resources tab -----//

            //--- Action: Click Saved Resources tab ---//
            action.IClick(driver, Elements.MyAnalog_Resources_Saved_Tab);

            //--- Expected Result: List of saved resources tab will display ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Resources_Saved_Resources_Rows);

            //--- Expected Result: The Resources title is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Resources_Label);

            //--- Expected Result: The Resources description is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Resources_Description_Text);

            //--- Expected Result: The Date Added is available (IQ-9091/AL-15109) ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Resources_Saved_Resources_DateAdded_Value);

            //--- Expected Result: The Ellipsis is available (IQ-8155/AL-15309) ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Resources_Saved_Resources_Ellipsis_Button);
        }
    }
}