﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System;
using System.Threading;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_Products_PartsTab_ProductDetails : BaseSetUp
    {
        //--- myAnalog Products URL: ---//
        //--- DEV: https://my-dev.corpnt.analog.com/en/app/products ---//
        //--- QA: https://my-qa.corpnt.analog.com/en/app/products ---//
        //--- PROD: https://my.analog.com/en/app/products ---//

        public MyAnalog_Products_PartsTab_ProductDetails() : base() { }

        //--- Login Credentials ---//
        string username = "myanalog_t3st_pr0ducts_555@mailinator.com";
        string password = "Test_1234";

        //--- URLs ---//
        string myAnalog_products_url = "app/products";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Model Updates Table is Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Model Updates Table is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Model Updates Table is Present and Working as Expected in JP Locale")]
        public void MyAnalog_Products_PartsTab_ProductDetails_VerifyModelUpdatesTable(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Products URL ---//
            string myAnalog_url = $"{Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt")}{ Locale }/{myAnalog_products_url}";

            //--- Action: Open myAnalog Products using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);

            //--- Action: Select the Parts Tab ---//
            if (!util.CheckElement(driver, Elements.MyAnalog_Products_Selected_Parts_Tab, 2))
            {
                action.IClick(driver, Elements.MyAnalog_Products_Parts_Tab);
            }

            //--- Action: Click anywhere in dark blue table header ---//
            action.IClick(driver, Elements.MyAnalog_Products_Saved_Products_ManagePcnPdn);

            //----- Product Test Plan > MAR>Dashboard>Products Tab > R7 > T4.1: Verify the table content "Model". -----//

            //--- Expected Result: "Model" must be displayed as name of column in the table. ---//
            if (Locale.Equals("en"))
            {
                test.validateStringIsCorrect(driver, Elements.MyAnalog_Products_ProductDetails_Model_Column_Header, "Model");
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.MyAnalog_Products_ProductDetails_Model_Column_Header);
            }

            //----- Product Test Plan > MAR>Dashboard>Products Tab > R7 > T4.2: Verify the table content "Status". -----//

            //--- Expected Result: "Status" must be displayed as name of column in the table. ---//
            if (Locale.Equals("en"))
            {
                test.validateStringIsCorrect(driver, Elements.MyAnalog_Products_ProductDetails_Status_Column_Header, "Status");
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.MyAnalog_Products_ProductDetails_Status_Column_Header);
            }

            //----- Product Test Plan > MAR>Dashboard>Products Tab > R7 > T4.3: Verify the table content "Updates" (PCN/PDN Notifications). -----//

            //--- Expected Result: "Updates" must be displayed as name of column in the table. ---//
            if (Locale.Equals("en"))
            {
                test.validateStringIsCorrect(driver, Elements.MyAnalog_Products_ProductDetails_Updates_Column_Header, "Updates");
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.MyAnalog_Products_ProductDetails_Updates_Column_Header);
            }

            //----- Product Test Plan > MAR>Dashboard>Products Tab > R7 > T4.4: Verify the table content "Publication Date". -----//

            //--- Expected Result: "Publication Date" must be displayed as name of column in the table. ---//
            if (Locale.Equals("en"))
            {
                test.validateStringIsCorrect(driver, Elements.MyAnalog_Products_ProductDetails_PublicationDate_Column_Header, "Publication Date");
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.MyAnalog_Products_ProductDetails_PublicationDate_Column_Header);
            }

            //----- Product Test Plan > MAR>Dashboard>Products Tab > R7 > T4.5: Verify the table content "Notify Me". -----//

            //--- Expected Result: "Notify Me" must be displayed as name of column in the table. ---//
            if (Locale.Equals("en"))
            {
                test.validateStringIsCorrect(driver, Elements.MyAnalog_Products_ProductDetails_NotifyMe_Column_Header, "Notify Me");
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.MyAnalog_Products_ProductDetails_NotifyMe_Column_Header);
            }

            //----- Product Test Plan > MAR>Dashboard>Products Tab > R7 > T4: Verify the table associated with the saved product is displayed when user clicks anywhere in dark blue table header. -----//

            //--- Expected Result: Model Numbers is Present ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Products_ProductDetails_Model_Row_Value);

            //--- Expected Result: Status is Present ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Products_ProductDetails_Status_Row_Value);

            //--- Expected Result: PCN/PDN Notifications is Present ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Products_ProductDetails_Updates_Row_Value);

            //--- Expected Result: Publication Date is Present ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Products_ProductDetails_PublicationDate_Row_Value);

            //----- Product Test Plan > MAR>Dashboard>Products Tab > R7 > T4.5: Verify the table content "Notify Me". -----//

            //--- Expected Result: Toggle button (ON/OFF) must be displayed. ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Products_ProductDetails_NotifyMe_Row_Toggle_Button);
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Resources Links are Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Resources Links are Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Resources Links are Present and Working as Expected in JP Locale")]
        public void MyAnalog_Products_PartsTab_ProductDetails_VerifyResourcesLinks(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Products URL ---//
            string myAnalog_url = $"{Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt")}{ Locale }/{myAnalog_products_url}";

            //--- Action: Open myAnalog Products using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);

            //--- Action: Select the Parts Tab ---//
            if (!util.CheckElement(driver, Elements.MyAnalog_Products_Selected_Parts_Tab, 2))
            {
                action.IClick(driver, Elements.MyAnalog_Products_Parts_Tab);
            }

            //--- Action: Click anywhere in dark blue table header ---//
            action.IClick(driver, Elements.MyAnalog_Products_Saved_Products_ManagePcnPdn);

            //----- Product Test Plan > MAR>Dashboard>Products Tab > R7 > T4: Verify the table associated with the saved product is displayed when user clicks anywhere in dark blue table header. -----//

            //--- Expected Result: “Data sheet” link is Present ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Products_ProductDetails_Datasheet_Link);

            //----- Product Test Plan > MAR>Dashboard>Products Tab > R7 > T4: Verify the table associated with the saved product is displayed when user clicks anywhere in dark blue table header. -----//

            //--- Expected Result: “View all documentation” link is Present ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Products_ProductDetails_ViewAllDocumentation_Link);

            if (util.CheckElement(driver, Elements.MyAnalog_Products_ProductDetails_ViewAllDocumentation_Link, 1))
            {
                //----- Product Test Plan > MAR>Dashboard>Products Tab > R7 > T4.7: Verify the table content "View all documentation". -----//

                //--- Action: Click view all documentation link ---//
                action.IOpenLinkInNewTab(driver, Elements.MyAnalog_Products_ProductDetails_ViewAllDocumentation_Link);
                Thread.Sleep(4000);

                //--- Expected Result: anchors to the product’s documentation section on the product details page. ---//
                string actual_result = driver.Url;
                string expected_result = "#product-documentation";
                Console.WriteLine("Actual Result: " + actual_result);
                Console.WriteLine("Expected Result: " + expected_result);
                test.validateStringInstance(driver, actual_result, expected_result);
            }
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Related Tools are Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Related Tools are Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Related Tools are Present and Working as Expected in JP Locale")]
        public void MyAnalog_Products_PartsTab_ProductDetails_VerifyRelatedTools(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Products URL ---//
            string myAnalog_url = $"{Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt")}{ Locale }/{myAnalog_products_url}";

            //--- Action: Open myAnalog Products using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);

            //--- Action: Select the Parts Tab ---//
            if (!util.CheckElement(driver, Elements.MyAnalog_Products_Selected_Parts_Tab, 2))
            {
                action.IClick(driver, Elements.MyAnalog_Products_Parts_Tab);
            }

            //--- Action: Click anywhere in dark blue table header ---//
            action.IClick(driver, Elements.MyAnalog_Products_Saved_Products_ManagePcnPdn);

            //----- Product Test Plan > MAR>Dashboard>Products Tab > R7 > T4: Verify the table associated with the saved product is displayed when user clicks anywhere in dark blue table header. -----//

            //--- Expected Result: Related tools is Present ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Products_ProductDetails_RelatedTools_Section);
        }
    }
}