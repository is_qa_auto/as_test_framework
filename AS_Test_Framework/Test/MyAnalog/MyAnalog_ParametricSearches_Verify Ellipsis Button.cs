﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System;
using System.Drawing;
using System.Threading;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_ParametricSearches_EllipsisButton : BaseSetUp
    {
        //--- myAnalog Parametric Searches URL: ---//
        //--- DEV: https://my-dev.corpnt.analog.com/en/app/parametric-searches ---//
        //--- QA: https://my-qa.corpnt.analog.com/en/app/parametric-searches ---//
        //--- PROD: https://my.analog.com/en/app/parametric-searches ---//

        public MyAnalog_ParametricSearches_EllipsisButton() : base() { }

        //--- Login Credentials ---//
        string username = "myanalog_t3st_3d1t_pst_555@mailinator.com";
        string password = "Test_1234";

        //--- URLs ---//
        string myAnalog_parametricSearches_url = "app/parametric-searches";
        string pst_url = "/parametricsearch/11470";

        //--- Labels ---//
        string copyLink_text = "Copy Link";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Save Button in Edit title Option is Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Save Button in Edit title Option is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Save Button in Edit title Option is Present and Working as Expected in JP Locale")]
        public void MyAnalog_ParametricSearches_EllipsisButton_EditTitleOption_VerifySaveButton(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- Action: Set the Window to a Larger Size since the Ellipsis Buttons are being cut in Small Screens ---//
            driver.Manage().Window.Size = new Size(1920, 1080);

            //--- myAnalog Parametric Searches URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_parametricSearches_url;

            //--- Action: Open parametric search using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);

            //--- Action: Save PST in myAnalog if there's none ---//
            if (!util.CheckElement(driver, Elements.MyAnalog_ParametricSearches_Saved_ParametricSearches_Rows, 2))
            {
                action.ISavePstInMyAnalog(driver, Locale, pst_url);
                driver.Navigate().GoToUrl(myAnalog_url);
            }

            //--- Action: Get the URL Value of the Title of the 1st Row ---//
            string default_titleUrl_value = util.GetCssValue(driver, Elements.MyAnalog_ParametricSearches_Saved_ParametricSearches_1st_Row_Title_Value, "href");

            //----- Parametric Searches Test Plan > myAnalog Dashboard Tab > R5 > T1: Ability to edit the title of a record in Parametric Searches table -----//

            //--- Action: Click the ellipsis button of the desired record to edit ---//
            action.IClick(driver, Elements.MyAnalog_ParametricSearches_Saved_ParametricSearches_1st_Row_Ellipsis_Button);

            //--- Expected Result: Options to edit the title is displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_ParametricSearches_Saved_ParametricSearches_EditTitle_Option);

            //--- Action: Click the Edit Title option ---//
            action.IClick(driver, Elements.MyAnalog_ParametricSearches_Saved_ParametricSearches_EditTitle_Option);

            //--- Expected Result: Title cell is now editable and value is highlighted but will not be allowed to remove ---//
            test.validateStringInstance(driver, "editing", util.GetCssValue(driver, Elements.MyAnalog_ParametricSearches_Saved_ParametricSearches_1st_Row, "class"));

            //--- Action: Enter value to the title cell ---//
            string title_input = "T3st_" + Locale.ToUpper() + "_Title_Input_" + DateTime.Now.ToString("MMddyyyy") + "_" + util.RandomString();
            action.IDeleteValueOnFields(driver, Elements.MyAnalog_ParametricSearches_Saved_ParametricSearches_1st_Row_Title_InputBox);
            action.IType(driver, Elements.MyAnalog_ParametricSearches_Saved_ParametricSearches_1st_Row_Title_InputBox, title_input);

            //--- Action: Click Save button ---//
            action.IClick(driver, Elements.MyAnalog_ParametricSearches_Saved_ParametricSearches_Save_Button);

            //--- Expected Result: The title has been updated correctly ---//
            test.validateStringIsCorrect(driver, Elements.MyAnalog_ParametricSearches_Saved_ParametricSearches_1st_Row_Title_Value, title_input);

            //--- Expected Result: URL is still the same; editing the title will not affect the URL ---//
            test.validateStringInstance(driver, default_titleUrl_value, util.GetCssValue(driver, Elements.MyAnalog_ParametricSearches_Saved_ParametricSearches_1st_Row_Title_Value, "href"));
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Cancel Button in Edit title Option is Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Cancel Button in Edit title Option is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Cancel Button in Edit title Option is Present and Working as Expected in JP Locale")]
        public void MyAnalog_ParametricSearches_EllipsisButton_EditTitleOption_VerifCancelButton(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- Action: Set the Window to a Larger Size since the Ellipsis Buttons are being cut in Small Screens ---//
            driver.Manage().Window.Size = new Size(1920, 1080);

            //--- myAnalog Parametric Searches URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_parametricSearches_url;

            //--- Action: Open parametric search using direct link  ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);

            //--- Action: Save PST in myAnalog if there's none ---//
            if (!util.CheckElement(driver, Elements.MyAnalog_ParametricSearches_Saved_ParametricSearches_Rows, 2))
            {
                action.ISavePstInMyAnalog(driver, Locale, pst_url);
                driver.Navigate().GoToUrl(myAnalog_url);
            }

            //----- Parametric Searches Test Plan > myAnalog Dashboard Tab > R5 > T2: Clicking Cancel with not save the changes made -----//

            //--- Action: Get the Title Value of the 1st Row ---//
            string default_title_value = util.GetText(driver, Elements.MyAnalog_ParametricSearches_Saved_ParametricSearches_1st_Row_Title_Value);

            //--- Action: Click the ellipsis button of the desired record to edit ---//
            action.IClick(driver, Elements.MyAnalog_ParametricSearches_Saved_ParametricSearches_1st_Row_Ellipsis_Button);

            //--- Action: Click the Edit Title option ---//
            action.IClick(driver, Elements.MyAnalog_ParametricSearches_Saved_ParametricSearches_EditTitle_Option);

            //--- Action: Enter new value to the title cell ---//
            string title_input = "T3st_" + Locale.ToUpper() + "_Title_Input_" + DateTime.Now.ToString("MMddyyyy") + "_" + util.RandomString();
            action.IDeleteValueOnFields(driver, Elements.MyAnalog_ParametricSearches_Saved_ParametricSearches_1st_Row_Title_InputBox);
            action.IType(driver, Elements.MyAnalog_ParametricSearches_Saved_ParametricSearches_1st_Row_Title_InputBox, title_input);

            //--- Action: Click Cancel button ---//
            action.IClick(driver, Elements.MyAnalog_ParametricSearches_Saved_ParametricSearches_Cancel_Button);

            //--- Expected Result: System will NOT save the changes made and will revert to its original value ---//
            test.validateStringIsCorrect(driver, Elements.MyAnalog_ParametricSearches_Saved_ParametricSearches_1st_Row_Title_Value, default_title_value);
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Save Button in Edit Note Option is Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Save Button in Edit Note Option is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Save Button in Edit Note Option is Present and Working as Expected in JP Locale")]
        public void MyAnalog_ParametricSearches_EllipsisButton_EditNoteOption_VerifySaveButton(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- Action: Set the Window to a Larger Size since the Ellipsis Buttons are being cut in Small Screens ---//
            driver.Manage().Window.Size = new Size(1920, 1080);

            //--- myAnalog Parametric Searches URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_parametricSearches_url;

            //--- Action: Open parametric search using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);

            //--- Action: Save PST in myAnalog if there's none ---//
            if (!util.CheckElement(driver, Elements.MyAnalog_ParametricSearches_Saved_ParametricSearches_Rows, 2))
            {
                action.ISavePstInMyAnalog(driver, Locale, pst_url);
                driver.Navigate().GoToUrl(myAnalog_url);
            }

            //----- BLANK INPUT -----//

            //----- Parametric Searches Test Plan > myAnalog Dashboard Tab > R6 > T2.1: Verify when no note entered then clicking save -----//

            //--- Action: Click the ellipsis button of the desired record to edit---//
            action.IClick(driver, Elements.MyAnalog_ParametricSearches_Saved_ParametricSearches_1st_Row_Ellipsis_Button);

            //--- Action: Click the Add Note option ---//
            action.IClick(driver, Elements.MyAnalog_ParametricSearches_Saved_ParametricSearches_EditNote_Option);

            //--- Action: Do not Enter notes ---//
            string note_input = " ";
            action.IDeleteValueOnFields(driver, Elements.MyAnalog_ParametricSearches_Saved_ParametricSearches_1st_Row_Note_InputBox);
            action.IType(driver, Elements.MyAnalog_ParametricSearches_Saved_ParametricSearches_1st_Row_Note_InputBox, note_input);

            //--- Action: Click save ---//
            action.IClick(driver, Elements.MyAnalog_ParametricSearches_Saved_ParametricSearches_Save_Button);

            //--- Expected Result: System able to accept even note field is blank ---//
            test.validateStringIsCorrect(driver, Elements.MyAnalog_ParametricSearches_Saved_ParametricSearches_1st_Row_Note_Value, "");

            //----- MORE THAN 256 CHARACTERS INPUT -----//

            //----- Parametric Searches Test Plan > myAnalog Dashboard Tab > R6 > T1: Verify when adding a note in a Parametric Search -----//

            //--- Action: Click the ellipsis button of the desired record to edit ---//
            action.IClick(driver, Elements.MyAnalog_ParametricSearches_Saved_ParametricSearches_1st_Row_Ellipsis_Button);

            //--- Expected Result: Options to add note is displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_ParametricSearches_Saved_ParametricSearches_EditNote_Option);

            //--- Action: Click the Add Note option ---//
            action.IClick(driver, Elements.MyAnalog_ParametricSearches_Saved_ParametricSearches_EditNote_Option);

            //--- Expected Result: Note text field is enabled ---//
            test.validateStringInstance(driver, "editing", util.GetCssValue(driver, Elements.MyAnalog_ParametricSearches_Saved_ParametricSearches_1st_Row, "class"));

            //----- Parametric Searches Test Plan > myAnalog Dashboard Tab > R6 > T3.1: Verify when adding note with more that 256 characters -----//

            //--- Action: Enter characters more than 256 ---//
            note_input = "T3st_" + Locale.ToUpper() + "_More_Than_256_Characters_Note_Input_" + DateTime.Now.ToString("MMddyyyy") + "_" + util.RandomString()
                                + "_zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz";
            action.IDeleteValueOnFields(driver, Elements.MyAnalog_ParametricSearches_Saved_ParametricSearches_1st_Row_Note_InputBox);
            action.IType(driver, Elements.MyAnalog_ParametricSearches_Saved_ParametricSearches_1st_Row_Note_InputBox, note_input);

            //--- Action: Click Save button ---//
            action.IClick(driver, Elements.MyAnalog_ParametricSearches_Saved_ParametricSearches_Save_Button);

            //--- Expected Result: Note is successfully saved ---//
            test.validateStringIsCorrect(driver, Elements.MyAnalog_ParametricSearches_Saved_ParametricSearches_1st_Row_Note_Value, note_input);
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Cancel Button in Edit Note Option is Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Cancel Button in Edit Note Option is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Cancel Button in Edit Note Option is Present and Working as Expected in JP Locale")]
        public void MyAnalog_ParametricSearches_EllipsisButton_EditNoteOption_VerifyCancelButton(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- Action: Set the Window to a Larger Size since the Ellipsis Buttons are being cut in Small Screens ---//
            driver.Manage().Window.Size = new Size(1920, 1080);

            //--- myAnalog Parametric Searches URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_parametricSearches_url;

            //--- Action: Open parametric search using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);

            //--- Action: Save PST in myAnalog if there's none ---//
            if (!util.CheckElement(driver, Elements.MyAnalog_ParametricSearches_Saved_ParametricSearches_Rows, 2))
            {
                action.ISavePstInMyAnalog(driver, Locale, pst_url);
                driver.Navigate().GoToUrl(myAnalog_url);
            }

            //----- Parametric Searches Test Plan > myAnalog Dashboard Tab > R6 > T2: Clicking Cancel with not save the changes made -----//

            //--- Action: Get the Note Value of the 1st Row ---//
            string default_note_value = util.GetText(driver, Elements.MyAnalog_ParametricSearches_Saved_ParametricSearches_1st_Row_Note_Value);

            //--- Action: Click the ellipsis button of the desired record to edit ---//
            action.IClick(driver, Elements.MyAnalog_ParametricSearches_Saved_ParametricSearches_1st_Row_Ellipsis_Button);

            //--- Action: Click the Add Note option ---//
            action.IClick(driver, Elements.MyAnalog_ParametricSearches_Saved_ParametricSearches_EditNote_Option);

            //--- Action: Enter new value to the Note cell ---//
            string note_input = "T3st_" + Locale.ToUpper() + "_Note_Input_" + DateTime.Now.ToString("MMddyyyy") + "_" + util.RandomString();
            action.IDeleteValueOnFields(driver, Elements.MyAnalog_ParametricSearches_Saved_ParametricSearches_1st_Row_Note_InputBox);
            action.IType(driver, Elements.MyAnalog_ParametricSearches_Saved_ParametricSearches_1st_Row_Note_InputBox, note_input);

            //--- Action: Click Cancel button ---//
            action.IClick(driver, Elements.MyAnalog_ParametricSearches_Saved_ParametricSearches_Cancel_Button);

            //--- Expected Result: System will NOT save the changes made and will revert to its original value ---//
            test.validateStringIsCorrect(driver, Elements.MyAnalog_ParametricSearches_Saved_ParametricSearches_1st_Row_Note_Value, default_note_value);
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Copy Link Option is Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Copy Link Option is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Copy Link Option is Present and Working as Expected in JP Locale")]
        public void MyAnalog_ParametricSearches_EllipsisButton_VerifyCopyLinkOption(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- Action: Set the Window to a Larger Size since the Ellipsis Buttons are being cut in Small Screens ---//
            driver.Manage().Window.Size = new Size(1920, 1080);

            //--- myAnalog Parametric Searches URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_parametricSearches_url;

            //--- Action: Open parametric search using direct link  ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);

            //--- Action: Save PST in myAnalog if there's none ---//
            if (!util.CheckElement(driver, Elements.MyAnalog_ParametricSearches_Saved_ParametricSearches_Rows, 2))
            {
                action.ISavePstInMyAnalog(driver, Locale, pst_url);
                driver.Navigate().GoToUrl(myAnalog_url);
            }

            //----- Parametric Searches Test Plan > myAnalog Dashboard Tab > R8 > T1: Verify if "share" in options dropdown change to "copy link" (IQ-9362/AL-14820) -----//

            //--- Action: Click menu/ Ellipsis. ---//
            action.IClick(driver, Elements.MyAnalog_ParametricSearches_Saved_ParametricSearches_1st_Row_Ellipsis_Button);

            //--- Expected Result: Copy link displayed ---//
            if (Locale.Equals("en"))
            {
                test.validateStringIsCorrect(driver, Elements.MyAnalog_ParametricSearches_Saved_ParametricSearches_CopyLink_Option, copyLink_text);
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.MyAnalog_ParametricSearches_Saved_ParametricSearches_CopyLink_Option);
            }
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Remove Button in Remove Option is Present and Working as Expected in EN Locale")]
        public void MyAnalog_ParametricSearches_EllipsisButton_RemoveOption_VerifyRemoveButton(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- Action: Set the Window to a Larger Size since the Ellipsis Buttons are being cut in Small Screens ---//
            driver.Manage().Window.Size = new Size(1920, 1080);

            //--- Login Credentials ---//
            username = "myanalog_t3st_r3m0v3_pst_555@mailinator.com";
            password = "Test_1234";

            //--- myAnalog Parametric Searches URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_parametricSearches_url;

            //--- Action: Open parametric search using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);

            //--- Action: Save PST in myAnalog if there's none ---//
            if (!util.CheckElement(driver, Elements.MyAnalog_ParametricSearches_Saved_ParametricSearches_Rows, 2))
            {
                action.ISavePstInMyAnalog(driver, Locale, pst_url);
                driver.Navigate().GoToUrl(myAnalog_url);
            }

            //----- Parametric Searches Test Plan > myAnalog Dashboard Tab > R7 > T3: Verify that Message prompt is displayed upon removing Parametric Search (IQ-7680/AL-13482) -----//

            //--- Action: Get the Title Value of the 1st Row ---//
            string default_title_value = util.GetText(driver, Elements.MyAnalog_ParametricSearches_Saved_ParametricSearches_1st_Row_Title_Value);

            //--- Action: Click the ellipsis button of the desired record to remove ---//
            action.IClick(driver, Elements.MyAnalog_ParametricSearches_Saved_ParametricSearches_1st_Row_Ellipsis_Button);

            //--- Expected Result: Options to remove is displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_ParametricSearches_Saved_ParametricSearches_Remove_Option);

            //--- Action: Click the Remove option ---//
            action.IClick(driver, Elements.MyAnalog_ParametricSearches_Saved_ParametricSearches_Remove_Option);

            if (Locale.Equals("en"))
            {
                //--- Expected Result: System will display confirmation message "You're about to remove <title>  from your parametric searches. Continue?" ---//
                test.validateStringIsCorrect(driver, Elements.MyAnalog_ParametricSearches_Remove_Confirmation_Modal_Message, "Are you sure you want to remove, \"" + default_title_value + "\" from your Parametric Searches?");
            }
            else
            {
                //--- Expected Result: System will display confirmation message ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_ParametricSearches_Remove_Confirmation_Modal_Message);
            }
            //----- Parametric Searches Test Plan > myAnalog Dashboard Tab > R7 > T1: Ability to remove desired record in Parametric Searches table -----//

            //--- Action: Click Remove button ---//
            action.IClick(driver, Elements.MyAnalog_ParametricSearches_Remove_Confirmation_Modal_Remove_Button);

            //--- Expected Result: System successfully removed the selected record ---//
            test.validateElementIsNotPresent(driver, Elements.MyAnalog_ParametricSearches_Saved_ParametricSearches_Rows);
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Cancel Button in Remove Option is Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Cancel Button in Remove Option is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Cancel Button in Remove Option is Present and Working as Expected in JP Locale")]
        public void MyAnalog_ParametricSearches_EllipsisButton_RemoveOption_VerifyCancelButton(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- Action: Set the Window to a Larger Size since the Ellipsis Buttons are being cut in Small Screens ---//
            driver.Manage().Window.Size = new Size(1920, 1080);

            //--- Login Credentials ---//
            username = "myanalog_t3st_r3m0v3_pst_555@mailinator.com";
            password = "Test_1234";

            //--- myAnalog Parametric Searches URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_parametricSearches_url;

            //--- Action: Open parametric search using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);

            //--- Action: Save PST in myAnalog if there's none ---//
            if (!util.CheckElement(driver, Elements.MyAnalog_ParametricSearches_Saved_ParametricSearches_Rows, 2))
            {
                action.ISavePstInMyAnalog(driver, Locale, pst_url);
                driver.Navigate().GoToUrl(myAnalog_url);
            }

            //----- Parametric Searches Test Plan > myAnalog Dashboard Tab > R7 > T2: Clicking Cancel will not remove the selected record -----//

            //--- Action: Click the ellipsis button of the desired record to remove ---//
            action.IClick(driver, Elements.MyAnalog_ParametricSearches_Saved_ParametricSearches_1st_Row_Ellipsis_Button);

            //--- Action: Click the Remove option ---//
            action.IClick(driver, Elements.MyAnalog_ParametricSearches_Saved_ParametricSearches_Remove_Option);

            //--- Action: Click Cancel button ---//
            action.IClick(driver, Elements.MyAnalog_ParametricSearches_Remove_Confirmation_Modal_Cancel_Button);

            //--- Expected Result: System will NOT remove the selected record ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_ParametricSearches_Saved_ParametricSearches_Rows);
        }
    }
}