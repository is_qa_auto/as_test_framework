﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_Projects_InviteModal_InputValidations : BaseSetUp
    {
        //--- myAnalog Projects URL: ---//
        //--- DEV: https://my-dev.corpnt.analog.com/en/app/projects ---//
        //--- QA: https://my-qa.corpnt.analog.com/en/app/projects ---//
        //--- PROD: https://my.analog.com/en/app/projects ---//

        public MyAnalog_Projects_InviteModal_InputValidations() : base() { }

        //----- Note: Please don't edit/delete any details on the Accounts below -----//

        //--- Login Credentials ---//
        string username = "my_t3st_pr0j3cts_555@mailinator.com";
        string password = "Test_1234";

        //--- URLs ---//
        string myAnalog_projects_url = "app/projects";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Input Validations are Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Input Validations are Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Input Validations are Working as Expected in JP Locale")]
        public void MyAnalog_Projects_InviteModal_VerifyInputValidations(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Projects URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_projects_url;

            //--- Action: Open myAnalog Projects using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);

            //--- Action: Click the Invite button ---//
            action.IClick(driver, Elements.MyAnalog_Projects_Invite_Button);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify previously selected quick message is clickable/selectable (on the previous failed attempt of creating new project) (IQ-6372/AL-13530) -----//

            //--- Action: Select quick message ---//
            action.IClick(driver, Elements.MyAnalog_Projects_Invite_Modal_QuickMessage_Buttons);

            //--- Action: Click Invite button ---//
            action.IClick(driver, Elements.MyAnalog_Projects_Invite_Modal_Invite_Button);

            //--- Expected Result: Prompt of failure in creating project is displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Projects_Invite_Modal_Error_Message);
        }
    }
}