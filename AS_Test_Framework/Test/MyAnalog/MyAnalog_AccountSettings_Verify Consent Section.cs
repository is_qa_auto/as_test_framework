﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_AccountSettings_ConsentSection : BaseSetUp
    {
        //--- myAnalog Account Settings URL: ---//
        //--- DEV: https://my-dev.corpnt.analog.com/en/app/account ---//
        //--- QA: https://my-qa.corpnt.analog.com/en/app/account ---//
        //--- PROD: https://my.analog.com/en/app/account ---//

        public MyAnalog_AccountSettings_ConsentSection() : base() { }

        //--- Login Credentials ---//
        string username = null;
        string password = null;

        //--- URLs ---//
        string myAnalog_accountSettings_url = "app/account";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Elements are Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Elements are Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Elements are Present and Working as Expected in JP Locale")]
        public void MyAnalog_AccountSettings_VerifyConsentSection(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- Login Credentials ---//
            if (Locale.Equals("en"))
            {
                username = "myanalog_t3st_999@mailinator.com";
            }
            else if (Locale.Equals("zh") || Locale.Equals("cn"))
            {
                username = "myanalog_t3st_cn_777@mailinator.com";
            }
            else
            {
                if (Util.Configuration.Environment.Equals("dev"))
                {
                    username = "test_jp_04252019@mailinator.com";
                }
                else
                {
                    username = "myanalog_t3st_jp_777@mailinator.com";
                }
            }
            password = "Test_1234";

            //--- myAnalog Account Settings URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_accountSettings_url;

            //--- Action: Open account settings page ---//
            action.Navigate(driver, myAnalog_url);

            action.ILogin(driver, username, password);

            //----- Account Settings Test Plan > myAnalog Dashboard Tab > R2 > T1: Verify Account Settings Landing Page -----//

            //--- Expected Result: The Account Settings title is displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_AccountSettings_Label);
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Submit Button is Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Submit Button is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Submit Button is Present and Working as Expected in JP Locale")]
        public void MyAnalog_AccountSettings_ConsentSection_VerifySubmitButton(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- Login Credentials ---//
            if (Locale.Equals("en"))
            {
                username = "myanalog_t3st_999@mailinator.com";
            }
            else if (Locale.Equals("zh") || Locale.Equals("cn"))
            {
                username = "myanalog_t3st_cn_777@mailinator.com";
            }
            else
            {
                if (Util.Configuration.Environment.Equals("dev"))
                {
                    username = "test_jp_04252019@mailinator.com";
                }
                else
                {
                    username = "myanalog_t3st_jp_777@mailinator.com";
                }
            }
            password = "Test_1234";

            //--- myAnalog Account Settings URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_accountSettings_url;

            //--- Action: Open account settings page ---//
            action.Navigate(driver, myAnalog_url);

            action.ILogin(driver, username, password);

            //----- Account Settings Test Plan > myAnalog Dashboard Tab > R2 > T1: Verify Consent update is working (IQ-8732/AL-15196) -----//

            //--- Action: Check or uncheck consent checkbox ---//
            bool selected_consentEmail = true;
            if (driver.FindElement(Elements.MyAnalog_AccountSettings_Consent_Section_ConsentEmail_Checkboxes).Selected)
            {
                action.IUncheck(driver, Elements.MyAnalog_AccountSettings_Consent_Section_ConsentEmail_Checkboxes);

                selected_consentEmail = false;
            }
            else
            {
                action.ICheck(driver, Elements.MyAnalog_AccountSettings_Consent_Section_ConsentEmail_Checkboxes);
            }

            //--- Action: Check or uncheck consent checkbox ---//
            bool selected_consentPhone = true;
            if (driver.FindElement(Elements.MyAnalog_AccountSettings_Consent_Section_ConsentPhone_Checkboxes).Selected)
            {
                action.IUncheck(driver, Elements.MyAnalog_AccountSettings_Consent_Section_ConsentPhone_Checkboxes);

                selected_consentPhone = false;
            }
            else
            {
                action.ICheck(driver, Elements.MyAnalog_AccountSettings_Consent_Section_ConsentPhone_Checkboxes);
            }

            //--- Action: Click Submit button ---//
            action.IClick(driver, Elements.MyAnalog_AccountSettings_Consent_Section_Submit_Button);

            //--- Expected Result: Save changed. ---//
            if (selected_consentEmail.Equals(true))
            {
                test.validateElementIsSelected(driver, Elements.MyAnalog_AccountSettings_Consent_Section_ConsentEmail_Checkboxes);
            }
            else
            {
                test.validateElementIsNotSelected(driver, Elements.MyAnalog_AccountSettings_Consent_Section_ConsentEmail_Checkboxes);
            }

            //--- Expected Result: Save changed. ---//
            if (selected_consentPhone.Equals(true))
            {
                test.validateElementIsSelected(driver, Elements.MyAnalog_AccountSettings_Consent_Section_ConsentPhone_Checkboxes);
            }
            else
            {
                test.validateElementIsNotSelected(driver, Elements.MyAnalog_AccountSettings_Consent_Section_ConsentPhone_Checkboxes);
            }

            //--- Action: Reload the Page ---//
            driver.Navigate().Refresh();
            Console.WriteLine("Refresh the Page.");

            //--- Expected Result: The update is retained ---//
            if (selected_consentEmail.Equals(true))
            {
                test.validateElementIsSelected(driver, Elements.MyAnalog_AccountSettings_Consent_Section_ConsentEmail_Checkboxes);
            }
            else
            {
                test.validateElementIsNotSelected(driver, Elements.MyAnalog_AccountSettings_Consent_Section_ConsentEmail_Checkboxes);
            }

            //--- Expected Result: The update is retained ---//
            if (selected_consentPhone.Equals(true))
            {
                test.validateElementIsSelected(driver, Elements.MyAnalog_AccountSettings_Consent_Section_ConsentPhone_Checkboxes);
            }
            else
            {
                test.validateElementIsNotSelected(driver, Elements.MyAnalog_AccountSettings_Consent_Section_ConsentPhone_Checkboxes);
            }
        }
    }
}