﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_Products_PartsTab : BaseSetUp
    {
        //--- myAnalog Products URL: ---//
        //--- DEV: https://my-dev.corpnt.analog.com/en/app/products ---//
        //--- QA: https://my-qa.corpnt.analog.com/en/app/products ---//
        //--- PROD: https://my.analog.com/en/app/products ---//

        public MyAnalog_Products_PartsTab() : base() { }

        //--- Login Credentials ---//
        string username = "myanalog_t3st_pr0ducts_555@mailinator.com";
        string password = "Test_1234";

        //--- URLs ---//
        string myAnalog_products_url = "app/products";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Add products to myAnalog - Search Input Box is Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Add products to myAnalog - Search Input Box is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Add products to myAnalog - Search Input Box is Present and Working as Expected in JP Locale")]
        public void MyAnalog_Products_PartsTab_VerifyAddProductsToMyAnalog(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Products URL ---//
            string myAnalog_url = $"{Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt")}{ Locale }/{myAnalog_products_url}";

            //--- Action: Open myAnalog Products using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);

            //--- Action: Select the Parts Tab ---//
            if (!util.CheckElement(driver, Elements.MyAnalog_Products_Selected_Parts_Tab, 2))
            {
                action.IClick(driver, Elements.MyAnalog_Products_Parts_Tab);
            }

            //----- Product Test Plan > MAR>Dashboard>Products Tab > R5 > T1: Verify "Add products to myAnalog" search textbox -----//

            //--- Expected Result: "Add products to myAnalog" search textbox must be displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Products_AddProductsToMyAnalog_InputBox);

            //----- Product Test Plan > MAR>Dashboard>Products Tab > R5 > T3: Verify that autosuggest works accrodingly (IQ-7913/AL-14001) -----//

            //--- Action: Enter any product model/part number and click the search icon (e.g. "ADR435") ---//
            string addProductsToMyAnalog_input = "AD7";
            action.IDeleteValueOnFields(driver, Elements.MyAnalog_Products_AddProductsToMyAnalog_InputBox);
            action.IType(driver, Elements.MyAnalog_Products_AddProductsToMyAnalog_InputBox, addProductsToMyAnalog_input);

            //--- Expected Result: Auto-suggest is activated upon entering minimum of 3 characters ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Products_AddProductsToMyAnalog_AutoSuggest_List);

            //----- Product Test Plan > MAR>Dashboard>Products Tab > R5 > T2: Verify adding non existing product in "Add products to myAnalog" search textbox (IQ-6406/AL-13364) -----//

            //--- Action: Enter a non existing product and click add button (e.g. "adsexeee")---//
            addProductsToMyAnalog_input = "adsexeee";
            action.IDeleteValueOnFields(driver, Elements.MyAnalog_Products_AddProductsToMyAnalog_InputBox);
            action.IType(driver, Elements.MyAnalog_Products_AddProductsToMyAnalog_InputBox, addProductsToMyAnalog_input);

            //--- Expected Result: An error message "! No Result found. Please Modify your query" will be displayed ---//
            string addProductsToMyAnalog_invalidInput_errorMessage = "No results found.\r\nPlease modify your query.";
            if (Locale.Equals("en"))
            {
                test.validateStringIsCorrect(driver, Elements.MyAnalog_Products_AddProductsToMyAnalog_InvalidInput_ErrorMessage, addProductsToMyAnalog_invalidInput_errorMessage);
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.MyAnalog_Products_AddProductsToMyAnalog_InvalidInput_ErrorMessage);
            }
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Pagination is Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Pagination is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Pagination is Present and Working as Expected in JP Locale")]
        public void MyAnalog_Products_PartsTab_VerifyPagination(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Products URL ---//
            string myAnalog_url = $"{Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt")}{ Locale }/{myAnalog_products_url}";

            //--- Action: Open myAnalog Products using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);

            //--- Action: Select the Parts Tab ---//
            if (!util.CheckElement(driver, Elements.MyAnalog_Products_Selected_Parts_Tab, 2))
            {
                action.IClick(driver, Elements.MyAnalog_Products_Parts_Tab);
            }

            //----- Product Test Plan > MAR>Dashboard>Products Tab > R7 > T7: Verify Pagination Button (IQ-6365/AL-13329) -----//

            //--- Expected Result: The pagination button will display "Privious" Button ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Products_Previous_Button);
        }
    }
}