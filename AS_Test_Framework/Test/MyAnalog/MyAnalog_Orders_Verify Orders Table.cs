﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System;
using System.Threading;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_Orders_OrdersTable : BaseSetUp
    {
        //--- myAnalog Orders URL: ---//
        //--- DEV: https://my-dev.corpnt.analog.com/en/app/orders ---//
        //--- QA: https://my-qa.corpnt.analog.com/en/app/orders ---//
        //--- PROD: https://my.analog.com/en/app/orders ---//

        public MyAnalog_Orders_OrdersTable() : base() { }

        //--- Login Credentials ---//
        string username = "marvin.bebe@analog.com";
        string password = "Test_1234";

        //--- URLs ---//
        string myAnalog_orders_url = "app/orders";
        string myAnalog_orderNumber_url_format = "app/orders/";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Elements are Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Elements are Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Elements are Present and Working as Expected in JP Locale")]
        public void MyAnalog_Orders_VerifyOrdersTable(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Orders URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_orders_url;

            //--- Action: Open order page direct link to browser ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Enter valid username and password and click login button ---//
            action.ILogin(driver, username, password);

            //----- Orders and Order Details Test Plan > Test Case Title: Verify Order Landing Page -----//

            //--- Expected Result: The Order title page is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Orders_Label);

            //----- COLUMN HEADERS -----//

            //----- Orders and Order Details Test Plan > Test Case Title: Verify Order Landing Page -----//

            //--- Expected Result: The Order Number is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Orders_OrderNumber_Column_Header);

            //--- Expected Result: The Order Date is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Orders_OrderDate_Column_Header);

            //--- Expected Result: The Model is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Orders_Model_Column_Header);

            //--- Expected Result: The Order Type is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Orders_OrderType_Column_Header);

            //--- Expected Result: The Quantity is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Orders_Quantity_Column_Header);

            //--- Expected Result: The Status is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Orders_Status_Column_Header);

            //----- ROWS -----//

            //----- Orders and Order Details Test Plan > Test Case Title: Verify Order Number -----//

            //--- Expected Result: Order number contains 10 characters ---//
            test.validateCountIsEqual(driver, 10, util.GetText(driver, Elements.MyAnalog_Orders_OrderNumber_Row_Link).Length);

            if (util.CheckElement(driver, Elements.MyAnalog_Orders_OrderNumber_Row_Link, 1))
            {
                //--- Action: Click order number ---//
                action.IOpenLinkInNewTab(driver, Elements.MyAnalog_Orders_OrderNumber_Row_Link);
                Thread.Sleep(1000);

                //--- Expected Result: Page will redirect to order detail page ---//
                test.validateStringInstance(driver, driver.Url, myAnalog_orderNumber_url_format);
            }
        }
    }
}