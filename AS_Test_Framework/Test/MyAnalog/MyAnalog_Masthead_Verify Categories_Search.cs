﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System;
using OpenQA.Selenium;
using System.Threading;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_Masthead_Categories_Search : BaseSetUp
    {
        public MyAnalog_Masthead_Categories_Search() : base() { }

        //--- Login Credentials ---//
        string username = "aries.sorosoro@analog.com";
        string password = "Test_1234";

        [Test, Category("MyAnalog"), Category("NonCore")]
        [TestCase("en", "Power Management", TestName = "Verify Searching product category in myAnalog Masthead is working in EN Locale")]
        [TestCase("zh", "音频和视频产品", TestName = "Verify Searching product category in myAnalog Masthead is working in CN Locale")]
        [TestCase("jp", "アンプ", TestName = "Verify Searching product category in myAnalog Masthead is working in JP Locale")]
        public void MyAnalog_Masthead_VerifySearchInCategories(string Locale, string Search_Keyword)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Resources URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/app";

            //--- Action: Open MyAnalog App direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);

            action.IClick(driver, Elements.MyAnalog_Masthead_Category_DD);

            action.IType(driver, Elements.MyAnalog_Masthead_Category_Search, Search_Keyword);
            Thread.Sleep(2000);

            test.validateStringIsCorrect(driver, By.CssSelector("section[class='interests__menu__items__options'] ul>li:nth-child(1) span"), Search_Keyword);
        }
    }
}