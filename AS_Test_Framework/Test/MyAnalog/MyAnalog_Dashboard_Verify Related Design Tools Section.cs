﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using OpenQA.Selenium;
using System;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_Dashboard_RelatedDesignToolsSection : BaseSetUp
    {
        public MyAnalog_Dashboard_RelatedDesignToolsSection() : base() { }

        //-- Login Credentials --//
        string username = "marvin.bebe@analog.com";
        string password = "Test_1234";

        //--- URLs ---//
        string myAnalog_dashboard_url = "app";
        string pdp_url_format = "/products/";
        string ltSpice_url = "/ltspice-simulator.html";
        string utilities_vrmsDbmDbuDbvCalculator_url = "/dbconvert.html";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", "LTspice", TestName = "Verify that the LTspice Link is Working as Expected in EN Locale")]
        [TestCase("zh", "LTspice", TestName = "Verify that the LTspice Link is Working as Expected in CN Locale")]
        [TestCase("jp", "LTspice", TestName = "Verify that the LTspice Link is Working as Expected in JP Locale")]
        public void MyAnalog_Dashboard_RelatedDesignToolsSection_VerifyLtSpiceLink(string Locale, string LtSpice_Label)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Dashboard URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_dashboard_url;

            //--- Action: Go to myAnalog dashboard ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid user account in the myAnalog Login page ---//
            action.ILogin(driver, username, password);
            Thread.Sleep(1000);

            if (util.CheckElement(driver, Elements.MyAnalog_Dashboard_SeeAll_SeeLess_Button, 2))
            {
                //--- Action: Click the See all Button ---//
                action.IClick(driver, Elements.MyAnalog_Dashboard_SeeAll_SeeLess_Button);
            }

            //--- Locators ---//
            string ltSpice_link_locator = null;
            string ltSpice_link_text = null;
            string ltSpice_part_links_locator = null;

            for (int relatedDesignTools_list_counter = 1; util.CheckElement(driver, By.CssSelector("ul[class^='tools']>li:nth-of-type(" + relatedDesignTools_list_counter + ")"), 2); relatedDesignTools_list_counter++)
            {
                ltSpice_link_locator = "ul[class^='tools']>li:nth-of-type(" + relatedDesignTools_list_counter + ")>a";
                ltSpice_link_text = util.GetText(driver, By.CssSelector(ltSpice_link_locator));
                ltSpice_part_links_locator = "ul[class^='tools']>li:nth-of-type(" + relatedDesignTools_list_counter + ")>ul>li>a";

                if (ltSpice_link_text.Equals(LtSpice_Label) || ltSpice_link_text.Equals("LTspice"))
                {
                    if (util.CheckElement(driver, By.CssSelector(ltSpice_link_locator), 2))
                    {
                        //----- Dashboard Test Plan > Desktop Tab > R5 > T2.1.1: Verify when clicking Ltspice link -----//

                        //--- Action: Click Ltspice link ---//
                        action.IOpenLinkInNewTab(driver, By.CssSelector(ltSpice_link_locator));
                        Thread.Sleep(1000);

                        //--- Expected Result: Ltspice link will displayed ---//
                        test.validateStringInstance(driver, driver.Url, ltSpice_url);

                        driver.Close();
                        driver.SwitchTo().Window(driver.WindowHandles.Last());
                    }

                    //----- Dashboard Test Plan > Desktop Tab > R5 > T2.1.2: Verify Product available for Ltspice -----//

                    //--- Expected Result: List of product is displayed ---//
                    test.validateElementIsPresent(driver, By.CssSelector(ltSpice_part_links_locator));

                    if (util.CheckElement(driver, By.CssSelector(ltSpice_part_links_locator), 5))
                    {
                        //----- Dashboard Test Plan > Desktop Tab > R5 > T2.1.3: Verify redirection when clicking Product under Ltspice -----//

                        //--- Action: Click Product link ---//
                        action.IOpenLinkInNewTab(driver, By.CssSelector(ltSpice_part_links_locator));
                        Thread.Sleep(2000);

                        //--- Expected Result: Page will redirect to Product detail page ---//
                        test.validateStringInstance(driver, driver.Url, pdp_url_format);
                    }

                    break;
                }
            }
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", "VRMS/dBm/dBu/dBV calculators", TestName = "Verify that the VRMS/dBm/dBu/dBV calculators Link is Working as Expected in EN Locale")]
        [TestCase("zh", "VRMS/dBm/dBu/dBV计算器", TestName = "Verify that the VRMS/dBm/dBu/dBV calculators Link is Working as Expected in CN Locale")]
        [TestCase("jp", "VRMS/dBm/dBu/dBVカリキュレータ", TestName = "Verify that the VRMS/dBm/dBu/dBV calculators Link is Working as Expected in JP Locale")]
        public void MyAnalog_Dashboard_RelatedDesignToolsSection_VerifyVrmsDbmDbuDbvCalculatorsLink(string Locale, string VrmsDbmDbuDbvCalculators_Label)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Dashboard URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_dashboard_url;

            //--- Action: Go to myAnalog dashboard ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid user account in the myAnalog Login page ---//
            action.ILogin(driver, username, password);
            Thread.Sleep(1000);

            if (util.CheckElement(driver, Elements.MyAnalog_Dashboard_SeeAll_SeeLess_Button, 2))
            {
                //--- Action: Click the See all Button ---//
                action.IClick(driver, Elements.MyAnalog_Dashboard_SeeAll_SeeLess_Button);
            }

            //--- Locators ---//
            string vrmsDbmDbuDbvCalculators_link_locator = null;
            string vrmsDbmDbuDbvCalculators_link_text = null;
            string vrmsDbmDbuDbvCalculators_part_links_locator = null;

            for (int relatedDesignTools_list_counter = 1; util.CheckElement(driver, By.CssSelector("ul[class^='tools']>li:nth-of-type(" + relatedDesignTools_list_counter + ")"), 2); relatedDesignTools_list_counter++)
            {
                vrmsDbmDbuDbvCalculators_link_locator = "ul[class^='tools']>li:nth-of-type(" + relatedDesignTools_list_counter + ")>a";
                vrmsDbmDbuDbvCalculators_link_text = util.GetText(driver, By.CssSelector(vrmsDbmDbuDbvCalculators_link_locator));
                vrmsDbmDbuDbvCalculators_part_links_locator = "ul[class^='tools']>li:nth-of-type(" + relatedDesignTools_list_counter + ")>ul>li>a";

                if (vrmsDbmDbuDbvCalculators_link_text.Equals(VrmsDbmDbuDbvCalculators_Label) || vrmsDbmDbuDbvCalculators_link_text.StartsWith("VRMS/dBm/dBu/dBV"))
                {
                    if (util.CheckElement(driver, By.CssSelector(vrmsDbmDbuDbvCalculators_link_locator), 2))
                    {
                        //----- Dashboard Test Plan > Desktop Tab > R5 > T2.4.1: Verify when clicking VRMS/dBm/dBu/dBV calculators link -----//

                        //--- Action: Click VRMS/dBm/dBu/dBV calculators link ---//
                        action.IOpenLinkInNewTab(driver, By.CssSelector(vrmsDbmDbuDbvCalculators_link_locator));

                        //--- Expected Result: VRMS/dBm/dBu/dBV calculatorslink will displayed ---//
                        test.validateStringInstance(driver, driver.Url, utilities_vrmsDbmDbuDbvCalculator_url);

                        driver.Close();
                        driver.SwitchTo().Window(driver.WindowHandles.Last());
                    }

                    //----- Dashboard Test Plan > Desktop Tab > R5 > T2.4.2: Verify Product available for VRMS/dBm/dBu/dBV calculators -----//

                    //--- Expected Result: List of product is displayed ---//
                    test.validateElementIsPresent(driver, By.CssSelector(vrmsDbmDbuDbvCalculators_part_links_locator));

                    if (util.CheckElement(driver, By.CssSelector(vrmsDbmDbuDbvCalculators_part_links_locator), 5))
                    {
                        //----- Dashboard Test Plan > Desktop Tab > R5 > T2.4.3: Verify redirection when clicking Product under VRMS/dBm/dBu/dBV calculators -----//

                        //--- Action: Click Product link ---//
                        action.IOpenLinkInNewTab(driver, By.CssSelector(vrmsDbmDbuDbvCalculators_part_links_locator));
                        Thread.Sleep(2000);

                        //--- Expected Result: Page will redirect to Product detail page ---//
                        test.validateStringInstance(driver, driver.Url, pdp_url_format);
                    }

                    break;
                }
            }
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", "ADIsimRF", TestName = "Verify that the ADIsimRF Link is Working as Expected in EN Locale")]
        [TestCase("zh", "ADIsimRF", TestName = "Verify that the ADIsimRF Link is Working as Expected in CN Locale")]
        [TestCase("jp", "ADIsimRF", TestName = "Verify that the ADIsimRF Link is Working as Expected in JP Locale")]
        public void MyAnalog_Dashboard_RelatedDesignToolsSection_VerifyAdiSimRfLink(string Locale, string AdiSimRf_Label)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Dashboard URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_dashboard_url;

            //--- Action: Go to myAnalog dashboard ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid user account in the myAnalog Login page ---//
            action.ILogin(driver, username, password);
            Thread.Sleep(1000);

            if (util.CheckElement(driver, Elements.MyAnalog_Dashboard_SeeAll_SeeLess_Button, 2))
            {
                //--- Action: Click the See all Button ---//
                action.IClick(driver, Elements.MyAnalog_Dashboard_SeeAll_SeeLess_Button);
            }

            //--- Locators ---//
            string adiSimRf_link_locator = null;
            string adiSimRf_link_text = null;
            string adiSimRf_part_links_locator = null;

            for (int relatedDesignTools_list_counter = 1; util.CheckElement(driver, By.CssSelector("ul[class^='tools']>li:nth-of-type(" + relatedDesignTools_list_counter + ")"), 2); relatedDesignTools_list_counter++)
            {
                adiSimRf_link_locator = "ul[class^='tools']>li:nth-of-type(" + relatedDesignTools_list_counter + ")>a";
                adiSimRf_link_text = util.GetText(driver, By.CssSelector(adiSimRf_link_locator));
                adiSimRf_part_links_locator = "ul[class^='tools']>li:nth-of-type(" + relatedDesignTools_list_counter + ")>ul>li>a";

                if (adiSimRf_link_text.Equals(AdiSimRf_Label) || adiSimRf_link_text.Equals("ADIsimRF"))
                {
                    //if (util.CheckElement(driver, By.CssSelector(adiSimRf_link_locator), 2))
                    //{
                    //    //----- Dashboard Test Plan > Desktop Tab > R5 > T2.5.1: Verify when clicking ADIsimRF link -----//

                    //    //--- Action: Click ADIsimRFs link ---//
                    //    action.IOpenLinkInNewTab(driver, By.CssSelector(adiSimRf_link_locator));
                    //    Thread.Sleep(1000);

                    //    //--- Expected Result: ADIsimRF slink will displayed ---//
                    //    if (driver.Url.Contains("ADISimRF.aspx"))
                    //    {
                    //        test.validateStringInstance(driver, driver.Url, "/ADISimRF.aspx");
                    //    }
                    //    else
                    //    {
                    //        test.validateStringInstance(driver, driver.Url, adiSimRfSignalChainCalculator_url);
                    //    }

                    //    driver.Close();
                    //    driver.SwitchTo().Window(driver.WindowHandles.Last());
                    //}

                    //----- Dashboard Test Plan > Desktop Tab > R5 > T2.5.2: Verify Product available for ADIsimRF -----//

                    //--- Expected Result: List of product is displayed ---//
                    test.validateElementIsPresent(driver, By.CssSelector(adiSimRf_part_links_locator));

                    if (util.CheckElement(driver, By.CssSelector(adiSimRf_part_links_locator), 5))
                    {
                        //----- Dashboard Test Plan > Desktop Tab > R5 > T2.5.3: Verify redirection when clicking Product under ADIsimRF -----//

                        //--- Action: Click Product link ---//
                        action.IOpenLinkInNewTab(driver, By.CssSelector(adiSimRf_part_links_locator));
                        Thread.Sleep(2000);

                        //--- Expected Result: Page will redirect to Product detail page ---//
                        test.validateStringInstance(driver, driver.Url, pdp_url_format);
                    }

                    break;
                }
            }
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the See all and See less Buttons are Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the See all and See less Buttons are Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the See all and See less Buttons are Working as Expected in JP Locale")]
        public void MyAnalog_Dashboard_RelatedDesignToolsSection_VerifySeeAllAndSeeLessButtons(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Dashboard URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_dashboard_url;

            //--- Action: Go to myAnalog dashboard ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid user account in the myAnalog Login page ---//
            action.ILogin(driver, username, password);
            Thread.Sleep(1000);

            if (util.CheckElement(driver, Elements.MyAnalog_Dashboard_SeeAll_SeeLess_Button, 2))
            {
                //----- Dashboard Test Plan > Desktop Tab > R5 > T3.1: Verify when clicking See all link -----//

                //--- Action: Click See all link ---//
                action.IClick(driver, Elements.MyAnalog_Dashboard_SeeAll_SeeLess_Button);

                //--- Expected Result: List of Related Design Tools expand ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_Dashboard_Expanded_RelatedDesignTools_Section);

                //----- Dashboard Test Plan > Desktop Tab > R5 > T3.2: Verify when clicking See less link -----//

                //--- Action: Click See less link ---//
                action.IClick(driver, Elements.MyAnalog_Dashboard_SeeAll_SeeLess_Button);

                //--- Expected Result: List of Related Design Tools set to hide ---//
                test.validateElementIsNotPresent(driver, Elements.MyAnalog_Dashboard_Expanded_RelatedDesignTools_Section);
            }
        }
    }
}