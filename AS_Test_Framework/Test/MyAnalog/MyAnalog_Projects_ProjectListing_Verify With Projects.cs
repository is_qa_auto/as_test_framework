﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_Projects_ProjectListing_WithProjects : BaseSetUp
    {
        //--- myAnalog Projects URL: ---//
        //--- DEV: https://my-dev.corpnt.analog.com/en/app/projects ---//
        //--- QA: https://my-qa.corpnt.analog.com/en/app/projects ---//
        //--- PROD: https://my.analog.com/en/app/projects ---//

        public MyAnalog_Projects_ProjectListing_WithProjects() : base() { }

        //----- Note: Please don't edit/delete any details on the Accounts below -----//

        //--- Login Credentials ---//
        string username = "my_t3st_pr0j3cts_555@mailinator.com";
        string password = "Test_1234";

        //--- Login Credentials (Account with Projects that has More than 5 Participants) ---//
        string username_projectWithMoreThan5Participants = "my_t3st_pr0j3ct_1nv1t3_555@mailinator.com";
        string password_projectWithMoreThan5Participants = "Test_1234";

        //--- URLs ---//
        string myAnalog_projects_url = "app/projects";
        string myAnalog_projectDetailsPage_url_format = "app/projects/";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Elements are Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Elements are Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Elements are Present and Working as Expected in JP Locale")]
        public void MyAnalog_Projects_ProjectListing_VerifyWithProjects(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Projects URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_projects_url;

            //--- Action: Open myAnalog Projects using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);

            //----- SECTION HEADER -----//

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify Project landing when there is already existing project (IQ-6371/AL-13538) -----//

            //--- Expected Result: Project label is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Projects_Label);

            //----- CREATE A NEW PROJECT -----//

            //--- Expected Result: Create New Project section is available ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Projects_CreateANewProject_Button);

            //----- PROJECT LISTING -----//

            //----- Project & Project Detail Page Test Plan > Test Case Title: Display Projects benefits video when myAnalog user has projects -----//

            //--- Expected Result: Video that shows the benefits of the "Projects" section is not displayed ---//
            test.validateElementIsNotPresent(driver, Elements.MyAnalog_Projects_CreatingANewProject_Video);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify the Project title -----//

            //--- Expected Result: The Project title should be present ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Projects_Project_Name_Link);

            if (util.CheckElement(driver, Elements.MyAnalog_Projects_Project_Name_Link, 1))
            {
                //----- Project & Project Detail Page Test Plan > Test Case Title: Verify if title were changed with the selected title (IQ-6440/AL-13525) -----//

                //--- Action: Click project title ---//
                action.IOpenLinkInNewTab(driver, Elements.MyAnalog_Projects_Project_Name_Link);

                //--- Expected Result: Page is redirected to Project Details page ---//
                test.validateStringInstance(driver, driver.Url, myAnalog_projectDetailsPage_url_format);

                driver.Close();
                driver.SwitchTo().Window(driver.WindowHandles.Last());
            }

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify Created by should display the full name (IQ-7429/ALA-12122, IQ-7727/AL-14998) -----//

            //--- Expected Result: Created by should display ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Projects_CreatedBy_Name_Label);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify te Date and Time is present in Created section (IQ-6373/AL-13512, IQ-9091/AL-15109) -----//

            //--- Expected Result: Date and Time should be present ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Projects_CreatedBy_DateAndTime_Label);

            if (Locale.Equals("en"))
            {
                //----- Project & Project Detail Page Test Plan > Test Case Title: Verify "Modified On" should be changed to "Modified" (IQ-9372/AL-16076) -----//

                //--- Expected Result: Changed to Modified ---//
                test.validateStringInstance(driver, util.GetText(driver, Elements.MyAnalog_Projects_ModifiedBy_Name_Label), "Modified");
            }
            else
            {
                //----- Project & Project Detail Page Test Plan > Test Case Title: Verify Modified by should display the full name (IQ-7429/ALA-12122, IQ-7727/AL-14998) -----//

                //--- Expected Result: Modified by should display ---//
                test.validateElementIsPresent(driver, Elements.MyAnalog_Projects_ModifiedBy_Name_Label);
            }

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify te Date and Time is present in Modified by section -----//

            //--- Expected Result: Date and Time should be present ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Projects_ModifiedBy_DateAndTime_Label);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify all sections as flags on Projects widget -----//

            //--- Expected Result: Contributors is displayed. ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Projects_Contributors);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify Contributers Stats -----//

            //--- Expected Result: The total count of Contributers should be present ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Projects_Contributors_Count_Label);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify all sections as flags on Projects widget -----//

            //--- Expected Result: Products is displayed. ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Projects_Products);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify all sections as flags on Projects widget -----//

            //--- Expected Result: Resources is displayed. ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Projects_Resources);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify the Resources Stats -----//

            //--- Expected Result: The total count of Resources should be present ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Projects_Resources_Count_Label);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify all sections as flags on Projects widget -----//

            //--- Expected Result: PSTs is displayed. ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Projects_Psts);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify all sections as flags on Projects widget -----//

            //--- Expected Result: Notes is displayed. ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Projects_Notes);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify the Notes Stats -----//

            //--- Expected Result: The total count of Notes should be present ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Projects_Notes_Count_Label);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify the Project Detais button -----//

            if (Locale.Equals("en"))
            {
                //--- Expected Result: Button should display as “Project Details” ---//
                test.validateStringIsCorrect(driver, Elements.MyAnalog_Projects_ProjectDetails_Buttons, "Project Details");
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.MyAnalog_Projects_ProjectDetails_Buttons);
            }

            if (util.CheckElement(driver, Elements.MyAnalog_Projects_ProjectDetails_Buttons, 1))
            {
                //--- Action: Click the button ---//
                action.IOpenLinkInNewTab(driver, Elements.MyAnalog_Projects_ProjectDetails_Buttons);

                //--- Expected Result: The page should go to the Project details page ---//
                test.validateStringInstance(driver, driver.Url, myAnalog_projectDetailsPage_url_format);
            }
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Contributor Avatars are Present and Working as Expected for Projects with Less than 5 Participants in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Contributor Avatars are Present and Working as Expected for Projects with Less than 5 Participants in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Contributor Avatars are Present and Working as Expected for Projects with Less than 5 Participants in JP Locale")]
        public void MyAnalog_Projects_ProjectListing_WithProjects_VerifyContributorAvatarsForProjectsWithLessThan5Participants(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Projects URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_projects_url;

            //--- Action: Open myAnalog Projects using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username, password);
            Thread.Sleep(2000);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify avatars when the participant is less than 5 -----//

            //--- Expected Result: All participant avatars should display ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Projects_Contributor_Avatars);

            //--- Expected Result: Ddropdown list should not be present ---//
            test.validateElementIsNotPresent(driver, Elements.MyAnalog_Projects_Participants_Dropdown);
        }

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Contributor Avatars are Present and Working as Expected for Projects with More than 5 Participants in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Contributor Avatars are Present and Working as Expected for Projects with More than 5 Participants in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Contributor Avatars are Present and Working as Expected for Projects with More than 5 Participants in JP Locale")]
        public void MyAnalog_Projects_ProjectListing_WithProjects_VerifyContributorAvatarsForProjectsWithMoreThan5Participants(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Projects URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_projects_url;

            //--- Action: Open myAnalog Projects using direct link ---//
            action.Navigate(driver, myAnalog_url);

            //--- Action: Login valid credentials and click login ---//
            action.ILogin(driver, username_projectWithMoreThan5Participants, password_projectWithMoreThan5Participants);
            Thread.Sleep(2000);

            //----- Project & Project Detail Page Test Plan > Test Case Title: Verify avatars when the participant is more than 5 -----//

            //--- Expected Result: Should display the avatars of the first 5 added to the project ---//
            //--- NOTE: AL-18571 has been logged for the Intermittent Issue where Avatars are Not Displaying in Project Listing when the Project has more than 5 Contributors. ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Projects_Contributor_Avatars);

            //--- Expected Result: Dropdown list should be present ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Projects_Participants_Dropdown);

            //--- Action: Click the dropdown list ---//
            action.IClick(driver, Elements.MyAnalog_Projects_Participants_Dropdown);

            //--- Expected Result: Dropdown list should show all the participants ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_Projects_Participants_Dropdown_Menu);

            //--- Action: Click again to close the dropdown ---//
            action.IClick(driver, Elements.MyAnalog_Projects_Participants_Dropdown);

            //--- Expected Result: Dropdown list should close ---//
            test.validateElementIsNotPresent(driver, Elements.MyAnalog_Projects_Participants_Dropdown_Menu);
        }
    }
}