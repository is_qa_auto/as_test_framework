﻿using NUnit.Framework;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System;
using System.Threading;

namespace AS_Test_Framework.MyAnalog
{
    [TestFixture]
    public class MyAnalog_MegaMenu_CategoriesTile : BaseSetUp
    {
        public MyAnalog_MegaMenu_CategoriesTile() : base() { }

        //-- Login Credentials --//
        string username = "myanalog_t3st_c4t3g0r13s_555@mailinator.com";
        string password = "Test_1234";

        //--- myAnalog Dashboard URL: ---//
        //--- DEV: https://my-dev.corpnt.analog.com/en/app ---//
        //--- QA: https://my-qa.corpnt.analog.com/en/app ---//
        //--- PROD: https://my.analog.com/en/app ---//

        //--- URLs ---//
        string myAnalog_dashboard_url = "app";
        string productCategory_page_url_format = "/products/";

        [Test, Category("MyAnalog"), Category("NonCore"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Categories Tile is Present and Working as Expected in EN Locale")]
        [TestCase("zh", TestName = "Verify that the Categories Tile is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Categories Tile is Present and Working as Expected in JP Locale")]
        public void MyAnalog_MegaMenu_VerifyCategoriesTile(string Locale)
        {
            Console.WriteLine("Steps to Test:");

            //--- myAnalog Dashboard URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + myAnalog_dashboard_url;

            action.Navigate(driver, myAnalog_url);

            //--- Action: Log in valid account in login page ---//
            action.ILogin(driver, username, password);

            //--- Action: click myanalog tab ---//
            action.IExpandMyAnalogMegaMenu(driver);
            Thread.Sleep(1000);

            //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R2 > T2: Verify myPage Components -----//

            //--- Expected Result: The Categories tile is displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_MegaMenu_Categories_Tile);

            //----- CATEGORIES TITLE -----//

            //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R9 > T1: Verify Category component -----//

            //--- Expected Result: The Categories title is displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_MegaMenu_Categories_Link);

            //----- SAVED PRODUCT CATEGORIES -----//

            //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R9 > T1: Verify Category component -----//

            //--- Expected Result: The Categories link are displayed ---//
            test.validateElementIsPresent(driver, Elements.MyAnalog_MegaMenu_Saved_ProductCategory_Links);

            if (util.CheckElement(driver, Elements.MyAnalog_MegaMenu_Saved_ProductCategory_Links, 2))
            {
                string productCategory_link = util.GetText(driver, Elements.MyAnalog_MegaMenu_Saved_ProductCategory_Links);

                //--- Expected Result: 10 most recent items ---//
                test.validateCountIsLessOrEqual(driver, 10, util.GetCount(driver, Elements.MyAnalog_MegaMenu_Saved_ProductCategory_Links));

                //----- My Analog Tab MyPage Test Plan > myAnalog Dashboard Tab > R9 > T2: Verify when clicking Category link -----//

                //--- Action: Click category link ---//
                action.IOpenLinkInNewTab(driver, Elements.MyAnalog_MegaMenu_Saved_ProductCategory_Links);
                Thread.Sleep(1000);

                //--- Expected Result: Link will redirect to category page ---//
                test.validateStringInstance(driver, driver.Url, productCategory_page_url_format);
            }
        }
    }
}