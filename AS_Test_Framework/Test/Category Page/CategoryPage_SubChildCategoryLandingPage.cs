﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Collections.Generic;

namespace AS_Test_Framework.Category
{

    [TestFixture]
    public class CategoryPage_SubChildCategoryLandingPage : BaseSetUp
    {
        public CategoryPage_SubChildCategoryLandingPage() : base() { }

        [Test, Category("Category"), Category("Core")]
        [TestCase("en", TestName = "Verify sub-child category landing page for EN Locale")]
        [TestCase("cn", TestName = "Verify sub-child category landing page for CN Locale")]
        [TestCase("jp", TestName = "Verify sub-child category landing page for JP Locale")]
        [TestCase("ru", TestName = "Verify sub-child category landing page for RU Locale")]
        public void CategoryPage_VerifySubChildLandingPage(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/products/amplifiers/adc-drivers/fully-differential-amplifiers.html");
            test.validateElementIsPresent(driver, By.CssSelector("h1[class='title-margin-32 header1']"));
        }


        //[Test, Category("Category"), Category("Core")]
        //[TestCase("en", TestName = "Verify PDST and Static PST is present in sub-child category page for EN Locale")]
        //[TestCase("cn", TestName = "Verify PDST and Static PST is present in sub-child category page for CN Locale")]
        //[TestCase("jp", TestName = "Verify PDST and Static PST is present in sub-child category page for JP Locale")]
        //[TestCase("ru", TestName = "Verify PDST and Static PST is present in sub-child category page for RU Locale")]
        //public void CategoryPage_VerifyPDST(string Locale)
        //{
        //    action.Navigate(driver, Configuration.Env_Url +  Locale + "/products/amplifiers/adc-drivers/fully-differential-amplifiers.html");
        //    test.validateElementIsPresent(driver, Elements.Category_PDST_PSTIcon);
        //    test.validateElementIsPresent(driver, Elements.Category_StaticPST);
        //    test.validateElementIsPresent(driver, Elements.Category_StaticPST_ResetBtn);
        //    test.validateElementIsPresent(driver, Elements.Category_StaticPST_ViewAllBtn);
        //    //test.validateElementIsPresent(driver, Elements.Category_StaticPST_ViewAllBtn_Footer);
        //    action.IClick(driver, Elements.Category_StaticPST_ViewAllBtn);
        //    test.validateStringInstance(driver, driver.Url, "parametricsearch");
        //    string PSTUrl = driver.Url;
        //    driver.Navigate().Back();
        //    //action.IClick(driver, Elements.Category_StaticPST_ViewAllBtn_Footer);
        //    //test.validateStringInstance(driver, PSTUrl, driver.Url);
        //    //driver.Navigate().Back();
        //    action.IClick(driver, Elements.Category_PDST_PSTIcon);
        //    test.validateStringInstance(driver, PSTUrl, driver.Url);
        //}

        //[Test, Category("Category"), Category("Core")]
        //[TestCase("en", TestName = "Verify that Static PST in sub-child category only shows the first 50 items")]
        //public void CategoryPage_VerifyStaticPST(string Locale)
        //{
        //    action.Navigate(driver, Configuration.Env_Url + Locale + "/products/amplifiers/adc-drivers/fully-differential-amplifiers.html");
        //    int RowCount = util.GetCount(driver, By.CssSelector("div[class='table-responsive'] tbody>tr>td[class='sortOrder']"));
        //    test.validateCountIsEqual(driver, 50, RowCount);
        //}

        //[Test, Category("Category"), Category("Core")]
        //[TestCase("en", TestName = "Verify that Product link in Static PST in sub-child category redirects to correct page (PDP)")]
        //public void CategoryPage_VerifyStaticPST2(string Locale)
        //{
        //    action.Navigate(driver, Configuration.Env_Url + Locale + "/products/amplifiers/adc-drivers/fully-differential-amplifiers.html");
        //    string prod_no  = util.GetText(driver, By.CssSelector("div[class='table-responsive'] tbody>tr>td a"));
        //    action.IClick(driver, By.CssSelector("div[class='table-responsive'] tbody>tr>td a"));
        //    test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/products/" + prod_no.ToLower() + ".html");
        //}
    }
}