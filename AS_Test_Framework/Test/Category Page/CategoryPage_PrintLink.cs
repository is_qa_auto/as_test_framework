﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;

namespace AS_Test_Framework.Category
{

    [TestFixture]
    public class CategoryPage_PrintLink : BaseSetUp
    {
        public CategoryPage_PrintLink() : base() { }

        [Test, Category("Category"), Category("Core")]
        [TestCase("en", TestName = "Verify that print is present for parent, child, and sub-child category pages for EN Locale")]
        [TestCase("cn", TestName = "Verify that print is present for parent, child, and sub-child category pages for CN Locale")]
        [TestCase("jp", TestName = "Verify that print is present for parent, child, and sub-child category pages for JP Locale")]
        [TestCase("ru", TestName = "Verify that print is present for parent, child, and sub-child category pages for RU Locale")]
        public void CategoryPage_VerifyPrint(string Locale)
        {           
            /****Cat*****/
            action.Navigate(driver, Configuration.Env_Url + Locale + "/products/analog-to-digital-converters.html");
            test.validateElementIsPresent(driver, Elements.Print_Widget_Btn);
            /****Sub Cat*****/
            action.IClick(driver, By.CssSelector("div[class='sub-category'] ul>li>a"));
            test.validateElementIsPresent(driver, Elements.Print_Widget_Btn);
            /****Child Cat*****/
            action.IClick(driver, By.CssSelector("div[class='sub-category'] ul>li>a"));
            test.validateElementIsPresent(driver, Elements.Print_Widget_Btn);

        }
    }
}