﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Collections.Generic;

namespace AS_Test_Framework.Category
{

    [TestFixture]
    public class CategoryPage_ChildCategoryLandingPage : BaseSetUp
    {
        public CategoryPage_ChildCategoryLandingPage() : base() { }

        [Test, Category("Category"), Category("Core")]
        [TestCase("en", TestName = "Verify child category landing page for EN Locale")]
        [TestCase("cn", TestName = "Verify child category landing page for CN Locale")]
        [TestCase("jp", TestName = "Verify child category landing page for JP Locale")]
        [TestCase("ru", TestName = "Verify child category landing page for RU Locale")]
        public void CategoryPage_VerifyChildLandingPage(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/products/amplifiers/adc-drivers.html");
            test.validateElementIsPresent(driver, By.CssSelector("h1[class='title-margin-32 header1']"));
            test.validateElementIsPresent(driver, Elements.Category_LandingPage_SubCategory);
            action.IClick(driver, By.CssSelector("div[class='sub-category'] ul>li:nth-child(1)>a"));
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/products/amplifiers/adc-drivers/fully-differential-amplifiers.html");

        }

        //[Test, Category("Category"), Category("Core")]
        //[TestCase("en", TestName = "Verify PDST and Static PST is present in child category page for EN Locale")]
        //[TestCase("cn", TestName = "Verify PDST and Static PST is present in child category page for CN Locale")]
        //[TestCase("jp", TestName = "Verify PDST and Static PST is present in child category page for JP Locale")]
        //[TestCase("ru", TestName = "Verify PDST and Static PST is present in child category page for RU Locale")]
        //public void CategoryPage_VerifyPDST(string Locale)
        //{
        //    action.Navigate(driver, Configuration.Env_Url +  Locale + "/products/amplifiers/adc-drivers.html");
        //    test.validateElementIsPresent(driver, Elements.Category_PDST_PSTIcon);
        //    test.validateElementIsPresent(driver, Elements.Category_StaticPST);
        //    test.validateElementIsPresent(driver, Elements.Category_StaticPST_ResetBtn);
        //    test.validateElementIsPresent(driver, Elements.Category_StaticPST_ViewAllBtn);
        //    test.validateElementIsPresent(driver, Elements.Category_StaticPST_ViewAllBtn_Footer);
        //    action.IClick(driver, Elements.Category_StaticPST_ViewAllBtn);
        //    test.validateStringInstance(driver, driver.Url, "parametricsearch");
        //    string PSTUrl = driver.Url;
        //    driver.Navigate().Back();
        //    action.IClick(driver, Elements.Category_StaticPST_ViewAllBtn_Footer);
        //    test.validateStringInstance(driver, PSTUrl, driver.Url);
        //    driver.Navigate().Back();
        //    action.IClick(driver, Elements.Category_PDST_PSTIcon);
        //    test.validateStringInstance(driver, PSTUrl, driver.Url);
        //}

        //[Test, Category("Category"), Category("Core")]
        //[TestCase("en", TestName = "Verify that Static PST in child category only shows the first 50 items")]
        //public void CategoryPage_VerifyStaticPST(string Locale)
        //{
        //    action.Navigate(driver, Configuration.Env_Url + Locale + "/products/amplifiers/adc-drivers.html");
        //    int RowCount = util.GetCount(driver, By.CssSelector("div[class='table-responsive'] tbody>tr>td[class='sortOrder']"));
        //    test.validateCountIsEqual(driver, 50, RowCount);
        //}

    }
}