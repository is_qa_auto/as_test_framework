﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Collections.Generic;

namespace AS_Test_Framework.Category
{

    [TestFixture]
    public class CategoryPage_LandingPage : BaseSetUp
    {
        public CategoryPage_LandingPage() : base() { }

        [Test, Category("Category"), Category("Core")]
        [TestCase("en", TestName = "Verify product category landing page contents for EN Locale", Category = "Smoke_ADIWeb")]
        [TestCase("cn", TestName = "Verify product category landing page contents for CN Locale", Category = "Smoke_ADIWeb")]
        [TestCase("jp", TestName = "Verify product category landing page contents for JP Locale", Category = "Smoke_ADIWeb")]
        [TestCase("ru", TestName = "Verify product category landing page contents for RU Locale")]
        public void CategoryPage_VerifyLandingPage1(string Locale)
        {
            string initial_steps = "1. Go to " + Configuration.Env_Url + Locale + "/products.html";
            string scenario = "";

            action.INavigate(driver, Configuration.Env_Url +  Locale + "/products.html");
            scenario = "Verify that page title is present";
            test.validateElementIsPresentv2(driver, By.CssSelector("div[class='container page-title '] h1"), scenario, initial_steps);

            scenario = "Verify that list of product category is present";
            test.validateElementIsPresentv2(driver, Elements.Category_LandingPage_SubCategory, scenario, initial_steps);

            scenario = "Verify that Category accordion header is present";
            test.validateElementIsPresentv2(driver, By.CssSelector("div[class*='product-row']:nth-child(3) div[class='section-bar'] div[id='amplifiers']"), scenario, initial_steps);

            scenario = "Verify that list of sub-category is present";
            test.validateElementIsPresentv2(driver, By.CssSelector("div[class*='product-row']:nth-child(3) div[class='sub-category clearfix'] ul"), scenario, initial_steps);

            scenario = "Verify that more <category> button is present";
            test.validateElementIsPresentv2(driver, By.CssSelector("div[class*='product-row']:nth-child(3) a[class='text-link']"), scenario, initial_steps);
        }

        [Test, Category("Category"), Category("Core")]
        [TestCase("en", TestName = "Verify that page will anchor to the selected Category and user can click on its sub-category for EN Locale")]
        [TestCase("cn", TestName = "Verify that page will anchor to the selected Category and user can click on its sub-category for CN Locale")]
        [TestCase("jp", TestName = "Verify that page will anchor to the selected Category and user can click on its sub-category for JP Locale")]
        [TestCase("ru", TestName = "Verify that page will anchor to the selected Category and user can click on its sub-category for RU Locale")]
        public void CategoryPage_VerifyLandingPage2(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/products.html");
            string AnchorUrl = util.ReturnAttribute(driver, By.CssSelector("div[class='sub-category'] ul>li:nth-child(1)>a"), "href");
            action.IClick(driver, By.CssSelector("div[class='sub-category'] ul>li:nth-child(1)>a"));
            test.validateScreenByUrl(driver, AnchorUrl);
            string SubCategory_Name = util.GetText(driver, By.CssSelector("div[class='sub-category clearfix'] ul>li:nth-child(1)>a"));
            action.IClick(driver, By.CssSelector("div[class='sub-category clearfix'] ul>li:nth-child(1)>a"));
            test.validateStringIsCorrect(driver, Elements.Category_Header_Title, SubCategory_Name);
        }

        [Test, Category("Category"), Category("Core")]
        [TestCase("en", TestName = "Verify that More About <category> button will redirect user to Category page for EN Locale")]
        [TestCase("cn", TestName = "Verify that More About <category> button will redirect user to Category page for CN Locale")]
        [TestCase("jp", TestName = "Verify that More About <category> button will redirect user to Category page for JP Locale")]
        [TestCase("ru", TestName = "Verify that More About <category> button will redirect user to Category page for RU Locale")]
        public void CategoryPage_VerifyLandingPage3(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/products.html");
            action.IClick(driver, By.CssSelector("div[class*='product-row']:nth-child(3) a[class='text-link']"));
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/products/amplifiers.html");
        }
    }
}