﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;

namespace AS_Test_Framework.Category
{

    [TestFixture]
    public class CategoryPage_SearchViaGlobalSearch : BaseSetUp
    {
        public CategoryPage_SearchViaGlobalSearch() : base() { }

        [Test, Category("Category"), Category("Core")]
        [TestCase("en", TestName = "Verify that user can search for product category as well as parent, child, and sub-child via global search for EN Locale")]
        //[TestCase("cn", TestName = "Verify that user can search for product category as well as parent, child, and sub-child via global search for CN Locale")]
        //[TestCase("jp", TestName = "Verify that user can search for product category as well as parent, child, and sub-child via global search for JP Locale")]
        //[TestCase("ru", TestName = "Verify that user can search for product category as well as parent, child, and sub-child via global search for RU Locale")]
        public void CategoryPage_VerifyCategoryViaGlobalSearch(string Locale)
        {
            string[] SearchCriteria = {
                                        "A/D Converters (ADC)",
                                        "High Speed A/D Converters >10 MSPS",
                                        "IF/RF Receivers"
                                      }; 

            for(int x=1, y=0; x <= SearchCriteria.Length; x++, y++)
            { 
                action.Navigate(driver, Configuration.Env_Url + Locale + "/index.html");
                action.ISearchViaGlobalSearchTextbox(driver, SearchCriteria[y]);
                action.IClick(driver, Elements.GlobalSearchResults_BaseballCard_Category_Link);
                test.validateStringIsCorrect(driver, Elements.Category_Header_Title, SearchCriteria[y]);
            }
        }
    }
}