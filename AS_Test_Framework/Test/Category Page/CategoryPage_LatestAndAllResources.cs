﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Collections.Generic;

namespace AS_Test_Framework.Category
{

    [TestFixture]
    public class CategoryPage_LatestAndAllResources : BaseSetUp
    {
        public CategoryPage_LatestAndAllResources() : base() { }

        [Test, Category("Category"), Category("Core")]
        [TestCase("en", "Latest Resources" , TestName = "Verify that Latest Resources section is present in Product Category page for EN Locale", Category = "Smoke_ADIWeb")]
        [TestCase("cn", "最新优势资源", TestName = "Verify that Latest Resources section is present in Product Category page for CN Locale", Category = "Smoke_ADIWeb")]
        [TestCase("jp", "最新情報", TestName = "Verify that Latest Resources section is present in Product Category page for JP Locale", Category = "Smoke_ADIWeb")]
        [TestCase("ru", "Актуальные ресурсы по теме", TestName = "Verify that Latest Resources section is present in Product Category page for RU Locale")]
        public void CategoryPage_VerifyLatestResources(string Locale, string Latest_Resources_Title)
        {
            string initial_steps = "1. Go to " + Configuration.Env_Url + Locale + "/products/amplifiers.html";
            string scenario = "";

            /*******Parent Category*******/
            action.INavigate(driver, Configuration.Env_Url +  Locale + "/products/amplifiers.html");
            scenario = "Verify that Latest resources title is present and correct";
            test.validateStringIsCorrectv2(driver, Elements.Applications_LatestResource_Label, Latest_Resources_Title, scenario, initial_steps);

            scenario = "Verify that Latest resources tile is present";
            test.validateElementIsPresentv2(driver, Elements.Category_LatestResources_Tile, scenario, initial_steps);

            scenario = "Verify that only 3 latest resources tiles will be present";
            test.validateCountIsEqualv2(driver, 3, util.GetCount(driver, By.CssSelector("div[class='featured-resource-tile'] ul[class='slides slides-layout-2']>li")), scenario, initial_steps);

            /*******Child Category*******/
            initial_steps = "1. Go to " + Configuration.Env_Url + Locale + "/products/amplifiers/adc-drivers.html";
            action.INavigate(driver, Configuration.Env_Url + Locale + "/products/amplifiers/adc-drivers.html");

            scenario = "Verify that Latest resources title is present and correct";
            test.validateStringIsCorrectv2(driver, Elements.Applications_LatestResource_Label, Latest_Resources_Title, scenario, initial_steps);

            scenario = "Verify that Latest resources tile is present";
            test.validateElementIsPresentv2(driver, Elements.Category_LatestResources_Tile, scenario, initial_steps);

            scenario = "Verify that only 3 latest resources tiles will be present";
            test.validateCountIsEqualv2(driver, 3, util.GetCount(driver, By.CssSelector("div[class='featured-resource-tile'] ul[class='slides slides-layout-2']>li")), scenario, initial_steps);

            /*******Sub-Child Category*******/
            initial_steps = "1. Go to " + Configuration.Env_Url + Locale + "/products/amplifiers/adc-drivers/fully-differential-amplifiers.html";
            action.INavigate(driver, Configuration.Env_Url + Locale + "/products/amplifiers/adc-drivers/fully-differential-amplifiers.html");

            scenario = "Verify that Latest resources title is present and correct";
            test.validateStringIsCorrectv2(driver, Elements.Applications_LatestResource_Label, Latest_Resources_Title, scenario, initial_steps);

            scenario = "Verify that Latest resources tile is present";
            test.validateElementIsPresentv2(driver, Elements.Category_LatestResources_Tile, scenario, initial_steps);

            scenario = "Verify that only 3 latest resources tiles will be present";
            test.validateCountIsEqualv2(driver, 3, util.GetCount(driver, By.CssSelector("div[class='featured-resource-tile'] ul[class='slides slides-layout-2']>li")), scenario, initial_steps);

        }
        [Test, Category("Category"), Category("Core")]
        [TestCase("en", "All Resources", TestName = "Verify that All Resources section is present in Product Category page for EN Locale")]
        [TestCase("cn", "所有资源", TestName = "Verify that All Resources section is present in Product Category page for CN Locale")]
        [TestCase("jp", "関連資料", TestName = "Verify that All Resources section is present in Product Category page for JP Locale")]
        [TestCase("ru", "Все ресурсы", TestName = "Verify that All Resources section is present in Product Category page for RU Locale")]
        public void CategoryPage_VerifyAllResources(string Locale, string All_Resources_Title)
        {
            /*******Parent Category*******/
            action.Navigate(driver, Configuration.Env_Url + Locale + "/products/amplifiers.html");
            test.validateStringIsCorrect(driver, Elements.Applications_AllResources_Label, All_Resources_Title);
          
            /*******Child Category*******/
            action.Navigate(driver, Configuration.Env_Url + Locale + "/products/amplifiers/adc-drivers.html");
            test.validateStringIsCorrect(driver, Elements.Applications_AllResources_Label, All_Resources_Title);
          
            /*******Sub-Child Category*******/
            action.Navigate(driver, Configuration.Env_Url + Locale + "/products/amplifiers/adc-drivers/fully-differential-amplifiers.html");
            test.validateStringIsCorrect(driver, Elements.Applications_AllResources_Label, All_Resources_Title);
        }

    }
}