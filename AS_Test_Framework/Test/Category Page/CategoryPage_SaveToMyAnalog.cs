﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;

namespace AS_Test_Framework.Category
{

    [TestFixture]
    public class CategoryPage_SaveToMyAnalog : BaseSetUp
    {
        public CategoryPage_SaveToMyAnalog() : base() { }

        [Test, Category("Category"), Category("Core")]
        [TestCase("en", TestName = "Verify user can successfully save Product Category only once in myAnalog for EN Locale")]
         public void CategoryPage_VerifySaveToMyAnalog1(string Locale)
        {

            action.IRemoveCategoryInMyAnalog(driver, Locale, "aries.sorosoro@analog.com", "Test_1234", "A/D Converters (ADC)");
            action.Navigate(driver, Configuration.Env_Url + Locale + "/products/analog-to-digital-converters.html");
            test.validateElementIsPresent(driver, Elements.MyAnalog_Widget_Btn);
            action.IClick(driver, Elements.MyAnalog_Widget_Btn);
            //action.IClick(driver, Elements.MyAnalog_Widget_LogInToMyAnalog_Btn);
            action.ILogin(driver, "aries.sorosoro@analog.com", "Test_1234");
          
            /****Validate IQ-IQ-8218****/
            test.validateStringIsCorrect(driver, Elements.MyAnalog_Widget_SaveToProject_Btn, "Save Category");
            test.validateStringIsCorrect(driver, Elements.MyAnalog_Widget_SaveToProject_Label, "* Save Product to:");
            /***************************/

            action.IClick(driver, Elements.MyAnalog_Widget_SaveToProject_Btn);
            test.validateElementIsPresent(driver, Elements.MyAnalog_Widget_SaveToAnalog_Success);

            /****added to validate IQ-7425******/
            test.validateElementIsPresent(driver, Elements.MyAnalog_Widget_ViewSavedLink);


            driver.Navigate().Refresh();
            action.IClick(driver, Elements.MyAnalog_Widget_SaveToProject_Btn);
            test.validateStringIsCorrect(driver, Elements.MyAnalog_Widget_DuplicateCat_Validation_Message, "Category has been already saved");  
        }
    }
}
 
 