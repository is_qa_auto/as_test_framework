﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Collections.Generic;

namespace AS_Test_Framework.Category
{

    [TestFixture]
    public class CategoryPage_ParentCategoryLandingPage : BaseSetUp
    {
        public CategoryPage_ParentCategoryLandingPage() : base() { }

        [Test, Category("Category"), Category("Core")]
        [TestCase("en", TestName = "Verify product category landing page contents for EN Locale")]
        [TestCase("cn", TestName = "Verify product category landing page contents for CN Locale")]
        [TestCase("jp", TestName = "Verify product category landing page contents for JP Locale")]
        [TestCase("ru", TestName = "Verify product category landing page contents for RU Locale")]
        public void CategoryPage_VerifyCategoryLandingPage1(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url +  Locale + "/products/amplifiers.html");
            test.validateElementIsPresent(driver, Elements.Category_Header_Title);
            test.validateElementIsPresent(driver, Elements.Category_LandingPage_SubCategory);
            action.IClick(driver, By.CssSelector("div[class='sub-category'] ul>li:nth-child(1)>a"));
            test.validateScreenByUrl(driver,Configuration.Env_Url + Locale + "/products/amplifiers/adc-drivers.html");
        }

        [Test, Category("Category"), Category("Core")]
        [TestCase("en", "New Products", TestName = "Verify new product listing accordion is present and working in parent category page for EN Locale")]
        [TestCase("cn", "新产品", TestName = "Verify new product listing accordion is present and working in parent category page for CN Locale")]
        [TestCase("jp", "新製品", TestName = "Verify new product listing accordion is present and working in parent category page for JP Locale")]
        [TestCase("ru", "Новые продукты", TestName = "Verify new product listing accordion is present and working in parent category page for RU Locale")]
        public void CategoryPage_VerifyNewProductListingAccordion(string Locale, string Title)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/products/rf-microwave.html");
            test.validateElementIsPresent(driver, Elements.Category_NewProduct_Accordion);
            test.validateStringIsCorrect(driver, Elements.Category_NewProduct_Title, Title);
            test.validateElementIsPresent(driver, Elements.Category_NewProduct_YearDropdown);
            action.IClick(driver, Elements.Category_NewProduct_YearDropdown);
            test.validateElementIsPresent(driver, By.CssSelector("ul[class='dropdown-menu analog-filter']>li>a[data-days='14']"));
            test.validateElementIsPresent(driver, By.CssSelector("ul[class='dropdown-menu analog-filter']>li>a[data-days='30']"));
            test.validateElementIsPresent(driver, By.CssSelector("ul[class='dropdown-menu analog-filter']>li>a[data-days='90']"));
            test.validateElementIsPresent(driver, By.CssSelector("ul[class='dropdown-menu analog-filter']>li>a[data-days='180']"));
            test.validateElementIsPresent(driver, By.CssSelector("ul[class='dropdown-menu analog-filter']>li>a[data-days='365']"));

            for(int x=1; util.CheckElement(driver, By.CssSelector("ul[class='dropdown-menu analog-filter']>li:nth-child(" + x + ")"),5); x++)
            { 
                action.IClick(driver, By.CssSelector("ul[class='dropdown-menu analog-filter']>li:nth-child(" + x + ")>a"));
                if (!util.CheckElement(driver, By.CssSelector("div[class*='NewProductListingSearchResult days']:nth-of-type(" + x + ") p"), 5)) {
                    test.validateElementIsPresent(driver, By.CssSelector("div[class*='NewProductListingSearchResult days']:nth-of-type(" + x + ")"));
                    action.IClick(driver, By.CssSelector("div[class*='NewProductListingSearchResult days']:nth-of-type(" + x + ") h3"));
                    string ProductNo = util.GetText(driver, By.CssSelector("div[class*='NewProductListingSearchResult days']:nth-of-type(" + x + ") div[class='product-content bg-grey'] a"));
                    action.IClick(driver, By.CssSelector("div[class*='NewProductListingSearchResult days']:nth-of-type(" + x + ") div[class='product-content bg-grey'] a"));
                    test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/products/" + ProductNo.ToLower() + ".html");
                    break;
                }
                action.IClick(driver, Elements.Category_NewProduct_YearDropdown);
            }
                  
        }

    }
}