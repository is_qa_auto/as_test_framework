﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Collections.Generic;

namespace AS_Test_Framework.Category
{

    [TestFixture]
    public class CategoryPage_2d_Matrix : BaseSetUp
    {
        public CategoryPage_2d_Matrix() : base() { }

        [Test, Category("Category"), Category("Core")]
        [TestCase("en", "Resolution (Bits)", "ADC Throughput Rate (SPS)", TestName = "Verify that Precision and General Purpose ADC Finder matrix is present for EN Locale")]
        //[TestCase("cn", "产品", TestName = "Verify that Breadcrumbs is present in product category page for CN Locale")]
        //[TestCase("jp", "製品", TestName = "Verify that Breadcrumbs is present in product category page for JP Locale")]
        //[TestCase("ru", "Продукты",  TestName = "Verify that Breadcrumbs is present in product category page for RU Locale")]
        public void CategoryPage_Verify2DMatrix1(string Locale, string FirstColumn, string SecondColumn)
        {
            action.Navigate(driver, Configuration.Env_Url +  Locale + "/products/analog-to-digital-converters.html");
            /******Precision And General Purpose ADC Finder******/
            test.validateStringIsCorrect(driver, By.CssSelector("table[class='table-root']>tbody>tr>td:nth-child(1)>span"), "Precision and General Purpose ADC Finder");
            test.validateStringIsCorrect(driver, By.CssSelector("table[class='table-root']>tbody>tr>td:nth-child(1) * tbody>tr>th:nth-child(1)"), FirstColumn);
            test.validateStringIsCorrect(driver, By.CssSelector("table[class='table-root']>tbody>tr>td:nth-child(1) * tbody>tr>th:nth-child(2)"), SecondColumn);
            test.validateElementIsPresent(driver, By.CssSelector("table[class='table-root']>tbody>tr>td:nth-child(1) table"));
            action.IClick(driver, By.CssSelector("table[class='table-root']>tbody>tr>td:nth-child(1) table a"));
            test.validateStringInstance(driver, driver.Url, "parametricsearch");
            test.validateStringInstance(driver, driver.Title, "Selection Table for Precision A/D");
        }

        [Test, Category("Category"), Category("Core")]
        [TestCase("en", "Resolution (Bits)", "ADC Throughput Rate (SPS)", TestName = "Verify that High-Speed ADC Finder matrix is present for EN Locale")]
        //[TestCase("cn", "产品", TestName = "Verify that Breadcrumbs is present in product category page for CN Locale")]
        //[TestCase("jp", "製品", TestName = "Verify that Breadcrumbs is present in product category page for JP Locale")]
        //[TestCase("ru", "Продукты",  TestName = "Verify that Breadcrumbs is present in product category page for RU Locale")]
        public void CategoryPage_Verify2DMatrix2(string Locale, string FirstColumn, string SecondColumn)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/products/analog-to-digital-converters.html");
            /******Precision And General Purpose ADC Finder******/
            test.validateStringIsCorrect(driver, By.CssSelector("table[class='table-root']>tbody>tr>td:nth-child(2)>span"), "High-Speed ADC Finder");
            test.validateStringIsCorrect(driver, By.CssSelector("table[class='table-root']>tbody>tr>td:nth-child(2) * tbody>tr>th:nth-child(1)"), FirstColumn);
            test.validateStringIsCorrect(driver, By.CssSelector("table[class='table-root']>tbody>tr>td:nth-child(2) * tbody>tr>th:nth-child(2)"), SecondColumn);
            test.validateElementIsPresent(driver, By.CssSelector("table[class='table-root']>tbody>tr>td:nth-child(2) table"));
            action.IClick(driver, By.CssSelector("table[class='table-root']>tbody>tr>td:nth-child(2) table a"));
            test.validateStringInstance(driver, driver.Url, "parametricsearch");
            test.validateStringInstance(driver, driver.Title, "Selection Table for High Speed");
        }
    }
}