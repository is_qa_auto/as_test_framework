﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Collections.Generic;

namespace AS_Test_Framework.Category
{

    [TestFixture]
    public class CategoryPage_BreadCrumbs : BaseSetUp
    {
        public CategoryPage_BreadCrumbs() : base() { }

        [Test, Category("Category"), Category("Core")]
        [TestCase("en", "Products", TestName = "Verify that Breadcrumbs is present in product category page for EN Locale")]
        [TestCase("cn", "产品", TestName = "Verify that Breadcrumbs is present in product category page for CN Locale")]
        [TestCase("jp", "製品", TestName = "Verify that Breadcrumbs is present in product category page for JP Locale")]
        [TestCase("ru", "Продукты",  TestName = "Verify that Breadcrumbs is present in product category page for RU Locale")]
        public void CategoryPage_VerifyBreadCrumbs(string Locale, string FirstLevelCat)
        {
            action.Navigate(driver, Configuration.Env_Url +  Locale + "/products/amplifiers.html");
            test.validateElementIsPresent(driver, Elements.Applications_BreadCrumbs);
            test.validateStringIsCorrect(driver, By.CssSelector("div[name='adi_breadcrumb'] li[class='active']>span"), util.GetText(driver, Elements.Category_Header_Title));
            test.validateStringIsCorrect(driver, By.CssSelector("div[name='adi_breadcrumb'] ol>li:nth-child(2)>span>a"), FirstLevelCat);
            action.IClick(driver, By.CssSelector("div[name='adi_breadcrumb'] ol>li:nth-child(2)>span>a"));
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/products.html");
            driver.Navigate().Back();
            test.validateElementIsPresent(driver, Elements.Applications_BreadCrumbs_Home);
            action.IClick(driver, Elements.Applications_BreadCrumbs_Home);
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/index.html");
        }
    }
}