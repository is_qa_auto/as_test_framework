﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Collections.Generic;
using System.Linq;

namespace AS_Test_Framework.Category
{

    [TestFixture]
    public class CategoryPage_NewProductListingPage : BaseSetUp
    {
        public CategoryPage_NewProductListingPage() : base() { }

        [Test, Category("Category"), Category("Core")]
        [TestCase("en", TestName = "Verify that New Product Listing page component in EN Locale")]
        //[TestCase("cn", TestName = "Verify that New Product Listing page component in CN Locale")]
        //[TestCase("jp", TestName = "Verify that New Product Listing page component in JP Locale")]
        //[TestCase("ru", TestName = "Verify that New Product Listing page component in RU Locale")]
        public void CategoryPage_VerifyNewProductListingPage1(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/products/landing-pages/new-products-listing.html");
            test.validateStringIsCorrect(driver, Elements.Category_NewProductPage_Header, "New Products Listing");
            test.validateElementIsPresent(driver, Elements.Category_NewProductPage_Description);
            test.validateElementIsPresent(driver, Elements.Category_NewProductPage_DaysFilterDropdown);

            if (util.CheckElement(driver, Elements.Category_NewProductPage_NoNewItem, 5))
            {
                test.validateElementIsNotPresent(driver, Elements.Category_NewProductPage_ProductListingFilter);
                test.validateElementIsNotPresent(driver, Elements.Category_NewProductPage_ListView);
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.Category_NewProductPage_ProductListingFilter);
                test.validateElementIsPresent(driver, Elements.Category_NewProductPage_ProductListingFilter_All);
                test.validateElementIsPresent(driver, Elements.Category_NewProductPage_ProductListingFilter_CategoriesAll);
                test.validateElementIsPresent(driver, Elements.Category_NewProductPage_ListView);
            }
            action.IClick(driver, Elements.Category_NewProductPage_DaysFilterDropdown);
            test.validateElementIsPresent(driver, Elements.Category_NewProductPage_DaysFilterMenu);
            test.validateElementIsPresent(driver, By.CssSelector("div[class='dropdown filterBtn open']>ul>li[data-days='14']"));
            test.validateElementIsPresent(driver, By.CssSelector("div[class='dropdown filterBtn open']>ul>li[data-days='30']"));
            test.validateElementIsPresent(driver, By.CssSelector("div[class='dropdown filterBtn open']>ul>li[data-days='90']"));
            test.validateElementIsPresent(driver, By.CssSelector("div[class='dropdown filterBtn open']>ul>li[data-days='180']"));
            test.validateElementIsPresent(driver, By.CssSelector("div[class='dropdown filterBtn open']>ul>li[data-days='365']"));
        }

        [Test, Category("Category"), Category("Core")]
        [TestCase("en", TestName = "Verify that Product link in new product listing will redirect to correct page")]
        //[TestCase("cn", TestName = "Verify that New Product Listing page component in CN Locale")]
        //[TestCase("jp", TestName = "Verify that New Product Listing page component in JP Locale")]
        //[TestCase("ru", TestName = "Verify that New Product Listing page component in RU Locale")]
        public void CategoryPage_VerifyNewProductListingPage2(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/products/landing-pages/new-products-listing.html");

           if (!util.CheckElement(driver, Elements.Category_NewProductPage_NoNewItem, 5))
           {
                string ProdLink = util.GetText(driver, By.CssSelector("section[class='productsListView']>div>div>div[class='productsListingContainer']:nth-child(1)>div[class='list-item'] div[class='product-id']>a"));
                action.IClick(driver, By.CssSelector("section[class='productsListView']>div>div>div[class='productsListingContainer']:nth-child(1)>div[class='list-item'] div[class='product-id']>a"));
                driver.SwitchTo().Window(driver.WindowHandles.Last());
                test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/products/" + ProdLink.ToLower() + ".html");
           }
        }
    }
}