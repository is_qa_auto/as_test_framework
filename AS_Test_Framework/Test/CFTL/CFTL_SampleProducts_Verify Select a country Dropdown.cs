﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;

namespace AS_Test_Framework.CFTL
{
    [TestFixture]
    public class CFTL_SampleProducts_SelectACountryDropdown : BaseSetUp
    {
        public CFTL_SampleProducts_SelectACountryDropdown() : base() { }

        //--- URLs ---//
        string cftl_url = "/CN0350";

        [Test, Category("CFTL"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Select a country Dropdown is Present and Working as Expected when the Same Country is Selected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Select a country Dropdown is Present and Working as Expected when the Same Country is Selected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Select a country Dropdown is Present and Working as Expected when the Same Country is Selected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Select a country Dropdown is Present and Working as Expected when the Same Country is Selected in RU Locale")]
        public void CFTL_SampleProducts_VerifySelectACountryDropdownWhenSameCountryIsSelected(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + cftl_url);

            if (util.CheckElement(driver, Elements.CFTL_SampleProducts_EvaluationBoards_Tbl_SelectACountry_Dd, 2))
            {
                //----- R11 > T4: Verify Check Inventory button in Inventory details view when user selects the same country -----//

                //--- Inputs ---//
                string selectACountry_dd_input = "US";

                //--- Action: Select a country from Country dropdown ---//
                action.IClick(driver, Elements.CFTL_SampleProducts_EvaluationBoards_Tbl_SelectACountry_Dd_Btn);
                action.IClick(driver, By.CssSelector("section[id$='sampleproducts'] * div>div:nth-of-type(1) * div[class$='country-filter'] * [data-id='" + selectACountry_dd_input + "']"));

                if (driver.FindElement(Elements.CFTL_SampleProducts_EvaluationBoards_Tbl_CheckInventory_Btn).Enabled)
                {
                    //--- Action: Click Check Inventory button ---//
                    action.IClick(driver, Elements.CFTL_SampleProducts_EvaluationBoards_Tbl_CheckInventory_Btn);

                    if (util.CheckElement(driver, Elements.CFTL_SampleProducts_EvaluationBoards_ViewInventory_Tbl, 2))
                    {
                        //--- Action: Select the same country from Country dropdown ---//
                        action.IClick(driver, Elements.CFTL_SampleProducts_EvaluationBoards_Tbl_SelectACountry_Dd_Btn);
                        action.IClick(driver, By.CssSelector("section[id$='sampleproducts'] * div>div:nth-of-type(1) * div[class$='country-filter'] * [data-id='" + selectACountry_dd_input + "']"));

                        //--- Expected Result: The page should remain on inventory details table view. ---//
                        test.validateElementIsPresent(driver, Elements.CFTL_SampleProducts_EvaluationBoards_ViewInventory_Tbl);
                    }
                    else
                    {
                        test.validateElementIsPresent(driver, Elements.CFTL_SampleProducts_EvaluationBoards_ViewInventory_Tbl);
                    }
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.CFTL_SampleProducts_EvaluationBoards_Tbl_SelectACountry_Dd);
            }
        }

        [Test, Category("CFTL"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Select a country Dropdown is Present and Working as Expected when a Different Country is Selected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Select a country Dropdown is Present and Working as Expected when a Different Country is Selected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Select a country Dropdown is Present and Working as Expected when a Different Country is Selected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Select a country Dropdown is Present and Working as Expected when a Different Country is Selected in RU Locale")]
        public void CFTL_SampleProducts_VerifySelectACountryDropdownWhenDifferentCountryIsSelected(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + cftl_url);

            if (util.CheckElement(driver, Elements.CFTL_SampleProducts_EvaluationBoards_Tbl_SelectACountry_Dd, 2))
            {
                //----- R11 > T5: Verify Check Inventory button in Inventory details view when user selects different country from the current selected country -----//

                //--- Inputs ---//
                string selectACountry_dd_input1 = "US";

                //--- Action: Select a country from Country dropdown ---//
                action.IClick(driver, Elements.CFTL_SampleProducts_EvaluationBoards_Tbl_SelectACountry_Dd_Btn);
                action.IClick(driver, By.CssSelector("section[id$='sampleproducts'] * div>div:nth-of-type(1) * div[class$='country-filter'] * [data-id='" + selectACountry_dd_input1 + "']"));

                if (driver.FindElement(Elements.CFTL_SampleProducts_EvaluationBoards_Tbl_CheckInventory_Btn).Enabled)
                {
                    //--- Action: Click Check Inventory button ---//
                    action.IClick(driver, Elements.CFTL_SampleProducts_EvaluationBoards_Tbl_CheckInventory_Btn);

                    if (util.CheckElement(driver, Elements.CFTL_SampleProducts_EvaluationBoards_ViewInventory_Tbl, 2))
                    {
                        //--- Inputs ---//
                        string selectACountry_dd_input2 = "MX";

                        //--- Action: Select the different country from Country dropdown ---//
                        action.IClick(driver, Elements.CFTL_SampleProducts_EvaluationBoards_Tbl_SelectACountry_Dd_Btn);
                        action.IClick(driver, By.CssSelector("section[id$='sampleproducts'] * div>div:nth-of-type(1) * div[class$='country-filter'] * [data-id='" + selectACountry_dd_input2 + "']"));

                        //--- Expected Result: The page should switch to initial view ---//
                        test.validateElementIsPresent(driver, Elements.CFTL_SampleProducts_EvaluationBoards_Tbl);

                        //--- Expected Result: Verify Check Inventory button becomes enabled. ---//
                        test.validateElementIsEnabled(driver, Elements.CFTL_SampleProducts_EvaluationBoards_Tbl_CheckInventory_Btn);
                    }
                    else
                    {
                        test.validateElementIsPresent(driver, Elements.CFTL_SampleProducts_EvaluationBoards_ViewInventory_Tbl);
                    }
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.CFTL_SampleProducts_EvaluationBoards_Tbl_SelectACountry_Dd);
            }
        }
    }
}