﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;

namespace AS_Test_Framework.CFTL
{
    [TestFixture]
    public class CFTL_Overview_Tabs : BaseSetUp
    {
        public CFTL_Overview_Tabs() : base() { }

        //--- URLs ---//
        string cftl_url = "/CN0235";
        string cftl_sampleProducts_url = "#rd-sampleproducts";

        [Test, Category("CFTL"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Design Resources is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Design Resources is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Design Resources is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Design Resources is Present and Working as Expected in RU Locale")]
        public void CFTL_Overview_VerifyDesignResources(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + cftl_url);

            //----- R4 > T2.1: Verify the Design Resources tab -----//

            //--- Action: Click The Design Resources tab ---//
            if (!util.CheckElement(driver, By.CssSelector("li[class='active']>a[href$='features-and-benefits']"), 2))
            {
                action.IClick(driver, Elements.CFTL_Overview_DesignResources_Link);
            }

            //--- Expected Result: The Design Resources content should display. ---//
            test.validateElementIsPresent(driver, Elements.CFTL_Overview_DesignResources_Content);

            //--- Expected Result: Download Design Files link should present ---//
            test.validateElementIsPresent(driver, Elements.CFTL_Overview_DownloadDesignFiles_Link);

            //--- Expected Result: Download file size should present. ---//
            test.validateElementIsPresent(driver, Elements.CFTL_Overview_DownloadDesignFiles_Size_Txt);

            //--- Expected Result: Verify the Check Inverntory & purchase link is present ---//
            test.validateElementIsPresent(driver, Elements.CFTL_Overview_CheckInventoryAndPurchase_Link);

            if (util.CheckElement(driver, Elements.CFTL_Overview_CheckInventoryAndPurchase_Link, 2) && !Configuration.Browser.Equals("Chrome_HL"))
            {
                //--- Action: Click the Check Inventory & Purchase Link ---//
                action.IClick(driver, Elements.CFTL_Overview_CheckInventoryAndPurchase_Link);

                //--- Expected Result: Clikcing the link should navigate to Sample Products section. ---//
                test.validateStringInstance(driver, driver.Url, cftl_sampleProducts_url);
            }
        }

        [Test, Category("CFTL"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Features & Benefits is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Features & Benefits is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Features & Benefits is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Features & Benefits is Present and Working as Expected in RU Locale")]
        public void CFTL_Overview_VerifyFeaturesAndBenefits(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + cftl_url);

            //----- R4 > T4.1: Verify Features and Benefitstab -----//

            //--- Action: Click the Features & Benfits tab ---//
            if (!util.CheckElement(driver, By.CssSelector("li[class='active']>a[href$='product-details']"), 2))
            {
                action.IClick(driver, Elements.CFTL_Overview_FeaturesAndBenefits_Link);
            }

            //--- Expected Result: Should display the Features & Benfits content ---//
            test.validateElementIsPresent(driver, Elements.CFTL_Overview_FeaturesAndBenefits_Content);
        }
    }
}