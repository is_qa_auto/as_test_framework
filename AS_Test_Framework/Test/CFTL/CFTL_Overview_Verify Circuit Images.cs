﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;

namespace AS_Test_Framework.CFTL
{
    [TestFixture]
    public class CFTL_Overview_CircuitImages : BaseSetUp
    {
        public CFTL_Overview_CircuitImages() : base() { }

        //--- URLs ---//
        string cftl_url = "/CN0235";

        [Test, Category("CFTL"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Circuit Images are Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Circuit Images are Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Circuit Images are Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Circuit Images are Present and Working as Expected in RU Locale")]
        public void CFTL_Overview_VerifyCircuitImages(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + cftl_url);

            int cftl_circuitImgs_count = 0;

            if (util.CheckElement(driver, Elements.CFTL_Overview_Thumbnail_Images, 2))
            {
                cftl_circuitImgs_count = util.GetCount(driver, Elements.CFTL_Overview_Thumbnail_Images);

                //----- R3 > T10.2: Verify the image thumbnail -----//

                //--- Action: Click each imgae thumbnail ---//
                action.IClick(driver, By.CssSelector("div[id='previewProductsThumnails']>div:nth-last-of-type(1) * img"));

                //--- Expected Result: The selected thumbnail should display the image ---//
                test.validateElementIsPresent(driver, Elements.CFTL_Overview_Image);
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.CFTL_Overview_Thumbnail_Images);
            }


            if (util.CheckElement(driver, Elements.CFTL_Overview_Image, 2))
            {
                //----- R3 > T10.1: Verify Image Overview section -----//

                //--- Action: Click on the image ---//
                action.IClick(driver, Elements.CFTL_Overview_Image);

                //--- Expected Result: The pop-up overlay should open ---//
                test.validateElementIsPresent(driver, Elements.CFTL_Overview_PopUp);

                if (util.CheckElement(driver, Elements.CFTL_Overview_PopUp_X_Icon, 2))
                {
                    if (util.CheckElement(driver, Elements.CFTL_Overview_PopUp_X_Icon, 2))
                    {
                        //--- Action: Click the Close (x) button ---//
                        action.IClick(driver, Elements.CFTL_Overview_PopUp_X_Icon);

                        //--- Expected Result: Clicking the Close (x) button hould close the pop-up ---//
                        test.validateElementIsNotPresent(driver, Elements.CFTL_Overview_PopUp);
                    }

                    //--- Action: Click on magnifying icon ---//
                    action.IClick(driver, Elements.CFTL_Overview_MagnifyingGlass_Icon);

                    //--- Expected Result: The pop-up overlay should open ---//
                    test.validateElementIsPresent(driver, Elements.CFTL_Overview_PopUp);

                    if (util.CheckElement(driver, Elements.CFTL_Overview_PopUp, 2))
                    {
                        //--- Expected Result: Download icon should display ---//
                        test.validateElementIsPresent(driver, Elements.CFTL_Overview_PopUp_Download_Icon);

                        //--- Expected Result: Print icon should display ---//
                        test.validateElementIsPresent(driver, Elements.CFTL_Overview_PopUp_Print_Icon);

                        if (cftl_circuitImgs_count > 1)
                        {
                            //--- Expected Result: Previous arrow should display if the image available is more than one. ---//
                            test.validateElementIsPresent(driver, Elements.CFTL_Overview_PopUp_Prev_Arrow_Icon);

                            //--- Expected Result: Nex arrow should display if the image available is more than one. ---//
                            test.validateElementIsPresent(driver, Elements.CFTL_Overview_PopUp_Next_Arrow_Icon);
                        }
                    }
                }
            }
        }
    }
}