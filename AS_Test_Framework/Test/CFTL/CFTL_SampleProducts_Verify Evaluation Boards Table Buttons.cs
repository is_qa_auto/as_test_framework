﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;

namespace AS_Test_Framework.CFTL
{
    [TestFixture]
    public class CFTL_SampleProducts_EvaluationBoardsTableButtons : BaseSetUp
    {
        public CFTL_SampleProducts_EvaluationBoardsTableButtons() : base() { }

        //--- Login Credentials ---//
        string username = "myanalog_t3st_333@mailinator.com";
        string password = "Test_1234";

        //--- URLs ---//
        string cftl_url = "/CN0350";
        string cftl_url_2 = "/CN0235";

        [Test, Category("CFTL"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Buttons in the Evaluation Boards Table are Present and Working as Expected when the User is Logged Out in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Buttons in the Evaluation Boards Table are Present and Working as Expected when the User is Logged Out in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Buttons in the Evaluation Boards Table are Present and Working as Expected when the User is Logged Out in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Buttons in the Evaluation Boards Table are Present and Working as Expected when the User is Logged Out in RU Locale")]
        public void CFTL_SampleProducts_VerifyEvaluationBoardsTableButtonsWhenUserIsLoggedOut(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + cftl_url);

            if (util.CheckElement(driver, Elements.CFTL_SampleProducts_EvaluationBoards_Tbl, 2))
            {
                //----- R11 > T3: Verify Evaluation Boards Table -----//

                //--- Expected Result: The Back button should be disabled ---//
                test.validateElementIsPresent(driver, Elements.CFTL_SampleProducts_EvaluationBoards_Tbl_Disabled_Back_Btn);

                //--- Expected Result: The Add to Cart button should be disabled ---//
                test.validateElementIsPresent(driver, Elements.CFTL_SampleProducts_EvaluationBoards_Tbl_Disabled_AddToCart_Btn);

                //--- Expected Result: The Check Inventory button should be disabled ---//
                test.validateElementIsPresent(driver, Elements.CFTL_SampleProducts_EvaluationBoards_Tbl_Disabled_CheckInventory_Btn);

                //----- R11 > T4: Verify Check Inventory button in Inventory details view when user selects the same country -----//

                //--- Inputs ---//
                string selectACountry_dd_input = "US";

                //--- Action: Select a country from Country dropdown ---//
                action.IClick(driver, Elements.CFTL_SampleProducts_EvaluationBoards_Tbl_SelectACountry_Dd_Btn);
                action.IClick(driver, By.CssSelector("section[id$='sampleproducts'] * div>div:nth-of-type(1) * div[class$='country-filter'] * [data-id='" + selectACountry_dd_input + "']"));

                //--- Expected Result: Check Inventory button becomes enable. ---//
                test.validateElementIsEnabled(driver, Elements.CFTL_SampleProducts_EvaluationBoards_Tbl_CheckInventory_Btn);

                if (driver.FindElement(Elements.CFTL_SampleProducts_EvaluationBoards_Tbl_CheckInventory_Btn).Enabled)
                {
                    //----- R11 > T3: Verify Evaluation Boards Table -----//

                    //--- Action: Click on the Check Inventory button ---//
                    action.IClick(driver, Elements.CFTL_SampleProducts_EvaluationBoards_Tbl_CheckInventory_Btn);

                    //--- Expected Result: Back button should be enabled ---//
                    test.validateElementIsEnabled(driver, Elements.CFTL_SampleProducts_EvaluationBoards_Tbl_Back_Btn);

                    //----- R11 > T4: Verify Check Inventory button in Inventory details view when user selects the same country -----//

                    //--- Expected Result: Check Inventory button becomes disabled ---//
                    test.validateElementIsPresent(driver, Elements.CFTL_SampleProducts_EvaluationBoards_Tbl_Disabled_CheckInventory_Btn);

                    //--- Expected Result: Verify Check Inventory button remains disabled. ---//
                    test.validateElementIsPresent(driver, Elements.CFTL_SampleProducts_EvaluationBoards_Tbl_Disabled_CheckInventory_Btn);

                    if (driver.FindElement(Elements.CFTL_SampleProducts_EvaluationBoards_Tbl_Back_Btn).Enabled)
                    {
                        //----- R11 > T3: Verify Evaluation Boards Table -----//

                        //--- Action: Click on the Back button ---//
                        action.IClick(driver, Elements.CFTL_SampleProducts_EvaluationBoards_Tbl_Back_Btn);

                        //--- Expected Result: The original table column should slide back ---//
                        test.validateElementIsPresent(driver, Elements.CFTL_SampleProducts_EvaluationBoards_Tbl);
                    }
                }               
            }
        }

        [Test, Category("CFTL"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Buttons in the Evaluation Boards Table are Present and Working as Expected when the User is Logged In in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Buttons in the Evaluation Boards Table are Present and Working as Expected when the User is Logged In in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Buttons in the Evaluation Boards Table are Present and Working as Expected when the User is Logged In in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Buttons in the Evaluation Boards Table are Present and Working as Expected when the User is Logged In in RU Locale")]
        public void CFTL_SampleProducts_VerifyEvaluationBoardsTableButtonsWhenUserIsLoggedIn(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + cftl_url_2);

            if (util.CheckElement(driver, Elements.CFTL_SampleProducts_EvaluationBoards_Tbl, 2))
            {
                if (util.CheckElement(driver, Elements.MyAnalog_Widget_Btn, 2))
                {
                    action.ILogInViaMyAnalogWidget(driver, username, password);

                    //----- R12 > T3: Verify Evaluation Boards Table -----//

                    //--- Inputs ---//
                    string selectACountry_dd_input = "US";

                    action.IClick(driver, Elements.CFTL_SampleProducts_EvaluationBoards_Tbl_SelectACountry_Dd_Btn);
                    action.IClick(driver, By.CssSelector("section[id$='sampleproducts'] * div>div:nth-of-type(1) * div[class$='country-filter'] * [data-id='" + selectACountry_dd_input + "']"));

                    if (driver.FindElement(Elements.CFTL_SampleProducts_EvaluationBoards_Tbl_CheckInventory_Btn).Enabled)
                    {
                        //--- Action: Click on the Check Inventory button ---//
                        action.IClick(driver, Elements.CFTL_SampleProducts_EvaluationBoards_Tbl_CheckInventory_Btn);

                        //--- Expected Result: Add to Cart buttons should be disabled. ---//
                        test.validateElementIsPresent(driver, Elements.CFTL_SampleProducts_EvaluationBoards_Tbl_Disabled_AddToCart_Btn);
                    }
                    else
                    {
                        test.validateElementIsEnabled(driver, Elements.CFTL_SampleProducts_EvaluationBoards_Tbl_CheckInventory_Btn);
                    }
                }
            }
        }
    }
}