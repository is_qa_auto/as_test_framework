﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Threading;

namespace AS_Test_Framework.CFTL
{
    [TestFixture]
    public class CFTL_StickyHeader : BaseSetUp
    {
        public CFTL_StickyHeader() : base() { }

        //--- URLs ---//
        string cftl_url = "/CN0350";
        string cftl_overview_url = "#rd-overview";

        [Test, Category("CFTL"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Sticky Header is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Sticky Header is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Sticky Header is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Sticky Header is Present and Working as Expected in RU Locale")]
        public void CFTL_VerifyStickyHeader(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + cftl_url);
            Thread.Sleep(1000);

            if (util.CheckElement(driver, Elements.CFTL_StickyHeader_Sec, 2))
            {
                //----- R3 > T4: Verify the top button -----//

                //--- Expected Result: The top button should not be present when the page is scrolled up ---//
                test.validateElementIsNotPresent(driver, Elements.CFTL_Top_Btn);

                //----- R3 > T1: Verify the Reference Design Circuit title -----//

                //--- Expected Result: The the Reference Design Number is present ---//
                test.validateElementIsPresent(driver, Elements.CFTL_No_Txt);

                //--- Expected Result: The Description is present ---//
                test.validateElementIsPresent(driver, Elements.CFTL_Desc_Txt);

                //----- R3 > T3: Verify the sticky nav -----//

                //--- Action: Scroll down the page ---//
                action.IMouseOverTo(driver, Elements.Footer);

                //--- Expected Result: The title and navigation menus should dock. ---//
                test.validateElementIsPresent(driver, Elements.CFTL_StickyHeader_Sec);

                if (util.CheckElement(driver, Elements.CFTL_StickyHeader_Sec, 2))
                {
                    //----- R3 > T4: Verify the top button -----//

                    //--- Expected Result: Top button should appear ---//
                    test.validateElementIsPresent(driver, Elements.CFTL_Top_Btn);

                    if (util.CheckElement(driver, Elements.CFTL_Top_Btn, 2))
                    {
                        //--- Action: Click the Top button ---//
                        action.IClick(driver, Elements.EvalBoard_NavigationalMenu_Top_Btn);

                        //--- Expected Result: The page should navigate to the top of the page.. ---//
                        test.validateStringInstance(driver, driver.Url, cftl_overview_url);
                    }
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.CFTL_StickyHeader_Sec);
            }
        }

        [Test, Category("CFTL"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that CFTL sections are present in EN Locale", Category = "Smoke_ADIWeb")]
        public void CFTL_VerifySection(string Locale)
        {
            string initial_steps = "1. Go to " + Configuration.Env_Url + Locale + "/CN0350";
            string scenario = "";
            action.INavigate(driver, Configuration.Env_Url + Locale + "/CN0350");

            scenario = "Verify that Overview Section is present";
            test.validateElementIsPresentv2(driver, Elements.CFTL_Overview_Section_Lbl, scenario, initial_steps);

            scenario = "Verify that Circuit Function and Benefits section is present";
            test.validateElementIsPresentv2(driver, Elements.CFTL_CircuitFunctionAndBenefits_Section_Lbl, scenario, initial_steps);
            
            scenario = "Verify that Circuit Description section is present";
            test.validateElementIsPresentv2(driver, Elements.CFTL_CircuitDescription_Section_Lbl, scenario, initial_steps);
            
            scenario = "Verify that Common Variations section is present";
            test.validateElementIsPresentv2(driver, Elements.CFTL_CommonVariations_Section_Lbl, scenario, initial_steps);

            scenario = "Verify that Circuit Evaluation and Test section is present";
            test.validateElementIsPresentv2(driver, Elements.CFTL_CircuitEvaluationAndTest_Section_Lbl, scenario, initial_steps);

            scenario = "Verify that Discussion section is present";
            test.validateElementIsPresentv2(driver, Elements.CFTL_Discussions_Section_Lbl, scenario, initial_steps);

            scenario = "Verify that sample section is present";
            test.validateElementIsPresentv2(driver, Elements.CFTL_SampleProducts_Section_Lbl, scenario, initial_steps);
            
        }

    }
}