﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;

namespace AS_Test_Framework.CFTL
{
    [TestFixture]
    public class CFTL_Overview_HeaderTitle : BaseSetUp
    {
        public CFTL_Overview_HeaderTitle() : base() { }

        //--- URLs ---//
        string cftl_url = "/CN0235";

        [Test, Category("CFTL"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Overview Header Title is Present in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Overview Header Title is Present in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Overview Header Title is Present in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Overview Header Title is Present in RU Locale")]
        public void CFTL_Overview_VerifyHeaderTitle(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + cftl_url);

            //----- R4 > T1: Verify the Overview title -----//

            //--- Expected Result: The Overview title text should appear ---//
            test.validateElementIsPresent(driver, Elements.CFTL_Overview_Section_Lbl);
        }
    }
}