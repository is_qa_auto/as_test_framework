﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;

namespace AS_Test_Framework.CFTL
{
    [TestFixture]
    public class CFTL_CircuitDescription_Section : BaseSetUp
    {
        public CFTL_CircuitDescription_Section() : base() { }

        //--- URLs ---//
        string cftl_url = "/CN0350";
        string cftl_circuitDescription_url = "#rd-description";

        [Test, Category("CFTL"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Circuit Description Section is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Circuit Description Section is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Circuit Description Section is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Circuit Description Section is Present and Working as Expected in RU Locale")]
        public void CFTL_CircuitDescription_VerifySection(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + cftl_url);

            //----- R7 > T1: Verify the Circuit Description section -----//

            if (util.CheckElement(driver, Elements.CFTL_NavigationalMenu_CircuitDescription, 2))
            {
                //--- Action: Click on the Circuit Description link on the navigation menu ---//
                action.IClick(driver, Elements.CFTL_NavigationalMenu_CircuitDescription);

                //--- Expected Result: The page should navigate to the Circuit Description section ---//
                test.validateStringInstance(driver, driver.Url, cftl_circuitDescription_url);
            }

            //--- Expected Result: Circuit Description title text should display ---//
            test.validateElementIsPresent(driver, Elements.CFTL_CircuitDescription_Section_Lbl);

            //--- Expected Result: The following elements should be displayed for the Circuit Description section: Circuit description, Magnifyih glass icon on the images ---//
            test.validateElementIsPresent(driver, Elements.CFTL_CircuitDescription_Desc_Txt);

            if (util.CheckElement(driver, Elements.CFTL_CircuitDescription_Imgs, 2))
            {
                if (!Locale.Equals("en"))
                {
                    test.validateElementIsPresent(driver, Elements.CFTL_CircuitDescription_MagnifyingGlass_Btn);
                }
                else {
                    test.validateElementIsPresent(driver, By.CssSelector("section[id$='description'] * span[class$='zoom']"));
                }
            }
        }
    }
}