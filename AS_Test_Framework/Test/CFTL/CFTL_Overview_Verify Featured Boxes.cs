﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;

namespace AS_Test_Framework.CFTL
{
    [TestFixture]
    public class CFTL_Overview_FeaturedBoxes : BaseSetUp
    {
        public CFTL_Overview_FeaturedBoxes() : base() { }

        //--- URLs ---//
        string cftl_url_withReferenceDesigns_featuredBox = "/CN0326";
        string cftl_url_withEvaluationHardware_featuredBox = "/CN0271";
        string cftl_sampleProducts_url = "#rd-sampleproducts";

        //--- Labels ---//
        string evaluationHardware_txt = "Evaluation Hardware";

        [Test, Category("CFTL"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Reference Designs Box is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Reference Designs Box is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Reference Designs Box is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Reference Designs Box is Present and Working as Expected in RU Locale")]
        public void CFTL_Overview_VerifyReferenceDesignBox(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + cftl_url_withReferenceDesigns_featuredBox);

            //----- R5 > T5.1/T5.2/T5.3/T5.4: Verify the Reference Designs box -----//

            //--- Expected Result: The icon should be present and displayed properly  ---//
            test.validateElementIsPresent(driver, Elements.CFTL_Overview_ReferenceDesigns_Box_Icon);
        }

        [Test, Category("CFTL"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Evaluation Hardware Featured Box is Present and Working as Expected in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Evaluation Hardware Featured Box is Present and Working as Expected in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Evaluation Hardware Featured Box is Present and Working as Expected in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Evaluation Hardware Featured Box is Present and Working as Expected in RU Locale")]
        public void CFTL_Overview_VerifyEvaluationHardwareFeaturedBox(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + cftl_url_withEvaluationHardware_featuredBox);

            if (util.CheckElement(driver, Elements.CFTL_Overview_EvaluationHardware_FeaturedBox, 2))
            {
                int cftl_featuredBoxes_count = util.GetCount(driver, By.CssSelector("section[id$='overview'] * div[class*='datasheets']>div"));

                for (int cftl_featuredBoxes_ctr = 1; cftl_featuredBoxes_ctr <= cftl_featuredBoxes_count; cftl_featuredBoxes_ctr++)
                {
                    if (util.GetText(driver, By.CssSelector("section[id$='overview'] * div[class*='datasheets']>div:nth-of-type(" + cftl_featuredBoxes_ctr + ") * div[class='title']")).Contains(evaluationHardware_txt))
                    {
                        //----- R3 > T8: Verify Evaluation Hardware -----//

                        //--- Expected Result: Verify the price is dsplayed ---//
                        test.validateElementIsPresent(driver, By.CssSelector("section[id$='overview'] * div[class*='datasheets']>div:nth-of-type(" + cftl_featuredBoxes_ctr + ") * span[class='price']"));

                        //--- Expected Result: Verify the Evaluation Hardware count is displayed ---//
                        test.validateElementIsPresent(driver, By.CssSelector("section[id$='overview'] * div[class*='datasheets']>div:nth-of-type(" + cftl_featuredBoxes_ctr + ") * span[class='count']"));

                        if (util.CheckElement(driver, By.CssSelector("section[id$='overview'] * div[class*='datasheets']>div:nth-of-type(" + cftl_featuredBoxes_ctr + ") * span[class='viewAll']"), 2) && !Configuration.Browser.Equals("Chrome_HL"))
                        {
                            //--- Action: Click on View All link ---//
                            action.IClick(driver, By.CssSelector("section[id$='overview'] * div[class*='datasheets']>div:nth-of-type(" + cftl_featuredBoxes_ctr + ") * span[class='viewAll']"));

                            //--- Expected Result: The page should avigate to Sample Products section ---//
                            test.validateStringInstance(driver, driver.Url, cftl_sampleProducts_url);
                        }
                        else
                        {
                            test.validateElementIsPresent(driver, By.CssSelector("section[id$='overview'] * div[class*='datasheets']>div:nth-of-type(" + cftl_featuredBoxes_ctr + ") * span[class='viewAll']"));
                        }

                        break;
                    }
                }
            }
        }
    }
}