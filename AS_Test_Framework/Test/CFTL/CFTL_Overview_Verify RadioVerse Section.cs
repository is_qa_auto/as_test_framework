﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Linq;
using System.Threading;
using System.Text.RegularExpressions;
using System;

namespace AS_Test_Framework.CFTL
{
    [TestFixture]
    public class CFTL_Overview_RadioVerseSection : BaseSetUp
    {
        public CFTL_Overview_RadioVerseSection() : base() { }

        //--- URLs ---//
        string cftl_withMarketsAndTechnologySection_url = "/CN0235";
        string cftl_withPartsUsedSection_url = "/CN0385";
        string applications_page_url_format = "/applications/";
        string pdp_url_format = "/products/";

        //--- Labels ---//
        string marketsAndTechnology_txt = "Markets and Technologies";
        string partsUsed_txt = "Parts Used";

        [Test, Category("CFTL"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Markets and Technology Section is Present and Working as Expected in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Markets & Technology Section is Present and Working as Expected in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Markets & Technology Section is Present and Working as Expected in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Markets & Technology Section is Present and Working as Expected in RU Locale")]
        public void CFTL_Overview_VerifyMarketsAndTechnologySection(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + cftl_withMarketsAndTechnologySection_url);

            int sectionHeader_count = util.GetCount(driver, By.CssSelector("div[class^='radioverse']>h2"));

            for (int sectionHeader_ctr = 1; sectionHeader_ctr <= sectionHeader_count; sectionHeader_ctr++)
            {
                if (util.GetText(driver, By.CssSelector("div[class^='radioverse']>h2:nth-of-type(" + sectionHeader_ctr + ")")).Equals(marketsAndTechnology_txt))
                {
                    //----- R4 > T5: Verify the Markets & technology section -----//

                    /*****this is for AL-16901*****/
                    test.validateElementIsPresent(driver, By.CssSelector("div[class^='radioverse'] ul>li[class='accordionGroup']"));
                    test.validateElementIsPresent(driver, By.CssSelector("div[class^='radioverse'] ul>li[class='accordionGroup']:nth-child(1)>div[class='accordionGroup__toggle collapsed']"));
                    string SubItemCount = Regex.Match(util.GetText(driver, By.CssSelector("div[class^='radioverse'] ul>li[class='accordionGroup']>div>a>span")), @"\d+").Value;
                    /***expand the accordion***/
                    action.IClick(driver, By.CssSelector("div[class^='radioverse'] ul>li[class='accordionGroup']:nth-child(1)>div[class='accordionGroup__toggle collapsed'] span[class='accordionGroup__toggle__icon']"));
                    test.validateElementIsPresent(driver, By.CssSelector("div[class^='radioverse'] ul>li[class='accordionGroup']:nth-child(1)>div[class='accordionGroup__toggle']"));
                    test.validateElementIsPresent(driver, By.CssSelector("div[class^='radioverse'] ul>li[class='accordionGroup']:nth-child(1) div[class='accordionGroup__body__inner']"));
                    test.validateCountIsEqual(driver, Int32.Parse(SubItemCount), util.GetCount(driver, By.CssSelector("div[class^='radioverse'] ul>li[class='accordionGroup']:nth-child(1) div[class='accordionGroup__body__inner']>ul>li")));

                    if (util.CheckElement(driver, Elements.CFTL_Overview_MarketsAndTechnology_Links, 2))
                    {
                        //--- Action: Clicking on a Markets link ---//
                        action.IOpenLinkInNewTab(driver, Elements.CFTL_Overview_MarketsAndTechnology_Links);
                        Thread.Sleep(2000);

                        //--- Expected Result: Clicking the Markets link should go correspnding Application pages ---//
                        test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + applications_page_url_format);
                    }
                    else
                    {
                        test.validateElementIsPresent(driver, Elements.CFTL_Overview_MarketsAndTechnology_Links);
                    }
                    driver.SwitchTo().Window(driver.WindowHandles.First());
                    action.IOpenLinkInNewTab(driver, Elements.PDP_MarketsAndTechnology_Link);
                    Thread.Sleep(2000);

                    //--- Expected Result: Clicking the Markets link should go correspnding Application pages ---//
                    test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + applications_page_url_format);

                    break;
                }
            }
        }

        [Test, Category("CFTL"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Parts Used Section is Present and Working as Expected in EN Locale")]
        //[TestCase("cn", TestName = "Verify that the Parts Used Section is Present and Working as Expected in CN Locale")]
        //[TestCase("jp", TestName = "Verify that the Parts Used Section is Present and Working as Expected in JP Locale")]
        //[TestCase("ru", TestName = "Verify that the Parts Used Section is Present and Working as Expected in RU Locale")]
        public void CFTL_Overview_VerifyPartsUsedSection(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + cftl_withPartsUsedSection_url);

            int sectionHeader_count = util.GetCount(driver, By.CssSelector("div[class^='radioverse']>h2"));

            for (int sectionHeader_ctr = 1; sectionHeader_ctr <= sectionHeader_count; sectionHeader_ctr++)
            {
                if (util.GetText(driver, By.CssSelector("div[class^='radioverse']>h2:nth-of-type(" + sectionHeader_ctr + ")")).Equals(partsUsed_txt))
                {
                    //----- R4 > T6: Verify the Parts used section -----//

                    if (util.CheckElement(driver, Elements.CFTL_Overview_All_PartsUsed_Links, 2))
                    {
                        //--- Action: Click the parts link ---//
                        action.IOpenLinkInNewTab(driver, Elements.CFTL_Overview_All_PartsUsed_Links);
                        Thread.Sleep(2000);

                        //--- Expected Result: Should go to corresponding product pages ---//
                        test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + pdp_url_format);

                        driver.Close();
                        driver.SwitchTo().Window(driver.WindowHandles.First());

                        int partsUsed_count = util.GetCount(driver, Elements.CFTL_Overview_All_PartsUsed_Links);

                        if (partsUsed_count <= 10)
                        {
                            //--- Expected Result: View all and view less link should not displayed ---//
                            test.validateElementIsNotPresent(driver, Elements.CFTL_Overview_PartsUsed_ViewAll_ViewLess_Link);
                        }
                        else
                        {
                            if (util.CheckElement(driver, Elements.CFTL_Overview_PartsUsed_ViewAll_ViewLess_Link, 2))
                            {
                                //--- Action: Click View All link ---//
                                action.IClick(driver, Elements.CFTL_Overview_PartsUsed_ViewAll_ViewLess_Link);

                                //--- Expected Result: The remaining applicable should expand. ---//
                                test.validateCountIsGreaterOrEqual(driver, util.GetCount(driver, Elements.CFTL_Overview_Displayed_PartsUsed_Links), 11);

                                //--- Action: Click View less link ---//
                                action.IClick(driver, Elements.CFTL_Overview_PartsUsed_ViewAll_ViewLess_Link);

                                //--- Expected Result: The content should collapsed back to 10 applicable products ---//
                                test.validateCountIsEqual(driver, 10, util.GetCount(driver, Elements.CFTL_Overview_Displayed_PartsUsed_Links));

                                //--- Expected Result: Verify View less link is displayed under the products in applicable parts ---//
                                test.validateElementIsPresent(driver, Elements.CFTL_Overview_PartsUsed_ViewAll_ViewLess_Link);
                            }
                            else
                            {
                                test.validateElementIsPresent(driver, Elements.CFTL_Overview_PartsUsed_ViewAll_ViewLess_Link);
                            }
                        }
                    }
                    else
                    {
                        test.validateElementIsPresent(driver, Elements.CFTL_Overview_All_PartsUsed_Links);
                    }

                    break;
                }
            }
        }
    }
}