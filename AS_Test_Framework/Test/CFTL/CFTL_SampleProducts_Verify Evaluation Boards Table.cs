﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;

namespace AS_Test_Framework.CFTL
{
    [TestFixture]
    public class CFTL_SampleProducts_EvaluationBoardsTable : BaseSetUp
    {
        public CFTL_SampleProducts_EvaluationBoardsTable() : base() { }

        //--- URLs ---//
        string cftl_url = "/CN0307";

        [Test, Category("CFTL"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Evaluation Boards Table is Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Evaluation Boards Table is Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Evaluation Boards Table is Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Evaluation Boards Table is Working as Expected in RU Locale")]
        public void CFTL_SampleProducts_VerifyEvaluationBoardsTableWhenUserIsLoggedOut(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + cftl_url);

            if (!util.CheckElement(driver, Elements.CFTL_SampleProducts_EvaluationBoards_Tbl, 2))
            {
                //----- R11 > T3: Verify Evaluation Boards Table -----//

                //--- Expected Result: If there's no Evaluation Boards Table, the Evaluation Boards Label should not be dispalyed ---//
                test.validateElementIsNotPresent(driver, Elements.CFTL_SampleProducts_EvaluationBoards_Tbl_Header_Txt);

                //--- Expected Result: If there's no Evaluation Boards Table, the Description should not be dispalyed ---//
                test.validateElementIsNotPresent(driver, Elements.CFTL_SampleProducts_EvaluationBoards_Tbl_Desc_Txt);

                //--- Expected Result: If there's no Evaluation Boards Table, the Back Button should not be dispalyed ---//
                test.validateElementIsNotPresent(driver, Elements.CFTL_SampleProducts_EvaluationBoards_Tbl_Back_Btn);

                //--- Expected Result: If there's no Evaluation Boards Table, the Add to card Button should not be dispalyed ---//
                test.validateElementIsNotPresent(driver, Elements.CFTL_SampleProducts_EvaluationBoards_Tbl_AddToCart_Btn);

                //--- Expected Result: If there's no Evaluation Boards Table, the Select a Country Dropdown should not be dispalyed ---//
                test.validateElementIsNotPresent(driver, Elements.CFTL_SampleProducts_EvaluationBoards_Tbl_SelectACountry_Dd);

                //--- Expected Result: If there's no Evaluation Boards Table, the Check Inventory Button should not be dispalyed ---//
                test.validateElementIsNotPresent(driver, Elements.CFTL_SampleProducts_EvaluationBoards_Tbl_CheckInventory_Btn);
            }
        }
    }
}