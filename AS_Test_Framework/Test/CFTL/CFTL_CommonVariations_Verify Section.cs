﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;

namespace AS_Test_Framework.CFTL
{
    [TestFixture]
    public class CFTL_CommonVariations_Section : BaseSetUp
    {
        public CFTL_CommonVariations_Section() : base() { }

        //--- URLs ---//
        string cftl_url = "/CN0350";
        string cftl_commonVariations_url = "#rd-commonvariations";

        [Test, Category("CFTL"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Common Variations Section is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Common Variations Section is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Common Variations Section is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Common Variations Section is Present and Working as Expected in RU Locale")]
        public void CFTL_CommonVariations_VerifySection(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + cftl_url);

            //----- R8 > T1: Verify the Common Variations section -----//

            if (util.CheckElement(driver, Elements.CFTL_NavigationalMenu_CommonVariations, 2))
            {
                //--- Action: Click on the Common Variations link on navigation menu ---//
                action.IClick(driver, Elements.CFTL_NavigationalMenu_CommonVariations);

                //--- Expected Result: The page should navigate to Common Variations section ---//
                test.validateStringInstance(driver, driver.Url, cftl_commonVariations_url);
            }

            //--- Expected Result: Common Variations title text should display ---//
            test.validateElementIsPresent(driver, Elements.CFTL_CommonVariations_Section_Lbl);          
        }
    }
}