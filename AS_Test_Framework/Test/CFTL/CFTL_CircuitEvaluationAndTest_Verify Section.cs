﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;

namespace AS_Test_Framework.CFTL
{
    [TestFixture]
    public class CFTL_CircuitEvaluationAndTest_Section : BaseSetUp
    {
        public CFTL_CircuitEvaluationAndTest_Section() : base() { }

        //--- URLs ---//
        string cftl_url = "/CN0287";
        string cftl_circuitEvaluationAndTest_url = "#rd-evaluationNtest";

        [Test, Category("CFTL"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Circuit Evaluation and Test Section is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Circuit Evaluation and Test Section is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Circuit Evaluation and Test Section is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Circuit Evaluation and Test Section is Present and Working as Expected in RU Locale")]
        public void CFTL_CircuitEvaluationAndTest_VerifySection(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + cftl_url);

            //----- R9 > T1: Verify the Circuit Evaluation and Test  section -----//

            if (util.CheckElement(driver, Elements.CFTL_NavigationalMenu_CircuitEvaluationAndTest, 2))
            {
                //--- Action: Click on the Circuit Evaluation and Test  on navigation menu ---//
                action.IClick(driver, Elements.CFTL_NavigationalMenu_CircuitEvaluationAndTest);

                //--- Expected Result: The page should navigate to Common Variations section ---//
                test.validateStringInstance(driver, driver.Url, cftl_circuitEvaluationAndTest_url);
            }

            //--- Expected Result: Circuit Evaluation and Test   title text should display ---//
            test.validateElementIsPresent(driver, Elements.CFTL_CircuitEvaluationAndTest_Section_Lbl);          
        }
    }
}