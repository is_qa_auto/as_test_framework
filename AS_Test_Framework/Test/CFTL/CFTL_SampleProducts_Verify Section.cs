﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;

namespace AS_Test_Framework.CFTL
{
    [TestFixture]
    public class CFTL_SampleProducts_Section : BaseSetUp
    {
        public CFTL_SampleProducts_Section() : base() { }

        //--- URLs ---//
        string cftl_url = "/CN0350";
        string cftl_sampleProducts_url = "#rd-sampleproducts";

        [Test, Category("CFTL"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Sample Products Section is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Sample Products Section is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Sample Products Section is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Sample Products Section is Present and Working as Expected in RU Locale")]
        public void CFTL_SampleProducts_VerifySection(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + cftl_url);

            //----- R11 > T1: Verify the Sample Products section -----//

            if (util.CheckElement(driver, Elements.CFTL_NavigationalMenu_SampleProducts, 2) && !Configuration.Browser.Equals("Chrome_HL"))
            {
                //--- Action: Click on the Sample Products link on the navigation menu ---//
                action.IClick(driver, Elements.CFTL_NavigationalMenu_SampleProducts);

                //--- Expected Result: It should navigate to  Sample Products section ---//
                test.validateStringInstance(driver, driver.Url, cftl_sampleProducts_url);
            }

            //--- Expected Result: Sample Products title text should be present ---//
            test.validateElementIsPresent(driver, Elements.CFTL_SampleProducts_Section_Lbl);
        }
    }
}