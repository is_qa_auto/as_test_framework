﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;

namespace AS_Test_Framework.CFTL
{
    [TestFixture]
    public class CFTL_Discussions_Section : BaseSetUp
    {
        public CFTL_Discussions_Section() : base() { }

        //--- URLs ---//
        string cftl_url = "/CN0350";
        string cftl_discussions_url = "#rd-discussions";

        [Test, Category("CFTL"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Discussions Section is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Discussions Section is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Discussions Section is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Discussions Section is Present and Working as Expected in RU Locale")]
        public void CFTL_Discussions_VerifySection(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + cftl_url);

            //----- R10 > T1: Verify the Discussions section -----//

            if (util.CheckElement(driver, Elements.CFTL_NavigationalMenu_Discussions, 2) && !Configuration.Browser.Equals("Chrome_HL"))
            {
                //--- Action: Click on the Discussion link on navigation menu ---//
                action.IClick(driver, Elements.CFTL_NavigationalMenu_Discussions);

                //--- Expected Result: The Discussions section should be displayed.  ---//
                test.validateStringInstance(driver, driver.Url, cftl_discussions_url);
            }

            //--- Expected Result: The Discussion ttitle text should display ---//
            test.validateElementIsPresent(driver, Elements.CFTL_Discussions_Section_Lbl);
        }
    }
}