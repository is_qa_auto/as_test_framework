﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;

namespace AS_Test_Framework.CFTL
{
    [TestFixture]
    public class CFTL_CircuitFunctionAndBenefits_Section : BaseSetUp
    {
        public CFTL_CircuitFunctionAndBenefits_Section() : base() { }

        //--- URLs ---//
        string cftl_url = "/CN0350";
        string cftl_circuitFunctionAndBenefits_url = "#rd-functionbenefits";

        [Test, Category("CFTL"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Circuit Function and Benefits Section is Present and Working as Expected in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Circuit Function and Benefits Section is Present and Working as Expected in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Circuit Function and Benefits Section is Present and Working as Expected in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Circuit Function and Benefits Section is Present and Working as Expected in RU Locale")]
        public void CFTL_CircuitFunctionAndBenefits_VerifySection(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + cftl_url);

            //----- R6 > T1: Verify the Circuit Function & Benefits section -----//

            if (util.CheckElement(driver, Elements.CFTL_NavigationalMenu_CircuitFunctionAndBenefits, 2))
            {
                //--- Action: Click on the Circuit Function & Benefits link in navigation menu ---//
                action.IClick(driver, Elements.CFTL_NavigationalMenu_CircuitFunctionAndBenefits);

                //--- Expected Result: The page should navigate to the Circuit Function & Benefits section ---//
                test.validateStringInstance(driver, driver.Url, cftl_circuitFunctionAndBenefits_url);
            }

            //--- Expected Result: The Circuit Function & Benefits title should display ---//
            test.validateElementIsPresent(driver, Elements.CFTL_CircuitFunctionAndBenefits_Section_Lbl);

            //--- Expected Result: The following elements should be displayed for the Circuit Function & Benefits section: description, diagram, Figure Reference, Zoom Out (magnifying lens icon) ---//
            test.validateElementIsPresent(driver, Elements.CFTL_CircuitFunctionAndBenefits_Desc_Txt);
            test.validateElementIsPresent(driver, Elements.CFTL_CircuitFunctionAndBenefits_Diagram_Img);

            if (util.CheckElement(driver, By.CssSelector("section[id$='functionbenefits'] * p>span[class^='analog-text']"), 2))
            {
                test.validateElementIsPresent(driver, By.CssSelector("section[id$='functionbenefits'] * p>span[class^='analog-text']"));
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.CFTL_CircuitFunctionAndBenefits_FigureReference_Txt);
            }
            if (Locale.Equals("cn") || Locale.Equals("jp"))
            {
                test.validateElementIsPresent(driver, Elements.CFTL_CircuitFunctionAndBenefits_MagnifyingGlass_Btn);

                if (util.CheckElement(driver, Elements.CFTL_CircuitFunctionAndBenefits_MagnifyingGlass_Btn, 2))
                {
                    //--- Action: Click on the magnifying lens icon ---//
                    action.IClick(driver, Elements.CFTL_CircuitFunctionAndBenefits_MagnifyingGlass_Btn);

                    //--- Expected Result: The enlarged image should be displayed on the modal box ---//
                    test.validateElementIsPresent(driver, Elements.CFTL_CircuitFunctionAndBenefits_Image_Modal);

                    if (util.CheckElement(driver, Elements.CFTL_CircuitFunctionAndBenefits_Image_Modal, 2))
                    {
                        //--- Expected Result: The following details should be displayed on the modal screen: Zoomed out Image, Title of the Image, Print link to print the given image ---//
                        test.validateElementIsPresent(driver, Elements.CFTL_CircuitFunctionAndBenefits_Image_Modal_Img);
                        test.validateElementIsPresent(driver, Elements.CFTL_CircuitFunctionAndBenefits_Image_Modal_Title);
                        test.validateElementIsPresent(driver, Elements.CFTL_CircuitFunctionAndBenefits_Image_Modal_Print_Icon);

                        if (util.CheckElement(driver, Elements.CFTL_CircuitFunctionAndBenefits_Image_Modal_X_Btn, 2))
                        {
                            //--- Action: Click on the Close (X) link on the modal ---//
                            action.IClick(driver, Elements.CFTL_CircuitFunctionAndBenefits_Image_Modal_X_Btn);

                            //--- Expected Result: The modal box should closed ---//
                            test.validateElementIsNotPresent(driver, Elements.CFTL_CircuitFunctionAndBenefits_Image_Modal);
                        }
                        else
                        {
                            test.validateElementIsPresent(driver, Elements.CFTL_CircuitFunctionAndBenefits_Image_Modal_X_Btn);
                        }
                    }
                }
            }
            else {
                test.validateElementIsPresent(driver, By.CssSelector("section[id$='functionbenefits'] * span[class$='zoom']"));

                if (util.CheckElement(driver, By.CssSelector("section[id$='functionbenefits'] * span[class$='zoom']"), 2))
                {
                    //--- Action: Click on the magnifying lens icon ---//
                    action.IClick(driver, By.CssSelector("section[id$='functionbenefits'] * span[class$='zoom']"));

                    //--- Expected Result: The enlarged image should be displayed on the modal box ---//
                    test.validateElementIsPresent(driver, Elements.CFTL_CircuitFunctionAndBenefits_Image_Modal);

                    if (util.CheckElement(driver, Elements.CFTL_CircuitFunctionAndBenefits_Image_Modal, 2))
                    {
                        //--- Expected Result: The following details should be displayed on the modal screen: Zoomed out Image, Title of the Image, Print link to print the given image ---//
                        test.validateElementIsPresent(driver, Elements.CFTL_CircuitFunctionAndBenefits_Image_Modal_Img);
                        test.validateElementIsPresent(driver, Elements.CFTL_CircuitFunctionAndBenefits_Image_Modal_Title);
                        test.validateElementIsPresent(driver, Elements.CFTL_CircuitFunctionAndBenefits_Image_Modal_Print_Icon);

                        if (util.CheckElement(driver, Elements.CFTL_CircuitFunctionAndBenefits_Image_Modal_X_Btn, 2))
                        {
                            //--- Action: Click on the Close (X) link on the modal ---//
                            action.IClick(driver, Elements.CFTL_CircuitFunctionAndBenefits_Image_Modal_X_Btn);

                            //--- Expected Result: The modal box should closed ---//
                            test.validateElementIsNotPresent(driver, Elements.CFTL_CircuitFunctionAndBenefits_Image_Modal);
                        }
                        else
                        {
                            test.validateElementIsPresent(driver, Elements.CFTL_CircuitFunctionAndBenefits_Image_Modal_X_Btn);
                        }
                    }
                }

            }
        }
    }
}