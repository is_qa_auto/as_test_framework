﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Linq;

namespace AS_Test_Framework.CFTL
{
    [TestFixture]
    public class CFTL_SampleProducts_EvaluationBoardsViewInventoryTable : BaseSetUp
    {
        public CFTL_SampleProducts_EvaluationBoardsViewInventoryTable() : base() { }

        //--- Login Credentials ---//
        string username = "myanalog_t3st_111@mailinator.com";
        string password = "Test_1234";

        //--- URLs ---//
        string cftl_url = "/CN0350";
        string cftl_url_2 = "/CN0271";
        string shoppingCart_page_url = "shoppingcart";

        //--- Labels ---//
        string country_selected = "United States";

        [Test, Category("CFTL"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Evaluation Boards - View Inventory Table is Present and Working as Expected when the User is Logged Out in EN Locale", Category = "Smoke_ADIWeb")]
        [TestCase("cn", TestName = "Verify that the Evaluation Boards - View Inventory Table is Present and Working as Expected when the User is Logged Out in CN Locale", Category = "Smoke_ADIWeb")]
        [TestCase("jp", TestName = "Verify that the Evaluation Boards - View Inventory Table is Present and Working as Expected when the User is Logged Out in JP Locale", Category = "Smoke_ADIWeb")]
        [TestCase("ru", TestName = "Verify that the Evaluation Boards - View Inventory Table is Present and Working as Expected when the User is Logged Out in RU Locale")]
        public void CFTL_SampleProducts_VerifyEvaluationBoardsViewInventoryTableWhenUserIsLoggedOut(string Locale)
        {
            string initial_steps = "1. Go to " + Configuration.Env_Url + Locale + cftl_url;
            string scenario = "";

            action.INavigate(driver, Configuration.Env_Url + Locale + cftl_url);

            if (util.CheckElement(driver, Elements.CFTL_SampleProducts_EvaluationBoards_Tbl_SelectACountry_Dd, 2))
            {
                //----- R11 > T3: Verify Evaluation Boards Table -----//

                //--- Inputs ---//
                string selectACountry_dd_input = "US";

                //--- Action: Select a country on the dropdown ---//
                initial_steps = initial_steps + "<br>2. Click country dropdown in evaluation board table of sample products section";
                action.IClick(driver, Elements.CFTL_SampleProducts_EvaluationBoards_Tbl_SelectACountry_Dd_Btn);

                initial_steps = initial_steps + "<br>3. Select United States as country";
                action.IClick(driver, By.CssSelector("section[id$='sampleproducts'] * div>div:nth-of-type(1) * div[class$='country-filter'] * [data-id='" + selectACountry_dd_input + "']"));

                if (driver.FindElement(Elements.CFTL_SampleProducts_EvaluationBoards_Tbl_CheckInventory_Btn).Enabled)
                {
                    //--- Action: Click on the Check Inventory button ---//
                    initial_steps = initial_steps + "<br>4. Click check inventory button";
                    action.IClick(driver, Elements.CFTL_SampleProducts_EvaluationBoards_Tbl_CheckInventory_Btn);

                    //--- Expected Result: Distributor columns should slide in ---//
                    scenario = "Verify that eval board distributor table will slide-in";
                    test.validateElementIsPresentv2(driver, Elements.CFTL_SampleProducts_EvaluationBoards_ViewInventory_Tbl, scenario, initial_steps);

                    if (util.CheckElement(driver, Elements.CFTL_SampleProducts_EvaluationBoards_ViewInventory_Tbl_Select_Cb, 2))
                    {
                        int evaluationBoards_viewInventory_tbl_row_count = util.GetCount(driver, By.CssSelector("section[id$='sampleproducts'] * div[class$='grid-clone view-inventory']>table>tbody>tr"));

                        for (int locator_ctr = 1; locator_ctr <= evaluationBoards_viewInventory_tbl_row_count; locator_ctr++)
                        {
                            if (util.CheckElement(driver, By.CssSelector("section[id$='sampleproducts'] * div[class$='grid-clone view-inventory']>table>tbody>tr:nth-of-type(" + locator_ctr + ") * input[class^='check']"), 2))
                            {
                                string model_name = util.ReturnAttribute(driver, By.CssSelector("section[id$='sampleproducts'] * div[class$='grid-clone view-inventory']>table>tbody>tr:nth-of-type(" + locator_ctr + ")"), "data-id");
                                //--- Action: Tick the checkbox that you want to buy. ---//
                                initial_steps = initial_steps + "<br>5. Tick select checkbox on product - \"" + model_name + "\"";
                                action.IClick(driver, By.CssSelector("section[id$='sampleproducts'] * div[class$='grid-clone view-inventory']>table>tbody>tr:nth-of-type(" + locator_ctr + ") * input[class^='check']"));

                                if (driver.FindElement(Elements.CFTL_SampleProducts_EvaluationBoards_Tbl_AddToCart_Btn).Enabled)
                                {
                                    string current_url = driver.Url;

                                    //--- Action: Click Add to Cart button ---//
                                    initial_steps = initial_steps + "<br>6. Click add to cart button";
                                    action.IClick(driver, Elements.CFTL_SampleProducts_EvaluationBoards_Tbl_AddToCart_Btn);

                                    if (driver.Url.Contains(current_url))
                                    {
                                        driver.SwitchTo().Window(driver.WindowHandles.Last());
                                    }

                                    ////--- Expected Result: The Evaluation boards selected on Evaluation Boards table should be added on the Shopping Cart ---//
                                    //test.validateStringInstance(driver, driver.Url, shoppingCart_page_url);

                                   
                                    if (util.CheckElement(driver, Elements.Shopping_Cart_SelectShippingCountry_ErrorMsg, 2))
                                    {
                                        initial_steps = initial_steps + "<br>7. If in shopping cart, selected country is empty/not allowed for distribution select US as a country";
                                        action.IClick(driver, Elements.Shopping_Cart_Country_Dropdown);
                                        action.IClick(driver, By.CssSelector("li[value='US']>a"));
                                    }

                                    scenario = "Verify that added product will be added to the shopping cart - cart table";
                                    test.validateStringIsCorrectv2(driver, Elements.Shopping_Cart_AddedModel, model_name, scenario, initial_steps);
                                   
                                }
                                else
                                {
                                    scenario = "Verify that Add To Cart button will be enabled";
                                    test.validateElementIsEnabledv2(driver, Elements.CFTL_SampleProducts_EvaluationBoards_Tbl_AddToCart_Btn,scenario, initial_steps);
                                }

                                break;
                            }
                        }
                    }
                }
                else
                {
                    scenario = "Verify that Check Inventory is enabled";
                    test.validateElementIsEnabledv2(driver, Elements.CFTL_SampleProducts_EvaluationBoards_Tbl_CheckInventory_Btn, scenario, initial_steps);
                }
            }
            else
            {
                scenario = "Verify that Country dropdown is presnet in Buy table";
                test.validateElementIsPresentv2(driver, Elements.CFTL_SampleProducts_EvaluationBoards_Tbl_SelectACountry_Dd, scenario, initial_steps);
            }           
        }

        [Test, Category("CFTL"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Evaluation Boards - View Inventory Table is Present and Working as Expected when the User is Logged In in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Evaluation Boards - View Inventory Table is Present and Working as Expected when the User is Logged In in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Evaluation Boards - View Inventory Table is Present and Working as Expected when the User is Logged In in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Evaluation Boards - View Inventory Table is Present and Working as Expected when the User is Logged In in RU Locale")]
        public void CFTL_SampleProducts_VerifyEvaluationBoardsViewInventoryTableWhenUserIsLoggedIn(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + cftl_url_2);

            if (util.CheckElement(driver, Elements.MyAnalog_Widget_Btn, 2))
            {
                action.ILogInViaMyAnalogWidget(driver, username, password);

                //--- Expected Result: The Country selected on the dropdown should be same to the country set on Account Information page ---//
                test.validateStringIsCorrect(driver, Elements.CFTL_SampleProducts_EvaluationBoards_Tbl_SelectACountry_Dd_Selected_Val, country_selected);

                if (!util.GetText(driver, Elements.CFTL_SampleProducts_EvaluationBoards_Tbl_SelectACountry_Dd_Selected_Val).Equals(country_selected))
                {
                    action.IClick(driver, Elements.CFTL_SampleProducts_EvaluationBoards_Tbl_SelectACountry_Dd_Btn);
                    action.IClick(driver, By.CssSelector("section[id$='sampleproducts'] * div>div:nth-of-type(1) * div[class$='country-filter'] * a[data-id='US']"));
                }

                if (driver.FindElement(Elements.CFTL_SampleProducts_EvaluationBoards_Tbl_CheckInventory_Btn).Enabled)
                {
                    //--- Action: Click on the Check Inventory button ---//
                    action.IClick(driver, Elements.CFTL_SampleProducts_EvaluationBoards_Tbl_CheckInventory_Btn);

                    if (util.CheckElement(driver, Elements.CFTL_SampleProducts_EvaluationBoards_ViewInventory_Tbl_Select_Cb, 2))
                    {
                        int evaluationBoards_viewInventory_tbl_row_count = util.GetCount(driver, By.CssSelector("section[id$='sampleproducts'] * div[class$='grid-clone view-inventory']>table>tbody>tr"));

                        for (int locator_ctr = 1; locator_ctr <= evaluationBoards_viewInventory_tbl_row_count; locator_ctr++)
                        {
                            if (util.CheckElement(driver, By.CssSelector("section[id$='sampleproducts'] * div[class$='grid-clone view-inventory']>table>tbody>tr:nth-of-type(" + locator_ctr + ") * input[class^='check']"), 2))
                            {
                                //--- Action: Tick any of the checkbox/ ---//
                                action.ICheck(driver, By.CssSelector("section[id$='sampleproducts'] * div[class$='grid-clone view-inventory']>table>tbody>tr:nth-of-type(" + locator_ctr + ") * input[class^='check']"));

                                if (driver.FindElement(Elements.CFTL_SampleProducts_EvaluationBoards_Tbl_AddToCart_Btn).Enabled)
                                {
                                    string model_name = util.ReturnAttribute(driver, By.CssSelector("section[id$='sampleproducts'] * div[class$='grid-clone view-inventory']>table>tbody>tr:nth-of-type(" + locator_ctr + ")"), "data-id");
                                    string current_url = driver.Url;

                                    //--- Action: Click "Add to Cart" button ---//
                                    action.IClick(driver, Elements.CFTL_SampleProducts_EvaluationBoards_Tbl_AddToCart_Btn);

                                    if (driver.Url.Contains(current_url))
                                    {
                                        driver.SwitchTo().Window(driver.WindowHandles.Last());
                                    }

                                    //--- Expected Result: The page should be redirected to Shopping Cart page ---//
                                    test.validateStringInstance(driver, driver.Url, shoppingCart_page_url);

                                    if (driver.Url.Contains(shoppingCart_page_url))
                                    {
                                        if (util.CheckElement(driver, Elements.Shopping_Cart_SelectShippingCountry_ErrorMsg, 2))
                                        {
                                            action.IClick(driver, Elements.Shopping_Cart_Country_Dropdown);
                                            action.IClick(driver, By.CssSelector("li[value='US']>a"));
                                        }

                                        //--- Expected Result: The Evaluation boards selected on Evaluation Boards table should be added on the Cart ---//
                                        test.validateStringIsCorrect(driver, Elements.Shopping_Cart_AddedModel, model_name);
                                    }
                                }
                                else
                                {
                                    test.validateElementIsEnabled(driver, Elements.CFTL_SampleProducts_EvaluationBoards_Tbl_AddToCart_Btn);
                                }

                                break;
                            }
                        }
                    }
                }
                else
                {
                    test.validateElementIsEnabled(driver, Elements.CFTL_SampleProducts_EvaluationBoards_Tbl_CheckInventory_Btn);
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.MyAnalog_Widget_Btn);
            }            
        }
    }
}