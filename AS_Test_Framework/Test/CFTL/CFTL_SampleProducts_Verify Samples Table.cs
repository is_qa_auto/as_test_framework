﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.CFTL
{
    [TestFixture]
    public class CFTL_SampleProducts_BuyTable : BaseSetUp
    {
        public CFTL_SampleProducts_BuyTable() : base() { }

        //--- Login Credentials ---//
        string username = "myanalog_t3st_333@mailinator.com";
        string password = "Test_1234";

        //--- URLs ---//
        string cftl_url = "/CN0350";
        string cftl_url_2 = "/CN0271";
        string pdp_url_format = "/products/";
        string shoppingCart_page_url = "shoppingcart";

        //--- Labels ---//
        
        [Test, Category("CFTL"), Category("Core"), Retry(2)]
        [TestCase("en", "Select Shipping Country/Region", TestName = "Verify that the Samples Table is Present and Working as Expected when the User is Logged Out in EN Locale")]
        [TestCase("cn", "CHINA", TestName = "Verify that the Samples Table is Present and Working as Expected when the User is Logged Out in CN Locale")]
        [TestCase("jp", "JAPAN", TestName = "Verify that the Samples Table is Present and Working as Expected when the User is Logged Out in JP Locale")]
        [TestCase("ru", "Select Shipping Country/Region", TestName = "Verify that the Samples Table is Present and Working as Expected when the User is Logged Out in RU Locale")]
        public void CFTL_SampleProducts_VerifyBuyTableWhenUserIsLoggedOut(string Locale, string selectShippingCountry_dd_default_value)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + cftl_url);

            //----- R11 > T1: Verify the Sample Products section -----//

            //--- Expected Result: The following Samples Table should be displayed for the Sample Products section Samples Table ---//
            test.validateElementIsPresent(driver, Elements.CFTL_SampleProducts_Samples_Tbl);

            if (util.CheckElement(driver, Elements.CFTL_SampleProducts_Samples_Tbl, 2))
            {
                

                //--- Expected Result: The Add Selections to Cart button should not be clickable ---//
                test.validateElementIsPresent(driver, Elements.CFTL_SampleProducts_Samples_Tbl_Disabled_AddSelectionsToCart_Btn);

                //--- Action: Select model number on the Available Product Models to Sample column ---//
                action.ICheck(driver, Elements.CFTL_SampleProducts_Samples_Tbl_AvailableProductModelsToSample_Cb);

                if (driver.FindElement(Elements.CFTL_SampleProducts_Samples_Tbl_AddSelectionsToCart_Btn).Enabled)
                {
                    string model_name = util.ReturnAttribute(driver, Elements.CFTL_SampleProducts_Samples_Tbl_AvailableProductModelsToSample_Cb, "id");
                    string current_url = driver.Url;

                    //--- Action: Click on the Add Selections to Cart button ---//
                    action.IClick(driver, Elements.CFTL_SampleProducts_Samples_Tbl_AddSelectionsToCart_Btn);

                    if (driver.Url.Contains(current_url))
                    {
                        driver.SwitchTo().Window(driver.WindowHandles.Last());
                    }

                    //--- Expected Result: An empty Shopping Cart page should be displayed ---//
                    test.validateStringInstance(driver, driver.Url, shoppingCart_page_url);

                    if (driver.Url.Contains(shoppingCart_page_url))
                    {
                        //--- Expected Result: An empty Shopping Cart page should be displayed ---//
                        if (Locale.Equals("en") || Locale.Equals("ru"))
                        {
                            test.validateElementIsNotPresent(driver, Elements.Shopping_Cart_AddedModel);
                            test.validateStringIsCorrect(driver, Elements.Shopping_Cart_SelectShippingCountry_Dropdown_SelectedVal, selectShippingCountry_dd_default_value);
                            //--- Action: Select a country on the dropdown ---//
                            action.IClick(driver, Elements.Shopping_Cart_Country_Dropdown);
                            action.IClick(driver, By.CssSelector("li[value='US']>a"));
                            test.validateStringIsCorrect(driver, Elements.Shopping_Cart_AddedModel, model_name);
                        }
                        else
                        {
                            test.validateStringIsCorrect(driver, Elements.Shopping_Cart_SelectShippingCountry_Dropdown_SelectedVal, selectShippingCountry_dd_default_value);
                            //--- Expected Result: The model number selected on Samples table should be added on the Shopping Cart ---//
                            test.validateStringIsCorrect(driver, Elements.Shopping_Cart_AddedModel, model_name);
                        }
                        driver.Navigate().GoToUrl(Configuration.Env_Url + Locale + cftl_url);

                        //----- R11 > T2: Verify the Samples Table -----//

                        if (util.CheckElement(driver, Elements.CFTL_SampleProducts_Samples_Tbl_Product_Links, 2))
                        {
                            //--- Action: Click on the part number link on the Products column ---//
                            action.IOpenLinkInNewTab(driver, Elements.CFTL_SampleProducts_Samples_Tbl_Product_Links);
                            Thread.Sleep(3000);

                            //--- Expected Result: The product detail page should be displayed ---//
                            test.validateStringInstance(driver, driver.Url, "analog.com/" + Locale + pdp_url_format);

                            driver.Close();
                            driver.SwitchTo().Window(driver.WindowHandles.First());
                        }
                        else
                        {
                            test.validateElementIsPresent(driver, Elements.CFTL_SampleProducts_Samples_Tbl_Product_Links);
                        }
                    }
                }
                else
                {
                    test.validateElementIsEnabled(driver, Elements.CFTL_SampleProducts_Samples_Tbl_AddSelectionsToCart_Btn);
                }
            }
        }

        [Test, Category("CFTL"), Category("Core"), Retry(2)]
        [TestCase("en", TestName = "Verify that the Samples Table is Present and Working as Expected when the User is Logged In in EN Locale")]
        [TestCase("cn", TestName = "Verify that the Samples Table is Present and Working as Expected when the User is Logged In in CN Locale")]
        [TestCase("jp", TestName = "Verify that the Samples Table is Present and Working as Expected when the User is Logged In in JP Locale")]
        [TestCase("ru", TestName = "Verify that the Samples Table is Present and Working as Expected when the User is Logged In in RU Locale")]
        public void CFTL_SampleProducts_VerifyBuyTableWhenUserIsLoggedIn(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + cftl_url_2);

            if (util.CheckElement(driver, Elements.MyAnalog_Widget_Btn, 2))
            {
                action.ILogInViaMyAnalogWidget(driver, username, password);

                if (util.CheckElement(driver, Elements.CFTL_SampleProducts_Samples_Tbl_AvailableProductModelsToSample_Cb, 2))
                {
                    //----- R12 > T2_1: Verify Add Selection button behavior (AL-9987) -----//

                    //--- Action: Click on the check box on any Products availbale in Available Product Models to Sample column ---//
                    action.ICheck(driver, Elements.CFTL_SampleProducts_Samples_Tbl_AvailableProductModelsToSample_Cb);

                    //--- Expected Result: Add Selection button will enable. ---//
                    test.validateElementIsEnabled(driver, Elements.CFTL_SampleProducts_Samples_Tbl_AddSelectionsToCart_Btn);

                    //--- Action: Uncheck the selected check box ---//
                    action.IUncheck(driver, Elements.CFTL_SampleProducts_Samples_Tbl_AvailableProductModelsToSample_Cb);

                    //--- Expected Result: Add Selection button will disabled ---//
                    test.validateElementIsPresent(driver, Elements.CFTL_SampleProducts_Samples_Tbl_Disabled_AddSelectionsToCart_Btn);

                    //----- R12 > T2: Verify the Samples Table (AL-9987) -----//
                    driver.Navigate().Refresh();
                    Thread.Sleep(1000);

                    //--- Action: Select model number on the Available Product Models to Sample column ---//
                    action.ICheck(driver, Elements.CFTL_SampleProducts_Samples_Tbl_AvailableProductModelsToSample_Cb);

                    //--- Action: Click on the Add Selections to Cart button ---//
                    action.IClick(driver, Elements.CFTL_SampleProducts_Samples_Tbl_AddSelectionsToCart_Btn);

                    //--- Expected Result: The page should be redirected to Shopping Cart page ---//
                    test.validateStringInstance(driver, driver.Url, shoppingCart_page_url);
                }
                else
                {
                    test.validateElementIsPresent(driver, Elements.CFTL_SampleProducts_Samples_Tbl_AvailableProductModelsToSample_Cb);
                }
            }
            else
            {
                test.validateElementIsPresent(driver, Elements.MyAnalog_Widget_Btn);
            }
        }
    }
}