﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Threading;

namespace AS_Test_Framework.AdiSignalPlus
{

    [TestFixture]
    public class SignalPlus_LandingPage_StickyHeader : BaseSetUp
    {
        public SignalPlus_LandingPage_StickyHeader() : base() { }

        //--- URLs ---//
        string adiSignals_url = "/signals.html";
        string connectivity_url = "/connectivity.html";
        string signalsArticles_url_format = "/articles/";
        string signalsVideos_url_format = "/videos/";

        [Test, Category("SignalPlus"), Category("Core")]
        [TestCase("en", TestName = "Verify that the Navigation Buttons are Working as Expected - EN Locale")]
        [TestCase("cn", TestName = "Verify that the Navigation Buttons are Working as Expected - CN Locale")]
        [TestCase("jp", TestName = "Verify that the Navigation Buttons are Working as Expected - JP Locale")]
        [TestCase("ru", TestName = "Verify that the Navigation Buttons are Working as Expected - RU Locale")]
        public void SignalPlus_LandingPage_StickyHeader_VerifyNavigationButtons(string Locale)
        {
            //--- Action: Go to Signals+ homepage ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + adiSignals_url);

            //----- ADI Signals+ TestPlan > Test Case Title: Verify display of sticky header (IQ-11008/AL-17216) -----//

            //--- Action: Scroll down ---//
            action.IMouseOverTo(driver, Elements.Footer);

            //--- Expected Result: Sticky header is displayed ---//
            test.validateElementIsPresent(driver, Elements.SignalPlus_StickyHeader);

            //--- Expected Result: All is selected by default ---//
            test.validateElementIsPresent(driver, Elements.SignalPlus_StickyHeader_Selected_All_Button);

            //----- ADI Signals+ TestPlan > Test Case Title: Verify navigation using sticky header (IQ-11008/AL-17216) -----//

            //----- CONNECTIVITY -----//

            //--- Action: Click Connectivity in sticky header ---//
            action.IClick(driver, Elements.SignalPlus_StickyHeader_Connectivity_Button);
            Thread.Sleep(2000);

            //--- Expected Result: Signals+ Connectivity page is displayed ---//
            test.validateStringInstance(driver, driver.Url, connectivity_url);
        }

        [Test, Category("SignalPlus"), Category("Core")]
        [TestCase("en", TestName = "Verify that the Filter Buttons are Working as Expected - EN Locale")]
        [TestCase("cn", TestName = "Verify that the Filter Buttons are Working as Expected - CN Locale")]
        [TestCase("jp", TestName = "Verify that the Filter Buttons are Working as Expected - JP Locale")]
        [TestCase("ru", TestName = "Verify that the Filter Buttons are Working as Expected - RU Locale")]
        public void SignalPlus_LandingPage_StickyHeader_VerifyFilterButtons(string Locale)
        {
            //--- Action: Go to Signals+ homepage ---//
            action.Navigate(driver, Configuration.Env_Url + Locale + adiSignals_url);

            //----- ADI Signals+ TestPlan > Test Case Title: Verify filters in sticky header (IQ-11008/AL-17216) -----//

            //--- Action: Get the Count of the Article Cards ---//
            int articleCards_count = util.GetCount(driver, Elements.SignalPlus_ArticleCards_Link);

            //--- Action: Scroll down ---//
            action.IMouseOverTo(driver, Elements.Footer);

            //--- Action: Click Articles toggle button ---//
            action.IClick(driver, Elements.SignalPlus_StickyHeader_Articles_Button);

            //--- Expected Result: Articles toggle button is active ---//
            test.validateElementIsPresent(driver, Elements.SignalPlus_StickyHeader_Selected_Articles_Button);

            //--- Action: Click Videos toggle button (do not make any changes to Articles button) ---//
            action.IClick(driver, Elements.SignalPlus_StickyHeader_Videos_Button);

            //--- Expected Result: Videos toggle button is active ---//
            test.validateElementIsPresent(driver, Elements.SignalPlus_StickyHeader_Selected_Videos_Button);

            //--- Action: Click Articles toggle button again ---//
            action.IClick(driver, Elements.SignalPlus_StickyHeader_Articles_Button);
            Thread.Sleep(1000);

            //--- Expected Result: Articles toggle button is reset ---//
            test.validateElementIsPresent(driver, Elements.SignalPlus_StickyHeader_Articles_Button);

            //--- Expected Result: Only Video links are displayed in the main body of page ---//
            test.validateStringInstance(driver, util.ReturnAttribute(driver, Elements.SignalPlus_ArticleCards_Link, "href"), signalsVideos_url_format);

            //--- Action: Click Articles toggle button (do not make any changes to Videos button) ---//
            action.IClick(driver, Elements.SignalPlus_StickyHeader_Articles_Button);

            //--- Action: Click Videos toggle button again ---//
            action.IClick(driver, Elements.SignalPlus_StickyHeader_Videos_Button);

            //--- Expected Result: Videos toggle button is reset ---//
            test.validateElementIsPresent(driver, Elements.SignalPlus_StickyHeader_Videos_Button);

            //--- Expected Result: Only Article links are displayed in the main body of page ---//
            test.validateStringInstance(driver, util.ReturnAttribute(driver, Elements.SignalPlus_ArticleCards_Link, "href"), signalsArticles_url_format);

            //--- Action: Click Articles toggle button again ---//
            action.IClick(driver, Elements.SignalPlus_StickyHeader_Articles_Button);

            //--- Expected Result: All Signals+ links are displayed ---//
            test.validateCountIsEqual(driver, articleCards_count, util.GetCount(driver, Elements.SignalPlus_ArticleCards_Link));
        }
    }
}