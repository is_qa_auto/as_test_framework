﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Threading;

namespace AS_Test_Framework.AdiSignalPlus
{

    [TestFixture]
    public class SignalPlus_Recommended : BaseSetUp
    {
        public SignalPlus_Recommended() : base() { }

        [Test, Category("SignalPlus"), Category("Core")]
        [TestCase("en", TestName = "Verify that recommended in Signal+ section is present in EN Locale")]
        [TestCase("cn", TestName = "Verify that recommended in Signal+ section is present in CN Locale")]
        [TestCase("jp", TestName = "Verify that recommended in Signal+ section is present in JP Locale")]
        [TestCase("ru", TestName = "Verify that recommended in Signal+ section is present in RU Locale")]
        public void SignalPlus_VerifyRecommendedSection(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/signals/articles/combating-infectious-diseases.html");
            action.IMouseOverTo(driver, Elements.SignalPlus_RecommendedSection);
            Thread.Sleep(3000);
            test.validateElementIsPresent(driver, Elements.SignalPlus_RecommendedSection);
            test.validateElementIsPresent(driver, Elements.SignalPlus_RecommendedSection_Title);
            test.validateElementIsPresent(driver, Elements.SignalPlus_RecommendedSection_Card);
            test.validateCountIsLessOrEqual(driver, 3, util.GetCount(driver, Elements.SignalPlus_RecommendedSection_Card));
            test.validateElementIsPresent(driver, Elements.SignalPlus_RecommendedSection_Card_Thumbnail);
            test.validateElementIsPresent(driver, Elements.SignalPlus_RecommendedSection_Card_Link);
            string CurrentPage = util.GetText(driver, Elements.SignalPlus_RecommendedSection_Card_Link);
            action.IClick(driver, Elements.SignalPlus_RecommendedSection_Card_Link);
            test.validateStringIsCorrect(driver, By.CssSelector("div[name='adi_breadcrumb'] li[class='active']>span"), CurrentPage);
        }

    }
}