﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Threading;

namespace AS_Test_Framework.AdiSignalPlus
{

    [TestFixture]
    public class SignalPlus_SocialSharing : BaseSetUp
    {
        public SignalPlus_SocialSharing() : base() { }

        [Test, Category("SignalPlus"), Category("Core")]
        [TestCase("en", TestName = "Verify that Social Sharing Tab is present in Signal+")]
        public void SignalPlus_VerifyRecommendedSection(string Locale)
        {            
            action.Navigate(driver, Configuration.Env_Url + Locale + "/signals/articles/combating-infectious-diseases.html");
            Thread.Sleep(5000);
            action.IMouseOverTo(driver, Elements.Footer);
            test.validateElementIsPresent(driver, Elements.SignalPlus_Social_Tab);
            test.validateElementIsPresent(driver, Elements.SignalPlus_Social_FB);
            test.validateElementIsPresent(driver, Elements.SignalPlus_Social_Twitter);
            test.validateElementIsPresent(driver, Elements.SignalPlus_Social_LinkedIn);
            test.validateElementIsPresent(driver, Elements.SignalPlus_Social_Line);
            test.validateElementIsPresent(driver, Elements.SignalPlus_Social_AddMore);
        }

    }
}