﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Linq;

namespace AS_Test_Framework.AdiSignalPlus
{

    [TestFixture]
    public class SignalPlus_RelatedContent : BaseSetUp
    {
        public SignalPlus_RelatedContent() : base() { }

        [Test, Category("SignalPlus"), Category("Core")]
        [TestCase("en", "RELATED CONTENT", TestName = "Verify that ralated content section is present in Signal+ page in EN Locale")]
        [TestCase("cn", "相关信息", TestName = "Verify that ralated content section is present in Signal+ page in CN Locale")]
        [TestCase("jp", "関連コンテンツ", TestName = "Verify that ralated content section is present in Signal+ page in JP Locale")]
        [TestCase("ru", "МАТЕРИАЛЫ ПО ТЕМЕ", TestName = "Verify that ralated content section is present in Signal+ page in RU Locale")]
        public void SignalPlus_VerifyRelatedContentSection(string Locale, string RelatedLabel)
        {            
            action.Navigate(driver, Configuration.Env_Url + Locale + "/signals/articles/combating-infectious-diseases.html");
            test.validateElementIsPresent(driver, Elements.SignalPlus_RelatedSection);
            test.validateStringIsCorrect(driver, Elements.SignalPlus_RelatedSection_Title, RelatedLabel);
        }

        [Test, Category("SignalPlus"), Category("Core")]
        [TestCase("en", "Markets and Technologies", TestName = "Verify that Market And Technology is present in related content section is present in Signal+ page for EN Locale")]
        [TestCase("cn", "领域 & 技术", TestName = "Verify that Market And Technology is present in related content section is present in Signal+ page for CN Locale")]
        [TestCase("jp", "マーケット ＆ テクノロジー", TestName = "Verify that Market And Technology is present in related content section is present in Signal+ page for JP Locale")]
        [TestCase("ru", "Области применения и технологии", TestName = "Verify that Market And Technology is present in related content section is present in Signal+ page for RU Locale")]
        public void SignalPlus_VerifyRelatedContentMarketAndTechnology(string Locale, string MarketAndTechnologyLabel)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/signals/articles/will-covid-19-accelerate-electrification-revolution.html");

            if (util.CheckElement(driver, Elements.SignalPlus_RelatedSection_MarketSection, 5))
            {
                test.validateStringIsCorrect(driver, Elements.SignalPlus_RelatedSection_MarketAndTechnology_Label, MarketAndTechnologyLabel);
                test.validateElementIsPresent(driver, Elements.SignalPlus_RelatedSection_MarketAndTechnology_Category);
                string Category = util.GetText(driver, Elements.SignalPlus_RelatedSection_MarketAndTechnology_Category).Split('(')[0].Trim();
                action.IClick(driver, Elements.SignalPlus_RelatedSection_MarketAndTechnology_Category);
                driver.SwitchTo().Window(driver.WindowHandles.Last());
                test.validateStringInstance(driver, util.GetText(driver, By.CssSelector("div[name='adi_breadcrumb']")), Category);

                /***Expand Category****/
                driver.SwitchTo().Window(driver.WindowHandles.First());
                action.IClick(driver, By.CssSelector("aside[class='related col-xs-12 col-sm-4'] div[class*='related__markets-and-technologies']>ul>li span[class='accordionGroup__toggle__icon']"));
                test.validateElementIsPresent(driver, Elements.SignalPlus_RelatedSection_MarketAndTechnology_SubCategory);

                string SubCategory = util.GetText(driver, Elements.SignalPlus_RelatedSection_MarketAndTechnology_SubCategory);
                action.IClick(driver, Elements.SignalPlus_RelatedSection_MarketAndTechnology_SubCategory);
                driver.SwitchTo().Window(driver.WindowHandles.Last());
                test.validateStringIsCorrect(driver, By.CssSelector("div[name='adi_breadcrumb'] li[class='active']>span"), SubCategory);
            }
        }

        [Test, Category("SignalPlus"), Category("Core")]
        [TestCase("en", "Resources",  TestName = "Verify that Resources is present in related content section is present in Signal+ page in EN Locale")]
        [TestCase("cn", "资源", TestName = "Verify that Resources is present in related content section is present in Signal+ page in CN Locale")]
        [TestCase("jp", "リソース",  TestName = "Verify that Resources is present in related content section is present in Signal+ page in JP Locale")]
        [TestCase("ru", "Ресурсы", TestName = "Verify that Resources is present in related content section is present in Signal+ page in RU Locale")]
        public void SignalPlus_VerifyRelatedContentResources(string Locale, string ResourceLabel)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/signals/articles/will-covid-19-accelerate-electrification-revolution.html");
            if (util.CheckElement(driver, Elements.SignalPlus_RelatedSection_ResourcesSection_Title, 5))
            {
                test.validateStringIsCorrect(driver, Elements.SignalPlus_RelatedSection_ResourcesSection_Title, ResourceLabel);
                test.validateElementIsPresent(driver, Elements.SignalPlus_RelatedSection_ResourcesSection_ArticleCategory);
                test.validateElementIsPresent(driver, Elements.SignalPlus_RelatedSection_ResourcesSection_ArticleLink);
                string ResourceTitle = util.GetText(driver, Elements.SignalPlus_RelatedSection_ResourcesSection_ArticleLink);
                action.IClick(driver, Elements.SignalPlus_RelatedSection_ResourcesSection_ArticleLink);
                if (!driver.Url.Contains("signals/videos") && !driver.Url.Contains("signals/articles"))
                {
                    test.validateStringIsCorrect(driver, By.CssSelector("section[class='col-lg-8 col-md-8 col-sm-12 col-xs-12 leftCol'] h1"), ResourceTitle.ToUpper());
                }
                else
                {
                   test.validateStringIsCorrect(driver, By.CssSelector("article[class='col-md-8 col-xs-12'] div[class='spotlight__container__column']>h1"), ResourceTitle.ToUpper());
                }
            }
        }

        [Test, Category("SignalPlus"), Category("Core")]
        [TestCase("en", TestName = "Verify that Products is present in related content section is present in Signal+ page")]
        public void SignalPlus_VerifyRelatedContentProducts(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/signals/articles/will-covid-19-accelerate-electrification-revolution.html");
            if (util.CheckElement(driver, Elements.SignalPlus_RelatedSection_ProductsSection_Title, 5))
            {
                test.validateStringIsCorrect(driver, Elements.SignalPlus_RelatedSection_ProductsSection_Title, "Products");
                test.validateElementIsPresent(driver, Elements.SignalPlus_RelatedSection_ProductNo);
                test.validateElementIsPresent(driver, Elements.SignalPlus_RelatedSection_ProductDescription);
                string ProductNo = util.GetText(driver, Elements.SignalPlus_RelatedSection_ProductNo);
                string ProductDescription = util.GetText(driver, Elements.SignalPlus_RelatedSection_ProductDescription);
                action.IClick(driver, Elements.SignalPlus_RelatedSection_ProductNo);
                test.validateStringIsCorrect(driver, Elements.PDP_ProductNo_Txt, ProductNo);
                test.validateStringIsCorrect(driver, Elements.PDP_Desc_Txt, ProductDescription);
            }
        }

        [Test, Category("SignalPlus"), Category("Core")]
        [TestCase("en", TestName = "Verify that Products Category is present in related content section is present in Signal+ page")]
        public void SignalPlus_VerifyRelatedContentProductsCategory(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/signals/articles/low-earth-orbit-satellites-providing-global-connectivity.html");
            if (util.CheckElement(driver, Elements.SignalPlus_RelatedSection_ProductsCategory_Title, 5))
            {
                test.validateStringIsCorrect(driver, Elements.SignalPlus_RelatedSection_ProductsCategory_Title, "Categories");
                test.validateElementIsPresent(driver, Elements.SignalPlus_RelatedSection_ProductsCategory_Link);
                string ProductCategoryLink = util.GetText(driver, Elements.SignalPlus_RelatedSection_ProductsCategory_Link).Split('(')[0].Trim();
                action.IClick(driver, Elements.SignalPlus_RelatedSection_ProductsCategory_Link);
                driver.SwitchTo().Window(driver.WindowHandles.Last());
                test.validateStringInstance(driver, util.GetText(driver, By.CssSelector("div[name='adi_breadcrumb']")), ProductCategoryLink);

                driver.SwitchTo().Window(driver.WindowHandles.First());
                if(util.CheckElement(driver, By.CssSelector("div[class='related__categories']>ul>li span[class='accordionGroup__toggle__icon']"), 5))
                { 
                    action.IClick(driver, By.CssSelector("div[class='related__categories']>ul>li span[class='accordionGroup__toggle__icon']"));
                    test.validateElementIsPresent(driver, Elements.SignalPlus_RelatedSection_ProductsSubCategory_Link);
                    string ProductSubCategoryLink = util.GetText(driver, Elements.SignalPlus_RelatedSection_ProductsSubCategory_Link);
                    action.IClick(driver, Elements.SignalPlus_RelatedSection_ProductsSubCategory_Link);
                    driver.SwitchTo().Window(driver.WindowHandles.Last());
                    test.validateStringIsCorrect(driver, By.CssSelector("div[name='adi_breadcrumb'] li[class='active']>span"), ProductSubCategoryLink);
                }
            }
        }
    }
}