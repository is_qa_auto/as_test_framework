﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Collections.Generic;
using System.Threading;

namespace AS_Test_Framework.AdiSignalPlus
{

    [TestFixture]
    public class SignalPlus_Search : BaseSetUp
    {
        public SignalPlus_Search() : base() { }

        [Test, Category("SignalPlus"), Category("Core")]
        [TestCase("en", TestName = "Verify that user can search signal+ article/video via global search")]
        public void SignalPlus_VerifySignalPlusCNLandingPage(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/index.html");
            /***Search For Article******/
            string ArticleKw = "Combating Infectious Diseases Through Rapid Nanosensor Technology Diagnostics";
            action.ISearchViaGlobalSearchTextbox(driver, ArticleKw);
            test.validateStringIsCorrect(driver, Elements.GlobalSearchResults_BaseballCard_Title_Link, ArticleKw);
            test.validateStringIsCorrect(driver, By.CssSelector("div[class='search-results-item top-result']>span[class='title-category']"), "Article");

            /***Search For Video******/
            string VideoKw = "Vehicle Electrification: Longer Range, Faster Charge, More Efficient";
            action.ISearchViaGlobalSearchTextbox(driver, VideoKw);
            test.validateStringIsCorrect(driver, Elements.GlobalSearchResults_BaseballCard_Title_Link, VideoKw);
            test.validateStringIsCorrect(driver, By.CssSelector("div[class='search-results-item top-result']>span[class='title-category']"), "Videos");

        }

    }
}