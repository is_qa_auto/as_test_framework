﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using System.Threading;
using OpenQA.Selenium;
using System.Linq;

namespace AS_Test_Framework.AdiSignalPlus
{

    [TestFixture]
    public class SignalPlus_Subscription : BaseSetUp
    {
        public SignalPlus_Subscription() : base() { }

        [Test, Category("SignalPlus"), Category("Core")]
        [TestCase("en", TestName = "Verify that Signal+ Subscription elements/components are all present")]
        public void SignalPlus_VerifySubscriptionELements(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/signals.html");
            Thread.Sleep(5000);
            test.validateElementIsNotPresent(driver, Elements.SignalPlus_Subscribe_ExpandedView);
            action.IClick(driver, Elements.SignalPlus_Subscribe_Tab);
            test.validateStringIsCorrect(driver, Elements.SignalPlus_Subscribe_Header, "SIGNALS+ NEWSLETTER SUBSCRIPTION");

            /*******Personal Information Field******/
            test.validateElementIsPresent(driver, Elements.SignalPlus_Subscribe_FName_Textfield);
            test.validateElementIsPresent(driver, Elements.SignalPlus_Subscribe_LName_Textfield);
            test.validateElementIsPresent(driver, Elements.SignalPlus_Subscribe_Email_Textfield);

            test.validateString(driver, util.ReturnAttribute(driver, Elements.SignalPlus_Subscribe_FName_Textfield, "placeholder"), "First Name");
            test.validateString(driver, util.ReturnAttribute(driver, Elements.SignalPlus_Subscribe_LName_Textfield, "placeholder"), "Last Name");
            test.validateString(driver, util.ReturnAttribute(driver, Elements.SignalPlus_Subscribe_Email_Textfield, "placeholder"), "Email Address");

            action.IClick(driver, Elements.SignalPlus_Subscribe_FName_Textfield);
            test.validateElementIsPresent(driver, Elements.SignalPlus_Subscribe_FName_OverheadLabel);
            action.IClick(driver, Elements.SignalPlus_Subscribe_LName_Textfield);
            test.validateElementIsPresent(driver, Elements.SignalPlus_Subscribe_LName_OverheadLabel);
            action.IClick(driver, Elements.SignalPlus_Subscribe_Email_Textfield);
            test.validateElementIsPresent(driver, Elements.SignalPlus_Subscribe_Email_OverheadLabel);

            /******GDPR Section Fields *****/
            test.validateElementIsPresent(driver, Elements.SignalPlus_Subscribe_Consent_Checkbox);
            test.validateStringIsCorrect(driver, Elements.SignalPlus_Subscribe_Consent_Message, "* I agree to allow Analog Devices to use my personal data to improve my user experience through personalization and analytics.");
            test.validateElementIsPresent(driver, Elements.SignalPlus_Subscribe_Communication_Checkbox);
            test.validateStringIsCorrect(driver, Elements.SignalPlus_Subscribe_Communication_Message, "* Yes, I’d like to receive communications from Analog Devices and authorized partners related to ADI’s products and services.");
            test.validateElementIsPresent(driver, Elements.SignalPlus_Subscribe_SubscribeToSignal_Button);
            test.validateElementIsPresent(driver, Elements.SignalPlus_Subscribe_PrivacySettingsLink);
            test.validateElementIsPresent(driver, Elements.SignalPlus_Subscribe_PrivacyAndSecurityLink);
            test.validateElementIsPresent(driver, Elements.SignalPlus_Subscribe_AuthorizedPartnersLink);

            //----- ADI Signals+ TestPlan > Test Case Title: Verify Content Hub Newsletter Subscription elements (IQ-10406/AL-17172) -----//

            //--- Action: Click again the Subscribe menu in the filtering bar ---//
            action.IClick(driver, Elements.SignalPlus_Subscribe_Tab);

            //--- Expected Result: The Subscribe panel is closed ---//
            test.validateElementIsNotPresent(driver, Elements.SignalPlus_Subscribe_ExpandedView);
        }


        [Test, Category("SignalPlus"), Category("Core")]
        [TestCase("en", TestName = "Verify that all links related to GDPR are working as expected")]
        public void SignalPlus_VerifyGDPRLinks(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/signals.html");
            Thread.Sleep(5000);
            action.IClick(driver, Elements.SignalPlus_Subscribe_Tab);

            action.IClick(driver, Elements.SignalPlus_Subscribe_Communication_AuthorizedLink);
            driver.SwitchTo().Window(driver.WindowHandles.Last());
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/about-adi/corporate-information/sales-distribution.html");
            driver.SwitchTo().Window(driver.WindowHandles.First());
            //driver.Navigate().Back();
            //action.IClick(driver, Elements.SignalPlus_Subscribe_Tab);

            action.IClick(driver, Elements.SignalPlus_Subscribe_PrivacySettingsLink);
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/landing-pages/001/privacy-settings.html");
            driver.Navigate().Back();
            action.IClick(driver, Elements.SignalPlus_Subscribe_Tab);

            action.IClick(driver, Elements.SignalPlus_Subscribe_PrivacyAndSecurityLink);
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/about-adi/landing-pages/001/privacy_security_statement.html");
            driver.Navigate().Back();
            action.IClick(driver, Elements.SignalPlus_Subscribe_Tab);

            action.IClick(driver, Elements.SignalPlus_Subscribe_AuthorizedPartnersLink);
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/about-adi/corporate-information/sales-distribution.html");


        }

        [Test, Category("SignalPlus"), Category("Core")]
        [TestCase("en", TestName = "Verify that error message will be displayed when user submits the form using an invalid email address format")]
        public void SignalPlus_VerifyEmailAddressValidation(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/signals.html");
            Thread.Sleep(5000);
            action.IClick(driver, Elements.SignalPlus_Subscribe_Tab);

            action.IType(driver, Elements.SignalPlus_Subscribe_FName_Textfield, "Test Firstname");
            action.IType(driver, Elements.SignalPlus_Subscribe_LName_Textfield, "Test Lastname");
            action.IType(driver, Elements.SignalPlus_Subscribe_Email_Textfield, "test only");
            action.IType(driver, Elements.SignalPlus_Subscribe_Email_Textfield, Keys.Tab);
            //action.IClick(driver, Elements.SignalPlus_Subscribe_Consent_Checkbox);
            //action.IClick(driver, Elements.SignalPlus_Subscribe_Communication_Checkbox);
            //action.IClick(driver, Elements.SignalPlus_Subscribe_SubscribeToSignal_Button);
            test.validateElementIsPresent(driver, By.CssSelector("input[id='subscribe-email'][class='ch-masthead__subscription__container__fields__field__input ch-masthead__subscription__container__fields__field__input--error']"));
        }

        [Test, Category("SignalPlus"), Category("Core")]
        [TestCase("en", TestName = "Verify subscription using an Email that is not yet registered to myAnalog and not yet subscribed to Content Hub")]
        public void SignalPlus_VerifySuccessfulSubscription1(string Locale)
        {
            string EmailAddress = util.Generate_EmailAddress();

            action.Navigate(driver, Configuration.Env_Url + Locale + "/signals.html");
            Thread.Sleep(5000);
            action.IClick(driver, Elements.SignalPlus_Subscribe_Tab);

            action.IType(driver, Elements.SignalPlus_Subscribe_FName_Textfield, "Test Firstname");
            action.IType(driver, Elements.SignalPlus_Subscribe_LName_Textfield, "Test Lastname");
            action.IType(driver, Elements.SignalPlus_Subscribe_Email_Textfield, EmailAddress);
            action.IClick(driver, Elements.SignalPlus_Subscribe_Consent_Checkbox);
            action.IClick(driver, Elements.SignalPlus_Subscribe_Communication_Checkbox);
            action.IClick(driver, Elements.SignalPlus_Subscribe_SubscribeToSignal_Button);
            test.validateStringIsCorrect(driver, Elements.SignalPlus_Subscribe_SuccessMessage, "Thank you for subscribing to ADI Signals+. A confirmation email has been sent to your inbox.");
            test.validateStringIsCorrect(driver, Elements.SignalPlus_Subscribe_SuccessMessage_Sub, "You'll soon receive timely updates on all the breakthrough technologies impacting human lives across the globe. Enjoy!");
            test.validateElementIsPresent(driver, Elements.SignalPlus_Subscribe_SuccessMessage_CloseBtn);
            action.IClick(driver, Elements.SignalPlus_Subscribe_SuccessMessage_CloseBtn);
            test.validateElementIsNotPresent(driver, Elements.SignalPlus_Subscribe_ExpandedView);

            /*****This is for T7*****/
            driver.Navigate().Refresh();
            Thread.Sleep(5000);
            action.IClick(driver, Elements.SignalPlus_Subscribe_Tab);

            action.IType(driver, Elements.SignalPlus_Subscribe_FName_Textfield, "Test Firstname");
            action.IType(driver, Elements.SignalPlus_Subscribe_LName_Textfield, "Test Lastname");
            action.IType(driver, Elements.SignalPlus_Subscribe_Email_Textfield, EmailAddress);
            action.IClick(driver, Elements.SignalPlus_Subscribe_Consent_Checkbox);
            action.IClick(driver, Elements.SignalPlus_Subscribe_Communication_Checkbox);
            action.IClick(driver, Elements.SignalPlus_Subscribe_SubscribeToSignal_Button);
            test.validateStringIsCorrect(driver, Elements.SignalPlus_Subscribe_SuccessMessage, "Thank you for subscribing to ADI Signals+. A confirmation email has been sent to your inbox.");
            test.validateStringIsCorrect(driver, Elements.SignalPlus_Subscribe_SuccessMessage_Sub, "You'll soon receive timely updates on all the breakthrough technologies impacting human lives across the globe. Enjoy!");
        }

        [Test, Category("SignalPlus"), Category("Core")]
        [TestCase("en", TestName = "Verify subscription using an Email that is registered to myAnalog but not yet subscribed to Content Hub")]
        public void SignalPlus_VerifySuccessfulSubscription2(string Locale)
        {
            string EmailAddress = util.Generate_EmailAddress();

            action.Navigate(driver, Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/app/registration");
            action.ICreateNewUser(driver, EmailAddress, "Test Subscribe", "Registered", "Test_1234", "Test_City", "12345", "Analog");
            driver.Navigate().GoToUrl(Configuration.Env_Url + Locale + "/signals.html");
            Thread.Sleep(5000);
            action.IClick(driver, Elements.SignalPlus_Subscribe_Tab);
            action.IType(driver, Elements.SignalPlus_Subscribe_FName_Textfield, "Test Firstname");
            action.IType(driver, Elements.SignalPlus_Subscribe_LName_Textfield, "Test Lastname");
            action.IType(driver, Elements.SignalPlus_Subscribe_Email_Textfield, EmailAddress);
            action.IClick(driver, Elements.SignalPlus_Subscribe_Consent_Checkbox);
            action.IClick(driver, Elements.SignalPlus_Subscribe_Communication_Checkbox);
            action.IClick(driver, Elements.SignalPlus_Subscribe_SubscribeToSignal_Button);
            test.validateStringIsCorrect(driver, Elements.SignalPlus_Subscribe_SuccessMessage, "Thank you for subscribing to ADI Signals+. A confirmation email has been sent to your inbox.");
            test.validateStringIsCorrect(driver, Elements.SignalPlus_Subscribe_SuccessMessage_Sub, "You'll soon receive timely updates on all the breakthrough technologies impacting human lives across the globe. Enjoy!");
            test.validateElementIsPresent(driver, Elements.SignalPlus_Subscribe_SuccessMessage_CloseBtn);
            action.IClick(driver, Elements.SignalPlus_Subscribe_SuccessMessage_CloseBtn);
            test.validateElementIsNotPresent(driver, Elements.SignalPlus_Subscribe_ExpandedView);

            /*****This is for T8*****/
            driver.Navigate().Refresh();
            Thread.Sleep(5000);
            action.IClick(driver, Elements.SignalPlus_Subscribe_Tab);

            action.IType(driver, Elements.SignalPlus_Subscribe_FName_Textfield, "Test Firstname");
            action.IType(driver, Elements.SignalPlus_Subscribe_LName_Textfield, "Test Lastname");
            action.IType(driver, Elements.SignalPlus_Subscribe_Email_Textfield, EmailAddress);
            action.IClick(driver, Elements.SignalPlus_Subscribe_Consent_Checkbox);
            action.IClick(driver, Elements.SignalPlus_Subscribe_Communication_Checkbox);
            action.IClick(driver, Elements.SignalPlus_Subscribe_SubscribeToSignal_Button);
            test.validateStringIsCorrect(driver, Elements.SignalPlus_Subscribe_SuccessMessage, "Thank you for subscribing to ADI Signals+. A confirmation email has been sent to your inbox.");
            test.validateStringIsCorrect(driver, Elements.SignalPlus_Subscribe_SuccessMessage_Sub, "You'll soon receive timely updates on all the breakthrough technologies impacting human lives across the globe. Enjoy!");
        }

        [Test, Category("SignalPlus"), Category("Core")]
        [TestCase("en", TestName = "Verify subscription using an Email that is not yet registered to myAnalog but already subscribed to Content Hub", Category = "Smoke_ADIWeb")]
        public void SignalPlus_VerifySuccessfulSubscription3(string Locale)
        {
            string EmailAddress = util.Generate_EmailAddress();
            string initial_steps = "1. Go to " + Configuration.Env_Url + Locale + "/signals.html"
                            + "<br>2. Click Subscribe Tab"
                            + "<br>3. Type valid email address (" + EmailAddress + ")"
                            + "<br>4. Type First name"
                            + "<br>5. Type Last name"
                            + "<br>6. Check gdpr consent and communication checkbox"
                            + "<br>7. Click Submit button";

            string scenario = "Verify that user will be able to successfully subscribe to Signal+. Confirmation message must be displayed";
            action.INavigate(driver, Configuration.Env_Url + Locale + "/signals.html");
            Thread.Sleep(5000);
            action.IClick(driver, Elements.SignalPlus_Subscribe_Tab);
            action.IType(driver, Elements.SignalPlus_Subscribe_FName_Textfield, "Test Firstname");
            action.IType(driver, Elements.SignalPlus_Subscribe_LName_Textfield, "Test Lastname");
            action.IType(driver, Elements.SignalPlus_Subscribe_Email_Textfield, "test_11132020_09@mailinator.com");
            action.IClick(driver, Elements.SignalPlus_Subscribe_Consent_Checkbox);
            action.IClick(driver, Elements.SignalPlus_Subscribe_Communication_Checkbox);
            action.IClick(driver, Elements.SignalPlus_Subscribe_SubscribeToSignal_Button);

            test.validateStringIsCorrectv2(driver, Elements.SignalPlus_Subscribe_ErrorMessage_InvalidEmail, "Good news! You already have a Signals+ subscription. Watch for the next issue in your inbox.", scenario, initial_steps);
        }


        [Test, Category("SignalPlus"), Category("Core")]
        [TestCase("en", TestName = "Verify subscription using an Email that is registered to myAnalog and already subscribed to Content Hub")]
        public void SignalPlus_VerifySuccessfulSubscription4(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/signals.html");
            Thread.Sleep(5000);
            action.IClick(driver, Elements.SignalPlus_Subscribe_Tab);

            action.IType(driver, Elements.SignalPlus_Subscribe_FName_Textfield, "Test Firstname");
            action.IType(driver, Elements.SignalPlus_Subscribe_LName_Textfield, "Test Lastname");
            action.IType(driver, Elements.SignalPlus_Subscribe_Email_Textfield, "aries.sorosoro@analog.com");
            action.IClick(driver, Elements.SignalPlus_Subscribe_Consent_Checkbox);
            action.IClick(driver, Elements.SignalPlus_Subscribe_Communication_Checkbox);
            action.IClick(driver, Elements.SignalPlus_Subscribe_SubscribeToSignal_Button);
            test.validateStringIsCorrect(driver, Elements.SignalPlus_Subscribe_ErrorMessage_InvalidEmail, "Good news! You already have a Signals+ subscription. Watch for the next issue in your inbox.");
        }

        [Test, Category("SignalPlus"), Category("Core")]
        [TestCase("en", TestName = "Verify subscription using a Blocked Email")]
        public void SignalPlus_VerifyBlockedAccountSubscription(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/signals.html");
            Thread.Sleep(5000);
            action.IClick(driver, Elements.SignalPlus_Subscribe_Tab);

            action.IType(driver, Elements.SignalPlus_Subscribe_FName_Textfield, "Test Firstname");
            action.IType(driver, Elements.SignalPlus_Subscribe_LName_Textfield, "Test Lastname");
            action.IType(driver, Elements.SignalPlus_Subscribe_Email_Textfield, "blocked_email_for_ad@mailinator.com");
            action.IClick(driver, Elements.SignalPlus_Subscribe_Consent_Checkbox);
            action.IClick(driver, Elements.SignalPlus_Subscribe_Communication_Checkbox);
            action.IClick(driver, Elements.SignalPlus_Subscribe_SubscribeToSignal_Button);
            test.validateStringIsCorrect(driver, Elements.SignalPlus_Subscribe_ErrorMessage_InvalidEmail, "The email you provided has been blocked, but we may be able to help. Please contact us at external.webmaster@analog.com");
        }

        [Test, Category("SignalPlus"), Category("Core")]
        [TestCase("en", TestName = "Verify subscription using a Expired Email")]
        public void SignalPlus_VerifyExpiredAccountSubscription(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/signals.html");
            Thread.Sleep(5000);
            action.IClick(driver, Elements.SignalPlus_Subscribe_Tab);

            action.IType(driver, Elements.SignalPlus_Subscribe_FName_Textfield, "Test Firstname");
            action.IType(driver, Elements.SignalPlus_Subscribe_LName_Textfield, "Test Lastname");
            action.IType(driver, Elements.SignalPlus_Subscribe_Email_Textfield, "expired_email_for_ad@mailinator.com");
            action.IClick(driver, Elements.SignalPlus_Subscribe_Consent_Checkbox);
            action.IClick(driver, Elements.SignalPlus_Subscribe_Communication_Checkbox);
            action.IClick(driver, Elements.SignalPlus_Subscribe_SubscribeToSignal_Button);
            test.validateStringIsCorrect(driver, Elements.SignalPlus_Subscribe_ErrorMessage_InvalidEmail, "Your password has expired, but help is available. Please contact us at external.webmaster@analog.com");
        }
    }
}