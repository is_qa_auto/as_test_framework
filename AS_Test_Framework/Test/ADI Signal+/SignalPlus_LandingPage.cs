﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Threading;

namespace AS_Test_Framework.AdiSignalPlus
{

    [TestFixture]
    public class SignalPlus_LandingPage : BaseSetUp
    {
        public SignalPlus_LandingPage() : base() { }

        [Test, Category("SignalPlus"), Category("Core")]
        [TestCase("cn", TestName = "Verify that the language code on the content hub landing page is \"cn\", not \"zh\"")]
        public void SignalPlus_VerifySignalPlusCNLandingPage(string Locale)
        {
            string[] UrlToCheck = {
                                    "zh/signals/connectivity.html",
                                    "zh/signals/digital-health.html",
                                    "zh/signals/electrification.html",
                                    "zh/signals/smart-industry.html",
                                    "zh/signals.html",
                                  };
            for (int x = 0, y = 1; y <= UrlToCheck.Length; x++, y++)
            {
                action.Navigate(driver, Configuration.Env_Url + UrlToCheck[x]);
                test.validateScreenByUrl(driver, Configuration.Env_Url + UrlToCheck[x].Replace("zh", Locale));
            }
        }

        [Test, Category("SignalPlus"), Category("Core")]
        [TestCase("en","ARTICLE", "VIDEO", TestName = "Verify components of signal plus landing page - EN Locale", Category = "Smoke_ADIWeb")]
        [TestCase("cn", "文章", "视频", TestName = "Verify components of signal plus landing page - CN Locale", Category = "Smoke_ADIWeb")]
        [TestCase("jp", "記事", "ビデオ", TestName = "Verify components of signal plus landing page - JP Locale", Category = "Smoke_ADIWeb")]
        [TestCase("ru", "СТАТЬЯ", "ВИДЕО", TestName = "Verify components of signal plus landing page - RU Locale")]
        public void SignalPlus_VerifySignalPlusLandingPage(string Locale, string ArticleLabel, string VideoLabel)
        {
            string initial_steps = "1. Go to " + Configuration.Env_Url + Locale + "/signals.html";
            string scenario = "";

            action.INavigate(driver, Configuration.Env_Url + Locale + "/signals.html");

            /****BreadCrumbs******/
            scenario = "Verify that breadcrumbs is present in signal+ landing page";
            test.validateElementIsPresentv2(driver, Elements.Applications_BreadCrumbs,scenario, initial_steps);
            scenario = "Verify that Signal+ label is displayed in breadcrumbs";
            test.validateStringIsCorrectv2(driver, By.CssSelector("div[name='adi_breadcrumb'] li[class='active']>span"), "ADI Signals+", scenario, initial_steps);


            /****Short Header*****/
            scenario = "Verify that short header is present in signal+ landing page";
            test.validateElementIsPresentv2(driver, Elements.ShortHeader, scenario, initial_steps);

            /****Middle Row - Header****/
            scenario = "Verify that Analog Logo is present in signal+ landing page";
            test.validateElementIsPresentv2(driver, Elements.AnalogLog, scenario, initial_steps);

            scenario = "Verify that Global search textfield is present in signal+ landing page";
            test.validateElementIsPresentv2(driver, Elements.GlobalSearchTextField, scenario, initial_steps);

            scenario = "Verify that main menu navigation is present in signal+ landing page";
            test.validateElementIsPresentv2(driver, By.CssSelector("nav[class='navbar']"), scenario, initial_steps);

            /****Footer*****/
            scenario = "Verify that footer section is present in signal+ landing page";
            test.validateElementIsPresentv2(driver, Elements.Footer, scenario, initial_steps);

            /****Header Title/Logo******/
            scenario = "Verify that signal+ logo is present in signal+ landing page";
            test.validateElementIsPresentv2(driver, Elements.SignalPlus_Logo, scenario, initial_steps);

            /****Hero Banner*****/
            scenario = "Verify that hero banner is present in signal+ landing page";
            test.validateElementIsPresentv2(driver, Elements.SignalPlus_HeroBanner, scenario, initial_steps);

            /****Facet****/
            scenario = "Verify that Article filter button is present in signal+ landing page";
            test.validateElementIsPresentv2(driver, Elements.SignalPlus_ArticleFacet, scenario, initial_steps);

            scenario = "Verify that Video filter button is present in signal+ landing page";
            test.validateElementIsPresentv2(driver, Elements.SignalPlus_VideoFacet, scenario, initial_steps);

            scenario = "Verify that article section is present in signal+ landing page";
            test.validateElementIsPresentv2(driver, Elements.SignalPlus_ArticleSection, scenario, initial_steps);


            action.IClick(driver, Elements.SignalPlus_VideoFacet);
            Thread.Sleep(3000);
            scenario = "Verify that user can filter videos in signal+ landing page";
            test.validateStringIsCorrectv2(driver, By.CssSelector("div[class='container article-cards'] a span[class='article-cards__card__image__type']"), VideoLabel, scenario , initial_steps + "<br>2. Click Video filter button");

            action.IClick(driver, Elements.SignalPlus_VideoFacet);
            Thread.Sleep(3000);
            action.IClick(driver, Elements.SignalPlus_ArticleFacet);
            Thread.Sleep(3000);
            scenario = "Verify that user can filter articles in signal+ landing page";
            test.validateStringIsCorrectv2(driver, By.CssSelector("div[class='container article-cards'] a span[class='article-cards__card__image__type']"), ArticleLabel, scenario, initial_steps + "<br>2. Click Article filter button");

            string articlename = util.GetText(driver, By.CssSelector("div[class='container article-cards'] a>Div[class='article-cards__card__title']>p"));
            action.IClick(driver, By.CssSelector("div[class='container article-cards'] a"));
            scenario = "Verify that user will be redirected to article page when clicking article in signal+";
            test.validateStringInstancev2(driver, driver.Url, "/signals/articles",scenario, initial_steps + "<br>2. Click Article filter button<br>3. Click article - \"" + articlename + "\"");
            
        }

        [Test, Category("SignalPlus"), Category("Core")]
        [TestCase("en", TestName = "Verify that all facet is present and only displayed a maximum of 40 articles - not applicable for Subscribe")]
        public void SignalPlus_VerifyMaxArticle(string Locale)
        {
            action.Navigate(driver, Configuration.Env_Url + Locale + "/signals.html");
            string[] MenuNav = { "ALL", "CONNECTIVITY", "DIGITAL HEALTH", "ELECTRIFICATION", "SMART INDUSTRY", "SUBSCRIBE" };

            for (int y = 1, x = 0; util.CheckElement(driver, By.CssSelector("header[class='ch-masthead'] menu[class='ch-masthead__navigation__menu container']>li:nth-child(" + y + ")"), 5); y++, x++)
            {
                test.validateStringIsCorrect(driver, By.CssSelector("header[class='ch-masthead'] menu[class='ch-masthead__navigation__menu container']>li:nth-child(" + y + ")>a"), MenuNav[x]);
                action.IClick(driver, By.CssSelector("header[class='ch-masthead'] menu[class='ch-masthead__navigation__menu container']>li:nth-child(" + y + ")>a"));
                if (MenuNav[x].Equals("SUBSCRIBE"))
                {
                    test.validateElementIsPresent(driver, Elements.SignalPlus_Subscribe_ExpandedView);
                }
                else
                {
                    test.validateCountIsLessOrEqual(driver, util.GetCount(driver, By.CssSelector("div[class='container article-cards']>a")), 40);
                }
            }
        }
    }
}