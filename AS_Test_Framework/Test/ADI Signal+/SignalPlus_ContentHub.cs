﻿using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using NUnit.Framework;
using OpenQA.Selenium;

namespace AS_Test_Framework.AdiSignalPlus
{

    [TestFixture]
    public class SignalPlus_ContentHub : BaseSetUp
    {
        public SignalPlus_ContentHub() : base() { }

        [Test, Category("SignalPlus"), Category("Core")]
        [TestCase("en", TestName = "Verify Content Hub Article Template and Components - EN Locale")]
        [TestCase("cn", TestName = "Verify Content Hub Article Template and Components - CN Locale")]
        [TestCase("jp", TestName = "Verify Content Hub Article Template and Components - JP Locale")]
        [TestCase("ru", TestName = "Verify Content Hub Article Template and Components - RU Locale")]
        public void SignalPlus_VerifyContentHubComponent(string Locale)
        {            
            action.Navigate(driver, Configuration.Env_Url + Locale + "/signals/articles/combating-infectious-diseases.html");
            /******Hero Banner*****/
            test.validateElementIsPresent(driver, Elements.SignalPlus_ContentHub_HeroBanner);

            /******Back Link******/
            test.validateElementIsPresent(driver, By.CssSelector("div[class='article-secondary-nav'] div[class='col-md-8 col-xs-12']>h3>a"));
            action.IClick(driver, By.CssSelector("div[class='article-secondary-nav'] div[class='col-md-8 col-xs-12']>h3>a"));
            test.validateScreenByUrl(driver, Configuration.Env_Url + Locale + "/signals.html");
            driver.Navigate().Back();

        }

    }
}