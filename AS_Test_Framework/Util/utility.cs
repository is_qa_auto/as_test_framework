﻿using AS_Test_Framework.Element;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using System.Configuration;
using System;
using System.Net;
using System.Collections.Specialized;
using System.Net.Http;
using System.Linq;
using System.IO;
using System.Text;
using Newtonsoft.Json.Linq;
using System.Data.SqlClient;
using System.Data;
using System.Threading;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Globalization;

namespace AS_Test_Framework.Util
{
    public class Configuration
    {
        public static DataTable dtElements;

        private static IWebDriver driver;

        public static string Browser;

        public static string Environment;

        public static string Env_Url;

        public static string Sel_Host;

        public IWebDriver SeleniumSetup()
        {
            NameValueCollection node = (NameValueCollection)ConfigurationManager.GetSection("GridConfig");

            if (!node["run_in_jenkins"].Equals("y"))
            {
                Environment = node["environment"];
                Browser = node["browser"];
                Sel_Host = node["sel_host"];
            }
            else
            {
                Environment = System.Environment.GetEnvironmentVariable("AUTO_ENVIRONMENT");
                Browser = System.Environment.GetEnvironmentVariable("AUTO_BROWSER_TYPE");
                Sel_Host = System.Environment.GetEnvironmentVariable("AUTO_SEL_HOST");
            }

            if (Environment.Equals("production"))
            {
                Env_Url = "https://www.analog.com/";
            }
            else if (Environment.Equals("qa"))
            {
                Env_Url = "https://www-qa.cldnet.analog.com/";
            }
            else
            {
                Env_Url = "https://www-dev.cldnet.analog.com/";
            }

            switch (Browser)
            {
                case "Chrome":
                    ChromeOptions options = new ChromeOptions();
                    options.AddArguments("start-maximized");
                    options.AddArguments("--ignore-certificate-errors");
                    options.AddArguments("--disable-extensions");
                    options.AddArguments("--auto-open-devtools-for-tabs");
                    driver = new RemoteWebDriver(new Uri(Sel_Host), options);
                    return driver;

                case "Chrome_HL":
                    //ChromeOptions options2 = new ChromeOptions();
                    //options2.AddArguments("--headless");
                    //options2.AddArguments("--disable-gpu");
                    //options2.AddArguments("--disable-extensions");
                    //options2.AddArguments("--ignore-certificate-errors");
                    //options2.AddArguments("window-size=1280,1696");
                    driver = new ChromeDriver();
                    return driver;
            }
            return null;
        }

        public void GetWebRequest(string Url)
        {

            using (var client = new HttpClient())
            {
                var responseString = client.GetStringAsync(Url);
            }

            ////string html = string.Empty;
            //string url = @Url;

            //HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            ////request.AutomaticDecompression = DecompressionMethods.GZip;

            ////using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            ////using (Stream stream = response.GetResponseStream())
            ////using (StreamReader reader = new StreamReader(stream))
            ////{
            ////    html = reader.ReadToEnd();
            ////}

            ////Console.WriteLine(html);
        }

        public int GetWebSiteReponse(String Url)
        {
            int statusCode = 0;
            HttpWebResponse response = null;
            try
            {
                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Ssl3 | System.Net.SecurityProtocolType.Tls12 | System.Net.SecurityProtocolType.Tls11 | System.Net.SecurityProtocolType.Tls;
                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(Url);
                webRequest.AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip;
                webRequest.UserAgent = "User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.131 Safari/537.36";
                webRequest.Method = "GET";
                webRequest.AllowAutoRedirect = false;
                response = (HttpWebResponse)webRequest.GetResponse();

                statusCode = (int)response.StatusCode;

                response.Close();

                return statusCode;
            }
            catch (WebException wex)
            {
                response = (HttpWebResponse)wex.Response;
                statusCode = (int)response.StatusCode;

                response.Close();

                return statusCode;
            }
        }

        public bool CheckElement(IWebDriver driver, By element, int timeout)
        {
            try
            {
                //var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(30));
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(timeout));
                var myElement = wait.Until(x => x.FindElement(element));
                return myElement.Displayed;
            }
            catch
            {
                return false;
            }
        }

        public bool IsAscendingOrder(object[] value)
        {
            var orderedByAsc = value.OrderBy(d => d);

            bool sort = false;

            if (value.SequenceEqual(orderedByAsc))
            {
                sort = true;
            }

            return sort;
        }

        public bool IsDescendingOrder(object[] value)
        {
            var orderedByDsc = value.OrderByDescending(d => d);

            bool sort = false;

            if (value.SequenceEqual(orderedByDsc))
            {
                sort = true;
            }

            return sort;
        }

        public int GetCount(IWebDriver driver, By element)
        {

            int Element_Count = driver.FindElements(element).Count;

            return Element_Count;

        }

        public string GetText(IWebDriver driver, By element)
        {
            string Element_text = "";

            if (CheckElement(driver, element, 5))
            {
                Element_text = driver.FindElement(element).Text;
            }
            return Element_text;

        }

        //--- Get the Value in a Input Box ---//
        public string GetInputBoxValue(IWebDriver driver, By element)
        {
            string Element_Text = "";

            if (CheckElement(driver, element, 5))
            {
                Element_Text = ReturnAttribute(driver, element, "value");
            }

            return Element_Text;
        }

        //--- Get the Selected Value in a Dropdown ---//
        public string GetSelectedDropdownValue(IWebDriver driver, By element)
        {
            string Element_Text = "";

            if (CheckElement(driver, element, 5))
            {
                SelectElement dropdown = new SelectElement(driver.FindElement((element)));
                Element_Text = dropdown.SelectedOption.Text.Trim();
            }

            return Element_Text;
        }

        //--- Extract a Number from a String ---//
        public string ExtractNumber(IWebDriver driver, By element)
        {
            return Regex.Match(GetText(driver, element), @"\d+").Value;
        }

        public string ReturnAttribute(IWebDriver driver, By element, string AttributeToReturn)
        {

            string attributeValue = driver.FindElement(element).GetAttribute(AttributeToReturn);
            return attributeValue;

        }

        //--- Get the Type of Element / Tag Type ---//
        public string ReturnType(IWebDriver driver, By element)
        {
            string attributeValue = driver.FindElement(element).TagName;
            return attributeValue;
        }

        public string GetCssValue(IWebDriver driver, By element, string attribute)
        {
            string cssValue = null;

            if (CheckElement(driver, element, 5))
            {
                cssValue = driver.FindElement(element).GetCssValue(attribute);
            }

            return cssValue;
        }

        public string Generate_EmailAddress()
        {
            DateTime EstDateTime = DateTime.UtcNow;
            EstDateTime = TimeZoneInfo.ConvertTimeFromUtc(EstDateTime, TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time"));

            const string number = "1234567890";
            var random3 = new Random();
            string rand3 = new string(Enumerable.Repeat(number, 2)
              .Select(s => s[random3.Next(s.Length)]).ToArray());

            string Email_Address = "Test_Email_" + EstDateTime.ToString("Mdyyyy") + "_" + rand3 + "@mailinator.com";

            return Email_Address;
        }

        public string GetDataValue(string EnvironmentValue, string ColumnToSelect)
        {
            string cn = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

            SqlConnection myConnection = new SqlConnection(cn);
            myConnection.Open();
            SqlCommand myCommand = new SqlCommand("SELECT * FROM ebiz_user_account", myConnection);

            dtElements = new DataTable();
            dtElements.Load(myCommand.ExecuteReader());

            myConnection.Close();

            return dtElements.Select(string.Format("Environment = '{0}' AND Email_Address LIKE '{1}%'", EnvironmentValue, "Test_Auto"))[0][ColumnToSelect].ToString();
        }

        //--- Generate a Random Alphanumeric String ---//
        public string RandomString()
        {
            const string UCase = "ABCDEFGHIJKLMN";
            var random = new Random();
            string rand1 = new string(Enumerable.Repeat(UCase, 4)
              .Select(s => s[random.Next(s.Length)]).ToArray());

            const string Lcase = "abcdefghijklomn";
            var random2 = new Random();
            string rand2 = new string(Enumerable.Repeat(Lcase, 4)
              .Select(s => s[random2.Next(s.Length)]).ToArray());

            const string number = "1234567890";
            var random3 = new Random();
            string rand3 = new string(Enumerable.Repeat(number, 2)
              .Select(s => s[random3.Next(s.Length)]).ToArray());

            return rand1 + "_" + rand2 + rand3 + "!";
        }

        //--- Generate a Random 9-Character String ---//
        public string GenerateRandomString()
        {
            const string UCase = "ABCDEFGHIJKLMN";
            var random_upperCase = new Random();
            string rand_upperCase = new string(Enumerable.Repeat(UCase, 4)
              .Select(s => s[random_upperCase.Next(s.Length)]).ToArray());

            const string Lcase = "abcdefghijklomn";
            var random_lowerCase = new Random();
            string rand_lowerCase = new string(Enumerable.Repeat(Lcase, 4)
              .Select(s => s[random_lowerCase.Next(s.Length)]).ToArray());

            return rand_upperCase + "_" + rand_lowerCase;
        }

        //--- Generate a Random 3-Digit Number ---//
        public int GenerateRandomNumber()
        {
            Random rand = new Random();
            int random_number = rand.Next(100, 1000);

            return random_number;
        }

        public void UpdateDBData(string ColumnToUpdate, string NewValue, string EnvironmentValue)
        {
            string cn = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            //string cn = ConfigurationManager.ConnectionStrings["LocalConnection"].ConnectionString;

            SqlConnection myConnection = new SqlConnection(cn);
            myConnection.Open();
            SqlCommand myCommand = new SqlCommand("UPDATE ebiz_user_account SET " + ColumnToUpdate + " = '" + NewValue + "' where Environment = '" + EnvironmentValue + "'", myConnection);
            myCommand.ExecuteNonQuery();
            myConnection = null;
        }

        //----- SHOPPING CART > Remove All the Models in Your Cart Table -----//
        public void RemoveAllModelsInYourCartTable(IWebDriver driver)
        {
            for (int ctr = 2; CheckElement(driver, By.CssSelector("table[class='table table-bordered table-striped cart-table'] tr:nth-child(" + ctr + ")"), 5) != false;)
            {
                driver.FindElement(By.CssSelector("table[class='table table-bordered table-striped cart-table'] tr:nth-child(" + ctr + ")>td:nth-child(8) a")).Click();
                Thread.Sleep(4000);
            }
        }

        public Dictionary<string, string> GetAdobeTags(IWebDriver driver)
        {
            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            js.ExecuteScript("window.open(\"\",\"stats_debugger\",\"width=600,height=600,location=0,menubar=0,status=1,toolbar=0,resizable=1,scrollbars=1\").document.write(\"<script language='JavaScript' id=dbg  src='https://www.adobetag.com/d1/digitalpulsedebugger/live/DPD.js'></script>\");");
            Thread.Sleep(3000);

            var availableWindows = new List<string>(driver.WindowHandles);

            foreach (string w in availableWindows)
            {
                driver.SwitchTo().Window(w);
                Console.WriteLine(w);
                if (CheckElement(driver, By.CssSelector("input[name='auto_refresh']"), 1))
                {
                    break;
                }
            }

            driver.Navigate().Refresh();
            Thread.Sleep(3000);
            driver.FindElement(By.CssSelector("input[name='auto_refresh']")).Click();

            Dictionary<string, string> Analog_Locals = new Dictionary<string, string>();

            for (int x = 10; CheckElement(driver, By.CssSelector("tbody>tr * table:nth-child(2)>tbody>tr:nth-child(" + x + ")"), 1); x++)
            {
                if (!Analog_Locals.ContainsKey(GetText(driver, By.CssSelector("tbody>tr * table:nth-child(2)>tbody>tr:nth-child(" + x + ")>td"))))
                {
                    Analog_Locals.Add(GetText(driver, By.CssSelector("tbody>tr * table:nth-child(2)>tbody>tr:nth-child(" + x + ")>td")), GetText(driver, By.CssSelector("tbody>tr * table:nth-child(2)>tbody>tr:nth-child(" + x + ")>td:nth-child(2)")));
                }
            }

            return Analog_Locals;
        }

        public string GetAlertMessageThenAccept(IWebDriver driver)
        {

            string AlertMsg = "";

            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(20) /*timeout in seconds*/);
            if (wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.AlertIsPresent()) == null)
            {
                return AlertMsg = "No Alert Message";
            }
            else
            {
                try
                {
                    AlertMsg = driver.SwitchTo().Alert().Text;
                    driver.SwitchTo().Alert().Accept();
                    return AlertMsg;
                }
                catch (NoAlertPresentException)
                {
                    return AlertMsg = "No Alert Message";
                }
            }
        }

        public decimal ConverterToMetric(string value)
        {
            double covnerted_value = 0;
            string unit = value.Substring(value.Length - 1);

            if (unit.Equals("E"))
            {
                covnerted_value = Convert.ToDouble(value.Remove(value.Length - 1)) * 1000000000000000000;
            }

            else if (unit.Equals("P"))
            {
                covnerted_value = Convert.ToDouble(value.Remove(value.Length - 1)) * 1000000000000000;
            }

            else if (unit.Equals("T"))
            {
                covnerted_value = Convert.ToDouble(value.Remove(value.Length - 1)) * 1000000000000;
            }

            else if (unit.Equals("G"))
            {
                covnerted_value = Convert.ToDouble(value.Remove(value.Length - 1)) * 1000000000;
            }

            else if (unit.Equals("M"))
            {
                covnerted_value = Convert.ToDouble(value.Remove(value.Length - 1)) * 1000000;
            }

            else if (unit.Equals("k"))
            {
                covnerted_value = Convert.ToDouble(value.Remove(value.Length - 1)) * 1000;
            }

            else if (unit.Equals("h"))
            {
                covnerted_value = Convert.ToDouble(value.Remove(value.Length - 1)) * 100;
            }

            //else if (value.Remove(value.Length - 1).Substring(value.Remove(value.Length - 1).Length - 2).Equals("da"))
            //{
            //    covnerted_value = Convert.ToDouble(value.Remove(value.Length - 2)) * 10;
            //}

            else if (unit.Equals("d"))
            {
                covnerted_value = Convert.ToDouble(value.Remove(value.Length - 1)) * 0.1;
            }

            else if (unit.Equals("c"))
            {
                covnerted_value = Convert.ToDouble(value.Remove(value.Length - 1)) * 0.01;
            }

            else if (unit.Equals("m"))
            {
                covnerted_value = Convert.ToDouble(value.Remove(value.Length - 1)) * 0.001;
            }

            else if (unit.Equals("u"))
            {
                covnerted_value = Convert.ToDouble(value.Remove(value.Length - 1)) * 0.000001;
            }

            else if (unit.Equals("n"))
            {
                covnerted_value = Convert.ToDouble(value.Remove(value.Length - 1)) * 0.000000001;
            }

            else if (unit.Equals("p"))
            {
                covnerted_value = Convert.ToDouble(value.Remove(value.Length - 1)) * 0.000000000001;
            }

            else if (unit.Equals("f"))
            {
                covnerted_value = Convert.ToDouble(value.Remove(value.Length - 1)) * 0.000000000000001;
            }

            else if (unit.Equals("a"))
            {
                covnerted_value = Convert.ToDouble(value.Remove(value.Length - 1)) * 0.000000000000000001;
            }

            else
            {
                covnerted_value = Convert.ToDouble(value);
            }

            return Decimal.Parse(covnerted_value.ToString(), NumberStyles.AllowExponent | NumberStyles.AllowDecimalPoint);
        }

        public string GetAllValueInDropdownThenSelectOne(IWebDriver driver, By element, int Index)
        {
            string Selected = null;

            SelectElement select_element = new SelectElement(driver.FindElement(element));
            IList<IWebElement> options2 = select_element.Options;

            Selected = options2[Index].GetAttribute("value");

            return Selected;
        }

        //----- MYANALOG > Get the Count of the Created Support Tickets -----//
        public int GetMyAnalogCreatedSupportTicketsCount(IWebDriver driver)
        {
            //--- Pre-condition: The User must be already Logged In ---//

            //--- myAnalog Support Tickets URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + "en/app/support";

            if (!driver.Url.Equals(myAnalog_url) && !driver.Url.Equals(myAnalog_url + "#"))
            {
                //--- Action: Go to Support Tickets ---//
                driver.Navigate().GoToUrl(myAnalog_url);
            }

            int createdSupportTickets_count = 0;
            if (CheckElement(driver, Elements.MyAnalog_SupportTickets_Pagination, 2) && Convert.ToInt32(GetText(driver, Elements.MyAnalog_SupportTickets_Last_Page_Button)) > 1)
            {
                createdSupportTickets_count = Convert.ToInt32(GetText(driver, Elements.MyAnalog_SupportTickets_Last_Page_Button));
                createdSupportTickets_count = (createdSupportTickets_count - 1) * GetCount(driver, Elements.MyAnalog_SupportTickets_Created_SupportTickets);

                //--- Action: Click the Last Page Link ---//
                driver.FindElement(Elements.MyAnalog_SupportTickets_Last_Page_Button).Click();
                Thread.Sleep(1000);
            }

            int lastPage_createdSupportTickets_count = GetCount(driver, Elements.MyAnalog_SupportTickets_Created_SupportTickets);
            int actual_createdSupportTickets_count = createdSupportTickets_count + lastPage_createdSupportTickets_count;

            return actual_createdSupportTickets_count;
        }

        //----- MYANALOG > Get the Case Title of the First Ticket in the Created Support Ticket List -----//
        public string GetMyAnalogFirstCreatedSupportTicketCaseTitle(IWebDriver driver)
        {
            //--- Pre-condition: The User must be already Logged In ---//

            //--- myAnalog Support Tickets URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + "en/app/support";

            if (!driver.Url.Equals(myAnalog_url) && !driver.Url.Equals(myAnalog_url + "#"))
            {
                //--- Action: Go to myAnalog Support Tickets ---//
                driver.Navigate().GoToUrl(myAnalog_url);
            }

            string first_created_supportTicket_caseTitle = GetText(driver, Elements.MyAnalog_SupportTickets_First_Created_SupportTickets_CaseTitle);

            return first_created_supportTicket_caseTitle;
        }

        //----- MYANALOG > Get the Name of the First Project in the Project Listing -----//
        public string GetMyAnalogFirstProjectName(IWebDriver driver)
        {
            //--- Pre-condition: The User must be already Logged In ---//

            //--- myAnalog Projects URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + "en/app/projects";

            if (!driver.Url.Equals(myAnalog_url) && !driver.Url.Equals(myAnalog_url + "#"))
            {
                //--- Action: Go to myAnalog Projects ---//
                driver.Navigate().GoToUrl(myAnalog_url);
            }

            //--- Action: Get the Name of the First Project ---//
            string first_project_name = GetText(driver, Elements.MyAnalog_Projects_First_Project_Name_Link);

            return first_project_name;
        }

        //----- MYANALOG > Get the Name of the Last Project in the Project Listing -----//
        public string GetMyAnalogLastProjectName(IWebDriver driver)
        {
            //--- Pre-condition: The User must be already Logged In ---//

            //--- myAnalog Projects URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + "en/app/projects";

            if (!driver.Url.Equals(myAnalog_url) && !driver.Url.Equals(myAnalog_url + "#"))
            {
                //--- Action: Go to myAnalog Projects ---//
                driver.Navigate().GoToUrl(myAnalog_url);
            }

            if (CheckElement(driver, Elements.MyAnalog_Projects_Pagination, 2) && Convert.ToInt32(GetText(driver, Elements.MyAnalog_Projects_Last_Page_Button)) > 1)
            {
                //--- Action: Click the Last Page Link ---//
                driver.FindElement(Elements.MyAnalog_Projects_Last_Page_Button).Click();
                Thread.Sleep(1000);
            }

            //--- Action: Get the Name of the Last Project ---//
            string last_project_name = GetText(driver, Elements.MyAnalog_Projects_Last_Project_Name_Link);

            return last_project_name;
        }

        //----- MYANALOG > Get the Count of the Created Projects -----//
        public int GetMyAnalogCreatedProjectsCount(IWebDriver driver)
        {
            //--- Pre-condition: The User must be already Logged In ---//

            //--- myAnalog Projects URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + "en/app/projects";

            if (!driver.Url.Equals(myAnalog_url) && !driver.Url.Equals(myAnalog_url + "#"))
            {
                //--- Action: Go to myAnalog Projects ---//
                driver.Navigate().GoToUrl(myAnalog_url);
            }

            int createdProjects_count = 0;
            if (CheckElement(driver, Elements.MyAnalog_Projects_Pagination, 2) && Convert.ToInt32(GetText(driver, Elements.MyAnalog_Projects_Last_Page_Button)) > 1)
            {
                createdProjects_count = Convert.ToInt32(GetText(driver, Elements.MyAnalog_Projects_Last_Page_Button));
                createdProjects_count = (createdProjects_count - 1) * GetCount(driver, Elements.MyAnalog_Projects_Created_Projects);

                //--- Action: Click the Last Page Link ---//
                driver.FindElement(Elements.MyAnalog_Projects_Last_Page_Button).Click();
                Thread.Sleep(1000);
            }

            int lastPage_createdProjects_count = GetCount(driver, Elements.MyAnalog_Projects_Created_Projects);
            int actual_createdProjects_count = createdProjects_count + lastPage_createdProjects_count;

            return actual_createdProjects_count;
        }

        //----- MYANALOG > Get the Count of the Saved Products -----//
        public int GetMyAnalogSavedProductsCount(IWebDriver driver)
        {
            //--- Pre-condition: The User must be already Logged In ---//

            //--- myAnalog Products URL ---//
            string myAnalog_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + "en/app/products";

            if (!driver.Url.Equals(myAnalog_url) && !driver.Url.Equals(myAnalog_url + "#"))
            {
                //--- Action: Go to myAnalog Products ---//
                driver.Navigate().GoToUrl(myAnalog_url);
            }

            int savedProducts_count = 0;
            if (CheckElement(driver, Elements.MyAnalog_Products_Pagination, 2) && Convert.ToInt32(GetText(driver, Elements.MyAnalog_Products_Last_Page_Button)) > 1)
            {
                savedProducts_count = Convert.ToInt32(GetText(driver, Elements.MyAnalog_Products_Last_Page_Button));
                savedProducts_count = (savedProducts_count - 1) * GetCount(driver, Elements.MyAnalog_Products_Saved_Products);

                //--- Action: Click the Last Page Link ---//
                driver.FindElement(Elements.MyAnalog_Products_Last_Page_Button).Click();
                Thread.Sleep(2000);
            }
            int lastPage_savedProducts_count = GetCount(driver, Elements.MyAnalog_Products_Saved_Products);
            int actual_savedProducts_count = savedProducts_count + lastPage_savedProducts_count;

            return actual_savedProducts_count;
        }

        /*******This will return the current country of the user based on the cookie DISTI_COUNTRY_CODE****/
        /*****Created to handle change for AL-17509***/
        public string GetCurrentCountry(IWebDriver driver)
        {
            string CurrentCountry = driver.Manage().Cookies.GetCookieNamed("DISTI_COUNTRY_CODE").ToString().Split(';')[0].Split('=')[1];
            RegionInfo countryName = new RegionInfo(CurrentCountry);
            return countryName.DisplayName.ToUpper();
        }

        //---- Get the Element Type ----//
        public string GetElementType(IWebDriver driver, By element)
        {
            string element_type = ReturnType(driver, element);

            if (element_type.Equals("button"))
            {
                element_type = " Button";
            }
            else if (element_type.Equals("textarea"))
            {
                element_type = " Text Field";
            }
            else
            {
                element_type = ReturnAttribute(driver, element, "type");
                string element_class = ReturnAttribute(driver, element, "class");
                string element_href = ReturnAttribute(driver, element, "href");

                if (!string.IsNullOrEmpty(element_type))
                {
                    if (element_type.Equals("button") || element_type.Equals("submit"))
                    {
                        element_type = " Button";
                    }
                    else if (element_type.Equals("checkbox"))
                    {
                        element_type = " Checkbox";
                    }
                    else if (element_type.Equals("radio"))
                    {
                        element_type = " Radio Button";
                    }
                    else if (element_type.Equals("text") || element_type.Equals("email") || element_type.Equals("password"))
                    {
                        element_type = " Text Field";
                    }
                    else
                    {
                        element_type = "";
                    }
                }
                else if (!string.IsNullOrEmpty(element_class))
                {
                    if (element_class.Contains("select") || element_class.Contains("dropdown"))
                    {
                        element_type = " Dropdown";
                    }
                    if (element_class.Contains("button") || element_class.Contains("btn"))
                    {
                        element_type = " Button";
                    }
                    else
                    {
                        element_type = "";
                    }
                }
                else if (!string.IsNullOrEmpty(element_href))
                {
                    element_type = " Link";
                }
                else
                {
                    element_type = "";
                }
            }

            return element_type;
        }

        //---- Get the Element Text ----//
        public string GetElementText(IWebDriver driver, By element)
        {
            string element_type = GetElementType(driver, element);

            string element_text = GetText(driver, element);
            string element_placeholder = ReturnAttribute(driver, element, "placeholder");
            string element_value = ReturnAttribute(driver, element, "value");
            string element_name = ReturnAttribute(driver, element, "name");
            string element_id = ReturnAttribute(driver, element, "id");

            if (!string.IsNullOrEmpty(element_text))
            {
                if (element_text.Split('\n').Length > 1)
                {
                    //--- If the String has multiple lines, get only the 1st Line ---//
                    element_text = element_text.Split(new[] { '\r', '\n' }).FirstOrDefault();
                }

                element_text = element_text.Trim();
            }
            else if (!string.IsNullOrEmpty(element_placeholder))
            {
                element_text = element_placeholder.Trim();
            }
            else if ((!string.IsNullOrEmpty(element_value) && !element_value.Equals("on") && !element_value.Equals("true")) && !element_type.Contains("Text Field"))
            {
                element_text = element_value.Trim();
            }
            else if (!string.IsNullOrEmpty(element_name))
            {
                element_text = element_name.Trim();
            }
            else if (!string.IsNullOrEmpty(element_id))
            {
                element_text = element_id.Trim();
            }
            else
            {
                element_text = element.ToString().Substring(element.ToString().IndexOf(':') + 2) + " Element";
            }

            return element_text;
        }

        //----- SHOPPING CART > Get an ADI Model -----//
        public string GetAdiModel()
        {
            //--- Add Valid ADI Models here: ---//
            string[] adi_models = { "AD7960BCPZ", "AD7915BRMZ" };

            int ctr = 0;
            Random rand = new Random();

            ctr = rand.Next(0, adi_models.Length);
            string adi_model = adi_models[ctr];

            return adi_model;
        }

        //----- SHOPPING CART > Get an LTC Model -----//
        public string GetLtcModel()
        {
            //--- Add Valid LTC Models here: ---//
            string[] ltc_models = { "LTC4415EMSE#PBF", "LTC4420CDD#PBF", "LTC2360HS6#TRMPBF" };

            int ctr = 0;
            Random rand = new Random();

            ctr = rand.Next(0, ltc_models.Length);
            string ltc_model = ltc_models[ctr];

            return ltc_model;
        }

        //----- SHOPPING CART > Get an HMC Model -----//
        public string GetHmcModel()
        {
            //--- Add Valid HMC Models here: ---//
            string[] hmc_models = { "HMC1110" };

            int ctr = 0;
            //Random rand = new Random();

            //ctr = rand.Next(0, hmc_models.Length);
            string hmc_model = hmc_models[ctr];

            return hmc_model;
        }
    }
}