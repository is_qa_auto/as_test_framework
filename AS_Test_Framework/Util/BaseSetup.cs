﻿using AS_Test_Framework.Action;
using AS_Test_Framework.Validation;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using System;

namespace AS_Test_Framework.Util
{
    [Parallelizable]
    [TestFixture]
    public class BaseSetUp
    {
        protected IWebDriver driver;
        protected Configuration util;
        protected Actionkey action;
        protected ValidationClass test;

        public BaseSetUp()
        {
            this.util = new Configuration();
            this.action = new Actionkey();
            this.test = new ValidationClass();
        }


        [SetUp]
        public void Init()
        {
            driver = util.SeleniumSetup();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
        }

        [TearDown]
        public void Cleanup()
        {
            driver.Quit();
        }
    }
}
