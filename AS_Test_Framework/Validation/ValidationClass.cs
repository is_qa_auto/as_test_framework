﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using AS_Test_Framework.Util;
using OpenQA.Selenium.Support.UI;
using System.Drawing;

namespace AS_Test_Framework.Validation
{
    public class ValidationClass
    {
        Configuration util = new Configuration();

        //--- Validate if the Current URL is Correct ---//
        public void validateScreenByUrl(IWebDriver driver, String screenUrl)
        {
            //--- Note: screenUrl is the Expected URL ---//
            string currentUrl = driver.Url;

            try
            {
                Assert.AreEqual(screenUrl, currentUrl);
            }
            catch (AssertionException)
            {
                Console.WriteLine("Expected Result: Url must be \"" + screenUrl + "\"");
                Console.WriteLine("Actual Result: Url is \"" + currentUrl + "\"\n");
            };
        }
        public void validateScreenByUrlv2(IWebDriver driver, String screenUrl, string scenario, string steps)
        {
            //--- Note: screenUrl is the Expected URL ---//
            string currentUrl = driver.Url;

            try
            {
                Assert.AreEqual(screenUrl, currentUrl);
            }
            catch (AssertionException)
            {
                Console.WriteLine("<b>Scenario:</b> " + scenario
                    + "\n<b>Steps To Reproduce:</b> " + steps
                    + "\n<b>Expected Result:</b> User must be redirected to \"" + screenUrl + "\""
                    + "\n<b>Actual Result:</b> User was redirected to \"" + currentUrl + "\"\n");
            };
        }

        public void validateStringInstance(IWebDriver driver, String Expected_String, String String_Instance)
        {
            try
            {
                Assert.IsTrue(Expected_String.Contains(String_Instance));
            }
            catch (AssertionException)
            {
                Console.WriteLine("Expected Result: string \"" + String_Instance + "\" - must be present in " + Expected_String);
                Console.WriteLine("Actual Result: string \"" + String_Instance + "\" - cannot be found in " + Expected_String + "\n");
            };
        }

        public void validateStringInstancev2(IWebDriver driver, String Expected_String, String String_Instance, string scenario, string steps)
        {
            try
            {
                Assert.IsTrue(Expected_String.Contains(String_Instance));
            }
            catch (AssertionException)
            {
                Console.WriteLine("<b>Scenario:</b> " + scenario 
                              + "\n<b>Steps To Replicate:</b> " + steps
                              + "\n<b>Expected Result:</b> string \"" + String_Instance + "\" - must be present" 
                              + "\n<b>Actual Result:</b> string \"" + String_Instance + "\" - cannot be found in " + Expected_String + "\n");
            };
        }

        public void validateElementIsPresent(IWebDriver driver, By element)
        {
            try
            {
                Assert.IsTrue(util.CheckElement(driver, element, 5));
                Console.WriteLine("Expected Result: Element: " + element + " must be present\nActual Result: Element " + element + " is present\n");

            }
            catch (AssertionException)
            {
                Console.WriteLine("Expected Result: Element: " + element + " must be present\nActual Result: Element " + element + " is missing\n");
            };
        }

        public void validateElementIsPresentv2(IWebDriver driver, By element, string scenario, string steps)
        {
            try
            {
                Assert.IsTrue(util.CheckElement(driver, element, 5));
            }
            catch (AssertionException)
            {
                Console.WriteLine("<b>Scenario:</b> " + scenario 
                              + "\n<b>Steps To Reproduce:</b>\n" + steps 
                              + "\n<b>Expected Result:</b> Element - " + element + " must be present"
                              + "\n<b>Actual Result:</b> Element - " + element + " is missing\n");
            };
        }

        public void validateElementIsNotPresent(IWebDriver driver, By element)
        {
            try
            {
                Assert.IsFalse(util.CheckElement(driver, element, 1));
            }
            catch (AssertionException)
            {
                Console.WriteLine("\nExpected Result: Element: " + element + " must not be present\nActual Result: Element " + element + " is present");
            };
        }

        public void validateElementIsNotPresentv2(IWebDriver driver, By element, string scenario, string steps)
        {
            try
            {
                Assert.IsFalse(util.CheckElement(driver, element, 1));
            }
            catch (AssertionException)
            {
                Console.WriteLine("<b>Scenario:</b> " + scenario + "\n<b>Steps To Reproduce:</b>\n" + steps + "\n<b>Expected Result:</b> Element: " + element + " must not be present\n<b>Actual Result:</b> Element " + element + " is currently present");
            };
        }


        public void validateElementIsNotEnabled(IWebDriver driver, By element)
        {
            string element_text = util.GetElementText(driver, element);
            string element_type = util.GetElementType(driver, element);

            try
            {
                Assert.IsFalse(driver.FindElement(element).Enabled);
            }
            catch (AssertionException)
            {
                Console.WriteLine("\nExpected Result: Element: " + element_text + element_type + " must not be enabled\nActual Result: Element " + element_text + element_type + " is currently enabled");
            };
        }
        public void validateElementIsNotEnabledv2(IWebDriver driver, By element, string scenario, string steps)
        {
            try
            {
                Assert.IsFalse(driver.FindElement(element).Enabled);
            }
            catch (AssertionException)
            {
                Console.WriteLine("<b>Scenario:</b> " + scenario + "\n<b>Steps To Reproduce:</b>\n" + steps + "\n<b>Expected Result:</b> Element: " + element + " must not be enabled\n<b>Actual Result:</b> Element " + element + " is currently enabled\n");
            };
        }

        //--- Validate if the Element is Enabled ---//
        public void validateElementIsEnabled(IWebDriver driver, By element)
        {
            string element_text = util.GetElementText(driver, element);
            string element_type = util.GetElementType(driver, element);

            try
            {
                Assert.IsTrue(driver.FindElement(element).Enabled);
            }
            catch (AssertionException)
            {
                Console.WriteLine("\nExpected Result: Element: " + element_text + element_type + " must be enabled\nActual Result: Element " + element_text + element_type + " is currently disabled");
            };
        }

        public void validateElementIsEnabledv2(IWebDriver driver, By element, string scenario, string steps)
        {
            try
            {
                Assert.IsTrue(driver.FindElement(element).Enabled);
            }
            catch (AssertionException)
            {
                Console.WriteLine("<b>Scenario:</b> " + scenario 
                            +   "\n<b>Steps To Reproduce:</b> " + steps
                            +   "\n<b>Expected Result:</b> Element: " + element + " must be enabled"
                            +   "\n<b>Actual Result:</b> Element " + element + " is currently disabled\n");
            };
        }

        //--- Validate if the Value is Correct by Supplying the Locator ---//
        public void validateStringIsCorrect(IWebDriver driver, By element, string Expected)
        {
            IWebElement findElement = driver.FindElement(element);

            try
            {
                Assert.AreEqual(Expected, findElement.Text);
            }
            catch (AssertionException)
            {
                Console.WriteLine("\nExpected Result: Expected String must be: " + Expected + "\nActual Result: Actual String is " + findElement.Text + "\n");
            };
        }
        public void validateStringIsCorrectv2(IWebDriver driver, By element, string Expected, string scenario, string steps)
        {
            IWebElement findElement = driver.FindElement(element);

            try
            {
                Assert.AreEqual(Expected, findElement.Text);
            }
            catch (AssertionException)
            {
                Console.WriteLine("<b>Scenario:</b> " + scenario + "\n<b>Steps To Reproduce:</b>\n" + steps + "\n<b>Expected Result:</b> Expected label must be: " + Expected + "\n<b>Actual Result:</b> Actual label is " + findElement.Text + "\n");
            };
        }



        //--- Validate if the Selected Value in a Dropdown is Correct ---//
        public void validateSelectedValueIsCorrect(IWebDriver driver, By element, string Expected)
        {
            SelectElement dropdown = new SelectElement(driver.FindElement(element));

            try
            {
                Assert.AreEqual(Expected, dropdown.SelectedOption.Text.Trim());
            }
            catch (AssertionException)
            {
                Console.WriteLine("Expected Result: Selected Value must be " + Expected + "\nActual Result: Selected value is " + dropdown.SelectedOption.Text.Trim());
            }
        }

        public void validateSelectedValueIsCorrectv2(IWebDriver driver, By element, string Expected, string scenario, string steps)
        {
            SelectElement dropdown = new SelectElement(driver.FindElement(element));

            try
            {
                Assert.AreEqual(Expected, dropdown.SelectedOption.Text.Trim());
            }
            catch (AssertionException)
            {
                Console.WriteLine("<b>Scenario:</b> " + scenario 
                          + "\n<b>Steps To Reproduce: " + steps 
                          + "\n<b>Expected Result: </b> Selected Value must be " + Expected 
                          + "\n<b>Actual Result:</b> Selected value is " + dropdown.SelectedOption.Text.Trim() + "\n");
            }
        }

        //--- Validate if the Value is Correct by Supplying the Text ---//
        public void validateString(IWebDriver driver, string Expected, string Actual)
        {
            try
            {
                Assert.AreEqual(Expected, Actual);
            }
            catch (AssertionException)
            {
                Console.WriteLine("Expected Resul: String must be " + Expected + "\nActual Result is: Actual String is " + Actual + "\n");
            }
        }

        public void validateStringChanged(IWebDriver driver, string ActualValue, string PrevValue)
        {
            try
            {
                Assert.AreNotEqual(ActualValue, PrevValue);
            }
            catch (AssertionException)
            {
                Console.WriteLine("Expected Resul: String must not be " + PrevValue);
                Console.WriteLine("Actual Result: String is " + ActualValue);
            }
        }

        public void validateStringIfInAscenading(object[] value)
        {
            try
            {
                Assert.IsTrue(util.IsAscendingOrder(value));
            }
            catch (AssertionException)
            {
                Console.WriteLine("Expected Result: List must be in Ascending order\nActual Result: List is not in Ascending order\n");
            };
        }

        public void validateStringIfInDescending(object[] value)
        {
            try
            {
                Assert.IsTrue(util.IsDescendingOrder(value));
            }
            catch (AssertionException)
            {
                Console.WriteLine("Expected Result: List must be in Descending order\nActual Result: List is not in descending order\n");
            };
        }

        public void validateStringIfInDescendingv2(object[] value, string scenario, string steps)
        {
            try
            {
                Assert.IsTrue(util.IsDescendingOrder(value));
            }
            catch (AssertionException)
            {
                Console.WriteLine("<b>Scenario:</b> " + scenario
                            + "\n<b>Steps To Reproduce:</b> " + steps
                            + "\n<b>Expected Result:</b> List must be in Descending order"
                            + "\n<b>Actual Result:</b> List is not in descending order\n");
            };
        }

        public void validateElementIsSelected(IWebDriver driver, By element)
        {
            string element_text = util.GetElementText(driver, element);
            string element_type = util.GetElementType(driver, element);

            try
            {
                Assert.IsTrue(driver.FindElement(element).Selected);
            }
            catch (AssertionException)
            {
                Console.WriteLine("Expected Result: Element " + element_text + element_type + " must be selected\nActual Result: Element is not selected\n");
            };
        }

        public void validateElementIsNotSelected(IWebDriver driver, By element)
        {
            string element_text = util.GetElementText(driver, element);
            string element_type = util.GetElementType(driver, element);

            try
            {
                Assert.IsTrue(!driver.FindElement(element).Selected);
            }
            catch (AssertionException)
            {
                Console.WriteLine("Expected Result: Element " + element_text + element_type + " must not be selected\nActual Result: Element is selected\n");
            };
        }

        public void validateCountIsLess(IWebDriver driver, decimal Actual, decimal Expected)
        {
            try
            {
                Assert.Less(Actual, Expected);
            }
            catch (AssertionException)
            {
                Console.WriteLine("Expected Result: Count must be Less than " + Expected);
                Console.WriteLine("Actual Result: Count is " + Actual);
            }
        }

        public void validateCountIsLessOrEqual(IWebDriver driver, decimal Actual, decimal Expected)
        {
            try
            {
                Assert.LessOrEqual(Actual, Expected);
            }
            catch (AssertionException)
            {
                Console.WriteLine("Expected Result: Count must be Less than or Equal to " + Expected);
                Console.WriteLine("Actual Result: Count is " + Actual);
            }
        }

        public void validateCountIsEqual(IWebDriver driver, decimal Expected, decimal Actual)
        {
            try
            {
                Assert.AreEqual(Expected, Actual);
            }
            catch (AssertionException)
            {
                Console.WriteLine("Expected Result: Count must be: " + Expected + "\nActual Result: Count is " + Actual);
            };
        }

        public void validateCountIsEqualv2(IWebDriver driver, int Expected, int Actual, string scenario, string steps)
        {
            try
            {
                Assert.AreEqual(Expected, Actual);
            }
            catch (AssertionException)
            {
                Console.WriteLine("<b>Scenario:</b> " + scenario 
                    + "\n<b>Steps To Reproduce:</b> " + steps
                    + "\n<b>Expected Result:</b> Item count must be: " + Expected 
                    + "\n<b>Actual Result:</b> Item Count is " + Actual);
            };
        }

        public void validateCountIsGreaterOrEqual(IWebDriver driver, decimal Actual, decimal Expected)
        {
            try
            {
                Assert.GreaterOrEqual(Actual, Expected);
            }
            catch (AssertionException)
            {
                Console.WriteLine("Expected Result: Count must be Greater than or Equal to " + Expected);
                Console.WriteLine("Actual Result: Count is " + Actual);
            }
        }

        public void validateCountIsGreater(IWebDriver driver, decimal Actual, decimal Expected)
        {
            try
            {
                Assert.Greater(Actual, Expected);
            }
            catch (AssertionException)
            {
                Console.WriteLine("Expected Result: Count must be Greater than " + Expected);
                Console.WriteLine("Actual Result: Count is " + Actual);
            }
        }

        //--- Validate if the Selected Radio Button is Correct ---//
        public void validateSelectedRadioButtonIsCorrect(IWebDriver driver, By element, bool Expected)
        {
            bool Actual = driver.FindElement(element).Selected;
            string element_text = util.GetElementText(driver, element);
            string element_type = util.GetElementType(driver, element);

            try
            {
                Assert.AreEqual(Expected, Actual);
            }
            catch (AssertionException)
            {
                Console.WriteLine("Expected Result: Radio button " + element_text + element_type + " must be selected\nActual Result: Radio button is not selected\n");
            };
        }

        //--- Validate if the Textbox/Field is Not Empty ---//
        public void validateFieldIsNotEmpty(IWebDriver driver, By element)
        {
            string element_text = util.GetElementText(driver, element);
            string element_type = util.GetElementType(driver, element);

            try
            {
                Assert.IsNotEmpty(driver.FindElement(element).GetAttribute("value"));
            }
            catch (AssertionException)
            {
                Console.WriteLine("Expected Result: Field " + element_text + element_type + " must be pre-populated\nActual Result: Field is empty\n");
            }
        }

        //--- Validate if the Color is Correct ---//
        public void validateColorIsCorrect(IWebDriver driver, string Actual, string Expected)
        {
            //--- This is used when the Expected Result provided is a HEX Color ---//
            if (Expected.StartsWith("#"))
            {
                int[] Rgb_Color = Array.ConvertAll(Actual.Replace("rgba(", "").Replace("rgb(", "").Replace(")", "").Split(','), int.Parse);

                Color Actual_Color = Color.FromArgb(Rgb_Color[0], Rgb_Color[1], Rgb_Color[2]);

                string Actual_HexColor = Actual_Color.R.ToString("X2") + Actual_Color.G.ToString("X2") + Actual_Color.B.ToString("X2");

                if (!Actual_HexColor.StartsWith("#"))
                {
                    Actual_HexColor = "#" + Actual_HexColor;
                }

                try
                {
                    Assert.AreEqual(Expected.ToUpper(), Actual_HexColor.ToUpper());
                }
                catch (AssertionException)
                {
                    Console.WriteLine("Expected Result: Color must be set to " + Expected + "\nActual Result: Color set is " + Actual_HexColor + "\n");
                };

            }
            //--- This is used when the Expected Result provided is an RGB Color ---//
            else
            {
                try
                {
                    Assert.AreEqual(Expected, Actual);
                }
                catch (AssertionException)
                {
                    Console.WriteLine("Expected Result: Color must be set to " + Expected + "\nActual Result: Color set is " + Actual + "\n");
                };
            }
        }

        public void validateColorIsCorrectv2(IWebDriver driver, string Actual, string Expected, string scenario, string steps, By Element)
        {
            //--- This is used when the Expected Result provided is a HEX Color ---//
            if (Expected.StartsWith("#"))
            {
                int[] Rgb_Color = Array.ConvertAll(Actual.Replace("rgba(", "").Replace("rgb(", "").Replace(")", "").Split(','), int.Parse);

                Color Actual_Color = Color.FromArgb(Rgb_Color[0], Rgb_Color[1], Rgb_Color[2]);

                string Actual_HexColor = Actual_Color.R.ToString("X2") + Actual_Color.G.ToString("X2") + Actual_Color.B.ToString("X2");

                if (!Actual_HexColor.StartsWith("#"))
                {
                    Actual_HexColor = "#" + Actual_HexColor;
                }

                try
                {
                    Assert.AreEqual(Expected.ToUpper(), Actual_HexColor.ToUpper());
                }
                catch (AssertionException)
                {
                    Console.WriteLine("<b>Scenario:</b> " + scenario 
                                   + "\n<b>Steps To Reproduce: " + steps 
                                   + "\n<b>Expected Result:</b> Background-color of element " + Element + " must be set to " + Expected 
                                   + "\n<b>Actual Result:</b> Background-color of element " + Element + " is " + Actual_HexColor + "\n");
                };

            }
            //--- This is used when the Expected Result provided is an RGB Color ---//
            else
            {
                try
                {
                    Assert.AreEqual(Expected, Actual);
                }
                catch (AssertionException)
                {
                    Console.WriteLine("<b>Scenario:</b> " + scenario
                         + "\n<b>Steps To Reproduce: " + steps 
                         + "\n<b>Expected Result:</b> Background-color of element " + Element + " must be set to " + Expected 
                         + "\n<b>Actual Result:</b> Background-color of element " + Element + " is " + Actual + "\n");
                };
            }
        }


        //--- Validate if the Browser Tab / Window Title is Correct ---//
        public void validateWindowTitleIsCorrect(IWebDriver driver, string Expected)
        {
            try
            {
                Assert.IsTrue(driver.Title.Contains(Expected));
            }
            catch (AssertionException)
            {
                Console.WriteLine("Expected Result: Window Title must contain " + Expected);
                Console.WriteLine("Actual Result: Window Title is " + driver.Title);
            }
        }

        //--- Validate if an Alert Dialog Box is Present ---//
        public void validateAlertIsPresent(IWebDriver driver)
        {
            bool alert = false;

            try
            {
                driver.SwitchTo().Alert();
                driver.SwitchTo().Alert().Accept();

                alert = true;
            }
            catch (NoAlertPresentException)
            {
                alert = false;
            }

            try
            {
                Assert.IsTrue(alert);
            }
            catch (AssertionException)
            {
                Console.WriteLine("Expected Result: Alert must be Present");
                Console.WriteLine("Actual Result: Alert is Not Present");
            }
        }

        //--- Validate if the Canonical URL/Value is Correct ---//
        public void validateCanonicalUrl(IWebDriver driver, string Expected)
        {
            string Actual = driver.FindElement(By.CssSelector("link[rel='canonical']")).GetAttribute("href");

            try
            {
                Assert.AreEqual(Actual, Expected);
            }
            catch (AssertionException)
            {
                Console.WriteLine("Expected Result: Canonical URL must be " + Expected);
                Console.WriteLine("Actual Result: Canonical URL is " + Actual);
            }
        }

        public void validateImageIsNotBeBroken(IWebDriver driver, By element)
        {
            IWebElement ImageFile = driver.FindElement(element);

            Boolean ImagePresent = (Boolean)((IJavaScriptExecutor)driver).ExecuteScript("return arguments[0].complete && typeof arguments[0].naturalWidth != \"undefined\" && arguments[0].naturalWidth > 0", ImageFile);

            string element_text = util.GetElementText(driver, element);
            string element_type = util.GetElementType(driver, element);

            try
            {
                Assert.IsTrue(ImagePresent);
            }
            catch (AssertionException)
            {
                Console.WriteLine("Expected Result: " + element_text + element_type + " must Not be Broken");
                Console.WriteLine("Actual Result: " + element_text + element_type + " is Broken");
            }
        }
    }
}