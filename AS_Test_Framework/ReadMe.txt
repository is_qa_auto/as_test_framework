﻿Project Title: AS_Test_Framework
Description: AS_Test_Framework is a selenium webdriver test automation framework that test analog.com applications
Created and Maintained by Aries Sorosoro and Marvin Bebe
Created Date: FY20-21
Technology Used: Selenium Webdriver, Chromedriver, NUnit, .Net, Bitbucket, Jenkins