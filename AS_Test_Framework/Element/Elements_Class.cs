﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace AS_Test_Framework.Element
{
    public static class Elements
    {
        //----- HEADER -----//

        public static By Header = By.CssSelector("header[id='header']");

        public static By AnalogLog = By.CssSelector("header[id='header'] * img");

        //----- GLOBAL SEARCH BAR -----//

        public static By GlobalSearchTextField = By.CssSelector("input[id='text-global-search']");

        public static By GlobalSearch_ClearButton = By.CssSelector("button[data-qa-element='button_clear_search']");

        public static By GlobalSearch_SearchIcon = By.CssSelector("button[data-qa-element='button_submit_search']");

        public static By XRefLink = By.CssSelector("div[class='helpful-links']>a");

        public static By GlobalSearch_PredictiveSearch_Items = By.CssSelector("ul[class='auto-complete']>li");

        public static By GlobalSearch_PredictiveSearch_Items_ContentType_Label = By.CssSelector("span[class='search-content-type']");

        public static By GlobalSearch_PredictiveSearch_Items_Title_Link = By.CssSelector("ul[class='auto-complete']>li>a>span[class='search-content-title']");

        public static By GlobalSearch_PredictiveSearch_Items_ShortDescription_Text = By.CssSelector("p[class='search-short-description']");

        public static By GlobalSearch_PredictiveSearch_Search_Link = By.CssSelector("a[class^='search-link']");

        public static By Cart_Icon = By.CssSelector("a[class='cart']");

        public static By AD_Link = By.CssSelector("a[class='AnD-link']");

        public static By EZ_Link = By.CssSelector("a[class='ez-link']");

        public static By Wiki_Link = By.CssSelector("a[class='wiki-link']");

        public static By Careers_Link = By.CssSelector("a[name*='Careers']");

        public static By myAnalog_Link = By.CssSelector("li[class='myAnalogLink']>a");

        public static By Regional_Link = By.CssSelector("li[class='insert-language js-insert-language'] a[name*='Region']>span[class='arrow-down-transparent']");

        public static By Regional_Menu = By.CssSelector("li[class='insert-language js-insert-language open']>div[class='dropdown-menu']>ul:nth-child(1)");

        public static By Regional_Menu_India = By.CssSelector("li[class='insert-language js-insert-language open']>div[class='dropdown-menu']>ul * a[name*='India']");

        public static By Regional_Menu_Korea = By.CssSelector("li[class='insert-language js-insert-language open']>div[class='dropdown-menu']>ul * a[name*='Korea']");

        public static By Regional_Menu_Singapore = By.CssSelector("li[class='insert-language js-insert-language open']>div[class='dropdown-menu']>ul * a[name*='Singapore']");

        public static By Regional_Menu_Taiwan = By.CssSelector("li[class='insert-language js-insert-language open']>div[class='dropdown-menu']>ul * a[name*='Taiwan']");

        public static By Language_Menu = By.CssSelector("li[class='insert-language js-insert-language open']>div[class='dropdown-menu']>ul[class='setLang']");

        public static By Language_Menu_Chinese = By.CssSelector("li[class='insert-language js-insert-language open']>div[class='dropdown-menu']>ul[class='setLang']>li>a[name*='Chinese']");

        public static By Language_Menu_Japan = By.CssSelector("li[class='insert-language js-insert-language open']>div[class='dropdown-menu']>ul[class='setLang']>li>a[name*='Japanese']");

        public static By Language_Menu_Russian = By.CssSelector("li[class='insert-language js-insert-language open']>div[class='dropdown-menu']>ul[class='setLang']>li>a[name*='Russian']");

        public static By Main_Carousel = By.CssSelector("div[id='carousel-main-home']");

        public static By TextArea = By.CssSelector("textarea[id='feedback-text']");

        public static By MyAnalogDashboard = By.CssSelector("div[id='myAnalog-app']");

        public static By CartWrapper = By.CssSelector("div[id='divShoppingCartWrapper']");

        public static By CartRemoveBtn = By.CssSelector("a[id*='lbRemove']");

        public static By AddModelField = By.CssSelector("input[name*='txAddModel']");

        public static By SampleRadioBtn = By.CssSelector("input[id='Sample']");

        public static By AddModelBtn = By.CssSelector("input[id='btAddToCart']");

        public static By CheckOutBtn = By.CssSelector("input[value='Checkout']");

        public static By ShippingNextBtn = By.CssSelector("input[name='ctl09']");

        public static By BillingNextBtn = By.CssSelector("input[name='btnNext1']");

        public static By SampleUsage = By.CssSelector("select[id='ddlSampleUsageUS']");

        public static By DesignPhase = By.CssSelector("select[id='designphase']");

        public static By BestEstimate = By.CssSelector("select[id='bestestimate']");

        public static By ComponentQty = By.CssSelector("select[id='componentqty']");

        public static By AcceptSCTerms = By.CssSelector("input[id='chkTerms']");

        public static By PlaceOrderBtn = By.CssSelector("input[value='Place Order']");

        public static By SampleOrderNumber = By.CssSelector("a[id='hlSampleOrderNumber1']");

        public static By SearchTextField = By.CssSelector("input[id='text-global-search']");

        public static By SearchResult = By.CssSelector("div[class='search-results']");

        public static By Sales_Distri_RegionDD = By.CssSelector("select[id='_content_ddlSalesRegion']");

        public static By Sales_Distri_CountryDD = By.CssSelector("select[id='_content_ddlSalesCountry']");

        public static By Sales_Distri_ViewBtn = By.CssSelector("input[id='_content_btnSearchLocations']");

        public static By Sales_Distri_AddNewBtn = By.CssSelector("input[id='_content_btnAddLocation']");

        public static By Sales_Distri_SearchResultTbl = By.CssSelector("table[id='_content_grdManageSNDPortal']");

        public static By Sales_Distri_EditLink = By.CssSelector("a[id='_content_grdManageSNDPortal_lnkViewEdit1_0']");

        public static By Virtual_Url_List = By.CssSelector("select[id='ContentPlaceHolder1_lstWebsite']");

        public static By Virtual_Url_SubmitBtn = By.CssSelector("input[id='ContentPlaceHolder1_btnSubmit']");

        public static By Virtual_Url_SearchResult = By.CssSelector("table[id='ContentPlaceHolder1_grvUrlResults']");

        //----- SHOPPING CART -----//

        public static By Shopping_Cart_Breadcrumbs = By.CssSelector("div[class='breadcrumb adicrumb isible-desktop']");

        public static By SC_LoginLink = By.CssSelector("a[id='lnkMyAnalogLogin']");

        public static By Shopping_Cart_Country_Dropdown = By.CssSelector("span[id='spCountry']");

        public static By Shopping_Cart_SelectShippingCountry_Dropdown = By.CssSelector("div[id^='dropdownMenu']");

        public static By Shopping_Cart_SelectShippingCountry_Dropdown_SelectedVal = By.CssSelector("span[class='custom-selected-text']");

        public static By Shopping_Cart_SelectShippingCountry_ErrorMsg = By.CssSelector("span[id='lblSelectCountry']");

        public static By SC_ErrorMessage_Label = By.CssSelector("span[id='lblWarning']");

        public static By SC_Quantity_Textfield = By.CssSelector("input[id='rptProduct_ctl01_txtBuyQuantity']");

        public static By SC_ErrorMessage_ProductPage_Link = By.CssSelector("span[id='lblWarning']>a");

        public static By SC_SampleAbuse_WarningMessage_Label = By.CssSelector("span[id='lblSampleAbuseWarning']");

        public static By SC_QtyCol_ErrorMessage_Line1_Label = By.CssSelector("span[id$='lblErrorMessage']");

        public static By SC_QtyCol_ErrorMessage_Line2_Label = By.CssSelector("span[id$='lblErrorMessage2']");

        public static By Shopping_Cart_Error_Message_For_Using_Block_Domain = By.CssSelector("span[id='lblSampleAbuseWarning']");

        public static By Shopping_Cart_ViewSalesAndDistributorMap_Link = By.CssSelector("span>a[href$='/sales-distribution.html']");

        public static By Shopping_Cart_AddedModel = By.CssSelector("table[class$='cart-table'] * tr:nth-last-of-type(2) * a[id$='ModelNbr']");

        public static By Shopping_Cart_Package_Value = By.CssSelector("table[class$='cart-table'] * tr:nth-last-of-type(2) * span[id$='PackageDescription']");

        public static By Shopping_Cart_MultipleQuantity_Label = By.CssSelector("table[class$='cart-table'] * tr:nth-last-of-type(2) * span[id$='lbUnits']");

        public static By Shopping_Cart_ShwCompanionProducts_Link = By.CssSelector("a[id$='CompanionProducts']");

        public static By Shopping_Cart_ShwCompanionProducts_LtBx = By.CssSelector("div[id$='ShoppingCart'] * div[id='companion-products']>div");

        public static By Shopping_Cart_Qty_Tbx = By.CssSelector("input[name$='Quantity']");

        public static By Shopping_Cart_BuyAddtl_Link = By.CssSelector("td[id$='Quantity'] * a:nth-last-of-type(1)");

        public static By Shopping_Cart_LogIn_Link = By.CssSelector("td[id$='Quantity'] * a[onclick*='Login']");

        public static By Shopping_Cart_ReqQte_Link = By.CssSelector("td[id$='Quantity'] * span[id$='spnError'] * a");

        public static By Shopping_Cart_SubTot_Text = By.CssSelector("span[id$='TotalPrice']");

        public static By Shopping_Cart_Update_Btn = By.CssSelector("input[id$='Update']");

        public static By Shopping_Cart_Remove_Btn = By.CssSelector("a[id$='lbRemove']");

        public static By Shopping_Cart_VShippingOptsAndEstShippingC_Link = By.CssSelector("span[id$='ViewShippingOptions']>a");

        public static By Shopping_Cart_Add_Model_Textfield = By.CssSelector("input[id*='txAddModel']");

        public static By Shopping_Cart_Purchase_Radio_Button = By.CssSelector("input[id$='Purchase']");

        public static By Shopping_Cart_Sample_Radio_Button = By.CssSelector("input[id$='Sample']");

        public static By Shopping_Cart_Add2Cart_Button = By.CssSelector("input[id='btAddToCart']");

        public static By Shopping_Cart_Checkout_Button = By.CssSelector("input[id$='Button1']");

        public static By Shopping_Cart_AddToCart_Sec = By.CssSelector("div[id='divEmptyCart']");

        public static By Shopping_Cart_FindMoreProducts_Btn = By.CssSelector("div[class='form-group text-right col-right']>input:nth-last-child(1)");

        public static By Shopping_Cart_CS_Link = By.CssSelector("div[class='contact-customer-service-wrapper']>a");

        public static By Shopping_Cart_AbtBuyingOL_Txt = By.CssSelector("div[id='divAboutBuying']>p");

        public static By Shopping_Cart_AbtBuyingOL_SelectedCountriesRgn_Link = By.CssSelector("div[id='divAboutBuying']>p>a:nth-of-type(1)");

        public static By Shopping_Cart_AbtBuyingOL_ClickHere_Link = By.CssSelector("div[id='divAboutBuying']>p>a:nth-of-type(2)");

        public static By Shopping_Cart_AbtSamplingOL_Txt = By.CssSelector("div[id='divAboutSamples']>p");

        public static By Shopping_Cart_AbtSamplingOL_SelectedCountriesRgn_Link = By.CssSelector("div[id='divAboutSamples']>p>a:nth-of-type(1)");

        public static By Shopping_Cart_AbtSamplingOL_AuthDistr_Link = By.CssSelector("div[id='divAboutSamples']>p>a:nth-last-of-type(2)");

        public static By Shopping_Cart_AbtSamplingOL_ClickHere_Link = By.CssSelector("div[id='divAboutSamples']>p>a:nth-last-of-type(1)");

        public static By Shopping_Cart_CBEC_PopUp = By.CssSelector("div[id='countryModal'] * div[class='modal-content']");

        public static By Shopping_Cart_CBEC_Link1 = By.CssSelector("div[id='countryModal'] * div[class='modal-body']>p>a:nth-of-type(1)");

        public static By Shopping_Cart_CBEC_Link2 = By.CssSelector("div[id='countryModal'] * div[class='modal-body']>p>a:nth-of-type(1)");

        public static By Shopping_Cart_CBEC_Link3 = By.CssSelector("div[id='countryModal'] * div[class='modal-body']>p>a:nth-of-type(1)");

        public static By Shopping_Cart_CBEC_Close_Btn = By.CssSelector("div[id='countryModal'] * button");

        public static By Shopping_Cart_Caution_PopUp = By.CssSelector("div[id='cautionModal'] * div[class='modal-content']");

        public static By Shopping_Cart_TaxExempt_Sec = By.CssSelector("div[id='divNewUSTax']");

        public static By Shopping_Cart_Taxable_Rb = By.CssSelector("input[id='rbNewTaxable']");

        public static By Shopping_Cart_TaxExpt_Rb = By.CssSelector("input[id$='NonTaxable']");

        public static By Shopping_Cart_TaxExptNotice_LtBx = By.CssSelector("div[id='myModal'] * div[class='modal-content']");

        public static By Shopping_Cart_TaxExptNotice_LtBx_X_Btn = By.CssSelector("div[id='myModal'] * button[class='close']");

        public static By Shopping_Cart_TaxExptNotice_LtBx_ChangeTaxStat_Btn = By.CssSelector("div[id='myModal'] * div[class='modal-footer']>button:nth-of-type(1)");

        public static By Shopping_Cart_TaxExptNotice_LtBx_KeepTaxExemptStatAndCont_Btn = By.CssSelector("div[id='myModal'] * div[class='modal-footer']>button:nth-of-type(2)");

        public static By Shopping_Cart_ShippingAddress_Heading_BackToSC_Button = By.CssSelector("button[class='btn btn-primary pull-right']");

        public static By Shopping_Cart_ShippingAddress_Footer_BackToSC_Button = By.CssSelector("div[class='panel-footer'] * input[class='btn btn-primary']");

        public static By Shopping_Cart_Heading_Next_Button = By.CssSelector("div[class='panel-heading'] * input[class='btn btn-primary pull-right']");

        public static By Shopping_Cart_Footer_Next_Button = By.CssSelector("input[id^='btnNext']");

        public static By Shopping_Cart_Prev_Button = By.CssSelector("div[class$='addtocart-form'] button[class^='btn btn-primary']");

        public static By Shopping_Cart_ShippingAddress_Message = By.CssSelector("span[id*='Subheader']");

        public static By Shopping_Cart_ShippingAddress_PrevAdd_Sec = By.CssSelector("div[class*='previous-address']");

        public static By Shopping_Cart_ShippingAddress_PrevAdd_OrgName_Value = By.CssSelector("span[id='lbSelectedOrgName']");

        public static By Shopping_Cart_ShippingAddress_PrevAdd_City_Value = By.CssSelector("span[id='lbSelectedCity']");

        public static By Shopping_Cart_ShippingAddress_Delete_Btn = By.CssSelector("input[id='btnDeleteSelectedAddress']");

        public static By Shopping_Cart_ShippingAddress_Edit_Btn = By.CssSelector("input[id='btnEditAddress']");

        public static By Shopping_Cart_ShippingAddress_NewAdd_Sec = By.CssSelector("div[id='updNewAddress']");

        public static By Shopping_Cart_OrgName_Tbx = By.CssSelector("input[id$='OrgName']");

        public static By Shopping_Cart_OrgName_Note_Txt = By.CssSelector("span[id='Label2']");

        public static By Shopping_Cart_ShippingAddress_OrgName_ErrMsg = By.CssSelector("span[id='rfvCompanyName']>span");

        public static By Shopping_Cart_BillingAddress_OrgName_ErrMsg = By.CssSelector("span[id$='OrgName']");

        public static By Shopping_Cart_FirstName_Tbx = By.CssSelector("input[id$='FirstName']");

        public static By Shopping_Cart_ShippingAddress_FirstName_ErrMsg = By.CssSelector("span[id='RequiredFieldValidator1']>span");

        public static By Shopping_Cart_BillingAddress_FirstName_ErrMsg = By.CssSelector("span[id$='FirstName']");

        public static By Shopping_Cart_LastName_Tbx = By.CssSelector("input[id$='LastName']");

        public static By Shopping_Cart_ShippingAddress_LastName_ErrMsg = By.CssSelector("span[id='RequiredFieldValidator2']>span");

        public static By Shopping_Cart_BillingAddress_LastName_ErrMsg = By.CssSelector("span[id$='LastName']");

        public static By Shopping_Cart_AddressLine1_Tbx = By.CssSelector("div[id$='Address'] * input[id$='Address1']");

        public static By Shopping_Cart_ShippingAddress_AddressLine1_ErrMsg = By.CssSelector("span[id='RequiredFieldValidator8']>span");

        public static By Shopping_Cart_BillingAddress_AddressLine1_ErrMsg = By.CssSelector("span[id$='Address']");

        public static By Shopping_Cart_AddressLine2_Tbx = By.CssSelector("div[id$='Address'] * input[id$='Address2']");

        public static By Shopping_Cart_AddressLine3_Tbx = By.CssSelector("div[id$='Address'] * input[id$='Address3']");

        public static By Shopping_Cart_City_Tbx = By.CssSelector("input[id$='City']");

        public static By Shopping_Cart_ShippingAddress_City_ErrMsg = By.CssSelector("span[id='RequiredFieldValidator7']>span");

        public static By Shopping_Cart_BillingAddress_City_ErrMsg = By.CssSelector("span[id$='City']");

        public static By Shopping_Cart_StateProvince_Dd = By.CssSelector("select[id$='State']");

        public static By Shopping_Cart_ShippingAddress_StateProvince_ErrMsg = By.CssSelector("span[id='RequiredFieldValidator6']>span");

        public static By Shopping_Cart_BillingAddress_StateProvince_ErrMsg = By.CssSelector("span[id$='State']");

        public static By Shopping_Cart_ZipPostalCode_Tbx = By.CssSelector("input[id$='Zipcode']");

        public static By Shopping_Cart_ShippingAddress_ZipPostalCode_ErrMsg = By.CssSelector("span[id='RequiredFieldValidator5']>span");

        public static By Shopping_Cart_BillingAddress_ZipPostalCode_ErrMsg = By.CssSelector("span[id$='Zip']");

        public static By Shopping_Cart_CountryRegion_Dd = By.CssSelector("select[id$='Country']");

        public static By Shopping_Cart_ShippingAddress_CountryRegion_ErrMsg = By.CssSelector("span[id='RequiredFieldValidator4']>span");

        public static By Shopping_Cart_Telephone_Tbx = By.CssSelector("input[id$='Phone']");

        public static By Shopping_Cart_ShippingAddress_Telephone_ErrMsg = By.CssSelector("span[id='RequiredFieldValidator3']>span");

        public static By Shopping_Cart_BillingAddress_Telephone_ErrMsg = By.CssSelector("span[id$='Phone']");

        public static By Shopping_Cart_VatNumber_Required_Icon = By.CssSelector("label[for*='NewVAT']>span[class='required']");

        public static By Shopping_Cart_ShippingAddress_ShipToThisAdd_Btn = By.CssSelector("input[id='btnShipToNewAddress']");

        public static By Shopping_Cart_ShippingAddress_ShipToThisEditedAdd_Btn = By.CssSelector("input[id*='ShipToEditedAddress']");

        public static By Shopping_Cart_ShippingAddress_TaxOption_ErrMsg = By.CssSelector("span[id='spanTax']");

        public static By Shopping_Cart_CustomerService_Link = By.CssSelector("a[class='customer-service']");

        public static By Shopping_Cart_EditShippingAddress_Cancel_Button = By.CssSelector("input[id^='btnCancel']");

        public static By Shopping_Cart_EditShippingAddress_Save_Button = By.CssSelector("input[id^='btnSave']");

        public static By Shopping_Cart_BillingAddress_UseMyShippingAddAsMyBillingAdd_Cb = By.CssSelector("input[id='chkSameAsShipping']");

        public static By Shopping_Cart_BillingAddress_Disclaimer_Txt = By.CssSelector("div[id='divRepeatCustomer']>p");

        public static By Shopping_Cart_AboutYourProject_AbtYourWorkOrStudies_Sec = By.CssSelector("div[id='divSampleNotChina']");

        public static By Shopping_Cart_AboutYourProject_SampleUsageUS_Dd = By.CssSelector("select[id='ddlSampleUsageUS']");

        public static By Shopping_Cart_AboutYourProject_OrgUrl_Txt = By.CssSelector("div[id='divURL']>label");

        public static By Shopping_Cart_AboutYourProject_OrgUrl_Tbx = By.CssSelector("input[id='orzurl']");

        public static By Shopping_Cart_AboutYourProject_PrefDistributor_Txt = By.CssSelector("div[id='divSampleNotChina'] * div[class^='container']>div:nth-child(3)>label");

        public static By Shopping_Cart_AboutYourProject_PrefDistributor_Dd = By.CssSelector("select[id='distributor']");

        public static By Shopping_Cart_AboutYourProject_Distributor_Dd = By.CssSelector("select[id='distributor']");

        public static By Shopping_Cart_AboutYourProject_SamplesQuestions_Sec = By.CssSelector("div[id='divSamplesQuestions']");

        public static By Shopping_Cart_AboutYourProject_Occupation_Dd = By.CssSelector("div[id='application_pod1']>button");

        public static By Shopping_Cart_AboutYourProject_Occupation_ErrMsg = By.CssSelector("span[id$='Occupation']");

        public static By Shopping_Cart_AboutYourProject_DesignPhase_Dd = By.CssSelector("select[id='designphase']");

        public static By Shopping_Cart_AboutYourProject_DesignPhase_ErrMsg = By.CssSelector("span[id$='DesignPhase']");

        public static By Shopping_Cart_AboutYourProject_BestEstimate_Dd = By.CssSelector("select[id='bestestimate']");

        public static By Shopping_Cart_AboutYourProject_BestEstimate_ErrMsg = By.CssSelector("span[id$='BestEstimate']");

        public static By Shopping_Cart_AboutYourProject_ComponentQty_Dd = By.CssSelector("select[id='componentqty']");

        public static By Shopping_Cart_AboutYourProject_ComponentQty_ErrMsg = By.CssSelector("span[id$='ComponentQty']");

        public static By Shopping_Cart_AboutYourProject_EndUse_Dd = By.CssSelector("div[id='application_pod']>button");

        public static By Shopping_Cart_AboutYourProject_EndUse_ErrMsg = By.CssSelector("span[id$='AppSelected']");

        public static By Shopping_Cart_AboutYourProject_NonUSQuestions_Sec = By.CssSelector("div[id='divNonUSQuestions']");

        public static By Shopping_Cart_AboutYourProject_Nuclear_Rbs = By.CssSelector("input[type='radio'][id^='nuclear']");

        public static By Shopping_Cart_AboutYourProject_Nuclear_No_Rb = By.CssSelector("input[id='nuclear_0']");

        public static By Shopping_Cart_AboutYourProject_Nuclear_Yes_Rb = By.CssSelector("input[id='nuclear_1']");

        public static By Shopping_Cart_AboutYourProject_ReExporting_Rbs = By.CssSelector("input[type='radio'][id^='reexporting']");

        public static By Shopping_Cart_AboutYourProject_ReExporting_No_Rb = By.CssSelector("input[id='reexporting_0']");

        public static By Shopping_Cart_AboutYourProject_ReExporting_Yes_Rb = By.CssSelector("input[id='reexporting_1']");

        public static By Shopping_Cart_AboutYourProject_ReExporting_ErrMsg = By.CssSelector("span[id$='Reexporting']");

        public static By Shopping_Cart_AboutYourProject_ReExportingCountry_Txt = By.CssSelector("div[id='reexportingcountry']>label");

        public static By Shopping_Cart_AboutYourProject_ReExportingCountry_Dd = By.CssSelector("select[id='ddlCountry']");

        public static By Shopping_Cart_PlaceOrder_Step_Lbl = By.CssSelector("div[class='adi-progress samples-progress']>div:nth-last-child(1)>span");

        public static By Shopping_Cart_PlaceOrder_ShippingMethod_Dd = By.CssSelector("select[id='ddlShippingMethod']");

        public static By Shopping_Cart_PlaceOrder_ShippingMethod_ErrMsg = By.CssSelector("span[id='spanDdlShippingMethod']");

        public static By Shopping_Cart_PlaceOrder_ViewShippingOptionsAndEstimatedShippingCosts_Link = By.CssSelector("span[id='lblShippingOptns']>a");

        public static By Shopping_Cart_PlaceOrder_OfferCode_Tbx = By.CssSelector("input[id='txtOfferCode']");

        public static By Shopping_Cart_PlaceOrder_Apply_Btn = By.CssSelector("input[id='btnApply']");

        public static By Shopping_Cart_PlaceOrder_InvalidOfferCode_ErrMsg = By.CssSelector("span[id='lbInvalidOfferCode']");

        public static By Shopping_Cart_PlaceOrder_YourShoppingCart_Tbl = By.CssSelector("table[class$='cart-table']");

        public static By Shopping_Cart_PlaceOrder_ProductNo_Link = By.CssSelector("a[id$='ModelNbr']");

        public static By Shopping_Cart_PlaceOrder_EditCart_Btn = By.CssSelector("div[class='container']>input");

        public static By Shopping_Cart_PlaceOrder_Payment_Sec = By.CssSelector("div[id='divPayment']");

        public static By Shopping_Cart_PlaceOrder_Submitted_Cc_Sec = By.CssSelector("div[class$='payment-summary']");

        public static By Shopping_Cart_PlaceOrder_Cc_Sec = By.CssSelector("div[id='paymentDetails']");

        public static By Shopping_Cart_PlaceOrder_Cc_CardNo_Tbx = By.CssSelector("input[formcontrolname='cardNumber']");

        public static By Shopping_Cart_PlaceOrder_Cc_InvalidCardNo_ErrMsg = By.CssSelector("span[class$='error-invalidccnum-default']");

        public static By Shopping_Cart_PlaceOrder_Cc_CardHolderName_Tbx = By.CssSelector("input[formcontrolname='cardholderName']");

        public static By Shopping_Cart_PlaceOrder_Cc_Expiry_Tbx = By.CssSelector("input[ccdate='cardExpiration']");

        public static By Shopping_Cart_PlaceOrder_Cc_Cvv_Tbx = By.CssSelector("input[formcontrolname='cardCvv']");

        public static By Shopping_Cart_PlaceOrder_Cc_InvalidCvv_ErrMsg = By.CssSelector("span[class$='error-invalidcvv-default']");

        public static By Shopping_Cart_PlaceOrder_Cc_Submit_Btn = By.CssSelector("span[class='rp-label-default']");

        public static By Shopping_Cart_PlaceOrder_PaymentDetails_ErrMsg = By.CssSelector("span[id$='PaymentDetails']");

        public static By Shopping_Cart_PlaceOrder_TermsAndConditions_Link = By.CssSelector("a[href$='terms-and-conditions.html']");

        public static By Shopping_Cart_PlaceOrder_tNC_ErrMsg = By.CssSelector("span[id='spanTnC']");

        public static By Shopping_Cart_OrderConfirmation_OrderRefNos = By.CssSelector("a[id*='OrderNumber']");

        public static By Shopping_Cart_OrderConfirmation_SamplesOnly_Msg = By.CssSelector("div[id$='SamplesOnly']>p");

        public static By Shopping_Cart_OrderConfirmation_Sample_OrderRefNos = By.CssSelector("div[id$='SamplesOnly'] * a");

        public static By Shopping_Cart_OrderConfirmation_Sample_OrderRefNo = By.CssSelector("a[id*='SampleOrderNumber']");

        public static By Shopping_Cart_OrderConfirmation_PurchaseOnly_Msg = By.CssSelector("div[id$='PurchaseOnly']>p");

        public static By Shopping_Cart_OrderConfirmation_Purchase_OrderRefNos = By.CssSelector("div[id$='PurchaseOnly'] * a");

        public static By Shopping_Cart_OrderConfirmation_Purchase_OrderRefNo = By.CssSelector("a[id*='PurchaseOrderNumber']");

        public static By Shopping_Cart_OrderConfirmation_Mixed_Msg = By.CssSelector("div[id='divMixedOrder']>p:nth-child(1)");

        public static By Shopping_Cart_OrderConfirmation_Mixed_SampledItems_Msg = By.CssSelector("div[id='divMixedOrder']>p:nth-child(2)");

        public static By Shopping_Cart_OrderConfirmation_Mixed_PurchasedItems_Msg = By.CssSelector("div[id='divMixedOrder']>p:nth-child(3)");

        public static By Shopping_Cart_OrderConfirmation_Ltc_Mixed_OrderRefNos = By.CssSelector("div[id$='LtcMixedOrder'] * a");

        public static By Shopping_Cart_OrderConfirmation_ViewOrderStatus_Link = By.CssSelector("a[id$='OrderStatus']");

        public static By Shopping_Cart_OrderConfirmation_BillingAddress_OrgName_Value = By.CssSelector("span[id$='BillingOrgName']");

        public static By Shopping_Cart_OrderConfirmation_ShippingAddress_OrgName_Value = By.CssSelector("span[id$='ShippingOrgName']");

        public static By Shopping_Cart_OrderConfirmation_EmailAddress_Value = By.CssSelector("span[id$='Email']");

        public static By Shopping_Cart_OrderConfirmation_OrderDetails_Model_Value = By.CssSelector("a[id$='ModelNbr']");

        public static By Shopping_Cart_OrderConfirmation_OrderDetails_Package_Value = By.CssSelector("span[id$='PackageDescription']");

        public static By Shopping_Cart_OrderConfirmation_OrderDetails_ShipsFrom_Value = By.CssSelector("span[id$='ShipsFrom']");

        public static By Shopping_Cart_OrderConfirmation_OrderDetails_EstDeliveryDate_Value = By.CssSelector("span[id$='EstDeliveryDate']");

        public static By Shopping_Cart_OrderConfirmation_OrderDetails_UnitPrice_Value = By.CssSelector("span[id*='BuyUSListPrice']");

        public static By Shopping_Cart_OrderConfirmation_OrderDetails_Quantity_Value = By.CssSelector("span[id*='Quantity']");

        public static By Shopping_Cart_OrderConfirmation_OrderDetails_Subtotal_Value = By.CssSelector("span[id$='TotalPrice']");

        public static By Shopping_Cart_OrderConfirmation_PaymentDetails_Subtotal_Value = By.CssSelector("span[id$='Subtotal']");

        public static By Shopping_Cart_OrderConfirmation_PaymentDetails_ShippingAndHandling_Value = By.CssSelector("span[id$='SnH']");

        public static By Shopping_Cart_OrderConfirmation_TotalCharges_Lbl = By.CssSelector("span[id$='Total']");

        public static By GDPR_Banner_Success_Btn = By.CssSelector("div[id='cookie-consent-container'] * div[class='short-description']>a[class='btn btn-success']");

        public static By Public_Community_Username_Textfield = By.CssSelector("input[id='extension_EZUserName']");

        public static By Public_Community_Continue_Button = By.CssSelector("button[id='continue']");

        public static By ShortHeader = By.CssSelector("header[class='common']");

        public static By MainNavigation = By.CssSelector("ul[class='nav navbar-nav']");

        public static By MainNavigation_MyAnalogTab = By.CssSelector("a[data-qa-element='link_myAnalog']");

        public static By MainNavigation_Company = By.CssSelector("a[data-qa-element='link_company']");

        public static By MainNavigation_CompanyTab_Expanded = By.CssSelector("div[class='company menu content expanded']");

        public static By MainNavigation_CompanyTab_CorporateSocialResponsibility_Link = By.CssSelector("div[class^='company menu'] * a[href$='/corporate-social-responsibility.html']");

        public static By MainNavigation_CompanyTab_CorporateSocialResponsibility_Subcategory_Links = By.CssSelector("div[class^='company menu'] * a[href*='/corporate-social-responsibility/']");

        public static By MainNavigation_MyHistoryTab = By.CssSelector("a[data-qa-element='link_myhistory']");

        public static By MainNavigation_ProductsTab = By.CssSelector("a[data-qa-element='link_products']");

        public static By MainNavigation_ApplicationsTab = By.CssSelector("a[data-qa-element='link_applications']");

        public static By MainNavigation_DesignCenterTab = By.CssSelector("a[data-qa-element='link_designcenter']");

        public static By MainNavigation_DesignCenterTab_Expanded = By.CssSelector("div[class='designcenter menu content expanded']");

        public static By MainNavigation_DesignCenterTab_Featured_Secs = By.CssSelector("section[class='design-center-section']>div:nth-of-type(1)>div>div");

        public static By MainNavigation_DesignCenterTab_Category_Links = By.CssSelector("div[class^='designcenter'] * h3>a");

        public static By MainNavigation_DesignCenterTab_SubCategory_Links = By.CssSelector("div[class^='designcenter'] * li>a");

        public static By MainNavigation_DesignCenterTab_ReferenceDesigns_Link = By.CssSelector("div[class^='designcenter menu'] * a[href*='/reference-designs.html']");

        public static By MainNavigation_DesignCenterTab_DriversAndReferenceCode_Link = By.CssSelector("div[class^='designcenter menu'] * a[href*='/drivers-reference-code.html']");

        public static By MainNavigation_DesignCenterTab_CircuitDesignToolsAndCalculators_Link = By.CssSelector("div[class^='designcenter menu'] * a[href*='/design-tools-and-calculators.html']");

        public static By MainNavigation_DesignCenterTab_CircuitDesignToolsAndCalculators_Subcategory_Links = By.CssSelector("div[class^='designcenter menu'] * a[href*='/design-tools-and-calculators/']");

        public static By MainNavigation_DesignCenterTab_EvaluationHardwareAndSoftware_Link = By.CssSelector("div[class^='designcenter menu'] * a[href*='/evaluation-hardware-and-software.html']");

        public static By MainNavigation_DesignCenterTab_EvaluationHardwareAndSoftware_Subcategory_Links = By.CssSelector("div[class^='designcenter menu'] * a[href*='/evaluation-hardware-and-software/']");

        public static By MainNavigation_DesignCenterTab_SimulationModels_Link = By.CssSelector("div[class^='designcenter menu'] * a[href*='/simulation-models.html']");

        public static By MainNavigation_DesignCenterTab_SimulationModels_Subcategory_Links = By.CssSelector("div[class^='designcenter menu'] * a[href*='/simulation-models/']");

        public static By MainNavigation_DesignCenterTab_PackagingQualitySymbolsAndFootprints_Link = By.CssSelector("div[class^='designcenter menu'] * a[href*='/packaging-quality-symbols-footprints.html']");

        public static By MainNavigation_DesignCenterTab_PackagingQualitySymbolsAndFootprints_Subcategory_Links = By.CssSelector("div[class^='designcenter menu'] * a[href*='/packaging-quality-symbols-footprints/']");

        public static By MainNavigation_DesignCenterTab_ProcessorsAndDsp_Link = By.CssSelector("div[class^='designcenter menu'] * a[href*='/processors-and-dsp.html']");

        public static By MainNavigation_DesignCenterTab_ProcessorsAndDsp_Subcategory_Links = By.CssSelector("div[class^='designcenter menu'] * a[href*='/processors-and-dsp/']");

        public static By MainNavigation_EducationTab = By.CssSelector("a[data-qa-element='link_education']");

        public static By MainNavigation_EducationTab_Expanded = By.CssSelector("div[class='education menu content expanded']");

        public static By MainNavigation_EducationTab_EducationLibrary_Link = By.CssSelector("div[class^='education menu'] * a[href*='/education-library.html']");

        public static By MainNavigation_EducationTab_EducationLibrary_Sec_Links = By.CssSelector("div[class^='education menu'] * a[href*='/education-library/']");

        public static By MainNavigation_EducationTab_AnalogCircuitDesignTutorials_Link = By.CssSelector("div[class^='education menu'] * a[href*='/tutorials.html']");

        public static By MainNavigation_EducationTab_Videos_Link = By.CssSelector("div[class^='education menu'] * a[href*='/videos.html']");

        public static By MainNavigation_EducationTab_Webcasts_Link = By.CssSelector("div[class^='education menu'] * a[href*='/webcasts.html']");

        public static By MainNavigation_EducationTab_EducationEngagement_Link = By.CssSelector("div[class^='education menu'] * a[href*='/university-engagement.html']");

        public static By MainNavigation_EducationTab_EducationEngagement_China_Link = By.CssSelector("div[class^='education menu'] * a[href*='/university-engagement/china.html']");

        public static By MainNavigation_EducationTab_EducationEngagement_Sec_Links = By.CssSelector("div[class^='education menu'] * a[href*='/university-engagement/']");

        public static By MainNavigation_EducationTab_NorthAmerica_Link = By.CssSelector("div[class^='education menu'] * a[href*='/north-america.html']");

        public static By MainNavigation_EducationTab_India_Link = By.CssSelector("div[class^='education menu'] * a[href*='/india-anveshan.html']");

        public static By MainNavigation_EducationTab_Philippines_Link = By.CssSelector("div[class^='education menu'] * a[href*='/philippines.html']");

        public static By MainNavigation_EducationTab_ModulesAndCourseware_Link = By.CssSelector("div[class^='education menu'] * a[href*='/courses-and-tutorials.html']");

        public static By MainNavigation_EducationTab_ModulesAndCourseware_Sec_Links = By.CssSelector("div[class^='education menu'] * a[href*='/courses-and-tutorials/']");

        public static By MainNavigation_EducationTab_ActiveLearningModules_Link = By.CssSelector("div[class^='education menu'] * a[href*='/active-learning-module.html");

        public static By MainNavigation_SupportTab = By.CssSelector("a[data-qa-element='link_support']");

        public static By MainNavigation_ExpandedMyAnalogTab = By.CssSelector("div[class='myAnalog menu content expanded']");

        public static By MyAnalogTab_LogInToMyAnalog_Btn = By.CssSelector("button[class='ma btn btn-primary']");

        public static By SupportTab_EZ_Community_DropDown = By.CssSelector("div[class*='dropdown ez-dropdown']>button");

        public static By SupportTab_EZ_Community_DropDown_List = By.CssSelector("div[class='dropdown ez-dropdown open']>ul[class='dropdown-menu support-dropdown-community']");

        public static By BreadCrumb_Home_Icon = By.CssSelector("div[class='breadcrumb adicrumb visible-desktop'] * a[class='home']");

        public static By GlobalSearch_Auto_Complete = By.CssSelector("ul[class='auto-complete']");

        public static By LoginToMyAnalog_Button = By.CssSelector("div[class='col-md-4 login'] * button");

        public static By GDPR_Cookie_Details_Link = By.CssSelector("div[id='cookie-consent-container'] * div[class='short-description'] * a[data-id='cookie-details']");

        public static By GDPR_Privacy_Pol_Link = By.CssSelector("div[id='cookie-consent-container'] * div[class='short-description'] * a:nth-of-type(2)");

        public static By GDPR_Privacy_Decline_Link = By.CssSelector("div[id='cookie-consent-container'] * div[class='long-description']>a[data-id='decline']");

        public static By Products_Tab_Prev_Visited_Cat_Section = By.CssSelector("div[class='outline-card previous-categories']");

        public static By Products_Tab_New_Prod_Section = By.CssSelector("div[class='outline-card new-products']");

        public static By Products_Tab_Search = By.CssSelector("div[class='search large still-searching'] * input");

        public static By Products_Tab_Search_Cancel = By.CssSelector("div[class='search large active'] span[class='close show']");

        public static By Products_Tab_ViewAll_Btn = By.CssSelector("div[class='new-products-btn']>a[class='btn btn-primary']");

        public static By Products_Tab_SmallSearch = By.CssSelector("div[class='search small'] * input");
        //----------------- FOOTER -----------------//

        public static By Footer = By.CssSelector("footer[id='footer']");

        public static By Footer_BigBlock = By.CssSelector("div[class='tiles-big']");

        public static By Footer_ProblemSolvers_BigBlock = By.CssSelector("div[class='tiles-big'] * a[name^='problem-solvers']>li");

        public static By Footer_PatentsWorldwide_BigBlock = By.CssSelector("div[class='tiles-big'] * a[name^='patents']>li");

        public static By Footer_Customers_BigBlock = By.CssSelector("div[class='tiles-big'] * a[name^='customers']>li");

        public static By Footer_Years_BigBlock = By.CssSelector("div[class='tiles-big'] * a[name^='years']>li");

        public static By Footer_AheadOfWhatsPossible_Header = By.CssSelector("div[class='callout']>span");

        public static By Footer_AheadOfWhatsPossible_Desc = By.CssSelector("div[class='callout']>p");

        public static By Footer_SeeTheInnovations_Link = By.CssSelector("div[class='callout'] * a");

        public static By Footer_Ez_SocialLink = By.CssSelector("li[class^='ez']");

        public static By Footer_Ig_SocialLink = By.CssSelector("li>a[class^='instagram']");

        public static By Footer_WeChat_SocialLink = By.CssSelector("div[class$='desktop'] * li>a[class='weixin']");

        public static By Footer_WeChat_Modal = By.CssSelector("div[class$='desktop'] * div[id='weixin-modal'] * div[class='modal-content']");

        public static By Footer_WeChat_Modal_QrCode_Image = By.CssSelector("div[class$='desktop'] * div[id='weixin-modal'] * img");

        public static By Footer_Fb_SocialLink = By.CssSelector("li>a[class^='facebook']");

        public static By Footer_Weibo_SocialLink = By.CssSelector("li[class^='weibo']");

        public static By Footer_Twitter_SocialLink = By.CssSelector("li>a[class^='twitter']");

        public static By Footer_Youku_SocialLink = By.CssSelector("li[class^='youku']");

        public static By Footer_LinkedIn_SocialLink = By.CssSelector("li>a[class^='linkedin']");

        public static By Footer_Yt_SocialLink = By.CssSelector("li>a[class^='youtube']");

        public static By Footer_AboutAdi_QuickLink = By.CssSelector("a[name^='About_ADI']");

        public static By Footer_Partners_QuickLink = By.CssSelector("a[name^='Partners']");

        public static By Footer_AnalogDialogue_QuickLink = By.CssSelector("a[name^='Analog_Dialogue']");

        public static By Footer_Careers_QuickLink = By.CssSelector("div[name='adi_footer_quicklinks'] * a[name^='Careers']");

        public static By Footer_ContactUs_QuickLink = By.CssSelector("a[name^='Contact_us']");

        public static By Footer_InvestorRelations_QuickLink = By.CssSelector("a[name^='Investor_Relations']");

        public static By Footer_NewsRoom_QuickLink = By.CssSelector("a[name^='News_Room']");

        public static By Footer_Qr_QuickLink = By.CssSelector("a[name^='Quality_N_Reliability']");

        public static By Footer_Sd_QuickLink = By.CssSelector("a[name^='Sales_N_Distributors']");

        public static By Footer_AnalogGarage_QuickLink = By.CssSelector("a[name^='analog-garage']");

        public static By Footer_JpSeminar_QuickLink = By.CssSelector("a[name^='seminar_jp']");

        public static By Footer_En_Language = By.CssSelector("div[name='adi_footer_languages_quicklinks'] * a[name^='English']");

        public static By Footer_Cn_Language = By.CssSelector("div[name='adi_footer_languages_quicklinks'] * a[name^='Chinese']");

        public static By Footer_Jp_Language = By.CssSelector("div[name='adi_footer_languages_quicklinks'] * a[name^='Japanese']");

        public static By Footer_Ru_Language = By.CssSelector("div[name='adi_footer_languages_quicklinks'] * a[name^='Russian']");

        public static By Footer_Newsletters_Header = By.CssSelector("div[name^='Callouts_Newsletter']>span");

        public static By Footer_Newsletters_Desc = By.CssSelector("div[name^='Callouts_Newsletter']>p");

        public static By Footer_Newsletters_SignUp_Btn = By.CssSelector("a[name^='Newsletter']");

        public static By Footer_Copyright_Symb = By.CssSelector("div[name='adi_footer_copyright']>ul:nth-of-type(1)>li:nth-child(1)");

        public static By Footer_Copyright_Txt = By.CssSelector("div[name='adi_footer_copyright']>ul:nth-of-type(1)>li:nth-child(2)");

        public static By Footer_IcpCode = By.CssSelector("div[name='adi_footer_copyright']>ul:nth-of-type(1)>li:nth-child(3)");

        public static By Footer_Sitemap_Link = By.CssSelector("a[name^='Sitemap']");

        public static By Footer_PrivacyAndSecurity_Link = By.CssSelector("a[name^='Privacy_N_Security']");

        public static By Footer_PrivacySettings_Link = By.CssSelector("a[name^='Privacy_Settings']");

        public static By Footer_TermsOfUse_Link = By.CssSelector("a[name^='Terms_of_Use']");

        public static By Footer_Fdbk_Btn = By.CssSelector("div:nth-last-child(1)>img[id^='UIF-IMG']");

        public static By Footer_FdbkForm = By.CssSelector("div[id^='SurveyId']");

        public static By Footer_FdbkForm_X_Btn = By.CssSelector("button[id='btnClose']");

        public static By BackToTop_Btn = By.CssSelector("a[class='backtotop']");

        //---------------Registration----------------//
        public static By MyAnalogTab_GetStarted_Btn = By.CssSelector("div[class='myAnalog menu content expanded'] * div[class='col-md-8 register']>div>button");

        public static By MyAnalogTab_RegisterNow_Link = By.CssSelector("div[class='myAnalog menu content expanded'] * div[class='col-md-4 login']>form>p>a");

        public static By Registration_ProdCat_Section = By.CssSelector("section[class='categories']");

        public static By PDP_MyAnalogWidget_Btn = By.CssSelector("div[class='socialmediawidget socialmediawidget-main visible-desktop SavetoAnalog'] * button[id='myAnalogWidgetBtn']");

        public static By PDP_SaveToAnalog_RegisterLink = By.CssSelector("div[class='saveToMyAnalog widget'] div[class='other actions']>a");

        public static By MyAnalogWidget_RegisterNow_Link = By.CssSelector("div[class='saveToMyAnalog widget']>div>div>a");

        public static By LoginPage_Register_Link = By.CssSelector("div[id='myAnalog-app'] * section[class='registration benefits']>div>a:nth-of-type(2)");

        public static By Registration_Language_Dropdown = By.CssSelector("div[class='change language'] * button");

        public static By Registration_Next_Btn = By.CssSelector("button[class='ma btn btn-primary']");

        public static By Registration_Skip_Btn = By.CssSelector("div[class='controls fade in']>button[class='ma btn btn-link']");

        public static By Registration_Back_Btn = By.CssSelector("section[class='registration']>form>button[class='ma btn btn-link']");

        public static By Registration_VerifyEmail_TxtField = By.CssSelector("input[name='verify-email']");

        //public static By Registration_WithSocial_Message = By.CssSelector("span[class='ma alert fade in alert-danger']");

        public static By Registration_BlockDomain_Message = By.CssSelector("span[class='ma alert fade in alert-warning']");

        public static By Registration_SendVerificationCode_Btn = By.CssSelector("div[class='verify email']>button[class='ma btn btn-primary']");

        public static By Registration_EnterCode_TextField = By.CssSelector("div[class='verify email']>div>input");

        public static By Registration_Country_Dropdown = By.CssSelector("div[class='registration fields']>div:nth-child(3) * button");

        public static By Registration_City_TxtField = By.CssSelector("input[name='city']");

        public static By Registration_FirstName_TxtField = By.CssSelector("input[name='first']");

        public static By Registration_LastName_TxtField = By.CssSelector("input[name='last']");

        public static By Registration_Password_TxtField = By.CssSelector("input[name='password']");

        public static By Registration_ConfirmPassword_TxtField = By.CssSelector("input[name='confirmPassword']");

        public static By Registration_ZipCode_TxtField = By.CssSelector("input[name='zipCode']");

        public static By Registration_CompName_TxtField = By.CssSelector("input[name='companyName']");

        public static By Registration_TermsOfUse_ChxBox = By.CssSelector("input[name='consent-termsofuse']");

        public static By Registration_EmailConsent_ChxBox = By.CssSelector("input[name='consent-email']");

        public static By Registration_PhoneConsent_ChxBox = By.CssSelector("input[name='consent-phone']");

        public static By Registration_Consent_Comm = By.CssSelector("input[name='consent-communication']");

        public static By Registration_Register_Btn = By.CssSelector("section[class='register'] * button[class='ma btn btn-primary']");

        public static By Registration_RegisterExisting_Message = By.CssSelector("span[class='ma alert fade in alert-danger']");

        public static By Registration_VerifyEmail_Login_Link = By.CssSelector("div[class='login']>a");

        public static By Registration_VerifyEmail_SendNewCode_Link = By.CssSelector("div[class='verify email']>button[class='ma btn btn-link']");

        public static By Registration_EmailAddress_TxtField = By.CssSelector("input[name='email']");

        public static By Registration_Company_RadioButton = By.CssSelector("input[value='school']");

        public static By Registration_School_RadioButton = By.CssSelector("input[value='company']");

        public static By Registration_School_TextField = By.CssSelector("input[name='schoolName']");

        public static By Telephone_TextField = By.CssSelector("input[name='phoneNumber']");

        //----------------- MAILINATOR -----------------//
        public static By Mailinator_EmailAddress_TextField = By.CssSelector("input[id='addOverlay']");

        public static By Mailinator_Go_Btn = By.CssSelector("button[id='go-to-public']");

        public static By Mailinator_LatestEmail = By.CssSelector("tr[class='ng-scope']:nth-child(1)>td");

        public static By Mailinator_LatestEmail_Sender = By.CssSelector("table[class='table-striped jambo_table'] * tr>td:nth-child(2)");

        public static By Mailinator_LatestEmail_Subj = By.CssSelector("table[class='table-striped jambo_table'] * tr>td:nth-child(3)");

        public static By Mailinator_Email_Verification_Code = By.CssSelector("td>strong");

        /*****Product Detail Page*****/
        public static By PDP_Country_Dropdown = By.CssSelector("div[class='search-with-filter pcnpdn-country-filter'] * a");

        public static By PDP_CheckInventory_Btn = By.CssSelector("div[id='check-inventory']");

        /*****DSP****/
        public static By DSP_CheckInventory_Btn = By.CssSelector("div[class*='check-inventory']");

        public static By DSP_AddToCart_Btn = By.CssSelector("a[class='add-to-cart btn btn-primary']");

        /*****Eval Board*****/
        public static By EvalBoard_CheckInventory_Btn = By.CssSelector("div[class^='check-inventory']");

        public static By EvalBoard_AddToCart_Btn = By.CssSelector("a[class^='add-to-cart']");

        /****Login Page***/

        public static By LogIn_Error_Message = By.CssSelector("div[class^='error pageLevel']>p");

        public static By Login_ForgotPassLink = By.CssSelector("div[class='forgot-password']>a");

        public static By UserTextField = By.CssSelector("input[id='signInName']");

        public static By PwdTextField = By.CssSelector("input[id='password']");

        public static By LoginBtn = By.CssSelector("button[id='next']");

        public static By Login_RememberMe_Btn = By.CssSelector("input[type='checkbox']");

        /****Social Registration - Login Page****/
        public static By Social_Header = By.CssSelector("div[class='social'] h2");

        public static By Social_LinkedIn_Btn = By.CssSelector("button[id='LinkedInExchange']");

        public static By Social_LindkenIn_Cn_Btn = By.CssSelector("button[id='LinkedInExchange-CN']");

        public static By Social_GoogleEx_Btn = By.CssSelector("button[id='GoogleExchange']");

        public static By Social_Facebook_Btn = By.CssSelector("button[id='FacebookExchange']");

        /****Social Registration - Registration Page****/
        public static By Social_Reg_Header = By.CssSelector("div[class='social_login'] h2");

        public static By Social_Reg_LinkedIn_Btn = By.CssSelector("a[class='linkedin social-btn']");

        public static By Social_Reg_GoogleEx_Btn = By.CssSelector("a[class='google social-btn']");

        public static By Social_Reg_Facebook_Btn = By.CssSelector("a[class='fb social-btn']");

        public static By Social_Reg_Fname = By.CssSelector("input[id='givenName']");
        public static By Social_Reg_Lname = By.CssSelector("input[id='surname']");
        public static By Social_Reg_Zip = By.CssSelector("input[id='postalCode']");
        public static By Social_Reg_CountryDD = By.CssSelector("select[id='dropdown_single']");
        public static By Social_Reg_Company_RB = By.CssSelector("input[id='Company']");
        public static By Social_Reg_School_RB = By.CssSelector("input[value='School']");
        public static By Social_Reg_CompanySchool_Txtbox = By.CssSelector("input[id='extension_CompanyOrSchoolValue']");
        public static By Social_Reg_ConsentTermsOfUse = By.CssSelector("input[id='extension_Consented_TermsOfUse_true']");
        public static By Social_Reg_ConsentCommunication = By.CssSelector("input[id='extension_Consented_Communication_true']");
        public static By Social_Reg_Submit_btn = By.CssSelector("button[id='continue']");


        /****Forgot Password****/
        public static By ForgotPass_Email_TxtField = By.CssSelector("input[id='email']");

        public static By ForgotPass_SendVerificationCode_Btn = By.CssSelector("button[id='email_ver_but_send']");

        public static By ForgotPass_Cancel_Btn = By.CssSelector("button[id='cancel']");

        public static By ForgotPass_VerifyEmail_TxtField = By.CssSelector("input[id='email_ver_input']");

        public static By ForgotPass_VerifyCode_Btn = By.CssSelector("button[id='email_ver_but_verify']");

        public static By ForgotPass_Continue_Btn = By.CssSelector("button[id='continue']");

        public static By ResetPass_NewPass_TextField = By.CssSelector("input[id='newPassword']");

        public static By ResetPass_ConfirmNewPass_TextField = By.CssSelector("input[id='reenterPassword']");

        //----------------- GLOBAL SEARCH -----------------//

        public static By GlobalSearchResults_LeftArrow = By.CssSelector("div[class='collapse-search']");

        public static By GlobalSearchResults_Collapsed_View = By.CssSelector("div[class='container global-search']");

        //----- Product Categories Filter -----//

        public static By GlobalSearchResults_ProductCategories_Filter = By.CssSelector("li[data-facetcategory^='prod_cat_l1']");

        public static By GlobalSearchResults_ProductCategories_Filter_Btn = By.CssSelector("li[data-facetcategory^='prod_cat_l1']>a[class='facet-heading']>span");

        public static By GlobalSearchResults_Default_Collapsed_ProductCategories_Filter = By.CssSelector("li[class='facet-filter-item '][data-facetcategory^='prod_cat_l1']");

        public static By GlobalSearchResults_Collapsed_ProductCategories_Filter = By.CssSelector("li[class='facet-filter-item'][data-facetcategory^='prod_cat_l1']");

        public static By GlobalSearchResults_Expanded_ProductCategories_Filter = By.CssSelector("li[class$='active'][data-facetcategory^='prod_cat_l1']");

        public static By GlobalSearchResults_ProductCategories_Selected_Filter_Lbl = By.CssSelector("li[data-facetcategory^='prod_cat_l1'] * li[class*='base-pivot  radio on']>a>span:nth-of-type(1)");

        public static By GlobalSearchResults_ProductCategories_Subfilter_SearchTot_Txt = By.CssSelector("li[data-facetcategory^='prod_cat_l1'] * span[class='count badge']");

        public static By GlobalSearchResults_ProductCategories_SeeLess_Subfilters = By.CssSelector("li[data-facetcategory^='prod_cat_l1']>ul>li[class$='non-zero']");

        public static By GlobalSearchResults_ProductCategories_SeeAll_Subfilters = By.CssSelector("li[data-facetcategory^='prod_cat_l1']>ul>li");

        public static By GlobalSearchResults_ProductCategories_Expanded_Subfilters = By.CssSelector("li[class$='see-all'][data-facetcategory^='prod_cat_l1']");

        public static By GlobalSearchResults_ProductCategories_Subfilter1 = By.CssSelector("li[data-facetcategory^='prod_cat_l1']>ul>li:nth-child(1)");

        public static By GlobalSearchResults_ProductCategories_Subsubfilters = By.CssSelector("li[class*='radio on'] * li[data-facetcategory^='prod_cat_l2']");

        public static By GlobalSearchResults_ProductCategories_Subsubfilters_Only_Lbl = By.CssSelector("li[class*='radio on'] * li[data-facetcategory^='prod_cat_l2'] * div[class='select-single']>span");

        public static By GlobalSearchResults_ProductCategories_SeeAll_Link = By.CssSelector("li[data-facetcategory^='prod_cat_l1'] * span[class='link-more']");

        public static By GlobalSearchResults_ProductCategories_SeeLess_Link = By.CssSelector("li[data-facetcategory^='prod_cat_l1'] * span[class='link-less']");

        //----- Markets Filter -----//

        public static By GlobalSearchResults_Markets_Filter = By.CssSelector("li[data-facetcategory^='market_l1']");

        public static By GlobalSearchResults_Markets_Filter_Btn = By.CssSelector("li[data-facetcategory^='market_l1']>a[class='facet-heading']>span");

        public static By GlobalSearchResults_Default_Collapsed_Markets_Filter = By.CssSelector("li[class='facet-filter-item '][data-facetcategory^='market_l1']");

        public static By GlobalSearchResults_Collapsed_Markets_Filter = By.CssSelector("li[class='facet-filter-item'][data-facetcategory^='market_l1']");

        public static By GlobalSearchResults_Expanded_Markets_Filter = By.CssSelector("li[class$='active'][data-facetcategory^='market_l1']");

        public static By GlobalSearchResults_Markets_Selected_Filter_Lbl = By.CssSelector("li[data-facetcategory^='market_l1'] * li[class*='base-pivot  checkbox on']>a>span:nth-of-type(1)");

        public static By GlobalSearchResults_Markets_Subfilter_Cb = By.CssSelector("li[data-facetcategory^='market_l1'] * li[class*='checkbox']>a");

        public static By GlobalSearchResults_Markets_Subfilter_SearchTot_Txt = By.CssSelector("li[data-facetcategory^='market_l1'] * span[class='count badge']");

        public static By GlobalSearchResults_Markets_SeeLess_Subfilters = By.CssSelector("li[data-facetcategory^='market_l1']>ul>li[class$='non-zero']");

        public static By GlobalSearchResults_Markets_SeeAll_Subfilters = By.CssSelector("li[data-facetcategory^='market_l1']>ul>li");

        public static By GlobalSearchResults_Markets_Expanded_Subfilters = By.CssSelector("li[class$='see-all'][data-facetcategory^='market_l1']");

        public static By GlobalSearchResults_Markets_Subfilter1 = By.CssSelector("li[data-facetcategory^='market_l1']>ul>li:nth-child(1)");

        public static By GlobalSearchResults_Markets_SeeAll_Link = By.CssSelector("li[data-facetcategory^='market_l1'] * span[class='link-more']");

        public static By GlobalSearchResults_Markets_SeeLess_Link = By.CssSelector("li[data-facetcategory^='market_l1'] * span[class='link-less']");

        //----- Technologies Filter -----//

        public static By GlobalSearchResults_Technologies_Filter = By.CssSelector("li[data-facetcategory^='technology_l1']");

        public static By GlobalSearchResults_Technologies_Filter_Btn = By.CssSelector("li[data-facetcategory^='technology_l1']>a[class='facet-heading']>span");

        public static By GlobalSearchResults_Default_Collapsed_Technologies_Filter = By.CssSelector("li[class='facet-filter-item '][data-facetcategory^='technology_l1']");

        public static By GlobalSearchResults_Collapsed_Technologies_Filter = By.CssSelector("li[class='facet-filter-item'][data-facetcategory^='technology_l1']");

        public static By GlobalSearchResults_Expanded_Technologies_Filter = By.CssSelector("li[class$='active'][data-facetcategory^='technology_l1']");

        public static By GlobalSearchResults_Technologies_Subfilter_Cb = By.CssSelector("li[data-facetcategory^='technology_l1'] * li[class*='checkbox']>a");

        public static By GlobalSearchResults_Technologies_Subfilter_SearchTot_Txt = By.CssSelector("li[data-facetcategory^='technology_l1'] * span[class='count badge']");

        public static By GlobalSearchResults_Technologies_SeeLess_Subfilters = By.CssSelector("li[data-facetcategory^='technology_l1']>ul>li[class$='non-zero']");

        public static By GlobalSearchResults_Technologies_SeeAll_Subfilters = By.CssSelector("li[data-facetcategory^='technology_l1']>ul>li");

        public static By GlobalSearchResults_Technologies_Expanded_Subfilters = By.CssSelector("li[class$='see-all'][data-facetcategory^='technology_l1']");

        public static By GlobalSearchResults_Technologies_Subfilter1 = By.CssSelector("li[data-facetcategory^='technology_l1']>ul>li:nth-child(1)");

        public static By GlobalSearchResults_Technologies_SeeAll_Link = By.CssSelector("li[data-facetcategory^='technology_l1'] * span[class='link-more']");

        public static By GlobalSearchResults_Technologies_SeeLess_Link = By.CssSelector("li[data-facetcategory^='technology_l1'] * span[class='link-less']");

        //----- Resources Filter -----//

        public static By GlobalSearchResults_Resources_Filter = By.CssSelector("li[data-facetcategory^='resource_type_fac']");

        public static By GlobalSearchResults_Resources_Filter_Btn = By.CssSelector("li[data-facetcategory^='resource_type_fac']>a[class='facet-heading']>span");

        public static By GlobalSearchResults_Default_Collapsed_Resources_Filter = By.CssSelector("li[class='facet-filter-item '][data-facetcategory^='resource_type_fac']");

        public static By GlobalSearchResults_Collapsed_Resources_Filter = By.CssSelector("li[class='facet-filter-item'][data-facetcategory^='resource_type_fac']");

        public static By GlobalSearchResults_Expanded_Resources_Filter = By.CssSelector("li[class$='active'][data-facetcategory^='resource_type_fac']");

        public static By GlobalSearchResults_Resources_Selected_Filter_Lbl = By.CssSelector("li[data-facetcategory^='resource_type_fac'] * li[class*='base-pivot  checkbox on']>a>span:nth-of-type(1)");

        public static By GlobalSearchResults_Resources_Selected_Subfilter_Lbl = By.CssSelector("li[data-facetcategory^='resource_type_fac'] * li[class*='child-pivot  checkbox on']>a>span:nth-of-type(1)");

        public static By GlobalSearchResults_Resources_Subfilter_Cb = By.CssSelector("li[data-facetcategory^='resource_type_fac'] * li[class*='checkbox']>a");

        public static By GlobalSearchResults_Resources_Subfilter_SearchTot_Txt = By.CssSelector("li[data-facetcategory^='resource_type_fac'] * span[class='count badge']");

        public static By GlobalSearchResults_Resources_Expanded_Subfilters = By.CssSelector("li[class$='see-all'][data-facetcategory^='resource_type_fac']");

        public static By GlobalSearchResults_Resources_Subfilter1 = By.CssSelector("li[data-facetcategory^='resource_type_fac']>ul>li:nth-child(1)");

        public static By GlobalSearchResults_Resources_SeeLess_Subfilters = By.CssSelector("li[data-facetcategory^='resource_type_fac']>ul>li[class$='non-zero']");

        public static By GlobalSearchResults_Resources_SeeAll_Subfilters = By.CssSelector("li[data-facetcategory^='resource_type_fac']>ul>li");

        public static By GlobalSearchResults_EngineerZone_Subfilter = By.CssSelector("a[data-value='13003f6ac11c469384b6b8dfc9e99f57']");

        public static By GlobalSearchResults_Resources_SeeAll_Link = By.CssSelector("li[data-facetcategory^='resource_type_fac'] * span[class='link-more']");

        public static By GlobalSearchResults_Resources_SeeLess_Link = By.CssSelector("li[data-facetcategory^='resource_type_fac'] * span[class='link-less']");

        //----- Tools and Models Filter -----//

        public static By GlobalSearchResults_ToolsAndModels_Filter = By.CssSelector("li[data-facetcategory^='tools_n_models_fac']");

        public static By GlobalSearchResults_ToolsAndModels_Filter_Btn = By.CssSelector("li[data-facetcategory^='tools_n_models_fac']>a[class='facet-heading']>span");

        public static By GlobalSearchResults_Default_Collapsed_ToolsAndModels_Filter = By.CssSelector("li[class='facet-filter-item '][data-facetcategory^='tools_n_models_fac']");

        public static By GlobalSearchResults_Collapsed_ToolsAndModels_Filter = By.CssSelector("li[class='facet-filter-item'][data-facetcategory^='tools_n_models_fac']");

        public static By GlobalSearchResults_Expanded_ToolsAndModels_Filter = By.CssSelector("li[class$='active'][data-facetcategory^='tools_n_models_fac']");

        public static By GlobalSearchResults_ToolsAndModels_Subfilter_Cb = By.CssSelector("li[data-facetcategory^='tools_n_models_fac'] * li[class*='checkbox']>a");

        public static By GlobalSearchResults_ToolsAndModels_Subfilter_SearchTot_Txt = By.CssSelector("li[data-facetcategory^='tools_n_models_fac'] * span[class='count badge']");

        public static By GlobalSearchResults_ToolsAndModels_SeeLess_Subfilters = By.CssSelector("li[data-facetcategory^='tools_n_models_fac']>ul>li[class$='non-zero']");

        public static By GlobalSearchResults_ToolsAndModels_SeeAll_Subfilters = By.CssSelector("li[data-facetcategory^='tools_n_models_fac']>ul>li");

        public static By GlobalSearchResults_ToolsAndModels_Expanded_Subfilters = By.CssSelector("li[class$='see-all'][data-facetcategory^='tools_n_models_fac']");

        public static By GlobalSearchResults_ToolsAndModels_Subfilter1 = By.CssSelector("li[data-facetcategory^='tools_n_models_fac']>ul>li:nth-child(1)");

        public static By GlobalSearchResults_ToolsAndModels_SeeAll_Link = By.CssSelector("li[data-facetcategory^='tools_n_models_fac'] * span[class='link-more']");

        public static By GlobalSearchResults_ToolsAndModels_SeeLess_Link = By.CssSelector("li[data-facetcategory^='tools_n_models_fac'] * span[class='link-less']");

        //----- Company Videos Filter -----//

        public static By GlobalSearchResults_CompanyVideos_Filter = By.CssSelector("li[data-facetcategory^='companyvideotag_s']");

        public static By GlobalSearchResults_CompanyVideos_Filter_Btn = By.CssSelector("li[data-facetcategory^='companyvideotag_s']>a[class='facet-heading']>span");

        public static By GlobalSearchResults_Default_Collapsed_CompanyVideos_Filter = By.CssSelector("li[class='facet-filter-item '][data-facetcategory^='companyvideotag_s']");

        public static By GlobalSearchResults_Collapsed_CompanyVideos_Filter = By.CssSelector("li[class='facet-filter-item'][data-facetcategory^='companyvideotag_s']");

        public static By GlobalSearchResults_Expanded_CompanyVideos_Filter = By.CssSelector("li[class$='active'][data-facetcategory^='companyvideotag_s']");

        public static By GlobalSearchResults_CompanyVideos_Subfilter_SearchTot_Txt = By.CssSelector("li[data-facetcategory^='companyvideotag_s'] * span[class='count badge']");

        public static By GlobalSearchResults_CompanyVideos_SeeLess_Subfilters = By.CssSelector("li[data-facetcategory^='companyvideotag_s']>ul>li[class$='non-zero']");

        public static By GlobalSearchResults_CompanyVideos_SeeAll_Subfilters = By.CssSelector("li[data-facetcategory^='companyvideotag_s']>ul>li");

        public static By GlobalSearchResults_CompanyVideos_Expanded_Subfilters = By.CssSelector("li[class$='see-all'][data-facetcategory^='companyvideotag_s']");

        public static By GlobalSearchResults_CompanyVideos_Subfilter1 = By.CssSelector("li[data-facetcategory^='companyvideotag_s']>ul>li:nth-child(1)");

        public static By GlobalSearchResults_CompanyVideos_SeeAll_Link = By.CssSelector("li[data-facetcategory^='companyvideotag_s'] * span[class='link-more']");

        public static By GlobalSearchResults_ProductCategoriesCompanyVideos_SeeLess_Link = By.CssSelector("li[data-facetcategory^='companyvideotag_s'] * span[class='link-less']");

        //----- Tradeshows Filter -----//

        public static By GlobalSearchResults_Tradeshows_Filter = By.CssSelector("li[data-facetcategory^='tradeshowtag_s']");

        public static By GlobalSearchResults_Tradeshows_Filter_Btn = By.CssSelector("li[data-facetcategory^='tradeshowtag_s']>a[class='facet-heading']>span");

        public static By GlobalSearchResults_Default_Collapsed_Tradeshows_Filter = By.CssSelector("li[class='facet-filter-item '][data-facetcategory^='tradeshowtag_s']");

        public static By GlobalSearchResults_Collapsed_Tradeshows_Filter = By.CssSelector("li[class='facet-filter-item'][data-facetcategory^='tradeshowtag_s']");

        public static By GlobalSearchResults_Expanded_Tradeshows_Filter = By.CssSelector("li[class$='active'][data-facetcategory^='tradeshowtag_s']");

        public static By GlobalSearchResults_Tradeshows_Selected_Filter_Lbl = By.CssSelector("li[data-facetcategory^='tradeshowtag_s'] * li[class*='base-pivot  radio on']>a>span:nth-of-type(1)");

        public static By GlobalSearchResults_Tradeshows_Subfilter_SearchTot_Txt = By.CssSelector("li[data-facetcategory^='tradeshowtag_s'] * span[class='count badge']");

        public static By GlobalSearchResults_Tradeshows_SeeLess_Subfilters = By.CssSelector("li[data-facetcategory^='tradeshowtag_s']>ul>li[class$='non-zero']");

        public static By GlobalSearchResults_Tradeshows_SeeAll_Subfilters = By.CssSelector("li[data-facetcategory^='tradeshowtag_s']>ul>li");

        public static By GlobalSearchResults_Tradeshows_Expanded_Subfilters = By.CssSelector("li[class$='see-all'][data-facetcategory^='tradeshowtag_s']");

        public static By GlobalSearchResults_Tradeshows_Subfilter1 = By.CssSelector("li[data-facetcategory^='tradeshowtag_s']>ul>li:nth-child(1)");

        public static By GlobalSearchResults_Tradeshows_SeeAll_Link = By.CssSelector("li[data-facetcategory^='tradeshowtag_s'] * span[class='link-more']");

        public static By GlobalSearchResults_Tradeshows_SeeLess_Link = By.CssSelector("li[data-facetcategory^='tradeshowtag_s'] * span[class='link-less']");

        //----- EngineerZone Filter -----//

        public static By GlobalSearchResults_EngineerZone_Filter = By.CssSelector("li[data-facetcategory^='ez_fac_s']");

        public static By GlobalSearchResults_EngineerZone_Filter_Btn = By.CssSelector("li[data-facetcategory^='ez_fac_s']>a[class='facet-heading']>span");

        public static By GlobalSearchResults_Default_Collapsed_EngineerZone_Filter = By.CssSelector("li[class='facet-filter-item '][data-facetcategory^='ez_fac_s']");

        public static By GlobalSearchResults_Collapsed_EngineerZone_Filter = By.CssSelector("li[class='facet-filter-item'][data-facetcategory^='ez_fac_s']");

        public static By GlobalSearchResults_Expanded_EngineerZone_Filter = By.CssSelector("li[class$='active'][data-facetcategory^='ez_fac_s']");

        public static By GlobalSearchResults_EngineerZone_Subfilter_SearchTot_Txt = By.CssSelector("li[data-facetcategory^='ez_fac_s'] * span[class='count badge']");

        public static By GlobalSearchResults_EngineerZone_SeeLess_Subfilters = By.CssSelector("li[data-facetcategory^='ez_fac_s']>ul>li[class$='non-zero']");

        public static By GlobalSearchResults_EngineerZone_SeeAll_Subfilters = By.CssSelector("li[data-facetcategory^='ez_fac_s']>ul>li");

        public static By GlobalSearchResults_EngineerZone_Expanded_Subfilters = By.CssSelector("li[class$='see-all'][data-facetcategory^='ez_fac_s']");

        public static By GlobalSearchResults_EngineerZone_Subfilter1 = By.CssSelector("li[data-facetcategory^='ez_fac_s']>ul>li:nth-child(1)");

        public static By GlobalSearchResults_EngineerZone_SeeAll_Link = By.CssSelector("li[data-facetcategory^='ez_fac_s'] * span[class='link-more']");

        public static By GlobalSearchResults_EngineerZone_SeeLess_Link = By.CssSelector("li[data-facetcategory^='ez_fac_s'] * span[class='link-less']");

        //----- Search Results Textbox -----//

        public static By GlobalSearchResults_Search_Txtbox = By.CssSelector("input[id='text-search']");

        public static By GlobalSearchResults_X_Btn = By.CssSelector("button[class='search-clear-icon']");

        public static By GlobalSearchResults_Search_Btn = By.CssSelector("button[class='search-icon']");

        public static By GlobalSearchResults_SelectedFilter_Txt = By.CssSelector("ul[class='list-inline clearfix']>li>span");

        public static By GlobalSearchResults_SelectedFilter_X_Btn = By.CssSelector("a[class='pull-right close']");

        //----- Sort Menu -----//

        public static By GlobalSearchResults_Sort_Menu = By.CssSelector("select[id='searchOrder']");

        public static By GlobalSearchResults_Sort_Menu_Newest = By.CssSelector("option[value='DESC']");

        public static By GlobalSearchResults_Sort_Menu_Oldest = By.CssSelector("option[value='ASC']");

        public static By GlobalSearchResults_Sort_Menu_Relevancy = By.CssSelector("option[value='relevancy']");

        //----- Show results instead for -----//

        public static By GlobalSearchResults_ShowingResultsFor_Txt = By.CssSelector("div[class='suggestion-container']>h3");

        public static By GlobalSearchResults_ShowResultsInsteadFor_Txt = By.CssSelector("div[class='suggestion-container']>h4");

        public static By GlobalSearchResults_SearchResultList = By.CssSelector("div[class='search-results-items']");
        //----- Baseball Card -----//

        public static By GlobalSearchResults_BaseballCard_ProductCat_Link = By.CssSelector("div[class='search-results-item top-result']>span[class='title-category']>a");

        public static By GlobalSearchResults_BaseballCard_ProductStat_Icon = By.CssSelector("div[class='search-results-item top-result'] div[class*='product-status']>img");

        public static By GlobalSearchResults_BaseballCard_ProductStat_Link = By.CssSelector("div[class='search-results-item top-result'] div[class*='product-status']>a");

        public static By GlobalSearchResults_BaseballCard_Title_Link = By.CssSelector("div[class='search-results-item top-result'] * h4>a");

        public static By GlobalSearchResults_BaseballCard_ShortDesc_Txt = By.CssSelector("div[class='search-results-item top-result'] * div>p");

        public static By GlobalSearchResults_BaseballCard_Resources_Link = By.CssSelector("div[class='search-results-item top-result']>ul[class='resources-links']");

        public static By GlobalSearchResults_BaseballCard_Category_Link = By.CssSelector("div[class='search-results-item top-result']>div[class='row media-title'] a");

        //----- No Search Results -----//

        public static By GlobalSearchResults_NoSearchResults_Txt = By.CssSelector("div[class='no-results']>p");

        //----- Invalid Search Results -----//

        public static By GlobalSearchResults_InvalidSearchResults_Txt_Line1 = By.CssSelector("div[class='no-results']>p:nth-of-type(1)");

        public static By GlobalSearchResults_InvalidSearchResults_Txt_Line2 = By.CssSelector("div[class='no-results']>p:nth-of-type(2)");

        //----- Pagination -----//

        public static By GlobalSearchResults_Prev_Btn = By.CssSelector("span[class^='prev']>a");

        public static By GlobalSearchResults_Next_Btn = By.CssSelector("span[class='next']>a");

        public static By GlobalSearchResults_PageSelected_Txt = By.CssSelector("span[class='active']>a");

        //----- COOKIE CONSENT BANNER -----//

        public static By CookieConsent_Banner = By.CssSelector("div[id^='cookie-consent'] * div[class='modal-content']");

        public static By CookieConsent_Banner_Expanded = By.CssSelector("div[id^='cookie-consent'] * div[class='long-description']");

        //----- HOME PAGE -----//

        //----- Hero Carousel -----//

        public static By HomePage_HeroCarousel = By.CssSelector("div[id='carousel-main-home']>div");

        public static By HomePage_HeroCarousel_Header_Txt = By.CssSelector("div[id='carousel-main-home'] * div[class$='active'] * div[class*='visible-desktop']>div[class^='title']");

        public static By HomePage_HeroCarousel_Description_Txt = By.CssSelector("div[id='carousel-main-home'] * div[class$='active'] * div[class='description rte visible-desktop']>p");

        public static By HomePage_HeroCarousel_LearnMore_Link = By.CssSelector("div[id='carousel-main-home'] * div[class$='active'] * div[class*='desktop']>a[class$='learn-more']");

        //----- New Products Section -----//

        public static By HomePage_NewProducts_Sec = By.CssSelector("div[class^='new-products']");

        public static By HomePage_ViewAllNewProducts_Btn = By.CssSelector("a[class^='new-products']");

        //----- Tiles Section -----//

        public static By HomePage_Tiles_Sec = By.CssSelector("div[data-personalizedcontent='adi-home_default-tiles']");

        //----- Analog Signals Section -----//

        public static By HomePage_AnalogSignals_Sec = By.CssSelector("div[name*='Analog_Signal']");

        public static By HomePage_AnalogSignals_CardsSpot = By.CssSelector("div[name*='Analog_Signal'] * ul * div[class='owl-stage']>div");

        public static By HomePage_AnalogSignals_Header_Txt = By.CssSelector("div[name*='Analog_Signal'] * div[class$='active'] * span[class^='header']");

        public static By HomePage_AnalogSignals_Date = By.CssSelector("div[name*='Analog_Signal'] * div[class$='active'] * span[class='date']");

        public static By HomePage_AnalogSignals_Title = By.CssSelector("div[name*='Analog_Signal'] * div[class$='active'] * a[class$='title']");

        //----- PDP -----//

        //----- Sticky Header -----//

        public static By PDP_StickyHeader_Sec = By.CssSelector("section[id^='stickyheader']");

        public static By PDP_ProductNo_Txt = By.CssSelector("section[id^='stickyheader'] * div[class='titletext']");

        public static By PDP_Desc_Txt = By.CssSelector("section[id^='stickyheader'] * div[class$='desc']>div:nth-of-type(1)");

        public static By PDP_StickyHeader_ProductStatus_Icon = By.CssSelector("section[id^='stickyheader'] * div[class*='recommended']>img");

        public static By PDP_StickyHeader_ProductStatus_Link = By.CssSelector("section[id^='stickyheader'] * div[class*='recommended'] * a");

        public static By PDP_DataSheet_Link = By.CssSelector("section[id^='stickyheader'] * a[class='datasheet']");

        public static By PDP_Top_Btn = By.CssSelector("section[id^='stickyheader'] * div[class^='scrollTotop']");

        public static By PDP_StickyHeader = By.CssSelector("section[id^='stickyheader'] * nav");

        public static By PDP_StickyHeader_Overview = By.CssSelector("a[href$='overview']");

        public static By PDP_StickyHeader_EvaluationKits = By.CssSelector("a[href$='evaluationkit']");

        public static By PDP_StickyHeader_Documentation = By.CssSelector("section[id^='stickyheader'] * a[href$='documentation']");

        public static By PDP_StickyHeader_ToolsAndSimulations = By.CssSelector("section[id^='stickyheader'] * a[href$='tools']");

        public static By PDP_StickyHeader_SoftwareAndSystemsRequirements = By.CssSelector("section[id^='stickyheader'] * a[href$='requirement']");

        public static By PDP_StickyHeader_ReferenceDesigns = By.CssSelector("section[id^='stickyheader'] * a[href$='designs']");

        public static By PDP_StickyHeader_ProductRecommendations = By.CssSelector("section[id^='stickyheader'] * a[href$='recommendations']");

        public static By PDP_ProductRecommendation_Secton = By.CssSelector("section[id='product-recommendations']");

        public static By PDP_StickyHeader_ReferenceMaterials = By.CssSelector("a[href$='reference']");

        public static By PDP_StickyHeader_DesignResources = By.CssSelector("a[href$='quality']");

        public static By PDP_StickyHeader_SupportAndDiscussions = By.CssSelector("a[href$='discussions']");

        public static By PDP_StickyHeader_SampleAndBuy = By.CssSelector("a[href$='samplebuy']");

        //----- Overview -----//

        public static By PDP_Overview_Sec = By.CssSelector("section[id$='overview']");

        public static By PDP_DataSheet_Card = By.CssSelector("section[id$='overview'] * div[class^='datasheet primary']");

        public static By PDP_DataSheet_Card_DataSheet_Link = By.CssSelector("section[id$='overview'] * div[class='title-type']>a[data-resource-type^='data-sheet']");

        public static By PDP_DataSheet_Card_RevNo_Txt = By.CssSelector("section[id$='overview'] * div[class='title-type'] * span[class='revision']");

        public static By PDP_RegionalVersion_RevNo_Txt = By.CssSelector("section[id$='overview'] * div[class='locales'] * span[class='revision']");

        public static By PDP_ProductInfoHelp_Link = By.CssSelector("section[id$='overview'] * div[class='help-link']>a");

        public static By PDP_ProductInfoHelp_Modal = By.CssSelector("div[id^='product-info-help'] * div[class='modal-content']");

        public static By PDP_SubtypeDataSheet = By.CssSelector("section[id$='overview'] * div[class^='children']");

        public static By PDP_SubtypeDataSheet_DropdownArrow_Up_Icon = By.CssSelector("section[id$='overview'] * div[class^='children'] * img[class='dropdown-icon up']");

        public static By PDP_SubtypeDataSheet_DropdownArrow_Down_Icon = By.CssSelector("section[id$='overview'] * div[class^='children'] * img[class='dropdown-icon down']");

        public static By PDP_SubtypeDataSheet_Counter_Txt = By.CssSelector("section[id$='overview'] * div[class='children'] * span[class='value']");

        public static By PDP_SubtypeDataSheet_Menu = By.CssSelector("section[id$='overview'] * div[class^='children']>ul");

        public static By PDP_UserGuides_Link = By.CssSelector("section[id$='overview'] * a[data-resource-type='user-guides']");

        public static By PDP_CircuitNote_Link = By.CssSelector("section[id$='overview'] * div[class='title']>a[data-resource-type='reference-designs']");

        public static By PDP_IbisModels_Link = By.CssSelector("section[id$='overview'] * a[data-resource-type='ibis-models']");

        public static By PDP_FpgaHdl_Link = By.CssSelector("section[id$='overview'] * a[data-resource-type='fpga-hdl']");

        public static By PDP_ProductImage = By.CssSelector("div[id='previewProducts'] * div[class$='active'] * img");

        public static By PDP_ProductImage_Thumbnails = By.CssSelector("div[id^='previewProductsThum']");

        public static By PDP_Zoom_Icon = By.CssSelector("a[class^='zoom']>img");

        public static By PDP_Product_PopUp = By.CssSelector("div[id^='product-modal'] * div[class='modal-content']");

        public static By PDP_Product_PopUp_X_Icon = By.CssSelector("div[id^='product-modal'] * div[class='modal-content'] * button[class='close']");

        public static By PDP_Product_PopUp_Download_Icon = By.CssSelector("div[id^='product-modal'] * div[class='modal-content'] * span[class$='download']");

        public static By PDP_Product_PopUp_Print_Icon = By.CssSelector("div[id^='product-modal'] * div[class='modal-content'] * span[class$='print']");

        public static By PDP_Overview_Section_Lbl = By.CssSelector("section[id$='overview'] * h2");

        public static By PDP_FeaturesAndBenefits_Link = By.CssSelector("a[href$='features-and-benefits']");

        public static By PDP_FeaturesAndBenefits_Content = By.CssSelector("div[id$='features-and-benefits'] * ul>li");

        public static By PDP_ProductDetails_Link = By.CssSelector("a[href$='product-details']");

        public static By PDP_ProductDetails_Content = By.CssSelector("div[id$='product-details']>p:nth-last-of-type(1)");

        public static By PDP_ProductCategories_Txt = By.CssSelector("div[class^='radioverse']>h2:nth-of-type(1)");

        public static By PDP_ProductCategory_Link = By.CssSelector("li[class='category parent']>div>a");

        public static By PDP_ProductSubcategory_Link = By.CssSelector("li[class='category']>div>a[class='category']");

        public static By PDP_Pst_Icon = By.CssSelector("a[class='pst']");

        public static By PDP_MarketsAndTechnology_Txt = By.CssSelector("div[class^='radioverse']>h2:nth-of-type(2)");

        public static By PDP_MarketsAndTechnology_Link = By.CssSelector("div[class^='radioverse'] ul>li>a");

        public static By PDP_ComparableParts_Txt = By.CssSelector("div[class$='comparableParts'] * span");

        public static By PDP_ClickToSeeAllInParametricSearch_Link = By.CssSelector("div[class$='comparableParts'] * a");

        public static By PDP_ProductLifecycle_Txt = By.CssSelector("section[id$='overview']>div[class='container']>div:nth-last-of-type(1) * h2");

        public static By PDP_ProductLifecycle_ProductStatus_Icon = By.CssSelector("a[class='recommended'] * img");

        public static By PDP_ProductLifecycle_ProductStatus_Link = By.CssSelector("a[class='recommended']");

        public static By PDP_ProductLifecycle_Desc_Txt = By.CssSelector("section[id$='overview']>div[class='container']>div:nth-last-of-type(1) * p");

        //----- Evaluation Kits -----//

        public static By PDP_EvaluationKits_Section_Lbl = By.CssSelector("section[id$='evaluationkit'] * h2");

        public static By PDP_EvaluationKits = By.CssSelector("section[id$='evaluationkit'] * div[id='tile-slider'] * div[class='owl-stage']>div");

        public static By PDP_EvaluationKits_FlyOut = By.CssSelector("div[class='evaluation-kits-section']>div[class*='gallery']");

        public static By PDP_EvaluationKits_FlyOut_X_Btn = By.CssSelector("section[id$='evaluationkit'] * a[class$='close-icon']");

        public static By PDP_EvaluationKits_FlyOut_Thumbnail_Imgs = By.CssSelector("div[style=''] * div[class='evaluation-kits-section'] * div[class='thumb-item'] * img");

        public static By PDP_EvaluationKits_FlyOut_Img = By.CssSelector("div[class='evaluation-kits-section'] * div[class$='active'] * img");

        public static By PDP_EvaluationKits_FlyOut_PartNo_Txt = By.CssSelector("div[class='evaluation-kits-section'] * div[class^='description']>h2");

        public static By PDP_EvaluationKits_FlyOut_ViewDetailedEvalKitInfo_Btn = By.CssSelector("div[class='evaluation-kits-section'] * a[class$='analog-rounded-button']");

        public static By PDP_EvaluationKits_FlyOut_ProductDesc_Txt = By.CssSelector("div[class='evaluation-kits-section'] * div[class$='details']>p");

        public static By PDP_EvaluationKits_FlyOut_Resources_Link = By.CssSelector("div[class='evaluation-kits-section'] * a[data-resource-type='evaluation-documentation']");

        public static By PDP_EvaluationKits_FlyOut_Software_Link = By.CssSelector("div[class='evaluation-kits-section'] * a[data-resource-type$='software']");

        //----- Documentation -----//

        public static By PDP_Documentation_Section_Lbl = By.CssSelector("section[id$='documentation'] * h2");

        public static By PDP_Documentation_Tabs = By.CssSelector("section[id$='documentation'] * div[class$='tabs']>ul>li");

        public static By PDP_Documentation_Tab_Title = By.CssSelector("section[id$='documentation'] * div[class='tab-content']>div[class*='active'] * h3");

        public static By PDP_Documentation_Tab_Links = By.CssSelector("section[id$='documentation'] * div[class='tab-content']>div[class*='active'] * a");

        public static By PDP_Documentation_Tab_Pdf_Icons = By.CssSelector("section[id$='documentation'] * div[class='tab-content']>div[class*='active'] * div[class$='IndicatorType']");

        //----- Tools & Simulations -----//

        public static By PDP_ToolsAndSimulations_Section_Lbl = By.CssSelector("section[id$='tools'] * h2");

        public static By PDP_ToolsAndSimulations_VirtualEval_Section_Lbl = By.CssSelector("section[id$='tools'] * div[class$='product-row'] * h3");

        public static By PDP_ToolsAndSimulations_VirtualEval_Section_Desc_Txt = By.CssSelector("section[id$='tools'] * div[class$='product-row'] * p[class$='description']");

        public static By PDP_ToolsAndSimulations_VirtualEval_Section_Img = By.CssSelector("section[id$='tools'] * div[class$='product-row']>img");

        public static By PDP_ToolsAndSimulations_Sections = By.CssSelector("section[id$='tools']>div>div[class='data-container']:nth-last-of-type(1)>div:nth-last-of-type(1)>div");

        //----- Software & Systems Requirements  -----//

        public static By PDP_SoftwareAndSystemsRequirements_Section_Lbl = By.CssSelector("section[id$='requirement'] * h2");

        public static By PDP_SoftwareAndSystemsRequirements_Section_Headers = By.CssSelector("section[id$='requirement'] * div[class='row']>div>h3");

        public static By PDP_SoftwareAndSystemsRequirements_Section_Links = By.CssSelector("section[id$='requirement'] * div[class='row']>div>h3");

        //----- Reference Designs -----//

        public static By PDP_ReferenceDesigns_Section_Lbl = By.CssSelector("section[id$='designs'] * h2");

        public static By PDP_ReferenceDesigns = By.CssSelector("section[id$='designs'] * div[id='tile-slider'] * div[class='owl-stage']>div");

        public static By PDP_ReferenceDesigns_ProductModel_Thumbnail_Img = By.CssSelector("section[id$='designs'] * div[id='tile-slider'] * div[class='owl-stage']>div * div[class$='img']>img");

        public static By PDP_ReferenceDesigns_ProductModel_Thumbnail_ModelNo = By.CssSelector("section[id$='designs'] * div[id='tile-slider'] * div[class='owl-stage']>div * h2");

        public static By PDP_ReferenceDesigns_ProductModel_Thumbnail_Desc_Txt = By.CssSelector("section[id$='designs'] * div[id='tile-slider'] * div[class='owl-stage']>div * p");

        public static By PDP_ReferenceDesigns_ProductModel_FlyOut_Box = By.CssSelector("div[style=''] * div[class='reference-design-section']>div[class*='gallery']");

        public static By PDP_ReferenceDesigns_ProductModel_FlyOut_Box_X_Btn = By.CssSelector("section[id$='designs'] * div[style=''] * a[class$='close-icon']");

        public static By PDP_ReferenceDesigns_ProductModel_FlyOut_Box_Thumbnail_Imgs = By.CssSelector("section[id$='designs'] * div[style=''] * div[class*='thumbnail-img']>div");

        public static By PDP_ReferenceDesigns_ProductModel_FlyOut_Box_Product_Img = By.CssSelector("section[id$='designs'] * div[style=''] * div[class$='active'] * img");

        public static By PDP_ReferenceDesigns_ProductModel_FlyOut_Box_ModelNo_Txt = By.CssSelector("section[id$='designs'] * div[style=''] * div[class$='mobile-hide']>h2");

        public static By PDP_ReferenceDesigns_ProductModel_FlyOut_Box_ModelName_Txt = By.CssSelector("section[id$='designs'] * div[style=''] * div[class$='mobile-hide']>span");

        public static By PDP_ReferenceDesigns_ProductModel_FlyOut_Box_ViewDetailedReferenceDesignInfo_Btn = By.CssSelector("section[id$='designs'] * div[style=''] * a[class^='btn']");

        public static By PDP_ReferenceDesigns_ProductModel_FlyOut_Box_FeaturesAndBenefits_Sec_Lbl = By.CssSelector("section[id$='designs'] * div[style=''] * div[class='benefits-details']>h3");

        public static By PDP_ReferenceDesigns_ProductModel_FlyOut_Box_FeaturesAndBenefits_Sec_Info = By.CssSelector("section[id$='designs'] * div[style=''] * div[class='benefits-details'] * ul>li");

        public static By PDP_ReferenceDesigns_ProductModel_FlyOut_Box_PartsUsed_Sec_Lbl = By.CssSelector("section[id$='designs'] * div[style=''] * div[class='features-benefits']>div:nth-of-type(1)>h3");

        public static By PDP_ReferenceDesigns_ProductModel_FlyOut_Box_PartsUsed_Links = By.CssSelector("section[id$='designs'] * div[style=''] * div[class='features-benefits']>div:nth-of-type(1)>ul>li");

        public static By PDP_ReferenceDesigns_ProductModel_FlyOut_Box_PartsUsed_Icons = By.CssSelector("section[id$='designs'] * div[style=''] * div[class='features-benefits']>div:nth-of-type(1)>ul>li>img");

        public static By PDP_ReferenceDesigns_ProductModel_FlyOut_Box_Resources_Sec_Lbl = By.CssSelector("section[id$='designs'] * div[style=''] * div[class='features-benefits']>div:nth-of-type(2)>h3");

        public static By PDP_ReferenceDesigns_ProductModel_FlyOut_Box_Resources_Links = By.CssSelector("section[id$='designs'] * div[style=''] * ul[class*='resource']>li");

        public static By PDP_ReferenceDesigns_ProductModel_FlyOut_Box_Resources_Download_Icons = By.CssSelector("section[id$='designs'] * div[style=''] * ul[class*='resource']>li img");

        public static By PDP_ReferenceDesigns_ProductModel_FlyOut_Box_Resources_File_Icons = By.CssSelector("section[id$='designs'] * div[style=''] * ul[class*='resource']>li div[class='label label-default linkIndicatorType']");
        
        //----- Reference Materials -----//

        public static By PDP_ReferenceMaterials_Sec_Lbl = By.CssSelector("section[id$='reference'] * h2");

        public static By PDP_ReferenceMaterials_Tabs = By.CssSelector("section[id$='reference'] * div[class$='tabs']>ul>li");

        public static By PDP_ReferenceMaterials_Tab_Links = By.CssSelector("section[id$='reference'] * div[class='tab-content']>div[class*='active'] * a");

        //----- Design Resources -----//

        public static By PDP_DesignResources_Section_Lbl = By.CssSelector("section[id$='quality'] * h2");

        public static By PDP_DesignResources_Section_Desc_Txt = By.CssSelector("section[id$='quality'] * div>p:nth-of-type(2)");

        public static By PDP_MaterialDeclaration_Link = By.CssSelector("section[id$='quality'] * div[class='row options']>div:nth-of-type(1) * a");

        public static By PDP_QualityAndReliability_Link = By.CssSelector("section[id$='quality'] * div[class='row options']>div:nth-of-type(2) * a");

        public static By PDP_SymbolsAndFootprints_Link = By.CssSelector("section[id$='quality'] * div[class='row options']>div:nth-of-type(3) * a");

        public static By PDP_PcnPdnInfo_Sec = By.CssSelector("div[class$='pcnInfo']>div");

        public static By PDP_SelectModel_Dd = By.CssSelector("div[class*='model'] * div[class='dropdown']");

        public static By PDP_SelectModel_Dd_Menu = By.CssSelector("div[class*='model'] * ul[class^='dropdown-menu']>li>span");

        public static By PDP_RequestProductProcessChangeNotifs_Link = By.CssSelector("div[id='requestProduct']>a");

        public static By PDP_SaveToMyAnalog_Widget = By.CssSelector("div[class='saveToMyAnalog widget']");

        public static By PDP_PCNPDN_SaveToMyAnalog_Widget = By.CssSelector("div[class='sample-buy socialmediawidget SavetoAnalog']>div[class='saveToMyAnalog widget']");

        public static By PDP_SaveToMyAnalog_Widget_SaveProduct_Btn = By.CssSelector("section[id$='quality'] * button[id='saveProductToMyanalog']");

        public static By PDP_Pcn_Tbl_X_Btn = By.CssSelector("div[id^='close'] * img");

        public static By PDP_Pcn_Tbl = By.CssSelector("section[id$='quality'] * div[class*='products-packaging']>table");

        public static By PDP_Pcn_Tbl_PcnNo_Col_Header_Txt = By.CssSelector("section[id$='quality'] * div[class*='products-packaging']>table>thead>tr>td:nth-of-type(1)>p");

        public static By PDP_Pcn_Tbl_Title_Col_Header_Txt = By.CssSelector("section[id$='quality'] * div[class*='products-packaging']>table>thead>tr>td:nth-of-type(2)>p");

        public static By PDP_Pcn_Tbl_PublicationDate_Col_Header_Txt = By.CssSelector("section[id$='quality'] * div[class*='products-packaging']>table>thead>tr>td:nth-of-type(3)>p");

        public static By PDP_Pcn_Tbl_PcnNo_Col_PcnNos = By.CssSelector("span[class='pcn-product-files-action']");

        public static By PDP_Pcn_Tbl_Title_Col_Titles = By.CssSelector("section[id$='quality'] * div[class*='products-packaging']>table>tbody>tr:nth-last-of-type(1)>td:nth-of-type(2)");

        public static By PDP_Pcn_Tbl_PublicationDate_Col_PublicationDates = By.CssSelector("section[id$='quality'] * div[class*='products-packaging']>table>tbody>tr:nth-last-of-type(1)>td:nth-of-type(3)");

        //----- Support & Discussions -----//

        public static By PDP_SupportAndDiscussions_Sec = By.CssSelector("section[id$='discussions']");

        public static By PDP_SupportAndDiscussions_Sec_Lbl = By.CssSelector("section[id$='discussions'] * h2");

        public static By PDP_SupportAndDiscussions_Discussions_Section_Lbl = By.CssSelector("div[class*='discussions'] * h3");

        public static By PDP_SupportAndDiscussions_Ez_Logo = By.CssSelector("div[class$='engineerZone']>div[class$='desktop'] * img[src*='engineerzone']");

        public static By PDP_SupportAndDiscussions_Cn_Ez_Logo = By.CssSelector("div[class$='engineerZone']>div[class$='desktop'] * img[src*='EZ']");

        public static By PDP_SupportAndDiscussions_AllDiscussions_Btn = By.CssSelector("section[id$='discussions'] * a[class^='btn']");

        //----- Sample & Buy -----//

        public static By PDP_SampleAndBuy_Sec_Lbl = By.CssSelector("section[id$='samplebuy'] * h2");

        public static By PDP_SampleAndBuy_Tbl = By.CssSelector("div[id='product-samplebuy'] * div[class$='model table-responsive']>table");

        public static By PDP_SampleAndBuy_Tbl_Automotive_Symbol = By.CssSelector("div[id='product-samplebuy'] * div[class$='model table-responsive']>table * img[alt$='Automotive']");

        public static By PDP_SampleAndBuy_Tbl_PackingQty_Col_Val = By.CssSelector("td[data-id='PackageQty']");

        public static By PDP_SampleAndBuy_Tbl_Sample_Btn = By.CssSelector("div[id='product-samplebuy'] * div[class$='model table-responsive']>table * a[class$='sample']");

        public static By PDP_SampleAndBuy_ViewInventory_Tbl = By.CssSelector("div[id='product-samplebuy'] * div[class$='model table-responsive grid-clone view-inventory']>table");

        public static By PDP_SampleAndBuy_ViewInventory_Tbl_Automotive_Symbol = By.CssSelector("div[id='product-samplebuy'] * div[class$='model table-responsive grid-clone view-inventory']>table * img[alt$='Automotive']");

        public static By PDP_SampleAndBuy_ViewInventory_Tbl_AddToCart_Btn = By.CssSelector("div[id='product-samplebuy'] * div[class$='model table-responsive grid-clone view-inventory']>table * td[data-id='AnalogDevices']>a[class^='btn']");

        public static By PDP_SampleAndBuy_ViewInventory_Tbl_ContactAdi_Link = By.CssSelector("div[id='product-samplebuy'] * div[class$='model table-responsive grid-clone view-inventory']>table * td[data-id='AnalogDevices']>a[href*='contact']");

        public static By PDP_SampleAndBuy_Tbl_Back_Btn = By.CssSelector("div[id='product-samplebuy'] * div[id^='back']");

        public static By PDP_SampleAndBuy_Tbl_SelectACountry_Dd = By.CssSelector("div[id='product-samplebuy']>div>div:nth-of-type(1) * div[class$='country-filter']>div>ul");

        public static By PDP_SampleAndBuy_Tbl_SelectACountry_Dd_Open = By.CssSelector("div[id='product-samplebuy']>div>div:nth-of-type(1) * div[class$='country-filter']>div>ul>li[class$='open']>ul>li");

        public static By PDP_SampleAndBuy_Tbl_SelectACountry_Dd_Btn = By.CssSelector("div[id='product-samplebuy']>div>div:nth-of-type(1) * div[class$='country-filter'] * span[class^='arrow']");

        public static By PDP_SampleAndBuy_Tbl_SelectACountry_Dd_Selected_Val = By.CssSelector("div[id='product-samplebuy']>div>div:nth-of-type(1) * div[class$='country-filter'] * span[data-id]");

        public static By PDP_SampleAndBuy_Tbl_Note = By.CssSelector("div[id='product-samplebuy']>div>div:nth-of-type(1)>div:nth-last-of-type(1)>div:nth-last-of-type(1)>p:nth-of-type(2)");

        public static By PDP_SampleAndBuy_Tbl_CheckInventory_Btn = By.CssSelector("div[id='product-samplebuy'] * div[id='check-inventory']");

        public static By PDP_PriceTableHelp_Link = By.CssSelector("div[id='product-samplebuy'] * div[class='row row-block']>div>p:nth-of-type(3)>a");

        public static By PDP_PriceTableHelp_PopUpBox = By.CssSelector("div[class*='glossary'] * div[class='modal-content']");

        public static By PDP_PriceTableHelp_X_Btn = By.CssSelector("div[class*='glossary'] * button[class='close']");

        public static By PDP_PriceTableHelp_Print_Link = By.CssSelector("div[class*='glossary'] * a[class^='print']");

        public static By PDP_EvaluationBoards_Tbl = By.CssSelector("div[id='product-samplebuy'] * div[class$='evaluation table-responsive']>table");

        public static By PDP_EvaluationBoards_Tbl_Desc_Txt = By.CssSelector("div[id='product-samplebuy']>div>div:nth-of-type(2)>span[class='description']");

        public static By PDP_EvaluationBoards_ViewInventory_Tbl = By.CssSelector("div[id='product-samplebuy'] * div[class$='evaluation table-responsive grid-clone view-inventory']>table");

        public static By PDP_EvaluationBoards_Tbl_SelectACountry_Dd = By.CssSelector("div[id='product-samplebuy']>div>div:nth-of-type(2) * div[class$='country-filter']>div>ul");

        public static By PDP_EvaluationBoards_Tbl_SelectACountry_Dd_Open = By.CssSelector("div[id='product-samplebuy']>div>div:nth-of-type(2) * div[class$='country-filter']>div>ul>li[class$='open']>ul>li");

        public static By PDP_EvaluationBoards_Tbl_SelectACountry_Dd_Btn = By.CssSelector("div[id='product-samplebuy']>div>div:nth-of-type(2) * div[class$='country-filter'] * span[class^='arrow']");

        public static By PDP_EvaluationBoards_Tbl_SelectACountry_Dd_Selected_Val = By.CssSelector("div[id='product-samplebuy']>div>div:nth-of-type(2) * div[class$='country-filter'] * span[data-id]");

        public static By PDP_EvaluationBoards_Tbl_Back_Btn = By.CssSelector("div[id='product-samplebuy']>div>div:nth-of-type(2) * div[class^='back']");

        public static By PDP_EvaluationBoards_ViewInventory_Tbl_AddToCart_Btn = By.CssSelector("div[id='product-samplebuy']>div>div:nth-of-type(2) * a[class^='add-to-cart']");

        public static By PDP_EvaluationBoards_Tbl_CheckInventory_Btn = By.CssSelector("div[id='product-samplebuy']>div>div:nth-of-type(2) * div[class^='check-inventory']");

        public static By PDP_EvaluationBoards_Tbl_Note = By.CssSelector("div[id='product-samplebuy']>div>div:nth-of-type(2)>div:nth-last-of-type(1)>div:nth-last-of-type(1)");

        //----- OBSOLETE PDP -----//

        //----- Sticky Header -----//

        public static By ObsoletePDP_StickyHeader_ProductStatus_Link = By.CssSelector("section[id^='stickyheader'] * a[class='obsolete']>img");

        //----- Overview -----//

        public static By ObsoletePDP_SuggestedReplacementParts_Sec = By.CssSelector("section[id$='overview']>div[class$='alternative-parts']");

        public static By ObsoletePDP_SuggestedReplacementPart_Img = By.CssSelector("div[class='alternate-part']>img");

        public static By ObsoletePDP_SuggestedReplacementPart_PartNo_Link = By.CssSelector("div[class='alternate-part'] * a");

        public static By ObsoletePDP_SuggestedReplacementPart_PartNo_Desc_Txt = By.CssSelector("div[class='alternate-part'] * p[class='description']");

        public static By ObsoletePDP_Overview_UseOurParametricSearchAndSelectionTbls_Link = By.CssSelector("section[id$='overview'] * div>ul>li:nth-of-type(1)>a");

        public static By ObsoletePDP_Overview_UseOurCrossReferenceSearch_Link = By.CssSelector("section[id$='overview'] * div>ul>li:nth-of-type(2)>a");

        //----- DSP -----//

        //----- Sticky Header -----//

        public static By DSP_StickyHeader_Sec = By.CssSelector("section[id^='stickyheader']");

        public static By DSP_Desc_Txt = By.CssSelector("section[id^='stickyheader'] * div[class*='desc']");

        public static By DSP_ShowMoreLess_Link = By.CssSelector("section[id^='stickyheader'] * span[class^='showMoreLess']");

        public static By DSP_Top_Btn = By.CssSelector("section[id^='stickyheader'] * div[class^='scrollTotop']");

        public static By DSP_StickyHeader_Overview = By.CssSelector("a[href$='overview']");

        public static By DSP_StickyHeader_DownloadsAndRelatedSoftware = By.CssSelector("a[href$='relatedsoftware']");

        public static By DSP_StickyHeader_Licensing = By.CssSelector("a[href$='licensing']");

        public static By DSP_StickyHeader_SystemsRequirements = By.CssSelector("a[href$='requirement']");

        public static By DSP_StickyHeader_Documentation = By.CssSelector("a[href$='documentation']");

        public static By DSP_StickyHeader_RelatedHardware = By.CssSelector("a[href$='relatedhardware']");

        public static By DSP_StickyHeader_Discussions = By.CssSelector("a[href$='discussions']");

        public static By DSP_StickyHeader_Buy = By.CssSelector("a[href$='buy']");

        //----- Overview -----//

        public static By DSP_Overview_SoftwareImage = By.CssSelector("div[id='previewProducts'] * div[class$='active'] * img");

        public static By DSP_Overview_Software_PopUp = By.CssSelector("div[id^='product-modal'] * div[class='modal-content']");

        public static By DSP_Overview_Software_PopUp_Title = By.CssSelector("div[id^='product-modal'] * div[class='modal-content'] * h4");

        public static By DSP_Overview_Software_PopUp_Download_Icon = By.CssSelector("div[id^='product-modal'] * div[class='modal-content'] * span[class$='download']");

        public static By DSP_Overview_Software_PopUp_Print_Icon = By.CssSelector("div[id^='product-modal'] * div[class='modal-content'] * span[class$='print']");

        public static By DSP_Overview_Software_PopUp_Img = By.CssSelector("div[id^='product-modal'] * div[class='modal-content'] * div[class$='active'] * img");

        public static By DSP_Overview_FeaturesAndBenefits_Link = By.CssSelector("a[href$='features-and-benefits']");

        public static By DSP_Overview_FeaturesAndBenefits_Content = By.CssSelector("div[id$='features-and-benefits'] ul>li");

        public static By DSP_Overview_ProductDetails_Link = By.CssSelector("a[href$='product-details']");

        public static By DSP_Overview_ProductDetails_Content = By.CssSelector("div[id$='product-details']>p:nth-last-of-type(1)");

        public static By DSP_Overview_CrossCoreSupport_Sec = By.CssSelector("div[id$='product-details'] table * td:nth-of-type(1)");

        public static By DSP_Overview_CrossCoreSupport_Content = By.CssSelector("div[id$='product-details'] table * td:nth-of-type(1)>p");

        public static By DSP_Overview_CrossCoreSupport_ContactSupport_Link = By.CssSelector("div[id$='product-details'] table * td:nth-of-type(1)>p>a:nth-last-of-type(1)");

        public static By DSP_Overview_CompatibleParts_Sec = By.CssSelector("div[class^='overview'] * div[class*='support']");

        public static By DSP_Overview_CompatibleParts_Product_Links = By.CssSelector("div[class^='overview'] * div[class*='support'] * li[style*='list-item']>a");

        public static By DSP_Overview_CompatibleParts_ViewAllLess_Link = By.CssSelector("div[class^='overview'] * div[class*='support'] * a[class$='viewAll']");

        //----- Downloads And Related Software -----//

        public static By DSP_DownloadsAndRelatedSoftware_Tabs = By.CssSelector("section[id$='relatedsoftware'] * div[class$='tabs']>ul>li");

        public static By DSP_DownloadsAndRelatedSoftware_ProductDownloads_Tab_Secs = By.CssSelector("section[id$='relatedsoftware'] * div[class='tab-content']>div[class*='active'] div[class='group']>div>div");

        public static By DSP_DownloadsAndRelatedSoftware_Middleware_Tab_Links = By.CssSelector("section[id$='relatedsoftware'] * div[class='tab-content']>div[class*='active'] * a[data-resource-type='middleware']");

        //----- Systems Requirements -----//

        public static By DSP_SystemsRequirements_Content = By.CssSelector("section[id$='requirement'] * ul>li");

        //----- Documentation -----//

        public static By DSP_Documentation_Sec_Lbl = By.CssSelector("section[id$='documentation'] * h2");

        public static By DSP_Documentation_Tabs = By.CssSelector("section[id$='documentation'] * div[class$='tabs']>ul>li");

        public static By DSP_Documentation_Tabs_Content = By.CssSelector("section[id$='documentation'] * div[class='tab-content']");

        public static By DSP_Documentation_Tab_Links = By.CssSelector("section[id$='documentation'] * div[class='tab-content']>div[class*='active'] * a");

        public static By DSP_Documentation_Tab_FileType_Icons = By.CssSelector("section[id$='documentation'] * div[class='tab-content']>div[class*='active'] * div[class$='IndicatorType']");

        public static By DSP_Documentation_Tab_FileSize_Texts = By.CssSelector("section[id$='documentation'] * div[class='tab-content']>div[class*='active'] * div[class$='IndicatorSize']");

        public static By DSP_Documentation_Tab_Video_Thumbnails = By.CssSelector("section[id$='documentation'] * div[class='tab-content']>div[class*='active'] * a>img");

        public static By DSP_Documentation_Tab_Video_PopUp = By.CssSelector("div[class*='video'] * div[class='modal-content']");

        public static By DSP_Documentation_Tab_Video_PopUp_X_Btn = By.CssSelector("div[class*='video'] * div[class='modal-content'] * button[class='close']");

        //----- Discussions -----//

        public static By DSP_Discussions_Sec_Lbl = By.CssSelector("section[id$='discussions'] * h2");

        //----- Related Hardware -----//

        public static By DSP_RelatedHardware_Section_Lbl = By.CssSelector("section[id$='relatedhardware'] * h2");

        public static By DSP_RelatedHardware_Links = By.CssSelector("section[id$='relatedhardware']>div * a[target]");

        public static By DSP_RelatedHardware_ReferenceDesigns_List = By.CssSelector("section[id$='relatedhardware'] * div[class*='reference-designs-list']");

        //----- Buy -----//

        public static By DSP_Buy_Section_Lbl = By.CssSelector("section[id$='buy'] * h2");

        public static By DSP_Buy_Reminder_Txt = By.CssSelector("section[id$='buy'] * span[class='description']");

        public static By DSP_Buy_Tbl = By.CssSelector("section[id$='buy'] * div[class$='evaluation table-responsive sample-table']>table");

        public static By DSP_Buy_Tbl_Model_Col_Val = By.CssSelector("section[id$='buy'] * div[class$='evaluation table-responsive sample-table']>table * td[data-id='ModelName']");

        public static By DSP_Buy_Tbl_Description_Col_Val = By.CssSelector("section[id$='buy'] * div[class$='evaluation table-responsive sample-table']>table * td[data-id='PackageDetail']");

        public static By DSP_Buy_Tbl_Price_Col_Val = By.CssSelector("section[id$='buy'] * div[class$='evaluation table-responsive sample-table']>table * td[data-id^='Price']");

        public static By DSP_Buy_Tbl_Rohs_Col_Val = By.CssSelector("section[id$='buy'] * div[class$='evaluation table-responsive sample-table']>table * td[data-id$='ROHS']");

        public static By DSP_Buy_Back_Btn = By.CssSelector("section[id$='buy'] * div[class^='back']");

        public static By DSP_Buy_Disabled_Back_Btn = By.CssSelector("section[id$='buy'] * div[class='back-inventory btn btn-primary disabled']");

        public static By DSP_Buy_AddToCart_Btn = By.CssSelector("section[id$='buy'] * a[class^='add-to-cart']");

        public static By DSP_Buy_Disabled_AddToCart_Btn = By.CssSelector("section[id$='buy'] * a[class='add-to-cart btn btn-primary disabled']");

        public static By DSP_Buy_SelectACountry_Dd = By.CssSelector("section[id$='buy'] * div>div:nth-of-type(1) * div[class$='country-filter']>div>ul");

        public static By DSP_Buy_SelectACountry_Dd_Open = By.CssSelector("section[id$='buy'] * div>div:nth-of-type(1) * div[class$='country-filter']>div>ul>li[class$='open']>ul>li");

        public static By DSP_Buy_SelectACountry_Dd_Btn = By.CssSelector("section[id$='buy'] * div>div:nth-of-type(1) * div[class$='country-filter']>div>ul * span[class^='arrow']");

        public static By DSP_Buy_SelectACountry_Dd_Selected_Val = By.CssSelector("section[id$='buy'] * div>div:nth-of-type(1) * div[class$='country-filter']>div>ul * span[data-id]");

        public static By DSP_Buy_CheckInventory_Btn = By.CssSelector("section[id$='buy'] * div[class^='check-inventory']");

        public static By DSP_Buy_Disabled_CheckInventory_Btn = By.CssSelector("section[id$='buy'] * div[class='check-inventory btn btn-primary disabled']");

        public static By DSP_Buy_ViewInventory_Tbl = By.CssSelector("section[id$='buy'] * div[class$='grid-clone view-inventory']>table");

        public static By DSP_Buy_ViewInventory_Tbl_Select_Cb = By.CssSelector("section[id$='buy'] * div[class$='grid-clone view-inventory']>table * input[class^='check']");

        //----- EVAL BOARD -----//

        //----- Navigational Menu -----//

        public static By EvalBoard_Title_Sec = By.CssSelector("section[id^='stickyheader'] * div[class$='titlewrap']");

        public static By EvalBoard_Title_Sec_Name_Txt = By.CssSelector("section[id^='stickyheader'] * div[class='title']");

        public static By EvalBoard_Title_Sec_Desc_Txt = By.CssSelector("section[id^='stickyheader'] * div[class^='description']");

        public static By EvalBoard_Title_Sec_Expanded_Desc_Txt = By.CssSelector("section[id^='stickyheader'] * span[class='rest-of-description']");

        public static By EvalBoard_Title_Sec_ShowMore_ShowLess_Link = By.CssSelector("section[id^='stickyheader'] * span[class^='showMoreLess']");

        public static By EvalBoard_NavigationalMenu_Top_Btn = By.CssSelector("section[id^='stickyheader'] * div[class^='scrollTotop']");

        public static By EvalBoard_NavigationalMenu_Sec = By.CssSelector("section[id^='stickyheader']>div[class^='menu']");

        public static By EvalBoard_NavigationalMenu_Overview = By.CssSelector("a[href$='overview']");

        public static By EvalBoard_NavigationalMenu_Documentation = By.CssSelector("section[id^='stickyheader'] * a[href$='documentation']");

        public static By EvalBoard_NavigationalMenu_Software = By.CssSelector("section[id^='stickyheader'] * a[href$='relatedsoftware']");

        public static By EvalBoard_NavigationalMenu_RelatedHardware = By.CssSelector("a[href$='relatedhardware']");

        public static By EvalBoard_NavigationalMenu_Discussions = By.CssSelector("a[href$='discussions']");

        public static By EvalBoard_NavigationalMenu_SystemRequirements = By.CssSelector("a[href$='systemrequirements']");

        public static By EvalBoard_NavigationalMenu_GettingStarted = By.CssSelector("a[href$='gettingstarted']");

        public static By EvalBoard_NavigationalMenu_ReferenceBoard = By.CssSelector("a[href$='referenceboard']");

        public static By EvalBoard_NavigationalMenu_Buy = By.CssSelector("a[href$='buy']");

        //----- Overview -----//

        public static By EvalBoard_UserGuides_FeaturedBox = By.CssSelector("section[id$='overview'] * a[data-resource-type='user-guides']");

        public static By EvalBoard_EvaluationSoftware_FeaturedBox = By.CssSelector("section[id$='overview'] * a[data-resource-type='evaluation-software']");

        public static By EvalBoard_Overview_Image = By.CssSelector("div[id='previewProducts'] * div[class$='active'] * img");

        public static By EvalBoard_Overview_ZoomIn_Icon = By.CssSelector("a[class^='zoom']>img");

        public static By EvalBoard_Overview_Thumbnail_Image = By.CssSelector("div[id='previewProductsThumnails'] * img");

        public static By EvalBoard_Overview_PopUp = By.CssSelector("div[id^='product-modal'] * div[class='modal-content']");

        public static By EvalBoard_Overview_PopUp_X_Icon = By.CssSelector("div[id^='product-modal'] * div[class='modal-content'] * button[class='close']");

        public static By EvalBoard_Overview_PopUp_Download_Icon = By.CssSelector("div[id^='product-modal'] * div[class='modal-content'] * span[class$='download']");

        public static By EvalBoard_Overview_PopUp_Print_Icon = By.CssSelector("div[id^='product-modal'] * div[class='modal-content'] * span[class$='print']");

        public static By EvalBoard_Overview_PopUp_Img = By.CssSelector("div[id^='product-modal'] * div[class='modal-content'] * div[class$='active'] * img");

        public static By EvalBoard_Overview_PopUp_Pagination = By.CssSelector("div[id^='product-modal'] * div[class='modal-content'] * div[class='owl-dots']>button");

        public static By EvalBoard_Overview_Section_Lbl = By.CssSelector("section[id$='overview'] * h2");

        public static By EvalBoard_Overview_FeaturesAndBenefits_Link = By.CssSelector("a[href$='features-and-benefits']");

        public static By EvalBoard_Overview_FeaturesAndBenefits_Content = By.CssSelector("div[id$='features-and-benefits'] ul>li");

        public static By EvalBoard_Overview_Selected_FeaturesAndBenefits_Tab = By.CssSelector("li[class='active']>a[href$='features-and-benefits']");

        public static By EvalBoard_Overview_ProductDetails_Link = By.CssSelector("a[href$='product-details']");

        public static By EvalBoard_Overview_ProductDetails_Content = By.CssSelector("div[id$='product-details']>p:nth-last-of-type(1)");

        public static By EvalBoard_Overview_All_ApplicableParts_Links = By.CssSelector("ul[id='compatible-parts'] * a");

        public static By EvalBoard_Overview_Displayed_ApplicableParts_Links = By.CssSelector("ul[id='compatible-parts']>li[style*='li']>a");

        public static By EvalBoard_Overview_ApplicableParts_ViewAll_ViewLess_Link = By.CssSelector("a[class$='viewAll']");

        //----- Documentation -----//

        public static By EvalBoard_Documentation_Sec_Lbl = By.CssSelector("section[id$='documentation'] * h2");

        public static By EvalBoard_Documentation_Tabs = By.CssSelector("section[id$='documentation'] * div[class$='tabs']>ul>li");

        public static By EvalBoard_Documentation_Tab_Title = By.CssSelector("section[id$='documentation'] * div[class='tab-content']>div[class*='active'] * h3");

        public static By EvalBoard_Documentation_Tab_Links = By.CssSelector("section[id$='documentation'] * div[class='tab-content']>div[class*='active'] * div>div:nth-of-type(1)>a");

        public static By EvalBoard_Documentation_UserGuides_Links = By.CssSelector("section[id$='documentation'] * a[data-resource-type='user-guides']");

        //----- Software -----//

        public static By EvalBoard_Software_Sec_Lbl = By.CssSelector("section[id$='software'] * h2");

        public static By EvalBoard_Software_Tabs = By.CssSelector("section[id$='software'] * div[class$='tabs']>ul>li");

        public static By EvalBoard_Software_Tab_Title = By.CssSelector("section[id$='software'] * div[class='tab-content']>div[class*='active'] * h3");

        public static By EvalBoard_Software_Tab_Links = By.CssSelector("section[id$='software'] * div[class='tab-content']>div[class*='active'] * div>div:nth-of-type(1)>a");

        public static By EvalBoard_Software_Tab_Exe_Links = By.CssSelector("section[id$='software'] * div[class='tab-content']>div[class*='active'] * div>div:nth-of-type(1)>a[href$='exe']");

        public static By EvalBoard_Software_Tab_Zip_Links = By.CssSelector("section[id$='software'] * div[class='tab-content']>div[class*='active'] * div>div:nth-of-type(1)>a[href$='zip']");

        public static By EvalBoard_Software_Tab_AceSoftwarePage_Link = By.CssSelector("section[id$='software'] * div[class='tab-content']>div[class*='active'] * a[href*='ace-software']");

        public static By EvalBoard_Software_EvaluationSoftware_Links = By.CssSelector("section[id$='software'] * a[data-resource-type='evaluation-software'][name]");

        public static By EvalBoard_Software_Tab_FileType_Icons = By.CssSelector("section[id$='software'] * div[class='tab-content']>div[class*='active'] * span[class$='IndicatorType']");

        //----- Related Hardware -----//

        public static By EvalBoard_RelatedHardware_Sec_Lbl = By.CssSelector("section[id$='relatedhardware'] * h2");

        public static By EvalBoard_RelatedHardware_Links = By.CssSelector("section[id$='relatedhardware']>div * a[href*='analog.com']");

        public static By EvalBoard_RelatedHardware_ReferenceDesigns_List = By.CssSelector("section[id$='relatedhardware'] * div[class*='reference-designs-list']");

        public static By EvalBoard_RelatedHardware_Accordion_Sec = By.CssSelector("section[id$='relatedhardware']>div>div:nth-last-of-type(1) * div[class*='related-accordion']");

        public static By EvalBoard_RelatedHardware_Expand_Accordion_Btn = By.CssSelector("section[id$='relatedhardware']>div>div:nth-last-of-type(1) * div[class*='related-accordion']>div:nth-last-of-type(1) * a[class='expand-icon']");

        public static By EvalBoard_RelatedHardware_Collapse_Accordion_Btn = By.CssSelector("section[id$='relatedhardware']>div>div:nth-last-of-type(1) * div[class*='related-accordion']>div:nth-last-of-type(1) * a[class='close-icon']");

        public static By EvalBoard_RelatedHardware_Collpased_Accordion = By.CssSelector("section[id$='relatedhardware']>div>div:nth-last-of-type(1) * div[class*='related-accordion']>div:nth-last-of-type(1)>div[class$='collapsed']");

        public static By EvalBoard_RelatedHardware_Collpased_Accordion_Header_Txt = By.CssSelector("section[id$='relatedhardware']>div>div:nth-last-of-type(1) * div[class*='related-accordion']>div:nth-last-of-type(1)>div[class$='collapsed'] * h5");

        public static By EvalBoard_RelatedHardware_Collpased_Accordion_Product_Links = By.CssSelector("section[id$='relatedhardware']>div>div:nth-last-of-type(1) * div[class*='related-accordion']>div:nth-last-of-type(1) * div>a");

        public static By EvalBoard_RelatedHardware_Expanded_Accordion = By.CssSelector("section[id$='relatedhardware']>div>div:nth-last-of-type(1) * div[class*='related-accordion']>div:nth-last-of-type(1)>div[class='section-bar']");

        public static By EvalBoard_RelatedHardware_Expanded_Accordion_Links = By.CssSelector("section[id$='relatedhardware']>div>div:nth-last-of-type(1) * div[class*='related-accordion']>div:nth-last-of-type(1)>div[class='product-content'] * a");

        public static By EvalBoard_RelatedHardware_Expanded_Accordion_Desc_Txt = By.CssSelector("section[id$='relatedhardware']>div>div:nth-last-of-type(1) * div[class*='related-accordion']>div:nth-last-of-type(1)>div[class='product-content'] * div[class$='Info']");

        //----- Discussions -----//

        public static By EvalBoard_Discussions_Sec_Lbl = By.CssSelector("section[id$='discussions'] * h2");

        //----- System Requirements -----//

        public static By EvalBoard_SystemRequirements_Sec_Lbl = By.CssSelector("section[id$='systemrequirements'] * h2");

        //----- Getting Started -----//

        public static By EvalBoard_GettingStarted_Sec_Lbl = By.CssSelector("section[id$='gettingstarted'] * h2");

        public static By EvalBoard_SystemRequirements_Sec_Content = By.CssSelector("section[id$='systemrequirements'] * ul>li");

        //----- Reference Board -----//

        public static By EvalBoard_ReferenceBoard_Sec_Lbl = By.CssSelector("section[id$='referenceboard'] * h2");

        public static By EvalBoard_ReferenceBoard_Title_Txt = By.CssSelector("div[id='refernce-board'] * h3");

        public static By EvalBoard_ReferenceBoard_ProductCategories = By.CssSelector("div[id='refernce-board'] * ul[class*='categories']>li");

        public static By EvalBoard_ReferenceBoard_Img = By.CssSelector("div[class='reference-board-circuit'] * img");

        public static By EvalBoard_ReferenceBoard_Pins = By.CssSelector("div[class='reference-board-pins']>span");

        public static By EvalBoard_ReferenceBoard_Pin_ToolTip_ProductCategory_Link = By.CssSelector("div[id='phcontent'] * span[class='title']>a");

        public static By EvalBoard_ReferenceBoard_Pin_ToolTip_PartNo_Link = By.CssSelector("div[id='phcontent'] div[class='content']>a");

        public static By EvalBoard_ReferenceBoard_Pin_ToolTip_ShortDesc_Txt = By.CssSelector("div[id='phcontent'] div[class='content']>p");

        //----- Buy -----//

        public static By EvalBoard_Buy_Section_Lbl = By.CssSelector("section[id$='buy'] * h2");

        public static By EvalBoard_Buy_Tbl = By.CssSelector("section[id$='buy'] * div[class$='evaluation table-responsive sample-table']>table");

        public static By EvalBoard_Buy_Tbl_Rohs_Col_Val = By.CssSelector("section[id$='buy'] * div[class$='evaluation table-responsive sample-table']>table * td[data-id$='ROHS']");

        public static By EvalBoard_Buy_Back_Btn = By.CssSelector("section[id$='buy'] * div[class^='back']");

        public static By EvalBoard_Buy_Disabled_Back_Btn = By.CssSelector("section[id$='buy'] * div[class='back-inventory btn btn-primary disabled']");

        public static By EvalBoard_Buy_AddToCart_Btn = By.CssSelector("section[id$='buy'] * a[class^='add-to-cart']");

        public static By EvalBoard_Buy_Disabled_AddToCart_Btn = By.CssSelector("section[id$='buy'] * a[class='add-to-cart btn btn-primary disabled']");

        public static By EvalBoard_Buy_SelectACountry_Dd = By.CssSelector("section[id$='buy'] * div>div:nth-of-type(1) * div[class$='country-filter']>div>ul");

        public static By EvalBoard_Buy_SelectACountry_Dd_Open = By.CssSelector("section[id$='buy'] * div>div:nth-of-type(1) * div[class$='country-filter']>div>ul>li[class$='open']>ul>li");

        public static By EvalBoard_Buy_SelectACountry_Dd_Btn = By.CssSelector("section[id$='buy'] * div>div:nth-of-type(1) * div[class$='country-filter']>div>ul * span[class^='arrow']");

        public static By EvalBoard_Buy_SelectACountry_Dd_Selected_Val = By.CssSelector("section[id$='buy'] * div>div:nth-of-type(1) * div[class$='country-filter']>div>ul * span[data-id]");

        public static By EvalBoard_Buy_CheckInventory_Btn = By.CssSelector("section[id$='buy'] * div[class^='check-inventory']");

        public static By EvalBoard_Buy_Disabled_CheckInventory_Btn = By.CssSelector("section[id$='buy'] * div[class='check-inventory btn btn-primary disabled']");

        public static By EvalBoard_Buy_ViewInventory_Tbl = By.CssSelector("section[id$='buy'] * div[class$='grid-clone view-inventory']>table");

        public static By EvalBoard_Buy_ViewInventory_Tbl_Select_Cb = By.CssSelector("section[id$='buy'] * div[class$='grid-clone view-inventory']>table * input[class^='check']");

        public static By EvalBoard_Buy_Msg_Txt = By.CssSelector("section[id$='buy'] * div[class='grid-data']>div:nth-last-of-type(1)>div");

        //----- CFTL -----//

        //----- Sticky Header -----//

        public static By CFTL_StickyHeader_Sec = By.CssSelector("section[id^='stickyheader']");

        public static By CFTL_No_Txt = By.CssSelector("section[id^='stickyheader'] * div[class='title']");

        public static By CFTL_Desc_Txt = By.CssSelector("section[id^='stickyheader'] * div[class*='desc']");

        public static By CFTL_ShowMoreLess_Link = By.CssSelector("section[id^='stickyheader'] * span[class^='showMoreLess']");

        public static By CFTL_Top_Btn = By.CssSelector("section[id^='stickyheader'] * div[class^='scrollTotop']");

        public static By CFTL_NavigationalMenu_CircuitFunctionAndBenefits = By.CssSelector("section[id^='stickyheader'] * a[href$='functionbenefits']");

        public static By CFTL_NavigationalMenu_CircuitDescription = By.CssSelector("section[id^='stickyheader'] * a[href$='description']");

        public static By CFTL_NavigationalMenu_CommonVariations = By.CssSelector("section[id^='stickyheader'] * a[href$='commonvariations']");

        public static By CFTL_NavigationalMenu_CircuitEvaluationAndTest = By.CssSelector("section[id^='stickyheader'] * a[href$='evaluationNtest']");

        public static By CFTL_NavigationalMenu_Discussions = By.CssSelector("section[id^='stickyheader'] * a[href$='discussions']");

        public static By CFTL_NavigationalMenu_SampleProducts = By.CssSelector("section[id^='stickyheader'] * a[href$='sampleproducts']");

        //----- Overview -----//

        public static By CFTL_Overview_ReferenceDesigns_Box_Icon = By.CssSelector("section[id$='overview'] * div[class^='datasheet primary'] * img");

        public static By CFTL_Overview_EvaluationHardware_FeaturedBox = By.CssSelector("section[id$='overview'] * a[data-resource-type='design-integration-files']");

        public static By CFTL_Overview_Image = By.CssSelector("div[id='previewProducts'] * div[class$='active'] * img");

        public static By CFTL_Overview_MagnifyingGlass_Icon = By.CssSelector("a[class^='zoom']>img");

        public static By CFTL_Overview_Thumbnail_Images = By.CssSelector("div[id='previewProductsThumnails'] * img");

        public static By CFTL_Overview_PopUp = By.CssSelector("div[id^='product-modal'] * div[class='modal-content']");

        public static By CFTL_Overview_PopUp_X_Icon = By.CssSelector("div[id^='product-modal'] * div[class='modal-content'] * button[class='close']");

        public static By CFTL_Overview_PopUp_Download_Icon = By.CssSelector("div[id^='product-modal'] * div[class='modal-content'] * span[class$='download']");

        public static By CFTL_Overview_PopUp_Print_Icon = By.CssSelector("div[id^='product-modal'] * div[class='modal-content'] * span[class$='print']");

        public static By CFTL_Overview_PopUp_Prev_Arrow_Icon = By.CssSelector("div[id^='product-modal'] * div[class='modal-content'] * button[class$='prev']");

        public static By CFTL_Overview_PopUp_Next_Arrow_Icon = By.CssSelector("div[id^='product-modal'] * div[class='modal-content'] * button[class$='next']");

        public static By CFTL_Overview_Section_Lbl = By.CssSelector("section[id$='overview'] * h2");

        public static By CFTL_Overview_DesignResources_Link = By.CssSelector("a[href$='features-and-benefits']");

        public static By CFTL_Overview_DesignResources_Content = By.CssSelector("div[id$='features-and-benefits'] ul>li");

        public static By CFTL_Overview_DownloadDesignFiles_Link = By.CssSelector("span[class='more-resource']>a[href$='.zip']");

        public static By CFTL_Overview_DownloadDesignFiles_Size_Txt = By.CssSelector("span[class='more-resource']>span[class='fileSize']");

        public static By CFTL_Overview_CheckInventoryAndPurchase_Link = By.CssSelector("span[class='more-resource']>a[href$='sampleproducts']");

        public static By CFTL_Overview_FeaturesAndBenefits_Link = By.CssSelector("a[href$='product-details']");

        public static By CFTL_Overview_FeaturesAndBenefits_Content = By.CssSelector("div[id$='product-details'] ul>li");

        public static By CFTL_Overview_MarketsAndTechnology_Links = By.CssSelector("div[class^='radioverse'] * a[href*='/applications/']");

        public static By CFTL_Overview_All_PartsUsed_Links = By.CssSelector("ul[id='compatible-parts'] * a");

        public static By CFTL_Overview_Displayed_PartsUsed_Links = By.CssSelector("ul[id='compatible-parts']>li[style*='li']>a");

        public static By CFTL_Overview_PartsUsed_ViewAll_ViewLess_Link = By.CssSelector("a[class$='viewAll']");

        //----- Circuit Function & Benefits -----//

        public static By CFTL_CircuitFunctionAndBenefits_Section_Lbl = By.CssSelector("section[id$='functionbenefits'] * h2");

        public static By CFTL_CircuitFunctionAndBenefits_Desc_Txt = By.CssSelector("section[id$='functionbenefits'] * p");

        public static By CFTL_CircuitFunctionAndBenefits_Diagram_Img = By.CssSelector("section[id$='functionbenefits'] * img");

        public static By CFTL_CircuitFunctionAndBenefits_FigureReference_Txt = By.CssSelector("section[id$='functionbenefits'] * figcaption");

        public static By CFTL_CircuitFunctionAndBenefits_MagnifyingGlass_Btn = By.CssSelector("section[id$='functionbenefits'] * div[class$='zoom']");

        public static By CFTL_CircuitFunctionAndBenefits_Image_Modal = By.CssSelector("div[id='image-modal'] * div[class='modal-content']");

        public static By CFTL_CircuitFunctionAndBenefits_Image_Modal_X_Btn = By.CssSelector("div[id='image-modal'] * div[class='modal-content'] * button[class='close']");

        public static By CFTL_CircuitFunctionAndBenefits_Image_Modal_Title = By.CssSelector("div[id^='image-modal'] * div[class='modal-content'] * h4[class$='title']");

        public static By CFTL_CircuitFunctionAndBenefits_Image_Modal_Print_Icon = By.CssSelector("div[id^='image-modal'] * div[class='modal-content'] * span[class$='print']");

        public static By CFTL_CircuitFunctionAndBenefits_Image_Modal_Img = By.CssSelector("div[id^='image-modal'] * div[class='modal-content'] * img");

        //----- Circuit Function & Benefits -----//

        public static By CFTL_CircuitDescription_Section_Lbl = By.CssSelector("section[id$='description'] * h2");

        public static By CFTL_CircuitDescription_Desc_Txt = By.CssSelector("section[id$='description'] * p");

        public static By CFTL_CircuitDescription_Imgs = By.CssSelector("section[id$='description'] * img[class^='img']");

        public static By CFTL_CircuitDescription_MagnifyingGlass_Btn = By.CssSelector("section[id$='description'] * div[class$='zoom']");

        //----- Circuit Function & Benefits -----//

        public static By CFTL_CommonVariations_Section_Lbl = By.CssSelector("section[id$='commonvariations'] * h2");

        //----- Circuit Evaluation & Test -----//

        public static By CFTL_CircuitEvaluationAndTest_Section_Lbl = By.CssSelector("section[id$='evaluationNtest'] * h2");

        //----- Circuit Evaluation & Test -----//

        public static By CFTL_Discussions_Section_Lbl = By.CssSelector("section[id$='discussions'] * h2");

        //----- Sample Products -----//

        public static By CFTL_SampleProducts_Section_Lbl = By.CssSelector("section[id$='sampleproducts'] * h2");

        public static By CFTL_SampleProducts_Samples_Tbl = By.CssSelector("section[id$='sampleproducts'] * div[class^='sample']>table");

        public static By CFTL_SampleProducts_Samples_Tbl_Product_Links = By.CssSelector("section[id$='sampleproducts'] * div[class^='sample'] * td[data-id='Product']>a");

        public static By CFTL_SampleProducts_Samples_Tbl_AvailableProductModelsToSample_Cb = By.CssSelector("section[id$='sampleproducts'] * div[class^='sample'] * td[data-id='Available'] * input");

        public static By CFTL_SampleProducts_Samples_Tbl_AddSelectionsToCart_Btn = By.CssSelector("section[id$='sampleproducts'] * div[class^='sample'] * a[id$='add-select-cart']");

        public static By CFTL_SampleProducts_Samples_Tbl_Disabled_AddSelectionsToCart_Btn = By.CssSelector("section[id$='sampleproducts'] * div[class^='sample'] * a[id$='add-select-cart'][class$='disabled']");

        public static By CFTL_SampleProducts_EvaluationBoards_Tbl_Header_Txt = By.CssSelector("section[id$='sampleproducts'] * div[class='packaging-grid'] * span[class^='header']");

        public static By CFTL_SampleProducts_EvaluationBoards_Tbl_Desc_Txt = By.CssSelector("section[id$='sampleproducts'] * div[class='packaging-grid'] * span[class='description']");

        public static By CFTL_SampleProducts_EvaluationBoards_Tbl = By.CssSelector("div[class='product-section'] * div[class$='evaluation table-responsive sample-table']>table");

        public static By CFTL_SampleProducts_EvaluationBoards_Tbl_Back_Btn = By.CssSelector("section[id$='sampleproducts'] * div[class^='back']");

        public static By CFTL_SampleProducts_EvaluationBoards_Tbl_Disabled_Back_Btn = By.CssSelector("section[id$='sampleproducts'] * div[class='back-inventory btn btn-primary disabled']");

        public static By CFTL_SampleProducts_EvaluationBoards_Tbl_AddToCart_Btn = By.CssSelector("section[id$='sampleproducts'] * a[class^='add-to-cart']");

        public static By CFTL_SampleProducts_EvaluationBoards_Tbl_Disabled_AddToCart_Btn = By.CssSelector("section[id$='sampleproducts'] * a[class='add-to-cart btn btn-primary disabled']");

        public static By CFTL_SampleProducts_EvaluationBoards_Tbl_CheckInventory_Btn = By.CssSelector("section[id$='sampleproducts'] * div[class^='check-inventory']");

        public static By CFTL_SampleProducts_EvaluationBoards_Tbl_Disabled_CheckInventory_Btn = By.CssSelector("section[id$='sampleproducts'] * div[class='check-inventory btn btn-primary disabled']");

        public static By CFTL_SampleProducts_EvaluationBoards_Tbl_SelectACountry_Dd = By.CssSelector("section[id$='sampleproducts'] * div>div:nth-of-type(1) * div[class$='country-filter']>div>ul");

        public static By CFTL_SampleProducts_EvaluationBoards_Tbl_SelectACountry_Dd_Btn = By.CssSelector("section[id$='sampleproducts'] * div>div:nth-of-type(1) * div[class$='country-filter']>div>ul * span[class^='arrow']");

        public static By CFTL_SampleProducts_EvaluationBoards_Tbl_SelectACountry_Dd_Selected_Val = By.CssSelector("section[id$='sampleproducts'] * div>div:nth-of-type(1) * div[class$='country-filter']>div>ul * span[data-id]");

        public static By CFTL_SampleProducts_EvaluationBoards_ViewInventory_Tbl = By.CssSelector("section[id$='sampleproducts'] * div[class$='grid-clone view-inventory']>table");

        public static By CFTL_SampleProducts_EvaluationBoards_ViewInventory_Tbl_Select_Cb = By.CssSelector("section[id$='sampleproducts'] * div[class$='grid-clone view-inventory']>table * input[class^='check']");

        public static By CFTL_Documentation_Section = By.CssSelector("section[id='rd-documentation']");

        public static By CFTL_ReferenceMaterial_Section = By.CssSelector("section[id='rd-reference']");

        //----- BREADCRUMB -----//

        public static By Breadcrumb_Details = By.CssSelector("div[class^='breadcrumb']>div>ol>li");

        public static By Breadcrumb_CurrentPage_Txt = By.CssSelector("div[class^='breadcrumb'] * li[class='active']>span");

        //----- myAnalog Widget -----//

        public static By MyAnalog_Widget_Btn = By.CssSelector("div[class$='SavetoAnalog'] * button[id='myAnalogWidgetBtn']");

        public static By MyAnalog_SaveToMyAnalog_Widget = By.CssSelector("div[class='saveToMyAnalog widget']");

        public static By MyAnalog_Widget_LogInToMyAnalog_Btn = By.CssSelector("div[class='saveToMyAnalog widget'] * button");

        public static By MyAnalog_Widget_Save_Button = By.CssSelector("button[id='saveProductToMyanalog']");

        public static By MyAnalog_Widget_SaveToProject_Label = By.CssSelector("div[id='saveToWidget']>label");

        public static By MyAnalog_Widget_SaveToProject_Dropdown = By.CssSelector("div[id='saveToWidget']>div[class='select-product dropdown']>div");

        public static By MyAnalog_Widget_PSTName_Label = By.CssSelector("div[class='saveToMyAnalog widget']>div>div[class='pst-table-name'] label");

        public static By MyAnalog_Widget_TableName_Label = By.CssSelector("div[class='saveToMyAnalog widget']>div>div[class='pst-table-name'] label");

        public static By MyAnalog_Widget_TableName_TextField = By.CssSelector("div[class='saveToMyAnalog widget']>div>div[class='pst-table-name'] input[id='pstTableName']");

        public static By MyAnalog_Widget_AddNote_Button = By.CssSelector("div[class='saveToMyAnalog widget']>div>div[class='pst-table-name']>div[class*='add item']>button");

        public static By MyAnalog_Widget_AddNote_Textarea = By.CssSelector("textarea[id='tableNote']");

        public static By MyAnalog_Widget_SaveToProject_Btn = By.CssSelector("div[class='saveTo createProject category'] button[id='saveProductToMyanalog']");

        public static By MyAnalog_Widget_SaveToAnalog_Success = By.CssSelector("div[class='saved-message']>h5");

        public static By MyAnalog_Widget_PCNPDN_Notification = By.CssSelector("div[class='pcn-notification']");

        public static By MyAnalog_Widget_DuplicateProject_Validate_Message = By.CssSelector("div[id='createProject']>div[class='input-control']>div[class='validation-message']");

        public static By MyAnalog_Widget_DuplicateCat_Validation_Message = By.CssSelector("div[id='saveToWidget'] div[class='validation-message']");

        public static By MyAnalog_Widget_ViewSavedLink = By.CssSelector("div[id='mySavedFooter']>a");

        //----- Print Widget -----//

        public static By Print_Widget_Btn = By.CssSelector("li[class$='print']>a");

        public static By MyAnalog_Widget_CreateNewProject_Tab = By.CssSelector("li[data-tab='createProject']");

        public static By MyAnalog_Widget_CreateNewProject_Label = By.CssSelector("div[id='createProject'] label");

        public static By MyAnalog_Widget_NewProjectTitle_TextField = By.CssSelector("input[id='newProjectTitle']");

        public static By MyAnalog_Widget_AddDescription_Button = By.CssSelector("div[id='createProject'] button");

        public static By MyAnalog_Widget_AddDescription_TextArea = By.CssSelector("textarea[id='projectDescription']");
        //----- Alliance Element ----//
        public static By Alliance_LandingPage_HeroImage = By.CssSelector("img[title='Alliances']");

        public static By Alliance_LandingPage_FindPartner_Img = By.CssSelector("div[class='first-tilecom'] * img[alt='Find a Partner']");

        public static By Alliance_LandingPage_FindPartner_Btn = By.CssSelector("div[class='first-tilecom']>p[class='analog-rounded-button']>a");

        public static By Alliance_LandingPage_AboutAlliances_Img = By.CssSelector("div[class='second-tilecom'] * img[alt='About Alliances']");

        public static By Alliance_LandingPage_AboutAlliances_LearnMore_Btn = By.CssSelector("div[class='second-tilecom']>p[class='analog-rounded-button']>a");

        public static By Alliance_Application_Modal = By.CssSelector("p[id='modalHtmlBodyRedirect']");

        public static By Alliance_LandingPage_MembersLounge_Img = By.CssSelector("div[class='third-tilecom'] * img[alt='Members Lounge']");

        public static By Alliance_LandingPage_MembersLounge_GoTo_Btn = By.CssSelector("div[class='third-tilecom']>p[class='analog-rounded-button']>a");

        public static By Alliance_MembersLounge_LandingPage_HeroImage = By.CssSelector("img[title='Members Lounge']");

        public static By Alliance_MembersLounge_LeftNav = By.CssSelector("ul[class='nav navbar-collapse collapse']");

        public static By Alliance_MemersLounge_CompanyProfile = By.CssSelector("a[name='My_Company_Profile_']");

        public static By Alliance_ApplicationForm_Email = By.CssSelector("input[id='Email']");

        public static By Alliance_ApplicationForm_CompName = By.CssSelector("input[id='CompanyName']");

        public static By Alliance_ApplicationForm_Fname = By.CssSelector("input[id='FirstName']");

        public static By Alliance_ApplicationForm_Lname = By.CssSelector("input[id='LastName']");

        public static By Alliance_ApplicationForm_Phone = By.CssSelector("input[id='Phone']");

        public static By Alliance_ApplicationForm_SubmitBtn = By.CssSelector("button[id='applicationSubmit']");

        public static By Alliance_ApplicationForm_CompValue = By.CssSelector("textarea[id='CompanyValue']");

        public static By Alliance_ApplicationForm_PartnershipValue = By.CssSelector("textarea[id='PartnershipValue']");

        public static By Alliance_ApplicationForm_SalesRep_Fname = By.CssSelector("input[id='SalesRepresentativeFirstName']");

        public static By Alliance_ApplicationForm_SalesRep_Lname = By.CssSelector("input[id='SalesRepresentativeLastName']");

        public static By Alliance_ApplicationForm_Sponsor_Fname = By.CssSelector("input[id='AllianceInternalContactFirstName']");

        public static By Alliance_ApplicationForm_Sponsor_Lname = By.CssSelector("input[id='AllianceInternalContactLastName']");

        public static By Alliance_ApplicationForm_ActiveEngagement = By.CssSelector("textarea[id='AllianceActiveEngagements']");

        public static By WebContent_Email_TextField = By.CssSelector("input[id='_ctl0__content_txtEmail']");

        public static By WebContent_Search_Btn = By.CssSelector("input[id='_ctl0__content_btnSearch']");

        public static By WebContent_Reject_Btn = By.CssSelector("input[id='_ctl0__content_btnRejected']");

        public static By Alliance_ProfileRegForm_Desc = By.CssSelector("h4[class='page-description']");

        public static By Alliance_ProfileRegForm_CompName = By.CssSelector("input[id='CompanyName']");

        public static By Alliance_ProfileRegForm_CompLogo = By.CssSelector("div[class='form-group company-info'] a[class='btn btn-primary']");

        public static By Alliance_ProfileRegForm_CompOverview = By.CssSelector("textarea[id='CompanyOverview']");

        public static By Alliance_ProfileRegForm_CapabilitiesOverview = By.CssSelector("textarea[id='CapabilitiesOverview']");

        public static By Alliance_ProfileRegForm_FirstName = By.CssSelector("input[id='FirstName']");

        public static By Alliance_ProfileRegForm_LastName = By.CssSelector("input[id='LastName']");

        public static By Alliance_ProfileRegForm_Address = By.CssSelector("input[id='Address']");

        public static By Alliance_ProfileRegForm_City = By.CssSelector("input[id='City']");

        public static By Alliance_ProfileRegForm_State = By.CssSelector("input[id='State']");

        public static By Alliance_ProfileRegForm_ZipCode = By.CssSelector("input[id='ZipCode']");

        public static By Alliance_ProfileRegForm_CountryDD = By.CssSelector("select[id='Country']");

        public static By Alliance_ProfileRegForm_ContactEmail = By.CssSelector("input[id='ContactEmail']");

        public static By Alliance_ProfileRegForm_WebSite = By.CssSelector("input[id='Website']");

        public static By Alliance_ProfileRegForm_Phone = By.CssSelector("input[id='Phone']");

        public static By Alliance_ProfileRegForm_CompExists = By.CssSelector("div[id='companyNamePopover']>div[class='popover-content']");

        public static By Alliance_ProfileRegForm_FB = By.CssSelector("input[id='Facebook']");

        public static By Alliance_ProfileRegForm_LinkedIn = By.CssSelector("input[id='LinkedIn']");

        public static By Alliance_ProfileRegForm_Gplus = By.CssSelector("input[id='GooglePlus']");

        public static By Alliance_ProfileRegForm_Twitter = By.CssSelector("input[id='Twitter']");

        public static By Alliance_ProfileRegForm_Modal = By.CssSelector("p[id='modalHtmlBody']");

        public static By Alliance_ProfileRegForm_SubmitBtn = By.CssSelector("button[id='profileSubmit']");

        public static By Alliance_PartnerPage_FBIcon = By.CssSelector("div[class='page-title title-vertical-top40p title-vertical-bottom30p'] a>i[class='fab fa-facebook-f']");

        public static By Alliance_PartnerPage_LIIcon = By.CssSelector("div[class='page-title title-vertical-top40p title-vertical-bottom30p'] a>i[class='fab fa-linkedin-in']");

        public static By Alliance_PartnerPage_TwitterIcon = By.CssSelector("div[class='page-title title-vertical-top40p title-vertical-bottom30p'] a>i[class='fab fa-twitter']");

        public static By Alliance_PartnerPage_CompanyLogo = By.CssSelector("div[class='partner-details']>div[class='thumb-image pull-left']>img");

        public static By Alliance_PartnerPage_CompanySite = By.CssSelector("div[class='partner-link']>a");

        public static By Alliance_PartnerPage_ContactAlliance = By.CssSelector("div[class='title-vertical-top20p']>a");

        /**********Forms*************/
        /**Note: Other Fields are with same locator, kindly check first if locator is already registered here before creating a new one*****/
        public static By Forms_SaveTimeByLoggingIn_Header = By.CssSelector("h4[id='_content_LogControl1_LoginHeader']>span>a");

        public static By Forms_SaveTimeByLoggingIn_LoginBtn = By.CssSelector("input[id='_content_LogControl1_btnLogin']");

        public static By Forms_FirstName_TxtField = By.CssSelector("input[id='_content_txtFirstName']");

        public static By Forms_FirstName_Label = By.CssSelector("span[id='_content_lblFirstName']");

        public static By Forms_LastName_TxtField = By.CssSelector("input[id='_content_txtLastName']");

        public static By Forms_LastName_Label = By.CssSelector("span[id='_content_lblLastName']");

        public static By Forms_Email_Label = By.CssSelector("span[id='_content_lblEmail']");

        public static By Forms_Email_TxtField = By.CssSelector("input[id='_content_txtEmail']");

        public static By Forms_TelPhone_TxtField = By.CssSelector("input[id='_content_txtTelephone']");

        public static By Forms_TelPhone_Label = By.CssSelector("span[id='_content_lblTelephone']");

        public static By Forms_OrgName_TxtField = By.CssSelector("input[id='_content_txtOrganizationName']");

        public static By Forms_OrgName_Label = By.CssSelector("span[id='_content_lblOrganizationName']");

        //This is for Contact Alliance Only please do not use in other forms unless they are the same//
        public static By Forms_ContactAlliance_CountryDD = By.CssSelector("select[id='_content_ddlCountry']");

        public static By Forms_Country_Label = By.CssSelector("span[id='_content_lblCountry']");

        public static By Forms_ContactAlliance_TypeOfSupp_Label = By.CssSelector("span[id='_content_lblCustomerSupport']");

        public static By Forms_ContactAlliance_TypeOfSupp_RadBtn = By.CssSelector("table[id='_content_rbSupport']");

        public static By Forms_Comments_Label = By.CssSelector("span[id='_content_lblComments']");

        public static By Forms_Comments_TxtField = By.CssSelector("textarea[id='_content_txtComments']");

        //----- DESIGN CENTER -----//

        //----- Landing Page -----//

        public static By DesignCenter_LandingPage_Title_Label = By.CssSelector("h1[data-page-title]");

        public static By DesignCenter_LandingPage_Description_Label = By.CssSelector("div[class*='page-title'] * p");

        public static By DesignCenter_LandingPage_Search_InputBox = By.CssSelector("input[id$='findanswer']");

        public static By DesignCenter_LandingPage_Search_Button = By.CssSelector("button[class='search-icon']");

        //----- Left Rail Navigation -----//

        public static By DesignCenter_LeftRailNav_SimulationModels_Link = By.CssSelector("a[name^='simulation-models']");

        public static By DesignCenter_LeftRailNav_ReferenceDesigns_Link = By.CssSelector("a[name^='reference-designs']");

        public static By DesignCenter_LeftRailNav_EvaluationHardwareAndSoftware_Link = By.CssSelector("a[name^='evaluation-hardware-and-software']");

        public static By DesignCenter_LeftRailNav_PackagingQualitySymbolsAndFootprints_Link = By.CssSelector("a[name^='packaging-quality-symbols-footprints']");

        public static By DesignCenter_LeftRailNav_CircuitDesignToolsAndCalculators_Link = By.CssSelector("a[name*='design-tools-and-calculators']");

        public static By DesignCenter_LeftRailNav_ProcessorsAndDsp_Link = By.CssSelector("a[name^='processors-and-dsp']");

        public static By DesignCenter_LeftRailNav_Selected_Sec_Link = By.CssSelector("ul[class$='left_nav']>li[class='active']>a");

        //----- Search Design Center -----//

        public static By DesignCenter_Search_Sections_Section = By.CssSelector("div[class*='facet-type'] * ul[class='nav']>li");

        public static By DesignCenter_Search_Selected_Section_Filter = By.CssSelector("li[class^=' radio on']>a>span:nth-of-type(1)");

        public static By DesignCenter_Search_Filters_Section = By.CssSelector("div[class*='facet-custom-filters']>div");

        public static By DesignCenter_Search_Filters_Section_DesignResources_Dd_Btn = By.CssSelector("li[data-facetcategory^='design_resource']>a[class='facet-heading']>span");

        public static By DesignCenter_Search_Filters_Section_DesignResources_Dd_Categories_ResultsCount_Lbl = By.CssSelector("li[data-facetcategory^='design_resource']>ul[class^='nav']>li[class*='checkbox']>a>span[class='inactive-badge']");

        public static By DesignCenter_Search_Filters_Section_Expanded_DesignResources_Dd = By.CssSelector("li[class^='active'][data-facetcategory^='design_resource']");

        public static By DesignCenter_Search_Filters_Section_Markets_Dd_Btn = By.CssSelector("li[data-facetcategory^='market']>a[class='facet-heading']>span");

        public static By DesignCenter_Search_Filters_Section_Markets_Dd_Categories_ResultsCount_Lbl = By.CssSelector("li[data-facetcategory^='market']>ul[class^='nav']>li[class*='radio']>a>span[class='inactive-badge']");

        public static By DesignCenter_Search_Filters_Section_Expanded_Markets_Dd = By.CssSelector("li[class^='active'][data-facetcategory^='market']");

        public static By DesignCenter_Search_Filters_Section_Technologies_Dd_Btn = By.CssSelector("li[data-facetcategory^='technology']>a[class='facet-heading']>span");

        public static By DesignCenter_Search_Filters_Section_Technologies_Dd_Categories_ResultsCount_Lbl = By.CssSelector("li[data-facetcategory^='technology']>ul[class^='nav']>li[class*='radio']>a>span[class='inactive-badge']");

        public static By DesignCenter_Search_Filters_Section_Expanded_Technologies_Dd = By.CssSelector("li[class^='active'][data-facetcategory^='technology']");

        public static By DesignCenter_Search_Filters_Section_ProductCategories_Dd_Btn = By.CssSelector("li[data-facetcategory^='prod_cat']>a[class='facet-heading']>span");

        public static By DesignCenter_Search_Filters_Section_ProductCategories_Dd_Categories_ResultsCount_Lbl = By.CssSelector("li[data-facetcategory^='prod_cat']>ul[class^='nav']>li[class*='radio']>a>span[class='inactive-badge']");

        public static By DesignCenter_Search_Filters_Section_Expanded_ProductCategories_Dd = By.CssSelector("li[class^='active'][data-facetcategory^='prod_cat']");

        public static By DesignCenter_Search_Selected_Filter_Filter_Cb = By.CssSelector("li[class*='checkbox on']");

        public static By DesignCenter_Search_Selected_Filter_Filter_Cb_Lbl = By.CssSelector("div[class*='facet-custom-filters'] * li[class*='checkbox on']>a>span:nth-of-type(1)");

        public static By DesignCenter_Search_Selected_Filter_Filter_Rb = By.CssSelector("div[class*='facet-custom-filters'] * li[class*='radio on']>a");

        public static By DesignCenter_Search_Selected_Filter_Filter_Rb_Lbl = By.CssSelector("div[class*='facet-custom-filters'] * li[class*='radio on']>a>span:nth-of-type(1)");

        public static By DesignCenter_Search_InputBx = By.CssSelector("input[id='text-search']");

        public static By DesignCenter_Search_Search_Icon = By.CssSelector("button[class='search-icon']");

        public static By DesignCenter_Search_SearchOrder_Dd = By.CssSelector("select[id='searchOrder']");

        public static By DesignCenter_Search_SearchOrder_Dd_Menu_Newest = By.CssSelector("option[value='DESC']");

        public static By DesignCenter_Search_SearchOrder_Dd_Menu_Oldest = By.CssSelector("option[value='ASC']");

        public static By DesignCenter_Search_SearchOrder_Dd_Menu_Relevancy = By.CssSelector("option[value='relevancy']");

        public static By DesignCenter_Search_SearchSuggestions_Section = By.CssSelector("div[class='search-suggestions']>div");

        public static By DesignCenter_Search_ShowingResultsFor_Label = By.CssSelector("div[class='suggestion-container']>h3");

        public static By DesignCenter_Search_ShowResultsInsteadFor_Label = By.CssSelector("div[class='suggestion-container']>h4");

        public static By DesignCenter_Search_SearchResults_Items = By.CssSelector("div[class='search-results-items']>div>div[class^='search-results-item']");

        public static By DesignCenter_Search_SearchResults_Items_ReferenceDesigns_Col_Header_Txt = By.CssSelector("div[class^='search-results-item']>div[class$='table-head']>div:nth-of-type(1)");

        public static By DesignCenter_Search_SearchResults_Items_DesignResources_Col_Header_Txt = By.CssSelector("div[class^='search-results-item']>div[class$='table-head']>div:nth-of-type(2)");

        public static By DesignCenter_Search_SearchResults_Items_ProductCategories_Col_Header_Txt = By.CssSelector("div[class^='search-results-item']>div[class$='table-head']>div:nth-of-type(3)");

        public static By DesignCenter_Search_SearchResults_Items_ReferenceDesign_Name_Link = By.CssSelector("div[class='search-results-items'] * div[class^='content-title']>div[class^='header']>a");

        public static By DesignCenter_Search_SearchResults_Items_ShortDesc_Txt = By.CssSelector("div[class='search-results-items'] * div[class='description']");

        public static By DesignCenter_Search_SearchResults_Items_DesignResources_Links = By.CssSelector("div[class='search-results-items'] * ul[class='design-resources']");

        public static By DesignCenter_Search_Expanded_SearchResults_Items = By.CssSelector("div[class='search-results-items'] * div[id^='collapse']");

        public static By DesignCenter_Search_SearchResults_Items_PartsUsed_Links = By.CssSelector("div[class$='part-used']>ul>li>a");

        //----- Reference Designs -----//

        public static By DesignCenter_ReferenceDesigns_Page_Title_Txt = By.CssSelector("div[class*='page-title'] * h1");

        public static By DesignCenter_ReferenceDesigns_Desc_Txt = By.CssSelector("div[class*='page-title'] * p");

        public static By DesignCenter_ReferenceDesigns_Search_Sec = By.CssSelector("section[class^='quick-search']");

        public static By DesignCenter_ReferenceDesigns_Search_Sec_SearchByKeywordOrDesignNumber_InputBx = By.CssSelector("input[id='text-search']");

        public static By DesignCenter_ReferenceDesigns_Search_Sec_SearchByKeywordOrDesignNumber_AutoSuggest = By.CssSelector("div[class='tt-menu tt-open']");

        public static By DesignCenter_ReferenceDesigns_Search_Sec_SearchByKeywordOrDesignNumber_AutoSuggest_Items = By.CssSelector("div[class='tt-menu tt-open']>div>p>a");

        public static By DesignCenter_ReferenceDesigns_Search_Sec_Search_Btn = By.CssSelector("div[class='search-filters'] * a[class^='btn']");

        public static By DesignCenter_ReferenceDesigns_Search_Sec_MarketsAndTechnologySolutions_Dd = By.CssSelector("div>ul>li[data-facetcategory^='market']");

        public static By DesignCenter_ReferenceDesigns_Search_Sec_MarketsAndTechnologySolutions_Dd_Sel_Val = By.CssSelector("div>ul>li[data-facetcategory^='market']>a>span");

        public static By DesignCenter_ReferenceDesigns_Search_Sec_MarketsAndTechnologySolutions_Dd_Btn = By.CssSelector("div>ul>li[data-facetcategory^='market']>a");

        public static By DesignCenter_ReferenceDesigns_Search_Sec_MarketsAndTechnologySolutions_Dd_Menu = By.CssSelector("li[data-facetcategory^='market']>ul");

        public static By DesignCenter_ReferenceDesigns_Search_Sec_MarketsAndTechnologySolutions_Dd_Menu_SecDivider = By.CssSelector("li[data-facetcategory^='market']>ul>li>span");

        public static By DesignCenter_ReferenceDesigns_Search_Sec_DesignResources_Dd = By.CssSelector("div>ul>li[data-facetcategory^='design_resource']");

        public static By DesignCenter_ReferenceDesigns_Search_Sec_DesignResources_Dd_Sel_Val = By.CssSelector("div>ul>li[data-facetcategory^='design_resource']>a>span");

        public static By DesignCenter_ReferenceDesigns_Search_Sec_DesignResources_Dd_Btn = By.CssSelector("div>ul>li[data-facetcategory^='design_resource']>a");

        public static By DesignCenter_ReferenceDesigns_Search_Sec_DesignResources_Dd_Menu = By.CssSelector("ul>li[data-facetcategory^='design_resource']>ul");

        public static By DesignCenter_ReferenceDesigns_Search_Sec_ProductCategories_Dd = By.CssSelector("div>div>ul>li[data-facetcategory^='prod_cat']");

        public static By DesignCenter_ReferenceDesigns_Search_Sec_ProductCategories_Dd_Sel_Val = By.CssSelector("div>ul>li[data-facetcategory^='prod_cat']>a>span");

        public static By DesignCenter_ReferenceDesigns_Search_Sec_ProductCategories_Dd_Btn = By.CssSelector("div>div>ul>li[data-facetcategory^='prod_cat']>a");

        public static By DesignCenter_ReferenceDesigns_Search_Sec_ProductCategories_Dd_Menu = By.CssSelector("ul>li[data-facetcategory^='prod_cat']>ul");

        public static By DesignCenter_ReferenceDesigns_Search_Sec_ProductCategories_Dd_SubMenu = By.CssSelector("ul>li[data-facetcategory^='prod_cat']>div[class='sublinks']>ul");

        public static By DesignCenter_ReferenceDesigns_NewDesigns_Sec = By.CssSelector("section[class='new-designs']");

        public static By DesignCenter_ReferenceDesigns_NewDesigns_Sec_MostRecentlyLaunched_Item = By.CssSelector("section[class='new-designs'] * div[class='row']>a>div");

        public static By DesignCenter_ReferenceDesigns_NewDesigns_Sec_MostRecentlyLaunched_Item_Img = By.CssSelector("section[class='new-designs'] * div>img");

        public static By DesignCenter_ReferenceDesigns_NewDesigns_Sec_Items = By.CssSelector("section[class='new-designs'] * div>ul>li");

        public static By DesignCenter_ReferenceDesigns_NewDesigns_Sec_Item_ReferenceDesign_Name_Link = By.CssSelector("section[class='new-designs'] * div>ul>li * a");

        public static By DesignCenter_ReferenceDesigns_NewDesigns_Sec_Item_ReferenceDesign_ShortDesc_Txt = By.CssSelector("section[class='new-designs'] * div>ul>li>p");

        public static By DesignCenter_ReferenceDesigns_NewDesigns_ViewAllNewReferenceDesigns_Btn = By.CssSelector("section[class='new-designs'] * a[class^='btn']");

        public static By DesignCenter_ReferenceDesigns_SubSecs = By.CssSelector("div[class='content']>div>div>div[name*='designcenter']>div>div");

        public static By DesignCenter_ReferenceDesigns_FpgaHdlCode_Link = By.CssSelector("a[name^='fpga-hdl-code']");

        public static By DesignCenter_ReferenceDesigns_LinuxDeviceDrivers_Link = By.CssSelector("a[name^='linux-device-drivers']");

        public static By DesignCenter_ReferenceDesigns_NoOsDrivers_Link = By.CssSelector("a[name^='no-os-drivers']");

        //----- Circuit Design Tools & Calculators -----//

        public static By DesignCenter_CircuitDesignToolsAndCalculators_LeftNav_Subsec_Links = By.CssSelector("li[class='active'] * a[href*='design-tools-and-calculators']");

        public static By DesignCenter_CircuitDesignToolsAndCalculators_LeftNav_AmplifierAndLinear_Link = By.CssSelector("a[name^='amplifier-and-linear']");

        public static By DesignCenter_CircuitDesignToolsAndCalculators_LeftNav_ClockAndTiming_Link = By.CssSelector("a[name^='clock-and-timing']");

        public static By DesignCenter_CircuitDesignToolsAndCalculators_LeftNav_DataConverter_Link = By.CssSelector("a[name^='converter']");

        public static By DesignCenter_CircuitDesignToolsAndCalculators_LeftNav_PowerManagement_Link = By.CssSelector("a[name^='power-management']");

        public static By DesignCenter_CircuitDesignToolsAndCalculators_LeftNav_RfAndSynthesis_Link = By.CssSelector("a[name^='rf-and-synthesis']");

        public static By DesignCenter_CircuitDesignToolsAndCalculators_LeftNav_SelectedVal = By.CssSelector("ul[class$='left_nav'] * a[class='selected']");

        public static By DesignCenter_CircuitDesignToolsAndCalculators_Page_Title_Txt = By.CssSelector("div[class='page-title']>h1");

        public static By DesignCenter_CircuitDesignToolsAndCalculators_Accordions = By.CssSelector("div[class$='accordion-comp']");

        public static By DesignCenter_CircuitDesignToolsAndCalculators_Spice_Accordion_Title_Expand_Btn = By.CssSelector("div[id='section-heading0'] * div[class='accordion-expand-all']>a[class='expand']");

        public static By DesignCenter_CircuitDesignToolsAndCalculators_Expanded_Spice_Accordion = By.CssSelector("div[id='section-heading0']>div>div[class$='expand-all']");

        public static By DesignCenter_CircuitDesignToolsAndCalculators_LtSpice_Img = By.CssSelector("a[href*='ltspice']>img");

        public static By DesignCenter_CircuitDesignToolsAndCalculators_LtSpice_Desc_Txt = By.CssSelector("div[id='section-heading0'] * div[class^='adi-accordion']>div:nth-of-type(2)>div[class^='product-content']>div>div:nth-of-type(2)");

        public static By DesignCenter_CircuitDesignToolsAndCalculators_LtSpice_LearnMore_Link = By.CssSelector("div[class*='accordion'] * a[name^='ltspice']");

        //----- Circuit Design Tools & Calculators > Amplifier & Linear Tools -----//

        public static By DesignCenter_AmplifierAndLinearTools_DesignTools_Links = By.CssSelector("div[class$='related-items']>ul>li * a");

        public static By DesignCenter_AmplifierAndLinearTools_DesignTools_Desc_Txts = By.CssSelector("div[class$='related-items']>ul>li div[class='links']>span");

        //----- Circuit Design Tools & Calculators > Clock & Timing Tools -----//

        public static By DesignCenter_ClockAndTimingTools_AdiSimClk_Sec = By.CssSelector("div[name*='ADIsimCLK']");

        public static By DesignCenter_ClockAndTimingTools_AdiSimClk_Btn = By.CssSelector("div[name*='ADIsimCLK'] * span[class$='button']");

        public static By DesignCenter_ClockAndTimingTools_AdiSimClk_Img = By.CssSelector("div[name*='ADIsimCLK'] * img");

        public static By DesignCenter_ClockAndTimingTools_DesignTools_Links = By.CssSelector("div[class$='related-items']>ul>li * a");

        public static By DesignCenter_ClockAndTimingTools_DesignTools_Desc_Txts = By.CssSelector("div[class$='related-items']>ul>li div[class='links']>span");

        //----- Circuit Design Tools & Calculators > Data Converter Tools -----//

        public static By DesignCenter_DataConverterTools_DesignTools_Links = By.CssSelector("div[class$='related-items']>ul>li * a");

        public static By DesignCenter_DataConverterTools_DesignTools_Desc_Txts = By.CssSelector("div[class$='related-items']>ul>li div[class='links']>span");

        //----- Circuit Design Tools & Calculators > LTspice -----//

        public static By DesignCenter_LtSpice_DownloadForWindows_Btn = By.CssSelector("div[class='container']>div>p:nth-of-type(2) a");

        public static By DesignCenter_LtSpice_DownloadForWindows_UpdatedDate_Txt = By.CssSelector("div[class='container']>div>p:nth-of-type(3)>span:nth-last-of-type(1)");

        public static By DesignCenter_LtSpice_DownloadForMac_Btn = By.CssSelector("div[class='container']>div>p:nth-of-type(3) a");

        public static By DesignCenter_LtSpice_DownloadForMac_UpdatedDate_Txt = By.CssSelector("div[class='container']>div>p:nth-of-type(3)>span:nth-last-of-type(1)");

        public static By DesignCenter_LtSpice_DownloadLtSpice_Note_Txt = By.CssSelector("div[class='container']>div>p:nth-last-of-type(1)");

        public static By DesignCenter_LtSpice_ViewOurTechnicalArticles_Link = By.CssSelector("p>a[href*='search.html']:nth-of-type(1)");

        public static By DesignCenter_LtSpice_ViewOurTechnicalVideos_Link = By.CssSelector("p>a[href*='search.html']:nth-of-type(2)");

        //----- Circuit Design Tools & Calculators > Power Management Tools -----//

        public static By DesignCenter_PowerManagementTools_DesignTools_Links = By.CssSelector("div[class$='related-items']>ul>li * a");

        public static By DesignCenter_PowerManagementTools_DesignTools_Desc_Txts = By.CssSelector("div[class$='related-items']>ul>li div[class='links']>span");

        //----- Circuit Design Tools & Calculators > RF & Synthesis Tools -----//

        public static By DesignCenter_RfAndSynthesisTools_DesignTools_Links = By.CssSelector("div[class$='related-items']>ul>li * a");

        public static By DesignCenter_RfAndSynthesisTools_DesignTools_Desc_Txts = By.CssSelector("div[class$='related-items']>ul>li div[class='links']>span");

        //----- Circuit Design Tools & Calculators > LTspice > LTspice Demo Circuits -----//

        public static By DesignCenter_LtSpiceDemoCircuits_Page_Title_Txt = By.CssSelector("div[class='page-title']>h1");

        public static By DesignCenter_LtSpiceDemoCircuits_Search_Txtbox = By.CssSelector("input[type='search']");

        public static By DesignCenter_LtSpiceDemoCircuits_Tbl = By.CssSelector("table[id^='DataTables_Table']");

        public static By DesignCenter_LtSpiceDemoCircuits_Product_Search_Txtbox = By.CssSelector("tr[class='filters']>th:nth-of-type(1)>input");

        public static By DesignCenter_LtSpiceDemoCircuits_PostedDate_Search_Txtbox = By.CssSelector("th[class$='desc']>input");

        public static By DesignCenter_LtSpiceDemoCircuits_DemonstrationCircuit_Search_Txtbox = By.CssSelector("tr[class='filters']>th:nth-last-of-type(1)>input");

        public static By DesignCenter_LtSpiceDemoCircuits_Tbl_Search_Results = By.CssSelector("table>tbody>tr[role='row']");

        public static By DesignCenter_LtSpiceDemoCircuits_Tbl_Empty_Search_Results = By.CssSelector("table * td[class='dataTables_empty']");

        public static By DesignCenter_LtSpiceDemoCircuits_Tbl_Product_Links = By.CssSelector("table * a[href*='products']");

        //----- Evaluation Hardware & Software -----//

        public static By DesignCenter_EvaluationHardwareAndSoftware_LeftRailNav_Sec = By.CssSelector("li[class='active']>a[name^='evaluation-hardware-and-software']");

        public static By DesignCenter_EvaluationHardwareAndSoftware_LeftRailNav_Subsec_Links = By.CssSelector("li[class='active'] * a[href*='evaluation-hardware-and-software']");

        public static By DesignCenter_EvaluationHardwareAndSoftware_LeftRailNav_ProductEvaluationBoardsAndKits_Link = By.CssSelector("li[class='active'] * a[name^='evaluation-boards-kits']");

        public static By DesignCenter_EvaluationHardwareAndSoftware_LeftRailNav_EvaluationAndDevelopmentPlatforms_Link = By.CssSelector("li[class='active'] * a[name^='evaluation-development-platforms");

        public static By DesignCenter_EvaluationHardwareAndSoftware_Page_Title_Txt = By.CssSelector("div[class='page-title']>h1");

        public static By DesignCenter_EvaluationHardwareAndSoftware_Desc_Txt = By.CssSelector("p[id$='PageDesc']");

        public static By DesignCenter_EvaluationHardwareAndSoftware_Subsec_Title_Txt = By.CssSelector("h3[class*='header']");

        public static By DesignCenter_EvaluationHardwareAndSoftware_Subsec_Desc_Txt = By.CssSelector("div[class^='container'] div[name*='designcenter'] * p");

        public static By DesignCenter_EvaluationHardwareAndSoftware_ProductEvaluationBoardsAndKits_Sec_ViewMore_Link = By.CssSelector("div[name*='designcenter'] * a[name^='evaluation-boards-kits']");

        public static By DesignCenter_EvaluationHardwareAndSoftware_EvaluationAndDevelopmentPlatforms_Sec_ViewMore_Link = By.CssSelector("div[name*='designcenter'] * a[name^='evaluation-development-platforms']");

        public static By DesignCenter_EvaluationHardwareAndSoftware_EvaluationAndDevelopmentPlatforms_Sec_Links = By.CssSelector("div[class='resource-list'] * a[href*='evaluation-development-platforms']");

        public static By DesignCenter_EvaluationHardwareAndSoftware_Ced_Link = By.CssSelector("a[name^='ced']");

        public static By DesignCenter_EvaluationHardwareAndSoftware_Dpg_Link = By.CssSelector("a[name^='dpg']");

        public static By DesignCenter_EvaluationHardwareAndSoftware_Sdp_Link = By.CssSelector("a[name^='sdp']");

        //----- Evaluation Hardware & Software > Product Evaluation Boards and Kits -----//

        public static By DesignCenter_ProductEvaluationBoardsAndKits_Page_Title_Txt = By.CssSelector("div[class='page-title']>h1");

        public static By DesignCenter_ProductEvaluationBoardsAndKits_Desc_Txt = By.CssSelector("p[id$='PageDesc']");

        public static By DesignCenter_ProductEvaluationBoardsAndKits_Search_Txtbox = By.CssSelector("input[id='text-search']");

        public static By DesignCenter_ProductEvaluationBoardsAndKits_Search_Txtbox_Autosuggest_Dd = By.CssSelector("div[class='tt-menu tt-open']");

        //----- Evaluation Hardware & Software > Evaluation & Development Platforms -----//

        public static By DesignCenter_EvaluationAndDevelopmentPlatforms_Page_Title_Txt = By.CssSelector("div[class='page-title']>h1");

        public static By DesignCenter_EvaluationAndDevelopmentPlatforms_Desc_Txt = By.CssSelector("p[id$='PageDesc']");

        public static By DesignCenter_EvaluationAndDevelopmentPlatforms_SubSecs = By.CssSelector("div[name*='designcenter']>div>div");

        public static By DesignCenter_EvaluationAndDevelopmentPlatforms_Ced_Sec_ViewMore_Link = By.CssSelector("a[name^='ced']");

        public static By DesignCenter_EvaluationAndDevelopmentPlatforms_Dpg_Sec_ViewMore_Link = By.CssSelector("a[name^='dpg']");

        public static By DesignCenter_EvaluationAndDevelopmentPlatforms_Sdp_Sec_ViewMore_Link = By.CssSelector("a[name^='sdp']");

        public static By DesignCenter_EvaluationAndDevelopmentPlatforms_Mems_Sec_ViewMore_Link = By.CssSelector("a[name^='inertial-mems-sensor-evaluation-tools']");

        //----- Evaluation Hardware & Software > Evaluation & Development Platforms > Converter Evaluation and Development Board (CED) -----//

        public static By DesignCenter_Ced_Page_Title_Txt = By.CssSelector("div[class='page-title']>h1");

        public static By DesignCenter_Ced_Desc_Txt = By.CssSelector("p[id$='PageDesc']");

        public static By DesignCenter_Ced_Img = By.CssSelector("img[alt^='ced']");

        //----- Evaluation Hardware & Software > Evaluation & Development Platforms > Data Pattern Generator (DPG) High-Speed DAC Evaluation Platform -----//

        public static By DesignCenter_Dpg_Page_Title_Txt = By.CssSelector("div[class='page-title']>h1");

        public static By DesignCenter_Dpg_Desc_Txt = By.CssSelector("p[id$='PageDesc']");

        public static By DesignCenter_Dpg_CompatibleProductEvaluationBoards_Sec_Desc_Txt = By.CssSelector("div[id^='section-heading']:nth-last-of-type(1) *  div[class='accordion-description']");

        public static By DesignCenter_Dpg_CompatibleProductEvaluationBoards_Sec_RecommendedController_Txt = By.CssSelector("div[id^='section-heading']:nth-last-of-type(1) * h5[class='pull-right']");

        public static By DesignCenter_Dpg_CompatibleProductEvaluationBoards_Sec_Expand_Accordion_Btn = By.CssSelector("div[id^='section-heading']:nth-last-of-type(1) * a[class='expand']");

        public static By DesignCenter_Dpg_CompatibleProductEvaluationBoards_Sec_Category_Name_Txts = By.CssSelector("div[id^='section-heading']:nth-last-of-type(1) * div[class^='section-bar'] * h5");

        public static By DesignCenter_Dpg_CompatibleProductEvaluationBoards_Sec_Product_Links = By.CssSelector("div[id^='section-heading']:nth-last-of-type(1) * a[href*='/products/']");

        //----- Evaluation Hardware & Software > Evaluation & Development Platforms > System Demonstration Platform (SDP) -----//

        public static By DesignCenter_Sdp_SubSecs = By.CssSelector("div[class$='column-content']>div");

        public static By DesignCenter_Sdp_ViewCompatibleEvaluationBoards_Btn = By.CssSelector("a[class$='button'][href*='Compatible-Boards']");

        public static By DesignCenter_Sdp_Accordions = By.CssSelector("div[class^='adi-accordion']");

        public static By DesignCenter_Sdp_ViewSDPCompatibleProductEvaluationBoards_Link = By.CssSelector("a[name^='Product_Evaluation_Boards']");

        public static By DesignCenter_Sdp_ViewSDPCompatibleReferenceCircuits_Link = By.CssSelector("a[name^='Circuits_from_the_Lab']");

        //----- Evaluation Hardware & Software > Evaluation & Development Platforms > Inertial MEMS Sensor Evaluation Tools -----//

        public static By DesignCenter_Mems_Page_Title_Txt = By.CssSelector("div[class='page-title']>h1");

        public static By DesignCenter_Mems_Desc_Txt = By.CssSelector("p[id$='PageDesc']");

        public static By DesignCenter_Mems_SubSecs = By.CssSelector("div[class$='column-content']>div");

        public static By DesignCenter_Mems_Accordions = By.CssSelector("div[class^='adi-accordion']");

        //----- Simulation Models -----//

        public static By DesignCenter_SimulationModels_LeftRailNav_Sec = By.CssSelector("li[class='active']>a[name^='simulation-models']");

        public static By DesignCenter_SimulationModels_LeftRailNav_Subsec_Links = By.CssSelector("li[class='active'] * a[href*='simulation-models']");

        public static By DesignCenter_SimulationModels_LeftRailNav_BsdlModels_Link = By.CssSelector("li>a[name^='bsdl-model']");

        public static By DesignCenter_SimulationModels_LeftRailNav_IbisModels_Link = By.CssSelector("li>a[name^='ibis-models']");

        public static By DesignCenter_SimulationModels_LeftRailNav_SParameters_Link = By.CssSelector("li>a[name^='s-parameters']");

        public static By DesignCenter_SimulationModels_LeftRailNav_SpiceModels_Link = By.CssSelector("li>a[name^='spice-models']");

        public static By DesignCenter_SimulationModels_LeftRailNav_SysParameterModelsForKeysightGenesys_Link = By.CssSelector("li>a[name^='sys-parameter-models-keysight-genesys']");

        public static By DesignCenter_SimulationModels_LeftRailNav_Selected_Val_Link = By.CssSelector("ul[class$='left_nav'] * a[class='selected']");

        public static By DesignCenter_SimulationModels_Page_Title_Txt = By.CssSelector("div[class='page-title']>h1");

        public static By DesignCenter_SimulationModels_SubCat_Name_Txts = By.CssSelector("h3[class*='header']");

        public static By DesignCenter_SimulationModels_SubCat_Desc_Txts = By.CssSelector("div[class^='container'] div[name*='designcenter'] * p");

        public static By DesignCenter_SimulationModels_SubCat_ViewMore_Links = By.CssSelector("div[name*='designcenter'] * a[href*='simulation-models']");

        //----- Simulation Models > BSDL Models -----//

        public static By DesignCenter_BsdlModels_Page_Title_Txt = By.CssSelector("div[class='page-title']>h1");

        public static By DesignCenter_BsdlModels_Search_Txtbox = By.CssSelector("input[type='search']");

        public static By DesignCenter_BsdlModels_Tbl = By.CssSelector("table[id^='DataTables_Table']");

        public static By DesignCenter_BsdlModels_Product_Search_Txtbox = By.CssSelector("tr[class='filters']>th:nth-of-type(1)>input");

        public static By DesignCenter_BsdlModels_Description_Search_Txtbox = By.CssSelector("th[class$='desc']>input");

        public static By DesignCenter_BsdlModels_ModelFile_Search_Txtbox = By.CssSelector("tr[class='filters']>th:nth-last-of-type(1)>input");

        public static By DesignCenter_BsdlModels_Tbl_Search_Results = By.CssSelector("table>tbody>tr[role='row']");

        public static By DesignCenter_BsdlModels_Tbl_Product_Links = By.CssSelector("table * a[href*='products']");

        //----- Simulation Models > IBIS Models -----//

        public static By DesignCenter_IbisModels_Page_Title_Txt = By.CssSelector("div[class='page-title']>h1");

        public static By DesignCenter_IbisModels_Search_Txtbox = By.CssSelector("input[type='search']");

        public static By DesignCenter_IbisModels_Tbl = By.CssSelector("table[id^='DataTables_Table']");

        public static By DesignCenter_IbisModels_Product_Search_Txtbox = By.CssSelector("tr[class='filters']>th:nth-of-type(1)>input");

        public static By DesignCenter_IbisModels_Description_Search_Txtbox = By.CssSelector("th[class$='desc']>input");

        public static By DesignCenter_IbisModels_ModelFile_Search_Txtbox = By.CssSelector("tr[class='filters']>th:nth-last-of-type(1)>input");

        public static By DesignCenter_IbisModels_Tbl_Search_Results = By.CssSelector("table>tbody>tr[role='row']");

        public static By DesignCenter_IbisModels_Tbl_Product_Links = By.CssSelector("table * a[href*='products']");

        public static By DesignCenter_IbisModels_Tbl_ModelFile_Links = By.CssSelector("table * ul[class='linked-resources'] * a");

        //----- Simulation Models > S-Parameters -----//

        public static By DesignCenter_SParameters_Page_Title_Txt = By.CssSelector("div[class='page-title']>h1");

        public static By DesignCenter_SParameters_Search_Txtbox = By.CssSelector("input[type='search']");

        public static By DesignCenter_SParameters_Tbl = By.CssSelector("table[id^='DataTables_Table']");

        public static By DesignCenter_SParameters_Product_Search_Txtbox = By.CssSelector("tr[class='filters']>th:nth-of-type(1)>input");

        public static By DesignCenter_SParameters_Description_Search_Txtbox = By.CssSelector("th[class$='desc']>input");

        public static By DesignCenter_SParameters_ModelFile_Search_Txtbox = By.CssSelector("tr[class='filters']>th:nth-last-of-type(1)>input");

        public static By DesignCenter_SParameters_Tbl_Search_Results = By.CssSelector("table>tbody>tr[role='row']");

        public static By DesignCenter_SParameters_Tbl_Product_Links = By.CssSelector("table * a[href*='products']");

        //----- Simulation Models > SPICE Models -----//

        public static By DesignCenter_SpiceModels_Page_Title_Txt = By.CssSelector("div[class='page-title']>h1");

        public static By DesignCenter_SpiceModels_Search_Txtbox = By.CssSelector("input[type='search']");

        public static By DesignCenter_SpiceModels_Tbl = By.CssSelector("table[id^='DataTables_Table']");

        public static By DesignCenter_SpiceModels_Product_Search_Txtbox = By.CssSelector("tr[class='filters']>th:nth-of-type(1)>input");

        public static By DesignCenter_SpiceModels_Description_Search_Txtbox = By.CssSelector("th[class$='desc']>input");

        public static By DesignCenter_SpiceModels_ModelFile_Search_Txtbox = By.CssSelector("tr[class='filters']>th:nth-last-of-type(1)>input");

        public static By DesignCenter_SpiceModels_Tbl_Search_Results = By.CssSelector("table>tbody>tr[role='row']");

        public static By DesignCenter_SpiceModels_Tbl_Product_Links = By.CssSelector("table * a[href*='products']");

        public static By DesignCenter_SpiceModels_Tbl_ModelFile_Links = By.CssSelector("table * ul[class='linked-resources'] * a");

        //----- Simulation Models > S-Parameters -----//

        public static By DesignCenter_MathWorksBehavioralModels_Page_Title_Txt = By.CssSelector("div[class='page-title']>h1");

        public static By DesignCenter_MathWorksBehavioralModels_Search_Txtbox = By.CssSelector("input[type='search']");

        public static By DesignCenter_MathWorksBehavioralModels_Tbl = By.CssSelector("table[id^='DataTables_Table']");

        public static By DesignCenter_MathWorksBehavioralModels_Product_Search_Txtbox = By.CssSelector("tr[class='filters']>th:nth-of-type(1)>input");

        public static By DesignCenter_MathWorksBehavioralModels_Description_Search_Txtbox = By.CssSelector("th[class$='desc']>input");

        public static By DesignCenter_MathWorksBehavioralModels_ModelFile_Search_Txtbox = By.CssSelector("tr[class='filters']>th:nth-last-of-type(1)>input");

        public static By DesignCenter_MathWorksBehavioralModels_Tbl_Search_Results = By.CssSelector("table>tbody>tr[role='row']");

        public static By DesignCenter_MathWorksBehavioralModels_Tbl_Product_Links = By.CssSelector("table * a[href*='products']");

        //----- Simulation Models > Sys-Parameter Models for Keysight Genesys -----//

        public static By DesignCenter_SysParameterModelsForKeysightGenesys_Page_Title_Txt = By.CssSelector("div[class='page-title']>h1");

        public static By DesignCenter_SysParameterModelsForKeysightGenesys_Video = By.CssSelector("div[class='vjs-poster']");

        public static By DesignCenter_SysParameterModelsForKeysightGenesys_DownloadNow_Btn = By.CssSelector("span[class='analog-rounded-button']>a");

        public static By DesignCenter_SysParameterModelsForKeysightGenesys_Download_Link = By.CssSelector("div[class='container'] * p>a");

        public static By DesignCenter_SysParameterModelsForKeysightGenesys_ModelLibraryVerChecker_Title_Txt = By.CssSelector("body>h1");

        public static By DesignCenter_SysParameterModelsForKeysightGenesys_ModelLibraryVerChecker_Desc_Txt = By.CssSelector("body>div>p");

        public static By DesignCenter_SysParameterModelsForKeysightGenesys_ModelLibraryVerChecker_Here_Link = By.CssSelector("body>div>p>a");

        //----- Packaging, Quality, Symbols & Footprints -----//

        public static By DesignCenter_PackagingQualitySymbolsAndFootprints_LeftRailNav = By.CssSelector("ul[class$='left_nav']>li[class='active']");

        public static By DesignCenter_PackagingQualitySymbolsAndFootprints_LeftRailNav_PackageIndex_Link = By.CssSelector("li>a[name^='package-index']");

        public static By DesignCenter_PackagingQualitySymbolsAndFootprints_LeftRailNav_PackageResources_Link = By.CssSelector("li>a[name^='package-resources']");

        public static By DesignCenter_PackagingQualitySymbolsAndFootprints_LeftRailNav_QualityAndReliability_Link = By.CssSelector("li>a[name^='quality-reliability']");

        public static By DesignCenter_PackagingQualitySymbolsAndFootprints_LeftRailNav_SymbolsFootprintsAnd3dModels_Link = By.CssSelector("li>a[name^='symbols-and-footprints']");

        public static By DesignCenter_PackagingQualitySymbolsAndFootprints_LeftRailNav_Selected_Sec_Link = By.CssSelector("ul[class$='left_nav']>li[class='active']>a");

        public static By DesignCenter_PackagingQualitySymbolsAndFootprints_LeftRailNav_Selected_Val_Link = By.CssSelector("ul[class$='left_nav'] * a[class='selected']");

        public static By DesignCenter_PackagingQualitySymbolsAndFootprints_Page_Title_Txt = By.CssSelector("div[class='page-title']>h1");

        //----- Packaging, Quality, Symbols & Footprints > Package Index -----//

        public static By DesignCenter_PackageIndex_Page_Title_Txt = By.CssSelector("div[class='page-title']>h1");

        public static By DesignCenter_PackageIndex_Desc_Txt = By.CssSelector("p[id$='PageDesc']");

        public static By DesignCenter_PackageIndex_SubCat_Header_Txts = By.CssSelector("h3[class*='header']");

        public static By DesignCenter_PackageIndex_SubCat_Desc_Txts = By.CssSelector("div[class^='container'] div[name*='designcenter'] * p");

        public static By DesignCenter_PackageIndex_SubCat_ViewMore_Links = By.CssSelector("a[class='text-link']");

        public static By DesignCenter_PackageIndex_SubCat_Links = By.CssSelector("div[class='resource-list'] * a[href*='package-index']");

        public static By DesignCenter_PackageIndex_SubSubCat_Package_Col_Header_Txt = By.CssSelector("table[id^='DataTables_Table']>thead>tr>th:nth-of-type(1)");

        public static By DesignCenter_PackageIndex_SubSubCat_Outline_Col_Header_Txt = By.CssSelector("table[id^='DataTables_Table']>thead>tr>th:nth-of-type(2)");

        public static By DesignCenter_PackageIndex_SubSubCat_MaterialInfo_Col_Header_Txt = By.CssSelector("table[id^='DataTables_Table']>thead>tr>th:nth-of-type(3)");

        public static By DesignCenter_PackageIndex_SubSubCat_Package_Col_Val_Txts = By.CssSelector("table[id^='DataTables_Table']>tbody>tr>td:nth-of-type(1)");

        public static By DesignCenter_PackageIndex_SubSubCat_Outline_Col_Val_Links = By.CssSelector("table[id^='DataTables_Table']>tbody>tr>td:nth-of-type(2)>a");

        public static By DesignCenter_PackageIndex_SubSubCat_MaterialInfo_Col_Val_Links = By.CssSelector("table[id^='DataTables_Table']>tbody>tr>td:nth-of-type(2)>a");

        //----- Packaging, Quality, Symbols & Footprints > Package Resources -----//

        public static By DesignCenter_PackageResources_Page_Title_Txt = By.CssSelector("div[class='page-title']>h1");

        public static By DesignCenter_PackageResources_Desc_Txt = By.CssSelector("p[id$='PageDesc']");

        public static By DesignCenter_PackageResources_SolderProfilePerJedecStandards_Link = By.CssSelector("a[href*='jedec']");

        public static By DesignCenter_PackageResources_KeyPackageInformation_Link = By.CssSelector("a[href*='keypackageinformation.html']");

        public static By DesignCenter_PackageResources_FullMaterialDeclarationRohsSearch_Link = By.CssSelector("a[href*='material-declarations.html']");

        public static By DesignCenter_PackageResources_IpcStandards_Link = By.CssSelector("a[href*='ipc']");

        public static By DesignCenter_PackageResources_SubCat_Header_Txts = By.CssSelector("div[name$='relatedresources'] * h3");

        public static By DesignCenter_PackageResources_SubCat_Links = By.CssSelector("div[name$='relatedresources'] * a");

        //----- Packaging, Quality, Symbols & Footprints > Symbols, Footprints & 3D Models -----//

        public static By DesignCenter_SymbolsFootprintsAnd3dModels_Page_Title_Txt = By.CssSelector("div[class='page-title']>h1");

        public static By DesignCenter_SymbolsFootprintsAnd3dModels_EcadModelsSearch_Desc_Txt = By.CssSelector("div[class$='symbols-search']>div>div");

        public static By DesignCenter_SymbolsFootprintsAnd3dModels_EcadModelsSearch_Search_Tbx = By.CssSelector("div[class$='symbols-search'] * input[id$='search']");

        public static By DesignCenter_SymbolsFootprintsAnd3dModels_EcadModelsSearch_Search_Btn = By.CssSelector("div[class$='symbols-search'] * button[class^='search-icon']");

        public static By DesignCenter_SymbolsFootprintsAnd3dModels_NoSearchResults_Txt = By.CssSelector("div[class$='no-results']");

        public static By DesignCenter_SymbolsFootprintsAnd3dModels_Symbols_Tbl = By.CssSelector("div[class^='symbols-result']");

        public static By DesignCenter_SymbolsFootprintsAnd3dModels_Symbols_Tbl_UltraLibrarianReader_ColHeader_Txt = By.CssSelector("div[class^='symbols-result']>ul>li:nth-of-type(2)");

        public static By DesignCenter_SymbolsFootprintsAnd3dModels_Symbols_Tbl_EcadModels_ColHeader_Txt = By.CssSelector("div[class^='symbols-result']>ul>li:nth-of-type(3)");

        public static By DesignCenter_SymbolsFootprintsAnd3dModels_Symbols_Tbl_Rows = By.CssSelector("div[class^='symbols-result']>div[class^='symbols']>ul");

        public static By DesignCenter_SymbolsFootprintsAnd3dModels_RequestNewFile_Sec = By.CssSelector("div[class^='request-file']");

        public static By DesignCenter_SymbolsFootprintsAnd3dModels_RequestNewFile_Sec_ProductPartNo_Txt = By.CssSelector("div[class^='request-file']>div[class$='desc']");

        public static By DesignCenter_SymbolsFootprintsAnd3dModels_RequestNewFile_Sec_YourEmail_Txtbox = By.CssSelector("input[id$='mail']");

        public static By DesignCenter_SymbolsFootprintsAnd3dModels_RequestNewFile_Sec_Send_Btn = By.CssSelector("button[class^='request-file-mail']");

        public static By DesignCenter_SymbolsFootprintsAnd3dModels_RequestNewFile_Sec_DialogBox = By.CssSelector("div[id='adi-modal'] * div[class='modal-content']");

        public static By DesignCenter_SymbolsFootprintsAnd3dModels_RequestNewFile_Sec_DialogBox_X_Btn = By.CssSelector("div[id='adi-modal'] * button[class='close']");

        public static By DesignCenter_SymbolsFootprintsAnd3dModels_RequestNewFile_Sec_DialogBox_Success_Msg_txt = By.CssSelector("div[id='adi-modal'] * h4[class$='email-success']");

        public static By DesignCenter_SymbolsFootprintsAnd3dModels_UltraLibrarianReader_Desc_Txt = By.CssSelector("div[class*='product-row'] * div[class*='description']");

        public static By DesignCenter_SymbolsFootprintsAnd3dModels_UltraLibrarianReader_Video = By.CssSelector("div[class='featured-video']");

        //----- Processors & DSP -----//

        public static By DesignCenter_ProcessorsAndDsp_LeftRailNav = By.CssSelector("ul[class$='left_nav']");

        public static By DesignCenter_ProcessorsAndDsp_LeftRailNav_Selected_Sec_Link = By.CssSelector("ul[class$='left_nav']>li[class='active']>a");

        public static By DesignCenter_ProcessorsAndDsp_LeftRailNav_SubCat_List_Links = By.CssSelector("li[class='active'] * a[href*='processors-and-dsp']");

        public static By DesignCenter_ProcessorsAndDsp_LeftRailNav_Hardware_Link = By.CssSelector("li>a[name^='evaluation-development-hardware']");

        public static By DesignCenter_ProcessorsAndDsp_LeftRailNav_TechnicalLibrary_Link = By.CssSelector("li>a[name*='technical-library']");

        public static By DesignCenter_ProcessorsAndDsp_LeftRailNav_ThirdPartyDevelopersProgram_Link = By.CssSelector("li>a[name^='third-party-developers']");

        public static By DesignCenter_ProcessorsAndDsp_Page_Title_Txt = By.CssSelector("div[class='page-title']>h1");

        public static By DesignCenter_ProcessorsAndDsp_SubSecs = By.CssSelector("div[class$='column-content']>div");

        public static By DesignCenter_ProcessorsAndDsp_ViewOurProductListings_Link = By.CssSelector("a[href*='processors-microcontrollers']");

        public static By DesignCenter_ProcessorsAndDsp_EngineerZoneSupportCommunityForDsp_Link = By.CssSelector("a[href*='ez.analog.com/community/dsp']");

        //----- Processors & DSP > Software -----//

        public static By DesignCenter_Software_Page_Title_Txt = By.CssSelector("div[class='page-title']>h1");

        public static By DesignCenter_Software_Search_Sec = By.CssSelector("div[class$='column-content']>div[class*='search']");

        public static By DesignCenter_Software_Search_Txtbox = By.CssSelector("input[id='text-search']");

        public static By DesignCenter_Software_Predictive_Search = By.CssSelector("div[class='tt-menu tt-open']");

        public static By DesignCenter_Software_SubSecs = By.CssSelector("div[class$='column-content']>div");

        public static By DesignCenter_AlgorithmicAddInAndPlugInSoftwareModules_AlgorithmicSoftwareModulesForBlackfinProcessors_Link = By.CssSelector("a[href*='blackfin']");

        public static By DesignCenter_AlgorithmicAddInAndPlugInSoftwareModules_AlgorithmicSoftwareModulesForSharcProcessors_Link = By.CssSelector("a[href*='sharc']");

        public static By DesignCenter_CodeExamples_BlackfinAndSharcCodeExamplesForCcesSoftware_Link = By.CssSelector("a[href*='download.analog.com']");

        public static By DesignCenter_CodeExamples_Adsp21xxCodeExamplesForVisualDsp_Link = By.CssSelector("a[href*='adsp-21']");

        public static By DesignCenter_CodeExamples_TigerSharcCodeExamplesForVisualDsp_Link = By.CssSelector("a[href*='tigersharc-code-examples']");

        public static By DesignCenter_Middleware_Links = By.CssSelector("div[class$='column-content'] * a[href*='software']");

        //----- Processors & DSP > Hardware -----//

        public static By DesignCenter_Hardware_Page_Title_Txt = By.CssSelector("div[class='page-title']>h1");

        public static By DesignCenter_Hardware_SubCat_Secs = By.CssSelector("div[class$='column-content']>div");

        public static By DesignCenter_Hardware_SubCat_Name_Txts = By.CssSelector("h3[class*='header']");

        public static By DesignCenter_Hardware_SubCat_Desc_Txts = By.CssSelector("div[class^='container'] div[name*='designcenter'] * p");

        public static By DesignCenter_Hardware_SubCat_ViewMore_Links = By.CssSelector("div[name*='designcenter'] * a[class='text-link']");

        public static By DesignCenter_Emulators_EmulatorSearch_Txtbox = By.CssSelector("input[id='text-search']");

        public static By DesignCenter_Emulators_EmulatorSearch_Btn = By.CssSelector("button[class='search-icon']");

        public static By DesignCenter_Emulators_EmulatorToolsUpgradeArchives_Link = By.CssSelector("a[href*='emulator-tools-upgrade-archives']");

        //----- Processors & DSP > Technical Library -----//

        public static By DesignCenter_TechnicalLibrary_Page_Title_Txt = By.CssSelector("div[class='page-title']>h1");

        public static By DesignCenter_TechnicalLibrary_Links = By.CssSelector("div[id='rte-body'] * a");

        public static By DesignCenter_TechnicalLibrary_DocumentationSearch_Sec = By.CssSelector("div[class*='search-control']");

        public static By DesignCenter_TechnicalLibrary_DocumentationSearch_Txtbox = By.CssSelector("input[id='text-search']");

        public static By DesignCenter_TechnicalLibrary_DocumentationSearch_Btn = By.CssSelector("button[class='search-icon']");

        public static By DesignCenter_TechnicalLibrary_DocumentationSearch_AutoSuggest = By.CssSelector("div[class='tt-menu tt-open']");

        public static By DesignCenter_TechnicalLibrary_DocumentationSearch_ProductCategories_Dd = By.CssSelector("div[class='search-filters'] * li[class^='dropdown dropdown-select']");

        public static By DesignCenter_TechnicalLibrary_DocumentationSearch_ProductCategories_Dd_Btn = By.CssSelector("div[class='search-filters'] * li[class^='dropdown dropdown-select']>a");

        public static By DesignCenter_TechnicalLibrary_DocumentationSearch_ProductCategories_Dd_Menu = By.CssSelector("div[class='search-filters'] * li[class^='dropdown dropdown-select']>ul");
        /***********ANALOG DIALOGUE***********/
        public static By AD_Search_TextField = By.CssSelector("input[id='header-search']");

        public static By AD_Search_Btn = By.CssSelector("button[class='btn search-icon']");

        public static By AD_ClearSearch_Btn = By.CssSelector("button[class='btn search-clear-icon']");

        public static By AD_Subscribe_Btn = By.CssSelector("li[class='subscribe']>a");

        public static By AD_Subscribe_Section = By.CssSelector("div[id='subscribe']");

        public static By AD_Subscribe_EmailAddress_TextField = By.CssSelector("input[name='subscribe-email']");

        public static By AD_Subscribe_Submit_Btn = By.CssSelector("input[id='adi_subscribe--submit-button']");

        public static By AD_Subscribe_FBFollow = By.CssSelector("a[class='fb-follow']");

        public static By AD_Carousel_MainArticle = By.CssSelector("div[class='home-pod main']");

        public static By AD_Carousel_MainArticle_Date = By.CssSelector("div[class='home-pod main']>div[class='pod-date']");

        public static By AD_Carousel_MainArticle_Title = By.CssSelector("div[class='home-pod main']>div>h1");

        public static By AD_Carousel_MainArticle_Author_Img = By.CssSelector("div[class='home-pod main']>div * p[class='author-byline']>img");

        public static By AD_Carousel_MainArticle_Author_Name = By.CssSelector("div[class='home-pod main']>div * p[class='author-byline']>span[class='author-name']");

        public static By AD_Carousel_MainArticle_Img = By.CssSelector("div[class='home-pod main']>img");

        public static By AD_Carousel_SubArticle_1 = By.CssSelector("div[class='col-lg-4 col-md-4 banner-right']>div[class='home-pod small']:nth-of-type(1)");

        public static By AD_Carousel_SubArticle_2 = By.CssSelector("div[class='col-lg-4 col-md-4 banner-right']>div[class='home-pod small']:nth-of-type(2)");

        public static By AD_Carousel_SubArticle_1_Author = By.CssSelector("div[class='col-lg-4 col-md-4 banner-right']>div[class='home-pod small']:nth-of-type(1) * p[class='author-byline']");

        public static By AD_Carousel_SubArticle_2_Author = By.CssSelector("div[class='col-lg-4 col-md-4 banner-right']>div[class='home-pod small']:nth-of-type(2) * p[class='author-byline']");

        public static By AD_Carousel_ViewRecent_Link = By.CssSelector("div[class='article-link']>p>a");

        public static By AD_NoteFromEditor = By.CssSelector("div[class='row editors-note']");

        public static By AD_NoteFromEditor_Label = By.CssSelector("div[class='row editors-note'] * h3");

        public static By AD_NoteFromEditor_Icon = By.CssSelector("div[class='row editors-note'] * img");

        public static By AD_NoteFromEditor_Content = By.CssSelector("div[class='row editors-note'] * div[class='summary']");

        public static By AD_NoteFromEditor_More_Btn = By.CssSelector("a[id='note-expander']");

        public static By AD_PastJournals_Section = By.CssSelector("div[class=past-journals-wrapper']");

        public static By AD_PastJournals_Section_Title = By.CssSelector("div[class='row past-journals']>div>h2");

        public static By AD_PastJournals_Slider = By.CssSelector("div[id='journal-slider-1']");

        public static By AD_PastJournals_Archives = By.CssSelector("a[class='btn btn-default home-btn pull-right visible-lg visible-md']");

        public static By AD_PastJournals_ModalPopup = By.CssSelector("div[class='modal fade archive-modal in']>div");

        public static By AD_PastJournals_ModalPopup_Title = By.CssSelector("div[class='modal fade archive-modal in']>div * h4");

        public static By AD_PastJournals_ModalPopup_Img = By.CssSelector("div[class='modal fade archive-modal in']>div * img");

        public static By AD_PastJournals_ModalPopup_Download = By.CssSelector("div[class='modal fade archive-modal in']>div * a[class='dl-link']");

        public static By AD_PastJournals_ModalPopup_Articles = By.CssSelector("div[class='modal fade archive-modal in']>div * div[class*='archive-highlights']");

        public static By AD_PastJournals_ModalPopup_Close = By.CssSelector("div[class='modal fade archive-modal in']>div * button[class='close']");

        public static By AD_RAQ_Title = By.CssSelector("div[class='row rarely-asked'] * h2");

        public static By AD_RAQ_ViewTheAnswer = By.CssSelector("div[class='row rarely-asked'] * a[class='btn btn-default home-btn pull-left answer-link']");

        public static By AD_RAQ_Expander = By.CssSelector("div[id='raq-expander']");

        public static By AD_RAQ_Img = By.CssSelector("div[class='raq-img']>img");

        public static By AD_AboutAD_Meet_the_Editor_Section = By.CssSelector("section[class='about meet-the-editor']");

        public static By AD_AboutAD_NavigationBar = By.CssSelector("div[class='article-info-left']>ul[class='nav navbar-nav']");

        public static By AD_AboutAD_Article = By.CssSelector("article[class='about']");

        public static By AD_RAQ_Page_Author = By.CssSelector("a[class='author_hyperlink']");

        public static By AD_RAQ_Page_Download = By.CssSelector("span[class='download'] a");

        public static By AD_RAQ_Page_Author_Img = By.CssSelector("section[class='author clearfix']>img");

        public static By AD_RAQ_Page_Author_Email = By.CssSelector("section[class='author clearfix'] * a[id='dialogue-author-email']");

        public static By AD_RAQ_Page_Author_Details = By.CssSelector("section[class='author clearfix'] * p[class='details']");

        public static By AD_RAQ_Page_Author_Name = By.CssSelector("section[class='author clearfix'] * p[class='name']");

        public static By AD_RAQ_Page_Related_Products = By.CssSelector("section[class='related-products']");

        public static By AD_RAQ_Page_Related_Articles = By.CssSelector("section[class='col-lg-4 col-md-4 col-sm-12 col-xs-12 rightCol']");

        public static By AD_RAQ_Page_SocialMedia_Widget = By.CssSelector("div[id='at-custom-sidebar']");

        public static By AD_RAQ_Page_SocialMedia_FB = By.CssSelector("div[id='at-custom-sidebar'] a[title='Facebook']");

        public static By AD_RAQ_Page_SocialMedia_Twitter = By.CssSelector("div[id='at-custom-sidebar'] a[title='Twitter']");

        public static By AD_RAQ_Page_SocialMedia_LinkedIn = By.CssSelector("div[id='at-custom-sidebar'] a[title='LinkedIn']");

        public static By AD_RAQ_Page_SocialMedia_Line = By.CssSelector("div[id='at-custom-sidebar'] a[title='LINE']");

        public static By AD_RAQ_Page_SocialMedia_More = By.CssSelector("div[id='at-custom-sidebar'] a[title='More']");

        public static By AD_AutoSuggest = By.CssSelector("div[class='tt-menu tt-open']");

        public static By AD_Search_Reset_Filters = By.CssSelector("button[class='btn btn-primary button-main']");

        public static By AD_RAQ_Page_ShowMore = By.CssSelector("div[class='content raq-list']>div>button");

        public static By AD_Archives_Year_DD = By.CssSelector("span[id='year']");

        public static By AD_Archives_Year_Menu = By.CssSelector("ul[aria-labelledby='YEAR']");

        public static By AD_Archives_Sort_DD = By.CssSelector("span[id='sort']");

        public static By AD_Archives_Sort_Menu = By.CssSelector("ul[aria-labelledby='SORT']");

        public static By AD_Archives_Page_Title = By.CssSelector("div[id='archives-app']>section>h1[class='page title']");

        public static By AD_Arcives_lightbox = By.CssSelector("div[class='archives details modal']");

        public static By AD_Archives_lightbox_DL = By.CssSelector("div[class='archives details modal'] * a[class='download']");

        public static By AD_Archives_lightbox_Close = By.CssSelector("div[class='archives details modal'] button[class='close']");

        public static By AD_50_Logo = By.CssSelector("img[alt='50th Anniversary']");

        public static By AD_MyAnalog_Widget = By.CssSelector("li[class='rr ma-last saveToMyAnalog']>button[id='myAnalogWidgetBtn']");

        public static By AD_StudentZone_ViewMore_Btn = By.CssSelector("div[class='col-md-3 col-xs-12 past-articles']>a");

        /*******************Support******************/
        public static By Support_Search_InputBox = By.CssSelector("div[class='support menu content expanded'] * form * input[name='q']");

        public static By Support_Submit_Button = By.CssSelector("div[class='support menu content expanded'] * form * button");

        public static By Support_EngineerZone_Header = By.CssSelector("h3[class='EngineerZone']>a>img");

        public static By Support_EngineerZone_SupportLink = By.CssSelector("a[class='support-link']");

        public static By Support_EngineerZone_RegisterLink = By.CssSelector("ul[class='register']>li:nth-child(1)>a");

        public static By Support_EngineerZone_PartnerLink = By.CssSelector("ul[class='register']>li:nth-child(2)>a");

        public static By Support_FindAnswerTab_SearchBox = By.CssSelector("div[name='adi_tabcontrol']  form * input[name='q']");

        public static By Support_FindAnswerTab_SearchButton = By.CssSelector("div[name='adi_tabcontrol']  form * button");

        public static By Support_AskEngineerZone_Dropdown = By.CssSelector("div[class*='dropdown ez-dropdown']>a");

        public static By Support_AskEngineerZone_SupportLink = By.CssSelector("a[class='support-class-link']");

        public static By Support_Regional_List = By.CssSelector("ul[class='map-space regional-list']");

        public static By Support_Regional_Result = By.CssSelector("div[id='results']>ul>li>div[class='contact']");

        public static By Support_ContactSupport_SubmitBtn = By.CssSelector("a[name*='Submit_your_question']");

        //----- EXPORT CLASSIFICATION -----//

        //----- View Export Classifications -----//

        public static By ExportClassification_ViewExportClassifications_Page_Title_Txt = By.CssSelector("div[class='header-main']>h1");

        public static By ExportClassification_ViewExportClassifications_AdiExportClassificationInformation_Link = By.CssSelector("a[href$='Classifications.pdf']");

        public static By ExportClassification_ViewExportClassifications_ExportComplianceDepartment_AnalogCom = By.CssSelector("a[href$='ExportComplianceDepartment@analog.com']");

        public static By ExportClassification_ViewExportClassifications_MultipleProductModels = By.CssSelector("div[id='productPanel'] * div[class='row']>div:nth-of-type(1)");

        public static By ExportClassification_ViewExportClassifications_MultipleProductModels_RadioButton = By.CssSelector("input[id='checkMultiple']");

        public static By ExportClassification_ViewExportClassifications_MultipleProductModels_Help_Icon = By.CssSelector("div[id='productPanel'] * div[class='row']>div:nth-of-type(1) * a");

        public static By ExportClassification_ViewExportClassifications_MultipleProductModels_Help_ToolTip = By.CssSelector("div[id='productPanel'] * div[class='row']>div:nth-of-type(1) * a[aria-describedby]");

        public static By ExportClassification_ViewExportClassifications_MultipleProductModels_InputBox = By.CssSelector("textarea[id='txtAreaProductName']");

        public static By ExportClassification_ViewExportClassifications_MultipleProductModels_ErrorMessage = By.CssSelector("label[id='errormsglblmultiple']");

        public static By ExportClassification_ViewExportClassifications_SingleProductModel = By.CssSelector("div[id='productPanel'] * div[class='row']>div:nth-of-type(2)");

        public static By ExportClassification_ViewExportClassifications_SingleProductModel_RadioButton = By.CssSelector("input[id='checkSingle']");

        public static By ExportClassification_ViewExportClassifications_SingleProductModel_Help_Icon = By.CssSelector("div[id='productPanel'] * div[class='row']>div:nth-of-type(2) * a");

        public static By ExportClassification_ViewExportClassifications_SingleProductModel_Help_ToolTip = By.CssSelector("div[id='productPanel'] * div[class='row']>div:nth-of-type(2) * a[aria-describedby]");

        public static By ExportClassification_ViewExportClassifications_SingleProductModel_InputBox = By.CssSelector("input[id='txtProductName']");

        public static By ExportClassification_ViewExportClassifications_SingleProductModel_ErrorMessage = By.CssSelector("label[id='errormsglblsingle']");

        public static By ExportClassification_ViewExportClassifications_ProductModelSearch_SearchNow_Button = By.CssSelector("input[id='Button2']");

        //----- Export Classification Information (Export Classification - Search Result) -----//

        public static By ExportClassification_ExportClassificationInformation_RunANewSearch_Link = By.CssSelector("a[class='new-search']");

        public static By ExportClassification_ExportClassificationInformation_NoResults_Label = By.CssSelector("div[id='exportErrorSec'] * h4");

        public static By ExportClassification_ExportClassificationInformation_ViewInvalidModels_NoDataFound_Link = By.CssSelector("p[class='help-block']>a[href='#']");

        public static By ExportClassification_ExportClassificationInformation_InvalidModels_Table = By.CssSelector("div[class='panel panel-primary']");

        public static By ExportClassification_ExportClassificationInformation_InvalidModels_Table_Row_Value = By.CssSelector("div[class='panel panel-primary'] * ul>li");

        public static By ExportClassification_ExportClassificationInformation_TechnicalSupport_Link = By.CssSelector("a[href$='/technical-support.html']");

        public static By ExportClassification_ExportClassificationInformation_Product_Table = By.CssSelector("table[class='ADIGrid table table-striped table-bordered']");

        public static By ExportClassification_ExportClassificationInformation_Product_Table_Column_Headers = By.CssSelector("table[class='ADIGrid table table-striped table-bordered'] * tr[id='ADIGrid-rowheader']");

        public static By ExportClassification_ExportClassificationInformation_Product_Table_ModelNumber_Column_Header = By.CssSelector("table[class='ADIGrid table table-striped table-bordered'] * th[id$='ModelNumber']");

        public static By ExportClassification_ExportClassificationInformation_Product_Table_Description_Column_Header = By.CssSelector("table[class='ADIGrid table table-striped table-bordered'] * th[id$='Description']");

        public static By ExportClassification_ExportClassificationInformation_Product_Table_UsEccn_Column_Header = By.CssSelector("table[class='ADIGrid table table-striped table-bordered'] * th[id$='-ECCN']");

        public static By ExportClassification_ExportClassificationInformation_Product_Table_SgEccn_Column_Header = By.CssSelector("table[class='ADIGrid table table-striped table-bordered'] * th[id$='SG_ECCN']");

        public static By ExportClassification_ExportClassificationInformation_Product_Table_IeEccn_Column_Header = By.CssSelector("table[class='ADIGrid table table-striped table-bordered'] * th[id$='IE_ECCN']");

        public static By ExportClassification_ExportClassificationInformation_Product_Table_EuroCommodityCode_Column_Header = By.CssSelector("table[class='ADIGrid table table-striped table-bordered'] * th[id$='Euro_Commodity_Code']");

        public static By ExportClassification_ExportClassificationInformation_Product_Table_UsCommodityCode_Column_Header = By.CssSelector("table[class='ADIGrid table table-striped table-bordered'] * th[id$='US_Commodity_Code']");

        public static By ExportClassification_ExportClassificationInformation_Product_Table_CountryOfOrigin_Column_Header = By.CssSelector("table[class='ADIGrid table table-striped table-bordered'] * th[id$='CountryOfOrigin']");

        public static By ExportClassification_ExportClassificationInformation_Product_Table_CountryOfDiffusion_Column_Header = By.CssSelector("table[class='ADIGrid table table-striped table-bordered'] * th[id$='CountryOfDiffusion']");

        public static By ExportClassification_ExportClassificationInformation_Product_Table_Remove_Column_Header = By.CssSelector("table[class='ADIGrid table table-striped table-bordered'] * th[class*='remove']");

        public static By ExportClassification_ExportClassificationInformation_Product_Table_Rows = By.CssSelector("table[class='ADIGrid table table-striped table-bordered']>tbody>tr");

        public static By ExportClassification_ExportClassificationInformation_Product_Table_Remove_Column_Remove_Buttons = By.CssSelector("table[class='ADIGrid table table-striped table-bordered'] * td[class*='remove']>button");

        public static By ExportClassification_ExportClassificationInformation_ExportToExcel_Link = By.CssSelector("a[id$='ExportToExcel']");

        public static By ExportClassification_ExportClassificationInformation_ProductMatches_Label = By.CssSelector("span[class='ADIGrid-dataCount']");

        public static By ExportClassification_ExportClassificationInformation_RestoreHiddenProducts_Link = By.CssSelector("a[class$='restoreViewTrigger']");

        public static By ExportClassification_ExportClassificationInformation_AdiExportClassificationInformation_Link = By.CssSelector("a[href$='Classifications.pdf']");

        public static By ExportClassification_ExportClassificationInformation_ExportComplianceDepartment_AnalogCom_Link = By.CssSelector("a[href$='ExportComplianceDepartment@analog.com']");

        //----- EDUCATION -----//

        //----- Education Library -----//

        public static By Education_EducationLibrary_LeftRail_Selected_Sec_Link = By.CssSelector("ul[class$='left_nav']>li[class='active']>a");

        public static By Education_EducationLibrary_LeftRail_Selected_Val_Link = By.CssSelector("ul[class$='left_nav'] * a[class='selected']");

        public static By Education_EducationLibrary_LeftPane_Subsec_Links = By.CssSelector("li[class='active'] * a[href*='education-library']");

        public static By Education_EducationLibrary_LeftPane_TechnicalArticles_Link = By.CssSelector("a[name^='Article']");

        public static By Education_EducationLibrary_LeftPane_Faqs_Link = By.CssSelector("a[name*='faqs']");

        public static By Education_EducationLibrary_LeftPane_TechnicalBooks_Link = By.CssSelector("a[name^='technical-books']");

        public static By Education_EducationLibrary_LeftPane_TrainingAndTutorials_Link = By.CssSelector("a[name^='training-and-tutorials']");

        public static By Education_EducationLibrary_LeftPane_WhitePapersAndCaseStudies_Link = By.CssSelector("a[name^='whitepapers-case-studies']");

        public static By Education_EducationLibrary_Page_Title_Txt = By.CssSelector("div[class='header-main']>h1");

        public static By Education_EducationLibrary_Hero_Img = By.CssSelector("div[class='hero-image'] * img");

        //----- Education Library > Videos -----//

        public static By Education_Videos_Bg_Vid = By.CssSelector("div[class='video-overlay']");

        public static By Education_Videos_AnalogDevicesVideoPortal_Lbl = By.CssSelector("div[class='video-message']>h3");

        public static By Education_Videos_Search_InputBx = By.CssSelector("input[id='text-search']");

        public static By Education_Videos_Search_Btn = By.CssSelector("button[class$='search']");

        public static By Education_Videos_Search_PredictiveSearch = By.CssSelector("div[class='tt-menu tt-open']");

        public static By Education_Videos_Search_PredictiveSearch_Item_Links = By.CssSelector("div[class='tt-menu tt-open']>div>p>a");

        public static By Education_Videos_Search_Filter_Menus = By.CssSelector("div[class$='search-filters']>ul>li");

        public static By Education_Videos_ProductCategories_Dd = By.CssSelector("li[class^='has-children dropdown dropdown-select'][data-facetcategory^='prod_cat']");

        public static By Education_Videos_ProductCategories_Dd_Menu = By.CssSelector("li[class^='has-children dropdown dropdown-select'][data-facetcategory^='prod_cat']>ul[class$='dropdown-menu']");

        public static By Education_Videos_ProductCategories_Dd_Menu_Val_Lbls = By.CssSelector("li[class^='has-children dropdown dropdown-select'][data-facetcategory^='prod_cat']>ul[class$='dropdown-menu']>li:nth-of-type(2)>a>span:nth-of-type(1)");

        public static By Education_Videos_Markets_Dd = By.CssSelector("li[class^='has-children dropdown dropdown-select'][data-facetcategory^='market']");

        public static By Education_Videos_Markets_Dd_Menu_Val_Lbls = By.CssSelector("li[class^='has-children dropdown dropdown-select'][data-facetcategory^='market']>ul[class$='dropdown-menu']>li:nth-of-type(2)>a>span:nth-of-type(1)");

        public static By Education_Videos_Tradeshows_Dd = By.CssSelector("li[class^='has-children dropdown dropdown-select'][data-facetcategory^='tradeshow']");

        public static By Education_Videos_Tradeshows_Dd_Menu_Val_Lbls = By.CssSelector("li[class^='has-children dropdown dropdown-select'][data-facetcategory^='tradeshow']>ul[class$='dropdown-menu']>li:nth-of-type(2)>a>span:nth-of-type(1)");

        public static By Education_Videos_2ndComponent_Large_Vid = By.CssSelector("div[class='video-container featured-video player large'] * video[id^='video']");

        public static By Education_Videos_2ndComponent_Small_Vids = By.CssSelector("div[class='video-container player small'] * div[class='vjs-poster']");

        public static By Education_Videos_Select_Dds = By.CssSelector("div[class^='video-filter'] * button");

        public static By Education_Videos_Select_Dds_Menu = By.CssSelector("div[class^='video-filter']>span[class^='dropdown open']>ul[class='dropdown-menu']");

        public static By Education_Videos_3rdComponent_Vid_Thumbnails = By.CssSelector("img[class$='thumbnail-video-window']");

        public static By Education_Videos_Video_DialogBx = By.CssSelector("div[class*='video-window'] * div[class='modal-content']");

        public static By Education_Videos_Video_DialogBx_X_Btn = By.CssSelector("div[class*='video-window'] * div[class='modal-content'] * button[class='close']");

        public static By Education_Videos_Video_DialogBx_Vid = By.CssSelector("div[class*='video-window'] * div[class='modal-content'] * div[class='vjs-poster']");

        //----- Education Library > Videos > Video Page -----//

        public static By Education_VideoPage_Page_Title_Lbl = By.CssSelector("div[class^='title'] * h1");

        //----- Education Library > Webcasts -----//

        public static By Education_Webcasts_Page_Title_Lbl = By.CssSelector("div[class^='header']>h1");

        public static By Education_Webcasts_Page_Desc_Txt = By.CssSelector("div[class^='header']>span");

        public static By Education_Webcasts_FindWebcasts_InputBx = By.CssSelector("input[id='text-search']");

        public static By Education_Webcasts_FindWebcasts_Search_Btn = By.CssSelector("button[class='search-icon']");

        public static By Education_Webcasts_FindWebcasts_AutoSuggest_Dd = By.CssSelector("div[class='tt-menu tt-open']");

        public static By Education_Webcasts_BrowseBySpecifiedCategory_ProductCategories_Dd = By.CssSelector("div[class='search-filters']>ul>li[data-facetcategory^='prod_cat']");

        public static By Education_Webcasts_BrowseBySpecifiedCategory_ProductCategories_Dd_Menu = By.CssSelector("div[class='search-filters']>ul>li[data-facetcategory^='prod_cat']>ul[class$='dropdown-menu']");

        public static By Education_Webcasts_BrowseBySpecifiedCategory_ProductCategories_Dd_Menu_Val_Lbls = By.CssSelector("div[class='search-filters']>ul>li[data-facetcategory^='prod_cat']>ul[class$='dropdown-menu']>li:nth-of-type(2)>a>span:nth-of-type(1)");

        public static By Education_Webcasts_BrowseBySpecifiedCategory_ProductCategories_Subcategories_Dd_Menu = By.CssSelector("div[class='search-filters']>ul>li[data-facetcategory^='prod_cat'] * li[class$='hover']>div[class='sublinks']>ul>li");

        //----- Education Library > Webcasts > Webcast Page -----//

        public static By Education_WebcastPage_AuthorSummary_Img = By.CssSelector("span[class^='author-image']>img");

        public static By Education_WebcastPage_AuthorSummary_Details_Txt = By.CssSelector("div[class^='biodata']>span:nth-last-of-type(1)");

        public static By Education_WebcastPage_3rdCol_Webcast_Title_Lbl = By.CssSelector("h2[class^='header']");

        public static By Education_WebcastPage_3rdCol_Webcast_Desc_Txt = By.CssSelector("div[class^='content'] * p");

        public static By Education_WebcastPage_3rdCol_AuthorInfo_Img = By.CssSelector("div[class$='biodata'] * img");

        public static By Education_WebcastPage_3rdCol_AuthorInfo_Details_Txt = By.CssSelector("div[class$='biodata'] * ul>li");

        public static By Education_WebcastPage_3rdCol_AuthorInfo_Bio_Txt = By.CssSelector("div[class$='biodata'] * p");

        //----- Education Engagement > North America ----//

        public static By Education_NorthAmerica_Nav_List = By.CssSelector("ul[class*='nav-list'] * ul>li");

        public static By Education_NorthAmerica_Nav_List_LocalStrategicAdvocates_Link = By.CssSelector("ul[class*='nav-list'] * a[name^='local-strategic-advocates']");

        public static By Education_NorthAmerica_Nav_List_CampusRecruitment_Link = By.CssSelector("ul[class*='nav-list'] * a[name^='campus-recruitment']");

        public static By Education_NorthAmerica_Nav_List_ContactUs_Link = By.CssSelector("ul[class*='nav-list'] * a[name^='contact-us']");

        public static By Education_NorthAmerica_Nav_List_NewsAndTrends_Link = By.CssSelector("ul[class*='nav-list'] * a[name^='news-and-trends']");

        //----- Education Engagement > North America > Local Strategic Advocates ----//

        public static By Education_LocalStrategicAdvocates_Articles = By.CssSelector("div[id='rte-body']>p");

        //----- Education Engagement > North America > On Campus Recruitment ----//

        public static By Education_OnCampusRecruitment_NorthAmerica_Link = By.CssSelector("a[href$='/north-america.html']");

        public static By Education_OnCampusRecruitment_Ireland_Link = By.CssSelector("p>a[href$='/ireland.html']:nth-last-of-type(1)");

        //----- Education Engagement > North America > Contact Us ----//

        public static By Education_ContactUs_AnalogDevices_Link = By.CssSelector("a[href^='mailto']");

        //----- Education Engagement > North America > News and Trends ----//

        public static By Education_NewsAndTrends_Link_List = By.CssSelector("div[id='rte-body'] * a");

        /****************Applications****************/
        public static By Applications_BreadCrumbs = By.CssSelector("div[name='adi_breadcrumb']");

        public static By Applications_BreadCrumbs_Home = By.CssSelector("div[name='adi_breadcrumb'] * a[class='home']");

        public static By Applications_Print = By.CssSelector("li[class='link print']>a");

        public static By Applications_SignaChains_Popup = By.CssSelector("div[id='waveformGenerator']>div[class='popUp']");

        public static By Applications_SignaChains_Popup_Header = By.CssSelector("div[id='waveformGenerator']>div[class='popUp']>h3>span[class='popup-header-label']");

        public static By Applications_SignalChains_Diagram = By.CssSelector("div[class='signal-chain-container']");

        public static By Applications_PSTDynamicTable = By.CssSelector("div[class='static-pst-container']");

        public static By Applications_LeftNav = By.CssSelector("div[class='col-md-12 floater-menu-container'] div[name='adi_products_left_navigation']");

        public static By Applications_LatestResource_Label = By.CssSelector("div[class='featured-resource-tile']>div[class='carousel-heading prodsubcat col-md-12 ']>h3");

        public static By Applications_AllResources_Label = By.CssSelector("div[class='clearfix all-resource-heading']>h3");

        public static By Applications_DataSheetv62_Link = By.CssSelector("div[class='relatedLink']>a:nth-child(2)");

        public static By Applications_SignalChain_Title = By.CssSelector("div[class='col-md-offset-2 container-offset product-section'] h3[id='signalChaintitle']");

        public static By Applications_SignalChain_NextBtn = By.CssSelector("div[class='nav-direction nav-next nav-dir next']");

        public static By Applications_SignalChain_PrevBtn = By.CssSelector("div[class='nav-direction nav-prev nav-dir prev']");

        public static By Applications_SignalChain_SelectedPart_CloseBtn = By.CssSelector("div[class='scd-partTitle'] a[class='close-icon']");

        public static By Applications_SignalChain_SelectedPart_ExpandBtn = By.CssSelector("div[class='scd-partTitle'] a[class='expand-icon']");

        public static By Applications_SignalChain_SelectedPart_Title = By.CssSelector("div[class='scd-partTitle']");

        public static By Applications_SignalChain_SelectedPart_Table = By.CssSelector("div[class='scd-partTable table-responsive']");

        public static By Applications_Technology_FeaturedProduct_Header = By.CssSelector("section[id='featured-products-sectionband'] h3");

        public static By Applications_PromoBanner = By.CssSelector("div[id='myCarousel']");

        public static By Applications_MenuNav_PromoBanner = By.CssSelector("div[class='menu-promo']");

        public static By Applications_MenuNav_PromoBanner_LearnMore = By.CssSelector("div[class='menu-promo'] a[class='link-learn-more']");


        /************Category Page**********/
        public static By Category_Header_Title = By.CssSelector("h1[class='title-margin-32 header1']");

        public static By Category_LandingPage_SubCategory = By.CssSelector("div[class='sub-category']");

        public static By Category_PDST_PSTIcon = By.CssSelector("a>i[class='pstlink-icon']");

        public static By Category_StaticPST = By.CssSelector("div[id*='PSTTable_']");

        public static By Category_StaticPST_ViewAllBtn = By.CssSelector("div[id='pst_container']>div[class='static-pst-container']>div[class='filterButton']>a:nth-child(1)>input");

        public static By Category_StaticPST_ResetBtn = By.CssSelector("div[id='pst_container'] a>input[id='btnresettable']");

        public static By Category_StaticPST_ViewAllBtn_Footer = By.CssSelector("td>div[class='filterButton']>a>input");

        public static By Category_NewProduct_Accordion = By.CssSelector("div[class='row fca-row top-spacing-32 bottom-spacing-32 new-products-accordion']");

        public static By Category_NewProduct_Title = By.CssSelector("div[class='row fca-row top-spacing-32 bottom-spacing-32 new-products-accordion'] div[class*='NewProductDetailsView']>h3");

        public static By Category_NewProduct_YearDropdown = By.CssSelector("div[class='row fca-row top-spacing-32 bottom-spacing-32 new-products-accordion'] ul");

        public static By Category_NewProduct_NoProductFound = By.CssSelector("div[class='container page-title new-product-no-results']");

        public static By Category_NewProductPage_Header = By.CssSelector("div[id='loadProductsListingHeaderInfo'] h1");

        public static By Category_NewProductPage_Description = By.CssSelector("div[id='loadProductsListingHeaderInfo'] h4");

        public static By Category_NewProductPage_DaysFilterDropdown = By.CssSelector("div[id='daysFilter']>a");

        public static By Category_NewProductPage_DaysFilterMenu = By.CssSelector("div[class='dropdown filterBtn open']>ul");

        public static By Category_NewProductPage_NoNewItem = By.CssSelector("p[id='errorMsg']");

        public static By Category_NewProductPage_ProductListingFilter = By.CssSelector("div[id='loadProductsListingFilter']");

        public static By Category_NewProductPage_ProductListingFilter_All = By.CssSelector("div[id='loadProductsListingFilter']>ul[class='all-filter']>li");

        public static By Category_NewProductPage_ProductListingFilter_CategoriesAll = By.CssSelector("div[id='loadProductsListingFilter']>ul[class='filter-categories']>li");

        public static By Category_NewProductPage_ListView = By.CssSelector("section[class='productsListView'] div[id='loadProductsList']>div[class='productsListingContainer']");

        public static By Category_LatestResources_Tile = By.CssSelector("div[class='featured-resource-tile'] ul[class='slides slides-layout-2']>li");

        //----- ABOUT ADI -----//

        public static By AboutAdi_2nd_Column_Page_Title_Txt = By.CssSelector("div[class='header-main']>h1");

        public static By AboutAdi_Page_Title_Txt = By.CssSelector("div[class='page-title']>h1");

        //----- Left Rail Navigation -----//

        public static By AboutAdi_LeftRailNav_Selected_Category_SubCategory_Links = By.CssSelector("ul[class$='left_nav']>li[class='active']>ul[class='list sublinks']>li>a");

        public static By AboutAdi_LeftRailNav_Selected_SubCategory_Link = By.CssSelector("ul[class$='left_nav'] * a[class='selected']");

        public static By AboutAdi_LeftRailNav_CorporateInformation_Link = By.CssSelector("a[name^='Corporate-Information']");

        public static By AboutAdi_LeftRailNav_Selected_CorporateInformation_Link = By.CssSelector("li[class='active']>a[name^='Corporate-Information']");

        public static By AboutAdi_LeftRailNav_CorporateGovernance_Link = By.CssSelector("a[name^='corporate-governance']");

        public static By AboutAdi_LeftRailNav_Selected_CorporateGovernance_Link = By.CssSelector("a[class='selected'][name^='corporate-governance']");

        public static By AboutAdi_LeftRailNav_RegionalHeadquarters_Link = By.CssSelector("a[name^='regional-headquarters']");

        public static By AboutAdi_LeftRailNav_SalesAndDistribution_Link = By.CssSelector("a[name^='sales-distribution']");

        public static By AboutAdi_LeftRailNav_ContactUs_Link = By.CssSelector("a[name^='contact-us']");

        public static By AboutAdi_LeftRailNav_Selected_ContactUs_Link = By.CssSelector("a[class='selected'][name^='contact-us']");

        public static By AboutAdi_LeftRailNav_Careers_Link = By.CssSelector("a[name^='careers']");

        public static By AboutAdi_LeftRailNav_QualityAndReliability_Link = By.CssSelector("a[name^='quality-reliability']");

        public static By AboutAdi_LeftRailNav_InvestorRelations_Link = By.CssSelector("a[name^='investor-relations']");

        public static By AboutAdi_LeftRailNav_Selected_NewsRoom_Link = By.CssSelector("li[class='active']>a[name^='news-room']");

        public static By AboutAdi_LeftRailNav_ExternalRecognition_Link = By.CssSelector("a[name^='external-recognition']");

        public static By AboutAdi_LeftRailNav_MultimediaGallery_Link = By.CssSelector("a[name^='multimedia-gallery']");

        public static By AboutAdi_LeftRailNav_PressReleases_Link = By.CssSelector("a[name^='press-releases']");

        public static By AboutAdi_LeftRailNav_Selected_PressReleases_Link = By.CssSelector("a[class='selected'][name^='press-releases']");

        public static By AboutAdi_LeftRailNav_NewsRoom_Link = By.CssSelector("a[name^='news-room']");

        public static By AboutAdi_LeftRailNav_Newsletters_Link = By.CssSelector("a[name^='newsletters']");

        public static By AboutAdi_LeftRailNav_Events_Link = By.CssSelector("a[name^='events']");

        public static By AboutAdi_LeftRailNav_Selected_Events_Link = By.CssSelector("a[class='selected'][name^='events']");

        //----- About ADI Landing Page -----//

        public static By AboutAdi_LandingPage_Video = By.CssSelector("div[name*='hero'] * div[class='vjs-poster']");

        public static By AboutAdi_LandingPage_Video_Desc_Txt = By.CssSelector("div[name='adi_threecolumn_content_section']>div:nth-of-type(2) * div[class='hero-description']>div[class='rte']");

        //----- Contact Us -----//

        public static By AboutAdi_ContactUs_CorporateContactInformation_Section = By.CssSelector("div[class='corporate-info col-md-12 padding-sd-box-bt-reset']");

        public static By AboutAdi_ContactUs_SearchWithinSupport_Button = By.CssSelector("button[class='search-icon']");

        public static By AboutAdi_ContactUs_SubmitDocumentFeedback_Link = By.CssSelector("a[href*='/documentfeedback.aspx']");

        public static By AboutAdi_ContactUs_ConnectionsHrServices_AnalogCom_Link = By.CssSelector("a[href*='ConnectionsHRService']");

        public static By AboutAdi_ContactUs_RecruitJapan_AnalogCom_Link = By.CssSelector("a[href*='recruit.japan']");

        public static By AboutAdi_ContactUs_NewsAndMediaRelations_Link = By.CssSelector("div[class$='corporate-info'] * a[href$='/news-room.html']");

        public static By AboutAdi_ContactUs_MyAnalog_Link = By.CssSelector("a[href*='/login']");

        public static By AboutAdi_ContactUs_SubscribeToENewsletters_Link = By.CssSelector("div[class$='corporate-info'] * a[href$='app/subscriptions']");

        public static By AboutAdi_ContactUs_ReadOnlineNews_Link = By.CssSelector("div[class$='corporate-info'] * a[href$='manage-newsletters.html']");

        public static By AboutAdi_ContactUs_ContactUs_Link = By.CssSelector("a[href$='Contact us']");

        public static By AboutAdi_ContactUs_DistributionLiterature_AnalogCom_Link = By.CssSelector("a[href$='distribution.literature@analog.com']");

        //----- Corporate Information > Corporate Governance -----//

        public static By AboutAdi_CorporateGovernance_BoardOfDirectors_Link = By.CssSelector("a[href*='directors']");

        public static By AboutAdi_CorporateGovernance_BoardCommitteeComposition_Link = By.CssSelector("a[href*='committee']");

        public static By AboutAdi_CorporateGovernance_CorporateOfficersAndExecutives_Link = By.CssSelector("a[href*='executive-officers']");

        public static By AboutAdi_CorporateGovernance_CorporateOfficersAndExecutives_Link2 = By.CssSelector("a[href*='management']");

        public static By AboutAdi_CorporateGovernance_GovernanceDocuments_Link = By.CssSelector("a[href*='governance-documents']");

        //----- Corporate Information > Sales and Distribution -----//

        public static By AboutAdi_SalesAndDistribution_Map_Img = By.CssSelector("div[id='map_area']>img");

        public static By AboutAdi_SalesAndDistribution_Map_China_Region = By.CssSelector("div[class^='map-area-info'][title='China']");

        public static By AboutAdi_SalesAndDistribution_Map_AsiaPacific_Region = By.CssSelector("div[class^='map-area-info'][title='Asia Pacific']");

        public static By AboutAdi_SalesAndDistribution_Map_Regions = By.CssSelector("div[class^='map-area-info']");

        public static By AboutAdi_SalesAndDistribution_Map_Expanded_Regions = By.CssSelector("div[class='map-area-info active']>div[class='clearfix']");

        public static By AboutAdi_SalesAndDistribution_Map_Expanded_Region_Country_Links = By.CssSelector("div[class='map-area-info active']>div[class='clearfix'] * ul[class^='country-column']>li>a");

        public static By AboutAdi_SalesAndDistribution_Region_Info = By.CssSelector("div[class^='region-info']");

        public static By AboutAdi_SalesAndDistribution_AdiExportAndImportClassificationInformation_Link = By.CssSelector("div[class='visible-desktop'] * a[name^='Export_N_Import_Classification']");

        public static By AboutAdi_SalesAndDistribution_PolicyOnPurchasesFromNonAuthorizedDistributorsOrBrokers_Link = By.CssSelector("div[class='visible-desktop'] * a[data-target$='PolicyOnPurchase']");

        public static By AboutAdi_SalesAndDistribution_PolicyOnPurchasesFromNonAuthorizedDistributorsOrBrokers_LightBox = By.CssSelector("div[id$='PolicyOnPurchase'] * div[class='modal-content']");

        //----- News Room -----//

        public static By AboutAdi_NewsRoom_PressReleases_Section_Title_Label = By.CssSelector("div[class='story-content']>div:nth-of-type(1) * h3[class$='title-text']");

        public static By AboutAdi_NewsRoom_PressReleases_Section_Article_Date_Labels = By.CssSelector("div[class='story-content']>div:nth-of-type(1) * span[class$='date']>small");

        public static By AboutAdi_NewsRoom_PressReleases_Section_Article_Title_Labels = By.CssSelector("div[class='story-content']>div:nth-of-type(1) * h4[class$='title']");

        public static By AboutAdi_NewsRoom_AdiInTheNews_Section_Title_Label = By.CssSelector("div[class='story-content']>div:nth-of-type(2) * h3[class$='title-text']");

        public static By AboutAdi_NewsRoom_AdiInTheNews_Section_Article_Date_Labels = By.CssSelector("div[class='story-content']>div:nth-of-type(2) * span[class$='date']>small");

        public static By AboutAdi_NewsRoom_AdiInTheNews_Section_Article_Title_Labels = By.CssSelector("div[class='story-content']>div:nth-of-type(2) * h4[class$='title']>a");

        public static By AboutAdi_NewsRoom_AdiInTheNews_Section_ReadMore_Links = By.CssSelector("div[class='story-content']>div:nth-of-type(2) * a[class='data-content-link']");

        public static By AboutAdi_NewsRoom_MediaContacts_Section_Title_Label = By.CssSelector("div[class='col-md-4']>div:nth-of-type(2) * tbody>tr:nth-last-of-type(1)>td");

        public static By AboutAdi_NewsRoom_MediaContacts_Section_Content = By.CssSelector("div[class='col-md-4']>div:nth-of-type(2) * tbody>tr:nth-last-of-type(1)>td");

        public static By AboutAdi_NewsRoom_Leadership_Section_Title_Label = By.CssSelector("div[class='col-md-4']>div:nth-last-of-type(2) * tbody>tr:nth-of-type(1)>td");

        public static By AboutAdi_NewsRoom_StayInformed_Section_Title_Label = By.CssSelector("div[class='col-md-4']>div:nth-last-of-type(1) * tbody>tr:nth-of-type(1)>td");

        public static By AboutAdi_NewsRoom_StayInformed_Section_Content = By.CssSelector("div[class='col-md-4']>div:nth-last-of-type(1) * tbody>tr:nth-last-of-type(1)>td");

        public static By AboutAdi_NewsRoom_StayInformed_Section_myAnalogCom_Link = By.CssSelector("a[href*='my.analog.com']");

        public static By AboutAdi_NewsRoom_StayInformed_Section_Subscribe_Link = By.CssSelector("td>a[href*='letter']:nth-last-of-type(1)");

        //----- News Room > Multimedia Gallery -----//

        public static By AboutAdi_MultimediaGallery_Graphics_Section = By.CssSelector("div[class$='column-content']>div:nth-of-type(2) * div[id='adi-mmg-placeholder'] * div[class='adi-mmg-content adi-mmg-image']");

        public static By AboutAdi_MultimediaGallery_Graphics_Section_Title_Label = By.CssSelector("div[class$='column-content']>div:nth-of-type(2) * div[id='adi-mmg-placeholder'] * h3[class*='title']");

        public static By AboutAdi_MultimediaGallery_Graphics_Section_Items = By.CssSelector("div[class$='column-content']>div:nth-of-type(2) * div[id='adi-mmg-placeholder'] * div[class$='adi-mmg-data-content']");

        public static By AboutAdi_MultimediaGallery_Expanded_Graphics_Section_Items = By.CssSelector("div[class$='column-content']>div:nth-of-type(2) * div[id='adi-mmg-placeholder'] * div[style='display: block;']>div[class$='adi-mmg-data-content']");

        public static By AboutAdi_MultimediaGallery_Graphics_Section_Item_Thumbnail_Images = By.CssSelector("div[class$='column-content']>div:nth-of-type(2) * div[id='adi-mmg-placeholder'] * img");

        public static By AboutAdi_MultimediaGallery_Graphics_Section_Item_Download_Buttons = By.CssSelector("div[class$='column-content']>div:nth-of-type(2) * div[id='adi-mmg-placeholder'] * a[class^='download']");

        public static By AboutAdi_MultimediaGallery_Graphics_Section_Item_ShowMore_Button = By.CssSelector("div[class$='column-content']>div:nth-of-type(2) * div[id='adi-mmg-placeholder'] * div[class='adi-mmg-show']>a[class$='show-button']");

        public static By AboutAdi_MultimediaGallery_Graphics_Section_Item_ShowLess_Button = By.CssSelector("div[class$='column-content']>div:nth-of-type(2) * div[id='adi-mmg-placeholder'] * div[class='adi-mmg-show expanded']>a[class$='show-button']");

        public static By AboutAdi_MultimediaGallery_Logos_Section = By.CssSelector("div[class$='column-content']>div:nth-of-type(4) * div[id='adi-mmg-placeholder'] * div[class='adi-mmg-content adi-mmg-image']");

        public static By AboutAdi_MultimediaGallery_Logos_Section_Title_Label = By.CssSelector("div[class$='column-content']>div:nth-of-type(4) * div[id='adi-mmg-placeholder'] * h3[class*='title']");

        public static By AboutAdi_MultimediaGallery_Logos_Section_Items = By.CssSelector("div[class$='column-content']>div:nth-of-type(4) * div[id='adi-mmg-placeholder'] * div[class$='adi-mmg-data-content']");

        public static By AboutAdi_MultimediaGallery_Expanded_Logos_Section_Items = By.CssSelector("div[class$='column-content']>div:nth-of-type(4) * div[id='adi-mmg-placeholder'] * div[style='display: block;']>div[class$='adi-mmg-data-content']");

        public static By AboutAdi_MultimediaGallery_Logos_Section_Item_Thumbnail_Images = By.CssSelector("div[class$='column-content']>div:nth-of-type(4) * div[id='adi-mmg-placeholder'] * img");

        public static By AboutAdi_MultimediaGallery_Logos_Section_Item_Download_Buttons = By.CssSelector("div[class$='column-content']>div:nth-of-type(4) * div[id='adi-mmg-placeholder'] * a[class^='download']");

        public static By AboutAdi_MultimediaGallery_Logos_Section_Item_ShowMore_Button = By.CssSelector("div[class$='column-content']>div:nth-of-type(4) * div[id='adi-mmg-placeholder'] * div[class='adi-mmg-show']>a[class$='show-button']");

        public static By AboutAdi_MultimediaGallery_Logos_Section_Item_ShowLess_Button = By.CssSelector("div[class$='column-content']>div:nth-of-type(4) * div[id='adi-mmg-placeholder'] * div[class='adi-mmg-show expanded']>a[class$='show-button']");

        public static By AboutAdi_MultimediaGallery_PopOut_Box = By.CssSelector("div[id='image-modal'] * div[class='modal-content']");

        public static By AboutAdi_MultimediaGallery_PopOut_Box_X_Button = By.CssSelector("div[id='image-modal'] * div[class='modal-content'] * button[class='close']");

        public static By AboutAdi_MultimediaGallery_PopOut_Box_Download_Button = By.CssSelector("div[id='image-modal'] * div[class='modal-content'] * span[id='downbutton']");

        public static By AboutAdi_MultimediaGallery_PopOut_Box_Print_Button = By.CssSelector("div[id='image-modal'] * div[class='modal-content'] * span[class='icon-print']");

        //----- News Room > Press Releases -----//

        public static By AboutAdi_PressReleases_YearFilter_Links = By.CssSelector("ul[class*='nav-list'] * ul>li:nth-last-of-type(1)>a");

        public static By AboutAdi_PressReleases_Selected_YearFilter_Link = By.CssSelector("ul[class*='nav-list'] * ul>li:nth-last-of-type(1)>a[class='selected']");

        public static By AboutAdi_PressReleases_Selected_Year_Title_Label = By.CssSelector("div>h2[data-page-title]");

        public static By AboutAdi_PressReleases_SearchWithinPressReleases_InputBox = By.CssSelector("input[id='text-search']");

        public static By AboutAdi_PressReleases_SearchWithinPressReleases_Button = By.CssSelector("button[class='search-icon']");

        public static By AboutAdi_PressReleases_PressReleases_Article_Date_Labels = By.CssSelector("div[class^='events-list']>div>ul>li:nth-of-type(1)>div");

        public static By AboutAdi_PressReleases_PressReleases_Article_Title_Links = By.CssSelector("div[class^='events-list']>div>ul>li>a");

        //----- News Room > Press Releases > Press Releases Article Page -----//

        public static By AboutAdi_PressReleasesArticlePage_Desc_Text = By.CssSelector("div[class$='press-release']");

        public static By AboutAdi_PressReleasesArticlePage_Image = By.CssSelector("a>div>img");

        public static By AboutAdi_PressReleasesArticlePage_Image_ZoomIn_Button = By.CssSelector("div[class='icon-zoom']");

        public static By AboutAdi_PressReleasesArticlePage_Image_LightBox = By.CssSelector("div[id='image-modal'] * div[class='modal-content']");

        public static By AboutAdi_PressReleasesArticlePage_Image_LightBox_X_Button = By.CssSelector("div[id='image-modal'] * button[class='close']");

        //----- Newsletters -----//

        public static By AboutAdi_Newsletters_SubscribeToENewsletters_Button = By.CssSelector("span[class='analog-rounded-button'] a[href*='my']");

        //----- Events -----//

        public static By AboutAdi_Events_Expanded_Accordion_Sections = By.CssSelector("div[class$='expand-all'][data-expand-accordion]");

        public static By AboutAdi_Events_Expanded_Accordions = By.CssSelector("div[class='section-bar']");

        //----- Privacy and Security Statement -----//

        public static By AboutAdi_PrivacyAndSecurityStatement_AcceptAndProceed_Button = By.CssSelector("div[name$='content_section'] * a[data-id$='consent']");

        public static By AboutAdi_PrivacyAndSecurityStatement_DeclineCookies_Button = By.CssSelector("div[name$='content_section'] * a[data-id$='decline']");

        public static By AboutAdi_PrivacyAndSecurityStatement_AcceptedCookies_Message = By.CssSelector("span[data-id='message-accepted']");

        public static By AboutAdi_PrivacyAndSecurityStatement_DeclinedCookies_Message = By.CssSelector("span[data-id='message-declined']");

        //------ Cross Reference Searh------//
        public static By XRef_XRefSearch_Toggle = By.CssSelector("a[rel='cross_search_part']");

        public static By XRef_ObsoletePartSearch_Toggle = By.CssSelector("a[rel='obsolete_search']");

        public static By XRef_XRefSearch_PartNumber_Textfield = By.CssSelector("input[id='txtSearchByPartNumber']");

        public static By XRef_XRefSearch_PartNumber_Search_Button = By.CssSelector("input[id='btnPartNumSearch']");

        public static By XRef_XRefSearch_Mfg_Dropdown = By.CssSelector("select[id='OptionGroupManufacturer']");

        public static By XRef_XRefSearch_Mfg_Search_Button = By.CssSelector("input[id='btnManufSearch']");

        public static By XRef_XRefSearch_TechnicalSupport_Link = By.CssSelector("div[class='well']>a");

        public static By XRef_ObsoletePart_TextField = By.CssSelector("input[id='txtObsoletepartSearchBox']");

        public static By XRef_ObsoletePart_Search_Button = By.CssSelector("input[id='btnObsoleteSearch']");

        public static By XRef_ObsoletePart_TechnicalSupport_Link = By.CssSelector("div[class='tab-pane obsolete-search active'] div[class='well']>a");

        public static By XRef_XRefSearch_ErrorByPart = By.CssSelector("span[id='lblPartError']");

        public static By XRef_XRefSearch_ErrorByMfg = By.CssSelector("span[id='lblError']");

        public static By XRef_XRefSearch_NoMatchFound_Label = By.CssSelector("div[id='divNoPartsMatch']>h3");

        public static By XRef_XRefSearch_SearchResult = By.CssSelector("div[id='divSearchResult']");

        public static By XRef_ResultPanel1_Header = By.CssSelector("span[class='col-md-4 select_part_heading']");

        public static By XRef_ResultPanel2_Header = By.CssSelector("span[class='col-md-8 equivalent_product_heading']");

        public static By XRef_PartNo_List = By.CssSelector("div[id='select_product_content']");

        public static By XRef_EquivalentPart_List = By.CssSelector("div[id='divequivalent']");

        public static By XRef_EquivalentPart_ToolTip_Button = By.CssSelector("span[class='glyphicon glyphicon-question-sign']");

        /**********Signal Plus***********/

        public static By SignalPlus_StickyHeader = By.CssSelector("header[class*='slideDown']");

        public static By SignalPlus_StickyHeader_All_Button = By.CssSelector("header[class*='slideDown'] * menu[class*='navigation'] * a[data-id='all']");

        public static By SignalPlus_StickyHeader_Selected_All_Button = By.CssSelector("header[class*='slideDown'] * menu[class*='navigation'] * a[class$='active'][data-id='all']");

        public static By SignalPlus_StickyHeader_Connectivity_Button = By.CssSelector("header[class*='slideDown'] * menu[class*='navigation'] * a[data-id='connectivity']");

        public static By SignalPlus_StickyHeader_Articles_Button = By.CssSelector("header[class*='slideDown']>div[class*='filters'] * a[data-id='Article']");

        public static By SignalPlus_StickyHeader_Selected_Articles_Button = By.CssSelector("header[class*='slideDown']>div[class*='filters'] * a[class='btn ch-filters__option'][data-id='Article']");

        public static By SignalPlus_StickyHeader_Videos_Button = By.CssSelector("header[class*='slideDown']>div[class*='filters'] * a[data-id='Video']");

        public static By SignalPlus_StickyHeader_Selected_Videos_Button = By.CssSelector("header[class*='slideDown']>div[class*='filters'] * a[class='btn ch-filters__option'][data-id='Video']");

        public static By SignalPlus_ArticleFacet = By.CssSelector("section>div>a[data-id='Article']");

        public static By SignalPlus_VideoFacet = By.CssSelector("section>div>a[data-id='Video']");

        public static By SignalPlus_ArticleSection = By.CssSelector("div[class='container article-cards']");

        public static By SignalPlus_ArticleCards_Link = By.CssSelector("a[class^='article-cards']");

        public static By SignalPlus_Logo = By.CssSelector("header[class='ch-masthead'] img[class='ch-masthead__logo']");

        public static By SignalPlus_HeroBanner = By.CssSelector("header[class='ch-masthead'] div[class*='ch-masthead__background']");

        public static By SignalPlus_RecommendedSection = By.CssSelector("section[class='recommended-content']");

        public static By SignalPlus_RecommendedSection_Title = By.CssSelector("section[class='recommended-content'] h3");

        public static By SignalPlus_RecommendedSection_Card = By.CssSelector("section[class='recommended-content'] div[class='recommended-content__container__card']");

        public static By SignalPlus_RecommendedSection_Card_Thumbnail = By.CssSelector("section[class='recommended-content'] div[class='recommended-content__container__card'] a>img");

        public static By SignalPlus_RecommendedSection_Card_Link = By.CssSelector("section[class='recommended-content'] div[class='recommended-content__container__card'] a>p");

        public static By SignalPlus_RelatedSection = By.CssSelector("aside[class='related col-xs-12 col-sm-4']");

        public static By SignalPlus_RelatedSection_Title = By.CssSelector("aside[class='related col-xs-12 col-sm-4'] h3");

        public static By SignalPlus_RelatedSection_MarketAndTechnology_Label = By.CssSelector("aside[class='related col-xs-12 col-sm-4'] div[class*='related__markets-and-technologies'] h4");

        public static By SignalPlus_RelatedSection_MarketSection = By.CssSelector("div[class*='related__markets-and-technologies']");

        public static By SignalPlus_RelatedSection_MarketAndTechnology_Category = By.CssSelector("aside[class='related col-xs-12 col-sm-4'] div[class*='related__markets-and-technologies']>ul>li a");

        public static By SignalPlus_RelatedSection_MarketAndTechnology_SubCategory = By.CssSelector("aside[class='related col-xs-12 col-sm-4'] div[class*='related__markets-and-technologies']>ul>li>div[id*='MarketTechnology'] ul>li>a");

        public static By SignalPlus_RelatedSection_ResourcesSection = By.CssSelector("div[class='related__resources']");

        public static By SignalPlus_RelatedSection_ResourcesSection_Title = By.CssSelector("div[class='related__resources'] h4");

        public static By SignalPlus_RelatedSection_ResourcesSection_ArticleCategory = By.CssSelector("div[class='related__resources'] ul>li>span");

        public static By SignalPlus_RelatedSection_ResourcesSection_ArticleLink = By.CssSelector("div[class='related__resources'] ul>li>a");

        public static By SignalPlus_RelatedSection_ProductsSection = By.CssSelector("div[class='related__generics']");

        public static By SignalPlus_RelatedSection_ProductsSection_Title = By.CssSelector("div[class='related__generics'] h4");

        public static By SignalPlus_RelatedSection_ProductNo = By.CssSelector("div[class='related__generics'] ul>li>a");

        public static By SignalPlus_RelatedSection_ProductDescription = By.CssSelector("div[class='related__generics'] ul>li>span");

        public static By SignalPlus_RelatedSection_ProductsCategory_Title = By.CssSelector("div[class='related__categories'] h4");

        public static By SignalPlus_RelatedSection_ProductsCategory_Link = By.CssSelector("div[class='related__categories']>ul>li a");

        public static By SignalPlus_RelatedSection_ProductsSubCategory_Link = By.CssSelector("div[class='related__categories']>ul>li>div[id*='Category-1'] ul>li>a");

        public static By SignalPlus_ContentHub_HeroBanner = By.CssSelector("section[id='mw-homePromo'] div[class='home-hero-bg visible-desktop'] img");

        public static By SignalPlus_Social_Tab = By.CssSelector("div[class='at-custom-sidebar-btns']");

        public static By SignalPlus_Social_FB = By.CssSelector("div[class='at-custom-sidebar-btns']>a[title='Facebook']");

        public static By SignalPlus_Social_Twitter = By.CssSelector("div[class='at-custom-sidebar-btns']>a[title='Twitter']");

        public static By SignalPlus_Social_LinkedIn = By.CssSelector("div[class='at-custom-sidebar-btns']>a[title='LinkedIn']");

        public static By SignalPlus_Social_Line = By.CssSelector("div[class='at-custom-sidebar-btns']>a[title='LINE']");

        public static By SignalPlus_Social_AddMore = By.CssSelector("div[class='at-custom-sidebar-btns']>a[title='More']");

        public static By SignalPlus_Subscribe_ExpandedView = By.CssSelector("div[class='ch-masthead__subscription__container ch-masthead__subscription__container--expanded']");

        public static By SignalPlus_Subscribe_Tab = By.CssSelector("header[class='ch-masthead'] li[class='ch-masthead__navigation__menu__subscribe']>a");

        public static By SignalPlus_Article_Subscribe_Tab = By.CssSelector("div[class='col-md-3 col-xs-12 article-secondary-nav__subscribe']>h3>a");

        public static By SignalPlus_Subscribe_Header = By.CssSelector("div[class='ch-masthead__subscription__container ch-masthead__subscription__container--expanded']>div[class='container ch-masthead__subscription__container__form'] h3");

        public static By SignalPlus_Subscribe_FName_Textfield = By.CssSelector("input[id='subscribe-firstName']");

        public static By SignalPlus_Subscribe_LName_Textfield = By.CssSelector("input[id='subscribe-lastName']");

        public static By SignalPlus_Subscribe_Email_Textfield = By.CssSelector("input[id='subscribe-email']");

        public static By SignalPlus_Subscribe_FName_OverheadLabel = By.CssSelector("label[for='subscribe-firstName']");

        public static By SignalPlus_Subscribe_LName_OverheadLabel = By.CssSelector("label[for='subscribe-lastName']");

        public static By SignalPlus_Subscribe_Email_OverheadLabel = By.CssSelector("label[for='subscribe-email']");

        public static By SignalPlus_Subscribe_Consent_Checkbox = By.CssSelector("input[id='termsofuse']");

        public static By SignalPlus_Subscribe_Consent_Message = By.CssSelector("label[for='termsofuse'] span");

        public static By SignalPlus_Subscribe_Communication_Checkbox = By.CssSelector("input[id='communication']");

        public static By SignalPlus_Subscribe_Communication_Message = By.CssSelector("label[for='communication']>span");

        public static By SignalPlus_Subscribe_Communication_AuthorizedLink = By.CssSelector("label[for='communication']>span>a");

        public static By SignalPlus_Subscribe_SubscribeToSignal_Button = By.CssSelector("div[class='col-md-12 ch-masthead__subscription__container__controls visible-desktop'] button[class='ch-masthead__subscription__container__controls__submit']");

        public static By SignalPlus_Subscribe_PrivacySettingsLink = By.CssSelector("a[title='privacy-settings']");

        public static By SignalPlus_Subscribe_PrivacyAndSecurityLink = By.CssSelector("a[title='privacy-and-security-statement']");

        public static By SignalPlus_Subscribe_AuthorizedPartnersLink = By.CssSelector("a[title='authorized-partners']");

        public static By SignalPlus_Subscribe_ErrorMessage_InvalidEmail = By.CssSelector("p[class='ch-masthead__subscription__container__form__error alert alert-danger']");

        public static By SignalPlus_Subscribe_SuccessMessage = By.CssSelector("div[class='container ch-masthead__subscription__container__thank-you'] h3[class='ch-masthead__subscription__container__title']");

        public static By SignalPlus_Subscribe_SuccessMessage_Sub = By.CssSelector("div[class='container ch-masthead__subscription__container__thank-you'] p");

        public static By SignalPlus_Subscribe_SuccessMessage_CloseBtn = By.CssSelector("div[class='container ch-masthead__subscription__container__thank-you'] a");

        //----- PST -----//

        public static By PST_Title_Txt = By.CssSelector("header>h1");

        public static By PST_Breadcrumb_CurrentPage_Txt = By.CssSelector("div[name='adi_breadcrumb'] * li:nth-last-of-type(1)>span");

        //----- Precision Quick Search -----//

        public static By PST_PrecisionQuickSearch_Resolution_Min_Slider = By.CssSelector("div[class='fields quick-filter']>div:nth-of-type(2) * span[class='irs-handle from']");

        public static By PST_PrecisionQuickSearch_Resolution_Max_Slider = By.CssSelector("div[class='fields quick-filter']>div:nth-of-type(2) * span[class='irs-handle to']");

        public static By PST_PrecisionQuickSearch_Resolution_Min_InputBox = By.CssSelector("input[id='resmin']");

        public static By PST_PrecisionQuickSearch_Resolution_Max_InputBox = By.CssSelector("input[id='resmax']");

        public static By PST_PrecisionQuickSearch_SampleRate_Min_Slider = By.CssSelector("div[class='fields quick-filter']>div:nth-of-type(3) * span[class='irs-handle from']");

        public static By PST_PrecisionQuickSearch_SampleRate_Max_Slider = By.CssSelector("div[class='fields quick-filter']>div:nth-of-type(3) * span[class='irs-handle to']");

        public static By PST_PrecisionQuickSearch_SampleRate_Min_InputBox = By.CssSelector("input[id='spsmin']");

        public static By PST_PrecisionQuickSearch_SampleRate_Max_InputBox = By.CssSelector("input[id='spsmax']");

        public static By PST_PrecisionQuickSearch_NumberOfInputChannels_Min_Slider = By.CssSelector("div[class='fields quick-filter']>div:nth-of-type(4) * span[class='irs-handle from']");

        public static By PST_PrecisionQuickSearch_NumberOfInputChannels_Max_Slider = By.CssSelector("div[class='fields quick-filter']>div:nth-of-type(4) * span[class='irs-handle to']");

        public static By PST_PrecisionQuickSearch_NumberOfInputChannels_Min_InputBox = By.CssSelector("input[id='chanmin']");

        public static By PST_PrecisionQuickSearch_NumberOfInputChannels_Max_InputBox = By.CssSelector("input[id='chanmax']");

        public static By PST_PrecisionQuickSearch_Automotive_Link = By.CssSelector("a[href*='Yes']");

        public static By PST_PrecisionQuickSearch_SmallSize_Link = By.CssSelector("a[href*='2|10']");

        public static By PST_PrecisionQuickSearch_LowPower_Link = By.CssSelector("a[href*='min|1m']");

        public static By PST_PrecisionQuickSearch_IntegratedVoltageReference_Link = By.CssSelector("a[href*='Internal']");

        public static By PST_PrecisionQuickSearch_AvailableAsDie_Link = By.CssSelector("a[href*='CHIPS OR DIE']");

        public static By PST_PrecisionQuickSearch_Interface_Dropdown = By.CssSelector("select[id='interface']");

        public static By PST_PrecisionQuickSearch_Architecture_Dropdown = By.CssSelector("select[id='architecture']");

        public static By PST_PrecisionQuickSearch_InputType_Dropdown = By.CssSelector("select[id='inputType']");

        public static By PST_PrecisionQuickSearch_Submit_Button = By.CssSelector("button[id='quickFilterSearch']");

        //----- Quick Power Search -----//

        public static By PST_QuickPowerSearch_ErrorMessage = By.CssSelector("div[id='qserrors']");

        public static By PST_QuickPowerSearch_VinMin_InputBox = By.CssSelector("input[id='vinmin']");

        public static By PST_QuickPowerSearch_VinMax_InputBox = By.CssSelector("input[id='vinmax']");

        public static By PST_QuickPowerSearch_VOut_InputBox = By.CssSelector("input[id='vout']");

        public static By PST_QuickPowerSearch_IOut_InputBox = By.CssSelector("input[id='iout']");

        public static By PST_QuickPowerSearch_LowEmi_CheckBox = By.CssSelector("input[id='lowemi']");

        public static By PST_QuickPowerSearch_Ultrathin_CheckBox = By.CssSelector("input[id='ultrathin']");

        public static By PST_QuickPowerSearch_InternalHeatSink_CheckBox = By.CssSelector("input[id='heatsink']");

        public static By PST_QuickPowerSearch_MultipleOutputs_Button = By.CssSelector("button[class='btn btn-primary btn-xs active']");

        public static By PST_QuickPowerSearch_VOut2_InputBox = By.CssSelector("input[id='vout2']");

        public static By PST_QuickPowerSearch_IOut2_InputBox = By.CssSelector("input[id='iout2']");

        public static By PST_QuickPowerSearch_VOut3_InputBox = By.CssSelector("input[id='vout3']");

        public static By PST_QuickPowerSearch_IOut3_InputBox = By.CssSelector("input[id='iout3']");

        public static By PST_QuickPowerSearch_Search_Button = By.CssSelector("button[id='quickFilterSearch']");

        //----- Table Navigation Main Buttons -----//

        public static By PST_ChooseParameters_Button = By.CssSelector("menu[class='tools']>div:nth-of-type(1)>button:nth-of-type(1)");

        public static By PST_All_Button = By.CssSelector("menu[class='tools']>div:nth-of-type(1)>button:nth-of-type(2)");

        public static By PST_ResetTable_Button = By.CssSelector("menu[class='tools']>div:nth-of-type(2)>button:nth-of-type(1)");

        public static By PST_MaximizeFilters_MinimizeFilters_Button = By.CssSelector("menu[class='tools']>div:nth-of-type(2)>button:nth-of-type(2)");

        public static By PST_SortByNewest_Button = By.CssSelector("menu[class='tools']>div:nth-of-type(2)>button:nth-of-type(3)");

        public static By PST_SaveToMyAnalog_Button = By.CssSelector("menu[class='tools']>div[class='green group']>button:nth-of-type(1)");

        public static By PST_SaveToMyAnalog_Widget_SaveTo_Tab = By.CssSelector("li[data-tab='saveToWidget']");

        public static By PST_SaveToMyAnalog_Widget_Selected_SaveTo_Tab = By.CssSelector("li[class='active'][data-tab='saveToWidget']");

        public static By PST_SaveToMyAnalog_Widget_SaveAllModelsTo_Dropdown = By.CssSelector("div[class^='select-product dropdown']");

        public static By PST_SaveToMyAnalog_Widget_TableName_InputBox = By.CssSelector("input[id='pstTableName']");

        public static By PST_SaveToMyAnalog_Widget_NewProject_Tab = By.CssSelector("li[data-tab='createProject']");

        public static By PST_SaveToMyAnalog_Widget_SavePst_Button = By.CssSelector("button[id='saveProductToMyanalog']");

        public static By PST_DownloadToExcel_Button = By.CssSelector("menu[class='tools']>div[class='green group']>button:nth-of-type(2)");

        public static By PST_Share_Button = By.CssSelector("menu[class='tools']>div[class='green group']>button:nth-of-type(3)");

        public static By PST_QuickTips_Button = By.CssSelector("menu[class='tools']>div[class='orange group']>button:nth-of-type(1)");

        public static By PST_QuickTips_DialogBox = By.CssSelector("div[class='react-joyride__tooltip']");

        public static By PST_QuickTips_DialogBox_Header_Label = By.CssSelector("div[class='react-joyride__tooltip'] * h4");

        public static By PST_QuickTips_DialogBox_X_Button = By.CssSelector("div[class='react-joyride__tooltip']>button[data-test-id='button-close']");

        public static By PST_QuickTips_DialogBox_Previous_Button = By.CssSelector("div[class='react-joyride__tooltip'] * button[data-test-id='button-back']");

        public static By PST_QuickTips_DialogBox_Next_Button = By.CssSelector("div[class='react-joyride__tooltip'] * button[data-test-id='button-primary']");

        public static By PST_QuickTips_DialogBox_Last_Button = By.CssSelector("div[class='react-joyride__tooltip'] * button[data-test-id='button-primary']");

        //----- Choose Parameters - Dialog Box -----//

        public static By PST_ChooseParameters_DialogBox = By.CssSelector("section[class='choose parameters']>div[class='dialog']");

        public static By PST_ChooseParameters_DialogBox_X_Button = By.CssSelector("section[class='choose parameters'] * button>i");

        public static By PST_ChooseParameters_DialogBox_SelectAll_Button = By.CssSelector("div[class='content options']>button:nth-of-type(1)");

        public static By PST_ChooseParameters_DialogBox_DeselectAll_Button = By.CssSelector("div[class='content options']>button:nth-of-type(2)");

        public static By PST_ChooseParameters_DialogBox_Default_Button = By.CssSelector("div[class='content options']>button:nth-of-type(3)");

        public static By PST_ChooseParameters_DialogBox_Checkboxes = By.CssSelector("section[class='choose parameters'] * input[type='checkbox']");

        public static By PST_ChooseParameters_DialogBox_Automotive_Checkbox = By.CssSelector("section[class='choose parameters'] * input[id$='s10']");

        public static By PST_ChooseParameters_DialogBox_Channels_Checkbox = By.CssSelector("section[class='choose parameters'] * input[id$='3062']");

        public static By PST_ChooseParameters_DialogBox_DataOutputInterface_Checkbox = By.CssSelector("section[class='choose parameters'] * input[id$='4365']");

        public static By PST_ChooseParameters_DialogBox_DeviceArchitecture_Checkbox = By.CssSelector("section[class='choose parameters'] * input[id$='4364']");

        public static By PST_ChooseParameters_DialogBox_InputType_Checkbox = By.CssSelector("section[class='choose parameters'] * input[id$='4363']");

        public static By PST_ChooseParameters_DialogBox_Package_Checkbox = By.CssSelector("section[class='choose parameters'] * input[id$='s5']");

        public static By PST_ChooseParameters_DialogBox_Resolution_Checkbox = By.CssSelector("section[class='choose parameters'] * input[id$='7']");

        public static By PST_ChooseParameters_DialogBox_SampleRate_Checkbox = By.CssSelector("section[class='choose parameters'] * input[id$='1746']");

        public static By PST_ChooseParameters_DialogBox_PackageArea_Checkbox = By.CssSelector("section[class='choose parameters'] * input[id$='s16']");

        public static By PST_ChooseParameters_DialogBox_VRefSource_Checkbox = By.CssSelector("section[class='choose parameters'] * input[id$='1747']");

        public static By PST_ChooseParameters_DialogBox_Ok_Button = By.CssSelector("div[class='footer']>button[class='button']");

        //----- Column Header Filter and Sort -----//

        public static By PST_Columns = By.CssSelector("section[class='parameters']>section");

        public static By PST_Minimized_Columns = By.CssSelector("section[class*='filter minimized']");

        public static By PST_Sorted_Columns = By.CssSelector("section[class='details'] * section[class$='filter active minimized draggable']");

        public static By PST_Column_Sort_Buttons = By.CssSelector("section[class='details'] * div[class='sortable title']");

        public static By PST_Column_Hide_Buttons = By.CssSelector("section[class='details']>section>section>button[class='custom action']");

        public static By PST_Rows = By.CssSelector("section[class='details']>article");

        public static By PST_Rows_NoResults_Label = By.CssSelector("section[class='details']>p[class='no-results']");

        //----- Part Number Column -----//

        public static By PST_Active_PartNumber_Column = By.CssSelector("section[class*='filter active'][data-parameter='parts']");

        //--- Toggle Description Column Button ---//

        public static By PST_ToggleDescriptionColumn_Button = By.CssSelector("button>img[src*='toggle-description']");

        public static By PST_Active_ToggleDescriptionColumn_Button = By.CssSelector("button[class$='active']>img[src*='toggle-description']");

        public static By PST_ToggleDescriptionColumn_Button_ToolTip = By.CssSelector("div[class='options']>button:nth-of-type(1)>div[class$='tooltip']>span");

        //--- Toggle Tools Column Button ---//

        public static By PST_ToggleToolsColumn_Button = By.CssSelector("button>img[src*='toggle-tools']");

        public static By PST_ToggleToolsColumn_Button_ToolTip = By.CssSelector("div[class='options']>button:nth-of-type(2)>div[class$='tooltip']>span");

        //--- Toggle Hardware Column Button ---//

        public static By PST_ToggleHardwareColumn_Button = By.CssSelector("div[class='options']>button:nth-of-type(3)");

        public static By PST_ToggleHardwareColumn_Button_ToolTip = By.CssSelector("div[class='options']>button:nth-of-type(3)>div[class$='tooltip']>span");

        //--- Toggle Status Column Button ---//

        public static By PST_ToggleStatusColumn_Button = By.CssSelector("button>img[src*='toggle-status']");

        public static By PST_ToggleStatusColumn_Button_ToolTip = By.CssSelector("div[class='options']>button:nth-of-type(4)>div[class$='tooltip']>span");

        public static By PST_Compare_ShowAll_Button = By.CssSelector("section[class='parts'] * button[class^='custom action']");

        public static By PST_PartNumber_Column_InputBox = By.CssSelector("section[class='parts'] * input[type='text']");

        public static By PST_PartNumber_Column_PartsCount_Label = By.CssSelector("section[class='parts'] * div[class='count']");

        public static By PST_PartNumber_Column_Rows = By.CssSelector("section[class='parts']>article");

        public static By PST_PartNumber_Column_1st_Row_Checkbox = By.CssSelector("section[class='parts']>article:nth-of-type(1) * input[type='checkbox']");

        public static By PST_PartNumber_Column_1st_Row_Link = By.CssSelector("section[class='parts']>article:nth-of-type(1) * a");

        public static By PST_PartNumber_Column_2nd_Row_Checkbox = By.CssSelector("section[class='parts']>article:nth-of-type(2) * input[type='checkbox']");

        public static By PST_PartNumber_Column_2nd_Row_Link = By.CssSelector("section[class='parts']>article:nth-of-type(2) * a");

        //----- Part Number Hover Window -----//

        public static By PST_PartNumber_HoverWindow = By.CssSelector("div[class='part popup']>div[class='content']");

        public static By PST_PartNumber_HoverWindow_PartNumber_Link = By.CssSelector("div[class='part popup']>div[class='content'] * h3>a");

        public static By PST_PartNumber_HoverWindow_DownloadDataSheet_Link = By.CssSelector("div[class='part popup']>div[class='content'] * a[href$='/datasheet']");

        public static By PST_PartNumber_HoverWindow_ViewDocumentation_Link = By.CssSelector("div[class='part popup']>div[class='content'] * a[href$='documentation']");

        public static By PST_PartNumber_HoverWindow_SampleAndBuy_Link = By.CssSelector("div[class='part popup']>div[class='content'] * a[href$='samplebuy']");

        public static By PST_PartNumber_HoverWindow_Diagram_Img = By.CssSelector("div[class='part popup']>div[class='content'] * img");

        //----- Automotive Column -----//

        public static By PST_Automotive_Column_Filter = By.CssSelector("section[data-parameter='s10'] * div[class='toggle']");

        public static By PST_Automotive_Column_Filter_Yes_Checkbox = By.CssSelector("section[data-parameter='s10'] * input[id='filter-s10-Yes']");

        public static By PST_Automotive_Column_Row_Values = By.CssSelector("div[data-parameter='s10']");

        //----- Channels Column -----//

        public static By PST_Channels_Column_Min_Value = By.CssSelector("section[data-parameter='3062'] * input[data-type='min']");

        public static By PST_Channels_Column_Max_Value = By.CssSelector("section[data-parameter='3062'] * input[data-type='max']");

        //----- Output Current Column -----//

        public static By PST_OutputCurrent_Column_Min_InputBox = By.CssSelector("section[data-parameter='5349'] * input[data-type='min']");

        //----- Package Area Column -----//

        public static By PST_Package_Column_Filter_ChipsOrDie_Checkbox = By.CssSelector("section[data-parameter='s5'] * input[id='filter-s5-CHIPS OR DIE']");

        //----- Package Area Column -----//

        public static By PST_PackageArea_Column_Min_Value = By.CssSelector("section[data-parameter='s16'] * input[data-type='min']");

        public static By PST_PackageArea_Column_Max_Value = By.CssSelector("section[data-parameter='s16'] * input[data-type='max']");

        //----- Power Column -----//

        public static By PST_Power_Column_Max_Value = By.CssSelector("section[data-parameter='3970'] * input[data-type='max']");

        //----- Product Description Column -----//

        public static By PST_ProductDescription_Column = By.CssSelector("section[data-parameter='s6']");

        public static By PST_ProductDescription_Column_InputBox = By.CssSelector("section[data-parameter='s6'] * input");

        //----- Resolution Column -----//

        public static By PST_Resolution_Column_Min_Value = By.CssSelector("section[data-parameter='7'] * input[data-type='min']");

        public static By PST_Resolution_Column_Max_Value = By.CssSelector("section[data-parameter='7'] * input[data-type='max']");

        //----- Sample Rate Column -----//

        public static By PST_SampleRate_Column_Min_Value = By.CssSelector("section[data-parameter='1746'] * input[data-type='min']");

        public static By PST_SampleRate_Column_Max_Value = By.CssSelector("section[data-parameter='1746'] * input[data-type='max']");

        //----- Vin (min | V) Column -----//

        public static By PST_Vin_min_V_Column_Max_InputBox = By.CssSelector("section[data-parameter='5573'] * input[data-type='max']");

        //----- Vin (max | V) Column -----//

        public static By PST_Vin_max_V_Column_Min_InputBox = By.CssSelector("section[data-parameter='5574'] * input[data-type='min']");

        //----- Vout Min Column -----//

        public static By PST_VOutMin_Column_Max_InputBox = By.CssSelector("section[data-parameter='5347'] * input[data-type='max']");

        //----- Vout Max Column -----//

        public static By PST_VOutMax_Column_Min_InputBox = By.CssSelector("section[data-parameter='5357'] * input[data-type='min']");

        //----- Vref Source Column -----//

        public static By PST_VRefSource_Column_Filter_Internal_Checkbox = By.CssSelector("section[data-parameter='1747'] * input[id='filter-1747-Internal']");

        /***********Quality And Reliability************/

        public static By QnR_ReliabilityData_WaferFabrication_Link = By.CssSelector("a[name*='wafer-fabrication']");

        public static By QnR_ReliabilityData_AssemblyPackage_Link = By.CssSelector("a[name*='assembly-package']");

        public static By QnR_SubCategory_heading = By.CssSelector("div[name='adi_threecolumn_content_section'] h2");

        public static By QnR_WaferFabrication_DataSummary_Table = By.CssSelector("table[id='Table3']");

        public static By QnR_WaferFabrication_ReliabilityCalc_Link = By.CssSelector("div[class='qua-col-md-12'] p>a");

        //public static By QnR_WaferFabrication_ProcessTech_Submit_Button = By.CssSelector("input[name='ctl08']");

        //public static By QnR_WaferFabrication_ProcessTech_DD = By.CssSelector("select[id='Select1']");

        //public static By QnR_WaferFabrication_ProcessTech_Result_Label = By.CssSelector("div[id='Process']>strong");

        public static By QnR_WaferFabrication_ProcessTech_Result_FirstTbl = By.CssSelector("div[id='divTotal0']");

        public static By QnR_WaferFabrication_ProcessTech_Result_SecondTbl = By.CssSelector("div[id='divResults0']");

        public static By QnR_WaferFabrication_ProcessTech_Temperature_TextField = By.CssSelector("input[id='txtTemp0']");

        public static By QnR_WaferFabrication_ProcessTech_Recalculate_Btn = By.CssSelector("input[id='btn30']");

        public static By QnR_WaferFabrication_ProcessTech_ErrorMessage = By.CssSelector("div[id='error_0']");

        public static By QnR_WaferFabrication_ProductPn_Submit_Button = By.CssSelector("input[name='ctl08']");

        public static By QnR_WaferFabrication_ProductPn_TxtField = By.CssSelector("input[id='productPin']");

        public static By QnR_WaferFabrication_ProductPn_Result_Label = By.CssSelector("div[id='Product']>strong");

        public static By QnR_WaferFabrication_BackToTop_Link = By.CssSelector("td[class='subCategoryText']>a");

        public static By QnR_AssemblyPackage_TestName_DD = By.CssSelector("select[id='Select1']");

        public static By QnR_AssemblyPackage_PackageFam_DD = By.CssSelector("select[id='Select2']");

        public static By QnR_AssemblyPackage_Submit_Btn = By.CssSelector("input[id='btnSubmit']");

        public static By QnR_AssemblyPackage_ModifiedDate = By.CssSelector("div[id='ModifiedDate']>p");

        public static By QnR_AssemblyPackage_Result_Tbl = By.CssSelector("div[id='HTMLData']>table[class='table table-bordered table-striped']");

        public static By QnR_AssemblyPackage_ReliabilityBuild = By.CssSelector("div[class='relability-build']");

        public static By QnR_AssemblyPackage_ReliabilityBuild_List = By.CssSelector("div[class='relability-build']>ul[class='realiablity-list']>li");

        public static By QnR_AssemblyPackage_ReliabilityProduct = By.CssSelector("div[class='relability-product']");

        public static By QnR_AssemblyPackage_ReliabilityProduct_List = By.CssSelector("div[class='relability-product']>ul[class='realiablity-list']>li");

        public static By QnR_AssemblyPackage_ReliabilityValid = By.CssSelector("div[class='relability-valid']");

        public static By QnR_AssemblyPackage_ReliabilityValid_List = By.CssSelector("div[class='relability-valid']>ul[class='realiablity-list");

        public static By QnR_MatDec_OtherInformation_Link = By.CssSelector("a[data-target*='Modal2420']");

        public static By QnR_MatDec_Disclaimer_Link = By.CssSelector("a[data-target*='Modal1516']");

        public static By QnR_MatDec_ModelSearch_Header = By.CssSelector("h3[class='panel-title']");

        public static By QnR_MatDec_ModelSearch_Multiple_RadioBtn = By.CssSelector("input[id='radbtnMultiple']");

        public static By QnR_MatDec_ModelSearch_Multiple_Help = By.CssSelector("a[data-original-title='Multi Search']");

        public static By QnR_MatDec_ModelSearch_Multiple_TextArea = By.CssSelector("textarea[id='txtMultiPartNo']");

        public static By QnR_MatDec_ModelSearch_Multiple_Error = By.CssSelector("label[id='lblMulProductTxtError']");

        public static By QnR_MatDec_ModelSearch_Multiple_ListError = By.CssSelector("span[id='lblMultiAllNotFound']");

        public static By QnR_MatDec_ModelSearch_Single_RadioBtn = By.CssSelector("input[id='radbtnSingle']");

        public static By QnR_MatDec_ModelSearch_Single_Help = By.CssSelector("a[data-original-title='Single product Search']");

        public static By QnR_MatDec_ModelSearch_Single_TextField = By.CssSelector("input[id='txtSinglePartNo']");

        public static By QnR_MatDec_ModelSearch_Single_Error = By.CssSelector("label[id='singleProductTxtError']");

        public static By QnR_MatDec_ModelSearch_Search_Btn = By.CssSelector("input[id='btnSearchNow']");

        public static By QnR_MatDec_ModelSearch_NoResult_Message = By.CssSelector("span[id='spnMsg']");

        public static By QnR_MatDec_ModelSearch_Result_Table = By.CssSelector("div[id='proSearchResult']");

        public static By QnR_MatDec_ModelSearch_LTCModal = By.CssSelector("div[id='LtcModelModal']");

        public static By QnR_MatDec_ModelSearch_LTCModal_CloseBtn = By.CssSelector("div[id='LtcModelModal'] button[id='LtcModelModalSuccess']");

        public static By QnR_MatDec_ModelSearch_SERP_GoToProductPage_Label = By.CssSelector("label[class='control-label']");

        public static By QnR_MatDec_ModelSearch_SERP_GoToProductPage_DD = By.CssSelector("select[id='ddlProduct']");

        public static By QnR_MatDec_ModelSearch_SERP_GoToProductPage_Go_Btn = By.CssSelector("input[id='btnGo']");

        public static By QnR_MatDec_ModelSearch_SERP_ViewProducts_Link = By.CssSelector("a[id='lnkbtnProductNotFound']");

        public static By QnR_MatDec_ModelSearch_SERP_InvalidModels = By.CssSelector("div[id='invalidModels']");

        public static By QnR_MatDec_ModelSearch_SERP_Suggestions = By.CssSelector("span[id='spnSuggestion'] ul");

        public static By QnR_MatDec_ModelSearch_SERP_Disclaimer_Link = By.CssSelector("div[id='Top']>a");

        public static By QnR_MatDec_ModelSearch_SERP_Link = By.CssSelector("div[id='Top']>a");

        public static By QnR_MatDec_ModelSearch_SERP_Pagination_First = By.CssSelector("ul[class='pagination pull-right']>li>a[id='imgbtnFirst']");

        public static By QnR_MatDec_ModelSearch_SERP_Pagination_Last = By.CssSelector("ul[class='pagination pull-right']>li>a[id='imgbtnLast']");

        public static By QnR_MatDec_ModelSearch_SERP_Pagination_Next = By.CssSelector("ul[class='pagination pull-right']>li>a[id='imgbtnNext']");

        public static By QnR_MatDec_ModelSearch_SERP_Pagination_Prev = By.CssSelector("ul[class='pagination pull-right']>li>a[id='imgbtnPrevious']");

        public static By QnR_MatDec_Display_DD = By.CssSelector("select[id='ddlNoOfResultsTop']");

        public static By QnR_MatDec_ModelSearch_SERP_Pagination_PageIndicator = By.CssSelector("span[id='spnPaginationRightTop']");

        public static By QnR_MatDec_ModelSearch_SERP_RemoveRows_Btn = By.CssSelector("input[id='btnRemoveRowsBtn']");

        public static By QnR_MatDec_ModelSearch_SERP_Pagination_ShowCount = By.CssSelector("span[id='spnPaginationLeftTop']");

        public static By QnR_MatDec_ModelSearch_SERP_RunNew = By.CssSelector("a[id='newSearch']");

        public static By QnR_MatDec_ModelSearch_SERP_GoToProduct_Btn = By.CssSelector("input[id='goToProduct']");

        public static By QnR_MatDec_ModelSearch_SERP_MatDec_Link = By.CssSelector("p[id='paraTextOne']>a");

        public static By QnR_MatDec_ModelSearch_SERP_RunNew_Btn = By.CssSelector("input[id='btnRunNewSearch']");

        public static By QnR_KPI_ModelSearch_SERP_Result_Tbl = By.CssSelector("div[id='meterialSearchResult']");

        /*********Tools Anomalie*********/
        public static By ToolsAnomalie_Families_Dropdown = By.CssSelector("select[id='selfamily']");

        public static By ToolsAnomalie_PageTitle = By.CssSelector("div[class='page-title']>h1");

        public static By ToolsAnomalie_Products_Dropdown = By.CssSelector("select[id='selproducts']");

        public static By ToolsAnomalie_Components_Dropdown = By.CssSelector("select[id='selcomponents']");

        public static By ToolsAnomalie_Keyword_TextField = By.CssSelector("input[id='txtKeyword']");

        public static By ToolsAnomalie_ReferenceNumbers_TextField = By.CssSelector("input[id='txtRefNum']");

        public static By ToolsAnomalie_Search_Btn = By.CssSelector("input[id='btn-epta-search']");

        public static By ToolsAnomalie_SERP_SearchResult_Label = By.CssSelector("div[class='ta-search-results']>div[class='header2']");

        public static By ToolsAnomalie_SERP_SearchList = By.CssSelector("div[class='ta-search-family']");

        public static By ToolsAnomalie_SERP_Criteria = By.CssSelector("div[class='panel panel-primary ta-search-criteria']");

        public static By ToolsAnomalie_SERP_Back_Btn = By.CssSelector("div[class='panel panel-primary ta-search-criteria'] input[id='btn-epta-back']");

        public static By ToolsAnomalie_SERP_NewSearch_Btn = By.CssSelector("div[class='panel panel-primary ta-search-criteria'] a[id='aToolsNewSearch']");

        //----- FORMS -----//

        public static By Forms_Title_Label = By.CssSelector("span[id$='PageTitle']");

        public static By Forms_AdiSimSrdDesignStudio_Title_Label = By.CssSelector("h1[id='H1']");

        public static By Forms_SmartmeshSoftwareAccessRequest_Title_Label = By.CssSelector("h1[id='landingTitle']");

        public static By Forms_Instructions_Text = By.CssSelector("div[class*='page-title']>p");

        public static By Forms_Captcha = By.CssSelector("div[id='rcaptcha'] * iframe");

        //----- Save time by logging in or registering for myAnalog -----//

        public static By Forms_SaveTimeByLoggingInOrRegisteringForMyAnalog_Link = By.CssSelector("h4[id$='LoginHeader'] * a");

        public static By Forms_Expanded_SaveTimeByLoggingInOrRegisteringForMyAnalog_Panel = By.CssSelector("div[id='collapseOne'][class$='collapse in']");

        public static By Forms_Expanded_SaveTimeByLoggingInOrRegisteringForMyAnalog_Panel_LoginToMyAnalog_Button = By.CssSelector("input[id$='btnLogin']");

        public static By Forms_Expanded_SaveTimeByLoggingInOrRegisteringForMyAnalog_Panel_RegisterNow_Link = By.CssSelector("div[id='collapseOne'] * a[href*='/registration']");

        //----- Request to Buy University Tools > Select the development tools to purchase -----//

        public static By Forms_SelectTheDevelopmentToolsToPurchase_Section_Label = By.CssSelector("div[class='form-horizontal']>div:nth-of-type(1) * h3");

        public static By Forms_SelectTheDevelopmentToolsToPurchase_Section_Description_Text = By.CssSelector("div[class='form-horizontal']>div:nth-of-type(2) * p");

        public static By Forms_SelectTheDevelopmentToolsToPurchase_Section_Table = By.CssSelector("table[class='table table-bordered table-striped']");

        public static By Forms_SelectTheDevelopmentToolsToPurchase_Section_Price_Values = By.CssSelector("table[class='table table-bordered table-striped'] * tr>td:nth-of-type(3)>span");

        public static By Forms_SelectTheDevelopmentToolsToPurchase_Section_Quantity_InputBoxes = By.CssSelector("input[id$='QTY']");

        public static By Forms_SelectTheDevelopmentToolsToPurchase_Section_Quantity_Range_ErrorMessage = By.CssSelector("span[id*='RangeValidator']");

        public static By Forms_SelectTheDevelopmentToolsToPurchase_Section_ReadOnly_Total_InputBoxes = By.CssSelector("input[id$='TOTAL'][readonly='readonly']");

        public static By Forms_SelectTheDevelopmentToolsToPurchase_Section_Reminder_Message = By.CssSelector("div[class='form-horizontal']>div:nth-of-type(3) * span[class='help-block']");

        public static By Forms_SelectTheDevelopmentToolsToPurchase_Section_Distributor_Link = By.CssSelector("a[href$='/sales-distribution']");

        //----- Privacy Settings -----//

        public static By Forms_Submit_Button = By.CssSelector("input[id$='submit']");

        //----- Sections (Note: This is being used by All the Forms Scripts.) -----//

        public static By Forms_Linear_Section = By.CssSelector("div[id='linear']");

        public static By Forms_HighSpeed_Section = By.CssSelector("div[id='HighSpeed']");

        public static By Forms_PrivacySettings_Section = By.CssSelector("div[id$='PrivacySetting']");

        public static By Forms_PrivacySettings_LegalDisclaimer_Text = By.CssSelector("p[class='legal-disclaimer']");

        //----- Labels (Note: This is being used by All the Forms Scripts.) -----//

        public static By Forms_CustomerServiceSupport_Country_Label = By.CssSelector("span[id$='lblCustomerServiceCountry']");

        public static By Forms_TechnicalSupport_Country_Label = By.CssSelector("span[id$='lblTechSupportCountry']");

        //----- Input Boxes (Note: This is being used by All the Forms Scripts.) -----//

        public static By Forms_Address_InputBox = By.CssSelector("input[id$='Address']");

        public static By Forms_Address_1_InputBox = By.CssSelector("input[id$='txtAddress']");

        public static By Forms_SmartmeshSoftwareAccessRequest_Address_1_InputBox = By.CssSelector("input[id$='address1']");

        public static By Forms_AdiSalesRepresentative_InputBox = By.CssSelector("input[id$='ADISalesRepresentativeContact']");

        public static By Forms_Company_InputBox = By.CssSelector("input[id$='CompanyName']");

        public static By Forms_CourseResearch_1_InputBox = By.CssSelector("input[id$='CourseResearch1']");

        public static By Forms_AddressLine_1_InputBox = By.CssSelector("input[id$='Addr1']");

        public static By Forms_SoftwareRequest_AddressLine_1_InputBox = By.CssSelector("input[id$='CompanyAddress']");

        public static By Forms_SoftwareRequest_AddressLine_2_InputBox = By.CssSelector("input[id$='CompanyAddress2']");

        public static By Forms_City_InputBox = By.CssSelector("input[id$='City']");

        public static By Forms_CityAndOthers_InputBox = By.CssSelector("input[id$='CityAndOthers']");

        public static By Forms_CompanyName_InputBox = By.CssSelector("input[id$='CompanyName']");

        public static By Forms_CompanyWebsite_InputBox = By.CssSelector("input[id$='CompanyWebsite']");

        public static By Forms_ContactName_InputBox = By.CssSelector("input[id$='ContactName']");

        public static By Forms_Department_InputBox = By.CssSelector("input[id$='Department']");

        public static By Forms_Email_InputBox = By.CssSelector("input[id$='Email']");

        public static By Forms_EmailId_InputBox = By.CssSelector("input[id$='Email'][type='text']");

        public static By Forms_CustomerServiceSupport_EmailId_InputBox = By.CssSelector("input[id$='CustomerServiceEmail']");

        public static By Forms_TechnicalSupport_Email_InputBox = By.CssSelector("input[id$='TechSupportEmail']");

        public static By Forms_FamilyName_InputBox = By.CssSelector("input[id$='FamilyName']");

        public static By Forms_FamilyNamePronounciation_InputBox = By.CssSelector("input[id$='FamilyNamePronounciation']");

        public static By Forms_Fax_InputBox = By.CssSelector("input[id$='Fax']");

        public static By Forms_FirstName_InputBox = By.CssSelector("input[id$='FirstName']");

        public static By Forms_ForWhichProductsDoYouNeedInformation_InputBox = By.CssSelector("input[id$='ProductInfo']");

        public static By Forms_FullName_InputBox = By.CssSelector("input[id$='FullName']");

        public static By Forms_GivenName_InputBox = By.CssSelector("input[id$='GivenName']");

        public static By Forms_GivenNamePronounciation_InputBox = By.CssSelector("input[id$='GivenNamePronounciation']");

        public static By Forms_HostId_InputBox = By.CssSelector("input[id*='Machine']");

        public static By Forms_IfOtherPleaseSpecify_InputBox = By.CssSelector("input[id$='txtOtherTool']");

        public static By Forms_SoftwareDownload_IfOtherPleaseSpecify_InputBox = By.CssSelector("input[id*='TargetHardwareProcessorOther']");

        public static By Forms_IfOutsideTheUsOrCanadaPleaseEnterTheRegion_InputBox = By.CssSelector("input[id$='Other']");

        public static By Forms_InputFrequency_InputBox = By.CssSelector("input[id$='InputFrequency']");

        public static By Forms_JobTitle_InputBox = By.CssSelector("input[id$='JobTitle']");

        public static By Forms_LastName_InputBox = By.CssSelector("input[id$='LastName']");

        public static By Forms_Name_InputBox = By.CssSelector("input[id$='Name']");

        public static By Forms_NumberOfStudents_1_InputBox = By.CssSelector("input[id$='Students1']");

        public static By Forms_HowDidYouObtainThisTestDrive_Other_InputBox = By.CssSelector("input[id$='Obtain']");

        public static By Forms_OrganizationName_InputBox = By.CssSelector("input[id$='CompanyName']");

        public static By Forms_OutputFrequency_InputBox = By.CssSelector("input[id$='OutputFrequency']");

        public static By Forms_PageNumber_InputBox = By.CssSelector("input[id$='PageNumber']");

        public static By Forms_PartNumber_InputBox = By.CssSelector("input[id$='PartNumber']");

        public static By Forms_Prefecture_InputBox = By.CssSelector("input[id$='Prefecture']");

        public static By Forms_PhoneNumber_InputBox = By.CssSelector("input[id$='PhoneNumber']");

        public static By Forms_PleaseProvideYourEmailIfYouWishSomeoneToContactYouRegardingThisInformation_InputBox = By.CssSelector("input[id$='EmailInclude']");

        public static By Forms_Role_InputBox = By.CssSelector("input[id$='Title']");

        public static By Forms_SectionGroupName_InputBox = By.CssSelector("input[id$='SectionGroupName']");

        public static By Forms_SerialNo_InputBox = By.CssSelector("input[id$='Serial']");

        public static By Forms_SpecificVersion_InputBox = By.CssSelector("input[id$='SpecificVersion']");

        public static By Forms_Telephone_InputBox = By.CssSelector("div>input[id$='hone']");

        public static By Forms_TheOthers_InputBox = By.CssSelector("input[id$='TheOthers']");

        public static By Forms_Title_InputBox = By.CssSelector("input[id$='Title']");

        public static By Forms_University_InputBox = By.CssSelector("input[id$='University']");

        public static By Forms_ZipPostalCode_InputBox = By.CssSelector("input[id*='Zip']");

        //----- Text Areas (Note: This is being used by All the Forms Scripts.) -----//

        public static By Forms_Comments_TextArea = By.CssSelector("textarea[id$='Comments']");

        public static By Forms_BrieflyDescribeYourApplication_TextArea = By.CssSelector("textarea[id$='BrieflyDescribeYourApplication']");

        public static By Forms_QuestionsComments_TextArea = By.CssSelector("textarea[id$='QuestionsComments']");

        public static By Forms_RequestQuestion_TextArea = By.CssSelector("textarea[id$='RequestQuestion']");

        public static By Forms_WhatIsYourAreaOfInterestForThisApplication_TextArea = By.CssSelector("textarea[id$='Interest']");

        public static By Forms_WhatIsYourTargetPlatformProcessorsAndOperatingSystem_TextArea = By.CssSelector("textarea[id$='targetplatform']");

        public static By Forms_WhyAreYouPrimarilyInterestedInThePlcDemoBoardSchematics_TextArea = By.CssSelector("textarea[id$='InterestInPLCDemo']");

        //----- Checkboxes (Note: This is being used by All the Forms Scripts.) -----//

        public static By Forms_AdiAuthorizedDistributor_Checkbox = By.CssSelector("input[id$='PrelimDataSheet_2']");

        public static By Forms_Agree_Checkbox = By.CssSelector("input[id$='agree']");

        public static By Forms_Communication_Checkbox = By.CssSelector("input[id$='communication']");

        public static By Forms_IAgreeToTheTermsAndConditions_Checkbox = By.CssSelector("input[id$='chkagree']");

        public static By Forms_ProductLine_Checkboxes = By.CssSelector("input[id*='ProductLine']");

        public static By Forms_SelectDistributor_Checkboxes = By.CssSelector("input[id*='chkPrelimDataSheet']");

        public static By Forms_SoftwareRequest_Checkboxes = By.CssSelector("input[id*='rptCategory_rptMapValue']");

        public static By Forms_WhatMarketsAreYouPrimarilyFocusedOn_Checkboxes = By.CssSelector("table[id$='MarketsPrimarilyFocussedOn'] * input");

        //----- Dropdowns (Note: This is being used by All the Forms Scripts.) -----//

        public static By Forms_Application_Dropdown = By.CssSelector("select[id$='ddlApplication']");

        public static By Forms_BestEstimate_Dropdown = By.CssSelector("select[id$='BestEstimate']");

        public static By Forms_CountryRegion_Dropdown = By.CssSelector("select[id$='ountry']");

        public static By Forms_DescribeApplication_Dropdown = By.CssSelector("select[id$='DescribeApplication']");

        public static By Forms_DevelopmentSchedule_Dropdown = By.CssSelector("select[id*='DevelopmentSchedule']");

        public static By Forms_HardwarePlatform_Dropdown = By.CssSelector("select[id$='HardwarePlatformDropDown']");

        public static By Forms_HowConfidentAreYouOfAchievingTheseVolumes_Dropdown = By.CssSelector("select[id$='HowConfidentAreYouAchievingVolumes']");

        public static By Forms_HowManyProcessorsAreYouLikelyToOrderOnAnAnnualBasis_Dropdown = By.CssSelector("select[id$='HowManyDSPsAreYouLikelyToOrder']");

        public static By Forms_JobTitle_Dropdown = By.CssSelector("select[id$='Title']");

        public static By Forms_ProcessorSoc_Dropdown = By.CssSelector("select[id$='ProductNo']");

        public static By Forms_SimulationToolUsed_Dropdown = By.CssSelector("select[id$='ddlSimulation']");

        public static By Forms_StateProvince_Dropdown = By.CssSelector("select[id$='tate']");

        public static By Forms_SelectDistributor_Dropdown = By.CssSelector("select[id$='Distributor']");

        public static By Forms_TargetHardware_Dropdown = By.CssSelector("select[id*='TargetHardware']");

        public static By Forms_TypeOfIssue_Dropdown = By.CssSelector("div[id='Div1']");

        public static By Forms_Volume_Dropdown = By.CssSelector("select[id$='Volume']");

        public static By Forms_SoftwareDownload_WhatIsThePrimaryMarketSegmentThatYourApplicationTargets_Dropdown = By.CssSelector("select[id$='PrimaryMarketSegment']");

        public static By Forms_VisualDspTestDriveRegistration_WhatIsThePrimaryMarketSegmentThatYourApplicationTargets_Dropdown = By.CssSelector("select[id$='Primary_Market_Segment']");

        public static By Forms_SoftwareDownload_WhatIsThePrimaryTypeOfApplicationThatYouDevelop_Dropdown = By.CssSelector("select[id$='PrimaryApplication']");

        public static By Forms_VisualDspTestDriveRegistration_WhatIsThePrimaryTypeOfApplicationThatYouDevelop_Dropdown = By.CssSelector("select[id$='Application_Type']");

        public static By Forms_WhatIsYourCircuitApplication_Dropdown = By.CssSelector("select[id$='CircuitApp']");

        public static By Forms_WhatIsYourEndMarket_Dropdown = By.CssSelector("select[id$='EndMarket']");

        public static By Forms_WhatStageIsYourApplicationIn_Dropdown = By.CssSelector("select[id$='WhatStageIsYourApplicationIn']");

        public static By Forms_WhenWillYourApplicationBeAvailableInTheMarketplace_Dropdown = By.CssSelector("select[id$='WhenWillYourApplicationBeAvailable']");

        public static By Forms_VisualDspLicenseRegistration_WhichEmbeddedProcessorAreYouEvaluating_Dropdown = By.CssSelector("select[id$='Evaluating_Which_DSP']");

        public static By Forms_VisualDspTestDriveRegistration_WhichEmbeddedProcessorAreYouEvaluating_Dropdown = By.CssSelector("select[id$='evaluating_dsp']");

        public static By Forms_WhichProductAreYouRegistering_Dropdown = By.CssSelector("select[id$='Tool_Model']");

        //----- Links (Note: This is being used by All the Forms Scripts.) -----//

        public static By Forms_AuthorizedPartners_Link = By.CssSelector("ul[class='list-inline']>li>a[href$='/sales-distribution.html']");

        public static By Forms_CustomerService_Link = By.CssSelector("a[href$='/customer-service-resources.html']");

        public static By Forms_EngineerZoneSupportForum_Link = By.CssSelector("div[class^='form-group'] * a[href*='ez.analog.com']");

        public static By Forms_PrivacySettings_Link = By.CssSelector("a[id='LinkWithoutLogin']");

        public static By Forms_PrivacyAndSecurityStatement_Link = By.CssSelector("ul[class='list-inline']>li>a[href$='/privacy_security_statement.html']");

        public static By Forms_TechnicalSupport_Link = By.CssSelector("a[href$='/technical-support']");

        public static By Forms_WebsiteFeedback_Link = By.CssSelector("a[href$='/websiteInfo.aspx']");

        //----- Radio Buttons (Note: This is being used by All the Forms Scripts.) -----//

        public static By Forms_Critical_RadioButton = By.CssSelector("input[value='Critical']");

        public static By Forms_Linear_RadioButton = By.CssSelector("input[id='Linear']");

        public static By Forms_Major_RadioButton = By.CssSelector("input[value='Major']");

        public static By Forms_Minor_RadioButton = By.CssSelector("input[value='Minor']");

        public static By Forms_NotADocumentError_RadioButton = By.CssSelector("input[value='Not a document error']");

        public static By Forms_DoYouHaveAnAdiContact_No_RadioButton = By.CssSelector("input[id$='rbtnADIContact_1']");

        public static By Forms_Processors_RadioButton = By.CssSelector("input[id='Processors']");

        public static By Forms_ProductDescription_RadioButtons = By.CssSelector("input[id*='rblProductFamily']");

        public static By Forms_ProductDescription_Other_RadioButton = By.CssSelector("input[value='Other']");

        public static By Forms_ProductDescription_SmartMeshWirelessHart_RadioButton = By.CssSelector("input[value='SmartMesh WirelessHART']");

        public static By Forms_SoftwareVersion_RadioButtons = By.CssSelector("input[id*='rblSoftwareVersion']");

        public static By Forms_SoftwareVersion_CurrentVersion_RadioButton = By.CssSelector("input[value='Current Version']");

        public static By Forms_SoftwareVersion_OtherPleaseSpecifyBelow_RadioButton = By.CssSelector("input[value='Other, please specify below']");

        public static By Forms_HowDidYouObtainThisTestDrive_RadioButtons = By.CssSelector("table[id$='rbtnlstObtain']>tbody>tr * input");

        public static By Forms_HowDidYouObtainThisTestDrive_Other_RadioButton = By.CssSelector("table[id$='rbtnlstObtain']>tbody>tr:nth-last-child(1) * input");

        public static By Forms_WhatTypeOfSupportDoYouNeed_RadioButtons = By.CssSelector("input[id*='rbSupport']");

        public static By Forms_WhichTestDriveAreYouRegistering_RadioButtons = By.CssSelector("table[id$='rbtnlstTestDrive']>tbody>tr:nth-last-of-type(1) * input");

        //----- Software Request Form -----//

        public static By Forms_SoftwareRequest_Label = By.CssSelector("h1[id='landingTitle']");

        public static By Forms_VisitTheAdiSoftwarePortal_Link = By.CssSelector("a[href$='/software.html']");

        public static By Forms_SoftwareRecipientInformation_Label = By.CssSelector("div>div[class$='section-bar']:nth-of-type(1) * h3");

        public static By Forms_CommercialInformation_Label = By.CssSelector("div>div[class$='section-bar']:nth-of-type(4) * h3");

        public static By Forms_SoftwareRequested_Label = By.CssSelector("div>div[class$='section-bar']:nth-of-type(6) * h3");

        public static By Forms_SoftwareRequested_Section_Description_Text = By.CssSelector("span[id$='SoftwareRequested1']");

        public static By Forms_DoYouRequireEvaluationOrProductionCodeAtThisStage_Note = By.CssSelector("div[class='form-group']>div>span[class='help-block']");

        public static By Forms_PrivacySettings_Description_Text = By.CssSelector("p[class='legal-disclaimer']");

        //----- ADIsimPLL Request for Software Form -----//

        public static By Forms_AdiSimPllRequestForSoftware_Video = By.CssSelector("div[id='vjs_video_3']>div[class='vjs-poster']");

        //----- VisualDSP++ License Registration Form -----//

        public static By Forms_VisualDspLicenseRegistration_Description_Text = By.CssSelector("div[class^='title'] * p");

        public static By Forms_VisualDspLicenseRegistration_TestDriveRegistrationForm_Link = By.CssSelector("a[href$='/visualDSPTestDrive.aspx']");

        //----- Catalina Design Form -----//

        public static By Forms_CatalinaDesign_Country_Label = By.CssSelector("span[id$='Label6']");

        //----- Software Registration Form -----//

        public static By Forms_SoftwareRegistration_Country_Label = By.CssSelector("span[id$='Label3']");

        //----- Design File Package Form -----//

        public static By Forms_DesignFilePackage_Country_Label = By.CssSelector("span[id$='Label3']");

        //----- Error Messages -----//

        public static By Forms_Address_BlankInput_ErrorMessage = By.CssSelector("span[id$='rfvAddress1']>span");

        public static By Forms_Address_1_BlankInput_ErrorMessage = By.CssSelector("span[id$='rfvAddress1']>span");

        public static By Forms_Application_BlankInput_ErrorMessage = By.CssSelector("span[id$='rfvApplication']>span");

        public static By Forms_City_BlankInput_ErrorMessage = By.CssSelector("span[id$='rfvCity']>span");

        public static By Forms_CompanyName_BlankInput_ErrorMessage = By.CssSelector("span[id$='rfvCompanyName']>span");

        public static By Forms_ContactName_BlankInput_ErrorMessage = By.CssSelector("span[id$='ContactName']>span");

        public static By Forms_CountryRegion_BlankInput_ErrorMessage = By.CssSelector("span[id$='ountry']>span[class='help-block']");

        public static By Forms_Email_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFVEmail']>span");

        public static By Forms_Email_InvalidInput_ErrorMessage = By.CssSelector("span[id='spanEmailREV']");

        public static By Forms_FirstName_BlankInput_ErrorMessage = By.CssSelector("span[id$='rfvirstName']>span");

        public static By Forms_FullName_BlankInput_ErrorMessage = By.CssSelector("span[id$='FullName']>span[class='help-block']");

        public static By Forms_FirstName_BlankInput_ErrorMessage_Catalina = By.CssSelector("span[id$='rfvFirstName']>span");

        public static By Forms_LastName_BlankInput_ErrorMessage_Catalina = By.CssSelector("span[id$='rfvLastName']>span");

        public static By Forms_HostId_BlankInput_ErrorMessage = By.CssSelector("span[id$='rfvMachine']>span");

        public static By Forms_SoftwareRegistration_HostId_BlankInput_ErrorMessage = By.CssSelector("span[id$='ReqFVHstID']>span");

        public static By Forms_IfOutsideTheUsOrCanadaPleaseEnterTheRegion_BlankInput_ErrorMessage = By.CssSelector("span[id$='cvOtherRegion']>span");

        public static By Forms_InputFrequency_BlankInput_ErrorMessage = By.CssSelector("span[id$='rfvInputFrequency']>span");

        public static By Forms_JobTitle_Dropdown_BlankInput_ErrorMessage = By.CssSelector("span[id$='rfvDDLTitle']>span");

        public static By Forms_JobTitle_InputBox_BlankInput_ErrorMessage = By.CssSelector("span[id$='rfvJobTitle']>span");

        public static By Forms_LastName_BlankInput_ErrorMessage = By.CssSelector("span[id$='rfvLastName']>span");

        public static By Forms_OrganizationName_BlankInput_ErrorMessage = By.CssSelector("span[id$='CompanyName']>span[class='help-block']");

        public static By Forms_OutputFrequency_BlankInput_ErrorMessage = By.CssSelector("span[id$='rfvOutputFrequency']>span");

        public static By Forms_PartNumber_BlankInput_ErrorMessage = By.CssSelector("span[id$='cvrDocTitlePartNo']>span");

        public static By Forms_Phone_BlankInput_ErrorMessage = By.CssSelector("span[id$='rfvTelephone']>span");

        public static By Forms_PleaseProvideYourEmailIfYouWishSomeoneToContactYouRegardingThisInformation_BlankInput_ErrorMessage = By.CssSelector("span[id$='rfvEmail']>span");

        public static By Forms_PleaseProvideYourSerialNumberExactlyAsItAppearsOnTheMediaThatYouReceived_BlankInput_ErrorMessage = By.CssSelector("span[id$='ReqFVSer']>span");

        public static By Forms_PleaseWriteYourQuestionsOrCommentsHere_BlankInput_ErrorMessage = By.CssSelector("span[id$='rfComment']>span");

        public static By Forms_PleaseWriteYourQuestionsOrCommentsHere_InvalidInput_ErrorMessage = By.CssSelector("span[id$='revtxtComments']>span");

        public static By Forms_SerialNo_BlankInput_ErrorMessage = By.CssSelector("span[id$='rfvSerial']>span");

        public static By Forms_StateProvince_BlankInput_ErrorMessage = By.CssSelector("span[id$='tate']>span[class='help-block']");

        public static By Forms_Telephone_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFVTelephone']>span");

        public static By Forms_WhatIsThePrimaryMarketSegmentThatYourApplicationTargets_BlankInput_ErrorMessage = By.CssSelector("span[id$='rfvPrimary_Market_Segment']>span");

        public static By Forms_WhatIsThePrimaryTypeOfApplicationThatYouDevelop_BlankInput_ErrorMessage = By.CssSelector("span[id$='cvApplication_Type']>span");

        public static By Forms_WhatIsYourCircuitApplication_BlankInput_ErrorMessage = By.CssSelector("span[id$='rfvCircuitApp']>span");

        public static By Forms_WhatIsYourEndMarket_BlankInput_ErrorMessage = By.CssSelector("span[id$='rfvEndMarket']>span");

        public static By Forms_WhichEmbeddedProcessorAreYouEvaluating_BlankInput_ErrorMessage = By.CssSelector("span[id$='cvEvaluatingProcessors']>span");

        public static By Forms_WhyAreYouPrimarilyInterestedInThePlcDemoBoardSchematics_BlankInput_ErrorMessage = By.CssSelector("span[id$='rfvInterestInPLCDemo']>span");

        public static By Forms_ZipPostalCode_BlankInput_ErrorMessage = By.CssSelector("span[id$='rfvZip']>span");

        //----- Request to Buy University Tools Form > Error Messages -----//

        public static By Forms_RequestToBuyUniversityTools_AddressLine_1_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV6']>span");

        public static By Forms_RequestToBuyUniversityTools_City_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV7']>span");

        public static By Forms_RequestToBuyUniversityTools_CourseResearch_1_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV04']>span");

        public static By Forms_RequestToBuyUniversityTools_Department_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV00']>span");

        public static By Forms_RequestToBuyUniversityTools_Email_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV1']>span");

        public static By Forms_RequestToBuyUniversityTools_Email_InvalidInput_ErrorMessage = By.CssSelector("span[id$='REV0']>span");

        public static By Forms_RequestToBuyUniversityTools_Fax_BlankInput_ErrorMessage = By.CssSelector("span[id$='RequiredFieldValidator3']>span");

        public static By Forms_RequestToBuyUniversityTools_Name_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV2']>span");

        public static By Forms_RequestToBuyUniversityTools_NumberOfStudents_1_BlankInput_ErrorMessage = By.CssSelector("span[id$='RequiredFieldValidator2']>span");

        public static By Forms_RequestToBuyUniversityTools_Telephone_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV9']>span");

        public static By Forms_RequestToBuyUniversityTools_Title_BlankInput_ErrorMessage = By.CssSelector("span[id$='RequiredFieldValidator1']>span");

        public static By Forms_RequestToBuyUniversityTools_University_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV3']>span");

        public static By Forms_RequestToBuyUniversityTools_ZipPostalCode_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV8']>span");

        //----- VisualDSP++ Test Drive Registration Form > Error Messages -----//

        public static By Forms_VisualDspTestDriveRegistration_Email_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV0']>span");

        public static By Forms_VisualDspTestDriveRegistration_Email_InvalidInput_ErrorMessage = By.CssSelector("span[id$='REV1']>span");

        public static By Forms_VisualDspTestDriveRegistration_HowDidYouObtainThisTestDrive_BlankInput_ErrorMessage = By.CssSelector("span[id$='cvrbtnlstObtain']>span");

        public static By Forms_VisualDspTestDriveRegistration_Telephone_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV9']>span");

        public static By Forms_VisualDspTestDriveRegistration_WhatIsThePrimaryTypeOfApplicationThatYouDevelop_BlankInput_ErrorMessage = By.CssSelector("span[id$='Application_Type']>span");

        public static By Forms_VisualDspTestDriveRegistration_WhichTestDriveAreYouRegistering_BlankInput_ErrorMessage = By.CssSelector("span[id$='rfvrbtnlstTestDrive']>span");

        //----- Software Download Form > Error Messages -----//

        public static By Forms_SoftwareDownload_City_BlankInput_ErrorMessage = By.CssSelector("span[id$='RequiredFieldValidator5']>span");

        public static By Forms_SoftwareDownload_Telephone_BlankInput_ErrorMessage = By.CssSelector("span[id$='rfvPhone']>span");

        public static By Forms_SoftwareDownload_WhenWillYourApplicationBeAvailableInTheMarketplace_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV4']>span");

        public static By Forms_SoftwareDownload_IfOtherPleaseSpecify_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV8']>span");

        public static By Forms_SoftwareDownload_HowManyProcessorsAreYouLikelyToOrderOnAnAnnualBasis_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV5']>span");

        public static By Forms_SoftwareDownload_HowConfidentAreYouOfAchievingTheseVolumes_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV6']>span");

        //----- Software Request Form > Error Messages -----//

        public static By Forms_SoftwareRequest_FirstName_BlankInput_ErrorMessage = By.CssSelector("span[id$='RequiredFieldValidator5']>span");

        public static By Forms_SoftwareRequest_Email_BlankInput_ErrorMessage = By.CssSelector("span[id$='RequiredFieldValidator6']>span");

        public static By Forms_SoftwareRequest_CompanyName_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV2']>span");

        public static By Forms_SoftwareRequest_AddressLine_1_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV6']>span");

        public static By Forms_SoftwareRequest_City_BlankInput_ErrorMessage = By.CssSelector("span[id$='RequiredFieldValidator3']>span");

        public static By Forms_SoftwareRequest_ZipPostalCode_BlankInput_ErrorMessage = By.CssSelector("span[id$='RequiredFieldValidator2']>span");

        public static By Forms_SoftwareRequest_LastName_BlankInput_ErrorMessage = By.CssSelector("span[id$='RequiredFieldValidator1']>span");

        public static By Forms_SoftwareRequest_CompanyWebsite_BlankInput_ErrorMessage = By.CssSelector("span[id$='RequiredFieldValidator4']>span");

        public static By Forms_SoftwareRequest_PhoneNumber_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV4']>span");

        public static By Forms_SoftwareRequest_WhatMarketsAreYouPrimarilyFocusedOn_BlankInput_ErrorMessage = By.CssSelector("span[id$='CV17']>span");

        public static By Forms_SoftwareRequest_WhatStageIsYourApplicationIn_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV21']>span");

        public static By Forms_SoftwareRequest_WhenWillYourApplicationBeAvailableInTheMarketplace_BlankInput_ErrorMessage = By.CssSelector("span[id$='RequiredFieldValidator7']>span");

        public static By Forms_SoftwareRequest_HowManyProcessorsAreYouLikelyToOrderOnAnAnnualBasis_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV23']>span");

        public static By Forms_SoftwareRequest_HowConfidentAreYouOfAchievingTheseVolumes_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV25']>span");

        public static By Forms_SoftwareRequest_BrieflyDescribeYourApplication_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV24']>span");

        public static By Forms_SoftwareRequest_TargetHardware_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV19']>span");

        public static By Forms_SoftwareRequested_BlankInput_ErrorMessage = By.CssSelector("span[id$='CVRE0']>span");

        public static By Forms_SoftwareRequest_Email_InvalidInput_ErrorMessage = By.CssSelector("span[id$='RegularExpressionValidator2']>span");

        public static By Forms_SoftwareRequest_WhatMarketsAreYouPrimarilyFocusedOn_InvalidInput_ErrorMessage = By.CssSelector("span[id$='CV17']>span");

        //----- ADIsimRF REQUEST FOR SOFTWARE > Error Messages -----//

        public static By Forms_AdiSimRfRequestForSoftware_Email_InvalidInput_ErrorMessage = By.CssSelector("span[id$='REV1']>span");

        public static By Forms_AdiSimRfRequestForSoftware_FirstName_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV2']>span");

        public static By Forms_AdiSimRfRequestForSoftware_LastName_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV3']>span");

        public static By Forms_AdiSimRfRequestForSoftware_OrganizationName_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV5']>span");

        public static By Forms_AdiSimRfRequestForSoftware_City_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV6']>span");

        public static By Forms_AdiSimRfRequestForSoftware_Email_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV0']>span");

        //----- ADIsimPLL Request for Software Form > Error Messages -----//

        public static By Forms_AdiSimPllRequestForSoftware_FirstName_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV2']>span");

        public static By Forms_AdiSimPllRequestForSoftware_LastName_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV3']>span");

        public static By Forms_AdiSimPllRequestForSoftware_OrganizationName_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV5']>span");

        public static By Forms_AdiSimPllRequestForSoftware_City_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV6']>span");

        public static By Forms_AdiSimPllRequestForSoftware_EmailId_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV1']>span");

        public static By Forms_AdiSimPllRequestForSoftware_EmailId_InvalidInput_ErrorMessage = By.CssSelector("span[id$='REV0']>span");

        //----- VisualDSP++ License Registration Form > Error Messages -----//

        public static By Forms_VisualDspLicenseRegistration_Email_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV0']>span");

        public static By Forms_VisualDspLicenseRegistration_Telephone_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV9']>span");

        public static By Forms_VisualDspLicenseRegistration_SelectDistributor_BlankInput_ErrorMessage = By.CssSelector("span[id$='CV0']");

        public static By Forms_VisualDspLicenseRegistration_Email_InvalidInput_ErrorMessage = By.CssSelector("span[id$='REV1']>span");

        //----- Document Feedback Form > Error Messages -----//

        public static By Forms_DocumentFeedback_Priority_BlankInput_ErrorMessage = By.CssSelector("span[id$='RequiredFieldValidator1']>span");

        public static By Forms_DocumentFeedback_DocumentTitle_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV1']>span");

        public static By Forms_DocumentFeedback_Email_InvalidInput_ErrorMessage = By.CssSelector("span[id$='REVEmail1']>span");

        //----- PLC Demo Schematics and Layout > Error Messages -----//

        public static By Forms_PlcDemoSchematicsAndLayout_Email_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV0']>span");

        public static By Forms_PlcDemoSchematicsAndLayout_Role_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV2']>span");

        public static By Forms_PlcDemoSchematicsAndLayout_Email_InvalidInput_ErrorMessage = By.CssSelector("span[id$='REV1']>span");

        //----- ADIsimCLK Request For Software Form > Error Messages -----//

        public static By Forms_AdiSimClkRequestForSoftware_EmailId_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV0']>span");

        public static By Forms_AdiSimClkRequestForSoftware_Telephone_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV9']>span");

        public static By Forms_AdiSimClkRequestForSoftware_EmailId_InvalidInput_ErrorMessage = By.CssSelector("span[id$='REV1']>span");

        //----- Catalina Design Form > Error Messages -----//

        public static By Forms_CatalinaDesign_Address_BlankInput_ErrorMessage = By.CssSelector("span[id$='RequiredFieldValidator2']>span");

        public static By Forms_CatalinaDesign_City_BlankInput_ErrorMessage = By.CssSelector("span[id$='RequiredFieldValidator1']>span");

        public static By Forms_CatalinaDesign_CountryRegion_BlankInput_ErrorMessage = By.CssSelector("span[id$='RequiredFieldValidator4']");

        public static By Forms_CatalinaDesign_Email_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV0']>span");

        public static By Forms_CatalinaDesign_StateProvince_BlankInput_ErrorMessage = By.CssSelector("span[id$='RequiredFieldValidator5']");

        public static By Forms_CatalinaDesign_Telephone_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV9']>span");

        public static By Forms_CatalinaDesign_ZipPostalCode_BlankInput_ErrorMessage = By.CssSelector("span[id$='RequiredFieldValidator3']>span");

        public static By Forms_CatalinaDesign_Email_InvalidInput_ErrorMessage = By.CssSelector("span[id$='REV1']>span");

        //----- Dialogue Feedback Form > Error Messages -----///

        public static By Forms_DialogueFeedbacks_Email_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV1']>span");

        public static By Forms_DialogueFeedbacks_EnterYourCommentsHere_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV15']>span");

        public static By Forms_DialogueFeedbacks_Name_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV2']>span");

        public static By Forms_DialogueFeedbacks_Email_InvalidInput_ErrorMessage = By.CssSelector("span[id$='REV0']>span");

        //----- ADI Web Site Support Form > Error Messages -----///

        public static By Forms_AdiWebSiteSupport_EmailId_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV1']>span");

        public static By Forms_AdiWebSiteSupport_EnterYourCommentsHere_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV15']>span");

        public static By Forms_AdiWebSiteSupport_FirstName_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV2']>span");

        public static By Forms_AdiWebSiteSupport_LastName_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV3']>span");

        public static By Forms_AdiWebSiteSupport_OrganizationName_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV5']>span");

        public static By Forms_AdiWebSiteSupport_EmailId_InvalidInput_ErrorMessage = By.CssSelector("span[id$='REV0']>span");

        //----- Contact I/O Subsystems Support Form > Error Messages -----///

        public static By Forms_ContactIOSubsystemsSupport_AddressLine_1_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV6']>span");

        public static By Forms_ContactIOSubsystemsSupport_City_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV7']>span");

        public static By Forms_ContactIOSubsystemsSupport_Email_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV1']>span");

        public static By Forms_ContactIOSubsystemsSupport_Name_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV3']>span");

        public static By Forms_ContactIOSubsystemsSupport_OrganizationName_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV5']>span");

        public static By Forms_ContactIOSubsystemsSupport_ZipPostalCode_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV8']>span");

        public static By Forms_ContactIOSubsystemsSupport_Telephone_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV9']>span");

        public static By Forms_ContactIOSubsystemsSupport_ProductLine_BlankInput_ErrorMessage = By.CssSelector("span[id$='CV0']");

        public static By Forms_ContactIOSubsystemsSupport_RequestQuestion_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV15']>span");

        public static By Forms_ContactIOSubsystemsSupport_Email_InvalidInput_ErrorMessage = By.CssSelector("span[id$='REV0']>span");

        //----- Customer Service Suppor Form > Error Messages -----///

        public static By Forms_CustomerServiceSupport_CountryRegion_BlankInput_ErrorMessage = By.CssSelector("span[id$='RequiredFieldValidator1']>span");

        public static By Forms_CustomerServiceSupport_EmailId_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV1']>span");

        public static By Forms_CustomerServiceSupport_FirstName_BlankInput_ErrorMessage = By.CssSelector("span[id$='RequiredFieldValidator2']>span");

        public static By Forms_CustomerServiceSupport_LastName_BlankInput_ErrorMessage = By.CssSelector("span[id$='RequiredFieldValidator3']>span");

        public static By Forms_CustomerServiceSupport_OrganizationName_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV5']>span");

        public static By Forms_CustomerServiceSupport_Telephone_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV9']>span");

        public static By Forms_CustomerServiceSupport_WhatTypeOfSupportDoYouNeed_InvalidInput_ErrorMessage = By.CssSelector("span[id$='RFV10']>span");

        public static By Forms_CustomerServiceSupport_ZipCode_InvalidInput_ErrorMessage = By.CssSelector("span[id$='RequiredFieldValidator4']>span");

        public static By Forms_CustomerServiceSupport_EmailId_InvalidInput_ErrorMessage = By.CssSelector("span[id$='REV0']>span");

        //----- Technical Support Form > Error Messages -----///

        public static By Forms_TechnicalSupport_Address_BlankInput_ErrorMessage = By.CssSelector("span[id='spanAddress']");

        public static By Forms_TechnicalSupport_City_BlankInput_ErrorMessage = By.CssSelector("span[id='spanCity']");

        public static By Forms_TechnicalSupport_CountryRegion_BlankInput_ErrorMessage = By.CssSelector("span[id='spanCountry']");

        public static By Forms_TechnicalSupport_DevelopmentSoftware_BlankInput_ErrorMessage = By.CssSelector("span[id='spandevelopmenttools']");

        public static By Forms_TechnicalSupport_Email_BlankInput_ErrorMessage = By.CssSelector("span[id='spanEmailRfv']");

        public static By Forms_TechnicalSupport_Emulator_BlankInput_ErrorMessage = By.CssSelector("span[id='spanEmulator']");

        public static By Forms_TechnicalSupport_FirstName_BlankInput_ErrorMessage = By.CssSelector("span[id='spanFirstName']");

        public static By Forms_TechnicalSupport_HardwarePlatform_BlankInput_ErrorMessage = By.CssSelector("span[id='spanHardwarePlatform']");

        public static By Forms_TechnicalSupport_HostOperatingSystem_BlankInput_ErrorMessage = By.CssSelector("span[id='spanOperatingSystem']");

        public static By Forms_TechnicalSupport_LastName_BlankInput_ErrorMessage = By.CssSelector("span[id='spanLastName']");

        public static By Forms_TechnicalSupport_OrganizationName_BlankInput_ErrorMessage = By.CssSelector("span[id='spanOrganization']");

        public static By Forms_TechnicalSupport_PartNumber_LinearProductCategory_BlankInput_ErrorMessage = By.CssSelector("span[id='spanPdtCat']");

        public static By Forms_TechnicalSupport_Processor_BlankInput_ErrorMessage = By.CssSelector("span[id='spanProcessor']");

        public static By Forms_TechnicalSupport_RequestDescription_BlankInput_ErrorMessage = By.CssSelector("span[id='spanInqDescDSP']");

        public static By Forms_TechnicalSupport_SelectRequestType_BlankInput_ErrorMessage = By.CssSelector("span[id='spanrequesttype']");

        public static By Forms_TechnicalSupport_StateProvince_BlankInput_ErrorMessage = By.CssSelector("span[id='spanState']");

        public static By Forms_TechnicalSupport_Linear_Subject_BlankInput_ErrorMessage = By.CssSelector("span[id='spanSubject']");

        public static By Forms_TechnicalSupport_Processors_Subject_BlankInput_ErrorMessage = By.CssSelector("span[id='spanSubjectDSP']");

        public static By Forms_TechnicalSupport_TargetSoftware_BlankInput_ErrorMessage = By.CssSelector("span[id='spantargetsoftware']");

        public static By Forms_TechnicalSupport_Telephone_BlankInput_ErrorMessage = By.CssSelector("span[id='spanTelephone']");

        public static By Forms_TechnicalSupport_TypeOfIssue_BlankInput_ErrorMessage = By.CssSelector("span[id='spanTypeOfIssueEMB']");

        public static By Forms_TechnicalSupport_WhatDesignCycleStageAreYouIn_BlankInput_ErrorMessage = By.CssSelector("span[id='spanDesignStage']");

        public static By Forms_TechnicalSupport_WhatIsTheEndUseApplicationThesePartsWillBeUsedFor_BlankInput_ErrorMessage = By.CssSelector("span[id='spanEndApplication']");

        public static By Forms_TechnicalSupport_WhatIsYourEstimatedAnnualVolume_BlankInput_ErrorMessage = By.CssSelector("span[id='spanEstimatedAnnualVolume']");

        public static By Forms_TechnicalSupport_ZipCode_BlankInput_ErrorMessage = By.CssSelector("span[id='spanZipCode']");

        //----- Technical Support Form > Error Messages -----///

        public static By Forms_AdiSiteSearchSupport_PleaseProvideYourEmailIfYouWishSomeoneToContactYouRegardingThisInformation_InvalidInput_ErrorMessage = By.CssSelector("span[id$='RegularExpressionValidator1']>span");

        //----- Contact Distributor Corner Support Form > Error Messages -----///

        public static By Forms_ContactDistributorCornerSupport_Email_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV1']>span");

        public static By Forms_ContactDistributorCornerSupport_Name_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV2']>span");

        public static By Forms_ContactDistributorCornerSupport_OrganizationName_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV5']>span");

        public static By Forms_ContactDistributorCornerSupport_Telephone_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV9']>span");

        public static By Forms_ContactDistributorCornerSupport_WhatTypeOfSupportDoYouNeed_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV10']>span");

        public static By Forms_ContactDistributorCornerSupport_Email_InvalidInput_ErrorMessage = By.CssSelector("span[id$='REV0']>span");

        //----- Software Registration Form > Error Messages -----//

        public static By Forms_SoftwareRegistration_EmailId_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV0']>span");

        public static By Forms_SoftwareRegistration_Telephone_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV9']>span");

        public static By Forms_SoftwareRegistration_EmailId_InvalidInput_ErrorMessage = By.CssSelector("span[id$='REV1']>span");

        //----- Healthcare Software Request Form > Error Messages -----//

        public static By Forms_HealthcareSoftwareRequest_AddressLine_1_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV6']>span");

        public static By Forms_HealthcareSoftwareRequest_AdiSalesRepresentativeContact_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV16']>span");

        public static By Forms_HealthcareSoftwareRequest_BrieflyDescribeYourApplication_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV24']>span");

        public static By Forms_HealthcareSoftwareRequest_EmailId_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV0']>span");

        public static By Forms_HealthcareSoftwareRequest_City_BlankInput_ErrorMessage = By.CssSelector("span[id$='RequiredFieldValidator2']>span");

        public static By Forms_HealthcareSoftwareRequest_CompanyWebsite_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV13']>span");

        public static By Forms_HealthcareSoftwareRequest_FirstName_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV1']>span");

        public static By Forms_HealthcareSoftwareRequest_IAgreeToTheTermsAndConditions_BlankInput_ErrorMessage = By.CssSelector("span[id$='CustomValidator2']>span");

        public static By Forms_HealthcareSoftwareRequest_LastName_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV3']>span");

        public static By Forms_HealthcareSoftwareRequest_OrganizationName_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV2']>span");

        public static By Forms_HealthcareSoftwareRequest_PhoneNumber_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV4']>span");

        public static By Forms_HealthcareSoftwareRequest_WhatIsYourTargetPlatformProcessorsAndOperatingSystem_BlankInput_ErrorMessage = By.CssSelector("span[id$='RequiredFieldValidator1']>span");

        public static By Forms_HealthcareSoftwareRequest_WhatMarketsAreYouPrimarilyFocusedOn_BlankInput_ErrorMessage = By.CssSelector("span[id$='CV17']>span");

        public static By Forms_HealthcareSoftwareRequest_WhatStageIsYourApplicationIn_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV21']>span");

        public static By Forms_HealthcareSoftwareRequest_WhenWillYourApplicationBeAvailableInTheMarketplace_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV22']>span");

        public static By Forms_HealthcareSoftwareRequest_ZipPostalCode_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV10']>span");

        //----- Design File Package Form > Error Messages -----//

        public static By Forms_DesignFilePackage_SimulationToolUsed_BlankInput_ErrorMessage = By.CssSelector("span[id$='RequiredFieldValidator6']>span");

        public static By Forms_DesignFilePackage_StateProvince_BlankInput_ErrorMessage = By.CssSelector("span[id$='RequiredFieldValidator5']>span");

        //----- Smartmesh Software Access Request > Error Messages -----//

        public static By Forms_SmartmeshSoftwareAccessRequest_Address_1_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV3']>span");

        public static By Forms_SmartmeshSoftwareAccessRequest_City_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV4']>span");

        public static By Forms_SmartmeshSoftwareAccessRequest_CountryRegion_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV5']>span");

        public static By Forms_SmartmeshSoftwareAccessRequest_Email_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV0']>span");

        public static By Forms_SmartmeshSoftwareAccessRequest_FirstName_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV1']>span");

        public static By Forms_SmartmeshSoftwareAccessRequest_LastName_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV2']>span");

        public static By Forms_SmartmeshSoftwareAccessRequest_OrganizationName_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV8']>span");

        public static By Forms_SmartmeshSoftwareAccessRequest_PhoneNumber_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV7']>span");

        public static By Forms_SmartmeshSoftwareAccessRequest_ProductDescription_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV9']>span");

        public static By Forms_SmartmeshSoftwareAccessRequest_SoftwareVersion_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV10']>span");

        public static By Forms_SmartmeshSoftwareAccessRequest_StateProvince_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV6']>span");

        //----- China Customer Service Form > Error Messages -----//

        public static By Forms_ChinaCustomerService_CompanyName_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV5']>span");

        public static By Forms_ChinaCustomerService_Email_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV1']>span");

        public static By Forms_ChinaCustomerService_FirstName_BlankInput_ErrorMessage = By.CssSelector("span[id$='RequiredFieldValidator2']>span");

        public static By Forms_ChinaCustomerService_LastName_BlankInput_ErrorMessage = By.CssSelector("span[id$='RequiredFieldValidator3']>span");

        public static By Forms_ChinaCustomerService_Telephone_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV9']>span");

        public static By Forms_ChinaCustomerService_WhatTypeOfSupportDoYouNeed_InvalidInput_ErrorMessage = By.CssSelector("span[id$='RFV10']>span");

        public static By Forms_ChinaCustomerService_Email_InvalidInput_ErrorMessage = By.CssSelector("span[id$='REV0']>span");

        //----- Japan Technical Support Form > Error Messages -----//

        public static By Forms_JapanTechnicalSupport_DescribeApplication_BlankInput_ErrorMessage = By.CssSelector("span[id='spanDescribeApplication']");

        public static By Forms_JapanTechnicalSupport_FamilyNamePronounciation_BlankInput_ErrorMessage = By.CssSelector("span[id='spanFamilyNamePronounciation']");

        public static By Forms_JapanTechnicalSupport_GivenNamePronounciation_BlankInput_ErrorMessage = By.CssSelector("span[id='spanGivenNamePronounciation']");

        public static By Forms_JapanTechnicalSupport_PartNumber_BlankInput_ErrorMessage = By.CssSelector("span[id='spanPartNumber']");

        public static By Forms_JapanTechnicalSupport_QuestionsComments_BlankInput_ErrorMessage = By.CssSelector("span[id='spanQuestionsComments']");

        public static By Forms_JapanTechnicalSupport_Volume_BlankInput_ErrorMessage = By.CssSelector("span[id='spanddlVolume']");

        //----- Downloads Form -----//

        public static By DownloadsForm = By.CssSelector("div[class^='ui-dialog ui-widget']");

        public static By DownloadsForms_Download_Button = By.CssSelector("div[class$='buttonset']>button:nth-of-type(1)");

        public static By DownloadsForms_X_Button = By.CssSelector("button[class$='close']");

        public static By DownloadsForms_Country_Label = By.CssSelector("label[for='country']");

        public static By DownloadsForms_Company_BlankInput_ErrorMessage = By.CssSelector("span[id$='Company']");

        public static By DownloadsForms_CountryRegion_BlankInput_ErrorMessag = By.CssSelector("span[id$='Country']");

        public static By DownloadsForms_DesignStage_BlankInput_ErrorMessage = By.CssSelector("span[id$='DesignStage']");

        public static By DownloadsForms_Email_BlankInput_ErrorMessage = By.CssSelector("span[id$='Email']");

        public static By DownloadsForms_FirstName_BlankInput_ErrorMessage = By.CssSelector("span[id$='First']");

        public static By DownloadsForms_LastName_BlankInput_ErrorMessage = By.CssSelector("span[id$='Last']");

        //----- Hittite Form -----//

        public static By HittiteForms_AdiHmcPllDesignAndEvaluationSoftware_Email_BlankInput_ErrorMessage = By.CssSelector("span[id$='RFV0']>span");

        public static By HittiteForms_AdiHmcPllDesignAndEvaluationSoftware_LicenseAgreement_BlankInput_ErrorMessage = By.CssSelector("span[id$='CustomValidator2']>span");

        public static By HittiteForms_AdiHmcPllDesignAndEvaluationSoftware_Email_InvalidInput_ErrorMessage = By.CssSelector("span[id$='REV1']>span");

        //----- ADIsimCLK Version Request for Software -----//

        public static By AdiSimClkReferenceDesignFiles_AdiSimClkReferenceDesignFiles_Link = By.CssSelector("a[href$='/adisimclk-reference-design-files.html']");

        //----- SDR Integrated Transceiver Design Resources -----//

        public static By SdrIntegratedTransceiverDesignResources_IDecline_Button = By.CssSelector("input[value='I Decline']");

        //----- Software Modules Download Listing -----//

        public static By SoftwareModulesDownloadListing_Software_Link = By.CssSelector("a[id='hlSoftwareLink']");

        public static By SoftwareModulesDownloadListing_Download_Button = By.CssSelector("a[id='ancDownloadbtn']");

        //----- Thank you for registering your free 90-day test drive -----//

        public static By ThankYouForRegisteringYourFree90DayTestDrive_DownloadTestDrive_Button = By.CssSelector("a[href$='/vdsp-bf-sh-ts.html']");

        //----- MYANALOG -----//

        //----- MYANALOG Mega Menu -----//

        //----- Logged Out State -----//

        public static By MyAnalog_MegaMenu_RegisterNow_Header_Text = By.CssSelector("div[class*='register'] * div[class='header']");

        public static By MyAnalog_MegaMenu_RegisterNow_Description_Text = By.CssSelector("div[class*='register']>div>p");

        public static By MyAnalog_MegaMenu_GetStarted_Button = By.CssSelector("div[class*='register'] * div[class='header']");

        public static By MyAnalog_MegaMenu_Video = By.CssSelector("div[id='videoTemplate'] * video");

        public static By MyAnalog_MegaMenu_ExploreMyAnalogFeaturesNow_Link = By.CssSelector("a[class='explore']");

        public static By MyAnalog_MegaMenu_Login_Header_Text = By.CssSelector("div[class$='login']>form>div[class='header']");

        public static By MyAnalog_MegaMenu_Login_Description_Text = By.CssSelector("div[class$='login']>form>p:nth-of-type(1)");

        public static By MyAnalog_MegaMenu_LogInToMyAnalog_Button = By.CssSelector("button[class='ma btn btn-primary']");

        public static By MyAnalog_MegaMenu_RegisterNow_Link = By.CssSelector("div[class$='login']>form>p>a");

        public static By MyAnalog_MegaMenu_DontHaveAnAccount_Text = By.CssSelector("div[class$='login']>form>p:nth-last-of-type(1)");

        //----- Logged In State -----//

        public static By MyAnalog_MegaMenu_Tab_Wave_Icon = By.CssSelector("a[data-qa-element='link_myAnalog'] * img");

        public static By MyAnalog_MegaMenu_Tab_Content = By.CssSelector("div[class='container']>div[class$='widgets']");

        //--- Profile Section ---//

        public static By MyAnalog_MegaMenu_Profile_Section = By.CssSelector("section[class='profile widget']");

        public static By MyAnalog_MegaMenu_Initials_Text = By.CssSelector("div[class='widget profile'] * span[class='initials']");

        public static By MyAnalog_MegaMenu_SignOut_Link = By.CssSelector("a[class='logout']");

        public static By MyAnalog_MegaMenu_MyAnalogDashboard_Button = By.CssSelector("a[class$='dashboard']");

        public static By MyAnalog_MegaMenu_FindYourLocalSalesContact_Link = By.CssSelector("a[class='local sales']");

        public static By MyAnalog_MegaMenu_DistributorPortal_Link = By.CssSelector("a[class='distributor portal']");

        //--- Projects Tile ---//

        public static By MyAnalog_MegaMenu_Projects_Tile = By.CssSelector("div[data-widget='projects']");

        public static By MyAnalog_MegaMenu_Projects_Link = By.CssSelector("div[class*='projects']>div[class='title']>a");

        public static By MyAnalog_MegaMenu_Created_Project_Name_Link = By.CssSelector("div[class='project']>a");

        public static By MyAnalog_MegaMenu_Created_Project_ModifiedDate_Label = By.CssSelector("p[class='timestamp']");

        public static By MyAnalog_MegaMenu_Created_Project_Contents = By.CssSelector("div[class='project']>div[class='info']");

        public static By MyAnalog_MegaMenu_CreateAProject_Button = By.CssSelector("a[class='create project']");

        public static By MyAnalog_MegaMenu_SeeAllYourProjects_Link = By.CssSelector("div[class*='projects'] * div[class='more']>a");

        //--- Products Tile ---//

        public static By MyAnalog_MegaMenu_Products_Tile = By.CssSelector("div[data-widget='products']");

        public static By MyAnalog_MegaMenu_NoSavedProduct_Message = By.CssSelector("div[data-widget='products'] * div[class='default state']");

        public static By MyAnalog_MegaMenu_Products_Link = By.CssSelector("div[data-widget='products']>div>a");

        public static By MyAnalog_MegaMenu_Saved_Products = By.CssSelector("div[class='info']>ul>li");

        public static By MyAnalog_MegaMenu_Saved_Products_Name_Links = By.CssSelector("li>span:nth-of-type(1)>a");

        public static By MyAnalog_MegaMenu_Saved_Products_Datasheet_Links = By.CssSelector("span[class='datasheet']>a");

        public static By MyAnalog_MegaMenu_SeeAllYourProducts_Link = By.CssSelector("div[data-widget='products'] * div[class='more']>a");

        //--- Categories Tile ---//

        public static By MyAnalog_MegaMenu_Categories_Tile = By.CssSelector("div[data-widget='productcategory']");

        public static By MyAnalog_MegaMenu_Categories_Link = By.CssSelector("div[data-widget='productcategory']>div>a");

        public static By MyAnalog_MegaMenu_Saved_ProductCategory_Links = By.CssSelector("div[data-widget='productcategory'] * div[class='details'] * a");

        //--- Markets Tile ---//

        public static By MyAnalog_MegaMenu_Markets_Tile = By.CssSelector("div[data-widget='applications']");

        public static By MyAnalog_MegaMenu_Markets_Link = By.CssSelector("div[data-widget='applications']>div>a");

        public static By MyAnalog_MegaMenu_Saved_Market_Links = By.CssSelector("div[data-widget='applications'] * div[class='details'] * a");

        //--- Parametric Searches Tile ---//

        public static By MyAnalog_MegaMenu_ParametricSearches_Tile = By.CssSelector("div[data-widget='parametric-search']");

        public static By MyAnalog_MegaMenu_ParametricSearches_Link = By.CssSelector("div[data-widget='parametric-search']>div>a");

        public static By MyAnalog_MegaMenu_Saved_Pst_Title_Links = By.CssSelector("div[data-widget='parametric-search'] * div[class='details'] * a");

        public static By MyAnalog_MegaMenu_SeeAllYourParametricSearches_Link = By.CssSelector("div[data-widget='parametric-search'] * div[class='more']>a");

        //--- Resources Tile ---//

        public static By MyAnalog_MegaMenu_Resources_Tile = By.CssSelector("div[data-widget='resources']");

        public static By MyAnalog_MegaMenu_Resources_Link = By.CssSelector("div[data-widget='resources']>div>a");

        public static By MyAnalog_MegaMenu_Saved_Resources_Title_Links = By.CssSelector("div[data-widget='resources'] * div[class='details'] * a");

        public static By MyAnalog_MegaMenu_SeeAllYourResources_Link = By.CssSelector("div[data-widget='resources'] * div[class='more']>a");

        //--- EngineerZone Tile ---//

        public static By MyAnalog_MegaMenu_EngineerZone_Tile = By.CssSelector("div[data-widget='engineerzone']");

        //--- Orders Tile ---//

        public static By MyAnalog_MegaMenu_Orders_Tile = By.CssSelector("div[data-widget='orders']");

        public static By MyAnalog_MegaMenu_Orders_Link = By.CssSelector("div[data-widget='orders']>div>a");

        public static By MyAnalog_MegaMenu_PlacedOrder_Number_Links = By.CssSelector("div[data-widget='orders'] * div[class='details'] * a");

        public static By MyAnalog_MegaMenu_PlacedOrder_Dates = By.CssSelector("div[data-widget='orders'] * div[class='details'] * p");

        public static By MyAnalog_MegaMenu_SeeAllYourOrders_Link = By.CssSelector("div[data-widget='orders'] * div[class='more']>a");

        //--- Profile Section ---//

        public static By MyAnalog_Profile_Section = By.CssSelector("section[class='profile widget']");

        public static By MyAnalog_Profile_Percentage = By.CssSelector("section[class='profile widget']>div>div>svg[class='doughnut']>text");

        public static By MyAnalog_Profile_Color = By.CssSelector("section[class*='profile']>div>div:nth-of-type(1)>div>div");

        public static By MyAnalog_Profile_Initials_Value = By.CssSelector("div[class$='avatar editable']>span[class='initials']");

        public static By MyAnalog_Profile_Edit_Button = By.CssSelector("button[class='edit']");

        public static By MyAnalog_Profile_EditProfile_Menu = By.CssSelector("menu[class='options dropdown']");

        public static By MyAnalog_Profile_EditProfile_Menu_ProfileCompletion_Percentage = By.CssSelector("section[class='profile widget']>menu * svg[class='doughnut']>text");

        public static By MyAnalog_Profile_EditProfile_Menu_Profile_Color = By.CssSelector("section[class='profile widget'] menu>div>div:nth-of-type(1)");

        public static By MyAnalog_Profile_EditProfile_Menu_Initials_Value = By.CssSelector("menu[class='options dropdown'] * span[class='initials']");

        public static By MyAnalog_Profile_EditProfile_Menu_ProfileScore_Checklist = By.CssSelector("dl[class='account completion']>dd");

        public static By MyAnalog_Profile_EditProfile_Menu_Color_Selection = By.CssSelector("menu[class='options dropdown'] * div[class='palette']>button");

        public static By MyAnalog_Profile_EditProfile_Menu_SaveAndClose_Button = By.CssSelector("button[class='item']");

        public static By MyAnalog_Profile_Username_Label = By.CssSelector("section[class*='profile'] * div[class='username']");

        //----- Navigation Section -----//

        public static By MyAnalog_Navigation_Selected_Link = By.CssSelector("a[class='active']");

        public static By MyAnalog_Navigation_MyAnalog_Link = By.CssSelector("ul[class='nav nav-pills nav-stacked'] * a[href$='/app/']");

        public static By MyAnalog_Navigation_AccountSettings_Link = By.CssSelector("ul[class='nav nav-pills nav-stacked'] * a[href$='/app/account']");

        public static By MyAnalog_Navigation_Products_Link = By.CssSelector("ul[class='nav nav-pills nav-stacked'] * a[href$='/app/products']");

        public static By MyAnalog_Navigation_Orders_Link = By.CssSelector("ul[class='nav nav-pills nav-stacked'] * a[href$='/app/orders']");

        public static By MyAnalog_Navigation_Projects_Link = By.CssSelector("ul[class='nav nav-pills nav-stacked'] * a[href$='/app/projects']");

        public static By MyAnalog_Navigation_Resources_Link = By.CssSelector("ul[class='nav nav-pills nav-stacked'] * a[href$='/app/resources']");

        public static By MyAnalog_Navigation_ParametricSearches_Link = By.CssSelector("ul[class='nav nav-pills nav-stacked'] * a[href$='/app/parametric-searches']");

        public static By MyAnalog_Navigation_Subscriptions_Link = By.CssSelector("ul[class='nav nav-pills nav-stacked'] * a[href$='/app/subscriptions']");

        public static By MyAnalog_Navigation_ManageContacts_Link = By.CssSelector("ul[class='nav nav-pills nav-stacked'] * a[href$='/app/manage-contacts']");

        //----- Find Your Sales Contact Section -----//

        public static By MyAnalog_FindYourSalesContact_Logo = By.CssSelector("section[class='sales contact'] * img");

        public static By MyAnalog_FindYourSalesContact_Link = By.CssSelector("section[class='sales contact']>a");

        //----- Hyperion Distributor Portal Section -----//

        public static By MyAnalog_Hyperion_Logo = By.CssSelector("section[class='hyperion portal'] * img");

        public static By MyAnalog_DistributorPortal_Link = By.CssSelector("section[class='hyperion portal']>a");

        //----- Dashboard -----//

        //--- Support Tickets Section ---//

        public static By MyAnalog_Dashboard_SupportTickets_Label = By.CssSelector("section[class='support widget']>div[class='section header']>h2");

        public static By MyAnalog_Dashboard_SeeAllYourSupportTickets_Link = By.CssSelector("section[class='support widget'] div[class='see all support']");

        public static By MyAnalog_Dashboard_SupportTicket_CaseTitle_Link = By.CssSelector("section[class='support widget'] * div[class='awssld__box awssld--active'] * h2>a");

        public static By MyAnalog_Dashboard_SupportTicket_ModifiedDate = By.CssSelector("section[class='support widget'] * div[class='awssld__box awssld--active'] * div[class='meta data']>span");

        public static By MyAnalog_Dashboard_CaseNumber_Statistic = By.CssSelector("section[class='support widget'] * div[class='awssld__box awssld--active'] * div[class^='blue statistic']");

        public static By MyAnalog_Dashboard_PrimaryProduct_Statistic = By.CssSelector("section[class='support widget'] * div[class='awssld__box awssld--active'] * div[class^='yellow statistic']");

        public static By MyAnalog_Dashboard_Status_Statistic = By.CssSelector("section[class='support widget'] * div[class='awssld__box awssld--active'] * div[class^='green statistic']");

        public static By MyAnalog_Dashboard_SupportTickets_Left_Arrow_Button = By.CssSelector("section[class='support widget'] div[class='awssld__controls']>button[class='awssld__prev']>img");

        public static By MyAnalog_Dashboard_SupportTickets_Right_Arrow_Button = By.CssSelector("section[class='support widget'] div[class='awssld__controls']>button[class='awssld__next']>img");

        public static By MyAnalog_Dashboard_CreateANewSupportTicket_Link = By.CssSelector("div[class='support'] button[class='ma btn btn-link']");

        //--- Your Projects Section ---//

        public static By MyAnalog_Dashboard_YourProjects_Label = By.CssSelector("section[class='projects widget']>div[class='section header']>h2");

        public static By MyAnalog_Dashboard_SeeAllYourProjects_Link = By.CssSelector("section[class='projects widget'] div[class='see all projects']");

        public static By MyAnalog_Dashboard_ProjectName_Link = By.CssSelector("section[class='projects widget'] * div[class='item active'] * h2>a");

        public static By MyAnalog_Dashboard_Project_ModifiedDate = By.CssSelector("section[class='projects widget'] * div[class='item active'] * div[class='meta data']>span");

        public static By MyAnalog_Dashboard_Contributors_Statistic = By.CssSelector("section[class='projects widget'] * div[class='item active'] * div[class^='blue statistic']");

        public static By MyAnalog_Dashboard_Products_Statistic = By.CssSelector("section[class='projects widget'] * div[class='item active'] * div[class^='red statistic']");

        public static By MyAnalog_Dashboard_Products_Statistic_Count = By.CssSelector("section[class='projects widget'] * div[class='item active'] * div[class^='red statistic']>span[class='count']");

        public static By MyAnalog_Dashboard_Resources_Statistic = By.CssSelector("section[class='projects widget'] * div[class='item active'] * div[class^='yellow statistic']");

        public static By MyAnalog_Dashboard_Psts_Statistic = By.CssSelector("section[class='projects widget'] * div[class='item active'] * div[class^='green statistic']");

        public static By MyAnalog_Dashboard_Psts_Statistic_Count = By.CssSelector("section[class='projects widget'] * div[class='item active'] * div[class^='green statistic']>span[class='count']");

        public static By MyAnalog_Dashboard_YourProjects_Left_Arrow_Button = By.CssSelector("section[class='projects widget'] * span[class='leftControl']>img");

        public static By MyAnalog_Dashboard_YourProjects_Right_Arrow_Button = By.CssSelector("section[class='projects widget'] * span[class='rightControl']>img");

        public static By MyAnalog_Dashboard_Notes_Statistic = By.CssSelector("section[class='projects widget'] * div[class='item active'] * div[class^='purple statistic']");

        public static By MyAnalog_Dashboard_YourProjects_Description_Header = By.CssSelector("div[class='projects'] * div>h4");

        public static By MyAnalog_Dashboard_YourProjects_Description_Text = By.CssSelector("div[class='projects']  * div[class='no-projects-data__content__text']");

        public static By MyAnalog_Dashboard_YourProjects_Video = By.CssSelector("div[class='projects'] * div[class='vjs-poster']");

        public static By MyAnalog_Dashboard_CreateANewProject_Link = By.CssSelector("div[class='projects'] button[class='ma btn btn-link']");

        public static By MyAnalog_Dashboard_CreateANewProject_Dialog = By.CssSelector("div[class='create project'] * div[class='modal-content']");

        //--- Saved Products Section ---//

        public static By MyAnalog_Dashboard_SavedProducts_Label = By.CssSelector("section[class^='products widget']>div[class='section header']>h2");

        public static By MyAnalog_Dashboard_SeeAllYourProducts_Link = By.CssSelector("div[class='see all products']>a");

        public static By MyAnalog_Dashboard_SavedProducts_NoData_Text = By.CssSelector("section[class^='products widget'] div[class^='no data']");

        public static By MyAnalog_Dashboard_Product_Tiles = By.CssSelector("div[class='products']>div>div");

        public static By MyAnalog_Dashboard_Product_Tiles_Zoom_Button = By.CssSelector("div[class='products']>div>div * button[class='zoom']>img");

        public static By MyAnalog_Dashboard_Product_Tiles_Image = By.CssSelector("div[class='products']>div>div * div>img");

        public static By MyAnalog_Dashboard_Product_Tiles_Status = By.CssSelector("div[class='products']>div>div * div[class='status']");

        public static By MyAnalog_Dashboard_Product_Tiles_Name_Link = By.CssSelector("div[class='products']>div>div * div[class='name']>a");

        public static By MyAnalog_Dashboard_Product_Tiles_Description_Text = By.CssSelector("div[class='products']>div>div * div[class='description']");

        public static By MyAnalog_Dashboard_SeeAlternativeParts_Links = By.CssSelector("div[class='products'] * button[class='ma btn btn-link']>span");

        public static By MyAnalog_Dashboard_Expanded_AlternativeParts_Section = By.CssSelector("div[class='products'] * div[class='alternative parts open");

        public static By MyAnalog_Dashboard_Expanded_AlternativeParts_Section_Part_Links = By.CssSelector("div[class='products'] * div[class='alternative parts open']>ul>li>a");

        public static By MyAnalog_Dashboard_Collapse_AlternativeParts_Section_Button = By.CssSelector("div[class='products'] * button[class='ma btn btn-link']>i[class$='up']");

        public static By MyAnalog_Dashboard_ProductImage_PopUp = By.CssSelector("div[class$='in']>div[class='modal-dialog']>div[class='modal-content']");

        public static By MyAnalog_Dashboard_ProductImage_PopUp_X_Button = By.CssSelector("div[class='modal-content'] * button[class*='close']");

        public static By MyAnalog_Dashboard_ProductImage_PopUp_Title = By.CssSelector("div[class$='in']>div[class='modal-dialog']>div[class='modal-content'] * h4");

        public static By MyAnalog_Dashboard_ProductImage_PopUp_Download_Button = By.CssSelector("a[class='downloadimagelink']>span");

        public static By MyAnalog_Dashboard_ProductImage_PopUp_Print_Button = By.CssSelector("a[class*='print']>span");

        //--- Special Resources Section ---//

        public static By MyAnalog_Dashboard_SpecialResources_Label = By.CssSelector("section[class='gated content widget']>div[class='section header']");

        public static By MyAnalog_Dashboard_SpecialResources_Links = By.CssSelector("ul[class='two column list']>li");

        public static By MyAnalog_Dashboard_SeeAllYourSpecialResources_Link = By.CssSelector("a[class='see all']");

        //--- Related Design Tools Section ---//

        public static By MyAnalog_Dashboard_SeeAll_SeeLess_Button = By.CssSelector("li[class='show more']>button");

        public static By MyAnalog_Dashboard_Expanded_RelatedDesignTools_Section = By.CssSelector("ul[class='tools expanded']>li");

        //--- Related Content Section ---//

        public static By MyAnalog_Dashboard_RelatedContent_Tabs = By.CssSelector("ul[class='nav nav-tabs']>li");

        public static By MyAnalog_Dashboard_RelatedContent_Selected_Tab = By.CssSelector("ul[class='nav nav-tabs']>li[class='active']");

        public static By MyAnalog_Dashboard_All_Tab_Tiles = By.CssSelector("div[class='masonry']>div[class*='tile']");

        public static By MyAnalog_Dashboard_NewProducts_Tiles = By.CssSelector("div[class='masonry']>div[class^='tile newProducts']");

        public static By MyAnalog_Dashboard_NewProducts_Tiles_NewProducts_Labels = By.CssSelector("div[class^='tile newProducts'] * span[class='heading']");

        public static By MyAnalog_Dashboard_NewProducts_Tiles_OtherOption_Icons = By.CssSelector("div[class^='tile newProducts'] * span[class^='option'] * img");

        public static By MyAnalog_Dashboard_NewProducts_Tiles_Thumbnail_Images = By.CssSelector("div[class^='tile newProducts'] * div[class='thumbnail-container'] * img");

        public static By MyAnalog_Dashboard_NewProducts_Tiles_ProductNumber_Links = By.CssSelector("div[class^='tile newProducts'] * div[class='title']>a");

        public static By MyAnalog_Dashboard_1st_NewProducts_Tile_Tag_Links = By.CssSelector("div[class^='tile newProducts']:nth-of-type(1) * div[class='tags']>a");

        public static By MyAnalog_Dashboard_Videos_Tiles = By.CssSelector("div[class='masonry']>div[class^='tile videos']");

        public static By MyAnalog_Dashboard_Videos_Tiles_Videos_Labels = By.CssSelector("div[class^='tile videos'] * span[class='heading']");

        public static By MyAnalog_Dashboard_Videos_Tiles_Thumbnail_Images = By.CssSelector("div[class^='tile videos'] * div[class='thumbnail-container'] * img");

        public static By MyAnalog_Dashboard_Videos_Tiles_Title_Links = By.CssSelector("div[class^='tile videos'] * div[class='title']>a");

        public static By MyAnalog_Dashboard_1st_Videos_Tile_Tag_Links = By.CssSelector("div[class^='tile videos']:nth-of-type(1) * div[class='tags']>a");

        public static By MyAnalog_Dashboard_TechnicalArticles_Tiles = By.CssSelector("div[class='masonry']>div[class^='tile technicalArticles']");

        public static By MyAnalog_Dashboard_TechnicalArticles_Tiles_TechnicalArticles_Labels = By.CssSelector("div[class^='tile technicalArticles'] * span[class='heading']");

        public static By MyAnalog_Dashboard_TechnicalArticles_Tiles_OtherOption_Icons = By.CssSelector("div[class^='tile technicalArticles'] * span[class^='option'] * img");

        public static By MyAnalog_Dashboard_TechnicalArticles_Tiles_Save_Buttons = By.CssSelector("div[class^='tile technicalArticles'] * menu[class='options dropdown']>button:nth-of-type(1)");

        public static By MyAnalog_Dashboard_TechnicalArticles_Tiles_CopyLink_Buttons = By.CssSelector("div[class^='tile technicalArticles'] * menu[class='options dropdown']>button:nth-of-type(2)");

        public static By MyAnalog_Dashboard_TechnicalArticles_Tiles_Title_Links = By.CssSelector("div[class^='tile technicalArticles'] * div[class='title']>a");

        public static By MyAnalog_Dashboard_1st_TechnicalArticles_Tile_Tag_Links = By.CssSelector("div[class^='tile technicalArticles']:nth-of-type(1) * div[class='tags']>a");

        public static By MyAnalog_Dashboard_1st_TechnicalArticles_Tile_ArrowDown_Button = By.CssSelector("div[class^='tile technicalArticles']:nth-of-type(1) * div>i[class$='down']");

        public static By MyAnalog_Dashboard_AnalogDialogue_Tiles = By.CssSelector("div[class='masonry']>div[class^='tile analogDialogue']");

        public static By MyAnalog_Dashboard_AnalogDialogue_Tiles_AnalogDialogue_Labels = By.CssSelector("div[class^='tile analogDialogue'] * span[class='heading']");

        public static By MyAnalog_Dashboard_AnalogDialogue_Tiles_OtherOption_Icons = By.CssSelector("div[class^='tile analogDialogue'] * span[class^='option'] * img");

        public static By MyAnalog_Dashboard_AnalogDialogue_Tiles_Save_Buttons = By.CssSelector("div[class^='tile analogDialogue'] * menu[class='options dropdown']>button:nth-of-type(1)");

        public static By MyAnalog_Dashboard_AnalogDialogue_Tiles_CopyLink_Buttons = By.CssSelector("div[class^='tile analogDialogue'] * menu[class='options dropdown']>button:nth-of-type(2)");

        public static By MyAnalog_Dashboard_AnalogDialogue_Tiles_Thumbnail_Images = By.CssSelector("div[class^='tile analogDialogue'] * div[class='thumbnail-container'] * img");

        public static By MyAnalog_Dashboard_AnalogDialogue_Tiles_Title_Links = By.CssSelector("div[class^='tile analogDialogue'] * div[class='title']>a");

        public static By MyAnalog_Dashboard_1st_AnalogDialogue_Tile_Tag_Links = By.CssSelector("div[class^='tile analogDialogue']:nth-of-type(1) * div[class='tags']>a");

        public static By MyAnalog_Dashboard_1st_AnalogDialogue_Tile_ArrowDown_Button = By.CssSelector("div[class^='tile analogDialogue']:nth-of-type(1) * div>i[class$='down']");

        public static By MyAnalog_Dashboard_1st_AnalogDialogue_Tile_ArrowUp_Button = By.CssSelector("div[class^='tile analogDialogue']:nth-of-type(1) * div>i[class$='up']");

        public static By MyAnalog_Dashboard_Webcasts_Tiles = By.CssSelector("div[class='masonry']>div[class^='tile webcast']");

        public static By MyAnalog_Dashboard_Webcasts_Tiles_OtherOption_Icons = By.CssSelector("div[class^='tile webcast'] * span[class^='option'] * img");

        public static By MyAnalog_Dashboard_Webcasts_Tiles_Save_Buttons = By.CssSelector("div[class^='tile webcast'] * menu[class='options dropdown']>button:nth-of-type(1)");

        public static By MyAnalog_Dashboard_Webcasts_Tiles_CopyLink_Buttons = By.CssSelector("div[class^='tile webcast'] * menu[class='options dropdown']>button:nth-of-type(2)");

        public static By MyAnalog_Dashboard_Webcasts_Tiles_Title_Links = By.CssSelector("div[class^='tile webcast'] * div[class='title']>a");

        public static By MyAnalog_Dashboard_1st_Webcasts_Tile_Tag_Links = By.CssSelector("div[class^='tile webcast']:nth-of-type(1) * div[class='tags']>a");

        public static By MyAnalog_Dashboard_1st_Webcasts_Tile_ArrowDown_Button = By.CssSelector("div[class^='tile webcast']:nth-of-type(1) * div>i[class$='down']");

        public static By MyAnalog_Dashboard_1st_Webcasts_Tile_ArrowUp_Button = By.CssSelector("div[class^='tile webcast']:nth-of-type(1) * div>i[class$='up']");

        public static By MyAnalog_Dashboard_PressReleases_Tiles = By.CssSelector("div[class='masonry']>div[class^='tile pressRelease']");

        public static By MyAnalog_Dashboard_PressReleases_Tiles_OtherOption_Icons = By.CssSelector("div[class^='tile pressRelease'] * span[class^='option'] * img");

        public static By MyAnalog_Dashboard_PressReleases_Tiles_Save_Buttons = By.CssSelector("div[class^='tile pressRelease'] * menu[class='options dropdown']>button:nth-of-type(1)");

        public static By MyAnalog_Dashboard_PressReleases_Tiles_CopyLink_Buttons = By.CssSelector("div[class^='tile pressRelease'] * menu[class='options dropdown']>button:nth-of-type(2)");

        public static By MyAnalog_Dashboard_PressReleases_Tiles_Title_Links = By.CssSelector("div[class^='tile pressRelease'] * div[class='title']>a");

        public static By MyAnalog_Dashboard_1st_PressReleasesTile_Tag_Links = By.CssSelector("div[class^='tile pressRelease']:nth-of-type(1) * div[class='tags']>a");

        public static By MyAnalog_Dashboard_1st_PressReleases_Tile_ArrowDown_Button = By.CssSelector("div[class^='tile pressRelease']:nth-of-type(1) * div>i[class$='down']");

        public static By MyAnalog_Dashboard_1st_PressReleases_Tile_ArrowUp_Button = By.CssSelector("div[class^='tile pressRelease']:nth-of-type(1) * div>i[class$='up']");

        public static By MyAnalog_Dashboard_CustomerCaseStudies_Tiles = By.CssSelector("div[class='masonry']>div[class^='tile customerCase']");

        public static By MyAnalog_Dashboard_CustomerCaseStudies_Tiles_CustomerCaseStudies_Labels = By.CssSelector("div[class^='tile customerCase'] * span[class='heading']");

        public static By MyAnalog_Dashboard_CustomerCaseStudies_Tiles_OtherOption_Icons = By.CssSelector("div[class^='tile customerCase'] * span[class^='option'] * img");

        public static By MyAnalog_Dashboard_CustomerCaseStudies_Tiles_Save_Buttons = By.CssSelector("div[class^='tile customerCase'] * menu[class='options dropdown']>button:nth-of-type(1)");

        public static By MyAnalog_Dashboard_CustomerCaseStudies_Tiles_CopyLink_Buttons = By.CssSelector("div[class^='tile customerCase'] * menu[class='options dropdown']>button:nth-of-type(2)");

        public static By MyAnalog_Dashboard_CustomerCaseStudies_Tiles_Title_Links = By.CssSelector("div[class^='tile customerCase'] * div[class='title']>a");

        public static By MyAnalog_Dashboard_1st_CustomerCaseStudies_Tile_Tag_Links = By.CssSelector("div[class^='tile customerCase']:nth-of-type(1) * div[class='tags']>a");

        public static By MyAnalog_Dashboard_SolutionsBulletinsAndBrochures_Tiles = By.CssSelector("div[class='masonry']>div[class^='tile solutionsBulletinsBrochures']");

        public static By MyAnalog_Dashboard_SolutionsBulletinsAndBrochures_Tiles_SolutionsBulletinsAndBrochures_Labels = By.CssSelector("div[class^='tile solutionsBulletinsBrochures'] * span[class='heading']");

        public static By MyAnalog_Dashboard_SolutionsBulletinsAndBrochures_Tiles_OtherOption_Icons = By.CssSelector("div[class^='tile solutionsBulletinsBrochures'] * span[class^='option'] * img");

        public static By MyAnalog_Dashboard_SolutionsBulletinsAndBrochures_Tiles_Save_Buttons = By.CssSelector("div[class^='tile solutionsBulletinsBrochures'] * menu[class='options dropdown']>button:nth-of-type(1)");

        public static By MyAnalog_Dashboard_SolutionsBulletinsAndBrochures_Tiles_CopyLink_Buttons = By.CssSelector("div[class^='tile solutionsBulletinsBrochures'] * menu[class='options dropdown']>button:nth-of-type(2)");

        public static By MyAnalog_Dashboard_SolutionsBulletinsAndBrochures_Tiles_Title_Links = By.CssSelector("div[class^='tile solutionsBulletinsBrochures'] * div[class='title']>a");

        public static By MyAnalog_Dashboard_1st_SolutionsBulletinsAndBrochures_Tile_Tag_Links = By.CssSelector("div[class^='tile solutionsBulletinsBrochures']:nth-of-type(1) * div[class='tags']>a");

        public static By MyAnalog_Dashboard_WhitePapers_Tiles = By.CssSelector("div[class='masonry']>div[class^='tile whitePapers']");

        public static By MyAnalog_Dashboard_WhitePapers_Tiles_WhitePapers_Labels = By.CssSelector("div[class^='tile whitePapers'] * span[class='heading']");

        public static By MyAnalog_Dashboard_WhitePapers_Tiles_OtherOption_Icons = By.CssSelector("div[class^='tile whitePapers'] * span[class^='option'] * img");

        public static By MyAnalog_Dashboard_WhitePapers_Tiles_Save_Buttons = By.CssSelector("div[class^='tile whitePapers'] * menu[class='options dropdown']>button:nth-of-type(1)");

        public static By MyAnalog_Dashboard_WhitePapers_Tiles_CopyLink_Buttons = By.CssSelector("div[class^='tile whitePapers'] * menu[class='options dropdown']>button:nth-of-type(2)");

        public static By MyAnalog_Dashboard_WhitePapers_Tiles_Title_Links = By.CssSelector("div[class^='tile whitePapers'] * div[class='title']>a");

        public static By MyAnalog_Dashboard_1st_WhitePapers_Tile_Tag_Links = By.CssSelector("div[class^='tile whitePapers']:nth-of-type(1) * div[class='tags']>a");

        public static By MyAnalog_Dashboard_ShowMore_Button = By.CssSelector("div[class='showMoreTiles']>button");

        //--- EngineerZone Section ---//

        public static By MyAnalog_Dashboard_EngineerZone_Username_Value = By.CssSelector("aside[class='engineer-zone widget'] * span[class='username']");

        public static By MyAnalog_Dashboard_EngineerZone_Username_InputBox = By.CssSelector("aside[class='engineer-zone widget'] * input[name='username']");

        public static By MyAnalog_Dashboard_CheckAvailability_Button = By.CssSelector("aside[class='engineer-zone widget'] * button[type='button']");

        public static By MyAnalog_Dashboard_Availability_X_Icon = By.CssSelector("aside[class='engineer-zone widget'] * img[class='availability']");

        public static By MyAnalog_Dashboard_GoToEngineerZone_Link = By.CssSelector("a[class='go to engineer-zone']");

        public static By MyAnalog_Dashboard_RelatedSupportForums_Section = By.CssSelector("div[class='related support forums']");

        public static By MyAnalog_Dashboard_RelatedSupportForums_Section_RelatedForums = By.CssSelector("div[class='forum']");

        public static By MyAnalog_Dashboard_RelatedSupportForums_Section_RelatedForums_Counter = By.CssSelector("div[class='forum'] * span[class='badge']");

        //----- Account Settings -----//

        public static By MyAnalog_AccountSettings_Label = By.CssSelector("section[class='account settings'] * h2");

        //--- Consent Section ---//

        public static By MyAnalog_AccountSettings_Consent_Section_ConsentEmail_Checkboxes = By.CssSelector("input[name='consent-email']");

        public static By MyAnalog_AccountSettings_Consent_Section_ConsentPhone_Checkboxes = By.CssSelector("input[name='consent-phone']");

        public static By MyAnalog_AccountSettings_Consent_Section_Submit_Button = By.CssSelector("section[class='account settings']>button[class^='ma btn btn-primary']");

        //--- Personal Section ---//

        public static By MyAnalog_AccountSettings_Personal_Section = By.CssSelector("div[class='personal fields']");

        public static By MyAnalog_AccountSettings_Personal_Section_Editing_Mode = By.CssSelector("form[class='edit setting']>div[class='personal fields']");

        public static By MyAnalog_AccountSettings_Personal_Section_Edit_Button = By.CssSelector("form[class='setting']:nth-of-type(1) * div[class='edit']>button");

        public static By MyAnalog_AccountSettings_Personal_Section_Save_Button = By.CssSelector("section[class='account settings']>form:nth-of-type(1) * button[type='submit']");

        public static By MyAnalog_AccountSettings_Personal_Section_Cancel_Button = By.CssSelector("section[class='account settings']>form:nth-of-type(1) * button[type='reset']");

        public static By MyAnalog_AccountSettings_Personal_Section_Required_FirstName_InputBox = By.CssSelector("input[name='first'][required]");

        public static By MyAnalog_AccountSettings_Personal_Section_FirstName_InputBox = By.CssSelector("input[name='first']");

        public static By MyAnalog_AccountSettings_Personal_Section_FirstName_InputBox_X_Button = By.CssSelector("div[class='personal fields']>div:nth-of-type(1)>img");

        public static By MyAnalog_AccountSettings_Personal_Section_FirstName_InputBox_Error = By.CssSelector("div[class='text input has value error required show title']>input[name='first']");

        public static By MyAnalog_AccountSettings_Personal_Section_Required_FirstPronounce_InputBox = By.CssSelector("input[name='firstPronounce'][required]");

        public static By MyAnalog_AccountSettings_Personal_Section_FirstPronounce_InputBox = By.CssSelector("input[name='firstPronounce']");

        public static By MyAnalog_AccountSettings_Personal_Section_Required_LastName_InputBox = By.CssSelector("input[name='last'][required]");

        public static By MyAnalog_AccountSettings_Personal_Section_LastName_InputBox = By.CssSelector("input[name='last']");

        public static By MyAnalog_AccountSettings_Personal_Section_LastName_InputBox_X_Button = By.CssSelector("div[class='personal fields']>div:nth-of-type(2)>img");

        public static By MyAnalog_AccountSettings_Personal_Section_LastName_InputBox_Error = By.CssSelector("div[class='text input has value error required show title']>input[name='last']");

        public static By MyAnalog_AccountSettings_Personal_Section_Required_LastPronounce_InputBox = By.CssSelector("input[name='lastPronounce'][required]");

        public static By MyAnalog_AccountSettings_Personal_Section_LastPronounce_InputBox = By.CssSelector("input[name='lastPronounce']");

        public static By MyAnalog_AccountSettings_Personal_Section_Required_Telephone_InputBox = By.CssSelector("input[name='phoneNumber'][required]");

        public static By MyAnalog_AccountSettings_Personal_Section_Telephone_InputBox = By.CssSelector("input[name='phoneNumber']");

        public static By MyAnalog_AccountSettings_Personal_Section_Telephone_InputBox_X_Button = By.CssSelector("div[class='personal fields']>div:nth-of-type(3)>img");

        public static By MyAnalog_AccountSettings_Personal_Section_Required_CompanyName_InputBox = By.CssSelector("input[name='companyName'][required]");

        public static By MyAnalog_AccountSettings_Personal_Section_CompanyName_InputBox = By.CssSelector("input[name='companyName']");

        public static By MyAnalog_AccountSettings_Personal_Section_CompanyName_InputBox_X_Button = By.CssSelector("div[class='personal fields']>div:nth-of-type(4)>img");

        public static By MyAnalog_AccountSettings_Personal_Section_CompanyName_InputBox_Error = By.CssSelector("div[class='text input has value error required show title']>input[name='companyName']");

        public static By MyAnalog_AccountSettings_Personal_Section_Required_CompanyNamePronounce_InputBox = By.CssSelector("input[name='companyNamePronounce'][required]");

        public static By MyAnalog_AccountSettings_Personal_Section_CompanyNamePronounce_InputBox = By.CssSelector("input[name='companyNamePronounce']");

        public static By MyAnalog_AccountSettings_Personal_Section_MobilePhone_Label = By.CssSelector("div[class='personal fields']>div:nth-of-type(5)>label");

        public static By MyAnalog_AccountSettings_Personal_Section_MobilePhone_InputBox = By.CssSelector("input[name='mobileNumber']");

        public static By MyAnalog_AccountSettings_Personal_Section_MobilePhone_InputBox_X_Button = By.CssSelector("div[class='personal fields']>div:nth-of-type(5)>img");

        public static By MyAnalog_AccountSettings_Personal_Section_Required_Division_InputBox = By.CssSelector("input[name='division'][required]");

        public static By MyAnalog_AccountSettings_Personal_Section_Division_InputBox = By.CssSelector("input[name='division']");

        //--- Masthead ---//

        public static By MyAnalog_Masthead_Category_DD = By.CssSelector("div[class='col-md-4 categories and markets']>aside:nth-child(1)>button");

        public static By MyAnalog_Masthead_Category_Menu_List = By.CssSelector("section[class='interests__menu__items__options'] ul");

        public static By MyAnalog_Masthead_Category_Saved = By.CssSelector("section[class='interests__menu__items__saved']");

        public static By MyAnalog_Masthead_Category_Count = By.CssSelector("div[class='col-md-4 categories and markets']>aside:nth-child(1) mark[class='count']");

        public static By MyAnalog_Masthead_Category_Search = By.CssSelector("section[class='interests__menu__items__options'] div[class*='search'] input");

        public static By MyAnalog_Masthead_Category_Saved_Button = By.CssSelector("button[class*='ma btn btn-primary btn-sm']");

        public static By MyAnalog_Masthead_Category_Cancel_Button = By.CssSelector("button[class='ma btn btn-link btn-sm']");

        public static By MyAnalog_Masthead_Market_DD = By.CssSelector("div[class='col-md-4 categories and markets']>aside:nth-child(2)>button");

        public static By MyAnalog_Masthead_Market_Count = By.CssSelector("div[class='col-md-4 categories and markets']>aside:nth-child(2) mark[class='count']");

        //--- Password Section ---//

        public static By MyAnalog_AccountSettings_Password_Section = By.CssSelector("section[class='change password']");

        public static By MyAnalog_AccountSettings_Password_Section_Password_InputBox = By.CssSelector("input[type='password']");

        public static By MyAnalog_AccountSettings_Password_Section_ResetPassword_Button = By.CssSelector("section[class='change password'] * button");

        //--- Email Section ---//

        public static By MyAnalog_AccountSettings_Email_Section_Edit_Button = By.CssSelector("form[class='email setting'] * div[class='edit']>button");

        public static By MyAnalog_AccountSettings_Email_Section_Editing_Mode = By.CssSelector("form[class='email edit setting']");

        public static By MyAnalog_AccountSettings_Email_Section_Cancel_Button = By.CssSelector("form[class='email edit setting'] * button[type='reset']");

        public static By MyAnalog_AccountSettings_Email_Section_Error_Message = By.CssSelector("form[class^='email'] span[class='ma alert fade in alert-danger']");

        public static By MyAnalog_AccountSettings_Email_Section_CurrentEmail_InputBox = By.CssSelector("div[class='email fields'] div:nth-of-type(1)>input");

        public static By MyAnalog_AccountSettings_Email_Section_EnterNewEmail_InputBox = By.CssSelector("input[name='newEmail']");

        public static By MyAnalog_AccountSettings_Email_Section_EnterNewEmail_InputBox_Warning_Message = By.CssSelector("span[class$='alert-warning']");

        public static By MyAnalog_AccountSettings_Email_Section_ConfirmNewEmail_InputBox = By.CssSelector("input[name='confirmNewEmail']");

        public static By MyAnalog_AccountSettings_Email_Section_SendVerificationCode_Button = By.CssSelector("div[class^='email'] * button[class='ma btn btn-primary']");

        public static By MyAnalog_AccountSettings_Email_Section_EnterVerificationCode_InputBox = By.CssSelector("div[class='text input show title']>input");

        public static By MyAnalog_AccountSettings_Email_Section_Submit_Button = By.CssSelector("div[class^='email'] * div:nth-last-child(1)>button[class='ma btn btn-primary']");

        //--- Occupation Section ---//

        public static By MyAnalog_AccountSettings_Occupation_Section = By.CssSelector("div[class='occupation fields']");

        public static By MyAnalog_AccountSettings_Occupation_Section_Edit_Button = By.CssSelector("form[class='setting']:nth-of-type(3) * div[class='edit']>button");

        public static By MyAnalog_AccountSettings_Occupation_Section_Editing_Mode = By.CssSelector("form[class='edit setting']>div[class='occupation fields']");

        public static By MyAnalog_AccountSettings_Occupation_Section_Save_Button = By.CssSelector("section[class='account settings']>form:nth-of-type(3) * button[type='submit']");

        public static By MyAnalog_AccountSettings_Occupation_Section_Cancel_Button = By.CssSelector("section[class='account settings']>form:nth-of-type(3) * button[type='reset']");

        public static By MyAnalog_AccountSettings_Occupation_Section_Required_WhatsYourOccupation_Dropdown = By.CssSelector("div[class='occupation fields']>div[class*='required']:nth-of-type(1)>button");

        public static By MyAnalog_AccountSettings_Occupation_Section_WhatsYourOccupation_Dropdown = By.CssSelector("div[class='occupation fields']>div:nth-of-type(1)>button");

        public static By MyAnalog_AccountSettings_Occupation_Section_WhatsYourOccupation_Dropdown_Values = By.CssSelector("div[class='occupation fields']>div:nth-of-type(1)>div[class^='options']>div");

        public static By MyAnalog_AccountSettings_Occupation_Section_Required_WhatTypeOf_Dropdown = By.CssSelector("div[class='occupation fields']>div[class*='required']:nth-of-type(2)>button");

        public static By MyAnalog_AccountSettings_Occupation_Section_WhatTypeOf_Dropdown = By.CssSelector("div[class='occupation fields']>div:nth-of-type(2)>button");

        public static By MyAnalog_AccountSettings_Occupation_Section_WhatTypeOf_Dropdown_Values = By.CssSelector("div[class='occupation fields']>div:nth-of-type(2)>div[class^='options']>div");

        public static By MyAnalog_AccountSettings_Occupation_Section_Required_GraduationYear_Dropdown = By.CssSelector("div[class='graduation year']>div[class*='required']>button");

        public static By MyAnalog_AccountSettings_Occupation_Section_GraduationYear_Dropdown = By.CssSelector("div[class='graduation year'] * button");

        public static By MyAnalog_AccountSettings_Occupation_Section_GraduationYear_Dropdown_Values = By.CssSelector("div[class='graduation year'] * div[class^='options']>div");

        public static By MyAnalog_AccountSettings_Occupation_Section_Required_SchoolName_InputBox = By.CssSelector("input[name='schoolName'][required]");

        public static By MyAnalog_AccountSettings_Occupation_Section_SchoolName_InputBox = By.CssSelector("input[name='schoolName']");

        //--- Location Section ---//

        public static By MyAnalog_AccountSettings_Location_Section = By.CssSelector("div[class='location fields']");

        public static By MyAnalog_AccountSettings_Location_Section_Edit_Button = By.CssSelector("form[class='setting']:nth-of-type(4) * div[class='edit']>button");

        public static By MyAnalog_AccountSettings_Location_Section_Editing_Mode = By.CssSelector("form[class='edit setting']>div[class='location fields']");

        public static By MyAnalog_AccountSettings_Location_Section_Save_Button = By.CssSelector("section[class='account settings']>form:nth-of-type(4) * button[type='submit']");

        public static By MyAnalog_AccountSettings_Location_Section_Cancel_Button = By.CssSelector("section[class='account settings']>form:nth-of-type(4) * button[type='reset']");

        public static By MyAnalog_AccountSettings_Location_Section_Required_AddressLine1_InputBox = By.CssSelector("input[name='addressLine1'][required]");

        public static By MyAnalog_AccountSettings_Location_Section_AddressLine1_InputBox = By.CssSelector("input[name='addressLine1']");

        public static By MyAnalog_AccountSettings_Location_Section_AddressLine1_InputBox_X_Button = By.CssSelector("div[class='location fields']>div:nth-of-type(1)>img");

        public static By MyAnalog_AccountSettings_Location_Section_Required_AddressLine2_InputBox = By.CssSelector("input[name='addressLine2'][required]");

        public static By MyAnalog_AccountSettings_Location_Section_AddressLine2_InputBox = By.CssSelector("input[name='addressLine2']");

        public static By MyAnalog_AccountSettings_Location_Section_AddressLine2_InputBox_X_Button = By.CssSelector("div[class='location fields']>div:nth-of-type(2)>img");

        public static By MyAnalog_AccountSettings_Location_Section_Required_CountryRegion_Dropdown = By.CssSelector("div[class='location fields']>div[class*='required']:nth-of-type(3)>button");

        public static By MyAnalog_AccountSettings_Location_Section_CountryRegion_Dropdown = By.CssSelector("div[class='location fields']>div[class^='select']:nth-of-type(3)>button");

        public static By MyAnalog_AccountSettings_Location_Section_Required_State_Dropdown = By.CssSelector("div[class='location fields']>div[class*='required']:nth-of-type(4)>button");

        public static By MyAnalog_AccountSettings_Location_Section_State_Dropdown = By.CssSelector("div[class='location fields']>div[class^='select']:nth-of-type(4)>button");

        public static By MyAnalog_AccountSettings_Location_Section_State_Dropdown_Values = By.CssSelector("div[class='location fields']>div[class^='select']:nth-of-type(4)>div[class^='options']>div");

        public static By MyAnalog_AccountSettings_Location_Section_Required_City_InputBox = By.CssSelector("input[name='city'][required]");

        public static By MyAnalog_AccountSettings_Location_Section_City_InputBox = By.CssSelector("input[name='city']");

        public static By MyAnalog_AccountSettings_Location_Section_City_InputBox_X_Button = By.CssSelector("div[class='location fields']>div:nth-of-type(5)>img");

        public static By MyAnalog_AccountSettings_Location_Section_Required_Zip_InputBox = By.CssSelector("input[name='zip'][required]");

        public static By MyAnalog_AccountSettings_Location_Section_Zip_InputBox = By.CssSelector("input[name='zip']");

        public static By MyAnalog_AccountSettings_Location_Section_Zip_InputBox_X_Button = By.CssSelector("div[class='location fields']>div:nth-of-type(6)>img");

        //--- Your EngineerZone Username Section ---//

        public static By MyAnalog_AccountSettings_EngineerZone_Section = By.CssSelector("div[class^='engineer-zone']");

        public static By MyAnalog_AccountSettings_EngineerZone_Section_Edit_Button = By.CssSelector("div[class^='engineer-zone'] * div[class='edit']>button");

        public static By MyAnalog_AccountSettings_EngineerZone_Section_Editing_Mode = By.CssSelector("div[class^='engineer-zone']>form[class='edit setting']");

        public static By MyAnalog_AccountSettings_EngineerZone_Section_Save_Button = By.CssSelector("div[class^='engineer-zone'] * button[type='submit']");

        public static By MyAnalog_AccountSettings_EngineerZone_Section_ViewYourEngineerZoneProfile_Link = By.CssSelector("a[class='profile']");

        public static By MyAnalog_AccountSettings_EngineerZone_Section_Required_YourEngineerZoneUsername_InputBox = By.CssSelector("input[name='ezUserName'][required]");

        public static By MyAnalog_AccountSettings_EngineerZone_Section_YourEngineerZoneUsername_InputBox = By.CssSelector("input[name='ezUserName']");

        public static By MyAnalog_AccountSettings_EngineerZone_Section_YourEngineerZoneUsername_InputBox_X_Button = By.CssSelector("div[class^='engineer-zone'] * img");

        public static By MyAnalog_AccountSettings_EngineerZone_Section_YourEngineerZoneUsername_InputBox_InvalidInput_ErrorMessage = By.CssSelector("div[class^='engineer-zone'] * span[class$='alert-danger']");

        //--- Data Search and Account Removal Section ---//

        public static By MyAnalog_AccountSettings_DataSearchAndAccountRemovalSection_Section_Label = By.CssSelector("section[class='account settings']>h3");

        public static By MyAnalog_AccountSettings_DataSearchAndAccountRemovalSection_Section_Description_Text = By.CssSelector("section[class='account settings']>p");

        //----- Products -----//

        public static By MyAnalog_Products_Label = By.CssSelector("div[class='section header']>h2");

        public static By MyAnalog_Products_Description_Text = By.CssSelector("section[class='products']>p");

        public static By MyAnalog_Products_NoSavedProduct_Description_Text = By.CssSelector("section[class='no products']>p");

        public static By MyAnalog_Products_Parts_Tab = By.CssSelector("div[class='tabs']>button:nth-of-type(1)");

        public static By MyAnalog_Products_Selected_Parts_Tab = By.CssSelector("div[class='tabs']>button[class$='active']:nth-of-type(1)");

        public static By MyAnalog_Products_AddProductsToMyAnalog_InputBox = By.CssSelector("form[class='product search'] * input[name^=search]");

        public static By MyAnalog_Products_AddProductsToMyAnalog_AutoSuggest_List = By.CssSelector("form[class='product search']>ul>li");

        public static By MyAnalog_Products_AddProductsToMyAnalog_InvalidInput_ErrorMessage = By.CssSelector("form[class='product search']>div[class^='message']>span");

        public static By MyAnalog_Products_Saved_Products = By.CssSelector("section[class='products'] section");

        public static By MyAnalog_Products_Saved_Products_GenericName_Link = By.CssSelector("div[class='name']>a");

        public static By MyAnalog_Products_Saved_Products_GenericName_Arrow_Icon = By.CssSelector("div[class='name']>a>img");

        public static By MyAnalog_Products_Saved_Products_ShortDescription = By.CssSelector("div[class='description']");

        public static By MyAnalog_Products_Saved_Products_Status = By.CssSelector("div[class='recommendation']");

        public static By MyAnalog_Products_Saved_Products_ManagePcnPdn = By.CssSelector("div[class='updates']>span");

        public static By MyAnalog_Products_Saved_Products_Ellipsis_Button = By.CssSelector("section[class='products'] * div[class='controls']>button");

        public static By MyAnalog_Products_Saved_Products_CopyLink_Option = By.CssSelector("menu[class='options dropdown']>button:nth-of-type(1)");

        public static By MyAnalog_Products_Saved_Products_Remove_Option = By.CssSelector("menu[class='options dropdown']>button:nth-last-of-type(1)");

        public static By MyAnalog_Products_Remove_Confirmation_Modal = By.CssSelector("div[class='modal fade in']>div>div[class='modal-content']");

        public static By MyAnalog_Products_Remove_Confirmation_Modal_Remove_Button = By.CssSelector("div[class^='modal'] * button[class='ma btn btn-primary']");

        public static By MyAnalog_Products_ProductDetails_Model_Column_Header = By.CssSelector("section[class='product expanded'] * section[class='model updates']>header>div[class='name']");

        public static By MyAnalog_Products_ProductDetails_Model_Row_Value = By.CssSelector("section[class='product expanded'] * div[class='details'] * div[class='name']");

        public static By MyAnalog_Products_ProductDetails_Status_Column_Header = By.CssSelector("section[class='product expanded'] * section[class='model updates']>header>div[class='status']");

        public static By MyAnalog_Products_ProductDetails_Status_Row_Value = By.CssSelector("section[class='product expanded'] * div[class='details'] * div[class='status']");

        public static By MyAnalog_Products_ProductDetails_Updates_Column_Header = By.CssSelector("section[class='product expanded'] * section[class='model updates']>header>div[class='updates']");

        public static By MyAnalog_Products_ProductDetails_Updates_Row_Value = By.CssSelector("section[class='product expanded'] * div[class='details'] * div[class='updates']");

        public static By MyAnalog_Products_ProductDetails_Updates_Row_PcnPdn_Button = By.CssSelector("section[class='product expanded'] * div[class='details'] * div[class='updates']>button");

        public static By MyAnalog_Products_ProductDetails_PublicationDate_Column_Header = By.CssSelector("section[class='product expanded'] * section[class='model updates']>header>div[class='publication date']");

        public static By MyAnalog_Products_ProductDetails_PublicationDate_Row_Value = By.CssSelector("section[class='product expanded'] * div[class='details'] * div[class='publication date']");

        public static By MyAnalog_Products_ProductDetails_NotifyMe_Column_Header = By.CssSelector("section[class='product expanded'] * section[class='model updates']>header>div[class='subscribed']");

        public static By MyAnalog_Products_ProductDetails_NotifyMe_Row_Toggle_Button = By.CssSelector("section[class='product expanded'] * div[class='details'] * div[class='subscribed'] * label");

        public static By MyAnalog_Products_ProductDetails_ViewAllDocumentation_Link = By.CssSelector("section[class='product expanded'] * a[href$='documentation']");

        public static By MyAnalog_Products_ProductDetails_Datasheet_Link = By.CssSelector("section[class='product expanded'] * a[href$='datasheet']");

        public static By MyAnalog_Products_ProductDetails_RelatedTools_Section = By.CssSelector("section[class='product expanded']>div[class='details']>section[class='content list']");

        public static By MyAnalog_Products_ModelUpdateList_Type_Row_Value = By.CssSelector("section[class='product expanded'] * div[class='model update list'] * tbody>tr>td:nth-of-type(1)");

        public static By MyAnalog_Products_ModelUpdateList_NotificationNumber_Row_Value = By.CssSelector("section[class='product expanded'] * div[class='model update list'] * tbody>tr>td:nth-of-type(2)");

        public static By MyAnalog_Products_ModelUpdateList_Description_Row_Value = By.CssSelector("section[class='product expanded'] * div[class='model update list'] * tbody>tr>td:nth-of-type(3)");

        public static By MyAnalog_Products_ModelUpdateList_PublicationDate_Row_Value = By.CssSelector("section[class='product expanded'] * div[class='model update list'] * tbody>tr>td:nth-of-type(4)");

        public static By MyAnalog_Products_ModelUpdateList_Documents_Row_Value = By.CssSelector("section[class='product expanded'] * div[class='model update list'] * tbody>tr>td:nth-of-type(5) * a");

        public static By MyAnalog_Products_Pagination = By.CssSelector("div[class='pagination']");

        public static By MyAnalog_Products_Previous_Button = By.CssSelector("div[class='pagination']>button:nth-of-type(1)");

        public static By MyAnalog_Products_Last_Page_Button = By.CssSelector("div[class='pagination']>button:nth-last-of-type(2)");

        public static By MyAnalog_Products_EvaluationBoards_Tab = By.CssSelector("div[class='tabs']>button:nth-of-type(2)");

        public static By MyAnalog_Products_Selected_EvaluationBoards_Tab = By.CssSelector("div[class='tabs']>button[class$='active']:nth-of-type(2)");

        public static By MyAnalog_Products_RegisterAnEvaluationBoard_Button = By.CssSelector("div[class='showRegister']>button");

        public static By MyAnalog_Products_ProductNumber_InputBox = By.CssSelector("input[name='modelNumber']");

        public static By MyAnalog_Products_ProductNumber_SuggestionsList_Items = By.CssSelector("ul[class$='suggestions-list']>li");

        public static By MyAnalog_Products_RegisterYourEvaluationBoard_Button = By.CssSelector("div[class='registerBoard']>button[type='submit']");

        public static By MyAnalog_Products_RegisteredEvaluationBoards_Section = By.CssSelector("section[class='evalboard']");

        public static By MyAnalog_Products_ModelNumber_Column_Header = By.CssSelector("section[class='evalboard']>table * tr>th:nth-of-type(1)");

        public static By MyAnalog_Products_SerialNumber_Column_Header = By.CssSelector("section[class='evalboard']>table * tr>th:nth-of-type(2)");

        public static By MyAnalog_Products_ProductVersion_Column_Header = By.CssSelector("section[class='evalboard']>table * tr>th:nth-of-type(3)");

        public static By MyAnalog_Products_NotifyMe_Column_Header = By.CssSelector("section[class='evalboard']>table * tr>th:nth-of-type(4)");

        public static By MyAnalog_Products_RegisteredEvaluationBoards = By.CssSelector("section[class='evalboard']>table>tbody>tr");

        public static By MyAnalog_Products_ModelNumber_Links = By.CssSelector("section[class='evalboard']>table>tbody * a");

        public static By MyAnalog_Products_SerialNumber_Value_Labels = By.CssSelector("section[class='evalboard']>table>tbody * td:nth-of-type(2)>span");

        public static By MyAnalog_Products_SerialNumber_InputBox = By.CssSelector("section[class='evalboard']>table>tbody * td:nth-of-type(2) * input");

        public static By MyAnalog_Products_ProductVersion_InputBox = By.CssSelector("section[class='evalboard']>table>tbody * td:nth-of-type(3) * input");

        public static By MyAnalog_Products_ProductVersion_Value_Labels = By.CssSelector("section[class='evalboard']>table>tbody * td:nth-of-type(3)>span");

        public static By MyAnalog_Products_NotifyMe_Switches = By.CssSelector("section[class='evalboard']>table>tbody * div[class='switch']");

        public static By MyAnalog_Products_RegisteredEvaluationBoards_Ellipsis_Button = By.CssSelector("section[class='evalboard'] * td[class^='controls'] * div[class='menu show']>button");

        public static By MyAnalog_Products_RegisteredEvaluationBoards_Options_Dropdown_Menu = By.CssSelector("section[class='evalboard'] * td[class^='controls']>menu[class='options dropdown']");

        public static By MyAnalog_Products_RegisteredEvaluationBoards_EditSerialNumber_Option = By.CssSelector("menu[class='options dropdown']>button:nth-of-type(1)");

        public static By MyAnalog_Products_RegisteredEvaluationBoards_EditProductVersion_Option = By.CssSelector("menu[class='options dropdown']>button:nth-of-type(2)");

        public static By MyAnalog_Products_RegisteredEvaluationBoards_CopyLink_Option = By.CssSelector("menu[class='options dropdown']>button:nth-of-type(3)");

        public static By MyAnalog_Products_RegisteredEvaluationBoards_Remove_Option = By.CssSelector("menu[class='options dropdown']>button:nth-last-of-type(1)");

        public static By MyAnalog_Products_RegisteredEvaluationBoards_Remove_Confirmation_Modal_Remove_Button = By.CssSelector("div[class^='modal'] * button[class='ma btn btn-primary']");

        public static By MyAnalog_Products_RegisteredEvaluationBoards_Save_Button = By.CssSelector("div[class='edit show']>button:nth-of-type(1)");

        //----- Orders -----//

        public static By MyAnalog_Orders_Label = By.CssSelector("div[class='section header']>h2");

        //--- Orders Table ---//

        public static By MyAnalog_Orders_OrderNumber_Column_Header = By.CssSelector("table[class='my orders']>thead>tr>th:nth-of-type(1)");

        public static By MyAnalog_Orders_OrderNumber_Row_Link = By.CssSelector("table[class='my orders']>tbody>tr>td>a");

        public static By MyAnalog_Orders_OrderDate_Column_Header = By.CssSelector("table[class='my orders']>thead>tr>th:nth-of-type(2)");

        public static By MyAnalog_Orders_OrderDate_Row_Label = By.CssSelector("table[class='my orders']>tbody>tr>td[rowspan]:nth-last-of-type(1)");

        public static By MyAnalog_Orders_Model_Column_Header = By.CssSelector("table[class='my orders']>thead>tr>th:nth-of-type(3)");

        public static By MyAnalog_Orders_OrderType_Column_Header = By.CssSelector("table[class='my orders']>thead>tr>th:nth-of-type(4)");

        public static By MyAnalog_Orders_Quantity_Column_Header = By.CssSelector("table[class='my orders']>thead>tr>th:nth-of-type(5)");

        public static By MyAnalog_Orders_Quantity_Row_Label = By.CssSelector("table[class='my orders'] * tr[class='detail']>td:nth-of-type(3)");

        public static By MyAnalog_Orders_Status_Column_Header = By.CssSelector("table[class='my orders']>thead>tr>th:nth-of-type(6)");

        public static By MyAnalog_Orders_Status_1st_Row_Label = By.CssSelector("table[class='my orders']>tbody>tr:nth-child(2)>td:nth-last-child(1)");

        //----- Order Number/Details -----//

        public static By MyAnalog_Orders_OrderNumber_BackArrow_Icon = By.CssSelector("a[class='back']>i");

        public static By MyAnalog_Orders_OrderNumber_OrderNumber_Label = By.CssSelector("div[class^='section header']>h2");

        public static By MyAnalog_Orders_OrderNumber_CustomerService_Link = By.CssSelector("a[class='customer service']");

        public static By MyAnalog_Orders_OrderNumber_Disclaimer_Text = By.CssSelector("div[class='disclaimer']");

        //--- Order Information ---//

        public static By MyAnalog_Orders_OrderNumber_OrderStatus_Label = By.CssSelector("div[class$='status one']>div:nth-of-type(1)>strong");

        public static By MyAnalog_Orders_OrderNumber_OrderStatus_Value = By.CssSelector("div[class$='status one']>div:nth-of-type(1)>div");

        public static By MyAnalog_Orders_OrderNumber_OrderPlaced_Label = By.CssSelector("div[class$='status one']>div:nth-of-type(2)>strong");

        public static By MyAnalog_Orders_OrderNumber_OrderPlaced_Value = By.CssSelector("div[class$='status one']>div:nth-of-type(2)>div");

        public static By MyAnalog_Orders_OrderNumber_PoNumber_Label = By.CssSelector("div[class$='status one']>div:nth-of-type(3)>strong");

        public static By MyAnalog_Orders_OrderNumber_PoNumber_Value = By.CssSelector("div[class$='status one']>div:nth-of-type(3)>div");

        public static By MyAnalog_Orders_OrderNumber_DeliveryMethod_Label = By.CssSelector("div[class$='status one']>div:nth-of-type(4)>strong");

        public static By MyAnalog_Orders_OrderNumber_DeliveryMethod_Value = By.CssSelector("div[class$='status one']>div:nth-of-type(4)>div");

        public static By MyAnalog_Orders_OrderNumber_ShippingAddress_Label = By.CssSelector("div[class$='status two']>div:nth-of-type(1)>strong");

        public static By MyAnalog_Orders_OrderNumber_ShippingAddress_Value = By.CssSelector("div[class$='status two']>div:nth-of-type(1)>address");

        public static By MyAnalog_Orders_OrderNumber_BillingAddress_Label = By.CssSelector("div[class$='status two']>div:nth-of-type(2)>strong");

        public static By MyAnalog_Orders_OrderNumber_BillingAddress_Value = By.CssSelector("div[class$='status two']>div:nth-of-type(2)>address");

        public static By MyAnalog_Orders_OrderNumber_Email_Label = By.CssSelector("div[class$='status two']>div:nth-of-type(3)>strong");

        public static By MyAnalog_Orders_OrderNumber_Email_Value = By.CssSelector("div[class$='status two']>div:nth-of-type(3)>div");

        //--- Order Details ---//

        public static By MyAnalog_Orders_OrderNumber_Model_Label = By.CssSelector("table[class='details']>thead>tr>th:nth-of-type(1)");

        public static By MyAnalog_Orders_OrderNumber_Model_Value = By.CssSelector("table[class='details']>tbody>tr:nth-of-type(1)>td:nth-of-type(1)>div:nth-of-type(1)");

        public static By MyAnalog_Orders_OrderNumber_ShipsFrom_Value = By.CssSelector("table[class='details'] * div[class='ships from']");

        public static By MyAnalog_Orders_OrderNumber_Status_Label = By.CssSelector("table[class='details']>thead>tr>th:nth-of-type(2)");

        public static By MyAnalog_Orders_OrderNumber_Status_Value = By.CssSelector("table[class='details']>tbody>tr:nth-of-type(1)>td:nth-of-type(2)");

        public static By MyAnalog_Orders_OrderNumber_DeliveryNumber_Label = By.CssSelector("table[class='details']>thead>tr>th:nth-of-type(3)");

        public static By MyAnalog_Orders_OrderNumber_EstDeliveryDate_Label = By.CssSelector("table[class='details']>thead>tr>th:nth-of-type(4)");

        public static By MyAnalog_Orders_OrderNumber_EstDeliveryDate_Value = By.CssSelector("table[class='details']>tbody>tr:nth-of-type(1)>td:nth-of-type(3)");

        public static By MyAnalog_Orders_OrderNumber_Quantity_Label = By.CssSelector("table[class='details']>thead>tr>th:nth-of-type(5)");

        public static By MyAnalog_Orders_OrderNumber_Quantity_Value = By.CssSelector("table[class='details']>tbody>tr:nth-of-type(1)>td:nth-of-type(5)");

        public static By MyAnalog_Orders_OrderNumber_UnitPrice_Label = By.CssSelector("table[class='details']>thead>tr>th:nth-of-type(6)");

        public static By MyAnalog_Orders_OrderNumber_UnitPrice_Value = By.CssSelector("table[class='details']>tbody>tr:nth-of-type(1)>td:nth-of-type(6)");

        public static By MyAnalog_Orders_OrderNumber_TotalPrice_Label = By.CssSelector("table[class='details']>thead>tr>th:nth-of-type(7)");

        public static By MyAnalog_Orders_OrderNumber_TotalPrice_Value = By.CssSelector("table[class='details']>tbody>tr:nth-of-type(1)>td:nth-of-type(7)");

        public static By MyAnalog_Orders_OrderNumber_CancelOrder_Option = By.CssSelector("td[class='cancel order']");

        public static By MyAnalog_Orders_OrderNumber_CancelOrder_PopUp_Box = By.CssSelector("div[class='modal fade in'] * div[class='modal-content']");

        public static By MyAnalog_Orders_OrderNumber_CancelOrder_PopUp_Box_CancelOrder_Button = By.CssSelector("button[class='ma btn btn-primary']");

        //--- Cost Details ---//

        public static By MyAnalog_Orders_OrderNumber_Subtotal_Value = By.CssSelector("tr>td[class='cost details']:nth-of-type(3)>div:nth-of-type(1)");

        public static By MyAnalog_Orders_OrderNumber_Taxes_Value = By.CssSelector("tr>td[class='cost details']:nth-of-type(3)>div:nth-of-type(2)");

        public static By MyAnalog_Orders_OrderNumber_Shipping_Value = By.CssSelector("tr>td[class='cost details']:nth-of-type(3)>div:nth-of-type(3)");

        public static By MyAnalog_Orders_OrderNumber_Total_Value = By.CssSelector("td[class='charges']");

        //----- Projects -----//

        public static By MyAnalog_Projects_Label = By.CssSelector("div[class='section header']>h2");

        public static By MyAnalog_Projects_CreateANewProject_Button = By.CssSelector("div[class='create new project']>button");

        public static By MyAnalog_Projects_CreateANewProject_Modal = By.CssSelector("div[class='create project'] * div[class='modal-content']");

        public static By MyAnalog_Projects_CreateANewProject_Modal_Error_Message = By.CssSelector("div[class='create project'] * span[class='ma alert fade in alert-danger']");

        public static By MyAnalog_Projects_CreateANewProject_Modal_Header_Label = By.CssSelector("div[class='create project'] * div[class='modal-header']>span");

        public static By MyAnalog_Projects_CreateANewProject_Modal_X_Button = By.CssSelector("div[class='create project'] * button[class='modal-close close']");

        public static By MyAnalog_Projects_CreateANewProject_Modal_ProjectName_InputBox = By.CssSelector("div[class='create project'] * input[name='title']");

        public static By MyAnalog_Projects_CreateANewProject_Modal_ProjectName_InputBox_Error = By.CssSelector("div[class='create project'] * div[class*='error']>input[name='title']");

        public static By MyAnalog_Projects_CreateANewProject_Modal_ProjectDescription_InputBox = By.CssSelector("div[class='create project'] * textarea[name='description']");

        public static By MyAnalog_Projects_CreateANewProject_Modal_ProjectDescription_GhostText_Label = By.CssSelector("div[class='create project'] * textarea[name='description'][placeholder]");

        public static By MyAnalog_Projects_CreateANewProject_Modal_Invitees_InputBox = By.CssSelector("div[class='create project'] * input[name='recipients']");

        public static By MyAnalog_Projects_CreateANewProject_Modal_Invitees_GhostText_Label = By.CssSelector("div[class='create project'] * input[name='recipients'][placeholder]");

        public static By MyAnalog_Projects_CreateANewProject_Modal_Message_InputBox = By.CssSelector("div[class='create project'] * textarea[name='message']");

        public static By MyAnalog_Projects_CreateANewProject_Modal_Message_GhostText_Label = By.CssSelector("div[class='create project'] * textarea[name='message'][placeholder]");

        public static By MyAnalog_Projects_CreateANewProject_Modal_QuickMessage_Buttons = By.CssSelector("div[class='create project'] * div[class='buttons']>button");

        public static By MyAnalog_Projects_CreateANewProject_Modal_Cancel_Button = By.CssSelector("div[class='create project'] * div[class^='buttons submit']>button[class='ma btn']");

        public static By MyAnalog_Projects_CreateANewProject_Modal_Done_Button = By.CssSelector("div[class='create project'] * div[class^='buttons submit']>button[class^='ma btn btn-primary']");

        public static By MyAnalog_Projects_CreatedNewProject_Modal = By.CssSelector("div[class='create project'] * div[class='modal-content']");

        public static By MyAnalog_Projects_CreatedNewProject_Modal_Message_Label_Line1 = By.CssSelector("div[class='created new project']>div:nth-of-type(1)");

        public static By MyAnalog_Projects_CreatedNewProject_Modal_Message_Label_Line2 = By.CssSelector("div[class='created new project']>div:nth-of-type(2)");

        public static By MyAnalog_Projects_CreatedNewProject_Modal_Ok_Button = By.CssSelector("div[class='create project'] * div[class^='buttons submit']>button[class='ma btn']");

        public static By MyAnalog_Projects_CannotCreateProject_Modal = By.CssSelector("div[class='cannot create project'] * div[class='modal-content']");

        public static By MyAnalog_Projects_CannotCreateProject_Modal_Ok_Button = By.CssSelector("div[class='cannot create project'] * button[class='ma btn btn-primary']");

        public static By MyAnalog_Projects_NewInvites = By.CssSelector("article[class='new invites']");

        public static By MyAnalog_Projects_NewInvites_YouAreInvited_Label = By.CssSelector("div[class='you invites']");

        public static By MyAnalog_Projects_NewInvites_Project_Name_Label = By.CssSelector("article[class='new invites'] * div[class='title']>span");

        public static By MyAnalog_Projects_NewInvites_Project_Description_Label = By.CssSelector("article[class='new invites'] * div[class='details']>div:nth-of-type(2)");

        public static By MyAnalog_Projects_NewInvites_Accept_Button = By.CssSelector("article[class='new invites'] * div[class='accept']>button");

        public static By MyAnalog_Projects_NewInvites_Decline_Button = By.CssSelector("article[class='new invites'] * div[class='decline']>button");

        public static By MyAnalog_Projects_CreatingANewProject_Video = By.CssSelector("div[class^='no-projects-data'] * div[class='vjs-poster']");

        public static By MyAnalog_Projects_Created_Projects = By.CssSelector("section[class='projects']>article");

        public static By MyAnalog_Projects_Project_Name_Link = By.CssSelector("section[class='projects']>article * h2>a");

        public static By MyAnalog_Projects_First_Project_Name_Link = By.CssSelector("section[class='projects']>article[class='project page'] * h2>a");

        public static By MyAnalog_Projects_Last_Project_Name_Link = By.CssSelector("section[class='projects']>article[class='project page']:nth-last-of-type(1) * h2>a");

        public static By MyAnalog_Projects_CreatedBy_Name_Label = By.CssSelector("div[class='created by']>div:nth-of-type(1)");

        public static By MyAnalog_Projects_CreatedBy_DateAndTime_Label = By.CssSelector("div[class='created by']>div:nth-of-type(2)");

        public static By MyAnalog_Projects_ModifiedBy_Name_Label = By.CssSelector("div[class='modified by']>div:nth-of-type(1)");

        public static By MyAnalog_Projects_ModifiedBy_DateAndTime_Label = By.CssSelector("div[class='modified by']>div:nth-of-type(2)");

        public static By MyAnalog_Projects_Participants_Dropdown = By.CssSelector("div[class^='dropdown participants']>button");

        public static By MyAnalog_Projects_Participants_Dropdown_Menu = By.CssSelector("div[class^='dropdown participants']>ul[class='dropdown-menu']");

        public static By MyAnalog_Projects_Contributor_Avatars = By.CssSelector("div[class='contributor']>div");

        public static By MyAnalog_Projects_First_Project_Contributor_Avatars = By.CssSelector("section[class='projects']>article:nth-of-type(1) * div[class='contributor']>div");

        public static By MyAnalog_Projects_Invite_Button = By.CssSelector("div[class='invite']>button");

        public static By MyAnalog_Projects_Invite_Modal = By.CssSelector("div[class='create invite'] * div[class='modal-content']");

        public static By MyAnalog_Projects_Invite_Modal_Header_Label = By.CssSelector("div[class='create invite'] * div[class='modal-header']>span");

        public static By MyAnalog_Projects_Invite_Modal_X_Button = By.CssSelector("div[class='create invite'] * button[class^='modal-close']");

        public static By MyAnalog_Projects_Invite_Modal_Error_Message = By.CssSelector("div[class='create invite'] * span[class='ma alert fade in alert-danger']");

        public static By MyAnalog_Projects_Invite_Modal_Project_Name_Label = By.CssSelector("div[class='create invite'] * div[class='fields']>div:nth-of-type(1)>h4");

        public static By MyAnalog_Projects_Invite_Modal_Invitees_InputBox = By.CssSelector("div[class='create invite'] * input[name='invitee']");

        public static By MyAnalog_Projects_Invite_Modal_Invitees_Error_Message = By.CssSelector("div[class='create invite'] * span[class='email error']");

        public static By MyAnalog_Projects_Invite_Modal_Message_InputBox = By.CssSelector("div[class='create invite'] * textarea[name='message']");

        public static By MyAnalog_Projects_Invite_Modal_QuickMessage_Buttons = By.CssSelector("div[class='create invite'] * div[class='buttons']>button");

        public static By MyAnalog_Projects_Invite_Modal_Cancel_Button = By.CssSelector("div[class='create invite'] * div[class^='buttons submit']>button[type='button']");

        public static By MyAnalog_Projects_Invite_Modal_Invite_Button = By.CssSelector("div[class='create invite'] * button[type='submit']");

        public static By MyAnalog_Projects_CreatedNewInvite_Modal_ConfirmationMessage_Text = By.CssSelector("div[class^='created new invite']>label");

        public static By MyAnalog_Projects_CreatedNewInvite_Modal_Ok_Button = By.CssSelector("div[class^='created new invite']>button[class='ma btn']");

        public static By MyAnalog_Projects_Components = By.CssSelector("div[class='statistics']");

        public static By MyAnalog_Projects_Contributors = By.CssSelector("div[class^='blue statistic']");

        public static By MyAnalog_Projects_Contributors_Count_Label = By.CssSelector("div[class^='blue statistic']>span[class='count']");

        public static By MyAnalog_Projects_Products = By.CssSelector("div[class^='red statistic']");

        public static By MyAnalog_Projects_Resources = By.CssSelector("div[class^='yellow statistic']");

        public static By MyAnalog_Projects_Resources_Count_Label = By.CssSelector("div[class^='yellow statistic']>span[class='count']");

        public static By MyAnalog_Projects_Psts = By.CssSelector("div[class^='green statistic']");

        public static By MyAnalog_Projects_Notes = By.CssSelector("div[class^='purple statistic']");

        public static By MyAnalog_Projects_Notes_Count_Label = By.CssSelector("div[class^='purple statistic']>span[class='count']");

        public static By MyAnalog_Projects_ProjectDetails_Buttons = By.CssSelector("div[class='detail'] * button");

        public static By MyAnalog_Projects_Pagination = By.CssSelector("div[class='pagination']");

        public static By MyAnalog_Projects_Current_Page_Selected_Button = By.CssSelector("div[class='pagination']>button[class$='active']");

        public static By MyAnalog_Projects_Previous_Button = By.CssSelector("div[class='pagination']>button:nth-of-type(1)");

        public static By MyAnalog_Projects_Next_Button = By.CssSelector("div[class='pagination']>button:nth-last-of-type(1)");

        public static By MyAnalog_Projects_Last_Page_Button = By.CssSelector("div[class='pagination']>button:nth-last-of-type(2)");

        //----- Projects > Project Detail Page-----//

        public static By MyAnalog_ProjectDetailPage_BackArrow_Icon = By.CssSelector("a[class='back']>i");

        public static By MyAnalog_ProjectDetailPage_ProjectName_Label = By.CssSelector("div[class^='section header']>h2");

        public static By MyAnalog_ProjectDetailPage_EditProjectName_InputBox = By.CssSelector("div[class^='section header'] * input[name='title']");

        public static By MyAnalog_ProjectDetailPage_EditProjectName_Save_Button = By.CssSelector("div[class^='section header'] * button[type='submit']");

        public static By MyAnalog_ProjectDetailPage_Ellipsis_Button = By.CssSelector("div[class='controls options']>button");

        public static By MyAnalog_ProjectDetailPage_EditProjectName_Option = By.CssSelector("menu[class='options dropdown']>button:nth-last-of-type(4)");

        public static By MyAnalog_ProjectDetailPage_EditDescription_Option = By.CssSelector("menu[class='options dropdown']>button:nth-last-of-type(3)");

        public static By MyAnalog_ProjectDetailPage_LeaveProject_Option = By.CssSelector("menu[class='options dropdown']>button:nth-last-of-type(2)");

        public static By MyAnalog_ProjectDetailPage_LeaveProject_Modal_Message = By.CssSelector("div[class='leave project'] * div[class='modal-header']>div");

        public static By MyAnalog_ProjectDetailPage_LeaveProject_YesLeave_Button = By.CssSelector("div[class='leave project'] * button[class='ma btn btn-primary']");

        public static By MyAnalog_ProjectDetailPage_DeleteProject_Option = By.CssSelector("menu[class='options dropdown']>button:nth-of-type(4)");

        public static By MyAnalog_ProjectDetailPage_DeleteProject_Modal_WarningMessage = By.CssSelector("div[class='delete project'] * div[class='modal-header']>div");

        public static By MyAnalog_ProjectDetailPage_DeleteProject_Modal_Cancel_Button = By.CssSelector("div[class='delete project'] * button[class='ma btn btn-link']");

        public static By MyAnalog_ProjectDetailPage_DeleteProject_Modal_Remove_Button = By.CssSelector("div[class='delete project'] * button[class='ma btn btn-primary']");

        public static By MyAnalog_ProjectDetailPage_AddedBy_Name_Label = By.CssSelector("div[class='history']>div:nth-of-type(1)>div[class='user']");

        public static By MyAnalog_ProjectDetailPage_AddedBy_DateAndTime_Label = By.CssSelector("div[class='history']>div:nth-of-type(1)>div[class='date']");

        public static By MyAnalog_ProjectDetailPage_ModifiedBy_Name_Label = By.CssSelector("div[class='history']>div:nth-of-type(2)>div[class='user']");

        public static By MyAnalog_ProjectDetailPage_ModifiedBy_DateAndTime_Label = By.CssSelector("div[class='history']>div:nth-of-type(2)>div[class='date']");

        public static By MyAnalog_ProjectDetailPage_Participants_Count_Label = By.CssSelector("span[class='participant count']");

        public static By MyAnalog_ProjectDetailPage_Participants_Dropdown = By.CssSelector("div[class^='dropdown participants']>button");

        public static By MyAnalog_ProjectDetailPage_Participants_Dropdown_Options = By.CssSelector("div[class^='dropdown participants']>ul[class='dropdown-menu']>li");

        public static By MyAnalog_ProjectDetailPage_Participants_Dropdown_Owner_Label = By.CssSelector("div[class^='dropdown participants']>ul[class='dropdown-menu']>li>span");

        public static By MyAnalog_ProjectDetailPage_Invite_Button = By.CssSelector("div[class='participants']>button");

        public static By MyAnalog_ProjectDetailPage_ProjectDescription_Label = By.CssSelector("div[class='description']");

        public static By MyAnalog_ProjectDetailPage_EditDescription_InputBox = By.CssSelector("div[class='description'] * input[name='description']");

        public static By MyAnalog_ProjectDetailPage_EditDescription_Save_Button = By.CssSelector("div[class='description'] * button[type='submit']");

        public static By MyAnalog_ProjectDetailPage_ProjectProducts_Section = By.CssSelector("article[class='products']>div[class='setting']");

        public static By MyAnalog_ProjectDetailPage_ProjectProducts_Edit_Button = By.CssSelector("article[class='products'] * div[class='edit']>button");

        public static By MyAnalog_ProjectDetailPage_ProjectProducts_Save_Button = By.CssSelector("article[class='products'] * div[class='options']>button[type='submit']");

        public static By MyAnalog_ProjectDetailPage_ProjectProducts_Cancel_Button = By.CssSelector("article[class='products'] * div[class='options']>button[type='reset']");

        public static By MyAnalog_ProjectDetailPage_ProjectProducts_AddProductsToMyAnalog_InputBox = By.CssSelector("article[class='products'] * input[name^='search']");

        public static By MyAnalog_ProjectDetailPage_ProjectProducts_AddProductsToMyAnalog_Results = By.CssSelector("article[class='products'] * form[class='product search']>ul>li");

        public static By MyAnalog_ProjectDetailPage_ProjectProducts_AddProductsToMyAnalog_Message = By.CssSelector("article[class='products'] * div[class='message  ']>span");

        public static By MyAnalog_ProjectDetailPage_ProjectProducts_AddProductsToMyAnalog_Error_Message = By.CssSelector("article[class='products'] * div[class^='message error']>span");

        public static By MyAnalog_ProjectDetailPage_SavedProducts = By.CssSelector("article[class='products'] * ul[class='saved product list']>li>span");

        public static By MyAnalog_ProjectDetailPage_First_SavedProduct_Link = By.CssSelector("article[class='products'] * ul[class='saved product list']>li:nth-of-type(1) * a[class='id']");

        public static By MyAnalog_ProjectDetailPage_Last_SavedProduct_Link = By.CssSelector("article[class='products'] * ul[class='saved product list']>li:nth-last-of-type(1) * a[class='id']");

        public static By MyAnalog_ProjectDetailPage_SavedProducts_X_Icon = By.CssSelector("article[class='products'] * ul[class='saved product list'] * a[class='delete']");

        public static By MyAnalog_ProjectDetailPage_ProjectProducts_Previous_Button = By.CssSelector("article[class='products'] * div[class='pagination']>button[class^='ma btn btn-primary btn-sm']:nth-of-type(1)");

        public static By MyAnalog_ProjectDetailPage_ProjectProducts_Next_Button = By.CssSelector("article[class='products'] * div[class='pagination']>button[class^='ma btn btn-primary btn-sm']:nth-last-of-type(1)");

        public static By MyAnalog_ProjectDetailPage_ProjectResources_Section = By.CssSelector("article[class='resources']>div[class='setting']");

        public static By MyAnalog_ProjectDetailPage_ProjectResources_Label = By.CssSelector("article[class='resources'] * div[class^='section header']>h2");

        public static By MyAnalog_ProjectDetailPage_ProjectResources_Edit_Button = By.CssSelector("article[class='resources'] * div[class='edit']>button");

        public static By MyAnalog_ProjectDetailPage_ProjectResources_Save_Button = By.CssSelector("article[class='resources'] * div[class='options']>button[type='submit']");

        public static By MyAnalog_ProjectDetailPage_ProjectResources_Cancel_Button = By.CssSelector("article[class='resources'] * div[class='options']>button[type='reset']");

        public static By MyAnalog_ProjectDetailPage_SavedResources = By.CssSelector("article[class='resources'] * ul[class='saved resource list']>li>span");

        public static By MyAnalog_ProjectDetailPage_Last_SavedResource_Link = By.CssSelector("article[class='resources'] * ul[class='saved resource list']>li:nth-last-of-type(1) * a[class='id']");

        public static By MyAnalog_ProjectDetailPage_SavedResources_X_Icon = By.CssSelector("article[class='resources'] * ul[class='saved resource list'] * a[class='delete']");

        public static By MyAnalog_ProjectDetailPage_ProjectResources_Previous_Button = By.CssSelector("article[class='resources'] * div[class='pagination']>button[class^='ma btn btn-primary btn-sm']:nth-of-type(1)");

        public static By MyAnalog_ProjectDetailPage_ProjectResources_Next_Button = By.CssSelector("article[class='resources'] * div[class='pagination']>button[class^='ma btn btn-primary btn-sm']:nth-last-of-type(1)");

        public static By MyAnalog_ProjectDetailPage_ProjectResources_Resource_InputBox = By.CssSelector("article[class='resources'] * textarea[name='description']");

        public static By MyAnalog_ProjectDetailPage_ProjectResources_AddLink_Button = By.CssSelector("article[class='resources'] * div[class='buttons']>button");

        public static By MyAnalog_ProjectDetailPage_ProjectParametricSearches_Section = By.CssSelector("article[class='parametric searches']>div[class='setting']");

        public static By MyAnalog_ProjectDetailPage_ProjectParametricSearches_Section_EditMode = By.CssSelector("article[class='parametric searches']>div[class='edit setting']");

        public static By MyAnalog_ProjectDetailPage_ProjectParametricSearches_Label = By.CssSelector("article[class='parametric searches'] * div[class^='section header']>h2");

        public static By MyAnalog_ProjectDetailPage_ProjectParametricSearches_Edit_Button = By.CssSelector("article[class='parametric searches'] * div[class='edit']>button");

        public static By MyAnalog_ProjectDetailPage_ProjectParametricSearches_Save_Button = By.CssSelector("article[class='parametric searches'] * div[class='options']>button[type='submit']");

        public static By MyAnalog_ProjectDetailPage_ProjectParametricSearches_Cancel_Button = By.CssSelector("article[class='parametric searches'] * div[class='options']>button[type='reset']");

        public static By MyAnalog_ProjectDetailPage_SavedParametricSearches = By.CssSelector("article[class='parametric searches'] * ul[class='saved parametric list']>li>span");

        public static By MyAnalog_ProjectDetailPage_First_SavedParametricSearch_Link = By.CssSelector("article[class='parametric searches'] * ul[class='saved parametric list']>li:nth-of-type(1) * a[class='id']");

        public static By MyAnalog_ProjectDetailPage_SavedParametricSearches_X_Icon = By.CssSelector("article[class='parametric searches'] * ul[class='saved parametric list'] * a[class='delete']");

        public static By MyAnalog_ProjectDetailPage_ProjectNotes_Section = By.CssSelector("article[class='notes']>div[class='setting']");

        public static By MyAnalog_ProjectDetailPage_Last_SavedNote_Comment_Label = By.CssSelector("article[class='notes'] * ul[class='saved notes list']>li:nth-last-of-type(1) * div[class='description']");

        public static By MyAnalog_ProjectDetailPage_Last_SavedNote_Contributor_Label = By.CssSelector("article[class='notes'] * ul[class='saved notes list']>li:nth-last-of-type(1) * h4");

        public static By MyAnalog_ProjectDetailPage_ProjectNotes_Edit_Button = By.CssSelector("article[class='notes'] * div[class='edit']>button");

        public static By MyAnalog_ProjectDetailPage_ProjectNotes_Description_InputBox = By.CssSelector("article[class='notes'] * textarea[name='description']");

        public static By MyAnalog_ProjectDetailPage_ProjectNotes_AddNote_Button = By.CssSelector("article[class='notes'] * div[class='buttons']>button[type='submit']");

        //----- Resources -----//

        public static By MyAnalog_Resources_Label = By.CssSelector("div[class='section header']>h2");

        public static By MyAnalog_Resources_Description_Text = By.CssSelector("section[class='resources']>p[class='description']");

        public static By MyAnalog_Resources_Saved_Tab = By.CssSelector("div[class='tabs']>button:nth-of-type(1)");

        public static By MyAnalog_Resources_Selected_Saved_Tab = By.CssSelector("div[class='tabs']>button[class$='active']:nth-of-type(1)");

        public static By MyAnalog_Resources_Special_Tab = By.CssSelector("div[class='tabs']>button:nth-of-type(2)");

        public static By MyAnalog_Resources_Saved_Resources_Rows = By.CssSelector("table[class='editable table']>tbody>tr");

        public static By MyAnalog_Resources_Saved_Resources_1st_Row = By.CssSelector("table[class='editable table']>tbody>tr:nth-of-type(1)");

        public static By MyAnalog_Resources_Saved_Resources_1st_Row_Title_InputBox = By.CssSelector("table[class='editable table']>tbody>tr:nth-of-type(1) * input");

        public static By MyAnalog_Resources_Saved_Resources_1st_Row_Title_Value = By.CssSelector("table[class='editable table']>tbody>tr:nth-of-type(1)>td:nth-of-type(1)>span");

        public static By MyAnalog_Resources_Saved_Resources_1st_Row_Note_Value = By.CssSelector("table[class='editable table']>tbody>tr:nth-of-type(1)>td:nth-of-type(2)>span");

        public static By MyAnalog_Resources_Saved_Resources_1st_Row_Note_InputBox = By.CssSelector("table[class='editable table']>tbody>tr:nth-of-type(1) * input");

        public static By MyAnalog_Resources_Saved_Resources_DateAdded_Value = By.CssSelector("tr>td:nth-of-type(3)>span");

        public static By MyAnalog_Resources_Saved_Resources_Ellipsis_Button = By.CssSelector("div[class='menu show']>button");

        public static By MyAnalog_Resources_Saved_Resources_1st_Row_Ellipsis_Button = By.CssSelector("table[class='editable table']>tbody>tr:nth-of-type(1) * div[class='menu show']>button");

        public static By MyAnalog_Resources_Saved_Resources_EditTitle_Option = By.CssSelector("menu[class='options dropdown']>button:nth-of-type(1)");

        public static By MyAnalog_Resources_Saved_Resources_EditNote_Option = By.CssSelector("menu[class='options dropdown']>button:nth-of-type(2)");

        public static By MyAnalog_Resources_Saved_Resources_CopyLink_Option = By.CssSelector("menu[class='options dropdown']>button:nth-of-type(3)");

        public static By MyAnalog_Resources_Saved_Resources_Remove_Option = By.CssSelector("menu[class='options dropdown']>button:nth-of-type(4)");

        public static By MyAnalog_Resources_Remove_Confirmation_Modal = By.CssSelector("div[class='modal fade in']>div>div[class='modal-content']");

        public static By MyAnalog_Resources_Remove_Confirmation_Modal_Remove_Button = By.CssSelector("div[class^='modal'] * button[class='ma btn btn-primary']");

        public static By MyAnalog_Resources_Saved_Resources_Save_Button = By.CssSelector("div[class='edit show']>button:nth-of-type(1)");

        public static By MyAnalog_Resources_Saved_Resources_Cancel_Button = By.CssSelector("div[class='edit show']>button:nth-of-type(2)");

        public static By MyAnalog_Resources_Previous_Button = By.CssSelector("div[class='pagination']>button:nth-of-type(1)");

        public static By MyAnalog_Resources_Next_Button = By.CssSelector("div[class='pagination']>button:nth-last-of-type(1)");

        public static By MyAnalog_Resources_Last_Page_Button = By.CssSelector("div[class='pagination']>button:nth-last-of-type(2)");

        public static By MyAnalog_Resources_Current_Page_Selected = By.CssSelector("div[class='pagination']>button[class$='active']");

        //----- Parametric Searches -----//

        public static By MyAnalog_ParametricSearchess_Label = By.CssSelector("div[class='section header']>h2");

        public static By MyAnalog_ParametricSearches_Description_Text = By.CssSelector("section[class='parametric searches']>p");

        public static By MyAnalog_ParametricSearches_Saved_ParametricSearches_Rows = By.CssSelector("table[class='editable table']>tbody>tr");

        public static By MyAnalog_ParametricSearches_Saved_ParametricSearches_1st_Row = By.CssSelector("table[class='editable table']>tbody>tr:nth-of-type(1)");

        public static By MyAnalog_ParametricSearches_Saved_ParametricSearches_Title_Value = By.CssSelector("table[class='editable table']>tbody>tr>td:nth-of-type(1)>span");

        public static By MyAnalog_ParametricSearches_Saved_ParametricSearches_1st_Row_Title_Value = By.CssSelector("table[class='editable table']>tbody>tr:nth-of-type(1)>td:nth-of-type(1)>span");

        public static By MyAnalog_ParametricSearches_Saved_ParametricSearches_1st_Row_Title_InputBox = By.CssSelector("table[class='editable table']>tbody>tr:nth-of-type(1) * input");

        public static By MyAnalog_ParametricSearches_Saved_ParametricSearches_1st_Row_Note_Value = By.CssSelector("table[class='editable table']>tbody>tr:nth-of-type(1)>td:nth-of-type(2)>span");

        public static By MyAnalog_ParametricSearches_Saved_ParametricSearches_1st_Row_Note_InputBox = By.CssSelector("table[class='editable table']>tbody>tr:nth-of-type(1) * input");

        public static By MyAnalog_ParametricSearches_Saved_ParametricSearches_DateAdded_Value = By.CssSelector("tr>td:nth-of-type(3)>span");

        public static By MyAnalog_ParametricSearches_Saved_ParametricSearches_Ellipsis_Button = By.CssSelector("div[class='menu show']>button");

        public static By MyAnalog_ParametricSearches_Saved_ParametricSearches_1st_Row_Ellipsis_Button = By.CssSelector("table[class='editable table']>tbody>tr:nth-of-type(1) * div[class='menu show']>button");

        public static By MyAnalog_ParametricSearches_Saved_ParametricSearches_EditTitle_Option = By.CssSelector("menu[class='options dropdown']>button:nth-of-type(1)");

        public static By MyAnalog_ParametricSearches_Saved_ParametricSearches_EditNote_Option = By.CssSelector("menu[class='options dropdown']>button:nth-of-type(2)");

        public static By MyAnalog_ParametricSearches_Saved_ParametricSearches_CopyLink_Option = By.CssSelector("menu[class='options dropdown']>button:nth-of-type(3)");

        public static By MyAnalog_ParametricSearches_Saved_ParametricSearches_Remove_Option = By.CssSelector("menu[class='options dropdown']>button:nth-of-type(4)");

        public static By MyAnalog_ParametricSearches_Remove_Confirmation_Modal_Message = By.CssSelector("div[class='modal fade in']>div * div[class='modal-body']");

        public static By MyAnalog_ParametricSearches_Remove_Confirmation_Modal_Cancel_Button = By.CssSelector("div[class^='modal'] * button[class='ma btn']");

        public static By MyAnalog_ParametricSearches_Remove_Confirmation_Modal_Remove_Button = By.CssSelector("div[class^='modal'] * button[class='ma btn btn-primary']");

        public static By MyAnalog_ParametricSearches_Saved_ParametricSearches_Save_Button = By.CssSelector("div[class='edit show']>button:nth-of-type(1)");

        public static By MyAnalog_ParametricSearches_Saved_ParametricSearches_Cancel_Button = By.CssSelector("div[class='edit show']>button:nth-of-type(2)");

        public static By MyAnalog_ParametricSearches_Previous_Button = By.CssSelector("div[class='pagination']>button:nth-of-type(1)");

        public static By MyAnalog_ParametricSearches_Next_Button = By.CssSelector("div[class='pagination']>button:nth-last-of-type(1)");

        public static By MyAnalog_ParametricSearches_Last_Page_Button = By.CssSelector("div[class='pagination']>button:nth-last-of-type(2)");

        public static By MyAnalog_ParametricSearches_Current_Page_Selected = By.CssSelector("div[class='pagination']>button[class$='active']");

        //----- Subscriptions -----//

        public static By MyAnalog_Subscriptions_Label = By.CssSelector("div[class='section header']>h2");

        public static By MyAnalog_Subscriptions_Description_Text = By.CssSelector("div[class='description head']");

        public static By MyAnalog_Subscriptions_SaveChanges_Button = By.CssSelector("div[class='wrapper']>div>button:nth-last-of-type(1)");

        public static By MyAnalog_Subscriptions_Disclaimer_Text = By.CssSelector("p[class='consent disclaimer']");

        public static By MyAnalog_Subscriptions_Tiles = By.CssSelector("div[class='subcriptions masonry']>div");

        public static By MyAnalog_Subscriptions_Tiles_Title_Text = By.CssSelector("div[class='subcriptions masonry']>div * div[class='title']");

        public static By MyAnalog_Subscriptions_Tiles_Toggle_Button = By.CssSelector("div[class='subcriptions masonry']>div * div[class='switch']>label");

        public static By MyAnalog_Subscriptions_Tiles_Locale_Dropdown = By.CssSelector("div[class='subcriptions masonry']>div * div[class='language control'] * button");

        public static By MyAnalog_Subscriptions_Tiles_Locale_Dropdown_English_Value = By.CssSelector("div[class='subcriptions masonry']>div * div[class='language control'] * div[class^='options']>div[data-label='English']");

        public static By MyAnalog_Subscriptions_Tiles_Html_RadioButton = By.CssSelector("div[class='subcriptions masonry']>div * input[id*='html']");

        public static By MyAnalog_Subscriptions_Tiles_Text_RadioButton = By.CssSelector("div[class='subcriptions masonry']>div * input[id*='text']");

        public static By MyAnalog_Subscriptions_Tiles_Description_Text = By.CssSelector("div[class='subcriptions masonry']>div * div[class='description']");

        public static By MyAnalog_Subscriptions_Alert_Disclaimer_Text = By.CssSelector("div[class='subscriptions alert'] * div[class='disclaimer']");

        public static By MyAnalog_Subscriptions_Alert_LeavePage_Button = By.CssSelector("div[class='subscriptions alert'] * button[class='ma btn']");

        //----- Support Tickets -----//

        public static By MyAnalog_SupportTickets_Label = By.CssSelector("div[class='section header']>h2");

        public static By MyAnalog_SupportTickets_CreateANewSupportTicket_Button = By.CssSelector("div[class='create-support-ticket']>button");

        public static By MyAnalog_SupportTickets_CaseTitle_Column_Header = By.CssSelector("table[class='ma table']>thead>tr>th:nth-of-type(1)");

        public static By MyAnalog_SupportTickets_CaseNumber_Column_Header = By.CssSelector("table[class='ma table']>thead>tr>th:nth-of-type(2)");

        public static By MyAnalog_SupportTickets_CreatedOn_Column_Header = By.CssSelector("table[class='ma table']>thead>tr>th:nth-of-type(3)");

        public static By MyAnalog_SupportTickets_PrimaryProduct_Column_Header = By.CssSelector("table[class='ma table']>thead>tr>th:nth-of-type(4)");

        public static By MyAnalog_SupportTickets_Status_Column_Header = By.CssSelector("table[class='ma table']>thead>tr>th:nth-of-type(6)");

        public static By MyAnalog_SupportTickets_Created_SupportTickets = By.CssSelector("section[class='support-tickets'] * tbody>tr");

        public static By MyAnalog_SupportTickets_First_Created_SupportTickets_CaseTitle = By.CssSelector("section[class='support-tickets'] * tbody>tr:nth-of-type(1)>td[class='caseTitle']>a");

        public static By MyAnalog_SupportTickets_Pagination = By.CssSelector("div[class='pagination']");

        public static By MyAnalog_SupportTickets_Last_Page_Button = By.CssSelector("div[class='pagination']>button:nth-last-of-type(2)");

        //----- Support Tickets > Support Tickets Details -----//

        public static By MyAnalog_SupportTicketsDetails_BackArrow_Icon = By.CssSelector("a[class='back']>i");

        public static By MyAnalog_SupportTicketsDetails_CaseTitle_Label = By.CssSelector("div[class='section header back']>h2");

        public static By MyAnalog_SupportTicketsDetails_CreatedOn_Label = By.CssSelector("div[class$='history']>div:nth-of-type(1)>div[class='user']>span:nth-of-type(1)");

        public static By MyAnalog_SupportTicketsDetails_CreatedOn_Value = By.CssSelector("div[class$='history']>div:nth-of-type(1)>div[class='date']");

        public static By MyAnalog_SupportTicketsDetails_CaseNumber_Label = By.CssSelector("div[class$='history']>div:nth-of-type(2)>div[class='user']");

        public static By MyAnalog_SupportTicketsDetails_CaseNumber_Value = By.CssSelector("div[class$='history']>div:nth-of-type(2)>div[class='date']");

        public static By MyAnalog_SupportTicketsDetails_PrimaryProduct_Label = By.CssSelector("div[class^='yellow statistic']>span[class='title']");

        public static By MyAnalog_SupportTicketsDetails_PrimaryProduct_Value = By.CssSelector("div[class^='yellow statistic']>span[class^='value']");

        public static By MyAnalog_SupportTicketsDetails_Status_Label = By.CssSelector("div[class^='green statistic']>span[class='title']");

        public static By MyAnalog_SupportTicketsDetails_Status_Value = By.CssSelector("div[class^='green statistic']>span[class^='value']");

        public static By MyAnalog_SupportTicketsDetails_TicketDetails_Label = By.CssSelector("article[class^='support-ticket-details']>div[class='section header']>h2");
    }
}