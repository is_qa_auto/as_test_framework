﻿using OpenQA.Selenium;
using AS_Test_Framework.Util;
using AS_Test_Framework.Element;
using System.Collections.Generic;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Interactions;
using System;
using System.Linq;
using System.Threading;

namespace AS_Test_Framework.Action
{
    public class Actionkey
    {
        Configuration util = new Configuration();

        public void INavigate(IWebDriver driver, string url) {
            driver.Manage().Cookies.DeleteAllCookies();
            driver.Manage().Window.Maximize();

            if (Configuration.Environment.Equals("production"))
            {
                driver.Navigate().GoToUrl("https://analogb2c.b2clogin.com/analogb2c.onmicrosoft.com/oauth2/v2.0/logout?p=B2C_1A_ADI_SignUpOrSignInWithKmsi&post_logout_redirect_uri=" + url);
            }
            else {
                driver.Navigate().GoToUrl("https://analogb2c" + Configuration.Environment + ".b2clogin.com/analogb2c" + Configuration.Environment + ".onmicrosoft.com/oauth2/v2.0/logout?p=B2C_1A_ADI_SignUpOrSignInWithKmsi&post_logout_redirect_uri=" + url);
            }

            driver.Navigate().GoToUrl(url);
            GivenIAcceptCookies(driver);
        }

        public void Navigate(IWebDriver driver, string url)
        {
            driver.Manage().Cookies.DeleteAllCookies();
            driver.Manage().Window.Maximize();

            if (Configuration.Environment.Equals("production"))
            {
                driver.Navigate().GoToUrl("https://analogb2c.b2clogin.com/analogb2c.onmicrosoft.com/oauth2/v2.0/logout?p=B2C_1A_ADI_SignUpOrSignInWithKmsi&post_logout_redirect_uri=" + url);
            }
            else
            {
                driver.Navigate().GoToUrl("https://analogb2c" + Configuration.Environment + ".b2clogin.com/analogb2c" + Configuration.Environment + ".onmicrosoft.com/oauth2/v2.0/logout?p=B2C_1A_ADI_SignUpOrSignInWithKmsi&post_logout_redirect_uri=" + url);

                //--- Added Buffer for the PDP, DSP and Design Center Scripts ---//
                if (url.Contains("/ad") || url.Contains("/ltc") || url.Contains("/adswt-cces") || url.Contains("/design-center/"))
                {
                    Thread.Sleep(7000);
                }

                //--- Added Buffer for the Eval Board Scripts ---//
                if (url.Contains("/EVAL"))
                {
                    Thread.Sleep(2000);
                }

                //--- Added Buffer for the CFTL Scripts ---//
                if (url.Contains("/CN"))
                {
                    Thread.Sleep(1000);
                }
            }

            driver.Navigate().GoToUrl(url);
            Console.WriteLine("Go to: " + url);

            //--- Added Buffer for the PDP Scripts ---//
            if (url.Contains("/ad") || url.Contains("/ltc"))
            {
                Thread.Sleep(5000);
            }

            //--- Added Buffer for the Design Center Scripts ---//
            if (url.Contains("/design-center/"))
            {
                Thread.Sleep(4000);
            }

            //--- Added Buffer for the PST Scripts ---//
            if (url.Contains("/parametricsearch/"))
            {
                Thread.Sleep(7000);
            }

            GivenIAcceptCookies(driver);
        }

        public void GivenIAcceptCookies(IWebDriver driver)
        {
            if (util.CheckElement(driver, Elements.GDPR_Banner_Success_Btn, 5))
            {
                //Console.Write("\t");
                IClick(driver, Elements.GDPR_Banner_Success_Btn);
                Thread.Sleep(3000);
            }
        }

        //--- Handling for the "New privacy legislation requires that we save your consent" ---//
        public void IRejectCCPA(IWebDriver driver)
        {
            if (driver.Url.Contains("ccpa"))
            {
                //--- No, I would not like to receive communications from Analog Devices and authorized partners related ADIs products and services. - Radio Button ---//
                driver.FindElement(By.CssSelector("input[id='option-false-undefined']")).Click();

                //--- Save Settings and Continue - Button ---//
                driver.FindElement(By.CssSelector("button[class='ma btn btn-primary']")).Click();
            }
        }

        public void IClick(IWebDriver driver, By element)
        {
            if (util.CheckElement(driver, element, 5))
            {
                string element_text = util.GetElementText(driver, element);
                string element_type = util.GetElementType(driver, element);

                //Console.WriteLine("Click " + element_text + element_type + ".");

                driver.FindElement(element).Click();
                Thread.Sleep(1000);
            }
        }

        public void IClickALink(IWebDriver driver, string LinkToClick)
        {
            Console.WriteLine("Click " + LinkToClick + " Link.");

            driver.FindElement(By.LinkText(LinkToClick)).Click();
            Thread.Sleep(1000);
        }

        public void IOpenLinkInNewTab(IWebDriver driver, By element)
        {
            if (util.CheckElement(driver, element, 5))
            {
                string element_text = util.GetElementText(driver, element);

                //Console.WriteLine("Open " + element_text + " Link in New Tab.");

                IWebElement body = driver.FindElement(element);
                Actions OpenLink = new Actions(driver);
                OpenLink.KeyDown(Keys.Control).MoveToElement(body).Click().KeyUp(Keys.Control).Perform();

                driver.SwitchTo().Window(driver.WindowHandles.Last());
                Thread.Sleep(2000);
            }
        }

        public void IType(IWebDriver driver, By element, string keyword)
        {
            if (util.CheckElement(driver, element, 5))
            {
                string element_text = util.GetElementText(driver, element);

                //if (keyword.Equals(Keys.Tab))
                //{
                //    Console.WriteLine("Press Tab Key.");
                //}
                //else
                //{
                //    Console.WriteLine("Enter \"" + keyword + "\" in " + element_text + " Text Field.");
                //}

                driver.FindElement(element).SendKeys(keyword);
                Thread.Sleep(1000);
            }
        }

        public void ILogin(IWebDriver driver, string Username, string Password)
        {
            //if (!driver.Url.Contains("returnUrl") && driver.Url.Contains("loginContext=myAnalog"))
            //{
            //    Console.WriteLine("Login via Login Page:");
            //}

            if (util.CheckElement(driver, Elements.UserTextField, 5))
            {
                Console.Write("\t");
                IType(driver, Elements.UserTextField, Username);
            }

            if (util.CheckElement(driver, Elements.PwdTextField, 5))
            {
                Console.Write("\t");
                IType(driver, Elements.PwdTextField, Password);
            }

            if (util.CheckElement(driver, Elements.LoginBtn, 5))
            {
                Console.Write("\t");
                IClick(driver, Elements.LoginBtn);
                Thread.Sleep(3000);
            }

            /*****This is for the new page of Public Community****/
            if (util.CheckElement(driver, Elements.Public_Community_Username_Textfield, 5))
            {
                Console.WriteLine("Create a Public Community Username:");

                Console.Write("\t");
                IType(driver, Elements.Public_Community_Username_Textfield, "Test_Username_" + util.GenerateRandomNumber());

                Console.Write("\t");
                IClick(driver, Elements.Public_Community_Continue_Button);
            }
            Thread.Sleep(2000);

            GivenIAcceptCookies(driver);
        }

        public void ILoginViaLogInLink(IWebDriver driver, string Username, string Password)
        {
            Console.WriteLine("Login via Log in/Register Link:");

            Console.Write("\t");
            IClick(driver, Elements.SC_LoginLink);

            ILogin(driver, Username, Password);
        }

        public void IExpandMyAnalogMegaMenu(IWebDriver driver)
        {
            if (!util.CheckElement(driver, Elements.MainNavigation_ExpandedMyAnalogTab, 2))
            {
                IClick(driver, Elements.MainNavigation_MyAnalogTab);
            }
        }

        public void ILoginViaMyAnalogMegaMenu(IWebDriver driver, string Username, string Password)
        {
            Console.WriteLine("Login via MYANALOG Mega Menu:");

            Console.Write("\t");
            IExpandMyAnalogMegaMenu(driver);

            //Console.Write("\t");
            //IClick(driver, Elements.MyAnalogTab_LogInToMyAnalog_Btn);

            ILogin(driver, Username, Password);

            IClick(driver, Elements.MainNavigation_MyAnalogTab);
        }

        public void ILogInViaMyAnalogWidget(IWebDriver driver, string Username, string Password)
        {
            Console.WriteLine("Login via myAnalog Widget:");

            //--- Action: Click the myAnalog Widget Button ---//
            Console.Write("\t");
            IClick(driver, Elements.MyAnalog_Widget_Btn);

            //--- Action: Click the Log in to myAnalog Button ---//
            //Console.Write("\t");
            //IClick(driver, Elements.MyAnalog_Widget_LogInToMyAnalog_Btn);

            //--- Action: Enter valid credentials then click the Login Button ---//
            ILogin(driver, Username, Password);
            Thread.Sleep(4000);
        }

        //----- Log In via Save time by logging in or registering for myAnalog Panel (Note: This is used by the Forms Scripts) -----//
        public void ILogInViaSaveTimeByLoggingInPanel(IWebDriver driver, string Username, string Password)
        {
            Console.WriteLine("Login via Save time by logging in or registering for myAnalog Panel:");

            if (!util.CheckElement(driver, Elements.Forms_Expanded_SaveTimeByLoggingInOrRegisteringForMyAnalog_Panel, 1))
            {
                //--- Action: Click the Save time by logging in or registering for myAnalog Link ---//
                Console.Write("\t");
                IClick(driver, Elements.Forms_SaveTimeByLoggingInOrRegisteringForMyAnalog_Link);
            }

            //--- Action: Click the Login to myAnalog Button ---//
            Console.Write("\t");
            IClick(driver, Elements.Forms_Expanded_SaveTimeByLoggingInOrRegisteringForMyAnalog_Panel_LoginToMyAnalog_Button);

            //--- Action: Enter credentials, then Click the Login Button ---//
            ILogin(driver, Username, Password);
        }

        public void ISignOut(IWebDriver driver)
        {
            Console.WriteLine("Sign out via MYANALOG Mega Menu:");

            //--- Action: Expand the MYANALOG Mega Menu ---//
            if (!util.CheckElement(driver, Elements.MainNavigation_ExpandedMyAnalogTab, 2))
            {
                Console.Write("\t");
            }
            IExpandMyAnalogMegaMenu(driver);

            //--- Action: Click the sign out Link ---//
            Console.Write("\t");
            IClick(driver, Elements.MyAnalog_MegaMenu_SignOut_Link);
        }

        //--- Select a Value from a Dropdown by supplying the value attributes ---//
        public void ISelectFromDropdown(IWebDriver driver, By element, string ValueToSelect)
        {
            string element_text = util.GetElementText(driver, element);

            Console.WriteLine("Select \"" + ValueToSelect + "\" in " + element_text + " Dropdown.");

            SelectElement SS = new SelectElement(driver.FindElement(element));
            SS.SelectByValue(ValueToSelect);
        }

        public void ISelectFromDropdownByStringText(IWebDriver driver, By element, string ValueToSelect)
        {
            string element_text = util.GetElementText(driver, element);

            Console.WriteLine("Select \"" + ValueToSelect + "\" in " + element_text + " Dropdown.");

            SelectElement SS = new SelectElement(driver.FindElement(element));
            SS.SelectByText(ValueToSelect);
        }

        public void IDeleteValueOnFields(IWebDriver driver, By element)
        {
            if (util.CheckElement(driver, element, 5))
            {
                driver.FindElement(element).Clear();
                Thread.Sleep(1500);

                //--- Added this to make sure that the values are deleted incase the Clear() function didn't work ---//
                if (!driver.FindElement(element).GetAttribute("value").Equals(""))
                {
                    for (int ctr = util.ReturnAttribute(driver, element, "value").Length; ctr != 0; ctr--)
                    {
                        driver.FindElement(element).SendKeys(Keys.Backspace);
                    }
                }
            }
        }

        public void IMouseOverTo(IWebDriver driver, By element)
        {
            string element_text = util.GetElementText(driver, element);
            string element_type = util.GetElementType(driver, element);

            //Console.WriteLine("Mouse Hover to " + element_text + element_type + ".");

            if (Configuration.Browser.Equals("Firefox"))
            {
                ((IJavaScriptExecutor)driver).ExecuteScript("$(\"" + element + "\").mouseover()");
            }
            else
            {
                Actions Hover = new Actions(driver);
                Hover.MoveToElement(driver.FindElement(element)).Perform();
            }
        }

        //----- Open a New Browser Tab -----//
        public void IOpenNewTab(IWebDriver driver)
        {
            Console.WriteLine("Open a New Browser Tab.");

            ((IJavaScriptExecutor)driver).ExecuteScript("window.open()");
            driver.SwitchTo().Window(driver.WindowHandles.Last());
        }

        public void IVerifyEmailVerificationCode(IWebDriver driver, string Username)
        {
            Console.WriteLine("Verify Email Verification Code:");

            Console.Write("\t");
            IClick(driver, Elements.Registration_Skip_Btn);

            Console.Write("\t");
            IType(driver, Elements.Registration_VerifyEmail_TxtField, Username);

            Console.Write("\t");
            IClick(driver, Elements.Registration_SendVerificationCode_Btn);
            Thread.Sleep(1000);

            Console.Write("\t");
            IOpenNewTab(driver);

            Console.Write("\t");
            ICheckMailinatorEmail(driver, Username);

            Console.Write("\t");
            IClick(driver, By.CssSelector("table[class='table-striped jambo_table'] * tr>td:nth-child(2)"));

            driver.SwitchTo().Frame(driver.FindElement(By.CssSelector("iframe[id='html_msg_body']")));
            string VerificationCode = util.GetText(driver, By.CssSelector("td>strong")).Substring(util.GetText(driver, By.CssSelector("td>strong")).Length - 6).Trim();

            driver.SwitchTo().Window(driver.WindowHandles.First());

            Console.Write("\t");
            IType(driver, Elements.Registration_EnterCode_TextField, VerificationCode);

            Console.Write("\t");
            IClick(driver, Elements.Registration_SendVerificationCode_Btn);
            Thread.Sleep(3000);
        }

        public void ICheckMailinatorEmail(IWebDriver driver, string Email)
        {
            Console.WriteLine("Check Mailinator Email:");

            Console.Write("\t");
            Navigate(driver, "https://www.mailinator.com");

            Console.Write("\t");
            IType(driver, Elements.Mailinator_EmailAddress_TextField, Email);

            Console.Write("\t");
            IClick(driver, Elements.Mailinator_Go_Btn);
            Thread.Sleep(2500);
        }

        public void IDeleteValueOnFieldUsingBackSpace(IWebDriver driver, By element, string InputText)
        {
            if (util.CheckElement(driver, element, 5))
            {
                for (int a = InputText.Length; a != 0; a--)
                {
                    driver.FindElement(element).SendKeys(Keys.Backspace);
                    Thread.Sleep(1000);
                }
            }
        }

        public void ICheck(IWebDriver driver, By element)
        {
            if (util.CheckElement(driver, element, 5))
            {
                if (!driver.FindElement((element)).Selected)
                {
                    string element_text = util.GetElementText(driver, element);

                    Console.WriteLine("Check " + element_text + " Checkbox.");

                    driver.FindElement((element)).Click();
                }
            }
        }

        public void IUncheck(IWebDriver driver, By element)
        {
            if (util.CheckElement(driver, element, 5))
            {
                if (driver.FindElement((element)).Selected)
                {
                    string element_text = util.GetElementText(driver, element);

                    Console.WriteLine("Uncheck " + element_text + " Checkbox.");

                    driver.FindElement((element)).Click();
                }
            }
        }

        public void GivenIDragAndDrop(IWebDriver driver, By element, int offset_x, int offset_y)
        {
            //--- Note: ---//
            //--- Add value on offset_x to drag LEFT or RIGHT ---//
            //--- Add value on offset_y to drag UP or DOWN ---//

            if (util.CheckElement(driver, element, 5))
            {
                string element_text = util.GetElementText(driver, element);

                if (offset_x != 0)
                {
                    Console.WriteLine("Drag " + element_text + " Slider to \"" + offset_x + "\".");
                }

                if (offset_y != 0)
                {
                    Console.WriteLine("Drag " + element_text + " Slider to \"" + offset_y + "\".");
                }

                Actions ac = new Actions(driver);

                ac.DragAndDropToOffset(driver.FindElement(element), offset_x, offset_y);
                ac.Build().Perform();
            }
        }

        //----- GLOBAL SEARCH > Search via Global Search Textbox -----//
        public void ISearchViaGlobalSearchTextbox(IWebDriver driver, string SearchKeyword)
        {
            Console.WriteLine("Search via Global Search Textbox:");

            IDeleteValueOnFields(driver, Elements.GlobalSearchResults_Search_Txtbox);

            Console.Write("\t");
            IType(driver, Elements.GlobalSearchTextField, SearchKeyword);

            Console.Write("\t");
            IClick(driver, Elements.GlobalSearch_SearchIcon);
        }

        //----- GLOBAL SEARCH > Search via Global Search Results Textbox -----//
        public void ISearchViaGlobalSearchResultsTextbox(IWebDriver driver, string SearchKeyword)
        {
            //Console.WriteLine("Search via Global Search Results Textbox:");

            IDeleteValueOnFields(driver, Elements.GlobalSearchResults_Search_Txtbox);

            //Console.Write("\t");
            IType(driver, Elements.GlobalSearchResults_Search_Txtbox, SearchKeyword);

            //Console.Write("\t");
            IClick(driver, Elements.GlobalSearchResults_Search_Btn);
        }

        //----- GLOBAL SEARCH > Search via Global Search Results Textbox using Enter Key -----//
        public void ISearchViaGlobalSearchResultsTextboxUsingEnterKey(IWebDriver driver, string SearchKeyword)
        {
            Console.WriteLine("Search via Global Search Results Textbox using Enter Key:");

            IDeleteValueOnFields(driver, Elements.GlobalSearchResults_Search_Txtbox);

            Console.Write("\t");
            IType(driver, Elements.GlobalSearchResults_Search_Txtbox, SearchKeyword);

            Console.WriteLine("\tPress Enter Key");
            driver.FindElement(Elements.GlobalSearchResults_Search_Btn).SendKeys(Keys.Enter);
        }

        //----- SHOPPING CART > Add Model to Purchase -----//
        public void IAddModelToPurchase(IWebDriver driver, string ModelNo)
        {
            Console.WriteLine("Add Model to Purchase:");

            Console.Write("\t");
            IClick(driver, Elements.Shopping_Cart_Purchase_Radio_Button);

            IDeleteValueOnFields(driver, Elements.Shopping_Cart_Add_Model_Textfield);

            Console.Write("\t");
            IType(driver, Elements.Shopping_Cart_Add_Model_Textfield, ModelNo);

            Console.Write("\t");
            IClick(driver, Elements.Shopping_Cart_Add2Cart_Button);
        }

        //----- SHOPPING CART > Add Model to Sample -----//
        public void IAddModelToSample(IWebDriver driver, string ModelNo)
        {
            Console.WriteLine("Add Model to Sample:");

            Console.Write("\t");
            IClick(driver, Elements.Shopping_Cart_Sample_Radio_Button);

            IDeleteValueOnFields(driver, Elements.Shopping_Cart_Add_Model_Textfield);

            Console.Write("\t");
            IType(driver, Elements.Shopping_Cart_Add_Model_Textfield, ModelNo);

            Console.Write("\t");
            IClick(driver, Elements.Shopping_Cart_Add2Cart_Button);
        }

        //----- SHOPPING CART > Populate All the Fields in the SHIPPING ADDRESS / SHIPPING INFORMATION Page -----//
        public void IPopulateFieldsInShippingAddressPage(IWebDriver driver)
        {
            Console.WriteLine("Populate Fields in Shipping Address Page:");

            //--- Handling for the "The Central Board of Excise & Customs (CBEC) of India…" Pop-up ---//
            if (util.CheckElement(driver, Elements.Shopping_Cart_CBEC_PopUp, 2))
            {
                //--- Close Button ---//
                Console.Write("\t");
                IClick(driver, Elements.Shopping_Cart_CBEC_Close_Btn);
            }

            if (util.GetCount(driver, Elements.Shopping_Cart_ShippingAddress_PrevAdd_Sec) > 1 || !util.CheckElement(driver, Elements.Shopping_Cart_ShippingAddress_NewAdd_Sec, 5))
            {
                //----- Choose a shipping address -----//
                Console.Write("\t");
                IClick(driver, Elements.Shopping_Cart_Footer_Next_Button);
            }
            else
            {
                if (util.CheckElement(driver, Elements.Shopping_Cart_ShippingAddress_Edit_Btn, 10))
                {
                    Console.Write("\t");
                    IClick(driver, Elements.Shopping_Cart_ShippingAddress_Edit_Btn);
                }

                //--- Organization Name Textbox ---//
                if (string.IsNullOrEmpty(driver.FindElement(Elements.Shopping_Cart_OrgName_Tbx).GetAttribute("value")))
                {
                    IDeleteValueOnFields(driver, Elements.Shopping_Cart_OrgName_Tbx);
                    Console.Write("\t");
                    IType(driver, Elements.Shopping_Cart_OrgName_Tbx, "ADI");
                }

                //--- State/Province Dropdown ---//
                Console.Write("\t");
                IClick(driver, Elements.Shopping_Cart_StateProvince_Dd);
                Console.Write("\t");
                IClick(driver, By.CssSelector("select[id$='State']>option:nth-child(2)"));

                //--- City Textbox ---//
                if (string.IsNullOrEmpty(driver.FindElement(Elements.Shopping_Cart_City_Tbx).GetAttribute("value")))
                {
                    IDeleteValueOnFields(driver, Elements.Shopping_Cart_City_Tbx);
                    Console.Write("\t");
                    IType(driver, Elements.Shopping_Cart_City_Tbx, "Lynn");
                }

                //--- Address Line 1 Textbox ---//
                if (string.IsNullOrEmpty(driver.FindElement(Elements.Shopping_Cart_AddressLine1_Tbx).GetAttribute("value")))
                {
                    IDeleteValueOnFields(driver, Elements.Shopping_Cart_AddressLine1_Tbx);
                    Console.Write("\t");
                    IType(driver, Elements.Shopping_Cart_AddressLine1_Tbx, "780 Lynnway");
                }

                //--- Address Line 2 Textbox ---//
                IDeleteValueOnFields(driver, Elements.Shopping_Cart_AddressLine2_Tbx);

                //--- Address Line 3 Textbox ---//
                IDeleteValueOnFields(driver, Elements.Shopping_Cart_AddressLine3_Tbx);

                //--- Country/Region Dropdown ---//
                SelectElement country_dropdown = new SelectElement(driver.FindElement(Elements.Shopping_Cart_CountryRegion_Dd));
                string countryDd_selectedVal = country_dropdown.SelectedOption.Text.Trim();

                //--- Change the Zip/Postal Code if the Country/Region Selected is not UNITED STATES ---// 
                if (!countryDd_selectedVal.Equals("UNITED STATES"))
                {
                    IDeleteValueOnFields(driver, Elements.Shopping_Cart_ZipPostalCode_Tbx);
                    Console.Write("\t");
                    IType(driver, Elements.Shopping_Cart_ZipPostalCode_Tbx, "123456");
                }

                //----- Enter a New Shipping Address -----//
                if (util.CheckElement(driver, Elements.Shopping_Cart_ShippingAddress_ShipToThisAdd_Btn, 2))
                {
                    if (string.IsNullOrEmpty(driver.FindElement(Elements.Shopping_Cart_FirstName_Tbx).GetAttribute("value")))
                    {
                        //--- First Name Textbox ---//
                        IDeleteValueOnFields(driver, Elements.Shopping_Cart_FirstName_Tbx);
                        Console.Write("\t");
                        IType(driver, Elements.Shopping_Cart_FirstName_Tbx, "Test Order");
                    }

                    //--- Last Name Textbox ---//
                    if (string.IsNullOrEmpty(driver.FindElement(Elements.Shopping_Cart_LastName_Tbx).GetAttribute("value")))
                    {
                        IDeleteValueOnFields(driver, Elements.Shopping_Cart_LastName_Tbx);
                        Console.Write("\t");
                        IType(driver, Elements.Shopping_Cart_LastName_Tbx, "Test Order");
                    }

                    //--- Zip/Postal Code Textbox ---//
                    IDeleteValueOnFields(driver, Elements.Shopping_Cart_ZipPostalCode_Tbx);

                    //--- Country/Region Dropdown ---//
                    SelectElement edit_country_dropdown = new SelectElement(driver.FindElement(By.CssSelector("select[id$='Country']")));
                    string edit_countryDd_selectedVal = edit_country_dropdown.SelectedOption.Text.Trim();

                    if (edit_countryDd_selectedVal.Equals("UNITED STATES"))
                    {
                        Console.Write("\t");
                        IType(driver, Elements.Shopping_Cart_ZipPostalCode_Tbx, "12345");
                    }
                    else
                    {
                        Console.Write("\t");
                        IType(driver, Elements.Shopping_Cart_ZipPostalCode_Tbx, "123456");
                    }

                    //--- Taxable Radio Button ---//
                    IMouseOverTo(driver, Elements.Footer);
                    if (util.CheckElement(driver, By.CssSelector("input[id='rbEditTaxable']"), 2))
                    {
                        Console.Write("\t");
                        IClick(driver, By.CssSelector("input[id='rbEditTaxable']"));
                    }
                    else
                    {
                        Console.Write("\t");
                        IClick(driver, Elements.Shopping_Cart_Taxable_Rb);
                    }

                    //--- Telephone Textbox ---//
                    IDeleteValueOnFields(driver, Elements.Shopping_Cart_Telephone_Tbx);
                    Console.Write("\t");
                    IType(driver, Elements.Shopping_Cart_Telephone_Tbx, "123456789");

                    //--- SHIP TO THIS ADDRESS Button ---//
                    Console.Write("\t");
                    IClick(driver, Elements.Shopping_Cart_ShippingAddress_ShipToThisAdd_Btn);
                    if (util.CheckElement(driver, Elements.Shopping_Cart_ShippingAddress_ShipToThisAdd_Btn, 2))
                    {
                        Console.Write("\t");
                        IClick(driver, Elements.Shopping_Cart_ShippingAddress_ShipToThisAdd_Btn);
                    }

                    if (util.CheckElement(driver, Elements.Shopping_Cart_ShippingAddress_TaxOption_ErrMsg, 2))
                    {
                        IMouseOverTo(driver, Elements.Footer);
                        if (util.CheckElement(driver, By.CssSelector("input[id='rbEditTaxable']"), 2))
                        {
                            Console.Write("\t");
                            IClick(driver, By.CssSelector("input[id='rbEditTaxable']"));
                        }
                        else
                        {
                            Console.Write("\t");
                            IClick(driver, Elements.Shopping_Cart_Taxable_Rb);
                        }

                        //--- SHIP TO THIS ADDRESS Button ---//
                        Console.Write("\t");
                        IClick(driver, Elements.Shopping_Cart_ShippingAddress_ShipToThisAdd_Btn);
                        if (util.CheckElement(driver, Elements.Shopping_Cart_ShippingAddress_ShipToThisAdd_Btn, 2))
                        {
                            Console.Write("\t");
                            IClick(driver, Elements.Shopping_Cart_ShippingAddress_ShipToThisAdd_Btn);
                        }
                    }
                }
                else
                {
                    //--- Taxable Radio Button ---//
                    if (util.CheckElement(driver, By.CssSelector("input[id='rbEditTaxable']"), 2))
                    {
                        Console.Write("\t");
                        IClick(driver, By.CssSelector("input[id='rbEditTaxable']"));
                    }
                    else
                    {
                        Console.Write("\t");
                        IClick(driver, Elements.Shopping_Cart_Taxable_Rb);
                    }

                    //--- Next Button ---//
                    if (util.CheckElement(driver, By.CssSelector("input[id^='btnShipToEditedAddress']"), 2))
                    {
                        Console.Write("\t");
                        IClick(driver, By.CssSelector("input[id^='btnShipToEditedAddress']"));
                    }
                    else
                    {
                        Console.Write("\t");
                        IClick(driver, Elements.Shopping_Cart_Footer_Next_Button);
                    }
                }
            }

            Thread.Sleep(1000);
        }

        //----- SHOPPING CART > Populate All the Fields in the BILLING ADDRESS / BILLING INFORMATION Page -----//
        public void IPopulateFieldsInBillingAddressPage(IWebDriver driver)
        {
            Console.WriteLine("Populate Fields in Billing Address Page:");

            if (util.CheckElement(driver, Elements.Shopping_Cart_BillingAddress_UseMyShippingAddAsMyBillingAdd_Cb, 2))
            {
                //--- Use my shipping address as my billing address Checkbox ---//
                Console.Write("\t");
                IClick(driver, Elements.Shopping_Cart_BillingAddress_UseMyShippingAddAsMyBillingAdd_Cb);
            }
            else
            {
                //--- Organization Name Textbox ---//
                if (string.IsNullOrEmpty(driver.FindElement(Elements.Shopping_Cart_OrgName_Tbx).GetAttribute("value")))
                {
                    IDeleteValueOnFields(driver, Elements.Shopping_Cart_OrgName_Tbx);
                    Console.Write("\t");
                    IType(driver, Elements.Shopping_Cart_OrgName_Tbx, "ADI");
                }

                //--- State/Province Dropdown ---//
                Console.Write("\t");
                IClick(driver, Elements.Shopping_Cart_StateProvince_Dd);
                Console.Write("\t");
                IClick(driver, By.CssSelector("select[id$='State']>option:nth-child(2)"));

                //--- City Textbox ---//
                if (string.IsNullOrEmpty(driver.FindElement(Elements.Shopping_Cart_City_Tbx).GetAttribute("value")))
                {
                    IDeleteValueOnFields(driver, Elements.Shopping_Cart_City_Tbx);
                    Console.Write("\t");
                    IType(driver, Elements.Shopping_Cart_City_Tbx, "Lynn");
                }

                //--- Address Line 1 Textbox ---//
                if (string.IsNullOrEmpty(driver.FindElement(Elements.Shopping_Cart_AddressLine1_Tbx).GetAttribute("value")))
                {
                    IDeleteValueOnFields(driver, Elements.Shopping_Cart_AddressLine1_Tbx);
                    Console.Write("\t");
                    IType(driver, Elements.Shopping_Cart_AddressLine1_Tbx, "780 Lynnway");
                }

                //--- Address Line 2 Textbox ---//
                IDeleteValueOnFields(driver, Elements.Shopping_Cart_AddressLine2_Tbx);

                //--- Address Line 3 Textbox ---//
                IDeleteValueOnFields(driver, Elements.Shopping_Cart_AddressLine3_Tbx);
            }

            //--- Next Button ---//
            Console.Write("\t");
            IClick(driver, Elements.Shopping_Cart_Footer_Next_Button);
        }

        //----- SHOPPING CART > Populate All the Fields in the ABOUT YOUR PROJECT / PROJECT DETAILS Page -----//
        public void IPopulateFieldsInAboutYourProjectPage(IWebDriver driver)
        {
            Console.WriteLine("Populate Fields in About Your Project Page:");

            if (util.CheckElement(driver, Elements.Shopping_Cart_AboutYourProject_SamplesQuestions_Sec, 2))
            {
                //--- * Occupation Dropdown (AL-14727: UI Changes for shopping cart changes for leads information) ---//
                Console.Write("\t");
                IClick(driver, Elements.Shopping_Cart_AboutYourProject_Occupation_Dd);
                Console.Write("\t");
                IClick(driver, By.CssSelector("div[class='adi-tree dropdown-menu']>ul>li>span"));
                Console.Write("\t");
                IClick(driver, By.CssSelector("div[class='adi-tree dropdown-menu']>ul>li>ul>li[style*='list-item']>a"));

                Dictionary<string, string> select_value = new Dictionary<string, string>();

                if (util.CheckElement(driver, Elements.Shopping_Cart_AboutYourProject_SampleUsageUS_Dd, 10))
                {
                    //--- *How will you be using the samples provided? Dropdown ---//
                    select_value.Add("select[id='ddlSampleUsageUS']", "Academic Use");

                    //--- *What is your preferred distributor? Dropdown ---//
                    select_value.Add("select[id='distributor']", "Arrow");
                }

                //--- * Which of the following best describes your design phase? Dropdown ---//
                select_value.Add("select[id='designphase']", "Requirements Definition");

                //--- * What is your best estimate of when the product will go into production? Dropdown ---//
                select_value.Add("select[id='bestestimate']", "0-3 months");

                //--- * What is your best estimate of how many components you will use on a yearly basis? Dropdown ---//
                select_value.Add("select[id='componentqty']", "1-999");

                foreach (KeyValuePair<string, string> value in select_value)
                {
                    Console.Write("\t");
                    ISelectFromDropdown(driver, By.CssSelector(value.Key), value.Value);
                }
            }

            //--- *What is the application / end use of these products? Dropdown ---//
            Console.Write("\t");
            IClick(driver, Elements.Shopping_Cart_AboutYourProject_EndUse_Dd);
            Console.Write("\t");
            IClick(driver, By.CssSelector("a[title='Aerospace & Defense']"));
            Console.Write("\t");
            IClick(driver, By.CssSelector("a[title='Radar']"));

            //--- Are you buying these components for a nuclear, a missile, chemical & biological, or military application or facility? ---//
            if (util.CheckElement(driver, Elements.Shopping_Cart_AboutYourProject_NonUSQuestions_Sec, 2))
            {
                //--- Nuclear - No Radio Button ---//
                Console.Write("\t");
                IClick(driver, Elements.Shopping_Cart_AboutYourProject_Nuclear_No_Rb);
            }

            //--- *Will you be re-exporting this product outside of your country without putting it in an application? ---//

            //--- Reexporting - No Radio Button ---//
            Console.Write("\t");
            IClick(driver, Elements.Shopping_Cart_AboutYourProject_ReExporting_No_Rb);

            //--- Next Button ---//
            Console.Write("\t");
            IClick(driver, Elements.Shopping_Cart_Footer_Next_Button);
            Thread.Sleep(4000);
        }

        //----- SHOPPING CART > Populate All the Fields in the ENTER PAYMENT AND PLACE ORDER Page -----//
        public void IPopulateFieldsInEnterPaymentAndPlaceOrderPage(IWebDriver driver)
        {
            Console.WriteLine("Populate Fields in Enter Payment and Place Order Page:");

            //--- *Select Delivery Method (Applies to purchased product only) Dropdown ---//
            if (util.GetCount(driver, By.CssSelector("select[id='ddlShippingMethod']>option")) > 2)
            {
                Console.Write("\t");
                ISelectFromDropdown(driver, Elements.Shopping_Cart_PlaceOrder_ShippingMethod_Dd, "F1");
            }
            else
            {
                Console.Write("\t");
                IClick(driver, By.CssSelector("select[id='ddlShippingMethod']>option:nth-child(2)"));
            }

            IMouseOverTo(driver, Elements.Shopping_Cart_PlaceOrder_Payment_Sec);
            Thread.Sleep(1000);

            driver.SwitchTo().Frame(driver.FindElement(By.CssSelector("iframe[class='fit-to-parent']")));

            //--- Card Number Textbox ---//
            IDeleteValueOnFields(driver, Elements.Shopping_Cart_PlaceOrder_Cc_CardNo_Tbx);
            Console.Write("\t");
            IType(driver, Elements.Shopping_Cart_PlaceOrder_Cc_CardNo_Tbx, "4111111111111111");

            //--- Card Holder Name Textbox ---//
            IDeleteValueOnFields(driver, Elements.Shopping_Cart_PlaceOrder_Cc_CardHolderName_Tbx);
            Console.Write("\t");
            IType(driver, Elements.Shopping_Cart_PlaceOrder_Cc_CardHolderName_Tbx, "Test Auto");

            //--- Expiry Textbox ---//
            IDeleteValueOnFields(driver, Elements.Shopping_Cart_PlaceOrder_Cc_Expiry_Tbx);
            Console.Write("\t");
            IType(driver, Elements.Shopping_Cart_PlaceOrder_Cc_Expiry_Tbx, "12/" + DateTime.Now.ToString("yy"));

            //--- CVV Textbox ---//
            IDeleteValueOnFields(driver, Elements.Shopping_Cart_PlaceOrder_Cc_Cvv_Tbx);
            Console.Write("\t");
            IType(driver, Elements.Shopping_Cart_PlaceOrder_Cc_Cvv_Tbx, "123");

            //--- SUBMIT Button ---//
            Console.Write("\t");
            IClick(driver, Elements.Shopping_Cart_PlaceOrder_Cc_Submit_Btn);
            driver.SwitchTo().Window(driver.WindowHandles.First());

            //--- Terms & Conditions Checkbox ---//
            Console.Write("\t");
            IClick(driver, Elements.AcceptSCTerms);

            //--- Place Order Button ---//
            Console.Write("\t");
            IClick(driver, By.CssSelector("input[id='btnNext2']"));
            Thread.Sleep(4000);
        }

        //----- SHOPPING CART > Populate All the Fields in the SAMPLE REQUEST Page -----//
        public void IPopulateFieldsInPlaceSampleRequestPage(IWebDriver driver)
        {
            Console.WriteLine("Populate Fields in Place Sample Request Page:");

            //--- Terms & Conditions Checkbox ---//
            Console.Write("\t");
            ICheck(driver, Elements.AcceptSCTerms);

            //--- Place Order Button ---//
            Console.Write("\t");
            IClick(driver, Elements.Shopping_Cart_Footer_Next_Button);
            Thread.Sleep(4000);
        }

        //----- SHOPPING CART > Go to the BILLING ADDRESS / BILLING INFORMATION Page from the SHIPPING ADDRESS / SHIPPING INFORMATION Page -----//
        public void GoToBillingAddressPage(IWebDriver driver)
        {
            //----- Shipping Address / Shipping Information Page -----//

            //--- Handling for the "The Central Board of Excise & Customs (CBEC) of India…" Pop-up ---//
            if (util.CheckElement(driver, Elements.Shopping_Cart_CBEC_PopUp, 2))
            {
                //--- Close Button ---//
                Console.Write("\t");
                IClick(driver, Elements.Shopping_Cart_CBEC_Close_Btn);
            }

            if (util.CheckElement(driver, Elements.Shopping_Cart_ShippingAddress_PrevAdd_Sec, 2))
            {
                //--- Next Button ---//
                Console.Write("\t");
                IClick(driver, Elements.Shopping_Cart_Heading_Next_Button);
            }
            else
            {
                IPopulateFieldsInShippingAddressPage(driver);
            }

            Thread.Sleep(1000);
        }

        //----- SHOPPING CART > Go to the ABOUT YOUR PROJECT / PROJECT DETAILS Page from the SHIPPING ADDRESS / SHIPPING INFORMATION Page -----//
        public void GoToAboutYourProjectPage(IWebDriver driver)
        {
            //----- Shipping Address / Shipping Information Page -----//
            GoToBillingAddressPage(driver);
            Thread.Sleep(1000);

            //----- Billing Address / Billing Information Page -----//
            if (driver.Url.Contains("BillingAddress.aspx"))
            {
                if (util.CheckElement(driver, Elements.Shopping_Cart_BillingAddress_UseMyShippingAddAsMyBillingAdd_Cb, 2))
                {
                    //--- Use my shipping address as my billing address Checkbox ---//
                    Console.Write("\t");
                    IClick(driver, Elements.Shopping_Cart_BillingAddress_UseMyShippingAddAsMyBillingAdd_Cb);
                }

                Console.Write("\t");
                IClick(driver, Elements.Shopping_Cart_Heading_Next_Button);

                if (util.CheckElement(driver, By.CssSelector("div[class$='error']"), 2))
                {
                    IPopulateFieldsInBillingAddressPage(driver);
                }
            }

            Thread.Sleep(2000);
        }

        //----- MYANALOG > Cancel the Orders -----//
        public void ICancelOrdersInMyAnalog(IWebDriver driver, string username, string password)
        {
            Console.WriteLine("Cancel Orders in myAnalog:");

            string myAnalog_orders_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + "en/app/orders/";

            IRejectCCPA(driver);

            if (!driver.Url.Equals(myAnalog_orders_url))
            {
                driver.Navigate().GoToUrl(myAnalog_orders_url);
                Console.WriteLine("Go to: " + myAnalog_orders_url);
                Thread.Sleep(1000);
            }

            //--- If the Page redirects to the Login Page ---//
            if (util.CheckElement(driver, By.CssSelector("div[class='my-analog-content']>form"), 2) || util.CheckElement(driver, By.CssSelector("section[class='account login']>form"), 2))
            {
                ILogin(driver, username, password);
                Thread.Sleep(500);
            }

            string orderNumber_locator = null;

            driver.Navigate().Refresh();
            Thread.Sleep(1000);

            int orders_count = util.GetCount(driver, By.CssSelector("table[class='my orders']>tbody>tr"));
            for (int ctr2 = 1; ctr2 <= orders_count; ctr2++)
            {
                if (util.GetText(driver, Elements.MyAnalog_Orders_Status_1st_Row_Label).Equals("Not delivered"))
                {
                    int ctr3 = 0;
                    do
                    {
                        ctr3++;
                    }
                    while (!util.CheckElement(driver, By.CssSelector("table[class='my orders']>tbody>tr:nth-child(" + ctr3 + ") * a"), 5));
                    orderNumber_locator = "table[class='my orders']>tbody>tr:nth-child(" + ctr3 + ") * a";

                    //--- Order Number - <Order Number> Link ---//
                    Console.Write("\t");
                    IClick(driver, By.CssSelector(orderNumber_locator));

                    //----- Order Number/Details Page -----//
                    do
                    {
                        driver.Navigate().Refresh();
                    }
                    while (util.CheckElement(driver, By.CssSelector("section[class='order details']>p"), 5));

                    int modelRows_count = util.GetCount(driver, By.CssSelector("tbody>tr>td[class='cancel order']"));
                    for (int ctr4 = 1; ctr4 <= modelRows_count; ctr4++)
                    {
                        IMouseOverTo(driver, Elements.Footer);
                        Thread.Sleep(1000);

                        //--- <Model Name> - Cancel Order Button ---//
                        Console.Write("\t");
                        IClick(driver, By.CssSelector("tbody>tr:nth-child(" + ctr4 + ")>td[class='cancel order']"));

                        //--- Cancel Order Dialog - Cancel Order Button ---//
                        Console.Write("\t");
                        IClick(driver, Elements.MyAnalog_Orders_OrderNumber_CancelOrder_PopUp_Box_CancelOrder_Button);
                        Thread.Sleep(1000);

                        driver.Navigate().Refresh();
                        Thread.Sleep(1000);
                    }

                    driver.Navigate().GoToUrl(myAnalog_orders_url);
                    Thread.Sleep(1000);

                    driver.Navigate().Refresh();
                    Thread.Sleep(1000);
                }
                else if (util.GetText(driver, Elements.MyAnalog_Orders_Status_1st_Row_Label).Equals("Cancelled"))
                {
                    break;
                }
                else if (util.GetText(driver, Elements.MyAnalog_Orders_Status_1st_Row_Label).Equals("Fully delivered"))
                {
                    break;
                }
            }
        }

        //----- MYANALOG > Remove All Saved Products -----//
        public void IRemoveAllSavedProductsInMyAnalog(IWebDriver driver, string username, string password)
        {
            Console.WriteLine("Remove All Saved Products in myAnalog:");

            string myAnalogProducts_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + "en/app/products";

            if (!driver.Url.EndsWith("/app/products"))
            {
                Console.Write("\t");
                driver.Navigate().GoToUrl(myAnalogProducts_url);
                Console.WriteLine("Go to: " + myAnalogProducts_url);
                Thread.Sleep(1000);

                if (driver.Url.Contains("b2clogin"))
                {
                    Console.Write("\t");
                    ILogin(driver, username, password);
                    Thread.Sleep(1000);
                }

                Thread.Sleep(1000);
            }

            Thread.Sleep(4000);
            do
            {
                int savedProducts_count = util.GetCount(driver, By.CssSelector("section[class='product']"));
                for (int ctr3 = 1; ctr3 <= savedProducts_count; ctr3++)
                {
                    //--- Options/Ellipsis Button ---//
                    Console.Write("\t");
                    IClick(driver, By.CssSelector("section[class='product']:nth-of-type(1) * div[class='controls'] * img"));

                    //--- Remove Button ---//
                    Console.Write("\t");
                    IClick(driver, By.CssSelector("menu[class='options dropdown']>button:nth-last-of-type(1)"));

                    if (util.CheckElement(driver, By.CssSelector("button[class='ma btn btn-primary']"), 2))
                    {
                        //--- Remove Button ---//
                        Console.Write("\t");
                        IClick(driver, By.CssSelector("button[class='ma btn btn-primary']"));
                    }

                    Thread.Sleep(4000);
                }
            }
            while (util.GetCount(driver, By.CssSelector("section[class='product']")) != 0);
        }

        /******Create a New US Account*******/
        public void ICreateNewUser(IWebDriver driver, string EmailAddress, string Fname, string Lname, string Pwd, string City, string ZipCode, string CompanyName)
        {
            Console.WriteLine("Create New User:");

            Console.Write("\t");
            IVerifyEmailVerificationCode(driver, EmailAddress);

            Console.Write("\t");
            IType(driver, Elements.Registration_FirstName_TxtField, Fname);

            Console.Write("\t");
            IType(driver, Elements.Registration_LastName_TxtField, Lname);

            Console.Write("\t");
            IType(driver, Elements.Registration_Password_TxtField, Pwd);

            Console.Write("\t");
            IType(driver, Elements.Registration_ConfirmPassword_TxtField, Pwd);

            Console.Write("\t");
            IType(driver, Elements.Registration_ZipCode_TxtField, ZipCode);

            Console.Write("\t");
            IType(driver, Elements.Registration_CompName_TxtField, CompanyName);

            /*****state Dropdown****/
            Console.Write("\t");
            IClick(driver, Elements.Registration_Country_Dropdown);

            Console.Write("\t");
            IClick(driver, By.CssSelector("div[class='select required']>div[class='options open']>div:nth-child(1)"));

            /*****state Dropdown****/
            Console.Write("\t");
            IClick(driver, By.CssSelector("div[class='select required']>button"));

            Console.Write("\t");
            IClick(driver, By.CssSelector("div[class='select required']>div[class='options open']>div:nth-child(1)"));

            /*****City****/
            Console.Write("\t");
            IType(driver, Elements.Registration_City_TxtField, City);

            Console.Write("\t");
            IClick(driver, Elements.Registration_TermsOfUse_ChxBox);
            Thread.Sleep(3000);

            Console.Write("\t");
            IClick(driver, Elements.Registration_Register_Btn);
        }

        /*****Remove Market Application in myAnalog******/
        public void IRemoveMarketInMyAnalog(IWebDriver driver, string Locale, string Username, string Password, string MarketToRemove)
        {
            Console.WriteLine("Remove Markets in myAnalog:");

            Console.Write("\t");
            Navigate(driver, Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/app");

            if (driver.Url.Contains("b2clogin"))
            {
                Console.Write("\t");
                ILogin(driver, Username, Password);
            }

            Console.Write("\t");
            IClick(driver, By.CssSelector("div[class='col-md-4 categories and markets']>aside:nth-child(2) button"));

            for (int x = 1; util.CheckElement(driver, By.CssSelector("section[class='interests__menu__items__saved'] ul[class='interests__menu__items__list']>li:nth-child(" + x + ")"), 5); x++)
            {
                if (util.GetText(driver, By.CssSelector("section[class='interests__menu__items__saved'] ul[class='interests__menu__items__list']>li:nth-child(" + x + ") span")).Equals(MarketToRemove))
                {
                    if (driver.FindElement(By.CssSelector("section[class='interests__menu__items__saved'] ul>li:nth-child(" + x + ") div[class='switch'] input")).Selected)
                    {
                        Console.Write("\t");
                        IClick(driver, By.CssSelector("section[class='interests__menu__items__saved'] ul>li:nth-child(" + x + ") div[class='switch'] label"));

                        Console.Write("\t");
                        IClick(driver, By.CssSelector("button[class='ma btn btn-primary btn-sm']"));

                        break;
                    }
                    else
                    {
                        break;
                    }
                }
            }
        }

        /*****Remove Item in myResources of myAnalog******/
        public void IRemoveItemInMyResources(IWebDriver driver, string Locale, string Username, string Password, string ItemToRemove)
        {
            Console.WriteLine("Remove Resources in myAnalog:");

            Console.Write("\t");
            Navigate(driver, Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/app/resources");

            if (driver.Url.Contains("b2clogin"))
            {
                Console.Write("\t");
                ILogin(driver, Username, Password);
            }

            for (int x = 1; util.CheckElement(driver, By.CssSelector("div[class='saved content'] tr:nth-child(" + x + ")"), 5); x++)
            {
                if (util.GetText(driver, By.CssSelector("div[class='saved content'] tr:nth-child(" + x + ")>td:nth-child(1) span")).Equals(ItemToRemove))
                {
                    Console.Write("\t");
                    IClick(driver, By.CssSelector("div[class='saved content'] tr:nth-child(" + x + ")>td:nth-child(4) button"));

                    Console.Write("\t");
                    IClick(driver, By.CssSelector("div[class='saved content'] tr:nth-child(" + x + ")>td:nth-child(4) menu[class='options dropdown'] button:nth-child(4)"));

                    Console.Write("\t");
                    IClick(driver, By.CssSelector("div[class='modal fade in'] button[class='ma btn btn-primary']"));

                    break;
                }
            }
        }

        /*****Remove Market Application in myAnalog******/
        public void IRemoveCategoryInMyAnalog(IWebDriver driver, string Locale, string Username, string Password, string CategoryToRemove)
        {
            Console.WriteLine("Remove Categories in myAnalog:");

            Console.Write("\t");
            Navigate(driver, Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/app");

            if (driver.Url.Contains("b2clogin"))
            {
                Console.Write("\t");
                ILogin(driver, Username, Password);
            }

            Console.Write("\t");
            IClick(driver, By.CssSelector("div[class='col-md-4 categories and markets']>aside:nth-child(1) button"));

            for (int x = 1; util.CheckElement(driver, By.CssSelector("section[class='interests__menu__items__saved'] ul[class='interests__menu__items__list']>li:nth-child(" + x + ")"), 5); x++)
            {
                if (util.GetText(driver, By.CssSelector("section[class='interests__menu__items__saved'] ul[class='interests__menu__items__list']>li:nth-child(" + x + ") span")).Equals(CategoryToRemove))
                {
                    if (driver.FindElement(By.CssSelector("section[class='interests__menu__items__saved'] ul>li:nth-child(" + x + ") div[class='switch'] input")).Selected)
                    {
                        Console.Write("\t");
                        IClick(driver, By.CssSelector("section[class='interests__menu__items__saved'] ul>li:nth-child(" + x + ") div[class='switch'] label"));

                        Console.Write("\t");
                        IClick(driver, By.CssSelector("button[class='ma btn btn-primary btn-sm']"));

                        break;
                    }
                    else
                    {
                        break;
                    }
                }
            }
        }

        //----- MYANALOG > Create a Project in myAnalog -----//
        public void ICreateAProjectInMyAnalog(IWebDriver driver, string Locale)
        {
            Console.WriteLine("Create a Project in myAnalog:");

            //--- Pre-condition: The User must be already Logged In ---//

            if (!driver.Url.EndsWith("/app/projects#") && !driver.Url.EndsWith("/app/projects"))
            {
                //--- Action: Go to the myAnalog Projects Page ---//
                Console.Write("\t");
                string myAnalog_projects_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + "app/projects";
                driver.Navigate().GoToUrl(myAnalog_projects_url);
                Console.WriteLine("Go to: " + myAnalog_projects_url);

                //--- Action: Accept the Cookie ---//
                GivenIAcceptCookies(driver);
            }

            //--- Action: Click the Create a new project Button ---//
            Console.Write("\t");
            IClick(driver, Elements.MyAnalog_Projects_CreateANewProject_Button);

            //--- Action: Enter a Value in Project Name ---//
            Console.Write("\t");
            string projectName_input = "ProjectName_" + util.RandomString();
            IDeleteValueOnFields(driver, Elements.MyAnalog_Projects_CreateANewProject_Modal_ProjectName_InputBox);
            IType(driver, Elements.MyAnalog_Projects_CreateANewProject_Modal_ProjectName_InputBox, projectName_input);

            //--- Action: Click the Done Button ---//
            Console.Write("\t");
            IClick(driver, Elements.MyAnalog_Projects_CreateANewProject_Modal_Done_Button);
            Thread.Sleep(1000);

            //--- Action: Click the Ok Button ---//
            Console.Write("\t");
            IClick(driver, Elements.MyAnalog_Projects_CreatedNewProject_Modal_Ok_Button);

            driver.Navigate().Refresh();
            Console.WriteLine("\tRefresh the Page.");
        }

        //----- MYANALOG > Create a Project Invite in myAnalog -----//
        public void ICreateAProjectInviteInMyAnalog(IWebDriver driver, string Locale, string Invitee_EmailAddress)
        {
            Console.WriteLine("Create a Project Invite in myAnalog:");

            //--- Pre-condition: The User must be already Logged In ---//

            if (!driver.Url.EndsWith("/app/projects#") && !driver.Url.EndsWith("/app/projects"))
            {
                //--- Action: Go to the myAnalog Projects Page ---//
                Console.Write("\t");
                string myAnalog_projects_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + "app/projects";
                driver.Navigate().GoToUrl(myAnalog_projects_url);
                Console.WriteLine("Go to: " + myAnalog_projects_url);

                //--- Action: Accept the Cookie ---//
                GivenIAcceptCookies(driver);
            }

            //--- Action: Click the Invite Button ---//
            Console.Write("\t");
            IClick(driver, Elements.MyAnalog_Projects_Invite_Button);

            //--- Action: Enter Email in Invitee ---//
            Console.Write("\t");
            string invitees_input = Invitee_EmailAddress;
            IDeleteValueOnFields(driver, Elements.MyAnalog_Projects_Invite_Modal_Invitees_InputBox);
            IType(driver, Elements.MyAnalog_Projects_Invite_Modal_Invitees_InputBox, invitees_input);

            //--- Action: Click the Invite Button ---//
            Console.Write("\t");
            IClick(driver, Elements.MyAnalog_Projects_Invite_Modal_Invite_Button);

            //--- Action: Click the Ok Button ---//
            Console.Write("\t");
            IClick(driver, Elements.MyAnalog_Projects_CreatedNewInvite_Modal_Ok_Button);
        }

        //----- MYANALOG > Project > Save Project Products in myAnalog -----//
        public void ISaveProjectProductsInMyAnalog(IWebDriver driver, string Locale, string ProductNumber_Input)
        {
            Console.WriteLine("Save Project Products in myAnalog:");

            //--- Pre-condition: The User must be already Logged In ---//

            if (!driver.Url.Contains("/app/projects/"))
            {
                //--- Action: Go to the myAnalog Projects Page ---//
                Console.Write("\t");
                string myAnalog_projects_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + "app/projects";
                driver.Navigate().GoToUrl(myAnalog_projects_url);
                Console.WriteLine("Go to: " + myAnalog_projects_url);

                //--- Action: Accept the Cookie ---//
                GivenIAcceptCookies(driver);

                //--- Action: Click the Project Details Button ---//
                Console.Write("\t");
                IClick(driver, Elements.MyAnalog_Projects_ProjectDetails_Buttons);
            }

            //--- Action: Enter a Product in Add products to myAnalog ---//
            Console.Write("\t");
            string addProductsToMyAnalog_input = ProductNumber_Input;
            IDeleteValueOnFields(driver, Elements.MyAnalog_ProjectDetailPage_ProjectProducts_AddProductsToMyAnalog_InputBox);
            IType(driver, Elements.MyAnalog_ProjectDetailPage_ProjectProducts_AddProductsToMyAnalog_InputBox, addProductsToMyAnalog_input);

            //--- Action: Click on the Search Results ---//
            Console.Write("\t");
            IClick(driver, Elements.MyAnalog_ProjectDetailPage_ProjectProducts_AddProductsToMyAnalog_Results);
        }

        //----- MYANALOG > Project > Delete All Saved Project Products in myAnalog -----//
        public void DeleteAllSavedProjectProductsInMyAnalog(IWebDriver driver, string Locale)
        {
            Console.WriteLine("Delete All Saved Project Products in myAnalog:");

            //--- Pre-condition: The User must be already Logged In ---//

            if (!driver.Url.Contains("/app/projects/"))
            {
                //--- Action: Go to the myAnalog Projects Page ---//
                Console.Write("\t");
                string myAnalog_projects_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + "app/projects";
                driver.Navigate().GoToUrl(myAnalog_projects_url);
                Console.WriteLine("Go to: " + myAnalog_projects_url);

                //--- Action: Accept the Cookie ---//
                GivenIAcceptCookies(driver);

                //--- Action: Click the Project Details Button ---//
                Console.Write("\t");
                IClick(driver, Elements.MyAnalog_Projects_ProjectDetails_Buttons);
            }

            //--- Action: Click the Edit Button ---//
            Console.Write("\t");
            IClick(driver, Elements.MyAnalog_ProjectDetailPage_ProjectProducts_Edit_Button);

            do
            {
                //--- Action: Click the X Icon ---//
                Console.Write("\t");
                IClick(driver, Elements.MyAnalog_ProjectDetailPage_SavedProducts_X_Icon);
            }
            while (util.CheckElement(driver, Elements.MyAnalog_ProjectDetailPage_SavedProducts, 1));

            //--- Action: Click the Save Button ---//
            Console.Write("\t");
            IClick(driver, Elements.MyAnalog_ProjectDetailPage_ProjectProducts_Save_Button);
        }

        //----- MYANALOG > Project > Delete All Saved Project Resources in myAnalog -----//
        public void DeleteAllSavedProjectResourcesInMyAnalog(IWebDriver driver, string Locale)
        {
            Console.WriteLine("Delete All Saved Project Resources in myAnalog:");

            //--- Pre-condition: The User must be already Logged In ---//

            if (!driver.Url.Contains("/app/projects/"))
            {
                //--- Action: Go to the myAnalog Projects Page ---//
                Console.Write("\t");
                string myAnalog_projects_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/" + "app/projects";
                driver.Navigate().GoToUrl(myAnalog_projects_url);
                Console.WriteLine("Go to: " + myAnalog_projects_url);

                //--- Action: Accept the Cookie ---//
                GivenIAcceptCookies(driver);

                //--- Action: Click the Project Details Button ---//
                Console.Write("\t");
                IClick(driver, Elements.MyAnalog_Projects_ProjectDetails_Buttons);
            }

            //--- Action: Click the Edit Button ---//
            Console.Write("\t");
            IClick(driver, Elements.MyAnalog_ProjectDetailPage_ProjectResources_Edit_Button);

            do
            {
                //--- Action: Click the X Icon ---//
                Console.Write("\t");
                IClick(driver, Elements.MyAnalog_ProjectDetailPage_SavedResources_X_Icon);
            }
            while (util.CheckElement(driver, Elements.MyAnalog_ProjectDetailPage_SavedResources, 1));

            //--- Action: Click the Save Button ---//
            Console.Write("\t");
            IClick(driver, Elements.MyAnalog_ProjectDetailPage_ProjectResources_Save_Button);
        }

        //----- MYANALOG > Project > Save Project Parametric Searches in myAnalog -----//
        public void ISaveProjectParametricSearchesInMyAnalog(IWebDriver driver, string Locale, string Pst_Url)
        {
            Console.WriteLine("Save Project Parametric Searches in myAnalog:");

            //--- Pre-condition: The User must be already Logged In ---//

            //--- Action: Go to a PST Page ---//
            Console.Write("\t");
            driver.Navigate().GoToUrl(Configuration.Env_Url + Locale + Pst_Url);
            Thread.Sleep(1000);
            Console.WriteLine("Go to: " + Configuration.Env_Url + Locale + Pst_Url);

            //--- Action: Accept the Cookie ---//
            GivenIAcceptCookies(driver);

            //--- Action: Click the Save to myAnalog Button ---//
            Console.Write("\t");
            IClick(driver, Elements.PST_SaveToMyAnalog_Button);

            if (util.CheckElement(driver, Elements.PST_SaveToMyAnalog_Widget_Selected_SaveTo_Tab, 2))
            {
                //--- Action: Select the Save To Tab ---//
                Console.Write("\t");
                IClick(driver, Elements.PST_SaveToMyAnalog_Widget_SaveTo_Tab);
            }

            //--- Action: Select the 2nd Option in the Save All Models to Dropdown ---//
            Console.Write("\t");
            IClick(driver, Elements.PST_SaveToMyAnalog_Widget_SaveAllModelsTo_Dropdown);
            Console.Write("\t");
            IClick(driver, By.CssSelector("div[class^='select-product dropdown']>ul>li:nth-of-type(2)"));

            //--- Action: Click the Save PST Button ---//
            Console.Write("\t");
            IClick(driver, Elements.PST_SaveToMyAnalog_Widget_SavePst_Button);
        }

        //----- MYANALOG > Save Products in myAnalog -----//
        public void ISaveProductsInMyAnalog(IWebDriver driver, string Locale, string Pdp_Url)
        {
            Console.WriteLine("Save Products in myAnalog:");

            //--- Pre-condition: The User must be already Logged In ---//

            //--- Action: Go to the Products Page ---//
            Console.Write("\t");
            driver.Navigate().GoToUrl(Configuration.Env_Url + Locale + Pdp_Url);
            Console.WriteLine("Go to: " + Configuration.Env_Url + Locale + Pdp_Url);

            //--- Action: Accept the Cookie ---//
            GivenIAcceptCookies(driver);

            //--- Action: Click the myAnalog Widget Button ---//
            Console.Write("\t");
            IClick(driver, Elements.MyAnalog_Widget_Btn);

            //--- Action: Click the Save Button ---//
            Console.Write("\t");
            IClick(driver, Elements.MyAnalog_Widget_Save_Button);
        }

        //----- MYANALOG > Save Resources in myAnalog -----//
        public void ISaveResourcesInMyAnalog(IWebDriver driver, string Locale, string Resources_Url)
        {
            Console.WriteLine("Save Resources in myAnalog:");

            //--- Pre-condition: The User must be already Logged In ---//

            //--- Action: Go to the Resources Page ---//
            Console.Write("\t");
            driver.Navigate().GoToUrl(Configuration.Env_Url + Locale + Resources_Url);
            Console.WriteLine("Go to: " + Configuration.Env_Url + Locale + Resources_Url);

            //--- Action: Accept the Cookie ---//
            GivenIAcceptCookies(driver);

            //--- Action: Click the myAnalog Widget Button ---//
            Console.Write("\t");
            IClick(driver, Elements.MyAnalog_Widget_Btn);

            //--- Action: Click the Save Button ---//
            Console.Write("\t");
            IClick(driver, Elements.MyAnalog_Widget_Save_Button);
        }

        //----- MYANALOG > Save PSTs in myAnalog -----//
        public void ISavePstInMyAnalog(IWebDriver driver, string Locale, string Pst_Url)
        {
            Console.WriteLine("Save PST in myAnalog:");

            //--- Pre-condition: The User must be already Logged In ---//

            //--- Action: Go to a PST Page ---//
            Console.Write("\t");
            driver.Navigate().GoToUrl(Configuration.Env_Url + Locale + Pst_Url);
            Thread.Sleep(1000);
            Console.WriteLine("Go to: " + Configuration.Env_Url + Locale + Pst_Url);

            //--- Action: Accept the Cookie ---//
            GivenIAcceptCookies(driver);

            //--- Action: Click the Save to myAnalog Button ---//
            Console.Write("\t");
            IClick(driver, Elements.PST_SaveToMyAnalog_Button);

            if (util.CheckElement(driver, Elements.PST_SaveToMyAnalog_Widget_Selected_SaveTo_Tab, 2))
            {
                //--- Action: Select the Save To Tab ---//
                Console.Write("\t");
                IClick(driver, Elements.PST_SaveToMyAnalog_Widget_SaveTo_Tab);
            }

            if (util.GetInputBoxValue(driver, Elements.PST_SaveToMyAnalog_Widget_TableName_InputBox).Equals(""))
            {
                string tableName_input = Locale + "_T3st_CustomTitle_" + DateTime.Now.ToString("MMddyyyy") + "_" + util.RandomString();

                IDeleteValueOnFields(driver, Elements.PST_SaveToMyAnalog_Widget_TableName_InputBox);
                Console.Write("\t");
                IType(driver, Elements.PST_SaveToMyAnalog_Widget_TableName_InputBox, tableName_input);
            }

            //--- Action: Click the Save PST Button ---//
            Console.Write("\t");
            IClick(driver, Elements.PST_SaveToMyAnalog_Widget_SavePst_Button);
        }

        //----- MYANALOG > Register an Evaluation Board in myAnalog -----//
        public void IRegisterAnEvalBoardInMyAnalog(IWebDriver driver, string Locale, string ProductNumber_Input)
        {
            Console.WriteLine("Register an Evaluation Board in myAnalog:");

            //--- Pre-condition: The User must be already Logged In ---//

            string myAnalogProducts_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/app/products";

            if (!driver.Url.Equals(myAnalogProducts_url + "#") && !driver.Url.EndsWith(myAnalogProducts_url))
            {
                //--- Action: Open myAnalog Products using direct link ---//
                driver.Navigate().GoToUrl(myAnalogProducts_url);
                Console.WriteLine("\tGo to: " + myAnalogProducts_url);
            }

            if (!util.CheckElement(driver, Elements.MyAnalog_Products_Selected_EvaluationBoards_Tab, 2))
            {
                //--- Action: Select the Evaluation Boards Tab ---//
                Console.Write("\t");
                IClick(driver, Elements.MyAnalog_Products_EvaluationBoards_Tab);
            }

            //--- Action: Click the "Register an evaluation board" button ---//
            Console.Write("\t");
            IClick(driver, Elements.MyAnalog_Products_RegisterAnEvaluationBoard_Button);

            //--- Action: Enter Product number in Product number field ---//
            IDeleteValueOnFields(driver, Elements.MyAnalog_Products_ProductNumber_InputBox);
            Console.Write("\t");
            IType(driver, Elements.MyAnalog_Products_ProductNumber_InputBox, ProductNumber_Input);

            Console.Write("\t");
            IClick(driver, Elements.MyAnalog_Products_ProductNumber_SuggestionsList_Items);

            //--- Action: Click the "Register your evaluation board" button ---//
            Console.Write("\t");
            IClick(driver, Elements.MyAnalog_Products_RegisterYourEvaluationBoard_Button);
        }

        //----- MYANALOG > I Remove All Registered Evaluation Boards in myAnalog -----//
        public void IRemoveAllRegisteredEvalBoardsInMyAnalog(IWebDriver driver, string Locale)
        {
            Console.WriteLine("Remove All Registered Evaluation Boards in myAnalog:");

            //--- Pre-condition: The User must be already Logged In ---//

            string myAnalogProducts_url = Configuration.Env_Url.Replace("www", "my").Replace("cldnet", "corpnt") + Locale + "/app/products";

            if (!driver.Url.Equals(myAnalogProducts_url + "#") && !driver.Url.EndsWith(myAnalogProducts_url))
            {
                //--- Action: Open myAnalog Products using direct link ---//
                driver.Navigate().GoToUrl(myAnalogProducts_url);
                Console.WriteLine("\tGo to: " + myAnalogProducts_url);
            }

            if (!util.CheckElement(driver, Elements.MyAnalog_Products_Selected_EvaluationBoards_Tab, 2))
            {
                //--- Action: Select the Evaluation Boards Tab ---//
                Console.Write("\t");
                IClick(driver, Elements.MyAnalog_Products_EvaluationBoards_Tab);
            }

            do
            {
                //--- Action: Click the ellipsis in any listed boards. ---//
                Console.Write("\t");
                IClick(driver, Elements.MyAnalog_Products_RegisteredEvaluationBoards_Ellipsis_Button);

                //--- Action: Click Remove in dropdown menu ---//
                Console.Write("\t");
                IClick(driver, Elements.MyAnalog_Products_RegisteredEvaluationBoards_Remove_Option);

                //--- Action: Click Remove in Confirmation Modal ---//
                Console.Write("\t");
                IClick(driver, Elements.MyAnalog_Products_RegisteredEvaluationBoards_Remove_Confirmation_Modal_Remove_Button);
            }
            while (util.GetCount(driver, Elements.MyAnalog_Products_RegisteredEvaluationBoards) != 0);
        }

        public void ILoginVia_LinkedIn(IWebDriver driver, string Username, string Password)
        {
            IType(driver, By.CssSelector("input[id='username']"), Username);
            IType(driver, By.CssSelector("input[id='password']"), Password);
            IClick(driver, By.CssSelector("button[aria-label='Sign in']"));
            if (util.CheckElement(driver, By.CssSelector("button[id='oauth__auth-form__submit-btn']"), 5))
            {
                IClick(driver, By.CssSelector("button[id='oauth__auth-form__submit-btn']"));
            }
        }

        public void ILoginVia_Google(IWebDriver driver, string Username, string Password)
        {
            IType(driver, By.CssSelector("input[id='identifierId']"), Username);
            IClick(driver, By.CssSelector("button[class='VfPpkd-LgbsSe VfPpkd-LgbsSe-OWXEXe-k8QpJ VfPpkd-LgbsSe-OWXEXe-dgl2Hf nCP5yc AjY5Oe DuMIQc qIypjc TrZEUc lw1w4b']"));
            Thread.Sleep(2000);
            IType(driver, By.CssSelector("input[name='password']"), Password);
            IClick(driver, By.CssSelector("button[class='VfPpkd-LgbsSe VfPpkd-LgbsSe-OWXEXe-k8QpJ VfPpkd-LgbsSe-OWXEXe-dgl2Hf nCP5yc AjY5Oe DuMIQc qIypjc TrZEUc lw1w4b']"));
        }

        public void ILoginVia_FB(IWebDriver driver, string Username, string Password)
        {
            IType(driver, By.CssSelector("input[id='email']"), Username);
            IType(driver, By.CssSelector("input[name='pass']"), Password);
            IClick(driver, By.CssSelector("button[id='loginbutton']"));
        }

        public void DeleteAccountInWebcontent(IWebDriver driver, string Username)
        {
            Navigate(driver, Configuration.Env_Url.Replace("www", "webuseradmin").Replace("cldnet", "corpnt") + "gdpradmin/deleteuserdata.aspx");
            IType(driver, By.CssSelector("input[id='txtEmail']"), Username);
            IType(driver, By.CssSelector("textarea[id='txtComments']"), "Test Only");
            IClick(driver, By.CssSelector("input[id='btnSubmit']"));
            Thread.Sleep(5000);
        }
    }
}